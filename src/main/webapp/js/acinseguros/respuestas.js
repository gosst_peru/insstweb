var listRespuestas;
var respuestaId;
var banderaEdicion2;

function verRespuestaSeguridad() {
	$('#modalRespuesta').on('show.bs.modal', function(e) {
		cargarRespuestas();
	
	});
	$("#modalRespuesta").modal("show");
	
}

function cargarRespuestas() {
	
	banderaEdicion2 = false;
	respuestaId = 0;
	$("#btnNuevoRpta").show();
	$("#btnVolverRpta").hide();
	$("#btnGuardarRpta").hide();
	$("#btnEliminarRpta").hide();

	
	$("#btnNuevoRpta").attr("onclick", "javascript:nuevoRespuestas();");
	$("#btnVolverRpta").attr("onclick", "javascript:cancelarRespuestas();");
	$("#btnGuardarRpta").attr("onclick", "javascript:guardarRespuestas();");
	$("#btnEliminarRpta").attr("onclick", "javascript:eliminarRespuestas();");
	
	var dataParam = {
		acInseguroId : acInseguroId
	};

	callAjaxPost(URL + '/acinseguro/respuestas', dataParam,
			procesarRespuestaDescargadaPrimerEstado);
	
	

}



function procesarRespuestaDescargadaPrimerEstado(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		var list = data.list;
		listRespuestas=list;
		
		$("#tblRespuestasSeguridad tbody tr").remove();

		for (index = 0; index < listRespuestas.length; index++) {

			$("#tblRespuestasSeguridad tbody:first").append(

					"<tr id='tr" + listRespuestas[index].respuestaId
							+ "' onclick='javascript:editarRespuesta("
							+ listRespuestas[index].respuestaId +","+index
							+ ")' >"

						
			

							+ "<td id='tddescripcion" + listRespuestas[index].respuestaId
							+ "'>"+listRespuestas[index].descripcion+"</td>"

							+ "<td id='tdevi" + listRespuestas[index].respuestaId
							+ "'>"+listRespuestas[index].evidenciaNombre+"</td>"

							+ "</tr>");
		}
		formatoCeldaSombreableTabla(true,"tblRespuestasSeguridad");
		
		break;
	default:
		alert("Ocurrió un error al traer las actos inseguros!");
	}

}

function editarRespuesta(prespuestaId,index) {
	if (!banderaEdicion2) {
		respuestaId = prespuestaId;
		$("#btnNuevoRpta").hide();
		$("#btnVolverRpta").show();
		$("#btnGuardarRpta").show();
		$("#btnEliminarRpta").show();
		
			
		var nombreEvidencia= listRespuestas[index].evidenciaNombre ;
	
		
		formatoCeldaSombreableTabla(false,"tblRespuestasSeguridad");
		var name = $("#tddescripcion" + respuestaId).text();
		$("#tddescripcion" + respuestaId)
				.html(
						"<input type='text' id='inputDesc' class='form-control' placeholder='Descripci&oacute;n'>");
		nombrarInput("inputDesc",name)
		


		$("#tdevi" + respuestaId)
				.html(
						"<a href='"
								+ URL
								+ "/acinseguro/respuesta/evidencia?respuestaId="
								+ respuestaId
								+ "' "
								+ "target='_blank'>"+nombreEvidencia+"</a>"
								+ "<br/><a href='#' onclick='javascript:mostrarCargarEvidenciaRespuesta("
								+ respuestaId + ")'  id='subirimagen"
								+ respuestaId + "'>Subir</a>");

		

		/** ************************************************************************* */
		
		banderaEdicion2 = true;
	
		
		
	}
}




function nuevoRespuestas() {
	if (!banderaEdicion2) {
		$("#btnNuevoRpta").hide();
		$("#btnVolverRpta").show();
		$("#btnGuardarRpta").show();
		$("#btnEliminarRpta").show();
		formatoCeldaSombreableTabla(false,"tblRespuestasSeguridad");
		$("#tblRespuestasSeguridad tbody")
				.append(
						"<tr id='0'>"
							
								+ "<td><input type='text' id='inputDesc' class='form-control' placeholder='Descripcion'></td>"
							+ "<td>...</td>" + "</tr>");
		respuestaId = 0;
		habilitarBotonesNuevo();
		banderaEdicion2 = true;
		
	} else {
		alert("Guarde primero.");
		
	}
}

function cancelarRespuestas() {
	verRespuestaSeguridad();
}

function eliminarRespuestas() {
	var r = confirm("¿Está seguro de eliminar el acto inseguro?");
	if (r == true) {
		var dataParam = {
				respuestaId : respuestaId
		};

		callAjaxPost(URL + '/acinseguro/respuesta/delete', dataParam,
				procesarResultadoEliminarRespuesta);
	}
}

function procesarResultadoEliminarRespuesta(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		cargarRespuestas();
		break;
	default:
		alert("Ocurrió un error al eliminar el acto inseguro!");
	}
}

function guardarRespuestas() {

	var campoVacio = true;
	var inputDesc = $("#inputDesc").val();
	

console.log(respuestaId+""+inputDesc);
	if (campoVacio) {

		var dataParam = {
			respuestaId : respuestaId,
			descripcion:inputDesc,
			acInseguroId:acInseguroId
		};

		callAjaxPost(URL + '/acinseguro/respuesta/save', dataParam,
				procesarResultadoGuardarRespuesta);
	} else {
		alert("Debe ingresar todos los campos.");
	}
}

function procesarResultadoGuardarRespuesta(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		cargarRespuestas();
		break;
	default:
		alert("Ocurrió un error al guardar el acto inseguro!");
	}
}



function mostrarCargarEvidenciaRespuesta(respuestaId) {
	$('#mdUploadRespuestas').modal('show');
	$('#btnUploadEvidenciaRespuesta').attr("onclick",
			"javascript:uploadEvidenciaRespuesta(" + respuestaId + ");");
}

function uploadEvidenciaRespuesta(respuestaId) {
	var inputFileImage = document.getElementById("fileEviResp");
	var file = inputFileImage.files[0];
	var data = new FormData();

	if (file.size > bitsEvidenciaRespuestaSeguridad) {
		alert("Lo sentimos, solo se pueden subir archivos menores a "+bitsEvidenciaRespuestaSeguridad/1000000+" MB");
	} else {
		data.append("fileEvi", file);
		data.append("respuestaId", respuestaId);
	
		var url = URL + '/acinseguro/respuesta/evidencia/save';
		$.blockUI({
			message : 'cargando...'
		});
		$
				.ajax({
					url : url,
					xhrFields: {
			            withCredentials: true
			        },
					type : 'POST',
					contentType : false,
					data : data,
					processData : false,
					cache : false,
					success : function(data, textStatus, jqXHR) {
						$.unblockUI();
						switch (data.CODE_RESPONSE) {
						case "06":
							sessionStorage.clear();
							document.location.replace(data.PATH);
							break;
						default:
							console.log('Se subio el archivo correctamente.');
							$('#mdUploadRespuestas').modal('hide');
						}

					},
					error : function(jqXHR, textStatus, errorThrown) {
						$.unblockUI();
						alert("Ups, tenemos problemas tecnicos. Intente más tarde. Error: "
								+ errorThrown);
						console.log('xhRequest: ' + jqXHR + "\n");
						console.log('ErrorText: ' + textStatus + "\n");
						console.log('thrownError: ' + errorThrown + "\n");
					}
				});
	}
}

/** *********************JsTree************** */
