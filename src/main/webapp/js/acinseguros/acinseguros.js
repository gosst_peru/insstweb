var banderaEdicion;
var acInseguroId;
var acInseguroTipoId;
var acInseguroCausa;
var areaId;
var reporteLugar;
var reporteTipo;
var reporteNombre;
var trabajadorId;

var reporteFecha;
var listArea;var listNivel;
var listActInsegTipo;
var listTrabsSelecccionados;
var listCausaBasica;
var listCausaInmediataSimple;
var listOrigenReporte;
var listTrabs;
var listDniNombre;
var listanombretab;
var listTabla;
var listTablaAux;
var listTablaAuxCopmleta;
var contador2;
var numPaginaTotal;
var numPaginaActual;

var tienePermisoReporte;
var fechaInicioFiltro="2000-01-01";
var fechaFinFiltro="2057-12-31";
var arrayFiltroTipo=[];
var arrayFiltroCausa=[];
var arrayFiltroOrigen=[];
var arrayFiltroNivel=[];
var arrayFiltroArea=[];

var toggleFiltroCausa;
function calcularFilasEntrantes(){
	var sizeArriba=245;
	var sizeAbajo=66;
	var alturaFila=58;
	var alturaWindow=$(window).height();
	var numFilasSugerido=Math.floor((alturaWindow-sizeAbajo-sizeArriba)/alturaFila);

	return numFilasSugerido
}
function filtrarListActosSubestandar(){ 
	 var listReturn=[];
listReturn=listTablaAux.filter(function(val,index){
			if( fechaIncluidaEnRango(convertirFechaNormal2(val.reporteFecha),fechaInicioFiltro,fechaFinFiltro)
					&& arrayFiltroArea.indexOf(parseInt(val.areaId))!=-1
					&&arrayFiltroOrigen.indexOf(parseInt(val.origen.id))!=-1
					&& arrayFiltroNivel.indexOf(val.nivelRiesgoId)!=-1
					&& arrayFiltroCausa.indexOf(val.acInseguroCausaId)!=-1
					&& arrayFiltroTipo.indexOf(val.acInseguroTipoId)!=-1){
				return true;
			}else{ 
				return false;
			}
		});
	return listReturn;
} 
$(document).ready(function() {  
	$(document).on("click","#filtroFechaEvento",function(){
		 $(".filtroMenuGosst").hide();
		 if($("#opcionesFiltroFecha").length>0){
			 $("#opcionesFiltroFecha").show(); 
		 } else{
			 $("body").append("<div class='filtroMenuGosst' id='opcionesFiltroFecha'></div>");
				var topDiv=$(this).position().top;
				var leftDiv=$(this).position().left;
				var cssFiltro={"top":199,"left":leftDiv+30 };
				var opcionesDiv=$("#opcionesFiltroFecha");
				opcionesDiv.css(cssFiltro).append("<div class='opcionFiltro'>" +
						"Desde: <input type='date' id='fechaInicioEvento' class='form-control'  value='2017-01-01'> " + 
						"Hasta: <input type='date' id='fechaFinEvento' class='form-control'  value='2017-12-31'> " + 
			"</div>");
				opcionesDiv.append("<br><button class='btn btn-danger' ><i class='fa fa-times' aria-hidden='true'> </i>Cancelar </button>");
				opcionesDiv.find(".btn-danger").on("click",function(){
					 $(".filtroMenuGosst").hide();
				});
				opcionesDiv.append("<button class='btn btn-success' ><i class='fa fa-check' aria-hidden='true'> </i>Listo </button>");
				opcionesDiv.find(".btn-success").on("click",function(){
					  fechaInicioFiltro=$("#fechaInicioEvento").val();
					  fechaFinFiltro=$("#fechaFinEvento").val();
					
					listTabla=filtrarListActosSubestandar();
					cambiarPaginaTabla();
					$(".filtroMenuGosst").hide();
					});
		 }
		
	});
	$(document).on("click","#filtroAreaEvento",function(){
		 $(".filtroMenuGosst").hide();
		 if($("#opcionesFiltroArea").length>0){
			 $("#opcionesFiltroArea").show(); 
		 } else{
			 $("body").append("<div class='filtroMenuGosst' id='opcionesFiltroArea'></div>");
				var topDiv=$(this).position().top;
				var leftDiv=$(this).position().left;
				var cssFiltro={"top":199,"left":leftDiv+30 };
				var opcionesDiv=$("#opcionesFiltroArea");
				listArea.forEach(function(val,index){
					var textoChecked="checked";
					if(val.filtroActivado){
						textoChecked="checked";
					}else{ 
						
					}
					opcionesDiv.css(cssFiltro).append("<div class='opcionFiltro'>" +
							"<input type='checkbox' id='checkFiltro"+val.areaId+"' "+textoChecked+"> " +
							val.areaName+"" +
							"</div>");
				}); 
				opcionesDiv.append("<br><button class='btn btn-danger' ><i class='fa fa-times' aria-hidden='true'> </i>Cancelar </button>");
				opcionesDiv.find(".btn-danger").on("click",function(){
					 $(".filtroMenuGosst").hide();
				});
				opcionesDiv.append(" <button class='btn btn-info'> <i class='fa  fa-pencil-square-o' aria-hidden='true'></i> </button>");
				opcionesDiv.find(".btn-info").on("click",function(){
					toggleFiltroCausa=!toggleFiltroCausa;
					 $("#opcionesFiltroArea .opcionFiltro input").each(function(index,elem){
						 
						 if(toggleFiltroCausa){
							 $(elem).prop("checked",false);
						 }else{
							 $(elem).prop("checked",true);
						 }
					 });
				});
				opcionesDiv.append(" <button class='btn btn-success' ><i class='fa fa-check' aria-hidden='true'></i>Listo </button>");
				opcionesDiv.find(".btn-success").on("click",function(){
					 
					arrayFiltroArea=[]; 
					$("#opcionesFiltroArea .opcionFiltro input").each(function(index,elem){
						if($(elem).prop("checked")){
							var idAux=parseInt($(elem).prop("id").substr(11,4));
						arrayFiltroArea.push(idAux);}
					 
					});  
					listTabla=filtrarListActosSubestandar();
					cambiarPaginaTabla();
					$(".filtroMenuGosst").hide();
					});
		 }
		
	});
	$(document).on("click","#filtroNivelEvento",function(){
		 $(".filtroMenuGosst").hide();
		 if($("#opcionesFiltroNivel").length>0){
			 $("#opcionesFiltroNivel").show(); 
		 } else{
		$("body").append("<div class='filtroMenuGosst' id='opcionesFiltroNivel'></div>");
		var topDiv=$(this).position().top;
		var leftDiv=$(this).position().left;
		var cssFiltro={"top":199,"left":leftDiv+30 };
		var opcionesDiv=$("#opcionesFiltroNivel");
		listNivel.forEach(function(val,index){
			var textoChecked="checked";
			if(val.filtroActivado){
				textoChecked="checked";
			}else{
				
			}
			opcionesDiv.css(cssFiltro).append("<div class='opcionFiltro'>" +
					"<input type='checkbox' id='checkFiltro"+val.nivelRiesgoId+"' "+textoChecked+"> " +
					val.nivelRiesgoNombre+"" +
					"</div>");
		});
		opcionesDiv.append("<br><button class='btn btn-danger' ><i class='fa fa-times' aria-hidden='true'> </i>Cancelar </button>");
		opcionesDiv.find(".btn-danger").on("click",function(){
			 $(".filtroMenuGosst").hide();
		});
		opcionesDiv.append(" <button class='btn btn-info'> <i class='fa  fa-pencil-square-o' aria-hidden='true'></i> </button>");
		opcionesDiv.find(".btn-info").on("click",function(){
			toggleFiltroCausa=!toggleFiltroCausa;
			 $("#opcionesFiltroNivel .opcionFiltro input").each(function(index,elem){
				 
				 if(toggleFiltroCausa){
					 $(elem).prop("checked",false);
				 }else{
					 $(elem).prop("checked",true);
				 }
			 });
		});
		opcionesDiv.append("<button class='btn btn-success' ><i class='fa fa-check' aria-hidden='true'></i>Listo </button>");
		opcionesDiv.find(".btn-success").on("click",function(){
			 
			arrayFiltroNivel=[]; 
			$("#opcionesFiltroNivel .opcionFiltro input").each(function(index,elem){
				if($(elem).prop("checked")){
				var idAux=parseInt($(elem).prop("id").substr(11,4));
				arrayFiltroNivel.push(idAux);
				}
			}); 
			listTabla=filtrarListActosSubestandar();
			cambiarPaginaTabla();
			$(".filtroMenuGosst").hide();
			});
		 }
	});
	
	$(document).on("click","#filtroTipoEvento",function(){
		 $(".filtroMenuGosst").hide();
		 if($("#opcionesFiltroTipo").length>0){
			 $("#opcionesFiltroTipo").show(); 
		 } else{
		$("body").append("<div class='filtroMenuGosst' id='opcionesFiltroTipo'></div>");
		var topDiv=$(this).position().top;
		var leftDiv=$(this).position().left;
		var cssFiltro={"top":199,"left":leftDiv+30 };
		var opcionesDiv=$("#opcionesFiltroTipo");
		listActInsegTipo.forEach(function(val,index){
			var textoChecked="checked";
			if(val.filtroActivado){
				textoChecked="checked";
			}else{
				
			}
			opcionesDiv.css(cssFiltro).append("<div class='opcionFiltro'>" +
					"<input type='checkbox' id='checkFiltro"+val.actTipoId+"' "+textoChecked+"> " +
					val.actTipoName+"" +
					"</div>");
		});
		opcionesDiv.append("<br><button class='btn btn-danger' ><i class='fa fa-times' aria-hidden='true'> </i>Cancelar </button>");
		opcionesDiv.find(".btn-danger").on("click",function(){
			 $(".filtroMenuGosst").hide();
		});
		opcionesDiv.append(" <button class='btn btn-success' ><i class='fa fa-check' aria-hidden='true'></i>Listo </button>");
		opcionesDiv.find(".btn-success").on("click",function(){
			 
			arrayFiltroTipo=[]; 
			$("#opcionesFiltroTipo .opcionFiltro input").each(function(index,elem){
				if($(elem).prop("checked")){
				var idAux=parseInt($(elem).prop("id").substr(11,4));
				arrayFiltroTipo.push(idAux);
			}
			}); 
			listTabla=filtrarListActosSubestandar();
			cambiarPaginaTabla();
			$(".filtroMenuGosst").hide();
			});
		 }
	});
	$(document).on("click","#filtroTipoOrigen",function(){
		 $(".filtroMenuGosst").hide();
		 if($("#opcionesFiltroOrigen").length>0){
			 $("#opcionesFiltroOrigen").show(); 
		 } else{
		$("body").append("<div class='filtroMenuGosst' id='opcionesFiltroOrigen'></div>");
		var topDiv=$(this).position().top;
		var leftDiv=$(this).position().left;
		var cssFiltro={"top":199,"left":leftDiv+30 };
		var opcionesDiv=$("#opcionesFiltroOrigen");
		listOrigenReporte.forEach(function(val,index){
			var textoChecked="checked";
			if(val.filtroActivado){
				textoChecked="checked";
			}else{
				
			}
			opcionesDiv.css(cssFiltro).append("<div class='opcionFiltro'>" +
					"<input type='checkbox' id='checkFiltro"+val.id+"' "+textoChecked+"> " +
					val.nombre+"" +
					"</div>");
		});
		opcionesDiv.append("<br><button class='btn btn-danger' ><i class='fa fa-times' aria-hidden='true'> </i>Cancelar </button>");
		opcionesDiv.find(".btn-danger").on("click",function(){
			 $(".filtroMenuGosst").hide();
		});
		opcionesDiv.append(" <button class='btn btn-success' ><i class='fa fa-check' aria-hidden='true'></i>Listo </button>");
		opcionesDiv.find(".btn-success").on("click",function(){
			 
			arrayFiltroOrigen=[]; 
			$("#opcionesFiltroOrigen .opcionFiltro input").each(function(index,elem){
				if($(elem).prop("checked")){
				var idAux=parseInt($(elem).prop("id").substr(11,4));
				arrayFiltroOrigen.push(idAux);
			}
			}); 
			listTabla=filtrarListActosSubestandar();
			cambiarPaginaTabla();
			$(".filtroMenuGosst").hide();
			});
		 }
	});
	$(document).on("click","#filtroCausaEvento",function(){
		 $(".filtroMenuGosst").hide();
		 if($("#opcionesFiltroCausa").length>0){
			 $("#opcionesFiltroCausa").show(); 
		 } else{
			$("body").append("<div class='filtroMenuGosst' id='opcionesFiltroCausa'></div>");
			var topDiv=$(this).position().top;
			var leftDiv=$(this).position().left;
			var cssFiltro={"top":199,"left":leftDiv+30 };
			var opcionesDiv=$("#opcionesFiltroCausa");
			listCausaBasica.forEach(function(val,index){
				var textoChecked="checked";
				if(val.filtroActivado){
					textoChecked="checked";
				}else{
					
				} 
				opcionesDiv.css(cssFiltro).append("<div class='opcionFiltro'>" +
						"<input type='checkbox' id='checkFiltro"+val.causaInmId+"'  "+textoChecked+"> " +
						val.causaInmNombre+"" +
						"</div>");
			});
			opcionesDiv.append("<br><button class='btn btn-danger' ><i class='fa fa-times' aria-hidden='true'> </i>Cancelar </button>");
			opcionesDiv.find(".btn-danger").on("click",function(){
				 $(".filtroMenuGosst").hide();
			});
			opcionesDiv.append(" <button class='btn btn-info'> <i class='fa  fa-pencil-square-o' aria-hidden='true'></i> </button>");
			opcionesDiv.find(".btn-info").on("click",function(){
				toggleFiltroCausa=!toggleFiltroCausa;
				 $("#opcionesFiltroCausa .opcionFiltro input").each(function(index,elem){
					 
					 if(toggleFiltroCausa){
						 $(elem).prop("checked",false);
					 }else{
						 $(elem).prop("checked",true);
					 }
				 });
			});
			opcionesDiv.append(" <button class='btn btn-success'> <i class='fa fa-check' aria-hidden='true'></i>Listo </button>");
			opcionesDiv.find(".btn-success").on("click",function(){
				 
				arrayFiltroCausa=[]; 
				$("#opcionesFiltroCausa .opcionFiltro input").each(function(index,elem){
					if($(elem).prop("checked"))
					var idAux=parseInt($(elem).prop("id").substr(11,4));
					arrayFiltroCausa.push(idAux);
				 
				});
		 
				listTabla=filtrarListActosSubestandar();
				cambiarPaginaTabla();
				$(".filtroMenuGosst").hide();
				});
		 
		 }
	 
	});
	
	
	
	$(document).on('change', ':file', function() {
	    var input = $(this),
	        numFiles = input.get(0).files ? input.get(0).files.length : 1,
	        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
	    input.trigger('fileselect', [numFiles, label]);
	    $(".modal-body img").hide();
	  });
	 $(document).on('fileselect',"input:file", function(event, numFiles, label) {
		 
		          var input = $(this).parents('.input-group').find(':text'),
		              log = numFiles > 1 ? numFiles + ' files selected' : label;

		          if( input.length ) {
		              input.val(log);
		          } else {
		              if( log ) alert(log);
		          }

		      });
	
	
	
	 $('.izquierda_flecha').on('click',function(){
		 numPaginaActual=numPaginaActual-1
    cambiarPaginaTabla();
    });
	 $('.derecha_flecha').on('click',function(){
		 numPaginaActual=numPaginaActual+1;
		 cambiarPaginaTabla();
    });
	 $('.izquierda_flecha').hover(function(){
         $(this).css('opacity','0.5');
     },function(){
         $(this).css('opacity','1');
     });

     $('.derecha_flecha').hover(function(){
         $(this).css('opacity','0.5');
     },function(){
         $(this).css('opacity','1');
     });
	insertMenu();
	cargarPrimerEstado();
	$("#mdVerif").load("agregaraccionmejora.html");
	$("#cambiarBuscador").attr("onclick","toggleBuscadorJstree(2)");
	
	$("#listTrabBuscar").hide();
	 
});

function cargarPrimerEstado() {
	numPaginaActual=1
	banderaEdicion = false;
	acInseguroId = 0;
	acInseguroTipoId = 0;
	acInseguroCausa=0;
	areaId = 0;
	reporteLugar = '';
	reporteTipo = -1;
	reporteNombre = '';
	reporteFecha = 0;
	listTrabsSelecccionados = [];
	listOrigenReporte=[];
	listTrabs = [];
	listCausaBasica=[];
	listCausaInmediataSimple=[];
	removerBotones();
	crearBotones();
	
	$("#btnUpload").attr("onclick", "javascript:importarDatos();");

	contador2=0;
	
$("#fsBotones")
		.append(
				"<button id='btnGenReporte' type='button' class='btn btn-success' " +
				"title='Generar Informe'>"
						+ "<i class='fa fa-file-word-o fa-2x'></i>"
						+ "</button>");
$("#fsBotones")
.append("<button id='btnClipTabla' type='button' "
  + " class='btn btn-success' >"
	+"<i class='fa fa-clipboard fa-2x'></i></button>");

$("#fsBotones")
.append("<button id='btnReportExcel' type='button' "
  + " class='btn btn-success btn-excel' >"
	+"<i class='fa fa-file-excel-o fa-2x'></i></button>");

$("#fsBotones")
.append("<button id='btnClearFiltro' type='button' title='Quitar filtros'"
  + " class='btn btn-success btn-refresh' >"
	+"<i class='fa fa-filter fa-2x'></i></button>");
	deshabilitarBotonesEdicion();
	$("#btnGenReporte").hide();
	$("#btnNuevo").attr("onclick", "javascript:nuevoACInseguros();");
	$("#btnCancelar").attr("onclick", "javascript:cancelarACInseguros();");
	$("#btnGuardar").attr("onclick", "javascript:guardarACInseguros();");
	$("#btnEliminar").attr("onclick", "javascript:eliminarACInseguros();");
	$("#btnGenReporte").attr("onclick", "javascript:generarReporteActo();");
	$("#btnImportar").attr("onclick", "javascript:importarDatos();");
	$("#btnClipTabla").on("click",function(){
		new Clipboard('#btnClipTabla', {
			text : function(trigger) {
				var listFullTabla="";
				listFullTabla="Tipo" +"\t"
				+"Causa Inmediata" +"\t"
				+"Nivel de Riesgo" +"\t"
				+"Descripción" +"\t"
				+"Fecha Reporte" +"\t"
				+"Área" +"\t"
				+"Lugar específico" +"\t"
				+"Reporta" +"\t"+"Evidencia" +"\t"+"Acción de mejora" +"\t"+" \n";
				for (var index = 0; index < listTabla.length; index++){

				
					listFullTabla=listFullTabla
					+listTabla[index].acInseguroTipoNombre+"\t"
					+listTabla[index].acInseguroCausaNombre+"\t"
					+listTabla[index].nivelRiesgoNombre+"\t"
					+listTabla[index].acInseguroNombre+"\t"
					+convertirFechaNormal(listTabla[index].reporteFecha)+"\t"
					+listTabla[index].areaNombre +"\t"
					+listTabla[index].reporteLugar +"\t"
					+listTabla[index].reporteNombre +"\t"
					+(listTabla[index].evidenciaNombre=="----"?"NO":"SI") +"\t"

					+listTabla[index].nombreAccionMejora+"\t"
					+"\n";
				
				

				}
				
				 return listFullTabla;
				
			}
		});

		
		alert("Se han guardado al clipboard la tabla de este módulo" );

	});
	$("#btnReportExcel").on("click",function(){
		var listaActosId="";
		var numInvalidos=0;
		listTabla.forEach(function(val,index){ 
			if(comprabarImagen(val.evidenciaNombre)){
				listaActosId=listaActosId+val.acInseguroId+","
			}else{
				numInvalidos+=1; 
			}
			
		});
		if(numInvalidos==0){
			window.open(URL
					+ "/acinseguro/reporte/excel?listActos="
					+ listaActosId + '&nada=_blank');	
		}else{
			alert("Algunos a/c subestándar tienen evidencias que no son imagenes.")
		}
		 
	});
	$("#btnClearFiltro").on("click",function(){
		arrayFiltroTipo=listarStringsDesdeArray(listActInsegTipo, "actTipoId");
		arrayFiltroCausa=listarStringsDesdeArray(listCausaBasica, "causaInmId");
		arrayFiltroNivel=listarStringsDesdeArray(listNivel, "nivelRiesgoId");
		arrayFiltroArea=listarStringsDesdeArray(listArea, "areaId");
		fechaInicioFiltro="2017-01-01";
		fechaFinFiltro="2017-12-31";
		$("#fechaInicioFiltro").val(fechaInicioFiltro);
		$("#fechaFinFiltro").val(fechaFinFiltro);
		$(".filtroMenuGosst .opcionFiltro input").each(function(index,elem){
			$(elem).prop("checked",true);
		 
		});  
		listTabla=listTablaAuxCopmleta;
		numPaginaActual=1;
		cambiarPaginaTabla();
	});
	var dataParam = {
	matrixId : getUrlParameter("mdfId"),
	companyId:  sessionStorage.getItem("gestopcompanyid")
	};
	var dataParamMovil = {
			matrixId : getUrlParameter("mdfId"),
			companyId:  sessionStorage.getItem("gestopcompanyid"),
			isRevisado: 0
			};
	if( isMobile.any() ) {
		callAjaxPost(URL + '/acinseguro/movil', dataParamMovil,
				procesarDataDescargadaPrimerEstadoMovil,function(){});
	}else{
		callAjaxPost(URL + '/acinseguro', dataParam,
				procesarDataDescargadaPrimerEstado);
	};
	goheadfixed("table.fixed");
	var dataParam2 = {
			idCompany: sessionStorage.getItem("gestopcompanyid")
		};
	
	callAjaxPost(URL + '/accidente/trab', dataParam2,
			function (data){
		switch (data.CODE_RESPONSE) {
			
			case "05":
			
				 listDniNombre=data.listDniNombre;
				
				
			
				
				break;
			default:
				alert("Ocurrió un error al traer los trabajadores!");
			}

		listanombretab=listarStringsDesdeArray(listDniNombre, "nombre");
		},function(){});
	


}

function listaTrabAcordeTipo(){
	if($("#selTipoRep").val()=="1"){
		ponerListaSugerida("inputNomRep",listanombretab,true);
		}
		
	if($("#selTipoRep").val()=="2")
	{ ponerListaSugerida("inputNomRep",listanombretab,false)}
	
}

function ponerGenerado(isReportaVacio,isLugarVacio,idCasilla){
	var generado;
	
	if(isReportaVacio=="" || isLugarVacio==""){
		generado="<td id='tdrep"+idCasilla+"' style='background-color:#C0504D;color:white'>No hay</td>"
	}else{
		generado="<td id='tdrep"+idCasilla+"' style='background-color:#9BBB59;color:white'>Generado</td>"
	}
	return generado
}
var htmlEvidencia="";
function cambiarPaginaTabla(){
	


	//Definiendo variables
	
	var list = listTabla;
	var totalSize=list.length;
	
	var partialSize=calcularFilasEntrantes();
	numPaginaTotal=Math.ceil(totalSize/partialSize);
	var inicioRegistro=(numPaginaActual*partialSize)-partialSize;
	var finRegistro=(numPaginaActual*partialSize)-1;
	
	//aplicando logica de compaginacion
	
	$('.izquierda_flecha').show();
	 $('.derecha_flecha').show();
	 if(numPaginaTotal==0){
			numPaginaTotal=1;
		}
		$("#labelFlechas").html(numPaginaActual+" de "+numPaginaTotal);
	 if(numPaginaActual==1){
			$('.izquierda_flecha').hide();
	 }
	
	 if(numPaginaActual==numPaginaTotal){
		 $('.derecha_flecha').hide();
	 }
	 
	if(finRegistro>=totalSize){
		finRegistro=totalSize-1;
	}
	
$("#tblInseg tbody tr").remove();
	for (var index = inicioRegistro; index <= finRegistro; index++) {
		var parteGenerado=ponerGenerado(
				list[index].reporteNombre,
				list[index].reporteLugar,list[index].acInseguroId );
		var functionClick=" onclick='javascript:editarACInseguros("
					+ list[index].acInseguroId +","+index
					+ ")' "
			$("#tblInseg tbody").append(
					"<tr id='tr" + list[index].acInseguroId
					+ "' >"

					+ "<td id='tdtipact" + list[index].acInseguroId
					+ "' "+functionClick+">" + list[index].acInseguroTipoNombre + "</td>"
					+ "<td id='tdori" + list[index].acInseguroId
					+ "' "+functionClick+">" + list[index].origen.nombre + "</td>"
					
					+ "<td id='tdcausa" + list[index].acInseguroId
					+ "' "+functionClick+">" + list[index].acInseguroCausaNombre + "</td>"
					+ "<td id='tdnivel" + list[index].acInseguroId
					+ "' "+functionClick+">" + list[index].nivelRiesgoNombre + "</td>"

					+ "<td id='tdnom" + list[index].acInseguroId + "' "+functionClick+">"
					+ list[index].acInseguroNombre + "</td>"
					
					+ "<td id='tdfecha" + list[index].acInseguroId + "' "+functionClick+">"
					+ convertirFechaNormal(list[index].reporteFecha) + "</td>"

					+ "<td id='tdare" + list[index].acInseguroId + "' "+functionClick+">"
					+ list[index].areaNombre + "</td>"

					+parteGenerado
	

					+ "<td id='tdevi" 
					+ list[index].acInseguroId
					+ "'><i class='fa fa-picture-o' aria-hidden='true'></i>"+list[index].evidenciaNombre+"</td>"

					+ "<td id='tdaccmej" + list[index].acInseguroId
					+ "' "+functionClick+">" + list[index].iconoEstadoAccion +list[index].nombreAccionMejora+"</td>"

					+ "</tr>");
		$("#tdevi" + list[index].acInseguroId).on("click",function(e){
			var actoId=parseInt($(this).prop("id").substr(5,9));
			if(!banderaEdicion || actoId==acInseguroId){
			callAjaxPost(URL + '/acinseguro/id', {acInseguroId:actoId},
					function(data){
				var alturaAux=$(window).height()*0.7;
				var anchoAux=$(window).height()*0.8;
				var nombreEvidencia=htmlEvidencia;
				var imagenPrevia=insertarImagenParaTabla(data.actos.evidencia,alturaAux+"px","100%");
				$("#mdVistaPrevia").modal("show");
				
				$("#mdVistaPrevia .modal-body #btnGuardarVista").attr("onclick","uploadEvidencia("+actoId+")")
				$("#mdVistaPrevia .modal-body #btnDescargarVista").attr("onclick","location.href='"+ URL
								+ "/acinseguro/evidencia?acInseguroId="
								+ actoId+"'")
				$("#mdVistaPrevia .modal-body #divVistaImagen").html(imagenPrevia)				
				.css({"background-color":"","color":""});
			},function(){
				
				$("#tdevi" + actoId).html("<img src='../imagenes/gif/ruedita.gif'></img>Descargando");
				
			});
			}

		});
		$("#tdevi" + list[index].acInseguroId).hover(function(){
			var actoId=parseInt($(this).prop("id").substr(5,9));
			if(!banderaEdicion || actoId==acInseguroId){
				$(this).css({"background-color":"#1c9da9","color":"white"});
				htmlEvidencia=$(this).html();
				$(this).html("<i class='fa fa-picture-o' aria-hidden='true'></i>Vista Previa")
				;
			}
			
			},function(){
				var actoId=parseInt($(this).prop("id").substr(5,9));
				if(!banderaEdicion || actoId==acInseguroId){
				$(this).html(htmlEvidencia);
				$(this).css({"background-color":"","color":""});
				}
			})
	}	
	
}
 var tipoGraf=1;
function procesarDataDescargadaPrimerEstadoMovil(data){
	switch (data.CODE_RESPONSE) {
	case "05":
		$("#eventosGosstDesktop").remove(); 
		listActInsegTipo = data.listActInsegTipo;
		listArea = data.listArea;
		listCausaBasica=data.listCausaInmediata; 
		listCausaInmediataSimple=data.listCausaInmediataSimple;
		listNivel=data.listNivel;
		listOrigenReporte=data.listOrigen;
		var numActos=data.numActos;
		$("#buscarEventoGosst .textoMuestra").append("<div class='alertaEventos'>"+numActos+"<div>")
		arrayFiltroTipo=listarStringsDesdeArray(listActInsegTipo, "actTipoId");
		arrayFiltroCausa=listarStringsDesdeArray(listCausaBasica, "causaInmId");
		arrayFiltroNivel=listarStringsDesdeArray(listNivel, "nivelRiesgoId");
		arrayFiltroArea=listarStringsDesdeArray(listArea, "areaId");
		arrayFiltroOrigen=listarStringsDesdeArray(listOrigenReporte, "id");
		completarBarraCarga();
		ajustarBarraSizeTop("eventosGosstMovil");
		if(sessionStorage.getItem("trabajadorGosstId")>0){
			$("#graficoResumenEventos").remove();
			$("#buscarEventoGosst").remove();
		}
		break;
	default:
		alert("Ocurrió un error al traer las actos inseguros!");
	}
}
function procesarDataDescargadaPrimerEstado(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		$("#eventosGosstMovil").remove();
		var list = data.list;
		listTabla=list;
		listTablaAux=list;
		listTablaAuxCopmleta=list;
		listActInsegTipo = data.listActInsegTipo;
		listArea = data.listArea;
		listCausaBasica=data.listCausaInmediata;
		listNivel=data.listNivel;
		listOrigenReporte=data.listOrigen;
		arrayFiltroTipo=listarStringsDesdeArray(listActInsegTipo, "actTipoId");
		arrayFiltroCausa=listarStringsDesdeArray(listCausaBasica, "causaInmId");
		arrayFiltroNivel=listarStringsDesdeArray(listNivel, "nivelRiesgoId");
		arrayFiltroArea=listarStringsDesdeArray(listArea, "areaId");
		arrayFiltroOrigen=listarStringsDesdeArray(listOrigenReporte, "id");
		cambiarPaginaTabla();
		completarBarraCarga();
		formatoCeldaSombreable(true);
		
		break;
	default:
		alert("Ocurrió un error al traer las actos inseguros!");
	}

}

function editarACInseguros(pacInseguroId,index) {
	if (!banderaEdicion) {
		acInseguroId = pacInseguroId;
		
		tienePermisoReporte=false;
			
		acInseguroTipoId= listTabla[index].acInseguroTipoId ;
		acInseguroCausa= listTabla[index].acInseguroCausaId ;
		
		areaId= listTabla[index].areaId ;
		reporteLugar= listTabla[index].reporteLugar;
		var reporteAccion= listTabla[index].reporteAccion;
		reporteTipo= listTabla[index].reporteTipo ;
		reporteNombre= listTabla[index].reporteNombre; 
		reporteFecha= listTabla[index].reporteFecha; 
		var nivelId=listTabla[index].nivelRiesgoId;
		var origenId=listTabla[index].origen.id;
		var nombreAccionMejora=listTabla[index].nombreAccionMejora
		listTrabsSelecccionados = [];
		listTrabs = [];
		
		formatoCeldaSombreable(false);
		var name = $("#tdnom" + acInseguroId).text();
		$("#tdnom" + acInseguroId)
				.html(
						"<input type='text' id='inputNom' class='form-control' placeholder='Descripci&oacute;n'>");
		nombrarInput("inputNom",name)
		$("#tdrep" + acInseguroId)
				.html(
						"<a href='#' onclick='javascript:editarDatosReporte()' style='color:white'>Editar Reporte</a>");

		$("#tdtrabs")
				.html(
						"<a href='#' onclick='javascript:seleccionarTrabajadores();'>Trabajadores</a>");
		$("#tdrespuestas")
		.html(
				"<a href='#' onclick='javascript:verRespuestaSeguridad();'>Sanciones</a>");
		$("#tdaccmej" + acInseguroId).html(
				"<a href='#' onclick='javascript:agregarAccionMejora(3, "
						+ acInseguroId + ",9);'>"+nombreAccionMejora+"</a>");

		$("#inputLug").val(reporteLugar);
		$("#inputNomRep").val(reporteNombre);
		$("#inputAccRep").val(reporteAccion);

		
		$("#selTipoRep").val(reporteTipo);	
		listaTrabAcordeTipo();
		var evidenciaNombre= listTabla[index].evidenciaNombre; 
		if(evidenciaNombre!="----"){
			tienePermisoReporte=true;
		}
		
		
		
		
		
		
		

		$("#tdfecha" + acInseguroId)
		.html(
				"<input type='date' id='inputFechaRep' class='form-control'  value='"
						+ convertirFechaInput(reporteFecha) + "'>");

		cargarModalTrabs();

		/** ************************************************************************* */
		var selActTipo = crearSelectOneMenu("selActTipo", "verCategAcordeTipoActoInseguro()", listActInsegTipo,
				acInseguroTipoId, "actTipoId", "actTipoName");
		$("#tdtipact" + acInseguroId).html(selActTipo);
		var selOrigen=crearSelectOneMenu("selOrigen", "", listOrigenReporte, origenId,
				"id", "nombre");
		$("#tdori"+acInseguroId).html(selOrigen);
		var selArea = crearSelectOneMenu("selArea", "", listArea, areaId,
				"areaId", "areaName");
		$("#tdare" + acInseguroId).html(selArea);
		
		var selCausa = crearSelectOneMenuY("selCausa", "", listCausaBasica, acInseguroCausa,
				"causaInmId","causaInmTipoEvent", "causaInmNombre");
		$("#tdcausa" + acInseguroId).html(selCausa);

		var selNivel = crearSelectOneMenu("selNivel", "", listNivel, nivelId,
				"nivelRiesgoId", "nivelRiesgoNombre");
		$("#tdnivel" + acInseguroId).html(selNivel);
		
		 
		banderaEdicion = true;
		habilitarBotonesEdicion(); 
		
	}
}

function cambiarGraficoEvento(){
	$("#grafEventosGosst").html("");
	switch(tipoGraf){
	case 1:
		tipoGraf=2;
		var data=[];
		var resto={name:"Otros",y:0};
		var totalAux=0;
		listCausaBasica.forEach(function(val,index){
			totalAux+=val.numEventosAsociados; 
		});
		listCausaBasica.forEach(function(val,index){ 
			if(val.numEventosAsociados>0){
				if(index<5){
					var porcAux=pasarDecimalPorcentaje(val.numEventosAsociados/totalAux,2);
					data.push({
						name:" ("+porcAux+") "+val.causaInmNombre,
						y:val.numEventosAsociados
					})
				}else{
					resto.y+=val.numEventosAsociados
				}
			}
			
		});
		resto.name="("+pasarDecimalPorcentaje(resto.y/totalAux,2)+")"+" Otros ";
		if(resto.y>0){
			data.push(resto);
		}

		 
		Highcharts.chart('grafEventosGosst', {
	        chart: { 
	            type: 'bar'
	        },
	        legend: {
	            enabled: false
	        },
	        xAxis:{visible:false},
	        yAxis:{visible:false},
	        title: {
	            text: '<span onclick="cambiarGraficoEvento()"><i class="fa fa-arrow-left" aria-hidden="true"></i></span>'
	            	+'<label style="font-size:20px;font-weight:900">Causas Inmediatas</label>' 
	            	+'<span onclick="cambiarGraficoEvento()"><i class="fa fa-arrow-right" aria-hidden="true"></i></span>',
	            useHTML:true,
	            style:{
	            	fontSize:"20px !important",
	            	fontWeight:"900 !important"
	            }
	        },
	        tooltip: {
	        	useHTML: true,
	        	headerFormat:"",
	            pointFormat: '<b style="font-size:">{point.name}</b>'
	        },
	        plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                    align: 'right',
	                    color: '#FFFFFF',
	                    style:{
	                    	fontSize:"15px !important"
	                    },
	                    x: -10
	                },
	                pointPadding: 0.1,
	                groupPadding: 0
	            }
	        },
	        series: [{
	            name: 'Causas Inmediatas',
	            colorByPoint: true,
	            data: data
	        }]
	    });
		break;
	case 2:
		tipoGraf=1;
		var dataGraf1=[];
		var colors=["#FA325D","#FFBC28","#F8F554","#12A882","#53D321","#8621D3","",""]
		listNivel.forEach(function(val,index){
			if(val.numEventosAsociados>0)
			dataGraf1.push({
				name:val.nivelRiesgoNombre,
				y:val.numEventosAsociados,
				color:{
		            radialGradient: {
		                cx: 0.5,
		                cy: 0.3,
		                r: 0.7
		            },
		            stops: [
		                [0, colors[index]],
		                [1, Highcharts.Color(colors[index]).brighten(-0.3).get('rgb')] // darken
		            ]
		        }
			});
		});
		
		 Highcharts.chart('grafEventosGosst', {
	            chart: {
	                plotBackgroundColor: null,
	                plotBorderWidth: null,
	                plotShadow: false,
	                type: 'pie'
	            },
	            title: {
	                text: '<span onclick="cambiarGraficoEvento()"><i class="fa fa-arrow-left" aria-hidden="true"></i></span>'
	                	+'<label style="font-size:20px;font-weight:900">Nivel de riesgo</label>' 
	                	+'<span onclick="cambiarGraficoEvento()"><i class="fa fa-arrow-right" aria-hidden="true"></i></span>',
	                useHTML:true,
	                style:{
	                	fontSize:"20px !important",
	                	fontWeight:"900 !important"
	                }
	            },
	            tooltip: {
	            	headerFormat:"",
	                pointFormat: '<b style="font-size:15px;font-weight:900">{point.name}: {point.percentage:.1f}%</b>'
	            },
	            plotOptions: {
	                pie: {
	                    allowPointSelect: true,
	                    cursor: 'pointer',
	                    size: "110%",
	                    dataLabels: {
	                        enabled: true,
	                        distance: -20,format:"{y}",
	                        style:{
	                        	fontSize:"12px !important"
	                        }
	                    },
	                    showInLegend: false
	                }
	            },
	            series: [{
	                name: 'Nivel de riesgo',
	                colorByPoint: true,
	                data: dataGraf1
	            }]
	        });
		break;
	}
	

}

function verCategAcordeTipoActoInseguro(){
	var auxTipoActo=$("#selActTipo").val();
	
switch(auxTipoActo){
case "2":

  $("#selCausa option").each(function()
			{	
	   			
	 
				  
				   $(this).show();
			   
			  
			   
			});
  $("#selCausa option").each(function()
			{	
	   			
			   var arr1= $(this).val().split('-');

			   if(arr1[1]!=2){
				  
				   $(this).hide();
			   }
			  
			   
			});
  

 break;
case "1":
	

	  $("#selCausa option").each(function()
				{	
				  
					   $(this).show();
		 		   
				});
	  $("#selCausa option").each(function()
				{	
		   			
				   var arr1= $(this).val().split('-');
				   
				   if(arr1[1]!=1){
					  
					   $(this).hide();
				   }
				  
				   
				});
 
  break;
 

 }


}

function nuevoACInseguros() {
	if (!banderaEdicion) {
		var selActTipo = crearSelectOneMenu("selActTipo", "verCategAcordeTipoActoInseguro()", listActInsegTipo,
				"-1", "actTipoId", "actTipoName");

		var selCausa = crearSelectOneMenuY("selCausa", "", listCausaBasica, "-1",
				"causaInmId","causaInmTipoEvent", "causaInmNombre");
		
		var selArea = crearSelectOneMenu("selArea", "", listArea, "-1",
				"areaId", "areaName");
		var selNivel = crearSelectOneMenu("selNivel", "", listNivel, -1,
				"nivelRiesgoId", "nivelRiesgoNombre");
		var selOrigen=crearSelectOneMenu("selOrigen", "", listOrigenReporte, -1,
				"id", "nombre");
		$("#tblInseg tbody")
				.append(
						"<tr id='0'>"
								+ "<td>"
								+ selActTipo
								+ "</td>"
								+"<td>"
								+selOrigen
								+"</td>"
								+ "<td>"
								+ selCausa
								+ "</td>"
								+ "<td>"
								+ selNivel
								+ "</td>"
								+ "<td><input type='text' id='inputNom' class='form-control' placeholder='Descripcion'></td>"
								+ "<td><input type='date' id='inputFechaRep' class='form-control' ></td>"
								+ "<td>" + selArea + "</td>" + "<td>...</td>"
								+ "<td>...</td>" + "<td>...</td>" + "</tr>");
		acInseguroId = 0;
		habilitarBotonesNuevo();
		banderaEdicion = true;
		llevarTablaFondo("wrapper");
	} else {
		alert("Guarde primero.");
		
	}
}

function cancelarACInseguros() {
	cargarPrimerEstado();
}

function eliminarACInseguros() {
	var r = confirm("¿Está seguro de eliminar el acto inseguro?");
	if (r == true) {
		var dataParam = {
			acInseguroId : acInseguroId
		};

		callAjaxPost(URL + '/acinseguro/delete', dataParam,
				procesarResultadoEliminarAC);
	}
}

function procesarResultadoEliminarAC(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		cargarPrimerEstado();
		break;
	default:
		alert("Ocurrió un error al eliminar el acto inseguro!");
	}
}

function guardarACInseguros() {

	var campoVacio = true;var trabajadorId;
	var nombreTrab=$("#inputNomRep").val();
	if($("#selTipoRep").val()==1){
		for (var index = 0; index < listDniNombre.length; index++) {
			
			if(listDniNombre[index]["nombre"]==nombreTrab){
			
			trabajadorId=listDniNombre[index]["trabajadoresId"];
			break;
			}else{
				trabajadorId=null;
			
			}
		}
	}
	
	
	
	var origenId=$("#selOrigen").val();
	var inputNom = $("#inputNom").val();
	var selActTipo = $("#selActTipo option:selected").val();
	var acInseguroCausa = $("#selCausa option:selected").val();
	var nivelId=$("#selNivel option:selected").val();
	var acAux=acInseguroCausa.split('-');
	var selArea = $("#selArea option:selected").val();

	var inRepLugar = acInseguroId > 0 ? $("#inputLug").val() : '';
	var inRepAcc = acInseguroId > 0 ? $("#inputAccRep").val() : '';
	var inRepTipo = acInseguroId > 0 ? $("#selTipoRep option:selected").val()
			: '';
	var inRepNombre = acInseguroId > 0 ? $("#inputNomRep").val() : '';
	
	var mes2 = $("#inputFechaRep").val().substring(5, 7) - 1;
	var fechatemp2 = new Date($("#inputFechaRep").val().substring(0, 4), mes2,
			$("#inputFechaRep").val().substring(8, 10));

	var inputFecha = fechatemp2;
	if (acInseguroId > 0) {
		

		var seleccionados = $('#jstree').jstree(true).get_checked();
		listTrabsSelecccionados = [];
		for (index = 0; index < seleccionados.length; index++) {
			var trab = seleccionados[index];
			if (seleccionados[index].substr(0, 4) == "nodo") {
				listTrabsSelecccionados.push(seleccionados[index].substr(4,
						seleccionados[index].length));
			}
		}
	}else{ 
if (parseInt(sessionStorage.getItem("accesoUsuarios")) == 1) {	 
			
		}else {
			inRepTipo=1;
			trabajadorId=parseInt(sessionStorage.getItem("trabajadorGosstId"));
			 
		}
	}
	
	if(!$("#inputFechaRep").val()){
		inputFecha=null;
	}

	if (selActTipo == '-1' || inputNom.length == 0 || selArea == '-1' || nivelId=='-1') {
		campoVacio = false;
	}

	if (campoVacio) { 
		var dataParam = {
			acInseguroId : acInseguroId,origen:{id:(origenId==-1?null:origenId)},
			areaId : selArea,nivelRiesgoId:nivelId,
			acInseguroTipoId : selActTipo,
			acInseguroCausaId: acAux[0],
			acInseguroNombre : inputNom,
			reporteLugar : inRepLugar,
			reporteAccion: inRepAcc,
			reporteTipo : inRepTipo,
			reporteNombre : inRepNombre,
			reporteFecha : inputFecha,trabajadorId:trabajadorId,
			trabajadores : listTrabsSelecccionados,
			matrixId : getUrlParameter("mdfId")
		};
		console.log(dataParam);
		callAjaxPost(URL + '/acinseguro/save', dataParam,
				procesarResultadoGuardarACInseguro);
	} else {
		alert("Debe ingresar todos los campos.");
	}
}

function procesarResultadoGuardarACInseguro(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		cargarPrimerEstado();
		break;
	default:
		alert("Ocurrió un error al guardar el acto inseguro!");
	}
}

function editarDatosReporte() {
	$('#mdReporte').modal('show');
}

function mostrarCargarImagen(acInseguroId) {
	$('#mdUpload').modal('show');
	$('#btnUploadEvidencia').attr("onclick",
			"javascript:uploadEvidencia(" + acInseguroId + ");");
}

function uploadEvidencia(acInseguroId,tipoImagenId) {
	var inputFileImage = document.getElementById("fileEvi");
	var file = inputFileImage.files[0];
	var data = new FormData();
console.log(file)
	if (file.size > bitsEvidenciaActoInseguro) {
		alert("Lo sentimos, solo se pueden subir archivos menores a "+bitsEvidenciaActoInseguro/1000000+" MB");
	} else {
		data.append("fileEvi", file);
		data.append("acInseguroId", acInseguroId);
	
		var url = URL + '/acinseguro/evidencia/save';
		$.blockUI({
			message : 'cargando...'
		});
		$
				.ajax({
					url : url,
					xhrFields: {
			            withCredentials: true
			        },
					type : 'POST',
					contentType : false,
					data : data,
					processData : false,
					cache : false,
					success : function(data, textStatus, jqXHR) {
						$.unblockUI();
						switch (data.CODE_RESPONSE) {
						case "06":
							sessionStorage.clear();
							document.location.replace(data.PATH);
							break;
						default:
							console.log('Se subio el archivo correctamente.');
							$('#mdVistaPrevia').modal('hide');
							$("#tdevi" + acInseguroId).html( 
									"<i class='fa fa-picture-o' aria-hidden='true'></i>"+file.name)
						}

					},
					error : function(jqXHR, textStatus, errorThrown) {
						$.unblockUI();
						alert("Ups, tenemos problemas tecnicos. Intente más tarde. Error: "
								+ errorThrown);
						console.log('xhRequest: ' + jqXHR + "\n");
						console.log('ErrorText: ' + textStatus + "\n");
						console.log('thrownError: ' + errorThrown + "\n");
					}
				});
	}
}

/** *********************JsTree************** */
function seleccionarTrabajadores() {
	$('#mdTrabs').modal('show');
}

function cargarModalTrabs() {
	if (listTrabsSelecccionados.length === 0) {

		var dataParam = {
				acInseguroId : acInseguroId,
			matrixId : getUrlParameter("mdfId")
		};

		callAjaxPost(
				URL + '/acinseguro/trabajadores/jstree',
				dataParam,
				function(data) {
					switch (data.CODE_RESPONSE) {
					case "05":
						$('#jstree').jstree("destroy");

						listTrabs = data.list;

						$('#jstree').jstree({
							"plugins" : [ "checkbox", "json_data","search","wholerow" ],
							'checkbox' : {
								"keep_selected_style" : false,
								"tie_selection" : false
							},
							'core' : {
								'data' : listTrabs
							}
						});
//Inicio Para Buscador en jstree
var to = false;
$('#buscarTrabJstree').keyup(function () {
  if(to) { clearTimeout(to); }
  to = setTimeout(function () {
    var v = $('#buscarTrabJstree').val();
$("#jstree").jstree(true).search(v);
  
  }, 250);
});
$.jstree.defaults.search.show_only_matches=true;

$('#listTrabBuscar').keyup(function () {
	    if(to) { clearTimeout(to); }
	    to = setTimeout(function () {
	
	    	busquedaMasivaJstree(listTrabs,"jstree","resultSearchTrab","listTrabBuscar",1)
	    }, 250);
	  });
//Fin Para Buscador en jstree
						for (index = 0; index < listTrabs.length; index++) {
							if (listTrabs[index].state.checked
									&& listTrabs[index].id.substr(0, 4) == "nodo") {
								listTrabsSelecccionados
										.push(listTrabs[index].id.substr(4,
												listTrabs[index].id.length));
							}
						}

						break;
					default:
						alert("Ocurrió un error al cargar el arbol de trabajadores.!");
					}
				});
	}
}

function generarReporteActo(){
	if(tienePermisoReporte){
		window.open(URL
				+ "/acinseguro/informe?actoInseguroId="
				+ acInseguroId 
				+ '');	
	}else{
		alert("Debe registrar una evidencia ")
		
	}

}
function importarDatos() {
	$("#btnGuardarNuevos").attr("onclick",
			"javascript:guardarMasivoCopiaExcel();");

	$('#mdImpDatos').modal('show');
	var selArea = crearSelectOneMenu("selAreaImp", "", listArea, "-1",
			"areaId", "areaName");
	$("#divSelArea").html(selArea);
}

function guardarMasivoCopiaExcel() {
	var texto = $("#txtListado").val();
	var listExcel = texto.split('\n');
	var listRecurso = [];
	var listActRepe = [];
	var listFullNombres = "";
	var validado = "";
	
	if (texto.length < 10000) {
		for (var int = 0; int < listExcel.length; int++) {

			var listCells = listExcel[int].split('\t');
			var filaTexto = listExcel[int];
			if (filaTexto.trim().length < 1) {
				validado = "Existen filas vacias.";
				break;
			}

			if (validado.length < 1 && listCells.length != 3) {
				validado = "No coincide el numero de celdas. ("
						+ listCells.length+"!=3 )";
				break;
			} else {
				var recurs = {};
				var tipo = {};
				for (var j = 0; j < listCells.length; j++) {
					var element = listCells[j];
					if (j === 1) {
						recurs.acInseguroNombre = element.trim();
						if ($.inArray(recurs.acInseguroNombre, listActRepe) === -1) {
							listActRepe.push(recurs.acInseguroNombre);
						} else {
							listFullNombres = listFullNombres + recurs.acInseguroNombre + ", ";
						}

					}
					
					if (j === 0) {
						for (var k = 0; k < listActInsegTipo.length; k++) {
							if (listActInsegTipo[k].actTipoName == element.trim()) {
								recurs.acInseguroTipoId = listActInsegTipo[k].actTipoId;
							}
						}

						if (!recurs.acInseguroTipoId) {
							validado = "Tipo de A/c Sub estándar no reconocido.";
							break;
						}
					}
					
					if (j === 2) {
						try {
							var fechaTexto = element.trim();
							var parts = fechaTexto.split('/');
							var dateIngreso = new Date(parts[2], parts[1] - 1,
									parts[0]);
							recurs.reporteFecha = dateIngreso;
						} catch (err) {
							validado = "Error en el formato de fecha de reporte.";
							break;
						}

					}
					recurs.areaId =$("#selAreaImp").val();
					recurs.acInseguroCausaId=34;
				if(recurs.areaId=="-1"){
					validado="seleccione un área a importar"
				}
				}

				listRecurso.push(recurs);
				if (listActRepe.length < listRecurso.length) {
					validado = "Existen A/C repetidos repetidos." + listFullNombres;
					break;
				}
			}
		}
	} else {
		validado = "Ha excedido los 10000 caracteres permitidos.";
	}

	if (validado.length < 1) {
		var r = confirm("¿Está seguro que desea subir estos "
				+ listExcel.length + " elemento(s)?");
		if (r == true) {
			var dataParam = {
				listRecurso : listRecurso
			};
			callAjaxPost(URL + '/acinseguro/masivo/save', dataParam,
					function (data) {
				switch (data.CODE_RESPONSE) {
				case "05":
					
						alert("Se guardaron exitosamente.");
						cargarPrimerEstado();
						$('#mdImpDatos').modal('hide');
					
					break;
				default:
					alert("Ocurri&oacute; un error al guardar los exams!");
				}
			});
		}
	} else {
		alert(validado);
	}
}