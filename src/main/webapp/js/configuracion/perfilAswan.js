/**
 * 
 */
var banderaEdicionPerfil;
var listFullPerfiles;
var perfilId;
function llamarModulos(){
	callAjaxPost(URL + '/perfil/modulos/aswan', null,
			procesarResultadoModulosASWAN);
}

function procesarResultadoModulosASWAN(data){
	switch (data.CODE_RESPONSE) {
	case "05":
		var list = data.modulos;
	
		$("#tableModulos tbody tr").remove();

		for (var index = 0; index < list.length; index++) {

			$("#tableModulos tbody").append(
					
			"<tr><td>"+list[index].moduloNombre+"</td><td>" 
			+"<input class='form-control' type='checkbox' id='checkModulo"+list[index].moduloId+"' " 
			+"onchange='marcarModuloPerfil("+list[index].moduloId+")'>"+
			"</td>" +
			"<td>" +"<input class='form-control' type='checkbox' id='checkModEditar"+list[index].moduloId+"' " 
			+">"+
			"</td>" +
			"<td>" +"<input class='form-control' type='checkbox' id='checkModEliminar"+list[index].moduloId+"' " 
			+">"+
			"</td>      </tr>"
			)
		}
		formatoCeldaSombreableTabla(true,"subUsuariosTable");
		
		break;
	default:
		alert("Ocurrió un error al traer los subusuarios!");
	}
	
}
function llamarPefilesAswan(){
	perfilId=0;
	banderaEdicionPerfil=false;
	$("#btnVolverPerfil").hide();
	$("#btnGuardarPerfil").hide();
	$("#btnNuevoPerfil").show();
	
	$("#btnNuevoPerfil").attr("onclick","nuevoPerfil()")
	$("#btnVolverPerfil").attr("onclick","llamarPefilesAswan()");
	$("#btnGuardarPerfil").attr("onclick","guardarPerfil()");
	

	$("#modulosPefil").hide();
	callAjaxPost(URL + '/perfil/aswan', "",
			procesarLlamadaPerfiles);
}
function procesarLlamadaPerfiles(data){
	switch (data.CODE_RESPONSE) {
	case "05":
		var list = data.list;
	listFullPerfiles=list;
	console.log(listFullPerfiles);
		$("#perfilesTable tbody tr").remove();

		for (index = 0; index < list.length; index++) {

			$("#perfilesTable tbody:first").append(

					"<tr id='tr" + list[index].perfilId
							+ "' onclick='javascript:editarPerfil("
							+ list[index].perfilId +","+index
							+ ")' >"

							+ "<td id='tdnombre" + list[index].perfilId
							+ "'>" + list[index].perfilNombre + "</td>"
							
							+ "<td id='tdmods" + list[index].perfilId
							+ "'>" + list[index].nombreModulosFull + " </td>"
							+ "</tr>");
		}
		formatoCeldaSombreable(true);
		
		break;
	default:
		alert("Ocurrió un error al traer las actos inseguros!");
	}

}

function nuevoPerfil(){
	if(!banderaEdicionPerfil){
		
			$("#btnVolverPerfil").show();
			$("#btnGuardarPerfil").show();
			$("#btnNuevoPerfil").hide();
			banderaEdicionPerfil=true;
			formatoCeldaSombreable(false);

		$("#perfilesTable tbody").append(
			"<tr id='tr0'>"
				+"<td><input type='text' class='form-control' id='nombrePerfilInput'>" +
						"</td>"
				+"<td>..." +
				"</td>"
				+"</tr>"	
		);
			
		
	
	}
}
function editarPerfil(pperfilId,pindex){
	if(!banderaEdicionPerfil){
		$("#btnVolverPerfil").show();
		$("#btnGuardarPerfil").show();
		$("#btnNuevoPerfil").hide();
		banderaEdicionPerfil=true;
		formatoCeldaSombreable(false);
			perfilId=pperfilId;
		var perfilNombre=listFullPerfiles[pindex].perfilNombre
		
		
		$("#tdnombre"+pperfilId).html(
		"<input class='form-control' id='nombrePerfilInput'>"		
		);
		nombrarInput("nombrePerfilInput",perfilNombre);
	var numModulos=	$("#tdmods"+pperfilId).text();
		
	$("#tdmods"+pperfilId).html(
			"<a onclick='verModulosPerfil("+pperfilId+")'>Seleccionar "+numModulos+"</a>"	
		);
	
$("#modulosPefil input[type='checkbox']").each(function(){
		$(this).prop("checked",false);
	})
	var dataParam={
			perfilId:perfilId
	}
	callAjaxPost(URL + '/perfil/modulos', dataParam,
			procesarResultadoModulosPerfil);
		
	}
}
function verModulosPerfil(perfilId){
	$("#modulosPefil").show();
	
}

function procesarResultadoModulosPerfil(data){
	switch (data.CODE_RESPONSE) {
	case "05":
		var listModulos=data.list;
		if(listModulos.length>0){
		for(var i=0;i<listModulos.length;i++){
			if(listModulos[i]){
			$("#checkModulo"+listModulos[i].moduloId).prop("checked",true);
			var editChecked=pasarNumberBool(listModulos[i].canEdit);
			$("#checkModEditar"+listModulos[i].moduloId).prop("checked",editChecked);
			var deleteChecked=pasarNumberBool(listModulos[i].canDelete);
			$("#checkModEliminar"+listModulos[i].moduloId).prop("checked",deleteChecked)
		}}
		}
		break;
	default:
		alert("Ocurrió un error al guardar el acto inseguro!");
	}
	
}


function marcarModuloPerfil(id){


	var accesoMarcado=$("#checkModulo"+id).prop("checked");
	
	if(accesoMarcado){
		$("#checkModEditar"+id).prop("checked",true);
		$("#checkModEliminar"+id).prop("checked",true);
		$("#checkModEditar"+id).prop("disabled",false);
		$("#checkModEliminar"+id).prop("disabled",false);
	}else{
		$("#checkModEditar"+id).prop("checked",false);
		$("#checkModEliminar"+id).prop("checked",false);
		$("#checkModEditar"+id).prop("disabled",true);
		$("#checkModEliminar"+id).prop("disabled",true);
	}
	
	
	switch(id){
	case 4:
		var opcionRecursosTotales=$("#checkModulo4").prop("checked");
		if(opcionRecursosTotales){
			$("#checkModulo5").prop("checked",true);
			$("#checkModulo6").prop("checked",true);
			$("#checkModulo7").prop("checked",true);
			$("#checkModulo8").prop("checked",true);
			$("#checkModulo21").prop("checked",true);

			$("#checkModEditar5").prop("disabled",false);
			$("#checkModEditar6").prop("disabled",false);
			$("#checkModEditar7").prop("disabled",false);
			$("#checkModEditar8").prop("disabled",false);
			$("#checkModEditar21").prop("disabled",false);
			
			$("#checkModEliminar5").prop("disabled",false);
			$("#checkModEliminar6").prop("disabled",false);
			$("#checkModEliminar7").prop("disabled",false);
			$("#checkModEliminar8").prop("disabled",false);
			$("#checkModEliminar21").prop("disabled",false);
			}else{
			$("#checkModulo5").prop("checked",false);
			$("#checkModulo6").prop("checked",false);
			$("#checkModulo7").prop("checked",false);
			$("#checkModulo8").prop("checked",false);
			$("#checkModulo21").prop("checked",false);
			
			$("#checkModEditar5").prop("disabled",true);
			$("#checkModEditar6").prop("disabled",true);
			$("#checkModEditar7").prop("disabled",true);
			$("#checkModEditar8").prop("disabled",true);
			$("#checkModEditar21").prop("disabled",true);
			
			$("#checkModEliminar5").prop("disabled",true);
			$("#checkModEliminar6").prop("disabled",true);
			$("#checkModEliminar7").prop("disabled",true);
			$("#checkModEliminar8").prop("disabled",true);
			$("#checkModEliminar21").prop("disabled",true);
			
			
			$("#checkModEditar5").prop("checked",false);
			$("#checkModEditar6").prop("checked",false);
			$("#checkModEditar7").prop("checked",false);
			$("#checkModEditar8").prop("checked",false);
			$("#checkModEditar21").prop("checked",false);
				
			$("#checkModEliminar5").prop("checked",false);
			$("#checkModEliminar6").prop("checked",false);
			$("#checkModEliminar7").prop("checked",false);
			$("#checkModEliminar8").prop("checked",false);
			$("#checkModEliminar21").prop("checked",false);
		}
		break;
	}
	
	var opcionRecurso1=$("#checkModulo5").prop("checked");
	var opcionRecurso2=$("#checkModulo6").prop("checked");
	var opcionRecurso3=$("#checkModulo7").prop("checked");
	var opcionRecurso4=$("#checkModulo8").prop("checked");
	var opcionRecurso5=$("#checkModulo21").prop("checked");
	if(opcionRecurso1 || opcionRecurso2 || opcionRecurso3 || opcionRecurso4 || opcionRecurso5){
		$("#checkModulo4").prop("checked",true);
	}
	
	if(!opcionRecurso1 && !opcionRecurso2 && !opcionRecurso3 && !opcionRecurso4 && !opcionRecurso5){
		$("#checkModulo4").prop("checked",false);
	}
	
}


function guardarPerfil(){
	
	var modulosId=[];
	var perfilNombre=$("#nombrePerfilInput").val();
	if(perfilId>0){
	$("#modulosPefil input[id^='checkModulo']").each(function(){
		
		if($(this).prop('checked')){
			var idcheck=$(this).prop("id");
			var moduloId=parseInt(idcheck.substr(11,14));
			var edit_checked=$("#checkModEditar"+moduloId).prop("checked");
			var eliminate_checked=$("#checkModEliminar"+moduloId).prop("checked");
			var menu_perfil={
					moduloId:moduloId,
					estadoModulo:1,
					canEdit:pasarBoolInt(edit_checked),
					canDelete:pasarBoolInt(eliminate_checked)
			};
			modulosId.push(menu_perfil);
			
		
		}
	})
	

	
	}else{
		modulosId=null;
		
	}
	
	 
	var dataParam={
			perfilId:perfilId,
			perfilNombre:perfilNombre,
			modulosId:modulosId
			
	};
	callAjaxPost(URL + '/perfil/save/aswan', dataParam,
			procesarResultadoGuardarPerfil);
}

function procesarResultadoGuardarPerfil(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		llamarPefilesAswan();
		break;
	default:
		alert("Ocurrió un error al guardar el acto inseguro!");
	}
}

function getPerfilesAswan(tipoId){
	if(tipoId==1){
		return listFullPerfiles.filter(function(val,index){
			if(val.perfilId==2 || val.perfilId==3){
				return false;
			}else{
				return true;
			}
		});
	}else{
		return listFullPerfiles.filter(function(val,index){
			if(val.perfilId==2 || val.perfilId==3){
				return true;
			}else{
				return false;
			}
		});
	}
	
	
}