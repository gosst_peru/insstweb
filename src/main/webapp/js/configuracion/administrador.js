var banderaEdicion;
var banderaEdicionUsuario;
var clienteId;
var usuarioId;
var listLicencia;
var listFullUsuarios;
var listFullIntertek;
var perfilId;
var usuariosMaximo;
var licenciaId;
var listAcceso=[
	   	         
                {
                	accesoId:1,
                	accesoNombre:"Acceso a trabajadores"
                	
                },
                {
                	accesoId:0,
                	accesoNombre:"Sin Acceso a otros trabajadores"
                	
                }
                
                ];
$(document).ready(function() {
	insertMenu();	
	$("#btnGuardar").attr("onclick","saveCliente()")
	$("#btnNuevo").attr("onclick","nuevoCliente()");
	$("#btnVolver").attr("onclick","llamarClientes()");
	
	$("thead td").addClass("tb-acc");
	
	
	$("#btnGuardarUsuario").attr("onclick","saveUsuario()")
	$("#btnNuevoUsuario").attr("onclick","nuevoUsuario()");
	$("#btnVolverUsuario").attr("onclick","volverUsuarios()");
	$('#inputClave').keyup(function(e){
	    if(e.keyCode == 13)
	    {
	        $(this).trigger("enterKey");
	    }
	}); 
	$('#inputClave').bind("enterKey",function(e){
		validarAdministrador();
		});
});

function validarAdministrador(){
	var clave=$("#inputClave").val(); 
	callAjaxPost(URL + '/administrador/clave', parseInt(clave),
			procesarValidacionAdministracion);
	
	llamarPefilesAswan();
	llamarModulos();
	
}

function procesarValidacionAdministracion(data){
	
	switch (data.CODE_RESPONSE) {
	case "05":
		llamarClientes();
		$("#inputClave").remove();
		break;
	default:
		alert("NOPE");
	}
	
}
function llamarClientes(){
	$("#btnVolver").hide();
	$("#btnNuevo").show();
	$("#btnGuardar").hide();
	
	$("#btnGuardarUsuario").hide();
	$("#btnNuevoUsuario").hide();
	$("#btnVolverUsuario").hide();
	
$("#subUsuariosCliente").hide();
	var dataParam = {
			
		};

		callAjaxPost(URL + '/administrador/clientes', dataParam,
				procesarLlamadaClientes);
}
var listFullCliente;
function procesarLlamadaClientes(data){
	
	switch (data.CODE_RESPONSE) {
	case "05":
		var list = data.list;
		var mensajes=data.mensajes;
		
		for (index = 0; index < mensajes.length; index++) {

			$("#mensajeTable tbody").append(

					"<tr id='tr" + mensajes[index].id
							+ "'  >"

							+ "<td id='tdnom" + mensajes[index].id
							+ "'>" + mensajes[index].tipoMensaje.nombre + "</td>"
							+ "<td id='tdruc" + mensajes[index].id
							+ "'>" + mensajes[index].fechaRegistrada
							+ "</td>" 
							+ "<td id='tdlicencia" + mensajes[index].id
							+ "'>" + mensajes[index].contenido
							+ "</td>"
							
							+"</tr>");
		}
		
		
		
		listFullCliente=list;
		listLicencia=data.listLicencia;
		banderaEdicion=false;
		$("#clienteTable tbody tr").remove();

		for (index = 0; index < list.length; index++) {

			$("#clienteTable tbody").append(

					"<tr id='tr" + list[index].clienteId
							+ "' onclick='javascript:editarCliente("
							+ list[index].clienteId + ","
							+ index + ")' >"

							+ "<td id='tdnom" + list[index].clienteId
							+ "'>" + list[index].clienteNombre + "</td>"
							+ "<td id='tdruc" + list[index].clienteId
							+ "'>" + list[index].clienteRuc
							+ "</td>" 
							+ "<td id='tdlicencia" + list[index].clienteId
							+ "'>" + list[index].licenciaNombre
							+ "</td>"
							+ "<td id='tdusu" + list[index].clienteId
							+ "'>" + list[index].usuariosActuales+"/"+ list[index].usuariosMaximo + "</td>"
							
							+ "<td id='tdemp"
							+ list[index].clienteId + "'>"
							+ list[index].empresasActuales+"/"+ list[index].empresasMaximo + "</td>"
							
							+ "<td id='tduni" + list[index].clienteId
							+ "'>" + list[index].unidadesActuales+"/"+ list[index].unidadesMaximo + "</td>"
							+ "<td id='tdgb" + list[index].clienteId
							+ "'>" + list[index].espacioActual+"/"+ list[index].espacioMaximo + "</td>"
							+ "<td id='tdhor" + list[index].clienteId
							+ "'>" + list[index].fechaInicioTexto
							+ "</td>"
							+ "<td id='tdfecv" + list[index].clienteId
							+ "'>" + list[index].fechaVencimientoTexto
							+ "</td>"
							+"</tr>");
		}
		formatoCeldaSombreableTabla(true,"clienteTable");
		
		
		break;
	default:
		alert("Ocurrió un error al traer las clientes!");
	}
	
	
}

function editarCliente(pclienteId,pindex){
	if(banderaEdicion==false){
		formatoCeldaSombreableTabla(false,"clienteTable");
		var fechaExpiracion=listFullCliente[pindex].fechaVencimiento;
		usuariosMaximo=listFullCliente[pindex].usuariosMaximo;
		licenciaId=listFullCliente[pindex].licenciaId;
		$("#btnVolver").show();
	$("#btnNuevo").hide();
		$("#btnGuardar").show();
		clienteId=pclienteId;
	
		var selTipoLicencia = crearSelectOneMenu("selTipoLicencia", "", listLicencia,
				licenciaId, "planType", "planOwner");
		$("#tdnom"+clienteId).html(
				"<input type='text' class='form-control' id='nombreClienteInput'>"
		);
		nombrarInput("nombreClienteInput",listFullCliente[pindex].clienteNombre);
		$("#tdruc"+clienteId).html(
				"<input type='text' class='form-control' id='rucClienteInput'>"
		)
			nombrarInput("rucClienteInput",listFullCliente[pindex].clienteRuc);
		$("#tdlicencia"+clienteId).html(
				selTipoLicencia
		)
		
	$("#tduni"+clienteId).html(
			listFullCliente[pindex].unidadesActuales		+"/ <input id='unidadesInput' type='number'  class='form-control'>  "
	);
		nombrarInput("unidadesInput",listFullCliente[pindex].unidadesMaximo);
	$("#tdemp"+clienteId).html(
			listFullCliente[pindex].empresasActuales	+"/ <input id='empresasInput' type='number' class='form-control'>  "	
	);
	nombrarInput("empresasInput",listFullCliente[pindex].empresasMaximo);
	$("#tdusu"+clienteId).html(
			"<a onclick='verUsuariosCliente("+clienteId+")'>"+	listFullCliente[pindex].usuariosActuales+"</a>"	
			+"/ <input  class='form-control' id='usuariosInput' type='number'> "	
	);
	nombrarInput("usuariosInput",listFullCliente[pindex].usuariosMaximo);
	$("#tdgb"+clienteId).html(
			listFullCliente[pindex].espacioActual	+"/ <input id='espacioInput' type='number'  class='form-control'>  "
	);
	nombrarInput("espacioInput",listFullCliente[pindex].espacioMaximo);
		
		
	
	
	$("#tdfecv"+clienteId).html(
		"<input type='date' class='form-control' id='fechaExpiracion'>"	
	);
		nombrarInput("fechaExpiracion",convertirFechaInput(fechaExpiracion) )
			
		
		banderaEdicion=true;	
		$("#tr"+clienteId +" td").css("background-color","#2EFEC8");
	}
}

function verUsuariosCliente(clienteId){
	$("#btnGuardarUsuario").hide();
	$("#btnNuevoUsuario").show();
	$("#btnVolverUsuario").hide();
	
$("#subUsuariosCliente").show();
	var dataparam={
			
			clienteId:clienteId,
	};
	callAjaxPost(URL + '/administrador/cliente/usuarios', dataparam,
			procesarLlamadaUsuariosClientes);
	
	
	
}


function procesarLlamadaUsuariosClientes(data){
	
	switch (data.CODE_RESPONSE) {
	case "05":
		var list = data.list;
		banderaEdicionUsuario=false;
		listFullUsuarios=list;
		listFullIntertek=data.listFullIntertek;
		$("#usuariosTable tbody tr").remove();

		for (index = 0; index < list.length; index++) {

			$("#usuariosTable tbody").append(

					"<tr id='tr" + list[index].userId
							+ "' onclick='javascript:editarUsuarioCliente(" 
							+ index + ")' >"

							+ "<td id='tdnomusu" + list[index].userId
							+ "'>" + list[index].userName + "</td>"
							+ "<td id='tdpass" + list[index].userId
							+ "'>" + list[index].password+"</td>"
							+ "<td id='tdperfil" + list[index].userId
							+ "'>" + list[index].perfilDescripcion + "</td>"
							+ "<td id='tdacceso" + list[index].userId
							+ "'>" +( list[index].accesoUsuarios==0?"No Accede":" Accede") + "</td>"
							+ "<td id='tdperfilin" + list[index].userId
							+ "'>" + list[index].perfilIntertek.nombre + "</td>"
								
							+"</tr>");
		}
		formatoCeldaSombreableTabla(true,"usuariosTable");
		
		
		break;
	default:
		alert("Ocurrió un error al traer las clientes!");
	}
	
	
}

function cambiarAcordeLicencia(){
	var licenciaId=$("#selTipoLicencia").val();
	
	if(licenciaId=="1"){
		$("#usuariosInput").val(1);$("#usuariosInput").prop("disabled",true);
	$("#empresasInput").val(1);$("#empresasInput").prop("disabled",true);
		$("#unidadesInput").val(5);$("#unidadesInput").prop("disabled",true);
		$("#espacioInput").val(20);$("#espacioInput").prop("disabled",true);
		
	}else{
		$("#usuariosInput").val(0);$("#usuariosInput").prop("disabled",false);
		$("#empresasInput").val(0);$("#empresasInput").prop("disabled",false);
			$("#unidadesInput").val(0);$("#unidadesInput").prop("disabled",false);
			$("#espacioInput").val(0);$("#espacioInput").prop("disabled",false);
		
	}
}
function nuevoCliente(){
	if (!banderaEdicion) {
	clienteId=0;$("#btnGuardar").show();$("#btnVolver").show();$("#btnNuevo").hide();
	var selTipoLicencia = crearSelectOneMenu("selTipoLicencia", "cambiarAcordeLicencia()", listLicencia,
			0, "planType", "planOwner");
	irFondoDiv("wrapper");
	$("#clienteTable tbody").append(

			"<tr id='tr" + clienteId+"'>"

					+ "<td id='tdnom" + clienteId
					+ "'>" + "<input type='text' class='form-control' id='nombreClienteInput'>"+ "</td>"
					+ "<td id='tdtipo" + clienteId
					+ "'>" + "<input type='text' class='form-control' id='rucClienteInput'>"+"</td>"
					+ "<td id='tdnom" + clienteId
					+ "'>" + selTipoLicencia + "</td>"
					+"<td> <input id='usuariosInput' type='number' class='form-control'></td>"
					+"<td><input id='empresasInput' type='number' class='form-control'> </td>"
						+"<td><input id='unidadesInput' type='number' class='form-control'> </td>"
					
						
						+"<td><input id='espacioInput' type='number ' class='form-control'></td>"
						+"<td>"+"<input type='date' class='form-control' id='fechaInicio' disabled>"	+"</td>"
						+"<td>"+"<input type='date' class='form-control' id='fechaExpiracion'>"	+"</td>"
					+"</tr>");banderaEdicion = true;
	}
	
	formatoCeldaSombreableTabla(false,"clienteTable");
}


function saveCliente(){
	
	var mes2 = $("#fechaExpiracion").val().substring(5, 7) - 1;
	var fechatemp2 = new Date($("#fechaExpiracion").val().substring(0, 4), mes2,
			$("#fechaExpiracion").val().substring(8, 10));

	var inputFecha = fechatemp2;
	
	var mes2 = obtenerFechaActual().substring(5, 7) - 1;
	var fechatemp2 = new Date(obtenerFechaActual().substring(0, 4), mes2,
			obtenerFechaActual().substring(8, 10));

	var inputFechaInicio = fechatemp2;
	var nombreCliente=$("#nombreClienteInput").val();
	var ruc=$("#rucClienteInput").val();
	var licenciaId=$("#selTipoLicencia").val();
	
	var clienteObj={
			clienteId:clienteId,
			clienteNombre:nombreCliente,
				clienteRuc:ruc,
				licenciaId:licenciaId,
				fechaVencimiento:inputFecha,
				fechaInicio:inputFechaInicio,
				usuariosMaximo:$("#usuariosInput").val(),
				empresasMaximo:$("#empresasInput").val(),
				unidadesMaximo:$("#unidadesInput").val(),
				espacioMaximo:$("#espacioInput").val()
					
	}
	
	callAjaxPost(URL + '/administrador/cliente/save', clienteObj,
			procesarGuardarCliente);
	
	
}

function procesarGuardarCliente(data){
	switch (data.CODE_RESPONSE) {
	case "05":
		llamarClientes();
		break;
	default:
		alert("Ocurrió un error al guardar la capacitacion!");
	}
	
}



function nuevoUsuario(){
	if (!banderaEdicionUsuario) {
		if(usuariosMaximo<=listFullUsuarios.length){
			alert("No se puede agregar más usuarios")
		}else{
			$("#btnGuardarUsuario").show();
			$("#btnNuevoUsuario").hide();
			$("#btnVolverUsuario").show();
		usuarioId=0;
	
		
if(licenciaId==1){
	var listPerfil=getPerfilesAswan(2);
	var selPerfil = crearSelectOneMenu("selPerfil", "", listPerfil,
			2, "perfilId", "perfilNombre");
	
	
	
}else{
	var listPerfil=getPerfilesAswan(1);
	var selPerfil = crearSelectOneMenu("selPerfil", "", listPerfil,
			0, "perfilId", "perfilNombre");
}
 
var selAcceso= crearSelectOneMenu("selAcceso", "veridacceso()", listAcceso,
		1, "accesoId", "accesoNombre");
var selInter= crearSelectOneMenuOblig("selInter", "", listFullIntertek,
		1, "id", "nombre");
		$("#btnGuardarUsuario").show();
		$("#btnVolverUsuario").show();
		$("#btnNuevoUsuario").hide();
		
		
		$("#usuariosTable tbody").append(

				"<tr id='tr" + usuarioId+"'>"

						+ "<td id='tdnom" + usuarioId
						+ "'>" + "<input type='text' class='form-control' id='usuarioName'>"+ "</td>"
						+ "<td id='tdtipo" + usuarioId
						+ "'>" + "<input type='text' class='form-control' id='usuarioPass'>"+"</td>"
						+ "<td id='tdnom" + usuarioId
						+ "'>" + selPerfil + "</td>"
						+ "<td id='tdacceso" + usuarioId
						+ "'>" + selAcceso + "</td>"
						+ "<td id='tdperfilin" + usuarioId
						+ "'>" + selInter + "</td>"	
						+"</tr>");
		
		if(licenciaId==1){
			$("#selPerfil").prop("disabled",true)
			}
		
		
		banderaEdicion = true;
						formatoCeldaSombreableTabla(false,"clienteTable");
		}
		
			
		}
		
}
function veridacceso(){
	console.log($("#selAcceso").val());
}
function saveUsuario(){
	
	var nombreUsuario=$("#usuarioName").val();
	var pass=$("#usuarioPass").val();
	var perfilId=$("#selPerfil").val();
	var perfilIntertekId=$("#selInter").val();
	var accesoId=$("#selAcceso").val();
	if(nombreUsuario.length==0 || pass.length==0 || perfilId=="-1"|| accesoId=="-1"){
		alert("llene todos los campos")
		
	}else{
		var usuarioObj={
				userId:usuarioId,
				userName:nombreUsuario,
				password:pass,
				clienteId:clienteId,
				accesoUsuarios:accesoId,
				perfilIntertek:{id:perfilIntertekId},
				idPerfil:perfilId
		};
		console.log(usuarioObj);
		
		callAjaxPost(URL + '/administrador/usuario/exists', usuarioObj,
				procesarExistenciaUsuario);
	}
	
	

}

function procesarExistenciaUsuario(data){
	switch (data.CODE_RESPONSE) {
	case "05":
		var usuarioTaken=data.usuarioTaken;
		if(usuarioTaken){
			alert("El Usuario existe, introduce otro nombre");
		}else{
			var nombreUsuario=$("#usuarioName").val();
			var pass=$("#usuarioPass").val();
			var perfilId=$("#selPerfil").val();
			var accesoId=$("#selAcceso").val();
			var perfilIntertekId=$("#selInter").val();
			var usuarioObj={
					userId:usuarioId,
					userName:nombreUsuario,
					password:pass,
					clienteId:clienteId,
					idPerfil:perfilId,
					perfilIntertek:{id:perfilIntertekId},
					accesoUsuarios:accesoId
			};
			callAjaxPost(URL + '/administrador/usuario/save', usuarioObj,
					procesarGuardarUsuario);
		}
		break;
	default:
		alert("Ocurrió un error al guardar la capacitacion!");
	}
}

function procesarGuardarUsuario(data){
	switch (data.CODE_RESPONSE) {
	case "05":
		verUsuariosCliente(clienteId);
		break;
	default:
		alert("Ocurrió un error al guardar la capacitacion!");
	}
	
}

function volverUsuarios(){
	$("#btnGuardarUsuario").hide();
	$("#btnVolverUsuario").hide();
	$("#btnNuevoUsuario").show();
	verUsuariosCliente(clienteId);
}

function editarUsuarioCliente(pindex){
	if(!banderaEdicionUsuario){
		banderaEdicionUsuario=true;
		usuarioId=listFullUsuarios[pindex].userId; 
		var userName=listFullUsuarios[pindex].userName;
		var password=listFullUsuarios[pindex].password;
		var perfilInter=listFullUsuarios[pindex].perfilIntertek.id;
		if(licenciaId==1){
			var listPerfil=getPerfilesAswan(2);
			var selPerfil = crearSelectOneMenu("selPerfil", "", listPerfil,
					2, "perfilId", "perfilNombre");
			
			
			
		}else{
			var listPerfil=getPerfilesAswan(1);
			var selPerfil = crearSelectOneMenu("selPerfil", "", listPerfil,
					0, "perfilId", "perfilNombre");
		}
	
		var selAcceso= crearSelectOneMenu("selAcceso", "veridacceso()", listAcceso,
				1, "accesoId", "accesoNombre");
				$("#btnGuardarUsuario").show();
				$("#btnVolverUsuario").show();
				$("#btnNuevoUsuario").hide(); 
		$("#tdnomusu"+usuarioId).html("<input type='text' class='form-control' id='usuarioName'>");
			$("#usuarioName").val(userName);
		$("#tdpass"+usuarioId).html("<input type='text' class='form-control' id='usuarioPass'>");
		$("#usuarioPass").val(password);
		$("#tdperfil"+usuarioId).html(selPerfil);
		$("#tdacceso"+usuarioId).html(selAcceso);

		var selInter= crearSelectOneMenuOblig("selInter", "", listFullIntertek,
				perfilInter, "id", "nombre");
		$("#tdperfilin"+usuarioId).html(selInter);
		$("#tdacceso"+usuarioId).html(selAcceso);

	}
}



