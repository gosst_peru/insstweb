var banderaEdicion;
var listFullPerfiles;
var listFullSubUsuarios;
var listFullPerfilesIntertek;
var perfilId;
var subUsuarioId;
//var listAcceso=[];
var listEmpresasSubUsuario=[];
var listHabilitadoUsuario = [];
var listAcceso=[
	   	         
                {
                	accesoId:1,
                	accesoNombre:"Acceso a trabajadores"
                	
                },
                {
                	accesoId:0,
                	accesoNombre:"Sin Acceso a otros trabajadores"
                	
                }
                
                ];
$(document).ready(function() {
	
	llamarPefiles();
	llamarSubUsuarios();
	llamarModulos();
	$("thead th").addClass("tb-acc");
	$("thead td").addClass("tb-acc");
$("#btnNuevoPerfil").attr("onclick","nuevoPerfil()")
	$("#btnVolverPerfil").attr("onclick","llamarPefiles()");
	$("#btnGuardarPerfil").attr("onclick","guardarPerfil()");
	

	$("#btnVolverSubUsuario").attr("onclick","llamarSubUsuarios()");
	$("#btnGuardarSubUsuario").attr("onclick","guardarSubUsuario()");
	
});
function llamarModulos(){
	var dataObj={perfilId:sessionStorage.getItem("perfilGosstId") };
	callAjaxPost(URL + '/perfil/modulos', dataObj,
			procesarResultadoModulosASWAN);
	
}
function procesarResultadoModulosASWAN(data){
	switch (data.CODE_RESPONSE) {
	case "05":
		var list = data.list;
		$("#tableModulos tbody tr").remove();

		for (var index = 0; index < list.length; index++) {

			$("#tableModulos tbody").append(
					
			"<tr><td>"+list[index].moduloNombre+"</td><td>" 
			+"<input type='checkbox' id='checkModulo"+list[index].moduloId+"' " 
			+"onchange='marcarModuloPerfil("+list[index].moduloId+")'>"+
			"</td>" +
			"<td>" +"<input type='checkbox' id='checkModEditar"+list[index].moduloId+"' " 
			+">"+
			"</td>" +
			"<td>" +"<input type='checkbox' id='checkModEliminar"+list[index].moduloId+"' " 
			+">"+
			"</td>      </tr>"
			)
		}
		formatoCeldaSombreableTabla(true,"subUsuariosTable");
		
		break;
	default:
		alert("Ocurrió un error al traer los subusuarios!");
	}
	
}
function llamarSubUsuarios(){
	subUsuarioId=0;banderaEdicion=false;
	$("#btnVolverSubUsuario").hide();
	$("#btnGuardarSubUsuario").hide();

	
	$("#empresasSubUsuario").hide();

	callAjaxPost(URL + '/subusuario', "",
			procesarLlamadaSubUsuarios);
}
function procesarLlamadaSubUsuarios(data){
	switch (data.CODE_RESPONSE) {
	case "05":
		listFullSubUsuarios = data.list;
		listEmpresasSubUsuario=data.empresasTotal;
		listHabilitadoUsuario = data.listHabilitado;
		listFullPerfilesIntertek=data.listFullPerfilesIntertek;
		$("#subUsuariosTable tbody tr").remove();
		listFullSubUsuarios.forEach(function(val,index){
			var textoRestantes = "Faltan "+val.diasRestantesContrasenia+" día(s)";
			var color = (val.diasRestantesContrasenia==0?"red":"");
			var textTotalRestante="<label style='color : "+color+"'>"+textoRestantes+"</label>"
			$("#subUsuariosTable tbody:first").append(

					"<tr id='tr" + val.userId
							+ "' onclick='javascript:editarSubUsuario("
							+   index
							+ ")' >"
							+ "<td id='tdnombre" + val.userId
							+ "'>" + val.userName + "</td>"
							+ "<td id='tdhabil" + val.userId
							+ "'>" + val.habilitado.nombre + "</td>"

							+ "<td id='tddiasres" + val.userId
							+ "'>" + textTotalRestante + "</td>"
							+ "<td id='tdnsubcorreo" + val.userId
							+ "'>" + val.correo + "</td>"
							
							+ "<td id='tdperfil" + val.userId
							+ "'>" + val.perfilDescripcion + "</td>"
							+ "<td id='tdperfilpi" + val.userId
							+ "'>" + val.perfilIntertek.nombre + "</td>"
							+ "<td id='tdacceso" + val.userId
							+ "'>" + ( val.accesoUsuarios==0?"No Accede":" Accede") + "</td>"
							+ "<td id='tdemp" + val.userId
							+ "'>(" + val.numEmpresas+ ") "+val.empresasNombre+" </td>"
							+ "</tr>");
		});
 
		formatoCeldaSombreableTabla(true,"subUsuariosTable");
		
		break;
	default:
		alert("Ocurrió un error al traer los subusuarios!");
	}

}

function editarSubUsuario(pindex){
	if(!banderaEdicion){
		$("#btnVolverSubUsuario").show();
		$("#btnGuardarSubUsuario").show();

		banderaEdicion=true;
		formatoCeldaSombreable(false);
		var subUsuarioObj = listFullSubUsuarios[pindex];
		subUsuarioId=subUsuarioObj.userId;
		var usuarioNombre=subUsuarioObj.userName;
		var idPerfilSubUsuario=subUsuarioObj.idPerfil;
		var perfilIntertek=subUsuarioObj.perfilIntertek.id;
		//
		var empresasId=subUsuarioObj.empresasId.split(",")
		listEmpresasSubUsuario.forEach(function(val){
			val.selected=0;
			empresasId.forEach(function(val1){
				if(parseInt(val1)==val.idCompany){
					val.selected=1;
				}
			})
		});	
		crearSelectOneMenuObligMultipleCompleto("slcAccesoEmpresasSubUsu", "",
				listEmpresasSubUsuario,  "idCompany", "name","#tdemp"+subUsuarioId,"Acceso...");
		//
		$("#tdnsubcorreo"+subUsuarioId).html(
		"<input class='form-control' id='inputCorreoSubUsuario' type='text'>"		
		);
		$("#inputCorreoSubUsuario").val(subUsuarioObj.correo);
		//
		$("#tdnombre"+subUsuarioId).html(
				"<input class='form-control' id='nombreSubUsuarioInput' type='text'>"		
				);
		nombrarInput("nombreSubUsuarioInput",usuarioNombre);
		var selTipoPerfil = crearSelectOneMenuOblig("selTipoPerfil", "", listFullPerfiles,
				idPerfilSubUsuario, "perfilId", "perfilNombre");
		var selTipoPerfilPi = crearSelectOneMenuOblig("selTipoPerfilPi", "", listFullPerfilesIntertek,
				perfilIntertek, "id", "nombre");
		var selAcceso= crearSelectOneMenu("selAcceso", "veridacceso()", listAcceso,
				listFullSubUsuarios[pindex].accesoUsuarios, "accesoId", "accesoNombre");
		var selHabilitadoUsu = crearSelectOneMenuOblig("selHabilitadoUsu", "", listHabilitadoUsuario,
				subUsuarioObj.habilitado.id, "id", "nombre");
		$("#tdperfil" + subUsuarioId).html(selTipoPerfil);
		$("#tdacceso" + subUsuarioId).html(selAcceso);
		$("#tdperfilpi"+subUsuarioId).html(selTipoPerfilPi);
		$("#tdhabil"+subUsuarioId).html(selHabilitadoUsu);
		
		
	}
}

function verEmpresasSubUsuario(){
	$("#empresasSubUsuario").show();
	
}
function procesarResultadoEmpresasSubUsuario(data){
	switch (data.CODE_RESPONSE) {
	case "05":
		
		var listEmpresaTotal=data.listTotal;
		$("#empresasSubUsuario input").remove();
		$("#empresasSubUsuario label").remove();
		$("#empresasSubUsuario br").remove();$("#empresasSubUsuario").append("<br>")
		for (var i = 0; i < listEmpresaTotal.length; i++) {

			$("#empresasSubUsuario").append(

					"<input type='checkbox' " +
					"id='checkEmpresa"+listEmpresaTotal[i].idCompany+"' " +
							"name='checkEmpresa"+listEmpresaTotal[i].idCompany+"' >" +
							"<label for='checkEmpresa"+listEmpresaTotal[i].idCompany+"'>" +
							listEmpresaTotal[i].name+"</label><br>");
		}
		
		
		var listEmpresaAutorizada=data.listAutorizada;
		for(var i=0;i<listEmpresaAutorizada.length;i++){
			if(listEmpresaAutorizada[i]){
			$("#empresasSubUsuario #checkEmpresa"+listEmpresaAutorizada[i].idCompany).prop("checked",true)
		}}
		
		break;
	default:
		alert("Ocurrió un error al guardar el empresas del subusuario!");
	}
	
}
function guardarSubUsuario(){
	
	var empresasId=[];
	var usuarioNombre=$("#nombreSubUsuarioInput").val();
	var correo=$("#inputCorreoSubUsuario").val();
	
	
	var dataParam={
		idPerfil:$("#selTipoPerfil").val(),correo:correo,
		perfilIntertek:{id:$("#selTipoPerfilPi").val()},
		userId:subUsuarioId,
			userName:usuarioNombre,
			empresas:empresasId
			
	};
	callAjaxPost(URL + '/administrador/usuario/exists', dataParam,
			procesarExistenciaUsuario);
	
	
}

function  procesarExistenciaUsuario(data){
	switch (data.CODE_RESPONSE) {
	case "05":
		var usuarioTaken=data.usuarioTaken;
		if(usuarioTaken){
			alert("El Usuario existe, introduce otro nombre");
			
		}else{
			var empresasId=[];
			var usuarioNombre=$("#nombreSubUsuarioInput").val();
			var correo=$("#inputCorreoSubUsuario").val();
			var empresas=$("#slcAccesoEmpresasSubUsu").val();
			var habilitadoId = $("#selHabilitadoUsu").val();
			empresas.forEach(function(val,index){
				empresasId.push({empresaId:val});
			});
			 
			var dataParam={
					idPerfil:$("#selTipoPerfil").val(),
					correo : correo,
					habilitado : {id : habilitadoId},
					
					perfilIntertek:{id:$("#selTipoPerfilPi").val()},
					userId:subUsuarioId,
						userName:usuarioNombre,
						empresas:empresasId,
						accesoUsuarios:$("#selAcceso").val()
						
				};

			callAjaxPost(URL + '/subusuario/save', dataParam,
					procesarResultadoGuardarSubUsuario);
		}
		break;
	default:
		alert("Ocurrió un error al guardar la capacitacion!");
	}
	
}


function procesarResultadoGuardarSubUsuario(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		llamarSubUsuarios();
		break;
	default:
		alert("Ocurrió un error al guardar el acto inseguro!");
	}
}

//// Gestio´pon d e Perfiles
function llamarPefiles(){
	perfilId=0;
	banderaEdicion=false;
	$("#btnVolverPerfil").hide();
	$("#btnGuardarPerfil").hide();
	$("#btnNuevoPerfil").show();
	
	$("#modulosPefil").hide();

	callAjaxPost(URL + '/perfil', "",
			procesarLlamadaPerfiles);
	
}

function procesarLlamadaPerfiles(data){
	switch (data.CODE_RESPONSE) {
	case "05":
		var list = data.list;
	listFullPerfiles=list;
		$("#perfilesTable tbody tr").remove();

		for (index = 0; index < list.length; index++) {

			$("#perfilesTable tbody:first").append(

					"<tr id='tr" + list[index].perfilId
							+ "' onclick='javascript:editarPerfil("
							+ list[index].perfilId +","+index
							+ ")' >"

							+ "<td id='tdnombre" + list[index].perfilId
							+ "'>" + list[index].perfilNombre + "</td>"
							
							+ "<td id='tdmods" + list[index].perfilId
							+ "'>" + list[index].nombreModulosFull + " </td>"
							+ "</tr>");
		}
		formatoCeldaSombreable(true);
		
		break;
	default:
		alert("Ocurrió un error al traer las actos inseguros!");
	}

}

function nuevoPerfil(){
	if(!banderaEdicion){
		if(listFullSubUsuarios.length>listFullPerfiles.length){
			$("#btnVolverPerfil").show();
			$("#btnGuardarPerfil").show();
			$("#btnNuevoPerfil").hide();
			banderaEdicion=true;
			formatoCeldaSombreable(false);

		$("#perfilesTable tbody").append(
			"<tr id='tr0'>"
				+"<td><input type='text' class='form-control' id='nombrePerfilInput'>" +
						"</td>"
				+"<td>..." +
				"</td>"
				+"</tr>"	
		);
			
		}else{
			
			alert("Ya no puede crear perfiles")
		}
	
	}
}

function editarPerfil(pperfilId,pindex){
	if(!banderaEdicion){
		$("#btnVolverPerfil").show();
		$("#btnGuardarPerfil").show();
		$("#btnNuevoPerfil").hide();
		banderaEdicion=true;
		formatoCeldaSombreable(false);
			perfilId=pperfilId;
		var perfilNombre=listFullPerfiles[pindex].perfilNombre
		
		
		$("#tdnombre"+pperfilId).html(
		"<input class='form-control' id='nombrePerfilInput'>"		
		);
		nombrarInput("nombrePerfilInput",perfilNombre);
	var numModulos=	$("#tdmods"+pperfilId).text();
		
	$("#tdmods"+pperfilId).html(
			"<a onclick='verModulosPerfil("+pperfilId+")'>Seleccionar "+numModulos+"</a>"	
		);
	
$("#modulosPefil input[type='checkbox']").each(function(){
		$(this).prop("checked",false);
	})
	var dataParam={
			perfilId:perfilId
	}
	callAjaxPost(URL + '/perfil/modulos', dataParam,
			procesarResultadoModulosPerfil);
		
	}
}
function verModulosPerfil(perfilId){
	$("#modulosPefil").show();
	
}
function procesarResultadoModulosPerfil(data){
	switch (data.CODE_RESPONSE) {
	case "05":
		var listModulos=data.list;
		if(listModulos.length>0){
		for(var i=0;i<listModulos.length;i++){
			if(listModulos[i]){
			$("#checkModulo"+listModulos[i].moduloId).prop("checked",true);
			var editChecked=pasarNumberBool(listModulos[i].canEdit);
			$("#checkModEditar"+listModulos[i].moduloId).prop("checked",editChecked);
			var deleteChecked=pasarNumberBool(listModulos[i].canDelete);
			$("#checkModEliminar"+listModulos[i].moduloId).prop("checked",deleteChecked)
		}}
		}
		break;
	default:
		alert("Ocurrió un error al guardar el acto inseguro!");
	}
	
}
function guardarPerfil(){
	
	var modulosId=[];
	var perfilNombre=$("#nombrePerfilInput").val();
	if(perfilId>0){
	$("#modulosPefil input[id^='checkModulo']").each(function(){
		
		if($(this).prop('checked')){
			var idcheck=$(this).prop("id");
			var moduloId=parseInt(idcheck.substr(11,14));
			var edit_checked=$("#checkModEditar"+moduloId).prop("checked");
			var eliminate_checked=$("#checkModEliminar"+moduloId).prop("checked");
			var menu_perfil={
					moduloId:moduloId,
					estadoModulo:1,
					canEdit:pasarBoolInt(edit_checked),
					canDelete:pasarBoolInt(eliminate_checked)
			};
			modulosId.push(menu_perfil);
			
		
		}
	})
	

	
	}else{
		modulosId=null;
		
	}
	
	
	console.log(modulosId);
	var dataParam={
			perfilId:perfilId,
			perfilNombre:perfilNombre,
			modulosId:modulosId
			
	};
	callAjaxPost(URL + '/perfil/save', dataParam,
			procesarResultadoGuardarPerfil);
}

function procesarResultadoGuardarPerfil(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		llamarPefiles();
		break;
	default:
		alert("Ocurrió un error al guardar el acto inseguro!");
	}
}
function eliminarPerfil(){
	
	
	
}


function marcarModuloPerfil(id){

	var accesoMarcado=$("#checkModulo"+id).prop("checked");
	
	if(accesoMarcado){
		$("#checkModEditar"+id).prop("checked",true);
		$("#checkModEliminar"+id).prop("checked",true);
		$("#checkModEditar"+id).prop("disabled",false);
		$("#checkModEliminar"+id).prop("disabled",false);
	}else{
		$("#checkModEditar"+id).prop("checked",false);
		$("#checkModEliminar"+id).prop("checked",false);
		$("#checkModEditar"+id).prop("disabled",true);
		$("#checkModEliminar"+id).prop("disabled",true);
	}
	
	
	switch(id){
	case 4:
		var opcionRecursosTotales=$("#checkModulo4").prop("checked");
		if(opcionRecursosTotales){
			$("#checkModulo5").prop("checked",true);
			$("#checkModulo6").prop("checked",true);
			$("#checkModulo7").prop("checked",true);
			$("#checkModulo8").prop("checked",true);
			$("#checkModulo21").prop("checked",true);

			$("#checkModEditar5").prop("disabled",false);
			$("#checkModEditar6").prop("disabled",false);
			$("#checkModEditar7").prop("disabled",false);
			$("#checkModEditar8").prop("disabled",false);
			$("#checkModEditar21").prop("disabled",false);
			
			$("#checkModEliminar5").prop("disabled",false);
			$("#checkModEliminar6").prop("disabled",false);
			$("#checkModEliminar7").prop("disabled",false);
			$("#checkModEliminar8").prop("disabled",false);
			$("#checkModEliminar21").prop("disabled",false);
			}else{
			$("#checkModulo5").prop("checked",false);
			$("#checkModulo6").prop("checked",false);
			$("#checkModulo7").prop("checked",false);
			$("#checkModulo8").prop("checked",false);
			$("#checkModulo21").prop("checked",false);
			
			$("#checkModEditar5").prop("disabled",true);
			$("#checkModEditar6").prop("disabled",true);
			$("#checkModEditar7").prop("disabled",true);
			$("#checkModEditar8").prop("disabled",true);
			$("#checkModEditar21").prop("disabled",true);
			
			$("#checkModEliminar5").prop("disabled",true);
			$("#checkModEliminar6").prop("disabled",true);
			$("#checkModEliminar7").prop("disabled",true);
			$("#checkModEliminar8").prop("disabled",true);
			$("#checkModEliminar21").prop("disabled",true);
			
			
			$("#checkModEditar5").prop("checked",false);
			$("#checkModEditar6").prop("checked",false);
			$("#checkModEditar7").prop("checked",false);
			$("#checkModEditar8").prop("checked",false);
			$("#checkModEditar21").prop("checked",false);
				
			$("#checkModEliminar5").prop("checked",false);
			$("#checkModEliminar6").prop("checked",false);
			$("#checkModEliminar7").prop("checked",false);
			$("#checkModEliminar8").prop("checked",false);
			$("#checkModEliminar21").prop("checked",false);
		}
		break;
	}
	
	var opcionRecurso1=$("#checkModulo5").prop("checked");
	var opcionRecurso2=$("#checkModulo6").prop("checked");
	var opcionRecurso3=$("#checkModulo7").prop("checked");
	var opcionRecurso4=$("#checkModulo8").prop("checked");
	var opcionRecurso5=$("#checkModulo21").prop("checked");
	if(opcionRecurso1 || opcionRecurso2 || opcionRecurso3 || opcionRecurso4 || opcionRecurso5){
		$("#checkModulo4").prop("checked",true);
	}
	
	if(!opcionRecurso1 && !opcionRecurso2 && !opcionRecurso3 && !opcionRecurso4 && !opcionRecurso5){
		$("#checkModulo4").prop("checked",false);
	}
	
}
