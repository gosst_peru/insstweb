var banderaEdicion;

$(document).ready(function() {
	insertMenu();
	$("#inputCorreoChange").val(getSession("gestopcorreo"));
	verificarPass();
});

function cambiarCorreo() {
	var correo = $("#inputCorreoChange").val();
	var band = true;
	
	if( correo.length == 0){
		band = false;
	}
	
	if(band){
		var dataParam = {
				correo : correo
			};

			callAjaxPost(URL + '/login/datos/save', dataParam,
					procesarCambiarCorreo);		
	} else {
		alert("Debe ingresar todos los campos.");
	}

}
function verificarPass(){
	  var psw = $("#inputPassword2").val();
	  $("#indMin").removeClass("fa fa-check");
	  $("#indMay").removeClass("fa fa-check");
	  $("#indNum").removeClass("fa fa-check");
	  $("#indCaracter").removeClass("fa fa-check");
	  var passValido = true;
	  if (comprobarMinuscula(psw)) {$("#indMin").addClass("fa fa-check");}else{passValido = false;};
	  if (comprabarMayuscula(psw)) {$("#indMay").addClass("fa fa-check");}else{passValido = false;};
	  if (comprabarNumero(psw)) {$("#indNum").addClass("fa fa-check");}else{passValido = false;};
	  if (psw.length >= 8) {$("#indCaracter").addClass("fa fa-check");}else{passValido = false;};
	  $("#btnChangePass").prop("disabled",!passValido)
}
function comprobarMinuscula(string){
	for(var index = 0; index < string.length; index++){
	    var letraActual = string.charAt(index);
	    var isMinuscula = (letraActual === letraActual.toLowerCase());
		if(isMinuscula && !comprabarNumero(letraActual)){return true;}
	}
	 
	return false;
}
function comprabarMayuscula(string){
	
	for(var index = 0; index < string.length; index++){
	    var letraActual = string.charAt(index);
	    var isMinuscula = (letraActual === letraActual.toUpperCase());
		if(isMinuscula && !comprabarNumero(letraActual)){return true;}
	}
	return false;
}
function comprabarNumero(string){
	var arrayNumeros = ["0","1","2","3","4","5","6","7","8","9"];
	for(var index = 0; index < string.length; index++){
	    var letraActual = string.charAt(index);
	    var isNumero = false;
	    arrayNumeros.forEach(function(val){
	    	if(parseInt(val) === parseInt(letraActual)){isNumero = true;}
	    }); 
	    if(isNumero){return isNumero};
	}
	return false;
}
function procesarCambiarCorreo(data){
	switch (data.CODE_RESPONSE) {
 
	case "05":
		alert("El correo se guardo correctamente.!");
		break;
	 
		break;
	default:
		alert("Ocurrió un error al guardar el correo!");
	}
}
function cambiarPassword() {
	var psw1 = $("#inputPassword1").val();
	var psw2 = $("#inputPassword2").val();
	var psw3 = $("#inputPassword3").val();
	var band = true;
	
	if(psw3.length == 0 || psw2.length == 0 || psw1.length == 0){
		band = false;
	}
	
	if(band){
		var dataParam = {
				userName : sessionStorage.getItem("gestopusername"),
				psw1 : psw1.trim(),
				psw2 : psw2.trim(),
				psw3 : psw3.trim()
			};

			callAjaxPost(URL + '/login/password/save', dataParam,
					procesarCambiarPsw);		
	} else {
		alert("Debe ingresar todos los campos.");
	}

}

function procesarCambiarPsw(data){
	switch (data.CODE_RESPONSE) {
	case "02":
		alert("El password actual es incorrecto.!");
		break;
	case "03":
		alert("El password se ha bloqueado por demasiados intentos fallidos.!");
		break;
	case "05":
		alert("El password se guardo correctamente.!");
		break;
	case "07":
		alert("La confirmacion del nuevo password es incorrecta.!");
		break;
	default:
		alert("Ocurrió un error al guardar el trabajador!");
	}
}