var sugerenciaId,sugerenciaObj,sugerenciaNombre;
var banderaEdicion11=false;
var listFullSugerencia,listFiltroSugerencia;
var listMotivoSugerencia;
var listModulosSugerencia;
var estadoSugerencia={};
var toggleFiltroEstado=false;
var listEstadoSug=[{id:1,nombre:"Por implementar"},{id:2,nombre:"Completado"},{id:3,nombre:"Retrasado"}];
var arrayFiltroEstadoSug=[];
var objtemp;
$(function(){
	$("#btnNuevoSugerencia").attr("onclick", "javascript:nuevoSugerencia();");
	$("#btnCancelarSugerencia").attr("onclick", "javascript:cancelarNuevoSugerencia();");
	$("#btnGuardarSugerencia").attr("onclick", "javascript:guardarSugerencia();");
	$("#btnEliminarSugerencia").attr("onclick", "javascript:eliminarSugerencia();");
	$("#btnCancelarSugerencia").hide(); 
	$("#btnEliminarSugerencia").hide();
	$("#btnGuardarSugerencia").hide(); 
	resizeDivGosst("wrapperSug");
})
/**
 * 
 */
function volverSugerencias(){ 
	$("#solicitudes .container-fluid").hide();
	$("#divContainSugerencia").show();
	$("#solicitudes").find("h2").html("Solicitudes de usuarios Gosst");
}
var indexFuncion=0;
function toggleMenuOpcionSugerencia(obj,pindex)
{
	indexFuncion=pindex;
	$(obj).parent("#verOpc"+pindex).find("ul").toggle();
	$(obj).parent("#verOpc"+pindex).siblings().find("ul").hide();
	sugerenciaObj=listFullSugerencia[pindex];
}
var funcionalidadesSugerencia=
	[
		{id:"opcEditarSug",nombre:"<i class='fa fa-pencil-square-o'></i> Editar Evaluacion",functionClick:function(data){
			editarSugerencia(indexFuncion);}},
		{id:"opcCalificacion",nombre:"<i class='fa fa-star'></i> ver Calificación",functionClick:function(data){verClasificacionSugerencia();}}
	];
function marcarSubOppcionSuger(pindex)
{
	funcionalidadesSugerencia[pindex].functionClick(); 
	$(".listaGestionGosst").hide();
}
function cargarPrimerSugerencias()
{
	callAjaxPost(URL + '/scheduler/sugerencias/full', 
			{  },
			function(data)
			{
				listFullSugerencia=data.list;
				listMotivoSugerencia=data.motivos;
			 	listModulosSugerencia=data.modulos;
			 	cargarSugerencias(data);
			});
}
function verSeccionAnalisis(id){
	$("#sugerencia"+id+" > .divVerOcultarSug  > .divDescripcion").html("<a class='efectoLink' " +
			"onclick='ocultarSeccionAnalisis("+id+")'>" +
					"<i class='fa fa-angle-double-up'></i>" +
					"Ocultar análisis sistemas</a>");
	$("#sugerencia"+id+" .classAnalisis").show();
}
function ocultarSeccionAnalisis(id){
	$("#sugerencia"+id+" > .divVerOcultarSug > .divDescripcion").html("<a class='efectoLink' " +
			"onclick='verSeccionAnalisis("+id+")'>" +
					"<i class='fa fa-angle-double-down'></i>" +
					"Ver análisis sistemas</a>");
	$("#sugerencia"+id+" .classAnalisis").hide();
}
function cargarSugerencias(data) {
	$("#btnCancelarSugerencia").hide();
	$("#btnNuevoSugerencia").show();
	$("#btnEliminarSugerencia").hide();
	$("#btnGuardarSugerencia").hide(); 
	volverSugerencias();  
		switch(data.CODE_RESPONSE){
		case "05":
			banderaEdicion11=false;
			
		 	var txtCompl="",txtImpl="",txtRetr="";
		 	var numCompl=0,numImpl=0,numRetr=0;
			var menuOpcion="<ul class='list-group listaGestionGosst' style='top:25px'>"
			funcionalidadesSugerencia.forEach(function(val1, index1) {
				menuOpcion+="<li class='list-group-item' onclick='marcarSubOppcionSuger("+index1+")'>"+val1.nombre+"</li>";
			});
			menuOpcion+="</ul>";
			$("#tblSugerencia tbody tr").remove();
			listFullSugerencia.forEach(function(val,index){
				var textEvidencia="Sin evidencia";
				if(val.evidenciaNombre.length > 0){
					textEvidencia = "<a class='efectoLink' href='"+URL+"/scheduler/sugerencia/evidencia?id="+val.id+"'>" +
						"Descargar" +
					"</a>";
				}
				var textEvidenciaDiseño="Sin evidencia";
				if(val.evidenciaDiseñoNombre.length > 0){
					textEvidenciaDiseño = "<a class='efectoLink' href='"+URL+"/scheduler/sugerencia/evaluacion/evidencia?id="+val.id+"'>" +
						"Descargar" +
					"</a>";
				}
				var textObservacion=val.observaciones;
				if(val.observaciones.trim() == ""){
					textObservacion ="Sin observaciones";
				}
				var elementos=[{titulo:"",descripcion:"#00"+val.id},
				               {titulo:"<i class='fa fa-calendar'></i>",descripcion:""+val.fechaSolicitudTexto},
				               {titulo:"<i class='fa fa-user'></i>",descripcion:"<strong>"+val.usuario.userName+"</strong>"},
				               {titulo:"<i class='fa fa-info-circle'></i>",descripcion:"<strong>"+val.descripcion+"</strong><a class='efectoLink' onclick='editarSugerencia("+index+")'><i class='fa fa-check-square-o'></i></a>"},
				               {titulo:"<i class='fa fa-cubes'></i>",descripcion:""+val.modulosNombre},
				               {titulo:"<i class='fa fa-download'></i>",descripcion:textEvidencia},
				              // {div:"",titulo:"",descripcion:""},
				               {titulo:"",clase:"divVerOcultarSug",descripcion:"<a class='efectoLink' onclick='verSeccionAnalisis("+val.id+")'><i class='fa fa-angle-double-down'></i>Ver análisis sistemas</a>"},
				                {clase:"classAnalisis",titulo:"<i class='fa fa-calendar-o'></i>",descripcion:""+val.fechaPlanificadaTexto},
				               {clase:"classAnalisis",titulo:"<i class='fa fa-calendar-check-o'></i>",descripcion:""+val.fechaRealTexto},
				               {clase:"classAnalisis",titulo:"<i class='fa fa-eye'></i>",descripcion:""+textObservacion},
				               {clase:"classAnalisis",titulo:"<i class='fa fa-download'></i>",descripcion:textEvidenciaDiseño},
				               
				               ]
				
				var txtBase="<div class='eventoGeneral' id='sugerencia"+val.id+"'>";
				elementos.forEach(function(val1,index1){
					var estiloDefault="";
					if(val1.titulo=="" &&  val1.descripcion=="" ){
						estiloDefault="    border-top: 1px solid #a5aaae;"
					}
					txtBase+="<div style='"+estiloDefault+"' " +
							"class='"+val1.clase+" row' id='"+val1.id+"'>" +
								"<div class='divTitulo col col-sm-2'>" +
									val1.titulo+
								"</div>"+
								"<div class='divDescripcion col col-sm-10'>" +
									val1.descripcion +
								"</div>"+
							"</div>"+"";	
				});
				txtBase+="</div>"

				switch(val.estado.id){
				case 1:
					numImpl++;
					txtImpl +=txtBase;
					break;
				case 2:
					numCompl++;
					txtCompl +=txtBase;
					break;
				case 3:
					numRetr++;
					txtRetr +=txtBase;
					break;
				}
			}); 
			$("#divCompletados").html(" " +
					"<div class='tituloSubList'>"+"Completados ("+numCompl+")"+"</div>" +
					"<div class='contenidoSubList'>"+txtCompl+"</div>" +
					" " );
			$("#divRetrasados").html(" " +
					"<div class='tituloSubList'>"+"Retrasados ("+numRetr+")"+"</div>" +
					"<div class='contenidoSubList'>"+txtRetr+"</div>" +
					" " );
			$("#divImplementar").html(" " +
					"<div class='tituloSubList'>"+"Por implementar ("+numImpl+")"+"</div>" +
					"<div class='contenidoSubList'>"+txtImpl+"</div>" +
					" " ); 
			 $(".classAnalisis").hide();
 
			$(".listaGestionGosst").hide(); 
			  
			break;
		default:
			alert("no");
		}
	
}
function toggleMenuSug(opc)
{
	if(opc==1)
	{
		$("#listaImpl").toggle();
	}
	else if(opc==2)
	{
		$("#listaCompl").toggle();
	}else if(opc==3)
	{
		$("#listaRetr").toggle();
	}
}
function editarSugerencia(pindex) {


	if (!banderaEdicion11) {
		sugerenciaId = listFullSugerencia[pindex].id;
		sugerenciaObj=listFullSugerencia[pindex];
		$("#modalEditarSugerencia").modal("hide");
		$("#modalEditarSugerencia").modal("show");   
		$(".modal-header").find("h4").html("Editar Evaluación ");
		var fechaPlanificada=convertirFechaInput(sugerenciaObj.fechaPlanificada);
		var fechaReal=convertirFechaInput(sugerenciaObj.fechaReal);
		var observaciones=sugerenciaObj.observaciones;
		$("#modalEditarSugerencia .modal-body div").remove();
		$("#modalEditarSugerencia .modal-body").html(
				"<div class='row'>" +
				"<section class='col col-xs-4'>" +
					"<i class='fa fa-info'></i> Descripción:"+ 
				"</section>" +
				"<section class='col col-xs-4'>" +
				sugerenciaObj.descripcion+
				"</section>" +
			"</div><br>"+
			
				"<div class='row'>" +
					"<section class='col col-xs-4'>" +
						"<i class='fa fa-calendar-times-o'></i> Fecha de Planificacion:"+ 
					"</section>" +
					"<section class='col col-xs-4'>" +
						"<input type='date' id='inputDatePlanSugerencia' class='form-control'" +
						"onchange='hallarEstadoImplementacionSugerencia()'>"+
					"</section>" +
				"</div><br>"+
				"<div class='row'>" +
					"<section class='col col-xs-4'>" +
						"<i class='fa fa-calendar-check-o'></i> Fecha Real:"+ 
					"</section>" +
					"<section class='col col-xs-4'>" +
					 "<input type='date'   id='inputDateRealSugerencia' class='form-control'" +
					 "onchange='hallarEstadoImplementacionSugerencia()'>"+
					"</section>" +
				"</div><br>"+
				"<div class='row'>" +
					"<section class='col col-xs-4'>" +
						"<i class='fa fa-align-justify'></i> Observaciones:"+
					"</section>" +
					"<section class='col col-xs-6'>" +
						"<textarea class='form-control' id='inputObservacionesSugerencia'></textarea>"+
					"</section>" +
				"</div><br>"+
				"<div class='row'>" +
					"<section class='col col-xs-4'>" +
						"<i class='fa fa-download'></i> Evidencia:"+
					"</section>"+
					"<section class='col col-xs-6' id='eviEval'>" +
					"</section>"+
				"</div><br>"+
				"<div class='row'>" +
					"<section class='col col-xs-4'>" +
						"<i class='fa fa-search'></i> Estado:"+
					"</section>" +
					"<section class='col col-xs-6' id='solest"+sugerenciaId+"'>" + 
					"</section>" +
				"</div><br>"
				
		);   
		var options={
				container:"#eviEval",
				functionCall: function(){},
				descargaUrl:"/scheduler/sugerencia/evaluacion/evidencia?id="+sugerenciaId,
				esNuevo:false,
				idAux:"EvaluacionSolicitud",
				evidenciaNombre: sugerenciaObj.evidenciaDiseñoNombre
		};
		crearFormEvidenciaCompleta(options);
		$("#inputDatePlanSugerencia").val(fechaPlanificada);
		$("#inputDateRealSugerencia").val(fechaReal); 
		$("#inputObservacionesSugerencia").val(observaciones); 
		$("#modalEditarSugerencia .modal-footer").html(
				 "<button type='button' class='btn btn-success'  onclick='guardarSugerencia()' data-dismiss='modal'><i class='fa fa-save'></i>Guardar</button>"+
				 "<button type='button' class='btn btn-default'	data-dismiss='modal'>Cerrar</button>"
		); 
		var notaResuelve=(sugerenciaObj.notaResuelve==0?"No":"Si"); 
		
		$("#tdResuelveSug").html(notaResuelve);
		//
		var notaTiempo=sugerenciaObj.notaTiempo;
		var textRatingPersonal="";
		for(var i=0;i<5;i++){
			if(i<notaTiempo){
				textRatingPersonal+="<i class='fa fa-star fa-2x ' id='ratingGosst"+(i+1)+"'></i>";
			}else{
				textRatingPersonal+="<i class='fa fa-star-o fa-2x ' id='ratingGosst"+(i+1)+"'></i>";
			}
			
		}
		
		$("#tdTiempoSug").html(textRatingPersonal);
		 
		//
		var comentario=sugerenciaObj.comentario;
		$("#tdComentSug").html(comentario); 
		//
		$("#btnCancelarSugerencia").hide();
		$("#btnNuevoSugerencia").hide();
		$("#btnEliminarSugerencia").hide();
		$("#btnGuardarSugerencia").hide();
		$("#btnGenReporte").hide();
		
		hallarEstadoImplementacionSugerencia();
		
		
	}
	
}


function verIdsmodulos(){
	console.log($("#slcModulosSugerencia").val());
} 

function cancelarSugerencia() {
	cargarSugerencias();
}


function guardarSugerencia() {

	var campoVacio = true;
	var fechaPlanificada=$("#inputDatePlanSugerencia").val();   
	var fechaReal=$("#inputDateRealSugerencia").val();   
	 var observaciones=$("#inputObservacionesSugerencia").val(); 
	 var evidenciaDiseño=$("#inputEviEvaluacionSolicitud").val(); 
	 objtemp={
		 fechaPlanificada:convertirFechaTexto(fechaPlanificada),
		 fechaReal:convertirFechaTexto(fechaReal),
		 estadoSugerencia:estadoSugerencia,
		 observaciones:observaciones,
		 evidenciaDiseñoNombre:evidenciaDiseño
	}
		if (campoVacio) {

			var dataParam = {
				id : sugerenciaId,  
				fechaPlanificada:convertirFechaTexto(fechaPlanificada),
				fechaReal:convertirFechaTexto(fechaReal),
				estado:estadoSugerencia,
				observaciones:observaciones
			};

			callAjaxPost(URL + '/scheduler/sugerencia/solucion/save', dataParam,
					procesarResultadoGuardarSugerencia,null,null,null,null,false);
			
		} else {
			alert("Debe ingresar todos los campos.");
		}
}
function procesarResultadoGuardarSugerencia(data)
{
	switch (data.CODE_RESPONSE) {
	case "05":
		guardarEvidenciaAuto(data.nuevoId,"fileEviEvaluacionSolicitud",
				bitsEvidenciaSolicitudEval,"/scheduler/sugerencia/evaluacion/evidencia/save",
				function(){},"id")
		 
			 
		listFullSugerencia.forEach(function(val,index)
		{
			if(val.id==sugerenciaId)
			{ 
				listFullSugerencia[index].fechaPlanificada=objtemp.fechaPlanificada;
				listFullSugerencia[index].fechaReal=objtemp.fechaReal;
				listFullSugerencia[index].estado=objtemp.estadoSugerencia;
				listFullSugerencia[index].observaciones=objtemp.observaciones;  
				listFullSugerencia[index].evidenciaDiseñoNombre=objtemp.evidenciaDiseñoNombre;  
			}
		}); 
		cargarSugerencias(data);
		$.unblockUI()
	 
		break;
	default:
		console.log("Ocurrió un error al guardar el sugerencia!");
	}
}
 function verClasificacionSugerencia(){
	 $("#solicitudes .container-fluid").hide();
		$("#divContainCalificacion").show();
		$("#solicitudes").find("h2").html(
				"<a onclick='volverSugerencias()'>Solicitud "+sugerenciaObj.descripcion+"</a> > Calificación");
 }
function cancelarNuevoSugerencia(){
	cargarSugerencias();
}



function hallarEstadoImplementacionSugerencia(){
	var fechaPlanificada=$("#inputDatePlanSugerencia").val();
	var fechaReal=$("#inputDateRealSugerencia").val();
	var fechaHoy=obtenerFechaActual();
	if(fechaReal!=''){
		estadoSugerencia={id:2,nombre:"Completado"} 
		}else{
		
		if(fechaPlanificada!=""){
			var dif=restaFechas(fechaHoy,fechaPlanificada)
			if(dif<0){
				estadoSugerencia={id:3,nombre:"Retrasado"} 
				}else{
				estadoSugerencia={id:1,nombre:"Por implementar"} 
				}
			
		}else{
			estadoSugerencia={id:1,nombre:"Por implementar"}
			
		}
	}
	
	$("#solest"+sugerenciaId).html(estadoSugerencia.nombre);
	
}





















