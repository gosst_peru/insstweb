var actualizacionId,actualizacionObj;
var banderaEdicion22=false;
var listFullActualizacion;
var listVersiones;
var listModulosActualizacion;
var listNuevoActualizacion=[{id:0,nombre:"No"},{id:1,nombre:"Sí"}];
$(function(){
	$("#btnNuevoActualizacion").attr("onclick", "javascript:nuevoActualizacion();");
	$("#btnCancelarActualizacion").attr("onclick", "javascript:cancelarNuevoActualizacion();");
	$("#btnGuardarActualizacion").attr("onclick", "javascript:guardarActualizacion();");
	$("#btnEliminarActualizacion").attr("onclick", "javascript:eliminarActualizacion();");
	resizeDivGosst("wrapperAct");
})
/**
 * 
 */
function volverActualizacions(){ 
	$("#tabActualizaciones .container-fluid").hide();
	$("#divContainActualizacion").show();
	$("#tabActualizaciones").find("h2").html("Actualizaciones de Gosst");
	 
}
function cargarActualizacions() {
	$("#btnCancelarActualizacion").hide();
	$("#btnNuevoActualizacion").show();
	$("#btnEliminarActualizacion").hide();
	$("#btnGuardarActualizacion").hide();
	volverActualizacions();
	callAjaxPost(URL + '/scheduler/actualizaciones', 
			{  }, function(data){
				if(data.CODE_RESPONSE=="05"){
					banderaEdicion22=false;
				 listFullActualizacion=data.list;
				 listVersiones=data.versiones;
				 listModulosActualizacion=data.modulos;
					$("#tblActualizacion tbody tr").remove();
					listFullActualizacion.forEach(function(val,index){
						
						$("#tblActualizacion tbody").append(
								"<tr id='tractu"+val.id+"' onclick='editarActualizacion("+index+")'>" 
								+"<td id='actuver"+val.id+"'>"+val.version.nombre+"</td>"  
								+"<td id='actufec"+val.id+"'>"+val.fechaTexto+"</td>"  
								+"<td id='actumod"+val.id+"'>"+val.modulo.nombre+"</td>" 
								+"<td id='actudes"+val.id+"'>"+val.descripcion+"</td>" 
								+"<td id='actuevi"+val.id+"'>"+val.evidenciaNombre+"</td>" 
								+"<td id='actunue"+val.id+"'>"+(val.isNuevo==0?"No":"Si")+"</td>" 
								+"</tr>");
					});  
					formatoCeldaSombreableTabla(true,"tblActualizacion");
				}else{
					console.log("NOPNPO")
				}
			});

	
}  
function editarActualizacion(pindex) {


	if (!banderaEdicion22) {
		formatoCeldaSombreableTabla(false,"tblActualizacion");
		actualizacionId = listFullActualizacion[pindex].id;
		actualizacionObj=listFullActualizacion[pindex];
		 
		var version=actualizacionObj.version.id;
		var slcVersionActualizacion=crearSelectOneMenuOblig("slcVersionActualizacion", "",
				listVersiones, version, "id", "nombre");
		$("#actuver" + actualizacionId).html(slcVersionActualizacion );
		//
		var fecha=convertirFechaInput(actualizacionObj.fecha);
		$("#actufec" + actualizacionId).html(
		 "<input type='date'   id='inputDateActualizacion'   class='form-control'>");
		$("#inputDateActualizacion").val(fecha);
		// 
		var modulo=actualizacionObj.modulo.id;
		var slcModuloActualizacion=crearSelectOneMenuOblig("slcModuloActualizacion", "",
				listModulosActualizacion, modulo, "id", "nombre");
		$("#actumod" + actualizacionId).html(slcModuloActualizacion );
		//
		var descripcion=actualizacionObj.descripcion;
		$("#actudes" + actualizacionId).html(
		 "<input  id='inputDescripcionActualizacion'  class='form-control'>");
		$("#inputDescripcionActualizacion").val(descripcion);
		//
		var options=
		{container:"#actuevi"+actualizacionId,
				functionCall:function(){ },
				descargaUrl: "/scheduler/actualizacion/evidencia?id="+ actualizacionId,
				esNuevo:false,
				idAux:"Actualizacion"+actualizacionId,
				evidenciaNombre:actualizacionObj.evidenciaNombre};
		crearFormEvidenciaCompleta(options);
		// 
		var isNuevo=actualizacionObj.isNuevo;
		var slcNuevoActualizacion=crearSelectOneMenuOblig("slcNuevoActualizacion", "",
				listNuevoActualizacion, isNuevo, "id", "nombre");
		$("#actunue" + actualizacionId).html(slcNuevoActualizacion );
		//
		banderaEdicion22 = true;
		$("#btnCancelarActualizacion").show();
		$("#btnNuevoActualizacion").hide();
		$("#btnEliminarActualizacion").show();
		$("#btnGuardarActualizacion").show();
		$("#btnGenReporte").hide(); 
		
		
	}
	
}
 
function cancelarActualizacion() {
	cargarActualizacions();
}


function nuevoActualizacion() {
	if (!banderaEdicion22) {
		actualizacionId = 0;
		var slcVersionActualizacion=crearSelectOneMenuOblig("slcVersionActualizacion", "",
				listVersiones, "", "id", "nombre");
		var slcModuloActualizacion=crearSelectOneMenuOblig("slcModuloActualizacion", "verIdsmodulos()",
				listModulosActualizacion, "", "id", "nombre");
		var slcNuevoActualizacion=crearSelectOneMenuOblig("slcNuevoActualizacion", "",
				listNuevoActualizacion, "", "id", "nombre");
		$("#tblActualizacion tbody")
				.prepend(
						"<tr  >"
						+"<td>"+slcVersionActualizacion+"</td>"
						+"<td>"+"<input type='date'   id='inputDateActualizacion'   class='form-control'>"+"</td>" 
						
						+"<td>"+slcModuloActualizacion+"</td>"
						+"<td>"+ "<input   id='inputDescripcionActualizacion' class='form-control'>"+"</td>" 
						+"<td id='actuevi0'>0</td>" 
						+"<td>"+slcNuevoActualizacion+"</td>"
								+ "</tr>"); 
		// 
		//
		var options=
		{container:"#actuevi"+actualizacionId,
				functionCall:function(){ },
				descargaUrl: "",
				esNuevo:true,
				idAux:"Actualizacion"+actualizacionId,
				evidenciaNombre:""};
		crearFormEvidenciaCompleta(options);
		$("#btnCancelarActualizacion").show();
		$("#btnNuevoActualizacion").hide();
		$("#btnEliminarActualizacion").hide();
		$("#btnGuardarActualizacion").show(); 
		banderaEdicion22 = true;
		formatoCeldaSombreableTabla(false,"tblActualizacion");
	} else {
		alert("Guarde primero.");
	}
}


function guardarActualizacion() {

	var campoVacio = true;
	var version=$("#slcVersionActualizacion").val();   
	var fecha=$("#inputDateActualizacion").val();   
	 var modulo=$("#slcModuloActualizacion").val(); 
	 var descripcion=$("#inputDescripcionActualizacion").val();
	 var isNuevo=$("#slcNuevoActualizacion").val();
		if (campoVacio) {

			var dataParam = {
				id : actualizacionId,
				version:{id:version},
				fecha:convertirFechaTexto(fecha), 
				modulo:{id:modulo},
				descripcion:descripcion ,
				isNuevo:isNuevo
			};

			callAjaxPost(URL + '/scheduler/actualizacion/save', dataParam,
					function(data) {
						switch (data.CODE_RESPONSE) {
						case "05":
							guardarEvidenciaAuto(data.nuevoId,"fileEviActualizacion"+actualizacionId
									,bitsEvidenciaAccionMejora,"/scheduler/actualizacion/evidencia/save",
									cargarActualizacions); 
							 
							break;
						default:
							console.log("Ocurrió un error al guardar el actualizacion!");
						}
					},null,null,null,null,false);
			
		} else {
			alert("Debe ingresar todos los campos.");
		}
	
	
} 
function cancelarNuevoActualizacion(){
	cargarActualizacions();
}

function eliminarActualizacion() {
	var r = confirm("¿Está seguro de eliminar la actualizacion?");
	if (r == true) {
		var dataParam = {
			id : actualizacionId,
		};

		callAjaxPost(URL + '/scheduler/actualizacion/delete', dataParam,
				function(data) {
					switch (data.CODE_RESPONSE) {
					case "05":
						cargarActualizacions();
						break;
					default:
						alert("Ocurrió un error al eliminar la actualizacion!");
					}
				});
	}
}





















