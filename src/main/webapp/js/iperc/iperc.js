/** *************variables de empresa*********** */
var banderaEdicion;
var idIPERCEdicion;
var idMdfEdicion;
var idMatrizRiesgo;
var selectOneBoxMR;
var nivelesRiesgos;
var nombreIperc;
var idFlagEvalIni;
var indicadorCons;
var idProbEspe;
var idEvalFrec;
var listProbEspe;
var listaSevEspe;

var mdfNombreAux;
var listaIpercMdfIds=[];
var listaAux=[];
var listaPersonaExpuesta;
var listTrabajadores;
var listanombretab;

var listaIndCapa;
var listaIndRisk;
var listaIndProc;
var listaFrecPel;
var listFullIperc;
$(document).ready(function() {
	$("#buscadorIperc").hide();
	$("#opcionesColumna").hide();
	insertMenu();
	listarIPERCs();
	$("#btnClipTabla").hide();
	
	resizeDivGosst("wrapper");
});

window.onresize = function(event) {
	resizeDivGosst("wrapper");
}



function listarIPERCs() {
	
	$("#btnVolverIPERC").hide();
	banderaEdicion = false;
	idIPERCEdicion = 0;
	idMdfEdicion = 0;
	idMatrizRiesgo = 0;
	nivelesRiesgos = null;
	nombreIperc = "";
	idFlagEvalIni = 0;
	indicadorCons = 0;
	idProbEspe=0;
	idEvalFrec=0;
	listFullIperc=[];
	deshabilitarBotonesEdicion();
	$("#btnDetalle").hide();
	$("#btnConfigurar").hide();
	$("#btnDescargar").hide();
	$("#btnCopiar").hide();
	$("#btnAutoAsign").hide();
	
	$("#btnCopiar").attr("onclick", "duplicarIperc()");
	$("#btnNuevo").attr("onclick", "nuevoIperc()");
	$("#btnCancelar").attr("onclick", "cancelarIperc()");
	$("#btnGuardar").attr("onclick", "guardarIperc()");
	$("#btnEliminar").attr("onclick", "eliminarIperc()");
	$("#btnDescargar").attr("onclick", "descargarMatriz()");

	
	var dataParam = {
			companyId : sessionStorage.getItem("gestopcompanyid")
	};
	var dataParam2= {
			idCompany : sessionStorage.getItem("gestopcompanyid")
	};
	callAjaxPost(URL + '/iperc', dataParam, procesarListarIperc,function(){$("#wrapper").block({message:'<img src="../imagenes/gif/ruedita.gif"></img>cargando...'});});
	
	callAjaxPost(URL + '/accidente/trab', dataParam2,
			function (data){
		switch (data.CODE_RESPONSE) {
			
			case "05":
			
				listTrabajadores=data.listDniNombre;
				
				break;
			default:
				alert("Ocurrió un error al traer los trabajadores!");
			}

		listanombretab=listarStringsDesdeArray(listTrabajadores, "nombre");
		},function(){});
}

function procesarListarIperc(data) {
	$("#tblMatrices").removeClass("fixed");
	switch (data.CODE_RESPONSE) {
	case "05":
		var list = data.list;
		listFullIperc=data.list;
		var lisInd=data.lisInd;
		listaPersonaExpuesta=data.listaPersonaExpuesta;
		listaIndCapa=data.listaIndCapa;
		listaIndRisk=data.listaIndRisk;
		listaIndProc=data.listaIndProc;
		listaFrecPel=data.listaFrecPel;
		listaSevEspe=data.listaSevEspe;
		listProbEspe=data.listProbEspe;
		$("#tblMatrices tbody tr").remove();
		$("#tblMatrices thead tr").remove();

		$("#h2Titulo").html(
				"<a onclick='javascript:listarIPERCs();' href='#'>IPERCs</a>");

		$("#tblMatrices thead").append("<tr id='thMatriz'> </tr>");
		var textoCabecera="<td class='tb-acc' rowspan='2'>Nombre</td>" +
				"<td class='tb-acc' rowspan='2'>Unidades - Nivel</td>" +
				"<td class='tb-acc' rowspan='2'>Matriz de Evaluación de Riesgo (PxC)</td>" +
			

				"<td class='tb-acc' rowspan='2'># evaluaciones con riesgo significativo</td>" +
				"<td class='tb-acc' rowspan='2'># de controles por implementar</td>" +
				"<td class='tb-acc' rowspan='2'># de controles con retraso</td>"+
		"<td class='tb-acc' rowspan='2'>Versiones</td>" +
"<td class='tb-acc' colspan='6'>Última Actualización</td>"
		;
		 
		var	textoCabeceraMovil="<div class='row' style='text-align: center;'>" 
			+ "<div class='col-xs-2 col-sm-2'>Nombre</div>"
			+ "<div class='col-xs-2 col-sm-2'>Unidad - Nivel</div>"
			+ "<div class='col-xs-2 col-sm-2'> (PxC) </div>"
			
			+ "<div class='col-xs-2 col-sm-2'>Riesgo</div>"
			+ "<div class='col-xs-2 col-sm-2'>Ctrl. Implementar</div>"
			+ "<div class='col-xs-2 col-sm-2'>Ctrl. Retraso </div>"
			+ "</div>";
		 
		textoCabecera=	ifVerificarMovil(2,textoCabeceraMovil,textoCabecera);
		$("#thMatriz").html(
				textoCabecera);
		$("#thMatriz").after("<tr>" +
				"<td class='tb-acc'>Responsable</td>" +
				"<td class='tb-acc' style='width:190px'>Fecha Actualización Planificada</td>" +
				"<td class='tb-acc' style='width:190px'>Hora Planificada</td>" +
				"<td class='tb-acc' style='width:190px'>Fecha Actualización Real</td>" +
				"<td class='tb-acc' style='width:190px'>Fecha Próxima Actualización </td>" +
	"<td class='tb-acc'>Evidencia</td>"+		
		"</tr>")
		$("#tblEmpresas tbody tr").remove();

		for (var index = 0; index < list.length; index++) {
			var versionActiva={
					responsable:"",
					fechaActualizacionTexto:"",
					horaPlanificadaTexto:"",
					fechaPlanificadaTexto:"",
					evidenciaNombre:""
						
			};
			if(list[index].versiones[0]){
				versionActiva=list[index].versiones[0];
			}
			$("#tblMatrices tbody").append(

					"<tr id='trm" + list[index].ipercId
							+ "' onclick='javascript:editarIperc("
							+ index+")' >"
							+ "<td  id='divNom"
							+ list[index].ipercId + "'>"
							+ list[index].ipercName + "</td>"

							+ "<td  id='divMdf"
							+ list[index].ipercId + "'>" + list[index].mdfName
							+ "</td>"

							+ "<td  id='divMR"
							+ list[index].ipercId + "'>"
							+ list[index].matrizRiesgoNombre + "</td>"
							
							+ "<td  id='evRie"
							+ list[index].ipercId + "'>"
							+ list[index].evalRiesgosas 
							+ " / "+list[index].evalRealizadas+"</td>"
							
							+ "<td  id='ctrImp"
							+ list[index].ipercId + "'>" + lisInd[index].controlesImplementar
							+ "</td>"

							+ "<td  id='ctrRea"
							+ list[index].ipercId + "'>"
							+ lisInd[index].controlesRetrasados 

							+ "</td>" 
							+ "<td  id='tdver"
							+ list[index].ipercId + "'>"
							+ list[index].versiones.length 
							+ "</td>"
							+ "<td  id='tdrespon"
							+ list[index].ipercId + "'>"
							+ versionActiva.responsable
							+ "</td>" 
							+ "<td  id='tdfap"
							+ list[index].ipercId + "'>"
							+ versionActiva.fechaPlanificadaTexto
							+ "</td>" 
							+ "<td  id='tdhap"
							+ list[index].ipercId + "'>"
							+ versionActiva.horaPlanificadaTexto
							+ "</td>" 
							+ "<td  id='tdfaar"
							+ list[index].ipercId + "'>"
							+ versionActiva.fechaActualizacionTexto
							+ "</td>" 
							+ "<td  id='tdfar"
							+ list[index].ipercId + "'>"
							+ list[index].fechaRealTexto
							+ "</td>" 
							+ "<td  id='tdeviper"
							+ list[index].ipercId + "'>"
							+ versionActiva.evidenciaNombre 

							+ "</td>" 
							+ "</tr>");
		
		}
		completarBarraCarga();
		formatoCeldaSombreableTabla(true,"tblMatrices");
		
		$("#wrapper").unblock();
		if(getSession("linkCalendarioIpercId")!=null){
			var indexIpercAux=0;
			listFullIperc.every(function(val,index3){
				
				if(val.ipercId==parseInt(getSession("linkCalendarioIpercId"))){
					editarIperc(index3);
					if(getSession("linkCalendarioVersionId")!=null){
						verVersionesIperc();
					}else{
						listarDetalleIperc();
					}
					
					
					sessionStorage.removeItem("linkCalendarioIpercId");
					return false;
				}else{
					return true;
				}
			});
			
		}
		break;
	default:
		alert("Ocurrió un error al traer los ipercs!");
	}
	
}

var mdfAux;
var yaExisteIperc=false;
var listaMdfNoHay=[];



function editarIperc(pindex) {
	if (!banderaEdicion) {
		
		idIPERCEdicion = listFullIperc[pindex].ipercId;
		
		idMdfEdicion = listFullIperc[pindex].mdfId;
		mdfNombreAux=listFullIperc[pindex].mdfName;
		idMatrizRiesgo = listFullIperc[pindex].matrizRiesgoId ;
		idFlagEvalIni = listFullIperc[pindex].flagEvaluacionInicial ;
		indicadorCons = listFullIperc[pindex].indicadorConsecuencia ;
		idProbEspe=listFullIperc[pindex].flagProbabilidadEspecifica ;
		idEvalFrec=listFullIperc[pindex].flagEvaluacionFrecuencia ;
		var versionActiva={
				responsable:"",
				fechaActualizacionTexto:"",
				horaPlanificadaTexto:"",
				fechaPlanificadaTexto:"",
				evidenciaNombre:""
					
		};
		if(listFullIperc[pindex].versiones[0]){
			versionActiva=listFullIperc[pindex].versiones[0];
		}
		var responsable=listFullIperc[pindex].responsable;
		var evidenciaNombre=$("#tdeviper" + idIPERCEdicion).text();
		var fechaPlanificada=convertirFechaInput(listFullIperc[pindex].fechaPlanificada);
		var horaPlanificada=listFullIperc[pindex].horaPlanificada;
		var fechaRealTexto=convertirFechaInput(listFullIperc[pindex].fechaReal);
		
	
		$("#tdfar" + idIPERCEdicion)
		.html("<input id='inputFar' type='date' class='form-control' >");

		
		$("#inputFar").val(fechaRealTexto);
		$("#tdver" + idIPERCEdicion)
		.html("<a onclick='verVersionesIperc()'>"+listFullIperc[pindex].versiones.length+"</a>")
		$("#tdeviper" + idIPERCEdicion)
		.html("<a id='linkEvidencia' " +
				"href='"+URL 
				+ "/iperc/version/evidencia?versionId="+versionActiva.id+"' target='_blank' >" +
				evidenciaNombre+	"</a>" )
		
		
		formatoCeldaSombreableTabla(false,"tblMatrices");
		var dataParam = {
			
				companyId : sessionStorage.getItem("gestopcompanyid")
		};

		callAjaxPost(URL + '/mdf/listbox', dataParam, disenoEditarIperc);
		habilitarBotonesEdicion();
		$("#btnDetalle").show();
		$("#btnDetalle").attr("onclick", "listarDetalleIperc()");

		banderaEdicion = true;
	}
}

function disenoEditarIperc(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		mdfs = data.list;
		nivelesRiesgos = data.listMR;

		var name = $("#divNom" + idIPERCEdicion).text();
		nombreIperc = name;
		$("#divNom" + idIPERCEdicion)
				.html(
						"<input type='text' " +
						"id='inputIperc' class='form-control' placeholder='Nombre IPERC' >");
		
		nombrarInput("inputIperc",name);
		var objectSocket={
				accion:"edit",
				id:idIPERCEdicion
		}
		sendText(objectSocket);
		
		break;
	default:
		alert("Ocurrió un error al traer las matrices de funciones!");
	}
}

function nuevoIperc() {
	if (!banderaEdicion) {
		idIPERCEdicion = 0;
		var dataParam = {
				companyId : sessionStorage.getItem("gestopcompanyid")
		};

		callAjaxPost(URL + '/mdf/listbox', dataParam, disenoNuevoIperc);

		pasoIdEdicion = 0;
		habilitarBotonesNuevo();
		formatoCeldaSombreableTabla(false,"tblMatrices");
		banderaEdicion = true;
	} else {
		alert("Guarde primero el IPERC.");
	}
}

function disenoNuevoIperc(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		mdfs = data.list;
		var listaTipoMatrizRiesgo = data.listMR;

		var selectOneBox = "<select id='idSelMdf' class='form-control'><option value='-1'>Escoja una Unidad </option>";
		for (index = 0; index < mdfs.length; index++) {
			selectOneBox = selectOneBox + "<option value='"
					+ mdfs[index].matrixId + "'>" + mdfs[index].matrixName
					+ "</option>";

		}
		selectOneBox = selectOneBox + "</select>";

		selectOneBoxMR = "<select id='idSelMR' class='form-control'><option value='-1'>Escoja una matriz de funciones</option>";
		for (index = 0; index < listaTipoMatrizRiesgo.length; index++) {
			selectOneBoxMR = selectOneBoxMR + "<option value='"
					+ listaTipoMatrizRiesgo[index].mrId + "'>"
					+ listaTipoMatrizRiesgo[index].mrNombre + "</option>";

		}
		selectOneBoxMR = selectOneBoxMR + "</select>";

		$("#tblMatrices tbody")
				.append(
						"<tr id='0'>"
								+ "<td >"
								+ "<input type='text' id='inputIperc' "
								+ "class='form-control' placeholder='IPERC'>"
								+ "</td>"
								
								+ "<td >"
								+ selectOneBox + "</td>"

								+ "<td>"
								+ selectOneBoxMR + "</td>"
								+"<td>...</td>"
								+"<td>...</td>"
								+"<td>...</td>"
								+"<td>...</td>"
								+"<td>...</td>"
								+"<td>...</td>"
								+"<td>...</td>"
								+"<td>...</td>"
								+"<td><input id='inputFar' type='date' class='form-control' ></td>"
								
								+"<td>...</td>"	+"</tr>");
		
		var objectSocket={
				accion:"insert",
				id:0
		}
		sendText(objectSocket);
		// inicializarAutocompletar();
		break;
	default:
		alert("Ocurrió un error al traer las pasos!");
	}
}

function guardarIperc() {
	
	var campoVacio = true;
	var idMdfEdicion = $("#idSelMdf option:selected").val();
	var idMREdicion = idMatrizRiesgo;
	var responsable=$("#inputResponsable").val();
	var fechaPlan=convertirFechaTexto($("#inputFap").val());
	var hora=$("#inputHap").val();
	var fechaReal=convertirFechaTexto($("#inputFar").val());
	if(idIPERCEdicion==0){
		idMREdicion = $("#idSelMR option:selected").val();
	}
	
	if ($("#inputIperc").val().length == 0) {
		campoVacio = false;
	}

	if (idMdfEdicion == '-1') {
		campoVacio = false;
	}
	if (idMREdicion == '-1') {
		campoVacio = false;
	}

	if (campoVacio) {

		var dataParam = {
			ipercName : $("#inputIperc").val(),
			ipercId : idIPERCEdicion,
			mdfId : idMdfEdicion,
			matrizRiesgoId : idMREdicion,
			flagEvaluacionInicial : idFlagEvalIni,
			indicadorConsecuencia : indicadorCons,
			flagProbabilidadEspecifica:idProbEspe,
			flagEvaluacionFrecuencia:idEvalFrec,
			fechaReal:fechaReal
		};

		callAjaxPost(URL + '/iperc/save', dataParam, procesarGuardarIperc,function(){console.log("Guardado");});
	} else {
		alert("Debe ingresar todos los campos.");
	}
}

function procesarGuardarIperc(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		$("#tblMatrices tbody tr").remove();
		$("#btnDetalle").hide();
		var objectSocket={
				accion:"success",
				id:idIPERCEdicion
		}
		sendText(objectSocket);
		listarIPERCs();
		deshabilitarBotonesEdicion();
		 
		banderaEdicion = false;
		break;
	default:
		alert("Ocurrió un error al guardar la matriz!");
	}
}

function cancelarIperc() {
	$("#btnDetalle").hide();
	var objectSocket={
			accion:"success",
			id:idIPERCEdicion
	}
	sendText(objectSocket);
	listarIPERCs();
	deshabilitarBotonesEdicion();
	banderaEdicion = false;
}

function eliminarIperc() {
	var r = confirm("¿Está seguro de eliminar el IPERC?");
	if (r == true) {
		var dataParam = {
				ipercId : idIPERCEdicion,
		};

		callAjaxPost(URL + '/iperc/delete', dataParam, procesarEliminarIperc);
	}

}

function procesarEliminarIperc(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		if(data.mensaje.length>0){
			alert(data.mensaje);
		}
		
		$("#btnDetalle").hide();
		listarIPERCs();
		deshabilitarBotonesEdicion();
		banderaEdicion = false;
		break;
	default:
		alert("Ocurrió un error al eliminar el IPERC!");
	}
}

function descargarMatriz(){
	window.open(URL
			+ "/iperc/detail/excel?ipercId="
			+ idIPERCEdicion + "&ipercName="
			+ nombreIperc, '_blank');	
}

function verVersionesIperc(){
	$("#mdVersionIperc").modal("show");
	llamarVersionesIperc();
}