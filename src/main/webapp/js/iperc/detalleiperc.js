var detalleIpercId;
var detalleIpercDetailId;
var peligroId;
var clasiId;
var riesgoId;
var probabilidadId;
var severidadId;
var probabilidadActId;
var severidadActId;
var probabilidadProyId;
var severidadProyId;
var eficacia;
var isRiskyProy;
var isRiskyAct;
var detallesFullIperc;
var listSeveridad;
var listProbabilidad;
var listNivelRiesgo;
var indexAux;
var tipoCtrlId;

var listFullConsecuencias=[];

var personExpId;
var indProcId;
var indCapaId;
var indRiskId;

var personExpIdAct;
var indProcIdAct;
var indCapaIdAct;
var indRiskIdAct;

var personExpIdFut;
var indProcIdFut;
var indCapaIdFut;
var indRiskIdFut;

var frecPelPuroId;
var frecPelActId;
var frecPelFutId;
var idRiesgoEspecial;
var idRiesgoActEspecial;
var idRiesgoFutEspecial;
var contador2 = 0;

/** ************INGRESO DE PELIGROS DE FORMA AUTOMATICA******************** */
var autoFocusPeligro = false;
var autoFocusRiesgo = false;
var autoFocusConsecuencia = false;
var nuevoDupIpercAutomatico = false;
var ipercDetalleEvalIdCopy = 0;
/** *********************************************************************** */
var combosSugeridos=[];
var listFullControles;
/** ****************SUELTOS******************** */
$(function(){
	
	$("#btnClipTabla").tooltip({
		title:"Copiar controles al clipboard", 
		placement: "right"
	});
	$("#btnClipTabla").on("click",function(){
		new Clipboard('#btnClipTabla', {
			text : function(trigger) {
				
				return listFullControles;
			}
		});

		
		alert("Se han guardado al clipboard la tabla de este módulo" );

	});
	
	$('#buscadorTrabajadorInput').keyup(function(e){
	    if(e.keyCode == 13)
	    {
	        $(this).trigger("enterKey");
	    }
	});
	$('#buscadorTrabajadorInput').bind("enterKey",function(e){
		listarDetalleIperc();
		});
	$("#buscadorTrabajadorInput").focus();
	
	$("#resetBuscador").attr("onclick","resetBuscador()");
$("#btnAutoAsign").on("click",function(){
	llamarSugeridosAsignar();
});

$("#btnAsignarCombo").on("click",function(){
	
var indexCombo=$("input[name=tipoCombo]:checked").val();
var listaDetallesCombo=combosSugeridos[indexCombo].detalles;
for(var index2=0;index2<listaDetallesCombo.length;index2++){
	var dataParam = {
			ipercDetailId : detalleIpercId,
			ipercDetailEvalID :(index2==0?detalleIpercDetailId:0) ,
			descripcionLugar : listaDetallesCombo[index2].descripcionLugar,
			descripcionPeligro : listaDetallesCombo[index2].descripcionPeligro,
			peligroName : listaDetallesCombo[index2].peligroName,
			idClasificacion : listaDetallesCombo[index2].idClasificacion,
			riesgoName : listaDetallesCombo[index2].riesgoName,
			consecuencia1 : listaDetallesCombo[index2].consecuencia1,
			controlInicial1:listaDetallesCombo[index2].controlInicial1,
			controlInicial2:listaDetallesCombo[index2].controlInicial2,
			controlInicial3:listaDetallesCombo[index2].controlInicial3,
			lastInsert:(index2==listaDetallesCombo.length-1?1:0)
		};

		callAjaxPostNoLoad(URL + '/iperc/detail/save', dataParam,
				function (data) {
			switch (data.CODE_RESPONSE) {
			case "05":
	if(data.lastInsert==1){
		listarDetalleIperc();
	}
	
				break;
			default:
				console.log("N");
			}
		});
		if(index2==listaDetallesCombo.length-1){
			$("#modalDetalleSugerido").modal("hide");
			
		}
}

	
	
	
	
});


$("#btnGestionCombo").on("click",function(){
	$(".modal-body").hide();
	$("#bodyGestion").show();
	$(".modal-title").html("<a onclick='volverAsignaciones()'>Combos Sugeridos</a> > Gestión de Combos ");
	llamarSugeridosEmpresa();
	
});

});

function llamarSugeridosAsignar(){
	$(".modal-body").hide();
	$("#bodyAsignar").show();
	$(".modal-title").html("Combos Sugeridos ");
	callAjaxPostNoLoad(URL+"/iperc/detalle/sugeridos",
			{companyId : sessionStorage.getItem("gestopcompanyid")}
	,function(data){
		 combosSugeridos=data.combosSugeridos;
		 $('#modalDetalleSugerido').modal({
			  keyboard: true,
			  show:true,
			  "backdrop":"static"
			});
		$("#listaCombos").html("");
		for(var index=0;index<combosSugeridos.length;index++){
			$("#listaCombos").append(
					"<div class='radio radio-primary radio-inline' id='divCombo"+combosSugeridos[index].id+"'>"
					+"<input type='radio' " 
						+	"name='tipoCombo' id='combo"+combosSugeridos[index].id+"' " 
						+	"value='"+index+"' >"
		            +"<label for='combo"+combosSugeridos[index].id+"'>"
		            +combosSugeridos[index].nombre+"&nbsp; " 
		            		
		                +"     </label>"+"<span><i class='fa fa-question-circle' aria-hidden='true'></i></span>"
		       +" </div><br>"
			);
		$("#divCombo"+combosSugeridos[index].id+" span").on("click",combosSugeridos[index].detalles,function(event){
			$("#modalListComboDetalle .modal-body").show();
			
			var contenidoTabla="";
			
			var detail=event.data;
			detail.forEach(function(val,index1){
				 
				contenidoTabla=contenidoTabla+"<tr><td> "+val.descripcionLugar+"</td>"+
				
				"<td>"+val.clasificacionName+"</td>"+
				"<td> "+val.peligroName+"</td>"+
				"<td> "+val.descripcionPeligro+"</td>"+
				"<td> "+val.riesgoName+"</td>"+
				"<td>"+val.consecuencia1+"</td>" +
				"<td>"+val.controlInicial1+"</td>"+
				"<td>"+val.controlInicial2+"</td>"+
				"<td>"+val.controlInicial3+"</td>"+
						"</tr>"
			});
			var tablaAyuda="<table class='table table-striped table-bordered table-hover'>" +
			"<thead>" +
			"<tr>" +
			"<td class='tb-acc'>Lugar</td>" +
			"<td class='tb-acc'>Clasificacion</td>" +
			"<td class='tb-acc'>Peligro</td>" +
			"<td class='tb-acc'>Descripciónn</td>" +
"<td class='tb-acc'>Riesgo</td>" +
			"<td class='tb-acc'>Consecuencia</td>" +
			"<td class='tb-acc'>Control Administrativo</td>" +
			"<td class='tb-acc'>Control Ingeniería</td>" +
			"<td class='tb-acc'>Control Equipo Seguridad</td>" +
			"</tr>" +
			"</thead>" +
			"<tbody>"+contenidoTabla+"</tbody>" +
			"</table>";		
		
		$("#modalListComboDetalle").modal("show");
		$("#modalListComboDetalle").css({"z-index":"1051"});
		$("#modalListComboDetalle .modal-body").html(tablaAyuda);
		});
		}
		
	});
}
function volverAsignaciones(){
	$(".modal-body").hide();
	$("#bodyAsignar").show();
	$(".modal-title").html("Combos Sugeridos ");
	llamarSugeridosAsignar()
}
function resetBuscador(){
	$("#buscadorTrabajadorInput").val("");
	listarDetalleIperc();
}
$('#exampleModal').on(
		'show.bs.modal',
		function(e) {
			$('#chckProbEsp').prop('checked', idProbEspe);
			$('#chkbCtrl').prop('checked', idFlagEvalIni);
			$('#chckFrecPel').prop('checked', idEvalFrec);
		
			
			
			$("#selectCons").val(indicadorCons);

			$("#selectNR").remove();
			$("#scmbRiesgo").append(
					crearSelectOneMenu("selectNR", "comprobarNivelRiesgo()", nivelesRiesgos,
							idMatrizRiesgo, "mrId", "mrNombre"));
			comprobarNivelRiesgo();
		});
$('#modalAyudaIperc').on(
		'hide.bs.modal',
		function(e) {
			$('#exampleModal').modal("show");
		});
/** *************************************************** */
function comprobarNivelRiesgo(){
	
	var mdfId=$("#selectNR").val();
	if(mdfId!=1){
		$('#chckProbEsp').prop('checked', false);
		$("#chckProbEsp").prop('disabled', true);
	}else{
		$('#chckProbEsp').prop('checked', idProbEspe);
		$("#chckProbEsp").prop('disabled', false);
		
	}
	
	if(mdfId!=5){
		$('#chckFrecPel').prop('checked', false);
		$("#chckFrecPel").prop('disabled', true);
	}else{
		$('#chckFrecPel').prop('checked', idEvalFrec);
		$("#chckFrecPel").prop('disabled', false);
		
	}
	
	
	
}
function listarDetalleIperc() {
	banderaEdicion = false;
	$("#btnClipTabla").show();
	$("#h2Titulo").html("Detalle Iperc de" + mdfNombreAux);
	indexAux=0;
	detalleIpercId = 0;
	detalleIpercDetailId = 0;
	peligroId = 0;
	clasiId = 0;
	riesgoId = 0;
	probabilidadId = 0;
	severidadId = 0;
	probabilidadActId = 0;
	severidadActId = 0;
	probabilidadProyId = 0;
	severidadProyId = 0;
	 personExpId=0;
	 indProcId=0;
	 indCapaId=0;
	 indRiskId=0;

	 personExpIdAct=0;
	 indProcIdAct=0;
	 indCapaIdAct=0;
	 indRiskIdAct=0;

	 personExpIdFut=0;
	 indProcIdFut=0;
	 indCapaIdFut=0;
	 indRiskIdFut=0;
	tipoCtrlId = 0;
	
	frecPelPuroId=0;
	frecPelActId=0;
	frecPelFutId=0;
	 frecPelPuroId=0;
	  frecPelActId=0;
	  frecPelFutId=0;
	ipercDetalleEvalIdCopy = 0;
	detallesFullIperc=[];
	listSeveridad = null;
	listProbabilidad = null;
	listNivelRiesgo = null;

	 idRiesgoEspecial=null;
	 idRiesgoActEspecial=null;
	 idRiesgoFutEspecial=null;
	deshabilitarBotonesEdicion();
	$("#btnDetalle").hide();
	$("#btnNuevo").hide();
	$("#btnCopiar").hide();
	$("#btnAutoAsign").hide();
	$("#btnNuevo").attr("onclick", "nuevoDetalleIperc()");

	$("#btnCancelar").attr("onclick", "cancelarDetalleIperc()");
	$("#btnGuardar").attr("onclick", "guardarDetalleIperc()");
	$("#btnEliminar").attr("onclick", "eliminarDetalleIperc()");

	$("#btnConfigurar").show();
	$("#btnDescargar").show();
	
	$("#btnOkConf").attr("onclick", "configurarIperc()");

	listarTablaDetalleIperc();
	
	
	var dataParamAux = {
			ipercId : idIPERCEdicion,
		matrizRiesgoId : idMatrizRiesgo
	};
	callAjaxPostNoLoad(URL+"/iperc/detalle/controles",dataParamAux,
			function(data){
		switch (data.CODE_RESPONSE) {
		case "05":
		 listFullControles="";
		var list=data.list;
		
		listFullControles="Detalle" +"\t"
		+"Lugar" +"\t"
		+"Clasificación de peligro" +"\t"
		+"Peligro" +"\t"
		+"Descripción Peligro" +"\t"
		
		+"Tipo de Control" +"\t"
		+"Descripción de Control" +"\t"
		+"Responsable" +"\t"
		+"Fecha planificada" +"\t"
		+"Fecha realizada" +"\t"
		+"Costo" +"\t"
		+"Estado" 
		+" \n";
		for (var index = 0; index < list.length; index++){
			var listControles=list[index].controlesAsociados;
			for (var index2 = 0; index2 < listControles.length; index2++){
				
				if(listControles[index2].estadoNombre!=null){
					listFullControles=listFullControles
					+list[index].ipercDetailName+" \t "
					+list[index].descripcionLugar+" \t "
					+list[index].clasificacionName+" \t "
					+list[index].peligroName+" \t "
					+list[index].descripcionPeligro.trim()+" \t "
					
					+listControles[index2].responsableTipoNombre+" \t "
					+listControles[index2].controlNombre+" \t "
					+listControles[index2].responsable+ " \t "
					+listControles[index2].fecha+" \t "
					+listControles[index2].fechaReal+" \t " 
					+listControles[index2].costo+" \t "
					+listControles[index2].estadoNombre+"  "
					+"\n"
				}
				

			}
		

		}
		break;
		default:
		console.log("NOPPPPP");
		break;
		}
		
	})
}

function mostrarOpcSegunTipoDO(tipoDO) {
	switch (tipoDO) {
	case 5:
		$("#btnActividad").hide();
		$("#btnTarea").hide();
		$("#btnPaso").hide();
		ocultarColumna(5, "tblMatrices");
		ocultarColumna(6, "tblMatrices");
		break;
	case 6:
		$("#btnActividad").show();
		$("#btnTarea").show();
		$("#btnPaso").hide();
		ocultarColumna(6, "tblMatrices");
		break;
	case 7:
		$("#btnActividad").show();
		$("#btnTarea").show();
		$("#btnPaso").show();
		break;
	}
}

function listarTablaDetalleIperc() {
	var palabraClave=$("#buscadorTrabajadorInput").val();
	if(palabraClave.length==0){
		palabraClave=null;
	}else{
		palabraClave=("%"+palabraClave+"%").toUpperCase();
	}
	$("#buscadorIperc").show();
	$("#tblMatrices").width('3500px');

	var dataParam = {
			ipercId : idIPERCEdicion,
		matrizRiesgoId : idMatrizRiesgo,
		palabraClave:palabraClave
	};

	callAjaxPost(URL + '/iperc/detalle', dataParam,
			procesarListarTablaDetalleIperc);

}

function estadoControl(numRetrasados, numImplementar, numCompletados) {
	var color = "";
	var icono = "";
	var propiedades;
	var mensaje = "";
	mensaje = "<br>";
	if (numImplementar > 0 && numRetrasados == 0) {
		mensaje = mensaje + numImplementar + "I";
		color = "#FF8000";

		icono = ' <i class="fa fa-eye"></i>';
	}

	if (numRetrasados > 0) {
		mensaje = mensaje + numRetrasados + "R ";
		icono = ' <i class="fa fa-exclamation-circle"></i>';
		color = "red";

	}

	if (numRetrasados == 0 && numImplementar == 0 && numCompletados > 0) {
		color = "green";

		icono = ' <i class="fa fa-check-square"></i>';

	}
	propiedades = {
		color : color,
		icono : icono,
		mensaje : mensaje
	}
	return propiedades

}

function procesarListarTablaDetalleIperc(data) {

	switch (data.CODE_RESPONSE) {
	case "05":
		var columnaEvalIni = "";
		var columnaEvalIni2 = "";
		var columnaTh1Consecuencia = "";
		var columnaTh2Consecuencia = "";
		var columnaEvalucionEspecifica="";
		var columnaProbabilidad="";
		var columnaFrecuencia="";
		
		var anchoEvaluacion;
		var anchoEvaluacionPx;
		var anchoEvaluacionActPx;
		var anchoEvaluacionFutPx;
		var anchoEvaluacion2;
		var list = data.list;
		 detallesFullIperc=data.list;
		 
		var listnum = data.numControles;
		
		var listNumAux=[];
		for(var index1=0;index1<listnum.length;index1++){
			for(var index2=0;index2<list.length;index2++){
				if(listnum[index1].ipercDetailEvalID==list[index2].ipercDetailEvalID){
					listNumAux.push(listnum[index1]);
					
					
				}
				
			}
			
		}
		listnum=listNumAux;
		var mapRiesgo = data.mapRiesgo;
		listSeveridad = mapRiesgo.severidades;
		listProbabilidad = mapRiesgo.probabilidades;
		listNivelRiesgo = mapRiesgo.nivelRiesgos;
		var detalleAux ;
		var longitudAux ;
		if(detallesFullIperc.length>0){
			if(list[0].ipercDetailName){
				 detalleAux = list[0].ipercDetailName.split(' - ');
				 longitudAux = list[0].longitudTotalDetalle;
			}
		}
	
		
		$("#tblMatrices tbody tr").remove();
		$("#tblMatrices thead tr").remove();
		var nombreMDF = mdfNombreAux.split('-');
		$("#h2Titulo").html("Detalle IPERC de " + nombreMDF[0]);
		columnaProbabilidad = "<th style='text-align:center;vertical-align:middle;width:100px'>Probabilidad</th>";
		anchoEvaluacion=3;
		anchoEvaluacion2=4;
		 anchoEvaluacionPx="500px";
		 anchoEvaluacionActPx="600px";
		 anchoEvaluacionFutPx="600px";
		if(idMatrizRiesgo==1){
		if(idProbEspe==0){
			columnaProbabilidad = "<th style='text-align:center;vertical-align:middle;width:100px'>Probabilidad</th>";
			anchoEvaluacion=3;
			anchoEvaluacion2=4;
			 anchoEvaluacionPx="500px";
			 anchoEvaluacionActPx="600px";
			 anchoEvaluacionFutPx="600px";
		}else{
			anchoEvaluacion=7;
			anchoEvaluacion2=8;
			 anchoEvaluacionPx="900px";
			 anchoEvaluacionActPx="1100px";
			 anchoEvaluacionFutPx="1100px";
			columnaProbabilidad = "<th style='text-align:center;vertical-align:middle;width:70px'>Personas Expuestas</th>"
				+"<th style='text-align:center;vertical-align:middle;width:90px'>Procedimiento</th>"
				+"<th style='text-align:center;vertical-align:middle;width:80px'>Capacitación</th>"
				+"<th style='text-align:center;vertical-align:middle;width:130px'>Exposición al Riesgo</th>"
				+"<th style='text-align:center;vertical-align:middle;width:100px'>Probabilidad</th>";
			
		}
		}
		
		
		if(idMatrizRiesgo==5){
		if(idEvalFrec==0){
		
			columnaProbabilidad = "<th style='text-align:center;vertical-align:middle;width:100px'>Probabilidad</th>";
			anchoEvaluacion=3;
			anchoEvaluacion2=4;
			 anchoEvaluacionPx="500px";
			 anchoEvaluacionActPx="600px";
			 anchoEvaluacionFutPx="600px";	 
		}else{
			
			columnaProbabilidad = "<th style='text-align:center;vertical-align:middle;width:100px'>Probabilidad</th>";
			columnaFrecuencia="<th style='text-align:center;vertical-align:middle;width:100px'>Frecuencia</th>";
			anchoEvaluacion=4;
			anchoEvaluacion2=5;
			 anchoEvaluacionPx="650px";
			 anchoEvaluacionActPx="750px";
			 anchoEvaluacionFutPx="750px";
		}
		
		}
		
		if (idFlagEvalIni) {
			columnaEvalIni = "<th class='tb-acc' colspan='"+anchoEvaluacion+"' style='text-align:center;vertical-align:middle;" +
					"width:"+anchoEvaluacionPx+"' id='tituloEvalPuro'>Evaluacion del Riesgo Puro</th>";
			columnaEvalIni2 = columnaFrecuencia+columnaProbabilidad
					+ "<th class='tb-acc' style='text-align:center;vertical-align:middle;width:70px;'>Severidad</th>"
					+ "<th class='tb-acc' style='text-align:center;vertical-align:middle;width:70px;'>Nivel de Riesgo</th>";
		}

		if (indicadorCons == 1) {
			columnaTh1Consecuencia = "<th class='tb-acc' rowspan='2' style='text-align:center;vertical-align:middle;width:300px;'>Consecuencias</th>";
		} else if (indicadorCons == 2) {
			columnaTh1Consecuencia = "<th colspan='3' style='text-align:center;vertical-align:middle;width:1000px;'>Consecuencias</th>";
			columnaTh2Consecuencia = "<th class='tb-acc' style='text-align:center;vertical-align:middle;width:320px;'>Consecuencia 1</th>"
					+ "<th class='tb-acc' style='text-align:center;vertical-align:middle;width:320px;'>Consecuencia 2</th>"
					+ "<th class='tb-acc' style='text-align:center;vertical-align:middle;width:320px;'>Consecuencia 3</th>"
		}
		var cabeceraAux="<th class='tb-acc' rowspan='2' style='text-align:center;vertical-align:middle;width:120px;'>Lugar</th>"
		+ "<th class='tb-acc' rowspan='2' style='text-align:center;vertical-align:middle;width:100px;'>Clase de Peligro</th>"
		+ "<th class='tb-acc' rowspan='2' style='text-align:center;vertical-align:middle;width:300px;'>Peligro</th>"
		+ "<th class='tb-acc' rowspan='2' style='text-align:center;vertical-align:middle;width:300px;'>Descripci&oacute;n del Peligro</th>"
		+ "<th class='tb-acc' rowspan='2' style='text-align:center;vertical-align:middle;width:300px;'>Riesgo</th>"
		+ columnaTh1Consecuencia

		+ columnaEvalIni

		+ "<th colspan='4' style='text-align:center;vertical-align:middle;width:1020px;'>Controles Existentes</th>"
		+ "<th colspan='"+anchoEvaluacion2+"' style='text-align:center;vertical-align:middle;width:"+anchoEvaluacionActPx+"'' id='tituloEvalAct'>Evaluacion del Riesgo Actual</th>"

		+ "<th colspan='5' style='text-align:center;vertical-align:middle;width:620px;'>Controles a Implementar</th>"
		+ "<th colspan='"+anchoEvaluacion2+"' style='text-align:center;vertical-align:middle;width:"+anchoEvaluacionFutPx+"'' id='tituloEvalFuturo'>Evaluacion del Riesgo Residual Proyectado</th>"

		+ "</tr>"

		+ columnaTh2Consecuencia

		+ columnaEvalIni2

		+ "<th class='tb-acc' style='text-align:center;vertical-align:middle;width:270px;'>1. Control Administrativo</th>"
		+ "<th class='tb-acc' style='text-align:center;vertical-align:middle;width:270px;'>2. Control Ingenieria</th>"

		+ "<th class='tb-acc' style='text-align:center;vertical-align:middle;width:270px;'>3. Equipos de Seguridad</th>"
		+ "<th class='tb-acc' >Eficacia</th>"

		+columnaFrecuencia+columnaProbabilidad
		+ "<th class='tb-acc' style='text-align:center;vertical-align:middle;width:70px;'>Severidad</th>"
		+ "<th class='tb-acc' style='text-align:center;vertical-align:middle;width:70px;'>Nivel de Riesgo</th>"
		+ "<th class='tb-acc' style='text-align:center;vertical-align:middle;width:120px;'>Riesgo Significativo</th>"

		+ "<th class='tb-acc' style='text-align:center;vertical-align:middle;'>1. Eliminaci&oacute;n</th>"
		+ "<th class='tb-acc' style='text-align:center;vertical-align:middle;'>2. Sustituci&oacute;n</th>"
		+ "<th class='tb-acc' style='text-align:center;vertical-align:middle;'>3. Control de Ingenieria</th>"
		+ "<th class='tb-acc' style='text-align:center;vertical-align:middle;'>4. Control Administrativo</th>"
		+ "<th class='tb-acc' style='text-align:center;vertical-align:middle;'>5. Equipos de Proteccion</th>"

		+columnaFrecuencia+columnaProbabilidad
		+ "<th style='text-align:center;vertical-align:middle;width:70px'>Severidad</th>"
		+ "<th style='text-align:center;vertical-align:middle;width:70px'>Nivel de Riesgo</th>"
		+ "<th class='tb-acc' style='text-align:center;vertical-align:middle;width:120px;'>Riesgo Significativo</th>";
		
		var cabecera7 = "<tr  style='background-color: #16365C; color: white;'>"
				+ "<th class='tb-acc' rowspan='2' style='text-align:center;vertical-align:middle;width:50px;'>Nro</th>"
				+ "<th class='tb-acc' rowspan='2' style='text-align:center;vertical-align:middle;width:150px'>Area</th>"
				+ "<th class='tb-acc' rowspan='2' style='text-align:center;vertical-align:middle;width:150px'>Puesto</th>"
				// Aca debo modificar

				+ "<th class='tb-acc' rowspan='2' style='text-align:center;vertical-align:middle;width:150px;'>Actividad</th>"
				+ "<th class='tb-acc' id='tareaAux' rowspan='2' style='text-align:center;vertical-align:middle;width:150px;'>Tarea</th>"
				+ "<th class='tb-acc' id='pasoAux' rowspan='2' style='text-align:center;vertical-align:middle;width:150px;'>Paso</th>"+cabeceraAux;

				

		var cabecera6 = "<tr  style='background-color: #16365C; color: white;'>"
				+ "<th class='tb-acc' rowspan='2' style='text-align:center;vertical-align:middle;width:50px;'>Nro</th>"
				+ "<th class='tb-acc' rowspan='2' style='text-align:center;vertical-align:middle;width:150px'>Area</th>"
				+ "<th class='tb-acc' rowspan='2' style='text-align:center;vertical-align:middle;width:150px'>Puesto</th>"
				// Aca debo modificar

				+ "<th class='tb-acc' rowspan='2' style='text-align:center;vertical-align:middle;width:150px;'>Actividad</th>"
				+ "<th class='tb-acc' id='tareaAux' rowspan='2' style='text-align:center;vertical-align:middle;width:150px;'>Tarea</th>"

				+cabeceraAux;

		var cabecera5 = "<tr  style='background-color: #16365C; color: white;'>"
				+ "<th class='tb-acc' rowspan='2' style='text-align:center;vertical-align:middle;width:50px;'>Nro</th>"
				+ "<th class='tb-acc' rowspan='2' style='text-align:center;vertical-align:middle;width:150px'>Area</th>"
				+ "<th class='tb-acc' rowspan='2' style='text-align:center;vertical-align:middle;width:150px'>Puesto</th>"
				// Aca debo modificar

				+ "<th class='tb-acc' rowspan='2' style='text-align:center;vertical-align:middle;width:150px;'>Actividad</th>"

				+cabeceraAux;
		
		switch (longitudAux) {
		case 7:

			$("#tblMatrices thead").append(cabecera7);

			break;
		case 6:

			$("#tblMatrices thead").append(cabecera6);

			break;
		case 5:

			$("#tblMatrices thead").append(cabecera5);

			break;

		}
		$("#tblMatrices thead tr th").addClass("tb-acc");
		$("#tblEmpresas tbody tr").remove();

		for (var index = 0; index < list.length; index++) {
			var contador = index + 1;
			var evaluacionTresFilas = "";
			var consecuenciasFilas = "";
			var probabilidadFilasPuro="";
			var probabilidadFilasActual="";
			var probabilidadFilasFuturo="";
			
			var frecuenciaPura="";
			var frecuemciaActual="";
			var frecuenciaFutura="";
			if (indicadorCons == 1) {
				consecuenciasFilas = "<td id='cons1"
						+ list[index].ipercDetailEvalID + "'>"
						+ list[index].consecuencia1 + "</td>";
			} else if (indicadorCons == 2) {
				consecuenciasFilas = "<td id='cons1"
						+ list[index].ipercDetailEvalID + "'>"
						+ list[index].consecuencia1 + "</td>" + "<td id='cons2"
						+ list[index].ipercDetailEvalID + "'>"
						+ list[index].consecuencia2 + "</td>" + "<td id='cons3"
						+ list[index].ipercDetailEvalID + "'>"
						+ list[index].consecuencia3 + "</td>";
			}

			

			if(idProbEspe==0){
				probabilidadFilasPuro = "<td id='evainiprob"
					+ list[index].ipercDetailEvalID + "'>"
					+ list[index].probabilidadNombre + "</td>";
				probabilidadFilasActual =  "<td id='evaactprob"
					+ list[index].ipercDetailEvalID
					+ "'>"
					+ list[index].probabilidadActNombre
					+ "</td>";
				probabilidadFilasFuturo = "<td id='evaproyprob"
					+ list[index].ipercDetailEvalID
					+ "'>"
					+ list[index].probabilidadProyNombre
					+ "</td>";
				
				
			
				
			}else{
				
				probabilidadFilasPuro = "<td id='persExp"
					+ list[index].ipercDetailEvalID + "'>"
					+ list[index].nombreProbPersonas + "</td>" + "<td id='probProc"
					+ list[index].ipercDetailEvalID + "'>"
					+ list[index].nombreProbProcedimiento + "</td>" + "<td id='probCapa"
					+ list[index].ipercDetailEvalID + "'>"
					+ list[index].nombreProbCapacitacion + "</td>"
					+ "<td id='probRiskExp"
					+ list[index].ipercDetailEvalID + "'>"
					+ list[index].nombreProbExpoRiesgo + "</td>"
					+ "<td id='evainiprob"
					+ list[index].ipercDetailEvalID + "'>"
					+ list[index].probabilidadNombre + "</td>";
				
				probabilidadFilasActual = "<td id='persExpAct"
					+ list[index].ipercDetailEvalID + "'>"
					+ list[index].nombreProbPersonasAct + "</td>" + "<td id='probProcAct"
					+ list[index].ipercDetailEvalID + "'>"
					+ list[index].nombreProbProcedimientoAct + "</td>" + "<td id='probCapaAct"
					+ list[index].ipercDetailEvalID + "'>"
					+ list[index].nombreProbCapacitacionAct + "</td>"
					+ "<td id='probRiskExpAct"
					+ list[index].ipercDetailEvalID + "'>"
					+ list[index].nombreProbExpoRiesgoAct + "</td>"
					+"<td id='evaactprob"
					+ list[index].ipercDetailEvalID
					+ "'>"
					+ list[index].probabilidadActNombre
					+ "</td>";
				
				probabilidadFilasFuturo = "<td id='persExpFut"
					+ list[index].ipercDetailEvalID + "'>"
					+ list[index].nombreProbPersonasFut + "</td>" + "<td id='probProcFut"
					+ list[index].ipercDetailEvalID + "'>"
					+ list[index].nombreProbProcedimientoFut + "</td>" + "<td id='probCapaFut"
					+ list[index].ipercDetailEvalID + "'>"
					+ list[index].nombreProbCapacitacionFut + "</td>"
					+ "<td id='probRiskExpFut"
					+ list[index].ipercDetailEvalID + "'>"
					+ list[index].nombreProbExpoRiesgoFut + "</td>"
					+"<td id='evaproyprob"
					+ list[index].ipercDetailEvalID
					+ "'>"
					+ list[index].probabilidadProyNombre
					+ "</td>";
				
				
				
			}
			if(idEvalFrec){
				frecuenciaPura="<td id='frecPelPuro"+list[index].ipercDetailEvalID+"'>" 
				+ list[index].frecuenciaPeligroPuraNombre + "</td>";
				frecuemciaActual="<td id='frecPelAct"+list[index].ipercDetailEvalID+"'>" 
				+ list[index].frecuenciaPeligroActNombre + "</td>";
				frecuenciaFutura="<td id='frecPelFut"+list[index].ipercDetailEvalID+"'>" 
				+ list[index].frecuenciaPeligroFutNombre + "</td>";
				
			}
			if (idFlagEvalIni) {
				evaluacionTresFilas =frecuenciaPura+ probabilidadFilasPuro
						+ "<td id='evainisev" + list[index].ipercDetailEvalID
						+ "'>" + list[index].severidadNombre + "</td>"
						+ "<td id='evaininiv" + list[index].ipercDetailEvalID
						+ "' style='background-color:"
						+ list[index].nivelRiesgoColor + ";font-weight:bold' >"
						+ "" + list[index].nivelRiesgoNombre + "</td>";
			}
			
			if(list[index].ipercDetailName){
				
				var detalleAux = list[index].ipercDetailName.split(' - ');
				var actividadAux = detalleAux[4];
				var tareaAux = detalleAux[5];
				var pasoAux = detalleAux[6];

				var puestoAux = detalleAux[3];
				var areaAux = detalleAux[2];
			

				var propiedades1 = estadoControl(listnum[index].numRetrasado1,
						listnum[index].numImplementar1,
						listnum[index].numCompletado1);
				var propiedades2 = estadoControl(listnum[index].numRetrasado2,
						listnum[index].numImplementar2,
						listnum[index].numCompletado2);
				var propiedades3 = estadoControl(listnum[index].numRetrasado3,
						listnum[index].numImplementar3,
						listnum[index].numCompletado3);
				var propiedades4 = estadoControl(listnum[index].numRetrasado4,
						listnum[index].numImplementar4,
						listnum[index].numCompletado4);
				var propiedades5 = estadoControl(listnum[index].numRetrasado5,
						listnum[index].numImplementar5,
						listnum[index].numCompletado5);
				var colorControles="#81F7F3"
				var cuerpoAux="<td id='lugar"
				+ list[index].ipercDetailEvalID
				+ "'>"
				+ list[index].descripcionLugar
				+ "</td>"
				+ "<td id='clasi"
				+ list[index].ipercDetailEvalID
				+ "'>"
				+ list[index].clasificacionName
				+ "</td>"
				+ "<td id='pel"
				+ list[index].ipercDetailEvalID
				+ "'>"
				+ list[index].peligroName
				+ "</td>"
				+ "<td id='despel"
				+ list[index].ipercDetailEvalID
				+ "'>"
				+ list[index].descripcionPeligro
				+ "</td>"
				+ "<td id='riesgo"
				+ list[index].ipercDetailEvalID
				+ "'>"
				+ list[index].riesgoName
				+ "</td>"

				+ consecuenciasFilas

				+ evaluacionTresFilas

				+ "<td   id='ctrlini1"
				+ list[index].ipercDetailEvalID
				+ "'>"
				+ list[index].controlInicial1
				+ "</td>"
				+ "<td   id='ctrlini2"
				+ list[index].ipercDetailEvalID
				+ "'>"
				+ list[index].controlInicial2
				+ "</td>"
				+ "<td   id='ctrlini3"
				+ list[index].ipercDetailEvalID
				+ "'>"
				+ list[index].controlInicial3
				+ "</td>"
				+ "<td id='tdeficaz"
				+ list[index].ipercDetailEvalID
				+ "'>"
				+ list[index].eficaz
				+ " %</td>"
				+frecuemciaActual
				+probabilidadFilasActual
				+ "<td id='evaactsev"
				+ list[index].ipercDetailEvalID
				+ "'>"
				+ list[index].severidadActNombre
				+ "</td>"
				+ "<td id='evaactniv"
				+ list[index].ipercDetailEvalID
				+ "' style='background-color:"
				+ list[index].nivelRiesgoActColor
				+ ";font-weight:bold' >"
				+ list[index].nivelRiesgoActNombre
				+ "</td>"
				+ "<td id='aceptableact"
				+ list[index].ipercDetailEvalID
				+ "'>"
				+ pasarBoolPalabra(list[index].ipercDetailEvalRiskAct)
				+ "</td>"

				+ "<td id='ctrlImpl1"
				+ list[index].ipercDetailEvalID
				+ "' style='color:"
				+ propiedades1.color
				+ ";font-weight:bold'>"
				+ devolverTagCtrl(listnum[index].numTipo1)
				+ ""
				+ propiedades1.icono
				+ "</td>"
				+ "<td id='ctrlImpl2"
				+ list[index].ipercDetailEvalID
				+ "' style='color:"
				+ propiedades2.color
				+ ";font-weight:bold'>"
				+ devolverTagCtrl(listnum[index].numTipo2)
				+ ""
				+ propiedades2.icono
				+ "</td>"
				+ "<td id='ctrlImpl3"
				+ list[index].ipercDetailEvalID
				+ "' style='color:"
				+ propiedades3.color
				+ ";font-weight:bold'>"
				+ devolverTagCtrl(listnum[index].numTipo3)
				+ ""
				+ propiedades3.icono
				+ "</td>"
				+ "<td id='ctrlImpl4"
				+ list[index].ipercDetailEvalID
				+ "' style='color:"
				+ propiedades4.color
				+ ";font-weight:bold'>"
				+ devolverTagCtrl(listnum[index].numTipo4)
				+ ""
				+ propiedades4.icono
				+ "</td>"
				+ "<td id='ctrlImpl5"
				+ list[index].ipercDetailEvalID
				+ "' style='color:"
				+ propiedades5.color
				+ ";font-weight:bold'>"
				+ devolverTagCtrl(listnum[index].numTipo5)
				+ ""
				+ propiedades5.icono
				+ "</td>"
				+frecuenciaFutura
				+ probabilidadFilasFuturo
				+ "<td id='evaproysev"
				+ list[index].ipercDetailEvalID
				+ "' >"
				+ list[index].severidadProyNombre
				+ "</td>"
				+ "<td id='evaproyniv"
				+ list[index].ipercDetailEvalID
				+ "' style='background-color:"
				+ list[index].nivelRiesgoProyColor
				+ ";font-weight:bold'>"
				+ list[index].nivelRiesgoProyNombre
				+ "</td>"
				+ "<td id='aceptablefut"
				+ list[index].ipercDetailEvalID
				+ "'>"
				+ pasarBoolPalabra(list[index].ipercDetailEvalRiskProy)
				+ "</td>";
				
				switch (longitudAux) {
				case 7:

					$("#tblMatrices tbody")
							.append(
									"<tr id='trm"
											+ list[index].ipercDetailEvalID
											+ "' onclick='javascript:editarIpercDetalle("
											
											+ index
											+ ")'>"

											+ "<td>"
											+ contador
											+ "</td>"
											+ "<td >"
											+ areaAux
											+ "</td>"
											+ "<td >"
											+ puestoAux
											+ "</td>"
											+ "<td >"
											+ actividadAux
											+ "</td>"
											+ "<td >"
											+ tareaAux
											+ "</td>"
											+ "<td >"
											+ pasoAux
											+ "</td>"
												+cuerpoAux
											 + "</tr>");
					break;
				case 6:

					$("#tblMatrices tbody")
							.append(
									"<tr id='trm"
											+ list[index].ipercDetailEvalID
											+ "' onclick='javascript:editarIpercDetalle("
											
											+ index
											+ ")'>"

											+ "<td>"
											+ contador
											+ "</td>"
											+ "<td >"
											+ areaAux
											+ "</td>"
											+ "<td >"
											+ puestoAux
											+ "</td>"
											+ "<td >"
											+ actividadAux
											+ "</td>"
											+ "<td >"
											+ tareaAux
											+ "</td>"
											+cuerpoAux
											+ "</tr>");
					break;
				case 5:

					$("#tblMatrices tbody")
							.append(
									"<tr id='trm"
											+ list[index].ipercDetailEvalID
											+ "' onclick='javascript:editarIpercDetalle("
											
											+ index
											+ ")'>"

											+ "<td>"
											+ contador
											+ "</td>"
											+ "<td >"
											+ areaAux
											+ "</td>"
											+ "<td >"
											+ puestoAux
											+ "</td>"
											+ "<td >"
											+ actividadAux
											+ "</td>"
												+cuerpoAux
											 + "</tr>");
					break;

				}
			}
			

		}

		formatoCeldaSombreable(true);
		$("#tblMatrices").addClass("fixed");
		$("#tblMatrices tbody tr").css("font-size","12px");
		if(getSession("linkCalendarioDetalleIpercId")!=null){
			
			detallesFullIperc.every(function(val,index3){
				
				if(val.ipercDetailEvalID==parseInt(getSession("linkCalendarioDetalleIpercId"))){
					editarIpercDetalle(index3);
					verControlSegunTipo(parseInt(getSession("linkCalendarioControlTipoId")));
					
					sessionStorage.removeItem("linkCalendarioDetalleIpercId");
					sessionStorage.removeItem("linkCalendarioControlTipoId");
					return false;
				}else{
					return true;
				}
			});
			
		}
		
		break;
	default:
		alert("Ocurrió un error al traer las evaluaciones!");
	}

	
		goheadfixed('table.fixed');
	
}
function devolverTagCtrl(nroControles) {
	var auxNum = parseInt(nroControles);
	var tagCtrl;

	if (auxNum > 1) {
		tagCtrl = auxNum + " Controles";
	} else {
		tagCtrl = auxNum + " Control";

	}
	if (auxNum == 0) {
		tagCtrl = "";
	}
	return tagCtrl;

}

function editarIpercDetalle(pindex,isDuplicado) {
	if (!banderaEdicion) {
		$("#btnVolverIPERC").hide();
		var nombreMDF = mdfNombreAux.split('-');
		var detalleAux = detallesFullIperc[pindex].ipercDetailName.split(' - ');
		var actividadAux = detalleAux[4];
		var tareaAux = (detalleAux[5]==null?"":"-"+detalleAux[5]);
		var pasoAux = (detalleAux[6]==null?"":"-"+detalleAux[6]);

		var puestoAux = detalleAux[3];
		var areaAux = detalleAux[2];
		$("#h2Titulo").html("Detalle IPERC de " + nombreMDF[0] +" " +
				"en <strong>"+areaAux+"-"+puestoAux+"-"+actividadAux+tareaAux+pasoAux+"</strong>");
		isDuplicado = isDuplicado || false;
		formatoCeldaSombreable(false);
		detalleIpercDetailId = detallesFullIperc[pindex].ipercDetailEvalID;
		detalleIpercId = detallesFullIperc[pindex].ipercDetailId;
		peligroId = detallesFullIperc[pindex].idPeligro;
		clasiId =(isDuplicado?"-1": detallesFullIperc[pindex].idClasificacion);
		riesgoId = detallesFullIperc[pindex].idRiesgo;
		probabilidadId = (isDuplicado?"-1":detallesFullIperc[pindex].probabilidadId);
		severidadId =(isDuplicado?"-1": detallesFullIperc[pindex].severidadId);
		eficacia =(isDuplicado?0: detallesFullIperc[pindex].eficaz);
		probabilidadActId =(isDuplicado?"-1": detallesFullIperc[pindex].probabilidadActId);
		severidadActId =(isDuplicado?"-1": detallesFullIperc[pindex].severidadActId);
		probabilidadProyId = (isDuplicado?"-1":detallesFullIperc[pindex].probabilidadProyId);
		severidadProyId = (isDuplicado?"-1":detallesFullIperc[pindex].severidadProyId);

		isRiskyProy =(isDuplicado?"-1": detallesFullIperc[pindex].ipercDetailEvalRiskProy);
		isRiskyAct =(isDuplicado?"-1": detallesFullIperc[pindex].ipercDetailEvalRiskAct);
		indexAux=pindex;
		
		if (!nuevoDupIpercAutomatico) {
			ipercDetalleEvalIdCopy = detallesFullIperc[pindex].ipercDetailEvalID;	
		}

if(isDuplicado==true){
	detalleIpercDetailId = detallesFullIperc[pindex].ipercDetailEvalID+"copy";
}else{
	detalleIpercDetailId = detallesFullIperc[pindex].ipercDetailEvalID;
}
		var nivelRiesgoNombre=detallesFullIperc[pindex].nivelRiesgoNombre;
		var nivelRiesgoActNombre=detallesFullIperc[pindex].nivelRiesgoActNombre;
		var nivelRiesgoFutNombre=detallesFullIperc[pindex].nivelRiesgoProyNombre;
		
		var nivelRiesgoColor=detallesFullIperc[pindex].nivelRiesgoColor;
		var nivelRiesgoActColor=detallesFullIperc[pindex].nivelRiesgoActColor;
		var nivelRiesgoFutColor=detallesFullIperc[pindex].nivelRiesgoProyColor;
		
		
		 personExpId=detallesFullIperc[pindex].probPersonas;
		 indProcId=detallesFullIperc[pindex].probProcedimiento;
		 indCapaId=detallesFullIperc[pindex].probCapacitacion;
		 indRiskId=detallesFullIperc[pindex].probExpoRiesgo;
		
		 personExpIdAct=detallesFullIperc[pindex].probPersonasAct;
		 indProcIdAct=detallesFullIperc[pindex].probProcedimientoAct;
		 indCapaIdAct=detallesFullIperc[pindex].probCapacitacionAct;
		 indRiskIdAct=detallesFullIperc[pindex].probExpoRiesgoAct;
		
		 personExpIdFut=detallesFullIperc[pindex].probPersonasFut;
		 indProcIdFut=detallesFullIperc[pindex].probProcedimientoFut;
		 indCapaIdFut=detallesFullIperc[pindex].probCapacitacionFut;
		 indRiskIdFut=detallesFullIperc[pindex].probExpoRiesgoFut;
		 
		  frecPelPuroId=detallesFullIperc[pindex].frecuenciaPeligroPuraId;
		  frecPelActId=detallesFullIperc[pindex].frecuenciaPeligroActId;
		  frecPelFutId=detallesFullIperc[pindex].frecuenciaPeligroFutId;
		 
		 
		 
		 
		$("#tdeficaz" + detalleIpercDetailId)
				.html(
						"<span><input type='number' id='inputEficaz' max='100' min='0' step='10' class='form-control' style='width:80px;' value='"
								+ eficacia + "'><label>%</label></span> ");

		var ided = "_" + detalleIpercDetailId;

		var name = $("#lugar" + detalleIpercDetailId).text();
		$("#lugar" + detalleIpercDetailId)
				.html(
						"<input type='text' id='inputLugar' class='form-control' placeholder='Lugar' style='width:100px;' value='"
								+ name + "'>");

		/** ****************Edicion de controles***************** */
		var name1 = $("#ctrlImpl1" + detalleIpercDetailId).html();
		if (name1 == "") {
			name1 = "Editar";
		}

		$("#ctrlImpl1" + detalleIpercDetailId).html(
				"<a href='#' onclick='javascript:verControlSegunTipo(1)' id='ctrlImplLink1'"
						+ detalleIpercDetailId + ">" + (name1) + "</a>");
		/** *********************************************** */
		var name2 = $("#ctrlImpl2" + detalleIpercDetailId).html();
		if (name2 == "") {
			name2 = "Editar";
		}
		$("#ctrlImpl2" + detalleIpercDetailId).html(
				"<a href='#' onclick='javascript:verControlSegunTipo(2)' id='ctrlImplLink2'"
						+ detalleIpercDetailId + ">" + (name2) + "</a>");
		/** *********************************************** */
		var name3 = $("#ctrlImpl3" + detalleIpercDetailId).html();
		if (name3 == "") {
			name3 = "Editar";
		}

		$("#ctrlImpl3" + detalleIpercDetailId).html(
				"<a href='#' onclick='javascript:verControlSegunTipo(3)' id='ctrlImplLink3'"
						+ detalleIpercDetailId + ">" + (name3) + "</a>");
		/** *********************************************** */
		var name4 = $("#ctrlImpl4" + detalleIpercDetailId).html();
		if (name4 == "") {
			name4 = "Editar";
		}

		$("#ctrlImpl4" + detalleIpercDetailId).html(
				"<a href='#' onclick='javascript:verControlSegunTipo(4)' id='ctrlImplLink4'"
						+ detalleIpercDetailId + ">" + (name4) + "</a>");
		/** *********************************************** */
		var name5 = $("#ctrlImpl5" + detalleIpercDetailId).html();

		if (name5 == "") {
			name5 = "Editar";
		}
		$("#ctrlImpl5" + detalleIpercDetailId).html(
				"<a href='#' onclick='javascript:verControlSegunTipo(5)' id='ctrlImplLink5'"
						+ detalleIpercDetailId + ">" + (name5) + "</a>");
		/** *********************************************** */
		var listaClasi = JSON.parse(sessionStorage.getItem("clasifs"));
		var selectClasif = crearSelectOneMenu("selClasi",
				"javascript:cargarComboPeligroRiesgo();", listaClasi, ided
						.lastIndexOf("copy") == -1 ? clasiId : "-1",
				"clasificacionId", "clasificacionNombre");
		$("#clasi" + detalleIpercDetailId).html(selectClasif);
		
		if (autoFocusPeligro) {
			$("#selClasi").focus();
		}

		var name6 = $("#despel" + detalleIpercDetailId).text();
		name6 = ided.lastIndexOf("copy") == -1 ? name6 : "";
		$("#despel" + detalleIpercDetailId)
				.html(
						"<input type='text' id='inputDesPel' class='form-control' placeholder='Descripci&oacute;n Peligro' style='width:280px;' value='"
								+ name6 + "'>");

		$("#inputDesPel").keypress(function(e) {
			if (e.which == 13) {
				nuevoDupIpercAutomatico = true;
				autoFocusPeligro = true;
				guardarDetalleIperc();
			}
		});
		
		
		
		
		
		/**
		 * ***********************eval inicial
		 * probabilidad************************
		 */
		
		if (idFlagEvalIni) {
			var selectProb = crearSelectOneMenu("selProb",
					"javascript:actualizarNivelRiesgo();", listProbabilidad,
					ided.lastIndexOf("copy") == -1 ? probabilidadId : "-1",
					"probabilidadId", "probabilidadNombre");
			$("#evainiprob" + detalleIpercDetailId).html(selectProb);
			/**
			 * ***********************eval inicial
			 * severidad************************
			 */
			var selectSev = crearSelectOneMenu("selSev",
					"javascript:actualizarNivelRiesgo();", listSeveridad, ided
							.lastIndexOf("copy") == -1 ? severidadId : "-1",
					"severidadId", "severidadNombre");
			$("#evainisev" + detalleIpercDetailId).html(selectSev);
			/** *******************nivel de riesgo**************************** */
			$("#evaininiv" + detalleIpercDetailId)
					.html(
							"<h5><strong>"
									+ nivelRiesgoNombre+ "</strong></h5>");
			$("#evaininiv" + detalleIpercDetailId).css(
					{
						"background-color" :nivelRiesgoColor,
						"color" : "black"
					});
		}

		/**
		 * ***********************eval actual
		 * probabilidad************************
		 */
		var selectProbAct = crearSelectOneMenu("selProbAct",
				"javascript:actualizarNivelRiesgoAct();", listProbabilidad,
				ided.lastIndexOf("copy") == -1 ? probabilidadActId : "-1",
				"probabilidadId", "probabilidadNombre");
		$("#evaactprob" + detalleIpercDetailId).html(selectProbAct);
		/** ***********************eval actual severidad************************ */
		var selectSevAct = crearSelectOneMenu("selSevAct",
				"javascript:actualizarNivelRiesgoAct();", listSeveridad, ided
						.lastIndexOf("copy") == -1 ? severidadActId : "-1",
				"severidadId", "severidadNombre");
		$("#evaactsev" + detalleIpercDetailId).html(selectSevAct);
		/** *******************nivel de riesgo actual**************************** */
		$("#evaactniv" + detalleIpercDetailId).html(
				"<h5><strong>"
						+ nivelRiesgoActNombre + "</strong></h5>");
		$("#evaactniv" + detalleIpercDetailId).css(
				{
					"background-color" : nivelRiesgoActColor,
					"color" : "black"
				});
		$("#aceptableact" + detalleIpercDetailId).html(
				"<input type='checkbox' id='riskAct'>");
		/**
		 * ***********************eval proyectado
		 * probabilidad************************
		 */
		var selectProbProy = crearSelectOneMenu("selProbProy",
				"javascript:actualizarNivelRiesgoProy();", listProbabilidad,
				ided.lastIndexOf("copy") == -1 ? probabilidadProyId : "-1",
				"probabilidadId", "probabilidadNombre");
		$("#evaproyprob" + detalleIpercDetailId).html(selectProbProy);
		/**
		 * ***********************eval proyectado
		 * severidad************************
		 */
		var selectSevProy = crearSelectOneMenu("selSevProy",
				"javascript:actualizarNivelRiesgoProy();", listSeveridad, ided
						.lastIndexOf("copy") == -1 ? severidadProyId : "-1",
				"severidadId", "severidadNombre");
		$("#evaproysev" + detalleIpercDetailId).html(selectSevProy);
		/**
		 * *******************nivel de riesgo
		 * proyectado****************************
		 */
		$("#evaproyniv" + detalleIpercDetailId)
				.html(
						"<h5><strong>"
								+ nivelRiesgoFutNombre + "</strong></h5>");
		$("#evaproyniv" + detalleIpercDetailId).css(
				{
					"background-color" :nivelRiesgoFutColor,
					"color" : "black"
				});

		$("#aceptablefut" + detalleIpercDetailId).html(
				"<input type='checkbox' id='riskProy'>");
		/** *********************consecuencias************************** */
		/** *********************marcar los checks :)************************** */
		marcarChechBox(isRiskyAct, "riskAct");
		marcarChechBox(isRiskyProy, "riskProy");

		
		
		/**
		 * **********************Listas de 
		 * frecuencia de peligro*********************
		 */
		if(idEvalFrec==1){
			if (idFlagEvalIni) {
				var selFrecPelPuro=crearSelectOneMenuY("selFrecPelPuro", "actualizarNivelRiesgoFrec()", listaFrecPel, frecPelPuroId, 
						"subProbabilidadId","puntajeSubProbabilidad","nombreSubProbilidad");
				$("#frecPelPuro"+ detalleIpercDetailId).html(selFrecPelPuro);
				var selProbEspePuro=crearSelectOneMenuY("selProbEspePuro", "actualizarNivelRiesgoFrec()", listProbEspe, probabilidadId, 
						"subProbabilidadId","puntajeSubProbabilidad","nombreSubProbilidad");
				$("#evainiprob"+ detalleIpercDetailId).html(selProbEspePuro);
				var selSevEspePuro=crearSelectOneMenuY("selSevEspePuro", "actualizarNivelRiesgoFrec()", listaSevEspe, severidadId, 
						"subProbabilidadId","puntajeSubProbabilidad","nombreSubProbilidad");
				$("#evainisev"+ detalleIpercDetailId).html(selSevEspePuro);
				
				actualizarNivelRiesgoFrec();
			}
			
	
		var selFrecPelAct=crearSelectOneMenuY("selFrecPelAct", "actualizarNivelRiesgoFrecAct()", listaFrecPel, frecPelActId, 
				"subProbabilidadId","puntajeSubProbabilidad","nombreSubProbilidad");
		var selFrecPelFut=crearSelectOneMenuY("selFrecPelFut", "actualizarNivelRiesgoFrecFut()", listaFrecPel, frecPelFutId, 
				"subProbabilidadId","puntajeSubProbabilidad","nombreSubProbilidad");
		
		$("#frecPelAct"+ detalleIpercDetailId).html(selFrecPelAct);
		$("#frecPelFut"+ detalleIpercDetailId).html(selFrecPelFut);
		
		
		var selProbEspeAct=crearSelectOneMenuY("selProbEspeAct", "actualizarNivelRiesgoFrecAct()", listProbEspe, probabilidadActId, 
				"subProbabilidadId","puntajeSubProbabilidad","nombreSubProbilidad");
		var selProbEspeFut=crearSelectOneMenuY("selProbEspeFut", "actualizarNivelRiesgoFrecFut()", listProbEspe, probabilidadProyId, 
				"subProbabilidadId","puntajeSubProbabilidad","nombreSubProbilidad");
		
		$("#evaactprob"+ detalleIpercDetailId).html(selProbEspeAct);
		$("#evaproyprob"+ detalleIpercDetailId).html(selProbEspeFut);
		
		var selSevEspeAct=crearSelectOneMenuY("selSevEspeAct", "actualizarNivelRiesgoFrecAct()", listaSevEspe, severidadActId, 
				"subProbabilidadId","puntajeSubProbabilidad","nombreSubProbilidad");
		var selSevEspeFut=crearSelectOneMenuY("selSevEspeFut", "actualizarNivelRiesgoFrecFut()", listaSevEspe, severidadProyId, 
				"subProbabilidadId","puntajeSubProbabilidad","nombreSubProbilidad");
		
		$("#evaactsev"+ detalleIpercDetailId).html(selSevEspeAct);
		$("#evaproysev"+ detalleIpercDetailId).html(selSevEspeFut);
		
		
		actualizarNivelRiesgoFrecFut();
		actualizarNivelRiesgoFrecAct();
		
		}
		
		
		
		
		/**
		 * **********************Listas de 
		 * probabilidad específica*********************
		 */
		
		
		
		if(idProbEspe==1){
			
			var selPerExp=	crearSelectOneMenuY("selPerExp", "actualizarProbabilidaEspecifica()", listaPersonaExpuesta, personExpId, 
					"subProbabilidadId","puntajeSubProbabilidad","nombreSubProbilidad");
			var selPerExpAct=	crearSelectOneMenuY("selPerExpAct", "actualizarProbabilidaEspecificaAct()", listaPersonaExpuesta, personExpIdAct, 
					"subProbabilidadId","puntajeSubProbabilidad","nombreSubProbilidad");
			var selPerExpFut=	crearSelectOneMenuY("selPerExpFut", "actualizarProbabilidaEspecificaFut()", listaPersonaExpuesta, personExpIdFut, 
					"subProbabilidadId","puntajeSubProbabilidad","nombreSubProbilidad");
			
			var selIndCapa=	crearSelectOneMenuY("selIndCapa", "actualizarProbabilidaEspecifica()", listaIndCapa, indCapaId, 
					"subProbabilidadId","puntajeSubProbabilidad","nombreSubProbilidad");
			var selIndCapaAct=	crearSelectOneMenuY("selIndCapaAct", "actualizarProbabilidaEspecificaAct()", listaIndCapa, indCapaIdAct, 
					"subProbabilidadId","puntajeSubProbabilidad","nombreSubProbilidad");
			var selIndCapaFut=	crearSelectOneMenuY("selIndCapaFut", "actualizarProbabilidaEspecificaFut()", listaIndCapa, indCapaIdFut, 
					"subProbabilidadId","puntajeSubProbabilidad","nombreSubProbilidad");
					
			
			var selIndRisk=	crearSelectOneMenuY("selIndRisk", "actualizarProbabilidaEspecifica()", listaIndRisk, indRiskId, 
					"subProbabilidadId","puntajeSubProbabilidad","nombreSubProbilidad");
			var selIndRiskAct=	crearSelectOneMenuY("selIndRiskAct", "actualizarProbabilidaEspecificaAct()", listaIndRisk, indRiskIdAct, 
					"subProbabilidadId","puntajeSubProbabilidad","nombreSubProbilidad");
			var selIndRiskFut=	crearSelectOneMenuY("selIndRiskFut", "actualizarProbabilidaEspecificaFut()", listaIndRisk, indRiskIdFut, 
					"subProbabilidadId","puntajeSubProbabilidad","nombreSubProbilidad");
					
			var selIndProc=	crearSelectOneMenuY("selIndProc", "actualizarProbabilidaEspecifica()", listaIndProc, indProcId, 
					"subProbabilidadId","puntajeSubProbabilidad","nombreSubProbilidad");
			var selIndProcAct=	crearSelectOneMenuY("selIndProcAct", "actualizarProbabilidaEspecificaAct()", listaIndProc, indProcIdAct, 
					"subProbabilidadId","puntajeSubProbabilidad","nombreSubProbilidad");
			var selIndProcFut=	crearSelectOneMenuY("selIndProcFut", "actualizarProbabilidaEspecificaFut()", listaIndProc, indProcIdFut, 
					"subProbabilidadId","puntajeSubProbabilidad","nombreSubProbilidad");
					

			
			$("#persExp"+ detalleIpercDetailId).html(selPerExp);
			$("#persExpAct"+ detalleIpercDetailId).html(selPerExpAct);
			$("#persExpFut"+ detalleIpercDetailId).html(selPerExpFut);
			
			$("#probCapa"+ detalleIpercDetailId).html(selIndCapa);
			$("#probCapaAct"+ detalleIpercDetailId).html(selIndCapaAct);
			$("#probCapaFut"+ detalleIpercDetailId).html(selIndCapaFut);
			
			$("#probRiskExp"+ detalleIpercDetailId).html(selIndRisk);
			$("#probRiskExpAct"+ detalleIpercDetailId).html(selIndRiskAct);
			$("#probRiskExpFut"+ detalleIpercDetailId).html(selIndRiskFut);
			
			$("#probProc"+ detalleIpercDetailId).html(selIndProc);
			$("#probProcAct"+ detalleIpercDetailId).html(selIndProcAct);
			$("#probProcFut"+ detalleIpercDetailId).html(selIndProcFut);
			
			$("#selProbAct").prop("disabled",true);
			$("#selProb").prop("disabled",true);
					$("#selProbProy").prop("disabled",true);

			}
		var dataParam = {
			};

		callAjaxPost(URL + '/iperc/listvarios/autocompletar', dataParam,
				implementarAutocompletar);

		habilitarBotonesEdicion();
		$("#btnEliminar").show();
		$("#btnCopiar").show();
		$("#btnAutoAsign").show();
		banderaEdicion = true;
	}
}

function actualizarNivelRiesgoGeneral(idSelProb, idSelSev, idCelda, nombreCelda) {

	$("#" + idCelda).html("<h5><strong>" + nombreCelda + "</strong></h5>");

	$("#" + idCelda).css({
		"background-color" : obtenerColorRiesgo(idSelProb, idSelSev),
		"color" : "black"
	});

}

function actualizarNivelRiesgo() {
	var idSelProb = $("#selProb option:selected").val();
	var idSelSev = $("#selSev option:selected").val();
	var idSelFrec=$("#selFrecPelPuro option:selected").val();

	$("#evaininiv" + detalleIpercDetailId).html(
			"<h5><strong>" + obtenerNivelRiesgo(idSelProb, idSelSev)
					+ "</strong></h5>");

	$("#evaininiv" + detalleIpercDetailId).css({
		"background-color" : obtenerColorRiesgo(idSelProb, idSelSev),
		"color" : "black"
	});
}

function actualizarNivelRiesgoAct() {
	var idSelProb = $("#selProbAct option:selected").val();
	var idSelSev = $("#selSevAct option:selected").val();

	$("#evaactniv" + detalleIpercDetailId).html(
			"<h5><strong>" + obtenerNivelRiesgo(idSelProb, idSelSev)
					+ "</strong></h5>");

	$("#evaactniv" + detalleIpercDetailId).css({
		"background-color" : obtenerColorRiesgo(idSelProb, idSelSev),
		"color" : "black"
	});

	
	
	
}

function actualizarNivelRiesgoProy() {
	var idSelProb = $("#selProbProy option:selected").val();
	var idSelSev = $("#selSevProy option:selected").val();
	$("#evaproyniv" + detalleIpercDetailId).html(
			"<h5><strong>" + obtenerNivelRiesgo(idSelProb, idSelSev)
					+ "</strong></h5>");

	$("#evaproyniv" + detalleIpercDetailId).css({
		"background-color" : obtenerColorRiesgo(idSelProb, idSelSev),
		"color" : "black"
	});

}

function obtenerNivelRiesgo(pprobabilidadId, pseveridadId) {

	for (i = 0; i < listNivelRiesgo.length; i++) {

		if (listNivelRiesgo[i].probabilidadId == pprobabilidadId
				&& listNivelRiesgo[i].severidadId == pseveridadId) {

			return listNivelRiesgo[i].nivelRiesgoNombre;
		}
	}

}

function obtenerColorRiesgo(pprobabilidadId, pseveridadId) {

	for (i = 0; i < listNivelRiesgo.length; i++) {

		if (listNivelRiesgo[i].probabilidadId == pprobabilidadId
				&& listNivelRiesgo[i].severidadId == pseveridadId) {

			return listNivelRiesgo[i].nivelRiesgoInfo;
		}
	}

}

function implementarAutocompletar(data) {
	switch (data.CODE_RESPONSE) {
	case "05": 
		
		listFullConsecuencias=data.list;
		var consecuencias = listarStringsDesdeArrayY(listFullConsecuencias,"consecuenciaNombre","clasificacionId",clasiId);
		var controlIniciales = data.listControl;
		console.log(controlIniciales);
		controlIniciales=[];
		var ided = "_" + detalleIpercDetailId;
		/** *********************consecuencias1************************** */
		if (indicadorCons == 1) {
			var cons1 = $("#cons1" + detalleIpercDetailId).text();
			cons1 = ided.lastIndexOf("copy") == -1 ? cons1 : "";
			$("#cons1" + detalleIpercDetailId)
					.html(
							"<input type='text' id='inputCons1' class='form-control' placeholder='Consecuencia' style='width:280px;' value='"
									+ cons1 + "'>");
			$("#inputCons1").autocomplete({
				source : consecuencias,
				minLength : 0
			}).focus(function() {
		$(this).autocomplete('search', '');
	});

		} else if (indicadorCons == 2) {
			var cons1 = $("#cons1" + detalleIpercDetailId).text();
			cons1 = ided.lastIndexOf("copy") == -1 ? cons1 : "";
			$("#cons1" + detalleIpercDetailId)
					.html(
							"<input type='text' id='inputCons1' class='form-control' placeholder='Consecuencia' style='width:280px;' value='"
									+ cons1 + "'>");
			$("#inputCons1").autocomplete({
				source : consecuencias,
				minLength : 0
			}).focus(function() {
		$(this).autocomplete('search', '');
	});
			/** *********************consecuencias2************************** */
			var cons2 = $("#cons2" + detalleIpercDetailId).text();
			cons2 = ided.lastIndexOf("copy") == -1 ? cons2 : "";
			$("#cons2" + detalleIpercDetailId)
					.html(
							"<input type='text' id='inputCons2' class='form-control' placeholder='Consecuencia' style='width:280px;' value='"
									+ cons2 + "'>");
			$("#inputCons2").autocomplete({
				source : consecuencias,
				minLength : 0
			}).focus(function() {
		$(this).autocomplete('search', '');
	});
			/** *********************consecuencias3************************** */
			var cons3 = $("#cons3" + detalleIpercDetailId).text();
			cons3 = ided.lastIndexOf("copy") == -1 ? cons3 : "";
			$("#cons3" + detalleIpercDetailId)
					.html(
							"<input type='text' id='inputCons3' class='form-control' placeholder='Consecuencia' style='width:280px;' value='"
									+ cons3 + "'>");
			$("#inputCons3").autocomplete({
				source : consecuencias,
				minLength : 0
			}).focus(function() {
		$(this).autocomplete('search', '');
	});

		}

		/** *********************************************** */
		var listaPels = JSON.parse(sessionStorage.getItem("peligros"));

		var pel = $("#pel" + detalleIpercDetailId).text();
		pel = ided.lastIndexOf("copy") == -1 ? pel : "";
		$("#pel" + detalleIpercDetailId)
				.html(
						"<input type='text' id='inputPeligro' class='form-control' placeholder='Peligro' style='width:280px;' value='"
								+ pel + "'>");
		$("#inputPeligro").autocomplete(
				{
					source : listarStringsDesdeArray(listaPels[clasiId],
							"peligroName"),
					minLength : 0
				}).focus(function() {
			$(this).autocomplete('search', '');
		});
		/** *********************************************** */
		var listaRiesgos = JSON.parse(sessionStorage.getItem("riesgos"));

		var riesgo = $("#riesgo" + detalleIpercDetailId).text();
		riesgo = ided.lastIndexOf("copy") == -1 ? riesgo : "";
		$("#riesgo" + detalleIpercDetailId)
				.html(
						"<input type='text' id='inputRiesgo' class='form-control' placeholder='Riesgo' style='width:230px;' value='"
								+ riesgo + "'>");
		$("#inputRiesgo").autocomplete(
				{
					source : listarStringsDesdeArray(listaRiesgos[clasiId],
							"riesgoName"),
					minLength : 0
				}).focus(function() {
			$(this).autocomplete('search', '');
		});
		/** *********************controlinicial1************************** */
		var cons1 = $("#ctrlini1" + detalleIpercDetailId).text();
		cons1 = ided.lastIndexOf("copy") == -1 ? cons1 : "";
		$("#ctrlini1" + detalleIpercDetailId)
				.html(
						"<textarea  id='inputCtrlIni1' class='form-control' placeholder='Control' style='width:200px;resize:none' ></textarea>");
		$("#inputCtrlIni1").autocomplete({
			source : controlIniciales
		});
		console.log(controlIniciales);
		/** *********************controlinicial2************************** */
		var cons2 = $("#ctrlini2" + detalleIpercDetailId).text();
		cons2 = ided.lastIndexOf("copy") == -1 ? cons2 : "";
		$("#ctrlini2" + detalleIpercDetailId)
				.html(
						"<textarea  id='inputCtrlIni2' class='form-control' placeholder='Control' style='width:200px;resize:none'></textarea>");
		$("#inputCtrlIni2").autocomplete({
			source : controlIniciales
		});
		/** *********************controlinicial3************************** */
		var cons3 = $("#ctrlini3" + detalleIpercDetailId).text();
		cons3 = ided.lastIndexOf("copy") == -1 ? cons3 : "";
		$("#ctrlini3" + detalleIpercDetailId)
				.html(
						"<textarea  id='inputCtrlIni3' class='form-control' placeholder='Control' style='width:200px;resize:none' ></textarea>");
		autosize($("#inputCtrlIni1"));$("#inputCtrlIni1").val(cons1);
		autosize($("#inputCtrlIni2"));$("#inputCtrlIni2").val(cons2);
		autosize($("#inputCtrlIni3"));$("#inputCtrlIni3").val(cons3);
		$("#inputCtrlIni3").autocomplete({
			source : controlIniciales
		});
		// inicializarAutocompletar();
		break;
	default:
		alert("Ocurrió un error al traer las consecuencias!");
	}
}

function guardarDetalleIperc() {

	var campoVacio = true;
	var eficaz = $("#inputEficaz").val();
	var isRiskyActAux = pasarBoolNumber($("#riskAct").prop("checked"));
	var isRiskyFutAux = pasarBoolNumber($("#riskProy").prop("checked"));

	var txtPel = $("#inputPeligro").val();
	var despel = $("#inputDesPel").val();
	var idclas = $("#selClasi option:selected").val();
	var txtRiesgo = $("#inputRiesgo").val();
	var txtlug = $("#inputLugar").val();

	var idprob = idFlagEvalIni ? $("#selProb option:selected").val() : null;
	var idsev = idFlagEvalIni ? $("#selSev option:selected").val() : null;
	var txtniv = idFlagEvalIni ? $("#evaininiv" + detalleIpercDetailId).text()
			: null;

	var idprobact = $("#selProbAct option:selected").val();
	var idsevact = $("#selSevAct option:selected").val();
	var txtnivact = $("#evaactniv" + detalleIpercDetailId).text();

	var idprobproy = $("#selProbProy option:selected").val();
	var idsevproy = $("#selSevProy option:selected").val();
	var txtnivproy = $("#evaproyniv" + detalleIpercDetailId).text();

	var txtcons1 = indicadorCons == 1 || indicadorCons == 2 ? $("#inputCons1")
			.val() : null;
	var txtcons2 = indicadorCons == 2 ? $("#inputCons2").val() : null;
	var txtcons3 = indicadorCons == 2 ? $("#inputCons3").val() : null;
	
	var txtctrli1 = $("#inputCtrlIni1").val();
	var txtctrli2 = $("#inputCtrlIni2").val();
	var txtctrli3 = $("#inputCtrlIni3").val();
	
if(idProbEspe==1){
	
	if (idFlagEvalIni==1) {
		var auxIdPersExp=$("#selPerExp").val().split('-');
		var auxIdCapa=$("#selIndCapa").val().split('-');
		var auxIdIndRisk=$("#selIndRisk").val().split('-');
		var auxIdIndProc=$("#selIndProc").val().split('-');
		
		
		
		var idPersExp=auxIdPersExp[0];
		var idCapa=auxIdCapa[0];
		var idIndRisk=auxIdIndRisk[0];
		var idIndProc=auxIdIndProc[0];
	}
	
	
	
	
	
	var auxIdPerExpAct=$("#selPerExpAct").val().split('-');
	var auxIdIndCapaAct=$("#selIndCapaAct").val().split('-');
	var auxIdIndRiskAct=$("#selIndRiskAct").val().split('-');
	var auxIdIndProcAct=$("#selIndProcAct").val().split('-');
	
	var auxIdPersExpFut=$("#selPerExpFut").val().split('-');
	var auxIdCapaFut=$("#selIndCapaFut").val().split('-');
	var auxIdIndRiskFut=$("#selIndRiskFut").val().split('-');
	var auxIdIndProcFut=$("#selIndProcFut").val().split('-');
	

	
	var idPerExpAct=auxIdPerExpAct[0];
	var idIndCapaAct=auxIdIndCapaAct[0];
	var idIndRiskAct=auxIdIndRiskAct[0];
	var idIndProcAct=auxIdIndProcAct[0];

	var idPersExpFut=auxIdPersExpFut[0];
	var idCapaFut=auxIdCapaFut[0];
	var idIndRiskFut=auxIdIndRiskFut[0];
	var idIndProcFut=auxIdIndProcFut[0];
	
	
}
var frecPelId;
	if(idEvalFrec==1){
		if (idFlagEvalIni==1) {
			var auxFrecPelId=$("#selFrecPelPuro").val().split('-');
			
			var auxProbEspeId=$("#selProbEspePuro").val().split('-');
			var auxSevEspeId=$("#selSevEspePuro").val().split('-');
			
		 frecPelId=parseInt(auxFrecPelId[0]);
		 idprob =parseInt(auxProbEspeId[0]);
		 idsev=parseInt(auxSevEspeId[0]);
		}
		
		var auxFecPelActId=$("#selFrecPelAct").val().split('-');
		var auxFrecPelFutId=$("#selFrecPelFut").val().split('-');
		
		
		var frecPelActId=parseInt(auxFecPelActId[0]);
		var frecPelFutId=parseInt(auxFrecPelFutId[0]);
		
		
		var auxProbEspeActId=$("#selProbEspeAct").val().split('-');
		var auxProbEspeFutId=$("#selProbEspeFut").val().split('-');
		
		
		var auxSevEspeActId=$("#selSevEspeAct").val().split('-');
		var auxSevEspeFutId=$("#selSevEspeFut").val().split('-');
		
		
		idprobact=auxProbEspeActId[0],
		 idsevact=auxSevEspeActId[0],
		 idprobproy=auxProbEspeFutId[0],
		 idsevproy=auxSevEspeFutId[0]


	
		
	}

	if (txtPel.length == 0 || despel.length == 0 || idclas == '-1'
			|| txtlug.length == 0) {
		campoVacio = false;
	}

	var idDuplicado = "_" + detalleIpercDetailId;
	var n = idDuplicado.lastIndexOf("copy");console.log(idDuplicado);
	if (n!=-1) {
		detalleIpercDetailId = 0;
		console.log(detalleIpercDetailId);
		nuevoDupIpercAutomatico=false;
	}

	if (campoVacio) {
	
		var dataParam = {
			ipercDetailId : detalleIpercId,
			ipercDetailEvalID : detalleIpercDetailId,
			descripcionLugar : txtlug,
			descripcionPeligro : despel,
			peligroName : txtPel,
			idClasificacion : idclas,
			riesgoName : txtRiesgo,
			consecuencia1 : txtcons1,
			consecuencia2 : txtcons2,
			consecuencia3 : txtcons3,
			probabilidadId : idprob,
			severidadId : idsev,
			controlInicial1 : txtctrli1,
			controlInicial2 : txtctrli2,
			controlInicial3 : txtctrli3,
			probabilidadActId : idprobact,
			severidadActId : idsevact,
			probabilidadProyId : idprobproy,
			severidadProyId : idsevproy,
			eficaz : eficaz,
			ipercDetailEvalRiskAct : isRiskyActAux,
			ipercDetailEvalRiskProy : isRiskyFutAux,
			probPersonas	:idPersExp,
			probCapacitacion: idCapa,
			probExpoRiesgo: idIndRisk,
		probProcedimiento: idIndProc,
		
		probPersonasAct: idPerExpAct,
		probCapacitacionAct: idIndCapaAct,
		probExpoRiesgoAct: idIndRiskAct,
		probProcedimientoAct: idIndProcAct,
		
		probPersonasFut: idPersExpFut,
		probCapacitacionFut: idCapaFut,
		probExpoRiesgoFut: idIndRiskFut,
		probProcedimientoFut: idIndProcFut,
		
		frecuenciaPeligroPuraId:frecPelId,
		frecuenciaPeligroActId:frecPelActId,
		frecuenciaPeligroFutId:frecPelFutId,
		
		nivelRiesgoProyId:idRiesgoFutEspecial,
		nivelRiesgoActId:idRiesgoActEspecial,
		nivelRiesgoId:idRiesgoEspecial
		};

		callAjaxPost(URL + '/iperc/detail/save', dataParam,
				procesarGuardarIpercDetalle);
	} else {
		alert("Debe ingresar todos los campos.");
	}
}

function procesarGuardarIpercDetalle(data) {
	switch (data.CODE_RESPONSE) {
	case "05":

		if (nuevoDupIpercAutomatico) {
			duplicarIperc();
		} else {
			listarDetalleIperc();
			banderaEdicion = false;
		}

		break;
	default:
		alert("Ocurrió un error al guardar la evaluacion!");
	}
}

function cancelarDetalleIperc() {
	autoFocusPeligro = false;
	autoFocusRiesgo = false;
	autoFocusConsecuencia = false;
	nuevoDupIpercAutomatico = false;
	listarDetalleIperc();
	banderaEdicion = false;
}

function cargarComboPeligroRiesgo() {
	var listaPels = JSON.parse(sessionStorage.getItem("peligros"));
	var idSelClasif = $("#selClasi option:selected").val();

	$("#inputPeligro").autocomplete(
			{
				source : listarStringsDesdeArray(listaPels[idSelClasif],
						"peligroName"),
				minLength : 0
			});
	/** ******************************************************** */
	var listaRiesgos = JSON.parse(sessionStorage.getItem("riesgos"));
	$("#inputRiesgo").autocomplete(
			{
				source : listarStringsDesdeArray(listaRiesgos[idSelClasif],
						"riesgoName"),
				minLength : 0
			});
	/***consecuencais**************/
	var consecuencias=listarStringsDesdeArrayY(listFullConsecuencias,"consecuenciaNombre","clasificacionId",idSelClasif);
	
	if (indicadorCons == 1) {
	
		$("#inputCons1").autocomplete({
			source : consecuencias,
			minLength : 0
		})

	} else if (indicadorCons == 2) {
		var cons1 = $("#cons1" + detalleIpercDetailId).text();
		cons1 = ided.lastIndexOf("copy") == -1 ? cons1 : "";
		$("#cons1" + detalleIpercDetailId)
				.html(
						"<input type='text' id='inputCons1' class='form-control' placeholder='Consecuencia' style='width:280px;' value='"
								+ cons1 + "'>");
		$("#inputCons1").autocomplete({
			source : consecuencias,
			minLength : 0
		}).focus(function() {
	$(this).autocomplete('search', '');
});
		/** *********************consecuencias2************************** */
		var cons2 = $("#cons2" + detalleIpercDetailId).text();
		cons2 = ided.lastIndexOf("copy") == -1 ? cons2 : "";
		$("#cons2" + detalleIpercDetailId)
				.html(
						"<input type='text' id='inputCons2' class='form-control' placeholder='Consecuencia' style='width:280px;' value='"
								+ cons2 + "'>");
		$("#inputCons2").autocomplete({
			source : consecuencias,
			minLength : 0
		}).focus(function() {
	$(this).autocomplete('search', '');
});
		/** *********************consecuencias3************************** */
		var cons3 = $("#cons3" + detalleIpercDetailId).text();
		cons3 = ided.lastIndexOf("copy") == -1 ? cons3 : "";
		$("#cons3" + detalleIpercDetailId)
				.html(
						"<input type='text' id='inputCons3' class='form-control' placeholder='Consecuencia' style='width:280px;' value='"
								+ cons3 + "'>");
		$("#inputCons3").autocomplete({
			source : consecuencias,
			minLength : 0
		}).focus(function() {
	$(this).autocomplete('search', '');
});

	}
	
	
}

function duplicarIperc() {
	$("#tblMatrices").width('3500px');

	var dataParam = {
		ipercId : idIPERCEdicion,
		matrizRiesgoId : idMatrizRiesgo
	};

	callAjaxPost(URL + '/iperc/detalle', dataParam, listarLuegoDuplicar);
}

function listarLuegoDuplicar(data) {

	procesarListarTablaDetalleIperc(data);
	if (nuevoDupIpercAutomatico) {
		detalleIpercDetailId = ipercDetalleEvalIdCopy;
	}

	var $tableBody = $('#tblMatrices').find("tbody"), 
	$trLast = $tableBody.find("#trm" + detalleIpercDetailId), $trNew = $trLast.clone();

	$trNew.find('*').andSelf().filter('[id]').each(function() {
		this.id += 'copy';
	});
	$trNew.removeAttr("onclick");

	$trLast.after($trNew);

	banderaEdicion = false;
	$("#btnEliminar").hide();
	
	editarIpercDetalle(indexAux,true);
	
}

function configurarIperc() {
	var flgEvalIni = 0;
	var flgProbEsp=0;
	var indCons = 0;
	var flagFrecPel=0;
	var msje = idMatrizRiesgo != $("#selectNR option:selected").val() ? "Al cambiar de tipo de matriz de riesgo, se borrarán los valores de las evaluaciones.¿Esta seguro del cambio?"
			: "¿Está seguro de cambiar la estructura del IPERC?";

	var r = confirm(msje);
	if (r == true) {
		if ($("#chkbCtrl").prop('checked')) {
			flgEvalIni = 1;
		}
		
		if ($("#chckProbEsp").prop('checked')) {
			flgProbEsp = 1;
		}
		if ($("#chckProbEsp").prop('checked')) {
			flgProbEsp = 1;
		}
		if($('#chckFrecPel').prop('checked')){
			flagFrecPel=1;
		}
		
		var dataParam = {
			ipercId : idIPERCEdicion,
			matrizRiesgoId : $("#selectNR option:selected").val(),
			flagEvaluacionInicial : flgEvalIni,
			flagProbabilidadEspecifica : flgProbEsp,
			flagEvaluacionFrecuencia:flagFrecPel,
			indicadorConsecuencia : $("#selectCons option:selected").val(),
			ipercName : nombreIperc,
			mdfId : idMdfEdicion
		};

		callAjaxPost(URL + '/iperc/conf', dataParam, configurarIpercRespuesta);
	}
}

function configurarIpercRespuesta(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		idMatrizRiesgo = $("#selectNR option:selected").val();
		indicadorCons = $("#selectCons option:selected").val();
		if ($("#chkbCtrl").prop('checked')) {
			idFlagEvalIni = 1;
		} else {
			idFlagEvalIni = 0;
		}
		
		if ($("#chckProbEsp").prop('checked')) {
			idProbEspe = 1;
		} else {
			idProbEspe = 0;
		}

		if ($("#chckFrecPel").prop('checked')) {
			idEvalFrec = 1;
		} else {
			idEvalFrec = 0;
		}
		listarDetalleIperc();
		banderaEdicion = false;
		break;
	default:
		alert("Ocurrió un error al resetear Evaluaciones!");
	
	}
	
}

function eliminarDetalleIperc() {
	var r = confirm("¿Está seguro de eliminar el detalle de IPERC?");
	if (r == true) {
		var dataParam = {
			ipercDetailId : detalleIpercId,
			ipercDetailEvalID : detalleIpercDetailId
		};

		callAjaxPost(URL + '/iperc/detail/delete', dataParam,
				procesarEliminarEvaluacionIperc);
	}
}

function procesarEliminarEvaluacionIperc(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		 
		if(data.mensaje.length>0){
			alert(data.mensaje);
		}
		listarDetalleIperc();
		banderaEdicion = false;

		break;
	default:
		alert("Ocurrió un error al guardar la evaluacion!");
	}
}


function actualizarProbabilidaEspecifica(){

	if(idProbEspe==1){
	var auxPuntajePersExp=$("#selPerExp").val().split('-');
	var auxPuntajeCapa=$("#selIndCapa").val().split('-');
	var auxPuntajeIndRisk=$("#selIndRisk").val().split('-');
	var auxPuntajeIndProc=$("#selIndProc").val().split('-');
	
	
	var puntajePersExp=auxPuntajePersExp[1];
	var puntajeCapa=auxPuntajeCapa[1];
	var puntajeIndRisk=auxPuntajeIndRisk[1];
	var puntajeIndProc=auxPuntajeIndProc[1];
	
	var puntajeProbPuro=parseInt(puntajePersExp)+
	parseInt(puntajeCapa)+
	parseInt(puntajeIndRisk)+
	parseInt(puntajeIndProc);
	
	if(puntajeProbPuro<13){
		
		$("#selProb").val("3");
		
		if(puntajeProbPuro<9){
			$("#selProb").val("2");
			
			if(puntajeProbPuro<5){
				$("#selProb").val("1");
			}	
		}
		
	}
	
	actualizarNivelRiesgo();
	
	}
	

}

function actualizarProbabilidaEspecificaAct(){

	
	
	var auxPuntajePerExpAct=$("#selPerExpAct").val().split('-');
	var auxPuntajeIndCapaAct=$("#selIndCapaAct").val().split('-');
	var auxPuntajeIndRiskAct=$("#selIndRiskAct").val().split('-');
	var auxPuntajeIndProcAct=$("#selIndProcAct").val().split('-');
	
	
	var puntajePerExpAct=auxPuntajePerExpAct[1];
	var puntajeIndCapaAct=auxPuntajeIndCapaAct[1];
	var puntajeIndRiskAct=auxPuntajeIndRiskAct[1];
	var puntajeIndProcAct=auxPuntajeIndProcAct[1];

	
	var puntajeProbAct=parseInt(puntajePerExpAct)+
	parseInt(puntajeIndCapaAct)+
	parseInt(puntajeIndRiskAct)+
	parseInt(puntajeIndProcAct);
	
	
if(puntajeProbAct<13){
		
		$("#selProbAct").val("3");
		
		if(puntajeProbAct<9){
			$("#selProbAct").val("2");
			
			if(puntajeProbAct<5){
				$("#selProbAct").val("1");
			}	
		}
		
	}



 actualizarNivelRiesgoAct() ;
	




}

function actualizarProbabilidaEspecificaFut(){

	
	
	var auxPuntajePersExpFut=$("#selPerExpFut").val().split('-');
	var auxPuntajeCapaFut=$("#selIndCapaFut").val().split('-');
	var auxPuntajeIndRiskFut=$("#selIndRiskFut").val().split('-');
	var auxPuntajeIndProcFut=$("#selIndProcFut").val().split('-');
	
	

	
	var puntajePersExpFut=auxPuntajePersExpFut[1];
	var puntajeCapaFut=auxPuntajeCapaFut[1];
	var puntajeIndRiskFut=auxPuntajeIndRiskFut[1];
	var puntajeIndProcFut=auxPuntajeIndProcFut[1];
	
	
	
	
	var puntajeProbFut=parseInt(puntajePersExpFut)+
	parseInt(puntajeCapaFut)+
	parseInt(puntajeIndRiskFut)+
	parseInt(puntajeIndProcFut);


if(puntajeProbFut<13){
	
	$("#selProbProy").val("3");
	
	if(puntajeProbFut<9){
		$("#selProbProy").val("2");
		
		if(puntajeProbFut<5){
			$("#selProbProy").val("1");
		}	
	}
	
}




 actualizarNivelRiesgoProy();



}



function actualizarNivelRiesgoFrec(){
	var auxFrecPelId=null;
	auxFrecPelId=$("#selFrecPelPuro").val().split('-');
	var frecPelPuntaje=auxFrecPelId[1];
	
	var auxSevEspeId=$("#selSevEspePuro").val().split('-');
	var auxProbEspeId=$("#selProbEspePuro").val().split('-');

	var sevPuntaje=auxSevEspeId[1];
	var probPuntaje=auxProbEspeId[1];
	
	var puntajeTotal=parseFloat(frecPelPuntaje)*
	parseFloat(sevPuntaje)*
	parseFloat(probPuntaje);
	var nombreRiesgo="";
	var colorRiesgo="";
	 idRiesgoEspecial="";
	if(puntajeTotal>400){
		idRiesgoEspecial=31;
		nombreRiesgo="Muy Alto";
		colorRiesgo="#FF0000";
		
	}else{
		
		if(puntajeTotal>200){
			idRiesgoEspecial=30;
			nombreRiesgo="Alto";
			colorRiesgo="#FF4000";
		}else{
			if(puntajeTotal>70){
				idRiesgoEspecial=29;
				nombreRiesgo="Importante";
				colorRiesgo="#80FF00";
			}else{
				idRiesgoEspecial=28;
				nombreRiesgo="Posible";
				colorRiesgo="#088A08";
			}
		}
	}
	if($("#selFrecPelPuro").val()==-1 ||
			$("#selSevEspePuro").val()==-1||
			$("#selProbEspePuro").val()==-1){
		
		idRiesgoEspecial=null;
		$("#evaininiv" + detalleIpercDetailId).html(
				"<h5><strong>" + ""
						+ "</strong></h5>");

		$("#evaininiv" + detalleIpercDetailId).css({
			"background-color" : "white",
			"color" : "black"
		});
		
	}else{
		
		$("#evaininiv" + detalleIpercDetailId).html(
				"<h5><strong>" + nombreRiesgo
						+ "</strong></h5>");

		$("#evaininiv" + detalleIpercDetailId).css({
			"background-color" : colorRiesgo,
			"color" : "black"
		});
	}
	

	
	
}

function actualizarNivelRiesgoFrecAct(){
	var auxFecPelActId=$("#selFrecPelAct").val().split('-');
	var auxProbEspeActId=$("#selProbEspeAct").val().split('-');
	var auxSevEspeActId=$("#selSevEspeAct").val().split('-');
	
	var frecPelActPuntaje=auxFecPelActId[1];
	var sevPuntajeAct=auxSevEspeActId[1];
	var probPuntajeAct=auxProbEspeActId[1];
	
	var puntajeTotal=parseFloat(frecPelActPuntaje)*parseFloat(sevPuntajeAct)*parseFloat(probPuntajeAct);
	var nombreRiesgo="";
	var colorRiesgo="";
	 idRiesgoActEspecial="";
	if(puntajeTotal>400){
		idRiesgoActEspecial=31;
		nombreRiesgo="Muy Alto";
		colorRiesgo="#FF0000";
		
	}else{
		
		if(puntajeTotal>200){
			idRiesgoActEspecial=30;
			nombreRiesgo="Alto";
			colorRiesgo="#FF4000";
		}else{
			if(puntajeTotal>70){
				idRiesgoActEspecial=29;
				nombreRiesgo="Importante";
				colorRiesgo="#80FF00";
			}else{
				idRiesgoActEspecial=28;
				nombreRiesgo="Posible";
				colorRiesgo="#088A08";
			}
		}
	}
	if($("#selProbEspeAct").val()==-1 ||
			$("#selSevEspeAct").val()==-1||
			$("#selFrecPelAct").val()==-1){
		idRiesgoActEspecial=null;
		$("#evaactniv" + detalleIpercDetailId).html(
				"<h5><strong>" + ""
						+ "</strong></h5>");

		$("#evaactniv" + detalleIpercDetailId).css({
			"background-color" : "white",
			"color" : "black"
		});
		
	}else{
		$("#evaactniv" + detalleIpercDetailId).html(
				"<h5><strong>" + nombreRiesgo
						+ "</strong></h5>");

		$("#evaactniv" + detalleIpercDetailId).css({
			"background-color" : colorRiesgo,
			"color" : "black"
		});
	}
	
}
function actualizarNivelRiesgoFrecFut(){


	var auxFrecPelFutId=$("#selFrecPelFut").val().split('-');
	var auxSevEspeFutId=$("#selSevEspeFut").val().split('-');
	var auxProbEspeFutId=$("#selProbEspeFut").val().split('-');

	var frecPelFutPuntaje=auxFrecPelFutId[1];
	var sevPuntajeFut=auxSevEspeFutId[1];
	var probPuntajeFut=auxProbEspeFutId[1];
	
	var puntajeTotal=parseFloat(frecPelFutPuntaje)*parseFloat(sevPuntajeFut)*parseFloat(probPuntajeFut);
	var nombreRiesgo="";
	var colorRiesgo="";
	 idRiesgoFutEspecial=null;
	if(puntajeTotal>400){
		idRiesgoFutEspecial=31;
		nombreRiesgo="Muy Alto";
		colorRiesgo="#FF0000";
		
	}else{
		
		if(puntajeTotal>200){
			idRiesgoFutEspecial=30;
			nombreRiesgo="Alto";
			colorRiesgo="#FF4000";
		}else{
			if(puntajeTotal>70){
				idRiesgoFutEspecial=29;
				nombreRiesgo="Importante";
				colorRiesgo="#80FF00";
			}else{
				idRiesgoFutEspecial=28;
				nombreRiesgo="Posible";
				colorRiesgo="#088A08";
			}
		}
	}
	
	if($("#selFrecPelFut").val()==-1 ||
			$("#selProbEspeFut").val()==-1||
			$("#selSevEspeFut").val()==-1){
		idRiesgoFutEspecial=null;
		$("#evaproyniv" + detalleIpercDetailId).html(
				"<h5><strong>" +""
						+ "</strong></h5>");

		$("#evaproyniv" + detalleIpercDetailId).css({
			"background-color" : "white",
			"color" : "black"
		});
		
	}else{
		
		$("#evaproyniv" + detalleIpercDetailId).html(
				"<h5><strong>" + nombreRiesgo
						+ "</strong></h5>");

		$("#evaproyniv" + detalleIpercDetailId).css({
			"background-color" : colorRiesgo,
			"color" : "black"
		});
	}
	
	
}

function iniciarCarrusel(numImagenes,numSimultaneas){
	var numeroImatges = numImagenes;
	var posicion=0;
	var numImagensSimultaneo=numSimultaneas;
	   if(numeroImatges<=numImagensSimultaneo){
	       $('.derecha_flecha').css('display','none');
	    $('.izquierda_flecha').css('display','none');
	   }

	     $('.izquierda_flecha').on('click',function(){
	         if(posicion>0){
	            posicion = posicion-1;
	        }else{
	            posicion = numeroImatges-numImagensSimultaneo;
	        }
	        $(".carrusel").animate({"left": -($('#product_'+posicion).position().left)}, 600);
	        return false;
	     });

	     $('.izquierda_flecha').hover(function(){
	         $(this).css('opacity','0.5');
	     },function(){
	         $(this).css('opacity','1');
	     });

	     $('.derecha_flecha').hover(function(){
	         $(this).css('opacity','0.5');
	     },function(){
	         $(this).css('opacity','1');
	     });

	     $('.derecha_flecha').on('click',function(){
	        if(numeroImatges>posicion+numImagensSimultaneo){
	            posicion = posicion+1;
	        }else{
	            posicion = 0;
	        }
	        $(".carrusel").animate({"left": -($('#product_'+posicion).position().left)}, 600);
	        return false;
	     });

	 
}
function verAyudaIPERC(parametroId){
	
	var textoPrueba='<div id="carrusel">'
	 +'   <a href="#" class="izquierda_flecha"><img src="../imagenes/flecha_01.png" /></a>'
	 +'   <a href="#" class="derecha_flecha"><img src="../imagenes/flecha_02.png" /></a>'
	 +' <div class="carrusel">'
	
		+'</div>'
+'	</div>';
	$("#exampleModal").modal("hide");
	$("#modalAyudaIperc").modal('show');
	var tipoMatriz=$("#selectNR").val();
	$("#modalAyudaIperc .modal-content").css({height:"190px",width:"500px"});
	var carruselImg=[];
	var carruselAux1=[];
	var carruselAux2=[];
	var carruselAux3=[];
	var carruselAux4=[];
	var carruselAux5=[];
	carruselAux1={
			imagenUrl:"../imagenes/logo3.png",
			textoDescriptivo:"-----",
			tituloImagen:"Grafico Sin Agregar"
	};
		carruselAux2={
				imagenUrl:"../imagenes/logo3.png",
				textoDescriptivo:"----",
				tituloImagen:"Grafico Sin Agregar 2"
		};
		carruselAux3={
				imagenUrl:"../imagenes/logo4.png",
				textoDescriptivo:"----",
				tituloImagen:"Grafico Sin Agregar 24"
		};
		carruselAux4={
				imagenUrl:"../imagenes/logo3.png",
				textoDescriptivo:"----",
				tituloImagen:"Grafico Sin Agregar P"
		};
	switch(parseInt(tipoMatriz)){
	case 1:
		carruselAux1={
			imagenUrl:"../imagenes/matriz3x3.png",
			tituloImagen:"Matriz 3x3",
			textoDescriptivo:"Relación entre Provabilidad y Severidad"
	};
		carruselAux2={
				imagenUrl:"../imagenes/descirp3x3.png",
				tituloImagen:"Detalle de Nivel de Riesgo",
				textoDescriptivo:"Significada de cada Nivel de riesgo"
		};
		carruselAux3={
				imagenUrl:"../imagenes/ConsecuenciaMet2.png",
				textoDescriptivo:"----",
				tituloImagen:"Descripcion de Consecuencias"
		};
		carruselAux4={
				imagenUrl:"../imagenes/expMet2.png",
				textoDescriptivo:"----",
				tituloImagen:"Exposición al riesgo"
		};
		carruselAux5={
				imagenUrl:"../imagenes/met2Prob.png",
				textoDescriptivo:"----",
				tituloImagen:"Descripción de Probabilidad"
		};
	
		carruselImg.push(carruselAux1,carruselAux2,carruselAux5,carruselAux4,carruselAux3);
		break;
	case 2:
		carruselImg.push(carruselAux1,carruselAux2,carruselAux3,carruselAux4);
		break;
	case 3:
		carruselImg.push(carruselAux1,carruselAux2,carruselAux3,carruselAux4);
		break;
	case 4:
		carruselAux1={
			imagenUrl:"../imagenes/matriz6x5.png",
			tituloImagen:"Matriz 6x5",
			textoDescriptivo:"Relación entre Provabilidad y Severidad"
	};
		carruselImg.push(carruselAux1);
		break;
	case 5:
		carruselImg.push(carruselAux1,carruselAux2,carruselAux3,carruselAux4);
		break;
	}
	
	
	
	var textoAyudaIperc="";
	switch(parametroId){
	case 1: 
		textoAyudaIperc="Utilizado por empresas que desean " +
				"evaluar riesgos o consecuencias sin ningún tipo de control";
		break;
	case 2: 
		textoAyudaIperc="Divide la evaluación de la probabilidad en 4 factores:" +
				"'Personas Expuestas', 'Índice de Riesgo', 'Índice de Capacitacion' y 'Índice de Severidad' " +
				"según la matriz modelo 2 de la RM 050, Ley 29783 (válido sólo para la matriz 3x3";
		break;
	case 3: 
		textoAyudaIperc="Añade el factor frecuencia en cada evaluación, " +
				"seguiendo la lógica de la matriz modelo 3 de la RM 050, Ley 29783 (válido sólo para la matriz 6x6)";
		break;
	case 4: 
		textoAyudaIperc=textoPrueba;
		$("#modalAyudaIperc .modal-content").css({"height":"620px",width:"720px"});
			break;	
			
	}
	$("#modalAyudaIpercBody").html(textoAyudaIperc);
	$("div .carrusel .product").remove();
	for(var i=0;i<carruselImg.length;i++){
		var imagen=carruselImg[i].imagenUrl;
		var descripcion=carruselImg[i].textoDescriptivo;
		var tituloImagen=carruselImg[i].tituloImagen;;
		$("div .carrusel").append(
				'   <div class="product" id="product_'+i+'">'
				+'         <img class="img_carrusel" src="'+imagen+'"  /><h2>'
					+tituloImagen+'</h2><br>        '+descripcion+''
				
					+'  </div>');
		
	}
	
	iniciarCarrusel(carruselImg.length,1);
	
}