var preguntas;
var buenasTotal;
var parcialTotal;
var malasTotal;
var noAplicanTotal;
var contador2=0;

 var tipoVerificacionId;
 var tablaFijada=false;

function listarVerificaciones() {
	resizeDivGosst("wrapper");
	$("#tituloAuditoria").html("Lista de verificaci&oacute;n "+"'"+ nombreTipoAudi+"' "+"' "+ nombreAudi+"' "+convertirFechaNormal(auditoriaFecha));
	buenasTotal = 0;
	parcialTotal = 0;
	malasTotal = 0;
	noAplicanTotal = 0;
	preguntas = [];
	 
	$("#divVerif").show();
	$("#divAudi").hide();

	removerBotones();
	$("#fsBotones")
			.append(
					"<button id='btnRegresar' type='button' class='btn btn-success' title='Regresar a listado de auditorias'>"
							+ "<i class='fa fa-sign-out fa-rotate-180 fa-2x'></i>"
							+ "</button>");
	$("#fsBotones")
	.append(
			"<button id='btnWordAudi' type='button' class='btn btn-success btn-word' title='Lista en Word'>"
					+ "<i class='fa fa-file-word-o fa-2x'></i>"
					+ "</button>");
	$("#btnRegresar").attr("onclick", "javascript:regresarAuditoria();");
	$("#btnWordAudi").on("click",function(){
		window.open(URL
				+ "/auditoria/exportable/auditoria?auditoriaId="
				+ auditoriaId 
			);
	});
	var dataParam = {
		auditoriaId : auditoriaId,
		auditoriaTipo : auditoriaTipo
	};

	callAjaxPost(URL + '/auditoria/verificaciones', dataParam,
			procesarDataDescargadaVerificaciones);
	 

}


function verListaVerif(tipoId){
	var filaId;
	 $("#tblVerif tbody tr").each(function (index) 
		        {
		 			 filaId=$(this).prop("id"); 
		 			var tipoAux=filaId.split('a');
		 			var tipoAux2=tipoAux[1];
		        	if(tipoId==tipoAux2){
		        		$(this).toggle();
		        	}
		           
		           
		        })
		      
}

function procesarDataDescargadaVerificaciones(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		preguntas = data.list;

		$("#tblVerif tbody tr").remove();
		var fila = 0;
		var tipoVerificacion = "";
		var restriccionId;
		if(auditoriaClasif==1){
			restriccionId=2;
		}else{
			restriccionId=3;
		}
		for (index = 0; index < preguntas.length; index++) {
			fila++;
			if (tipoVerificacion != preguntas[index].tipoVerificacion) {
				tipoVerificacion = preguntas[index].tipoVerificacion;
				$("#tblVerif tbody").append(
						"<tr onclick='verListaVerif("+preguntas[index].tipoVerificacionId+")'>" + "<td colspan='10'  class='info'>"
								+ preguntas[index].tipoVerificacion + "</td>"
								+ "</tr>");
			}
			$("#tblVerif tbody")
					.append(

							"<tr id='trv"
									+ preguntas[index].verificacionId
									+ "a"+preguntas[index].tipoVerificacionId+"'>"
									+ "<td id='tdid"
									+ preguntas[index].verificacionId
									+ "'>"
									+ fila
									+ "</td>"

									+ "<td id='tdnom"
									+ preguntas[index].verificacionId
									+ "'>"
									+ preguntas[index].verificacion
									+ "</td>"

									+ "<td id='tdaplica"
									+ preguntas[index].verificacionId
									+ "'>"
									+ "<input type='checkbox' class='check' "
									+ "id='chckaplica"
									+ preguntas[index].verificacionId
									+ "' "
									+ preguntas[index].chckAplica
									+ " onclick='javascript:guardarCeldaDeshabilitarPregunta("
									+ preguntas[index].verificacionId+","+preguntas[index].tipoVerificacionId
									+ ")'>"
									+ "</td>"

									

									+ "<td id='tdsi"
									+ preguntas[index].verificacionId
									+ "'>"
									+ "<input type='radio' "
									+ "name='rpta"
									+ preguntas[index].verificacionId
									+ "' "
									+ "id='rptasi"
									+ preguntas[index].verificacionId
									+ "' "
									
									+ " value='1' onclick='javascript:guardarRespuestaRadioButton("
									+ preguntas[index].verificacionId+","+preguntas[index].tipoVerificacionId
									+ ")'"+ preguntas[index].chckSi+">"
									+ "</td>"

									+ "<td id='tdparc"
									+ preguntas[index].verificacionId
									+ "'>"
									+ "<input type='radio' "
									+ "name='rpta"
									+ preguntas[index].verificacionId
									+ "' "
									+ "id='rptaparc"
									+ preguntas[index].verificacionId
									+ "' "
									
									+ " value='0.5' onclick='javascript:guardarRespuestaRadioButton("
									+ preguntas[index].verificacionId+","+preguntas[index].tipoVerificacionId
									+ ")'"+ preguntas[index].chckParc+">"
									+ "</td>"

									+ "<td id='tdno"
									+ preguntas[index].verificacionId
									+ "'>"
									+ "<input type='radio' "
									+ "name='rpta"
									+ preguntas[index].verificacionId
									+ "' "
									+ "id='rptano"
									+ preguntas[index].verificacionId
									+ "' "
									
									+ " value='0'  onclick='javascript:guardarRespuestaRadioButton("
									+ preguntas[index].verificacionId+","+preguntas[index].tipoVerificacionId
									+ ")'" + preguntas[index].chckNo+
											">"
									+ "</td>"

									+ "<td id='tdriesgo"
									+ preguntas[index].verificacionId
									+ "'>"
									+ "<input type='checkbox' class='check' "
									+ "id='chckriesgo"
									+ preguntas[index].verificacionId
									+ "' onclick='javascript:guardarRespuesta("
									+ preguntas[index].verificacionId
									+ ")' "
									+ preguntas[index].chckRiesgo
									+ ">"
									+ "</td>"

									+ "<td id='tdcom"
									+ preguntas[index].verificacionId
									+ "'>"
									+ "<textarea class='form-control' rows='1'  onfocus='javascript:aparecerBtnGuardCom("
									+ preguntas[index].verificacionId
									+ ")' "
									+ "id='inputcom"
									+ preguntas[index].verificacionId
									+ "'>"
									+ preguntas[index].comentario
									+ "</textarea></td>"

									+ "<td id='tdacc"
									+ preguntas[index].verificacionId
									+ "'>"+preguntas[index].iconoEstadoAccion+"<a href='#' onclick='javascript:agregarAccionMejora(2, "
						+ preguntas[index].detalleAuditoriaId + ","+restriccionId+");'>"+preguntas[index].nombreAccionMejora+"</a></td>"

									+ "<td id='tdevi"
									+ preguntas[index].verificacionId
									+ "'><a href='"
									+ URL
									+ "/auditoria/verificaciones/evidencia?verificacionId="
									+ preguntas[index].verificacionId
									+ "&auditoriaId="
									+ auditoriaId
									+ "' "
									+ "target='_blank' id='descargarimagen"
									+ preguntas[index].verificacionId
									+ "'>"+preguntas[index].evidenciaNombre+"</a>"
									+ "<br/><a href='#' onclick='javascript:mostrarCargarImagen("
									+ preguntas[index].verificacionId
									+ ")'  id='subirimagen"
									+ preguntas[index].verificacionId
									+ "'>"+"Subir"+"</a></td>" + "</tr>");
			deshabilitarPregunta(preguntas[index].verificacionId,preguntas[index].tipoVerificacionId);
			
			$("#tdnom"+ preguntas[index].verificacionId).css({"text-align":"left"});
			
			
			if (preguntas[index].bitAplica == 1) {
				if (preguntas[index].bitSi == 1) {
					buenasTotal++;
				}

				if (preguntas[index].bitParc == 1) {
					parcialTotal++;
				}

				if (preguntas[index].bitNo == 1) {
					malasTotal++;
				}
				deshabilitarRiesgoPotencial(preguntas[index].verificacionId);
			} else {
				noAplicanTotal++;
			}
			resaltarInput(preguntas[index].chckNo,"tdno"+preguntas[index].verificacionId,"red");
			resaltarInput(preguntas[index].chckParc,"tdparc"+preguntas[index].verificacionId,"yellow");
			resaltarInput(preguntas[index].chckSi,"tdsi"+preguntas[index].verificacionId,"green");
		}

		$("#indicador1").html("<strong>"+buenasTotal+"</strong>");
		$("#indicador2").html("<strong>"+parcialTotal+"</strong>");
		$("#indicador3").html("<strong>"+malasTotal+"</strong>");
		$("#indicador4").html(
				preguntas.length - noAplicanTotal - buenasTotal - parcialTotal
						- malasTotal);
		$("#indicador5").html(
				indicador2);
		$("#indicador6")
				.html(
						nivelAvance2);
		
		break;
	default:
		alert("Ocurrió un error al traer las verificaciones!");
	}
	
	if(!tablaFijada){
		tablaFijada=true;
		goheadfixedY('#tblVerif',"#wrapper");
	}

		
	$(".info").css({"text-align":"center"});
	  setTextareaHeight($('textarea'));
}

function resaltarInput(isChecked,inputId,color){
	if(isChecked=="checked"){
	$("#"+inputId).css("background-color",color);
	}
}

function regresarAuditoria() {
	$("#divVerif").hide();
	$("#divAudi").show();
	removerBotones();
	crearBotones();
	$("#fsBotones")
			.append(
					"<button id='btnListarVerificaciones' type='button' class='btn btn-success' title='Listado de Verificaciones del tipo de auditoria'>"
							+ "<i class='fa fa-tasks fa-2x'></i></button>");
	habilitarBotonesEdicion();
	$("#fsBotones")
	.append(
			"<button id='btnGenReporte' type='button' class='btn btn-success' title='Generar Informe'>"
					+ "<i class='fa fa-file-word-o fa-2x'></i>"
					+ "</button>");

	$("#btnGenReporte").show();
	$("#btnGenReporte").attr("onclick","javascript:generarReporteAuditoria();");
	$("#btnNuevo").attr("onclick", "javascript:nuevoAuditoria();");
	$("#btnCancelar").attr("onclick", "javascript:cancelarAuditoria();");
	$("#btnGuardar").attr("onclick", "javascript:guardarAuditoria();");
	$("#btnEliminar").attr("onclick", "javascript:eliminarAuditoria();");
	$("#btnListarVerificaciones").attr("onclick",
			"javascript:listarVerificaciones();");
}

function uploadEvidencia(verificacionId) {
	var inputFileImage = document.getElementById("fileEvi");
	var file = inputFileImage.files[0];
	var data = new FormData();
	if (file.size > bitsEvidenciaVerificacion) {
		alert("Solo se pueden subir archivos menores a "+bitsEvidenciaVerificacion/1000000+" MB");
	} else {
		data.append("fileEvi", file);
		data.append("verificacionId", verificacionId);
		data.append("auditoriaId", auditoriaId);

		var url = URL + '/auditoria/verificaciones/evidencia/save';
		$.blockUI({message:'cargando...'});
		$
				.ajax({
					url : url,
					xhrFields: {
			            withCredentials: true
			        },
					type : 'POST',
					contentType : false,
					data : data,
					processData : false,
					cache : false,
					success : function(data, textStatus, jqXHR) {
						$.unblockUI();
						switch (data.CODE_RESPONSE) {
						case "06":
							sessionStorage.clear();
							document.location.replace(data.PATH);
							break;
						default:
							console.log('Se subio el archivo correctamente.');
							$('#mdUpload').modal('hide');
							$("#modalProg").show();
							$("#modalTree").hide();
							$("#modalUpload").hide();
							listarVerificaciones();
						}

					},
					error : function(jqXHR, textStatus, errorThrown) {
						$.unblockUI();
						alert("Ups, tenemos problemas tecnicos. Intente más tarde. Error: "
								+ errorThrown);
						console.log('xhRequest: ' + jqXHR + "\n");
						console.log('ErrorText: ' + textStatus + "\n");
						console.log('thrownError: ' + errorThrown + "\n");
					}
				});
		
	}
}

function mostrarCargarImagen(verificacionId) {
	$('#mdUpload').modal('show');
	$('#btnUploadEvidencia').attr("onclick",
			"javascript:uploadEvidencia(" + verificacionId + ");");
}

$('#mdUpload').on('show.bs.modal', function(e) {
	cargarModalArea();
});

function aparecerBtnGuardRegLegal(verificacionId) {
	if (!($("#btnGuardarCelda").length > 0)) {
		$("#tdreflegal" + verificacionId)
				.append(
						"<button id='btnGuardarCelda' type='button' class='btn  btn-warning' "
								+ "title='Guardar Referencia Legal' onclick='javascript:guardarRespuesta("
								+ verificacionId
								+ ")'>"
								+ "<i class='fa fa-floppy-o fa-lg'></i></button>");
	}
}

function aparecerBtnGuardCom(verificacionId) {
	if (!($("#btnGuardarCelda").length > 0)) {
		$("#tdcom" + verificacionId)
				.append(
						"<button id='btnGuardarCelda' type='button' class='btn  btn-warning' "
								+ "title='Guardar Comentario' onclick='javascript:guardarRespuesta("
								+ verificacionId
								+ ")'>"
								+ "<i class='fa fa-floppy-o fa-lg'></i></button>");
	}
}

function guardarCeldaDeshabilitarPregunta(verificacionId,tipoId) {
	tipoVerificacionId=tipoId;
	deshabilitarPregunta(verificacionId,tipoId);
	guardarRespuesta(verificacionId);
	

	calcularSubTotales();

}

function deshabilitarPregunta(verificacionId,tipoId) {
	
	var bitAplica =$("#chckaplica" + verificacionId).is(':checked');
	
	if (bitAplica) {
		
		$("#trv" + verificacionId + "a"+tipoId+" :input").attr("disabled", false);
		$("#descargarimagen" + verificacionId).attr("onclick", "");
		$("#subirimagen" + verificacionId).attr("onclick", "javascript:mostrarCargarImagen("+verificacionId+")");
		deshabilitarRiesgoPotencial(verificacionId);
	} else {
		
		$("#trv" + verificacionId + "a"+tipoId+" :input").attr("disabled", true);
		$("#descargarimagen" + verificacionId).attr("onclick", "return false;");
		$("#subirimagen" + verificacionId).attr("onclick", "return false;");
		$("#chckaplica" + verificacionId).attr("disabled", false);
	}
}

function guardarRespuestaRadioButton(verificacionId,tipoId) {
	tipoVerificacionId=tipoId;
	deshabilitarRiesgoPotencial(verificacionId);
	guardarRespuesta(verificacionId);
	calcularSubTotales();
}

function deshabilitarRiesgoPotencial(verificacionId) {
	var bitRespuesta = parseFloat($(
			"input[name='rpta" + verificacionId + "']:checked").val());
	if(bitRespuesta<1){
		$("#chckriesgo" + verificacionId).prop("disabled", true);
	}else{
		$("#chckriesgo" + verificacionId).prop("disabled", false);
	}
}

/**
 * 
 */
function calcularSubTotales() {
	buenasTotal = 0;
	parcialTotal = 0;
	malasTotal = 0;
	noAplicanTotal = 0;
	for (var index = 0; index < preguntas.length; index++) {
		var bitAplica = +$("#chckaplica" + preguntas[index].verificacionId).is(
				':checked');
		var bitRespuesta = parseFloat($(
				"input[name='rpta" + preguntas[index].verificacionId
						+ "']:checked").val())
		if (bitAplica == 1) {
			if (bitRespuesta == 1) {
				buenasTotal++;
			}

			if (bitRespuesta == 0.5) {
				parcialTotal++;
			}

			if (bitRespuesta == 0) {
				malasTotal++;
			}
		} else {
			noAplicanTotal++;
		}
	}

	$("#indicador1").html("<strong>"+buenasTotal+"</strong>");
	$("#indicador2").html("<strong>"+parcialTotal+"</strong>");
	$("#indicador3").html("<strong>"+malasTotal+"</strong>");
	$("#indicador4").html(
			preguntas.length - noAplicanTotal - buenasTotal - parcialTotal
					- malasTotal);
	$("#indicador5").html(
			((buenasTotal * 1 + parcialTotal * 0.5) / (buenasTotal
					+ parcialTotal + malasTotal)).toFixed(2));
	$("#indicador6")
			.html(
					(((buenasTotal + parcialTotal + malasTotal) / (preguntas.length - noAplicanTotal)) * 100)
							.toFixed(2));
}

function guardarRespuesta(verificacionId) {
	var bitAplica = $("#chckaplica" + verificacionId).is(':checked');
	
	var bitRespuesta=null ;
	if(bitAplica){
	bitRespuesta= parseFloat($(
			"input[name='rpta" + verificacionId + "']:checked").val());
	}
	var bitRiesgo = $("#chckriesgo" + verificacionId).is(':checked');
	var referenciaLegal = $("#inputreflegal" + verificacionId).val();
	referenciaLegal="";
	var comentario = $("#inputcom" + verificacionId).val();
	
	var dataParam = {
		verificacionId : verificacionId,
		bitAplica : pasarBoolInt(bitAplica),
		bitRiesgo : pasarBoolInt(bitRiesgo),
		bitRespuesta : bitRespuesta,
		
		referenciaLegal : referenciaLegal,
		comentario : comentario,
		auditoriaId : auditoriaId
	};
var dataParamBefore="trv"+verificacionId+"a"+tipoVerificacionId;
	callAjaxPostNoLoad(URL + '/auditoria/verificaciones/respuesta/save', dataParam,
			procesarResultadoGuardarVerif,loadingCelda,dataParamBefore);
}

function procesarResultadoGuardarVerif(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		$("#btnGuardarCelda").remove();
		listarVerificaciones();
		break;
	default:
		alert("Ocurrió un error al guardar la auditoria!");
	}
}