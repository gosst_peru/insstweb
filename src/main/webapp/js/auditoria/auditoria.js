var banderaEdicion;
var auditoriaId;
var auditoriaTipo;
var auditoriaFecha;
var numAreas;
var listAudiTipo;
var listAreasSelecccionados;
var listAreas;
var auditoriaClasif;
var nombreAudi;
var nombreTipoAudi;
var listDniNombre;
var listanombretab;
var listAudiFull;
var numRpta;
var indicador2;
var nivelAvance2;

var auditoriaEstado;
var auditoriaEstadoNombre;
$(document).ready(
		function() {
			auditoriaClasif = location.pathname.substring(location.pathname
					.lastIndexOf("/") + 1)=='auditoria.html'?1:2;
			// getUrlParameter("mdfId")
			insertMenu();
			cargarPrimerEstado();
			$("#mdVerif").load("agregaraccionmejora.html");
			
			
			 
			$("#btnClipTabla").on("click",function(){
				
						var listFullTabla="";
						listFullTabla="Nombre" +"\t"
						+"CheckList utilizado" +"\t"
						+"Fecha planificada" +"\t"
						+"Fecha Real" +"\t"
						+"Inversión" +"\t"
						+"Responsable" +"\t"
						+"Conformidad" +"\t"
						+"Nivel de Avance" +"\t"
						+"Acciones de mejora Asociadas" +"\t"
						+"Estado de implementacion" +" \n";
						for (var index = 0; index < listAudiFull.length; index++){
							var rptaSi=numRpta[index].rptaSi;
							var rptaParc=numRpta[index].rptaParcial;
							var rptaNo=numRpta[index].rptaNo;
							var rptaRP=numRpta[index].rptaRP;
							var rptaFaltante=numRpta[index].rptaRespondida;
							var pregLlenada=numRpta[index].pregAvance;
							var pregTotal=listAudiFull[index].numPreguntas;


							 nivelAvance2=rptaFaltante+"/"+pregTotal;

							var puntaje=rptaSi*1+rptaParc*0.5+rptaRP*0.75+rptaNo*0;
							var puntajeMax=rptaSi+rptaParc+rptaRP+rptaNo;
							indicador2= pasarDecimalPorcentaje(puntaje/puntajeMax,2);
							
							listFullTabla=listFullTabla
							+listAudiFull[index].auditoriaNombre.replace("\t","")+"\t"
							+listAudiFull[index].auditoriaTipoNombre+"\t"
							+listAudiFull[index].fechaPlanificadaNombre+"\t"
							+listAudiFull[index].auditoriaFechaTexto+"\t"
							+listAudiFull[index].inversionAuditoria +"\t"
							+listAudiFull[index].auditoriaResponsable.replace("\t","")+"\t"
							+pasarDecimalPorcentaje(puntaje/puntajeMax,2)+"\t"
							+nivelAvance2.replace("/","//")+"\t"
							+ listAudiFull[index].accionesCompletas+" // "+listAudiFull[index].accionesTotales +"\t"
							+listAudiFull[index].estadoImplementacionNombre
							+"\n";
						

						}
						copiarAlPortapapeles(listFullTabla); 
				
				alert("Se han guardado al clipboard la tabla de este módulo");
		}
		
		


);
		});
function cargarPrimerEstado() {
		$("#divVerif").hide();
	$("#divAudi").show();
	resizeDivGosst("wrapper2",0);
	banderaEdicion = false;
	auditoriaId = 0;
	auditoriaTipo = 0;
	listAudiTipo=[];
	listAreas=[];
	listAreasSelecccionados=[];
	
	removerBotones();
	crearBotones();
	$("#fsBotones")
	.append(
			"<button id='btnListarVerificaciones' type='button' class='btn btn-success' title='Listado de Verificaciones del tipo de auditoria'>"
					+ "<i class='fa fa-tasks fa-2x'></i>"
					+ "</button>");
	$("#fsBotones")
	.append(
			"<button id='btnClipTabla' type='button' class='btn btn-success' title='Tabla Clipboard' >"
					+ "<i class='fa fa-clipboard fa-2x'></i>" + "</button>");
	deshabilitarBotonesEdicion();
	$("#btnListarVerificaciones").hide();
	$("#fsBotones")
	.append(
			"<button id='btnGenReporte' type='button' class='btn btn-success btn-word' title='Generar Informe'>"
					+ "<i class='fa fa-file-word-o fa-2x'></i>"
					+ "</button>");
	$("#fsBotones")
	.append(
			"<button id='btnHallazgo' type='button' class='btn btn-success' title='Listar Hallazgos'>"
					+ "<i class='fa fa-list fa-2x'></i>"
					+ "</button>");
$("#btnHallazgo").hide();
	$("#btnGenReporte").hide();
	$("#btnGenReporte").attr("onclick","javascript:generarReporteAuditoria();");
	$("#btnNuevo").attr("onclick", "javascript:nuevoAuditoria();");
	$("#btnCancelar").attr("onclick", "javascript:cancelarAuditoria();");
	$("#btnGuardar").attr("onclick", "javascript:guardarAuditoria();");
	$("#btnEliminar").attr("onclick", "javascript:eliminarAuditoria();");
	$("#btnListarVerificaciones").attr("onclick", "javascript:listarVerificaciones();");

	var dataParam = {
		idCompany : sessionStorage.getItem("gestopcompanyid"),
		auditoriaClasifId : auditoriaClasif
	};
	
	callAjaxPost(URL + '/auditoria', dataParam,
			procesarDataDescargadaPrimerEstado);
	
	var dataParam2 = {
				idCompany: sessionStorage.getItem("gestopcompanyid")
		};
	
	callAjaxPost(URL + '/accidente/trab', dataParam2,
			procesarDataDescargadaPrimerEstado2);
	
	$("#btnUpload").hide();
}

var colorSi="green";
var colorNo="red";
var colorParc="yellow";
var colorRP="orange";

function colorPuntaje(puntaje){
	var color="black";
		if(puntaje<=0.66){
			color="orange"
		}
		if(puntaje<=0.34){
		color="red"
	}
		if(puntaje>0.66){
			color="green"
		}
	return color;
}

function procesarDataDescargadaPrimerEstado(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		var list = data.list;
		 numRpta=data.listRpta;
		listAudiTipo = data.listAudiTipo;
		listAudiFull=list;
		$("#h2Titulo")
				.html(
						"<a onclick='javascript:cargarPrimerEstado();' href='#'>Auditoria</a>");
		$("#tblAudi tbody tr").remove();

		for (index = 0; index < list.length; index++) {
var rptaSi=numRpta[index].rptaSi;
var rptaParc=numRpta[index].rptaParcial;
var rptaNo=numRpta[index].rptaNo;
var rptaRP=numRpta[index].rptaRP;
var rptaFaltante=numRpta[index].rptaRespondida;

var conformidadAux= "<span style='font-weight:bold;color:"+colorSi+"'>"+rptaSi+"</span>-"
						+"<span style='font-weight:bold;color:"+colorRP+"'>"+rptaRP+"</span>-"
						+"<span style='font-weight:bold;color:"+colorParc+"'>"+rptaParc+"</span>-"
							+"<span style='font-weight:bold;color:"+colorNo+"'>"+rptaNo+"</span>-"
							+"<span style='font-weight:bold;color:"+"black"+"'>"+rptaFaltante+"</span>";
var pregLlenada=numRpta[index].pregAvance;
var pregTotal=list[index].numPreguntas;


 nivelAvance2=rptaFaltante+"/"+pregTotal;

var puntaje=rptaSi*1+rptaParc*0.5+rptaRP*0.75+rptaNo*0;
var puntajeMax=rptaSi+rptaParc+rptaRP+rptaNo;
indicador2= pasarDecimalPorcentaje(puntaje/puntajeMax,2);
var auditoriaTipoAux = listAudiFull[index].auditoriaTipo;
			$("#tblAudi tbody").append(

					"<tr id='tr" + list[index].auditoriaId
							+ "' onclick='javascript:editarAudi("
							+index+")' >"

							+ "<td id='tdnom" + list[index].auditoriaId
							+ "'>" + list[index].auditoriaNombre + "</td>"
							
							+ "<td id='tdtipo" + list[index].auditoriaId
							+ "'>" + list[index].auditoriaTipoNombre
							+ "</td>" 
							
							+ "<td id='tdarea"
							+ list[index].auditoriaId + "'>" 
							+list[index].numAreas +		"</td>"
							
							+ "<td id='tdfechaPlan"
							+ list[index].auditoriaId + "'>"+list[index].fechaPlanificadaNombre+"</td>"  
							+ "<td id='tdhorapla"
							+ list[index].auditoriaId + "'>"+list[index].horaPlanificadaTexto+"</td>"  
							
							+ "<td id='tdfecha"
							+ list[index].auditoriaId + "'>"+list[index].auditoriaFechaTexto+"</td>" 
							+ "<td id='tdinversion"
							+ list[index].auditoriaId + "'>S/ "+list[index].inversionAuditoria+"</td>" 
							+ "<td id='tdres"
							+ list[index].auditoriaId + "'>"
							+ list[index].auditoriaResponsable + "</td>"
							
							+ "<td id='tdcar"
							+ list[index].auditoriaId + "'>"
							+ list[index].auditoriaCargo + "</td>"

							+ "<td id='tdconf"
							+ list[index].auditoriaId + "' style='color:"+
							colorPuntaje(puntaje/puntajeMax)+";font-weight:bold'>"
							+(auditoriaTipoAux==94?"":pasarDecimalPorcentaje(puntaje/puntajeMax,2) )+ "</td>"
							
							+ "<td id='tdcomp"
							+ list[index].auditoriaId + "'>"
							+ (auditoriaTipoAux==94?"":nivelAvance2)+"</td>"
							+ "<td id='tdacc"
							+ list[index].auditoriaId + "'>"
							+(auditoriaTipoAux==94?list[index].numAccionesCompletaHallazgo+" / "+list[index].numAccionesTotalHallazgo
									: list[index].accionesCompletas+" / "+list[index].accionesTotales)  +"</td>"
							+ "<td id='tdeviAudi"
							+ list[index].auditoriaId + "'>"
							+ list[index].evidenciaNombre+"</td>"
							+ "<td id='estadoAudi"
							+ list[index].auditoriaId + "'>"
							+ list[index].estadoImplementacionNombre+"</td>"
							+ "</tr>");
		}
		formatoCeldaSombreable(true);
		goheadfixed("#tblAudi","#wrapper2");
		completarBarraCarga();
		if(getSession("linkCalendarioAuditoriaId")!=null){
			
			list.every(function(val,index3){
				
				if(val.auditoriaId==parseInt(getSession("linkCalendarioAuditoriaId"))){
					editarAudi(index3);
					
					sessionStorage.removeItem("linkCalendarioAuditoriaId");
					return false;
				}else{
					return true;
				}
			});
			
		}
		break;
	default:
		alert("Ocurrió un error al traer las auditorias!");
	}
	
}


function procesarDataDescargadaPrimerEstado2(data){
switch (data.CODE_RESPONSE) {
	
	case "05":
	
		 listDniNombre=data.listDniNombre;
		
		
	
		
		break;
	default:
		alert("Ocurrió un error al traer los trabajdores!");
	}

listanombretab=listarStringsDesdeArray(listDniNombre, "nombre");
}




function editarAudi(pindex) {
	
	if (!banderaEdicion) { 
		auditoriaId = listAudiFull[pindex].auditoriaId;
		auditoriaTipo = listAudiFull[pindex].auditoriaTipo;
		auditoriaFecha = listAudiFull[pindex].auditoriaFecha;
		numAreas=listAudiFull[pindex].numAreas;
		listAreasSelecccionados=[];
		listAreas=[];
		var fechaPlan=listAudiFull[pindex].fechaPlanificada;
		var inversionAudi=listAudiFull[pindex].inversionAuditoria;
		
		var rptaSi=numRpta[pindex].rptaSi;
		var rptaParc=numRpta[pindex].rptaParcial;
		var rptaNo=numRpta[pindex].rptaNo;
		var rptaRP=numRpta[pindex].rptaRP;
		var rptaFaltante=numRpta[pindex].rptaRespondida;
		var pregLlenada=numRpta[pindex].pregAvance;
		var pregTotal=listAudiFull[pindex].numPreguntas;
		var horaPlanificada=listAudiFull[pindex].horaPlanificada;

		 nivelAvance2=pregLlenada+"/"+pregTotal;

		var puntaje=rptaSi*1+rptaParc*0.5+rptaRP*0.75+rptaNo*0;
		var puntajeMax=rptaSi+rptaParc+rptaRP+rptaNo;
		indicador2= pasarDecimalPorcentaje(puntaje/puntajeMax,2);
		formatoCeldaSombreableTabla(false,"tblAudi");
		var name = $("#tdnom" + auditoriaId).text();
		var nameTipoAudi = listAudiFull[pindex].auditoriaTipoNombre;
		var nombreEvidencia=listAudiFull[pindex].evidenciaNombre;
		nombreAudi=name;
		nombreTipoAudi=nameTipoAudi;
		$("#tdeviAudi" + auditoriaId)
		.html(
				"<a id='linkEvidencia' " +
				"href='"+URL 
				+ "/auditoria/evidencia?auditoriaId="+auditoriaId+"' target='_blank' >" +
				nombreEvidencia+	"</a><br/>" +
				"<a id='linkEvidencia' href='#' onclick='javascript:editarEvidenciaAuditoria();'>Subir</a>");
		$("#tdhorapla" + auditoriaId)
		.html(
				"<input type='time' id='inputHora' class='form-control'  value='"
						+ horaPlanificada + "'>");
		$("#btnGenReporte").show();
		$("#tdinversion"+auditoriaId).html(
				"<input type='number' id='inputInversion' class='form-control' value='"
				+ inversionAudi + "'>"
		
		);
		$("#tdnom" + auditoriaId)
				.html(
						"<input type='text' id='inputNom' class='form-control' placeholder='Nombre' autofocus='true' value='"
								+ name + "'>");
		var name1 = $("#tdres" + auditoriaId).html();
		$("#tdres" + auditoriaId)
				.html(
						"<input type='text' id='inputResp' class='form-control' placeholder='Responsable' value='"
								+ name1 + "'>");
		
		$("#inputResp").on("autocompleteclose",function(){
			var nombreTrab=$("#inputResp").val();
			
			var puestoNombre;
				for (index = 0; index < listDniNombre.length; index++) {
					
					if(listDniNombre[index]["nombre"]==nombreTrab){
						puestoNombre=listDniNombre[index]["puestoNombre"]
					$("#inputCar").val(puestoNombre);
					
					break;
					}else{
						$("#inputCar").val("");
					
					
					}
				}
			
		})
			
		var name2 = $("#tdcar" + auditoriaId).text();
		$("#tdcar" + auditoriaId)
				.html(
						"<input type='text' id='inputCar' class='form-control' placeholder='Cargo' value='"
								+ name2 + "'>");
		
		$("#tdarea" + auditoriaId).html("<a href='#' onclick='javascript:seleccionarArea();'>Areas</a>");
		/** ************************************************************************* */
		
	
		$("#tdfecha" + auditoriaId).html(
		"<input type='date' id='inputFecha' onchange='hallarEstadoImplementacion("+auditoriaId+")' class='form-control'>");
		
		$("#tdfechaPlan" + auditoriaId).html(
		"<input type='date' id='inputFechaPlan' onchange='hallarEstadoImplementacion("+auditoriaId+")' class='form-control'>");
	
		$("#inputFecha").val(convertirFechaInput(auditoriaFecha));
		$("#inputFechaPlan").val(convertirFechaInput(fechaPlan));
		hallarEstadoImplementacion(auditoriaId);
		
		cargarModalArea();
		banderaEdicion = true;
		habilitarBotonesEdicion();
		$("#btnListarVerificaciones").show();
		ponerListaSugerida("inputResp",listanombretab,true);
		$("#inputResp").val(name1);
		cambiarAcordeTipoAuditoria(auditoriaTipo);
		var dataParam2={
				auditoriaId:auditoriaId
		}
		callAjaxPostNoLoad(URL + '/auditoria/acciones', dataParam2,
				function(data){
			if(data.CODE_RESPONSE="05"){
			var accionesComple=	$("#tdacc"+auditoriaId).html();
			$("#tdacc"+auditoriaId).html("<a>"+accionesComple+"</a>");
			$("#tdacc"+auditoriaId+" a").on("click",function(){
				$("#modalAccionesAuditoria").modal("show");
				$("#btnClipTablaAcciones").remove();
				$("#modalAccionesAuditoria .modal-body").prepend(
						"<button id='btnClipTablaAcciones' type='button' class='btn btn-success clipGosst' title='Tabla Clipboard' >"
						+ "<i class='fa fa-clipboard fa-2x'></i>" + "</button>");
				$("#btnClipTablaAcciones").on("click",function(){
					
					copiarAlPortapapeles(obtenerDatosTablaEstandarNoFija("tblAccionesAudi") ,"btnClipTablaAcciones");
					 
					
					alert("Se han guardado al clipboard " + ""
							+ " las acciones asociadas");
				});
				var list=data.list;
				var list2=data.list2;
				list=list.concat(list2);
				$("#tblAccionesAudi tbody tr").remove();
				list.forEach(function(value,index){
					$("#tblAccionesAudi tbody").append(
							"<tr>" +
							"<td>"+list[index].tipoAccionNombre+"</td>" +
							"<td>"+list[index].descripcion+"</td>" +
							"<td>"+list[index].responsableNombre+"</td>" +

							"<td>"+list[index].fechaRevisionTexto+"</td>" +
							"<td>"+list[index].fechaRealTexto+"</td>" +
							"<td>"+list[index].inversionAccion+"</td>" +
							"<td style='font-weight:800;color:"+list[index].estadoColor+"'>"+list[index].estadoCumplimientoNombre+"</td>" +
							"</tr>"
					)
				});
				
			})
			}else{
				console.log("NOP")
			}
		});
	}
	
}
function editarEvidenciaAuditoria(){
	
	$("#modalUploadAuditoria").modal("show");
}

function nuevoAuditoria() {
	if (!banderaEdicion) {
		var selectAudiTipo = crearSelectOneMenu("selAudiTipo", "",
				listAudiTipo, "-1", "audiTipoId", "audiTipoNombre");
		auditoriaId = 0;
		$("#tblAudi tbody:first")
				.append(
						"<tr id='0'><td><input type='text' id='inputNom' class='form-control' placeholder='Nombre' autofocus='true'></td>"
								+ "<td>"
								+ selectAudiTipo
								+ "</td>"
								+ "<td>...</td>"
								+ "<td>"
								+"<input type='date' id='inputFechaPlan' onchange='hallarEstadoImplementacion("+auditoriaId+")' class='form-control'>"
								+"</td>"
								+ "<td>"
								+"<input type='time' id='inputHora' onchange='hallarEstadoImplementacion("+auditoriaId+")' class='form-control'>"
								+"</td>"
								
								+ "<td><input type='date' id='inputFecha' onchange='hallarEstadoImplementacion("+auditoriaId+")' class='form-control'></td>"
								+ "<td><label>S/ </label><input type='number' id='inputInversion' class='form-control'></td>"
								+ "<td><input type='text' id='inputResp' class='form-control' placeholder='Responsable'></td>"
								+ "<td><input type='text' id='inputCar' class='form-control' placeholder='Cargo'></td>"
								+ "<td>...</td>"
								+ "<td>...</td>"+ "<td>...</td>"+ "<td>...</td>"
								+ "<td id='estadoAudi0'>...</td>"
								+ "</tr>");
		ponerListaSugerida("inputResp",listanombretab,true);
		$("#inputResp").on("autocompleteclose",function(){
			var nombreTrab=$("#inputResp").val();
			
			var puestoNombre;
				for (index = 0; index < listDniNombre.length; index++) {
					
					if(listDniNombre[index]["nombre"]==nombreTrab){
						puestoNombre=listDniNombre[index]["puestoNombre"]
					$("#inputCar").val(puestoNombre);
					
					break;
					}else{
						$("#inputCar").val("");
					
					
					}
				}
			
		});
		habilitarBotonesNuevo();
		
		  
		
		$("#btnListarVerificaciones").hide();
		banderaEdicion = true;
	} else {
		alert("Guarde primero.");
	}
	
}

function cancelarAuditoria() {
	cargarPrimerEstado();
}

function eliminarAuditoria() {
	var r = confirm("¿Está seguro de eliminar la Auditoria?");
	if (r == true) {
		var dataParam = {
				auditoriaId : auditoriaId,
		};

		callAjaxPost(URL + '/auditoria/delete', dataParam,
				procesarResultadoEliminarAudi);
	}
}

function procesarResultadoEliminarAudi(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		var msj=data.mensaje;
		if(msj.length>0){
			alert( "No se puede eliminar registros que tengan asociados acciones de mejora");
		}
		cargarPrimerEstado();
		break;
	default:
		alert("Ocurrió un error al eliminar la auditoria!");
	}
}

function guardarAuditoria() {

	var campoVacio = true;
	var inversionAud=$("#inputInversion").val();
	var inputNom = $("#inputNom").val();
	var horaPlanificada=$("#inputHora").val();
	if(!$("#inputHora").val()){
		horaPlanificada=null;
	}
	var inputResp = $("#inputResp").val();
	var inputCar = $("#inputCar").val();
	
	var mes2 = $("#inputFecha").val().substring(5, 7) - 1;
	var fechatemp2 = new Date($("#inputFecha").val().substring(0, 4), mes2,
			$("#inputFecha").val().substring(8, 10));

	var inputFecha = fechatemp2;
	
	var mes1 = $("#inputFechaPlan").val().substring(5, 7) - 1;
	var fechatemp1 = new Date($("#inputFechaPlan").val().substring(0, 4), mes1,
			$("#inputFechaPlan").val().substring(8, 10));

	var inputFechaPlan = fechatemp1;
	
	var audiTipo;
	if(auditoriaId==0){
		audiTipo = $("#selAudiTipo option:selected").val();
		
	}else{
		audiTipo=auditoriaTipo;
	}
	
if(!$("#inputFechaPlan").val()){
		
	inputFechaPlan=null;
	}

if(!$("#inputFecha").val()){
	
	inputFecha=null;
}
	
	if(auditoriaId>0){
		var seleccionados = $('#jstree').jstree(true).get_checked();
		listAreasSelecccionados = [];
		for (index = 0; index < seleccionados.length; index++) {
			var trab = seleccionados[index];
			if (seleccionados[index].substr(0, 4) == "nodo") {
				listAreasSelecccionados.push(seleccionados[index]
						.substr(4, seleccionados[index].length));
			}
		}	
	}
	

	if (audiTipo == '-1' || inputNom.length == 0
			|| inputResp.length == 0) {
		campoVacio = false;
	}

	if (campoVacio) {

		var dataParam = {
			auditoriaId : auditoriaId,
			auditoriaNombre : inputNom,
			auditoriaResponsable : inputResp,
			auditoriaCargo : inputCar,
			auditoriaTipo: audiTipo,horaPlanificada:horaPlanificada,
			auditoriaFecha: inputFecha,
			areas: listAreasSelecccionados,
			estadoImplementacion: auditoriaEstado,
			inversionAuditoria:inversionAud,
			fechaPlanificada:inputFechaPlan,
			idCompany : sessionStorage.getItem("gestopcompanyid")
		};

		callAjaxPost(URL + '/auditoria/save', dataParam,
				procesarResultadoGuardarAudi);
	} else {
		alert("Debe ingresar todos los campos.");
	}
}

function procesarResultadoGuardarAudi(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		cargarPrimerEstado();
		break;
	default:
		alert("Ocurrió un error al guardar la auditoria!");
	}
}

function seleccionarArea() {
	$('#mdArea').modal('show');
}

$('#mdArea').on('show.bs.modal', function(e) {
	cargarModalArea();
});


function cargarModalArea(){
	if (listAreasSelecccionados.length === 0) {

		var dataParam = {
			auditoriaId : auditoriaId,
			idCompany : sessionStorage.getItem("gestopcompanyid")
		};

		callAjaxPost(
				URL + '/auditoria/area/jstree',
				dataParam,
				function(data) {
					switch (data.CODE_RESPONSE) {
					case "05":
						$('#jstree').jstree("destroy");

						listAreas = data.list;

						$('#jstree').jstree({
							"plugins" : [ "checkbox", "json_data" ],
							'checkbox' : {
								"keep_selected_style" : false,
								"tie_selection" : false
							},
							'core' : {
								'data' : listAreas
							}
						});


						for (index = 0; index < listAreas.length; index++) {
							if (listAreas[index].state.checked
									&& listAreas[index].id.substr(
											0, 4) == "nodo") {
								listAreasSelecccionados
										.push(listAreas[index].id
												.substr(
														4,
														listAreas[index].id.length));
							}
						}

						break;
					default:
						alert("Ocurrió un error al cargar el arbol de areas!");
					}
				});
	}
}

function hallarEstadoImplementacion(progId){
	var fechaPlanificada=$("#inputFechaPlan").val();
	var fechaReal=$("#inputFecha").val();
	var fechaHoy=obtenerFechaActual();
	if(fechaReal!=''){
		auditoriaEstado=2;
		auditoriaEstadoNombre="Completado";
	}else{
		
		if(fechaPlanificada!=""){
			var dif=restaFechas(fechaHoy,fechaPlanificada)
			if(dif<0){
				auditoriaEstado=3;
				auditoriaEstadoNombre="Retrasado";
			}else{
				auditoriaEstado=1;
				auditoriaEstadoNombre="Por implementar";
			}
			
		}else{
			
			auditoriaEstado=1;
			auditoriaEstadoNombre="Por implementar";
			
		}
	}
	
	$("#estadoAudi"+progId).html(auditoriaEstadoNombre);
	
}


function generarReporteAuditoria(){
	
switch(parseInt(auditoriaClasif)){
	
	case 1:
		if(numAreas==0){
			 alert("No hay áreas asignadas a la auditoría")
		}else{
			window.open(URL
					+ "/auditoria/informe/auditoria?auditoriaId="
					+ auditoriaId 
				);	
		}
		
		break;
	case 2:
		if(numAreas==0){
			 alert("No hay áreas asignadas a la inspección")
		}else{
			window.open(URL
					+ "/auditoria/informe/inpeccion?auditoriaId="
					+ auditoriaId +"&tipoAuditoriaId="+auditoriaTipo
				
					);	
		}
	
		break; 
	default:
		break;
	
	}
	
}
function uploadEvidenciaAuditoria(){
	var inputFileImage = document.getElementById("fileEviAuditoria");
	var file = inputFileImage.files[0];
	var data = new FormData();

	if(file.size>bitsEvidenciaAuditoria){
		alert("Lo sentimos, solo se pueden subir archivos menores a "+bitsEvidenciaAuditoria/1000000+" MB");	
	}else{
		data.append("fileEvi", file);
		data.append("auditoriaId", auditoriaId);
		$("#tdeviAudi"+auditoriaId+" ").html(file.name);
		var url = URL + '/auditoria/evidencia/save';
		$.blockUI({message:'cargando...'});
		$.ajax({
			url : url,
			xhrFields: {
	            withCredentials: true
	        },
			type : 'POST',
			contentType : false,
			data : data,
			processData : false,
			cache : false,
			success : function(data, textStatus, jqXHR) {
				switch (data.CODE_RESPONSE) {
				case "06":
					sessionStorage.clear();
					document.location.replace(data.PATH);
					break;
				default:
					console.log('Se subio el archivo correctamente.');
				$.unblockUI();
					$("#modalUploadAuditoria").modal("hide");
				}
				

			},
			error : function(jqXHR, textStatus, errorThrown) {
				alert("Ups, tenemos problemas tecnicos. Intente más tarde. Error: "
						+ errorThrown);
				console.log('xhRequest: ' + jqXHR + "\n");
				console.log('ErrorText: ' + textStatus + "\n");
				console.log('thrownError: ' + errorThrown + "\n");
				$.unblockUI();
			}
		});
	}
		
}


function cambiarAcordeTipoAuditoria(tipoAudi){
	var tipoAudiId=tipoAudi;
	if(parseInt(tipoAudiId)==94){
		$("#btnListarVerificaciones").hide();
		$("#btnHallazgo").show();
		$('#mdVerif').off('hidden.bs.modal');
	}else{
		$("#btnListarVerificaciones").show();
		$("#btnHallazgo").hide();
		$('#mdVerif').on('hidden.bs.modal', function(e) {
			listarVerificaciones();

		});
	}
}


