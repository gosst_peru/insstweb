var banderaEdicion3;
var hallazgoId;


var listHallazgoFull;

var auditoriaEstado;
var auditoriaEstadoNombre;
var tablaFijaHallazgo=false;
$(document).ready(
		function() {
		$(document).on("click","#btnHallazgo",function(){
			cargarHallazgos();
	
		});
		$("#modalHallazgos").on("shown.bs.modal",function(){
			banderaEdicion3 = false;
			hallazgoId = 0;
			$("#btnNuevoHall").show();
			$("#btnEliminarHall").hide();
			$("#btnVolverHall").hide();
			$("#btnGuardarHall").hide();
			var dataParam = {
					
					auditoriaId : auditoriaId
				};
				
				callAjaxPost(URL + '/auditoria/hallazgos', dataParam,
						procesarDataHallazgos);
		});
		$("#mdVerif").on("show.bs.modal",function(){
			$("#modalHallazgos").modal("hide");
		});
		$("#mdVerif").on("hide.bs.modal",function(){
			$("#modalHallazgos").modal("show");
		});
		
		});
function cargarHallazgosNeutral(){
	banderaEdicion3 = false;
	hallazgoId = 0;
	$("#btnNuevoHall").show();
	$("#btnEliminarHall").hide();
	$("#btnVolverHall").hide();
	$("#btnGuardarHall").hide();
	
	$("#btnNuevoHall").attr("onclick", "javascript:nuevoHallazgo();");
	$("#btnVolverHall").attr("onclick", "javascript:cancelarHallazgo();");
	$("#btnGuardarHall").attr("onclick", "javascript:guardarHallazgo();");
	$("#btnEliminarHall").attr("onclick", "javascript:eliminarHallazgo();");
 
	
	var dataParam = {
			
			auditoriaId : auditoriaId
		};
		
		callAjaxPost(URL + '/auditoria/hallazgos', dataParam,
				procesarDataHallazgos);
}
function cargarHallazgos() {
	banderaEdicion3 = false;
	hallazgoId = 0;
	$("#btnNuevoHall").show();
	$("#btnEliminarHall").hide();
	$("#btnVolverHall").hide();
	$("#btnGuardarHall").hide();
	
	$("#btnNuevoHall").attr("onclick", "javascript:nuevoHallazgo();");
	$("#btnVolverHall").attr("onclick", "javascript:cancelarHallazgo();");
	$("#btnGuardarHall").attr("onclick", "javascript:guardarHallazgo();");
	$("#btnEliminarHall").attr("onclick", "javascript:eliminarHallazgo();");

	$("#modalHallazgos").modal("show");
	
	
	
}


function procesarDataHallazgos(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		var list = data.list;
		listHallazgoFull=list;
		$("#tblHallazgo tbody tr").remove();

		for (index = 0; index < list.length; index++) {

			$("#tblHallazgo tbody").append(

					"<tr id='tr" + list[index].id
							+ "' onclick='javascript:editarHallazgo("
							+index+")' >"

							+ "<td id='tddesc" + list[index].id
							+ "'>" + list[index].descripcion + "</td>"
							
							+ "<td id='tdcausa" + list[index].id
							+ "'>" + list[index].causa
							+ "</td>" 
							+ "<td id='tdlugar" + list[index].id
							+ "'>" + list[index].lugar
							+ "</td>" 
							
							+ "<td id='tdaccion"
							+ list[index].id + "'>" 
							+list[index].accion.estadoIcono+list[index].accion.resumen +		"</td>"
							+ "<td id='tdeviHall"
							+ list[index].id + "'>"
							+ list[index].evidenciaNombre+"</td>"
							+ "</tr>");
		}
		formatoCeldaSombreableTabla(true,"tblHallazgo");
		if(!tablaFijaHallazgo){
			tablaFijaHallazgo=true;
			$("#wrapper3").css("height",$(window).height()*0.6-50+"px");
				goheadfixedY("#tblHallazgo","#wrapper3");
			}
		
		break;
	default:
		alert("Ocurrió un error al traer las auditorias!");
	}
	
}


function editarEvidenciaHallazgo(){
	
	$("#modalUploadHallazgo").modal("show");
}


function editarHallazgo(pindex) {
	
	if (!banderaEdicion3) {
		hallazgoId = listHallazgoFull[pindex].id;
		formatoCeldaSombreableTabla(false,"tblHallazgo");

		var descripcionHallazgo=listHallazgoFull[pindex].descripcion;
		var causaHallazgo=listHallazgoFull[pindex].causa;
		var lugarHallazgo=listHallazgoFull[pindex].lugar;

		var nombreEvidencia=listHallazgoFull[pindex].evidenciaNombre;
		var iconoAccion=listHallazgoFull[pindex].accion.estadoIcono;
		var nombreAccion=listHallazgoFull[pindex].accion.resumen;

		$("#tdeviHall" + hallazgoId)
		.html(
				"<a  " +
				"href='"+URL 
				+ "/auditoria/hallazgo/evidencia?hallazgoId="+hallazgoId+"' target='_blank' >" +
				nombreEvidencia+	"</a><br/>" +
				"<a id='linkEvidencia' href='#' onclick='javascript:editarEvidenciaHallazgo();'>Subir</a>");
		
		
		$("#tddesc" + hallazgoId)
				.html(
						"<input type='text' id='inputDesc' class='form-control' " +
						"placeholder='Descripcion' autofocus='true' value='"
								+ descripcionHallazgo + "'>");
		$("#tdcausa" + hallazgoId)
		.html(
				"<input type='text' id='inputCausa' class='form-control' " +
				"placeholder='Causa' value='"
						+ causaHallazgo + "'>");
		$("#tdlugar" + hallazgoId)
		.html(
				"<input type='text' id='inputLugar' class='form-control' " +
				"placeholder='Lugar' value='"
						+ lugarHallazgo + "'>");
		
		$("#tdaccion" + hallazgoId).html(
				"<a href='#' onclick='javascript:agregarAccionMejora(6, "
						+ hallazgoId + ",4);'>"+nombreAccion+"</a>");
	
		
		/** ************************************************************************* */
		
	
		banderaEdicion3 = true;
		$("#btnNuevoHall").hide();
		$("#btnEliminarHall").show();
		$("#btnVolverHall").show();
		$("#btnGuardarHall").show();
	}
	
}
function editarEvidenciaAuditoria(){
	
	$("#modalUploadAuditoria").modal("show");
}

function nuevoHallazgo() {
	if (!banderaEdicion3) {
		hallazgoId = 0;
		$("#tblHallazgo tbody")
				.append(
						"<tr id='0'>" 
						
								+ "<td>"
								+"<input type='text' id='inputDesc'  class='form-control'>"
								+"</td>"
								+ "<td>"
								+"<input type='text' id='inputCausa'  class='form-control'>"
								+"</td>"
								+ "<td>"
								+"<input type='text' id='inputLugar'  class='form-control'>"
								+"</td>"
								+ "<td>...</td>"
								+ "<td>...</td>"
								+ "</tr>");
	
		banderaEdicion3 = true;
		$("#btnNuevoHall").hide();
		$("#btnEliminarHall").hide();
		$("#btnVolverHall").show();
		$("#btnGuardarHall").show();
		
		$("#inputDesc").focus();
	} else {
		alert("Guarde primero.");
	}
}

function cancelarHallazgo() {
	cargarHallazgosNeutral();
}

function eliminarHallazgo() {
	var r = confirm("¿Está seguro de eliminar el hallazgo?");
	if (r == true) {
		var dataParam = {
				id : hallazgoId,
		};

		callAjaxPost(URL + '/auditoria/hallazgo/delete', dataParam,
				procesarResultadoEliminarHalazgo);
	}
}

function procesarResultadoEliminarHalazgo(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		cargarHallazgosNeutral();
		break;
	default:
		alert("Ocurrió un error al eliminar la auditoria!");
	}
}

function guardarHallazgo() {

	var campoVacio = true;
	var inputDesc = $("#inputDesc").val();
	var inputCausa = $("#inputCausa").val();
	var inputLugar = $("#inputLugar").val();
	
	

	if (campoVacio) {

		var dataParam = {
			id : hallazgoId,
			descripcion : inputDesc,
			causa: inputCausa,
			lugar:inputLugar,
			auditoriaId: auditoriaId
		};

		callAjaxPost(URL + '/auditoria/hallazgo/save', dataParam,
				procesarResultadoGuardarHallazgo);
	} else {
		alert("Debe ingresar todos los campos.");
	}
}

function procesarResultadoGuardarHallazgo(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		cargarHallazgosNeutral();
		break;
	default:
		alert("Ocurrió un error al guardar la auditoria!");
	}
}




function uploadEvidenciaHallazgo(){
	var inputFileImage = document.getElementById("fileEviHallazgo");
	var file = inputFileImage.files[0];
	var data = new FormData();

	if(file.size>bitsEvidenciaHallazgo){
		alert("Lo sentimos, solo se pueden subir archivos menores a "+bitsEvidenciaHallazgo/1000000+" MB");	
	}else{
		data.append("fileEvi", file);
		data.append("hallazgoId", hallazgoId);
		$("#tdeviHall"+hallazgoId+" ").html(file.name);
		var url = URL + '/auditoria/hallazgo/evidencia/save';
		$.blockUI({message:'cargando...'});
		$.ajax({
			url : url,
			xhrFields: {
	            withCredentials: true
	        },
			type : 'POST',
			contentType : false,
			data : data,
			processData : false,
			cache : false,
			success : function(data, textStatus, jqXHR) {
				switch (data.CODE_RESPONSE) {
				case "06":
					sessionStorage.clear();
					document.location.replace(data.PATH);
					break;
				default:
					console.log('Se subio el archivo correctamente.');
				$.unblockUI();
					$("#modalUploadHallazgo").modal("hide");
				}
				

			},
			error : function(jqXHR, textStatus, errorThrown) {
				alert("Ups, tenemos problemas tecnicos. Intente más tarde. Error: "
						+ errorThrown);
				console.log('xhRequest: ' + jqXHR + "\n");
				console.log('ErrorText: ' + textStatus + "\n");
				console.log('thrownError: ' + errorThrown + "\n");
				$.unblockUI();
			}
		});
	}
		
}