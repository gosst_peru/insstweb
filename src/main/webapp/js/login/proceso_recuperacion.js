
$(document).ready(function() {
	
 $("#btnRecuperar").on("click",function(){
	enviarCorreoRecuperacion(); 
 });
 $("#inputNewPass").on("keyup",function(){
	 validarPass();
 });
 $("#inputNewPassConfirm").on("keyup",function(){
	 validarPass();
 }); 
 
 $('#inputNewPass').keyup(function(e){
	    if(e.keyCode == 13)
	    {
	        $(this).trigger("enterKey");
	    }
	});
	$('#inputNewPass').bind("enterKey",function(e){
		enviarCorreoRecuperacion();
		});
	 $('#inputNewPassConfirm').keyup(function(e){
		    if(e.keyCode == 13)
		    {
		        $(this).trigger("enterKey");
		    }
		});
		$('#inputNewPassConfirm').bind("enterKey",function(e){
			enviarCorreoRecuperacion();
			});
});
 
function enviarCorreoRecuperacion(){
	var correo1=$("#inputNewPass").val();
	var correo2=$("#inputNewPassConfirm").val();
	var access=true;
	if(correo1!=correo2){
		access=false;
	}else{
		access=true;
	}
	if(access){
		
	
	var dataParam = {
			userId : getUrlParameter("userId"),
			psw2 : correo1.trim(),
			psw3 : correo2.trim()
		};

		callAjaxPost(URL + '/public/password/save', dataParam,
				
				procesarCambiarPsw);
	}
}
function procesarCambiarPsw(data){
	switch (data.CODE_RESPONSE) {
	case "02":
		alert("El password actual es incorrecto.!");
		break;
	case "03":
		alert("El password se ha bloqueado por demasiados intentos fallidos.!");
		break;
	case "05":
		alert("El password se guardo correctamente.!");
		location.href="login.html";
		break;
	case "07":
		alert("La confirmacion del nuevo password es incorrecta.!");
		$("#inputNewPass").val("");
		$("#inputNewPassConfirm").val("");
		break;
	default:
		alert("Ocurrió un error al guardar el trabajador!");
	}
}
function validarPass(){
	var correo1=$("#inputNewPass").val();
	var correo2=$("#inputNewPassConfirm").val();
	
	if(correo1!=correo2){
		$("#msj").html("Contraseñas diferentes");
		$("#btnRecuperar").prop( "disabled", true );
	}else{
		$("#msj").html("");
		$("#btnRecuperar").prop( "disabled", false );
	}
	
}