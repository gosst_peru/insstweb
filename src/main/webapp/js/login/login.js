var pathRedireccion;
$(document).ready(function() {
	
	if(!are_cookies_enabled()){
		alert("Se ha detectado que las cookies están deshabilitadas en este navegador.");
		$("#msj")
		.append(
				"<b>Estimado usuario para su seguridad, nuestro aplicativo requiere que habilite las cookies en su navegador.</b>");
	}
	
});

$(function(){
	removeAllSession();
	
	$('#inputUser').keyup(function(e){
	    if(e.keyCode == 13)
	    {
	        $(this).trigger("enterKey");
	    }
	});
	$('#inputUser').bind("enterKey",function(e){
		 login(e);
		});
	
	$('#inputPassword').keyup(function(e){
	    if(e.keyCode == 13)
	    {
	        $(this).trigger("enterKey");
	    }
	});
	$('#inputPassword').bind("enterKey",function(e){
		 login(e);
		});
	
	$('#inputUser').attr("autofocus",true);
	
	$("#btnLogin").on("click",function(event){
		login(event) 
	})
})

function login(event) {
	event.preventDefault();
	
	if (validarLogueo()) {
		var dataParam = {
			userName : $("#inputUser").val(),
			password : $("#inputPassword").val()
		};

		callAjaxPost(URL + '/login/validated', dataParam, procesarRespuesta,
				null,{},
				function(){console.log("eror")},function(){
					if(pathRedireccion){
						
						document.location.href=(pathRedireccion);
					}
			});

	}

};

function validarLogueo() {
	var user = $("#inputUser").val();
	var psw = $("#inputPassword").val();
	if (user.length == 0 || psw.length == 0) {
		return false;
	} else {
		return true;
	}
}

function procesarRespuesta(data) {
	$("#msj").html("");
	switch(data.CODE_RESPONSE){
	case "10":
		$("#msj").append("<b>Usuario deshabilitado </b>");
		break;
	case "11":
		$("#msj").append("<b>Contraseña vencida</b>");
		break;
	case "02":
		$("#msj").append("<b>Usuario y/o contrase&ntilde;a incorrectos.</b>");
		break;
	case "03":
		$("#msj")
		.append(
				"<b>Usuario está bloqueado, intentelo dentro de 2 minutos.</b>");
		break;
	case "01":
		var clasifs = data.listaClasificaciones;
		var pels = data.mapPeligros;
		var riesgos = data.mapRiesgos; 
		pathRedireccion=data.PATH; 
		createSession("clasifs", JSON.stringify(clasifs));
		createSession("peligros", JSON.stringify(pels));
		createSession("riesgos", JSON.stringify(riesgos));
		createSession("gestopusername", $("#inputUser").val());
		createSession("gestopcorreo", data.correo);
		createSession("numEmpresas", data.numEmpresas);
		createSession("numUnidades", data.numUnidades);
		createSession("trabajadorGosstId", data.trabajadorId);
		createSession("unidadGosstId", data.trabajadorUnidadId);

		createSession("accesoUsuarios", data.accesoUsuarios);
		createSession("gestopInicio",0);
		createSession("perfilIntertek",data.perfilIntertek);
		createSession("modulo", "Modulos");
		var numDiasRestantes=restaFechas(obtenerFechaActual(),data.fechaVencimientoLicencia);
		if(numDiasRestantes<=0){
			$("#modalFechaLimite").modal('show');
			$("#modalFechaLimite .modal-body").html(
			"Estimado usuario:<br>" +
			"Su licencia expir&oacute; hace "+(numDiasRestantes	*-1)
			+" dia(s), comun&iacute;quese con ventas@aswan.pe para solicitar una nueva licencia<br>"
			+"Atentamente,<br><br> El Equipo Aswan"
			
			);
			pathRedireccion=false
			setTimeout(cerrarSesion,1500);
		}else{
			 
			 
			
			
		}
		break;
	default:
		$("#msj")
		.append("<b>Usuario incorrecto.</b>");
		break;
	}
	 
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

function crearOverlaySobreDiv(divObject){
	 
	$("body").append("<div class='overlayGosst'></div>");
	
	
	
	if(divObject!=null){
		var cuerpoTexto=divObject.cuerpo;
		var tituloTexto=divObject.titulo;
		$("body").append("<div class='mensajeBienvenida'></div>");
		var tituloTutorial="<div class='tituloTutorial'>"+tituloTexto+"</div>";
		var cuerpoTutorial="<div class='cuerpoTutorial'>"+cuerpoTexto+"</div>";
		var imagenUrl="<img src='../imagenes/logo3.png'>";
		$(".mensajeBienvenida").append(tituloTutorial)
		.append(imagenUrl)
		.append(cuerpoTutorial);
	}
}