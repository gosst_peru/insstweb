var divisionIdEdicion;
var nombreDivisionEdicion;

function listarDivisiones(matrixId) {
	banderaEdicion = false;
	deshabilitarBotonesEdicion();
	$("#btnDetalle").hide();
	$("#btnNuevos").hide();
	$("#btnAsociadoMdf").hide();
	$("#btnAsociarNuevaDivision").hide();
	$("#btnNuevo").attr("onclick", "nuevaDivision()");
	$("#btnCancelar").attr("onclick", "cancelarDivision()");
	$("#btnGuardar").attr("onclick","guardarDivision()");
	$("#btnEliminar").attr("onclick","eliminarDivision()");
	$("#btnPreview").show();
	$("#btnDescargar").hide();
	$("#btnInformeAnual").hide();
	$("#btnTransferPuesto").hide();
	divisionIdEdicion = 0;

	var dataParam = {
			matrixId : matrixId
	};

	callAjaxPost(URL + '/division', dataParam, procesarListarDivisiones);
}

function procesarListarDivisiones(data) {

	switch (data.CODE_RESPONSE) {
	case "05":
		var list = data.list;

		$("#tblMatrices tbody tr").remove();
		$("#thMatriz").html("Divisiones");
		$("#h2Titulo")
				.html(
						"<a onclick='javascript:listarMatrices();' href='#'>Unidades("+nombreMatrizEdicion+")</a> : Divisiones");

		for (index = 0; index < list.length; index++) {

			$("#tblMatrices tbody").append(

					"<tr id='trm" + list[index].divisionId + "' onclick='javascript:editarDivision("+list[index].divisionId+")' >"
							+ "<td id='tdm" + list[index].divisionId + "'>"

							+ "<div class='row'>" + "<div class='col-md-3' id='div" + list[index].divisionId + "'>"
							+ list[index].divisionName + "</div>" + "</div>"

							+ "</td>" + "</tr>");
		}
		formatoCeldaSombreable(true);
		break;
	default:
		alert("Ocurrió un error al traer las divisiones!");
	}

}

function nuevaDivision() {
	
	
	if (!banderaEdicion) {
		formatoCeldaSombreable(false);
		$("#tblMatrices tbody")
				.append(
						"<tr id='0'><td><input type='text' id='inputDivision' class='form-control' placeholder='Division' autofocus='true'></td>"
								+ "</tr>");
		divisionIdEdicion = 0;
		habilitarBotonesNuevo();
		banderaEdicion = true;
	} else {
		alert("Guarde primero la division.");
	}
}

function guardarDivision() {

	var campoVacio = true;
	var nuevaEmpresa = 0;

	if ($("#inputDivision").val().length == 0) {
		campoVacio = false;
	}

	if (campoVacio) {

		var dataParam = {
			divisionName : $("#inputDivision").val().toUpperCase(),
			divisionId : divisionIdEdicion,
			matrixId : idMatrizEdicion
		};

		callAjaxPost(URL + '/division/save', dataParam, procesarGuardarDivision);
	} else {
		alert("Debe ingresar todos los campos.");
	}
}

function procesarGuardarDivision(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		$("#tblMatrices tbody tr").remove();
		listarDivisiones(idMatrizEdicion);
		deshabilitarBotonesEdicion();
		banderaEdicion = false;
		break;
	default:
		alert("Reducir longitud de caracteres ingresados");
	}
}

function cancelarDivision() {
	// listarTareasEdicion(idMatrizEdicion);
	listarDivisiones(idMatrizEdicion);
	deshabilitarBotonesEdicion();
	banderaEdicion = false;
}

function editarDivision(divisionId) {
	if (!banderaEdicion) {
		divisionIdEdicion = divisionId;
		formatoCeldaSombreable(false);
		var name = $("#div" + divisionId).text();
		nombreDivisionEdicion=name;
		$("#div" + divisionId)
				.html(
						"<input type='text' id='inputDivision' class='form-control' placeholder='Division' value='"
								+ name + "'>");
		habilitarBotonesEdicion();
		$("#btnDetalle").show();
		$("#btnAsociadoMdf").show();
		$("#btnDetalle").attr("onclick","listarAreasMdf()");


		banderaEdicion = true;
	
		areaIdEdicion=null;
		puestoIdEdicion=null;
		actividadIdEdicion=null;
		tareaIdEdicion=null;
		pasoIdreal=null;
	}

}

function eliminarDivision(){
	var r = confirm("¿Está seguro de eliminar la division?");
	if (r == true) {
		var dataParam = {
			divisionId : divisionIdEdicion,
			mdfTypeId:tipoMdfId
		};

		callAjaxPost(URL + '/division/delete', dataParam,
				procesarEliminarDivision);
	}

}

function procesarEliminarDivision(data) {
switch (data.CODE_RESPONSE) {
case "05":
	listarDivisiones(idMatrizEdicion);
	deshabilitarBotonesEdicion();
	banderaEdicion = false;
	break;
case "08": 
	alert("No se puede eliminar, tiene "+data.areaSons.length+ " área(s) asociada(s)."); 
	break;
default:
	alert("Ocurrió un error al eliminar la division!");
}
}