var areaIdEdicion;
var nombreAreaEdicion;
function procesarResultadoListadoAreas(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		if (data.repetidos > 0) {
			alert("No se pudo subir el listado debido a que algunos datos ya existen. Total: "
					+ data.repetidos);
		} else {
			$('#mdNuevos').modal('hide');
		}

		listarAreas(divisionIdEdicion);
		deshabilitarBotonesEdicion();
		banderaEdicion = false;
		break;
	default:
		alert("Ocurrió un error al guardar listado!");
	}
}
function listarAreas(divisionId) {
	$("#btnGuardarNuevos").attr("onclick",
			"javascript:guardarMasivoPlano(5, " + divisionId + ", " +
					"procesarResultadoListadoAreas,0);");
	
	
	
	
	
	banderaEdicion = false;
	deshabilitarBotonesEdicion();
	$("#btnDetalle").hide();
	$("#btnNuevos").show();
	$("#btnAsociadoMdf").hide();
	
	$("#btnAsociarNuevaDivision").hide();
	$("#btnNuevo").attr("onclick", "nuevaArea()");
	$("#btnCancelar").attr("onclick", "cancelarArea()");
	$("#btnGuardar").attr("onclick", "guardarArea()");
	$("#btnEliminar").attr("onclick","eliminarArea()");
	$("#btnPreview").show();
	$("#btnDescargar").hide();
	$("#btnInformeAnual").hide();
	$("#btnTransferPuesto").hide();
	areaIdEdicion = 0;

	var dataParam = {
		divisionId : divisionId
	};

	callAjaxPost(URL + '/area', dataParam, procesarListarAreas);
}

function procesarListarAreas(data) {

	switch (data.CODE_RESPONSE) {
	case "05":
		var list = data.list;

		$("#tblMatrices tbody tr").remove();
		$("#h2Titulo")
				.html("<a onclick='javascript:listarMatrices();' href='#'>Unidades("+nombreMatrizEdicion+")</a> : "
								+ "<a onclick='javascript:listarDivisionesMdf();' href='#'>Divisiones("+nombreDivisionEdicion+")</a> : &Aacute;reas");

		$("#thMatriz").html("&Aacute;reas");

		for (index = 0; index < list.length; index++) {

			$("#tblMatrices tbody").append(

					"<tr id='trm" + list[index].areaId
							+ "' onclick='javascript:editarArea("
							+ list[index].areaId + ")' >" + "<td id='tdm"
							+ list[index].areaId + "'>"

							+ "<div class='row'>"
							+ "<div class='col-md-3' id='div"
							+ list[index].areaId + "'>" + list[index].areaName
							+ "</div>" + "</div>"

							+ "</td>" + "</tr>");
		};
		formatoCeldaSombreable(true);
		break;
	default:
		alert("Ocurrió un error al traer las areas!");
	}

}
// //////////////////////////////////////////////////////////////////////////////////////////////////////
function nuevaArea() {
	if (!banderaEdicion) {
		formatoCeldaSombreable(false);
		$("#tblMatrices tbody")
				.append(
						"<tr id='0'><td><input type='text' id='inputArea' class='form-control' placeholder='Area'" +
						" autofocus='true' value='"+nombreDivisionEdicion+"'></td>"
								+ "</tr>");
		areaIdEdicion = 0;
		habilitarBotonesNuevo();
		banderaEdicion = true;
	} else {
		alert("Guarde primero el area.");
	}
}

function guardarArea() {

	var campoVacio = true;
	var nuevaEmpresa = 0;

	if ($("#inputArea").val().length == 0) {
		campoVacio = false;
	}

	if (campoVacio) {

		var dataParam = {
				areaName : $("#inputArea").val().toUpperCase(),
			areaId : areaIdEdicion,
			divisionId : divisionIdEdicion
		};

		callAjaxPost(URL + '/area/save', dataParam, procesarGuardarArea);
	} else {
		alert("Debe ingresar todos los campos.");
	}
}

function procesarGuardarArea(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		$("#tblMatrices tbody tr").remove();
		listarAreas(divisionIdEdicion);
		deshabilitarBotonesEdicion();
		banderaEdicion = false;
		break;
	
	default:
		alert("Reducir longitud de caracteres ingresados");
	}
}

function cancelarArea() {
	// listarTareasEdicion(idMatrizEdicion);
	listarAreas(divisionIdEdicion);
	deshabilitarBotonesEdicion();
	banderaEdicion = false;
}

function editarArea(areaId) {
	if (!banderaEdicion) {
		areaIdEdicion = areaId;
		formatoCeldaSombreable(false);
		var name = $("#div" + areaId).text();
		nombreAreaEdicion=name;
		$("#div" + areaId)
				.html(
						"<input type='text' id='inputArea' class='form-control' placeholder='Area' value='"
								+ name + "'>");
		habilitarBotonesEdicion();
		$("#btnDetalle").show();
		$("#btnDetalle").attr("onclick", "listarPuestosMdf()");
		$("#btnAsociadoMdf").show();
		$("#btnAsociarNuevaDivision").show().attr("onclick", "verModalNuevaAsociar()");
		banderaEdicion = true;
		
		
		puestoIdEdicion=null;
		actividadIdEdicion=null;
		tareaIdEdicion=null;
		pasoIdreal=null;
	}

}
function verModalNuevaAsociar(){
	$("#modalAsociarNuevo").modal("show");
	
	var btnGuaradr="<button id='btnGuardarNuevoAsociado' type='button' class='btn btn-success'"
	+"	title='Guardar'>"
+"	<i class='fa fa-floppy-o fa-2x'></i>"
+"</button>"
var divisionesActuales=[];
	var dataParam = {
			matrixId : idMatrizEdicion
	};

	callAjaxPost(URL + '/division', dataParam, function(data){
		divisionesActuales=data.list;
		var selNuevaDivision= crearSelectOneMenuOblig("selNuevaDivision", "", divisionesActuales, "", "divisionId",
		"divisionName");
$("#modalAsociarNuevo").find(".modal-body")
.html(btnGuaradr+"<br><br>Cambiar a:<br>"+selNuevaDivision);
$("#btnGuardarNuevoAsociado").attr("onclick","guardarNuevoAsociado()")
	});
	
}
function guardarNuevoAsociado(){
	var r = confirm("¿Está seguro de cambiar la divisón del area?");
	if (r == true) {
		var dataParam = {
				areaId : areaIdEdicion,
				divisionId:$("#selNuevaDivision").val()
		};

		callAjaxPost(URL + '/area/division/save', dataParam,
				procesarEliminarArea);
	}
}
function eliminarArea(){
	var r = confirm("¿Está seguro de eliminar el area?");
	if (r == true) {
		var dataParam = {
				areaId : areaIdEdicion,
				mdfTypeId:tipoMdfId
		};

		callAjaxPost(URL + '/area/delete', dataParam,
				procesarEliminarArea);
	}

}

function procesarEliminarArea(data) {
switch (data.CODE_RESPONSE) {
case "05":
	listarAreas(divisionIdEdicion);
	$("#modalAsociarNuevo").modal("hide");
	deshabilitarBotonesEdicion();
	banderaEdicion = false;
	break;
case "08": 
	alert("No se puede eliminar, tiene "+data.puestoSons.length+ " puesto(s) asociado(s)."); 
	break;
default:
	alert("Ocurrió un error al eliminar el area!");
}
}