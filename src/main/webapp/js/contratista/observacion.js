var observacionId;var observacionObj={};
var fechaPla;
var fechaReal;
var responsable;
var banderaEdicion2;

var listPerdida;
var listReporte;
var listFactores;
var ObservacionEstado;
var ObservacionEstadoNombre;
var listaFullObservacion;
var listEvaluacion;
var listNotas;
var numPaginaActual;
var numPaginaTotal;
var trabajadoresSugeridos;
var listaTrabs;
var selEstadoObs=[
                  {id:1,nombre:"En proceso"},
                  {id:2,nombre:"Cerrado"}];
var listTipoNotaObservacion=[];
var listTrabajadores;var contadorListTrab;
$(function(){
	$('#modalEvaluacion').on('hidden.bs.modal', function(e) {
		
		$('#mdProgFecha').modal('show');
		
	});
	$('#buscadorTrabajadorInput').keyup(function(e){
	    if(e.keyCode == 13)
	    {
	        $(this).trigger("enterKey");
	    }
	});
	$('#buscadorTrabajadorInput').bind("enterKey",function(e){
		cargarModalObservaciones();
		});
	$("#buscadorTrabajadorInput").focus();
	$("#btnUploadEvaluacionNormal").on("click",function(){
		$("#divEvalNormal").show();
		$("#divImportarEval").hide();
	});
	
	$("#btnUploadEvaluacion").on("click",function(){
		$("#divImportarEval").show();
		$("#divEvalNormal").hide();
	});
	$("#btnGuardarImportEvals").on("click",function(){
		guardarMasivoEvaluacionesExcel();
	});
	$("#resetBuscador").attr("onclick","resetBuscador()")
	
	$("#cambiarBuscador").attr("onclick","toggleBuscadorJstree(2)");
	$("#cambiarBuscadorAsist").attr("onclick","toggleBuscadorAsistJstree(2)");
	
	$('.izquierda_flecha').on('click',function(){
		 numPaginaActual=numPaginaActual-1
	cambiarPaginaTabla();
	});
	$('.derecha_flecha').on('click',function(){
		 numPaginaActual=numPaginaActual+1;
		 cambiarPaginaTabla();
	}); 

}) 
 var titulo="";
function verObservaciones(tipoId) {
	$("#tabProyectos .container-fluid").hide();
	$("#divObsProyecto").show();
	
	 titulo="";
	if(tipoId==1){
		titulo="del contratista";
		cargarModalObservacionesContratistas();

	}else{
		cargarModalObservaciones();
	
	}
	$("#tabProyectos h2")
	.html("<a onclick='volverDivProyectos()'>Proyecto ("+
			proyectoObj.titulo+") </a> " +
			"> Observaciones "+titulo);


}

/**
 * 
 */

function cargarModalObservacionesContratistas(){
	$("#btnCancelarObs").hide();
	$("#btnAgregarObs").hide();
	$("#btnEliminarObs").hide();
	$("#btnGuardarObs").hide();
	
	var dataParam = {
			id : ProyectoId,
			contratistaId:proyectoObj.postulante.contratista.id
		};

		callAjaxPost(URL + '/contratista/proyecto/observaciones', dataParam, function(data) {
			switch (data.CODE_RESPONSE) {
			case "05":
				var list = data.list;
				
				listaFullObservacion=list;
				$("#tblObservacion tbody tr").remove();
				listaFullObservacion.forEach(function(val,index) {
					var iconoDescarga=
						"<br><a class='btn btn-success' target='_blank'" +
						"href='"+URL+"/contratista/observacion/evidencia?observacionId="+val.id+"'>" +
								"<i class='fa fa-download' aria-hidden='true'></i>Descargar</a>";
					if(val.evidenciaNombre=="----"){
						iconoDescarga="";
					}
					var tiempoTranscurrido=obtenerTiempoTranscurrido(val.fecha,val.hora);
					 
					$("#tblObservacion tbody").append(
							"<tr id='tr" + val.id
									+ "'  >"
									+ "<td id='desc"
									+ val.id + "'>"
									+val.descripcion +"</td>"
									+ "<td id='fec"
									+ val.id + "'>"
									+val.fechaTexto +"<br><strong>"+tiempoTranscurrido+"</strong>"+"</td>"
									+ "<td id='tipre"
									+ val.id + "'>"
									+val.reporte.nombre +"</td>"
									+ "<td id='factor"
									+ val.id + "'>"
									+val.factor.nombre +"</td>"
									+ "<td id='pot"
									+ val.id + "'>"
									+val.perdida.nombre +"</td>"
									
									+ "<td id='tdevi"
									+ val.id
									+ "'>"+val.evidenciaNombre+iconoDescarga+"</td>" 
									+ "<td id='tdobsaccm"
									+ val.id + "' class='linkGosst' " +
											"onclick='verCompletarAccioneObservacionContratista("+index+")'>"
									+"<i class='fa fa-star fa-2x' aria-hidden='true' ></i>"+val.indicadorAcciones +"</td>"
									+"<td>---</td>"
									+ "<td id='estObs"
									+ val.id + "'>"
									+(val.nivel==1?"Proceso":"Cerrado") +"</td>"
										+"</tr>");
					
				}) 
				break;
			default:
				alert("Ocurrió un error al traer las programaciones!");
			}
		})
}
function cargarModalObservaciones() {
 
	$("#btnAgregarObs").attr("onclick", "javascript:nuevoObservacion();");
	$("#btnCancelarObs").attr("onclick", "javascript:cancelarObservacion();");
	$("#btnGuardarObs").attr("onclick", "javascript:guardarObservacion();");
	$("#btnEliminarObs").attr("onclick", "javascript:eliminarObservacion();");
	
	$("#btnGenReporte").attr("onclick","javascript:generarHojaRutaForm();");
	
	$("#btnCancelarObs").hide();
	$("#btnAgregarObs").show();
	$("#btnEliminarObs").hide();
	$("#btnGuardarObs").hide(); 

	observacionId = 0;
	fechaPla = null;
	fechaReal = null;
	banderaEdicion2 = false;
	listaFullObservacion=[];listEvaluacion=[];
	var dataParam = {
		id : ProyectoId 
	};

	callAjaxPost(URL + '/contratista/proyecto/observaciones', dataParam, function(data) {
		switch (data.CODE_RESPONSE) {
		case "05":
			var list = data.list;
			listaFullObservacion=data.list;
			listPerdida=data.listPerdida;
			listFactores=data.listFactores;
			listReporte=data.listReporte; 
			listTipoNotaObservacion=data.tipoNota;
			listaFullObservacion=list;
			$("#tblObservacion tbody tr").remove();
			listaFullObservacion.forEach(function(val,index) {

				$("#tblObservacion tbody").append(
						"<tr id='tr" + val.id
								+ "' onclick='editarObservacion("+index+")' >"
								+ "<td id='desc"
								+ val.id + "'>"
								+val.descripcion +"</td>"
								+ "<td id='fec"
								+ val.id + "'>"
								+val.fechaTexto +"</td>"
								+ "<td id='tipre"
								+ val.id + "'>"
								+val.reporte.nombre +"</td>"
								+ "<td id='factor"
								+ val.id + "'>"
								+val.factor.nombre +"</td>"
								+ "<td id='pot"
								+ val.id + "'>"
								+val.perdida.nombre +"</td>"
								
								+ "<td id='tdevi"
								+ val.id
								+ "'>"+val.evidenciaNombre+"</td>" 
								+ "<td id='tdobsaccm"
								+ val.id + "'>"
								+val.indicadorAcciones +"</td>"
								+ "<td id='estObs"+ val.id + "'>"
								+val.nota.nombre +"</td>"
								+ "<td id='anotObs"+ val.id + "'>"
								+val.anotacion +"</td>"
									+"</tr>");
				
			})
			formatoCeldaSombreableTabla(true,"tblObservacion");
			break;
		default:
			alert("Ocurrió un error al traer las programaciones!");
		}
	});
}

function editarObservacion(pindex) {

	if (!banderaEdicion2) {
		observacionObj= listaFullObservacion[pindex];
		observacionId = listaFullObservacion[pindex].id;
		var slcTipoPerdida = crearSelectOneMenu("slcTipoPerdida", "",
				listPerdida, listaFullObservacion[pindex].perdida.id, "id", "nombre");
		var slcFactor = crearSelectOneMenu("slcFactor", "",
				listFactores, listaFullObservacion[pindex].factor.id, "id", "nombre");
		var selReporte = crearSelectOneMenu("selReporte", "",
				listReporte, listaFullObservacion[pindex].reporte.id, "id", "nombre");
		
        var selNotaObsGen=crearSelectOneMenuOblig("selNotaObsGen", "",
        		listTipoNotaObservacion, listaFullObservacion[pindex].nota.id, "id", "nombre");
		
        	
        	$("#estObs"+ observacionId).html(selNotaObsGen);
        	//
        	
        	$("#anotObs" + observacionId).html(
			"<input type='text' id='inputAnotacObs' class='form-control'>");
	$("#inputAnotacObs").val(listaFullObservacion[pindex].anotacion);
        	
        	//
		fechaReal = listaFullObservacion[pindex].fecha;
		var descripcion=listaFullObservacion[pindex].descripcion; 
		formatoCeldaSombreableTabla(false,"tblObservacion");
	var nombreEvidencia=listaFullObservacion[pindex].evidenciaNombre; 
		var options=
		{container:"#tdevi"+observacionId,
				functionCall:function(){ },
				descargaUrl: "/contratista/observacion/evidencia?observacionId="+observacionId,
				esNuevo:false,
				idAux:"Observacion"+observacionId,
				evidenciaNombre:nombreEvidencia};
		crearFormEvidenciaCompleta(options);
		$("#desc" + observacionId).html(
				"<input type='text' id='inputDesc' class='form-control'>");
		$("#inputDesc").val(descripcion);


		$("#fec" + observacionId).html(
				"<input type='date' id='inputFecReal'  class='form-control'>");
	 
		var today2 = convertirFechaInput(fechaReal);
		$("#inputFecReal").val(today2);
		
	$("#tipre"+observacionId).html(selReporte);
	$("#factor"+observacionId).html(slcFactor);
	$("#pot"+observacionId).html(slcTipoPerdida);
	
	//
	var textAcciones=$("#tdobsaccm"+observacionId).text();
	$("#tdobsaccm"+observacionId).addClass("linkGosst")
	.on("click",function(){verCompletarAccioneObservacionGeneral();})
	.html("<i class='fa fa-star fa-2x' aria-hidden='true' ></i>"+textAcciones);
	//
	
		hallarEstadoImplementacion(observacionId);
		
		banderaEdicion2 = true;
		$("#btnCancelarObs").show();
		$("#btnAgregarObs").hide();
		$("#btnEliminarObs").show();
		$("#btnGuardarObs").show();
		$("#btnGenReporte").hide();
		
		
		
		
	}
}

function nuevoObservacion() {
	if (!banderaEdicion2) {
		observacionId = 0;
		formatoCeldaSombreableTabla(false,"tblObservacion");
		var slcTipoPerdida = crearSelectOneMenuOblig("slcTipoPerdida", "",
				listPerdida, "-1", "id", "nombre");
		var slcFactor = crearSelectOneMenuOblig("slcFactor", "",
				listFactores, "-1", "id", "nombre");
		var selReporte = crearSelectOneMenuOblig("selReporte", "",
				listReporte, "-1", "id", "nombre");
		var selNotaObsGen = crearSelectOneMenuOblig("selNotaObsGen", "",
				listTipoNotaObservacion, "3", "id", "nombre");
		
		$("#tblObservacion tbody")
				.append(
						"<tr id='0'>"
						+"<td>"
						+"<input type='text' id='inputDesc' " +
						" class='form-control'>"
						+"</td>"
							
								+ "<td><input type='date' id='inputFecReal' class='form-control'></td>"
								
								+ "<td>"+selReporte+"</td>"
								+ "<td>"+slcFactor+"</td>"
								+ "<td>"+slcTipoPerdida+"</td>"
								+ "<td id='tdevi0'>...</td>"
								+ "<td>...</td>"
								+ "<td>"+selNotaObsGen+"</td>"
								+ "<td><input id='inputAnotacObs' class='form-control'></td>"
								+ "</tr>");
		var options=
		{container:"#tdevi"+observacionId,
				functionCall:function(){ },
				descargaUrl: "",
				esNuevo:true,
				idAux:"Observacion"+observacionId,
				evidenciaNombre:""};
		crearFormEvidenciaCompleta(options);
		$("#inputFecReal").val(convertirFechaInput( new Date() ) );
		$("#btnCancelarObs").show();
		$("#btnAgregarObs").hide();
		$("#btnEliminarObs").hide();
		$("#btnGuardarObs").show();
		$("#btnGenReporte").hide();
		banderaEdicion2 = true;
	} else {
		alert("Guarde primero.");
	}
}

function cancelarObservacion() {
	cargarModalObservaciones();
}

function eliminarObservacion() {
	var r = confirm("¿Está seguro de eliminar la programacion?");
	if (r == true) {
		var dataParam = {
				id : observacionId,
		};

		callAjaxPost(URL + '/contratista/observacion/delete', dataParam,
				function(data) {
					switch (data.CODE_RESPONSE) {
					case "05":
						cargarModalObservaciones();
						break;
					default:
						alert("Ocurrió un error al eliminar la programacion!");
					}
				});
	}
}

function guardarObservacion() {

	var campoVacio = true;
	var perdida=$("#slcTipoPerdida").val();
	var descricion=$("#inputDesc").val();
var factor=$("#slcFactor").val();
var reporte=$("#selReporte").val(); 
	var inputFecReal =convertirFechaTexto($("#inputFecReal").val());
	
if (perdida == -1 || factor == -1 || reporte == -1 || inputFecReal==null) {
	campoVacio = false;
	}
var nota=$("#selNotaObsGen").val();
var anotacion=$("#inputAnotacObs").val();
		if (campoVacio) {

			var dataParam = {
				id : observacionId,
				fecha:inputFecReal,nota:{id:nota},anotacion:anotacion,
				nivel: 2,
				descripcion:descricion,
				perdida:{id:perdida},
				factor:{id:factor},
				reporte:{id:reporte},
				contratistaAsociado:{id:null},
				proyectoId : ProyectoId
			};

			callAjaxPost(URL + '/contratista/observacion/save', dataParam,
					function(data) {
						switch (data.CODE_RESPONSE) {
						case "05":
							guardarEvidenciaAuto(data.nuevoId,"fileEviObservacion"+observacionId
									,bitsEvidenciaObservacion,"/contratista/observacion/evidencia/save"
									,cargarModalObservaciones,"observacionId"); 
							break;
						default:
							console.log("Ocurrió un error al guardar la programacion!");
						}
					});
			
		
		
			
			
			
			
		} else {
			alert("Debe ingresar todos los campos.");
		}
	
	
}

function volverDivObservaciones() {
	$("#tabProyectos .container-fluid").hide();
	$("#divObsProyecto").show();
	$("#tabProyectos h2")
	.html("<a onclick='volverDivProyectos()'>Proyecto ("+
			proyectoObj.titulo+") </a> " +
			"> Observaciones "+titulo);
}

function editarEvidenciaObservacion(){
	$("#tabProyectos .container-fluid").hide();
	$("#divObsProyecto").show();
	$("#mdObservaciones .modal-body").hide();
	$("#mdObservaciones #modalEviObs").show();}
 
function generarHojaRutaForm(){
	
	window.open(URL
			+ "/capacitacion/informe?observacionId="
			+ observacionId , '_blank');	
}

function hallarEstadoImplementacion(progId){
	var fechaPlanificada=$("#inputFecPla").val();
	var fechaReal=$("#inputFecReal").val();
	var fechaHoy=obtenerFechaActual();
	if(fechaReal!=''){
		ObservacionEstado=2;
		ObservacionEstadoNombre="Completado";
	}else{
		
		if(fechaPlanificada!=""){
			var dif=restaFechas(fechaHoy,fechaPlanificada)
			if(dif<0){
				ObservacionEstado=3;
				ObservacionEstadoNombre="Retrasado";
			}else{
				ObservacionEstado=1;
				ObservacionEstadoNombre="Por implementar";
			}
			
		}else{
			
			ObservacionEstado=1;
			ObservacionEstadoNombre="Por implementar";
			
		}
	}
	
	$("#estadoControl"+progId).html(ObservacionEstadoNombre);
	
}
 
function agruparRegistrosTabla(){
	

	var numAgrupacion=10;
		

	cambiarPaginaTabla();
	
	 $('.izquierda_flecha').hover(function(){
         $(this).css('opacity','0.5');
     },function(){
         $(this).css('opacity','1');
     });

     $('.derecha_flecha').hover(function(){
         $(this).css('opacity','0.5');
     },function(){
         $(this).css('opacity','1');
     });
	
	
}
var listFullAccionesAsociadas=[];
var banderaEdicionRespuestaAcc=false;
function verCompletarAccioneObservacionGeneral(){
	$("#tabProyectos .container-fluid").hide();
	$("#divAccionesObservacion").show();
	$("#tabProyectos h2")
	.html("" +
			"<a onclick='volverDivProyectos()'>Proyecto ("+
			proyectoObj.titulo+") </a> " +
			"> <a onclick='volverDivObservaciones()'> Observación " +
			"("+observacionObj.descripcion+") </a> > Acciones de Mejora");
var audiObj={accionMejoraTipoId : 1,
		observacionId:observacionId};
callAjaxPost(URL + '/gestionaccionmejora/accionmejora', 
			audiObj, function(data){
				if(data.CODE_RESPONSE=="05"){ 
					$("#tblAccObservacion tbody tr").remove();
					$("#btnNuevaAccObs").hide();
					$("#btnEliminarAccObs").hide();
					$("#btnGuardarAccObs").hide();
					$("#btnVolverAccObs").hide();
					banderaEdicionRespuestaAcc=false;
					listFullAccionesAsociadas=data.list;
					listFullAccionesAsociadas.forEach(function(val,index){
						val.id=val.accionMejoraId;
						var iconoDescarga=
							"<a href='"+URL+"/gestionaccionmejora/accionmejora/evidencia?accionMejoraId="+val.accionMejoraId+"'>" +
									"<i class='fa fa-download fa-2x' aria-hidden='true'></i></a>";
						if(val.evidenciaNombre=="----"){
							iconoDescarga="";
						}
						$("#tblAccObservacion tbody").append(
								"<tr id='trtip"+val.id+"'  >" +
//							
								"<td id='actpdesc"+val.id+"'>"+val.descripcion+"</td>" +
								"<td id='actpfp"+val.id+"'>"+val.fechaRevisionTexto+"</td>" +
								"<td id='actpfr"+val.id+"'>"+val.fechaRealTexto+"</td>" +
								"<td id='actpobs"+val.id+"'>"+val.observacion+"</td>" +
								"<td id='actpresp"+val.id+"'>"+val.respuesta+
									"<button onclick='modificarRespuestaAccionMejora("+index+")' class='btn btn-success'>" +
									"Modificar respuesta <i class='fa fa-envelope-o'></i>" +
									"</button>"+"</td>" +
								"<td id='actpevi"+val.id+"'>"+val.evidenciaNombre+iconoDescarga+"</td>" +
								"<td id='actpes"+val.id+"'>"+val.estadoCumplimientoNombre+"</td>" +
								
								 
								"</tr>");
					});
				}else{
					console.log("NOPNPO")
				}
			});
}
 function modificarRespuestaAccionMejora(pindex){
	 if(!banderaEdicionRespuestaAcc){
		 banderaEdicionRespuestaAcc=true;
	 var accObj=listFullAccionesAsociadas[pindex];
	 var btnVolver="<button onclick='volverModificarRespuesta("+pindex+")' class='btn btn-danger'>" +
		" <i class='fa fa-arrow-left'></i>" +
		"</button>";
	 var btnGuardar="<button onclick='guardarModificarRespuesta("+pindex+")' class='btn btn-success'>" +
		" <i class='fa fa-floppy-o'></i>" +
		"</button>";
	 $("#actpresp"+accObj.id)
	 .html("<input type='text' id='inputRespAccASoc' class='form-control'> " +
	 		""+btnVolver+btnGuardar);
	 $("#inputRespAccASoc").focus();
	 $("#inputRespAccASoc").val(accObj.respuesta);
	 }else{
		 alert("Hay una respuesta en edición")
	 }
 }
 function volverModificarRespuesta(pindex){
	 verCompletarAccioneObservacionGeneral();
 }
 function guardarModificarRespuesta(pindex){
	 var accObj=listFullAccionesAsociadas[pindex];
	 var accSend={accionMejoraId:accObj.id,
			 respuesta:$("#inputRespAccASoc").val()}
	 callAjaxPost(URL + '/gestionaccionmejora/accionmejora/respuesta/save', 
			 accSend, function(data){
		 
		 verCompletarAccioneObservacionGeneral();
	 });  
 }
var listFullAccionesMejoraObs=[];
var accionMejoraObsObj={};
var banderaEdicionAccObs;
function verCompletarAccioneObservacionContratista(indexObs){
	observacionObj=listaFullObservacion[indexObs];
	observacionObj.index=indexObs;
	banderaEdicionAccObs=false;
	$("#tabProyectos .container-fluid").hide();
	$("#divAccionesObservacion").show();
	$("#tabProyectos h2")
	.html("" +
			"<a onclick='volverDivProyectos()'>Proyecto ("+
			proyectoObj.titulo+") </a> " +
			"> <a onclick='volverDivObservaciones()'> Observación " +
			"("+observacionObj.descripcion+") </a> > Acciones de Mejora");
	var audiObj={accionMejoraTipoId : 1,
			observacionId:observacionObj.id};
	$("#btnNuevaAccObs").show().attr("onclick","nuevaAccionMejoraObs()");
	$("#btnEliminarAccObs").hide().attr("onclick","eliminarAccionMejoraObs()");
	$("#btnGuardarAccObs").hide().attr("onclick","guardarAccionMejoraObs()");
	$("#btnVolverAccObs").hide().attr("onclick","verCompletarAccioneObservacionContratista("+indexObs+")");
	callAjaxPost(URL + '/gestionaccionmejora/accionmejora', 
				audiObj, function(data){
					if(data.CODE_RESPONSE=="05"){ 
						$("#tblAccObservacion tbody tr").remove();
						listFullAccionesMejoraObs=data.list;
						data.list.forEach(function(val,index){
							val.id=val.accionMejoraId;
							$("#tblAccObservacion tbody").append(
									"<tr id='trtip"+val.id+"' onclick='editarAccionMejoraObs("+index+")'  >" +
//								
									"<td id='actpdesc"+val.id+"'>"+val.descripcion+"</td>" +
									"<td id='actpfp"+val.id+"'>"+val.fechaRevisionTexto+"</td>" +
									"<td id='actpfr"+val.id+"'>"+val.fechaRealTexto+"</td>" +
									"<td id='ss"+val.id+"'>"+"..."+"</td>" +
									"<td id='actpobs"+val.id+"'>"+val.observacion+"</td>" +
									"<td id='tdeviaccobs"+val.id+"'>"+val.evidenciaNombre+"</td>" +
									"<td id='actpes"+val.id+"'>"+val.estadoCumplimientoNombre+"</td>" +
									
									 
									"</tr>");
						});
					}else{
						console.log("NOPNPO")
					}
				});
}
function nuevaAccionMejoraObs(){
	if(!banderaEdicionAccObs){banderaEdicionAccObs=true;
	accionMejoraObsObj.accionMejoraId=0;
	$("#btnNuevaAccObs").hide();
	$("#btnEliminarAccObs").hide();
	$("#btnGuardarAccObs").show();
	$("#btnVolverAccObs").show();
	$("#tblAccObservacion tbody").prepend("<tr>"+
			"<td >"+"<input class='form-control' id='inputDescAcc'>"+"</td>" +
			"<td>"+"<input type='date' class='form-control' id='inputFecAcc'>"+"</td>" +
			"<td>"+"..."+"</td>" + 
			"<td >"+"..."+"</td>" +
			"<td>"+"<input class='form-control' id='inputObsAcc'>"+"</td>" +
			"<td id='tdeviaccobs0'>"+" "+"</td>" +
			"<td>...</td>" +
			"</tr>");
	var options=
	{container:"#tdeviaccobs"+accionMejoraObsObj.accionMejoraId,
			functionCall:function(){ },
			descargaUrl: "",
			esNuevo:true,
			idAux:"AccObservacion"+accionMejoraObsObj.accionMejoraId,
			evidenciaNombre:""};
	crearFormEvidenciaCompleta(options);
	}
}
function editarAccionMejoraObs(indexAcc){
	if(!banderaEdicionAccObs){banderaEdicionAccObs=true;
	$("#btnNuevaAccObs").hide();
	$("#btnEliminarAccObs").show();
	$("#btnGuardarAccObs").show();
	$("#btnVolverAccObs").show();
	accionMejoraObsObj=listFullAccionesMejoraObs[indexAcc];
	$("#actpdesc"+accionMejoraObsObj.id).html("<input class='form-control' id='inputDescAcc'>")
	$("#actpfp"+accionMejoraObsObj.id).html("<input type='date' class='form-control' id='inputFecAcc'>")
	$("#actpobs"+accionMejoraObsObj.id).html("<input class='form-control' id='inputObsAcc'>")
	
	$("#inputDescAcc").val(accionMejoraObsObj.descripcion);
	$("#inputFecAcc").val(convertirFechaInput(accionMejoraObsObj.fechaRevision));
	$("#inputObsAcc").val(accionMejoraObsObj.observacion);
	
	var nombreEvidencia=accionMejoraObsObj.evidenciaNombre; 
	var options=
	{container:"#tdeviaccobs"+accionMejoraObsObj.accionMejoraId,
			functionCall:function(){ },
			descargaUrl: "/gestionaccionmejora/accionmejora/evidencia?accionMejoraId="+accionMejoraObsObj.id,
			esNuevo:false,
			idAux:"AccObservacion"+accionMejoraObsObj.id,
			evidenciaNombre:nombreEvidencia};
	crearFormEvidenciaCompleta(options);
	}
}
function guardarAccionMejoraObs(){
	var editarDiv=$("#tblAccObservacion");
	var campoVacio = false;
	var inputDesc = editarDiv.find("#inputDescAcc").val(); 
	var inputFecha =convertirFechaTexto( editarDiv.find("#inputFecAcc").val());
	var obsevacion=editarDiv.find("#inputObsAcc").val()
	  
	if(inputFecha==null){
		campoVacio=true;
	}
	hallarEstadoImplementacionAccAuditoria();
	if (!campoVacio) {

		var dataParam = {
				accionMejoraId : accionMejoraObsObj.accionMejoraId ,
				observacion:obsevacion,
			descripcion : inputDesc,
			fechaRevision : inputFecha,
			fechaReal:null,
			responsableNombre:"Contratista",responsableMail:"@asd",
			accionMejoraTipoId : 1,
			estadoId : 1,
			companyId : getSession("gestopcompanyid"),
			gestionAccionMejoraId : null, 
			observacionId:observacionObj.id 
		};
		 
		callAjaxPost(URL + '/gestionaccionmejora/accionmejora/save', dataParam,
				function(data){ 
				guardarEvidenciaAuto(data.nuevoId,"fileEviAccObservacion"+accionMejoraObsObj.accionMejoraId
						,bitsEvidenciaAccionMejora,"/gestionaccionmejora/accionmejora/evidencia/save",function(){
				 
					verCompletarAccioneObservacionContratista(observacionObj.index); 
				},"accionMejoraId"); 
				
			 
		} );
	} else {
		alert("Debe ingresar todos los campos.");
	}
}
function eliminarAccionMejoraObs(){
	var r = confirm("¿Está seguro de eliminar la Accion de mejora?");
	if (r == true) {
		var dataParam = {
				accionMejoraId : accionMejoraObsObj.accionMejoraId 
		};

		callAjaxPost(URL + '/gestionaccionmejora/accionmejora/delete',
				dataParam, function(){  
			verCompletarAccioneObservacionContratista(observacionObj.index); 	 
				 
		});
	}
	
}

function guardarMasivoEvaluacionesExcel() {
	var texto = $("#txtListadoEval").val();
	var listExcel = texto.split('\n');
	var listEvaluaciones = [];
	var listTrabRepe = [];
	var listFullNombres = "";
	var validado = "";
	
	if (texto.length < 20000) {
		for (var int = 0; int < listExcel.length; int++) {

			var listCells = listExcel[int].split('\t');
			var filaTexto = listExcel[int];
			if (filaTexto.trim().length < 1) {
				validado = "Existen filas vacias.";
				break;
			}

			if (validado.length < 1 && listCells.length != 3) {
				validado = "No coincide el numero de celdas. ("
						+ listCells.length+"!=3 )";
				break;
			} else {
				var evals = {};
				var tipo={}
				for (var j = 0; j < listCells.length; j++) {
					var element = listCells[j];
					if (j === 0) {
						var trabId=0;var seguirBusqueda=true;
						listEvaluacion.every(function(val,index2){
							if(val.trabNombre==element.trim()){
								 trabId=val.trabId;

									evals.trabId =parseInt(trabId);
									seguirBusqueda= false;
							};
							if (!seguirBusqueda)
							{  return false}
							else {return true};
							if(index2==listEvaluacion.length-1){
								validado="No se encontró a : "+element.trim();
								
							}
						});
						
						
						
						if ($.inArray(element.trim(), listTrabRepe) === -1) {
							listTrabRepe.push(element.trim());
						} else {
							listFullNombres = listFullNombres + element.trim() + ", ";
						}

					}
					
					if (j === 1) {
						for (var k = 0; k < listNotas.length; k++) {
							if (listNotas[k].nombre == element.trim()) {
								tipo.id= listNotas[k].id;
								evals.tipoNota=tipo;
							}
						}
						
						if (!evals.tipoNota) {
							validado = "Tipo de Resultado" +
									" no reconocido.";
							break;
						}
					}
					if (j === 2) {
						
								evals.comentario = element.trim();
							
						

						
					}
					
					evals.ObservacionId=observacionId
				}

				listEvaluaciones.push(evals);
				if (listTrabRepe.length < listEvaluaciones.length) {
					validado = "Existen trabajadores repetidos repetidos." + listFullNombres;
					break;
				}
			}
		}
	} else {
		validado = "Ha excedido los 20000 caracteres permitidos.";
	}

	if (validado.length < 1) {
		var r = confirm("¿Está seguro que desea subir estos "
				+ listExcel.length + " elemento(s)?");
		if (r == true) {
			console.log(listEvaluaciones);
			var listIds=listarStringsDesdeArray(listEvaluaciones,"trabId");
			var listEvalAux=listEvaluacion.filter(function(val){
				if(listIds.indexOf(val.trabId)==-1){
					val.id=observacionId;
					return val;
				}
					
			});
			var listEvalAux2=listEvalAux.concat(listEvaluaciones);
			var dataParam={
					observacionId:id,
					trabajadoresEvaluacion:listEvalAux2
			};
			console.log(listEvalAux2);
			callAjaxPost(URL + '/capacitacion/programacion/evaluacion/save', dataParam,
					function (data) {
				switch (data.CODE_RESPONSE) {
				case "05":
					
					alert("Evaluaciones guardadas");
					$("#modalEvaluacion").modal("hide");
					cargarModalObservaciones();
					break;
				default:
					alert("Ocurrió un error al guardar las evaluaiones!");
				}
			});
		}
	} else {
		alert(validado);
	}
}


 
function uploadEvidenciaObservacion(obsId) {
	guardarEvidenciaAuto(observacionId,"fileEviObs",bitsEvidenciaObservacion,
			'/contratista/observacion/evidencia/save',volverModalDivObservaciones,"observacionId")
 
}