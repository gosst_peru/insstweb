var banderaEdicion;
var contratistaId,contratistaObj;
var contratistaNombre;
var encabezadosTotal;
var encabezadoId;
var idTipoEncabezado;
var listFullContratista;
var listUnidades;
var listUnidadesObj;
var listTipoObj;
var listFactorObj;
var listPerdidaObj;
var listResponsableObj;
var listObjetivos;
var listResponsable;
var listResponsableSeguridad;
var listProyectoSeguridad;
var listFullTablaIndicadores;
var listFullContratistaObj;
var listPerfilContratistaAsociado;
var listTabs=[
              {id:1,nombre:"Contratista" },
              {id:2,nombre:"Perfiles"    },
              {id:3,nombre:"Tipo de actividades críticas " },
              {id:4,nombre:"Unidades" },
              {id:5,nombre:"Áreas" },
              {id:6,nombre:"Responsables" },
              {id:7,nombre:"Listas" },
              {id:8,nombre:"Proyectos  " },
              {id:9,nombre:"Inducciones internas" },
              {id:10,nombre:"Indicadores" },
              {id:11,nombre:"Comunicados" },
              {id:12,nombre:"Informe Auditoría Check List" },
              {id:13,nombre:"Informe Inspecciones Libres" },
              {id:14,nombre:"Seguimiento de actividades internas" },
              {id:15,nombre:"Detalle Seguimiento de actividades internas" }
              ];
var subModuloContratistaId =0;
function verAcordeTabContratista(){
	var tabId=$("#selTabContratista").val();
	var tabNombre="";
	$(".tab-content .tab-pane").hide();
	subModuloContratistaId = parseInt(tabId)
	switch(subModuloContratistaId){
	case 1:
		tabNombre="tabContratistas";$(""+tabNombre).show();
		listarContratistas();
		break;
	case 2:
		tabNombre="tabPerfil"
			cargarPerfilContratistas();
		break;
	case 3:
		tabNombre="tabActividadesCriticas"
			cargarTipoActivCriticasContratista();
		break;
	case 4:
		tabNombre="tabClasificiones"
			cargarClasificacionsContratista();
		break;
	case 5:
		tabNombre="tabTipos"
			cargarTiposContratista();
		break;
	case 6:
		tabNombre="tabResponsable"
			cargarResponsablesSeguridad();
		break;
	case 7:
		tabNombre="tabListas"
			cargarListasContratista();
		break;
	case 8:
		tabNombre="tabProyectos"
			cargarProyectosSeguridad();
		break;
	case 9:
		tabNombre="tabInduccion"
			cargarInduccionsSeguridad();
		break;
	case 10:
		tabNombre="tabIndicadores"
			cargarIndicadoresContratista();
		break; 
	case 11:
		tabNombre="tabComunicado"
		cargarComunicado();
		break; 
		 
	case 12:
		tabNombre="tabResumenAuditoria"
			cargarFiltrosResumenAuditoriaFinal(3);
		break; 

	case 13:
		tabNombre="tabResumenAuditoria"
			cargarFiltrosResumenAuditoriaFinal(2);
		break; 
	case 14:
		tabNombre="tabResumenCalendarioActividades";
			cargarCalendarioActividadesAuditoria();
		completarBarraCarga();
		break;
	case 15:
		tabNombre="tabResumenActividades"
			cargarActividadesAuditoria();
		completarBarraCarga();
		break; 
	}
	$("#"+tabNombre).show();
}
var listProyectoActividadAudi = [];
var listContratistasActividadAudi = [];
var listResponsablesActividadAudi = [];

var listTipoActividadAudi = [{id : 3,nombre:"Auditoría"},
                             //{id : 2,nombre:"Inspección"},
                             {id : 94,nombre:"Hallazgos Libres"}];
$(document).ready(function() {
	var selTabContratista=crearSelectOneMenuOblig("selTabContratista", "verAcordeTabContratista()", listTabs, "1", "id",
	"nombre");
$("#divListaTabs").html("Pestaña: "+selTabContratista);
	resizeDivWrapper("wrapContratista",300);
	goheadfixedY("#wrapContratista table","#wrapContratista");
	resizeDivWrapper("wrapResumTrab",200);
	
	resizeDivWrapper("wrapActCrit",300);
	resizeDivWrapper("wrapUnidades",300);
	resizeDivWrapper("wrapAreas",300);
	resizeDivWrapper("wrapResp",300);
	resizeDivWrapper("wrapProyecto",300);
	resizeDivWrapper("wrapEvalTotalProy",200);
	resizeDivWrapper("divCapProyecto",300);
	resizeDivWrapper("wrapCursoProy",250);
	resizeDivWrapper("wrapActProyecto",250);
	resizeDivWrapper("wrapObservaciones",300);
	resizeDivWrapper("wrapResumIncidentes",250);
	resizeDivWrapper("wrapTrabProyecto",250);
	resizeDivWrapper("wrapInduccEmpresa",300);
	resizeDivWrapper("wrapTrabInducc",300);
	resizeDivWrapper("wrapConsoInspecc",300);
	resizeDivWrapper("wrapResumInspecc",300);
	
	resizeDivWrapper("wrapResumAudiFinal",300);
	resizeDivWrapper("wrapAccAudiRes",220);
	resizeDivWrapper("wrapperAct",220);
	
	 resizeDivWrapper("wrapperCalendarioAct",220);
	
	banderaEdicion = false;
	contratistaId = 0; 
$("#btnTogleParam").on("click",function(){
	$("#filtroUnidad").toggle();$("#filtroTipoReporte").toggle();
	$(this).find("i").toggleClass("<i class='fa fa-arrow-up fa-2x'></i>");
	$(this).find("i").toggleClass("<i class='fa fa-arrow-down fa-2x'></i>");
});
	$("#btnClipTablaAnalisisObservacion").on("click",function(){
		var list=obtenerDatosTablaEstandarNoFija("tblAnalisisObservacion");
		copiarAlPortapapeles(list,"btnClipTablaAnalisisObservacion");
		 
		alert("Se han guardado los indicadores al Clipboard");

	});
	$("#btnClipTablaResInspecc").on("click",function(){
		var list=obtenerDatosTablaEstandarNoFija("tblResumInspecc");
		copiarAlPortapapeles(list,"btnClipTablaResInspecc");
		 
		alert("Se han guardado los datos al Clipboard");

	});

	$("#btnClipTablaResDetaInspecc").on("click",function(){
		var list=obtenerDatosTablaEstandarNoFija("tblResumDetaInspecc");
		copiarAlPortapapeles(list,"btnClipTablaResDetaInspecc");
		 
		alert("Se han guardado los datos al Clipboard");

	});
	if(getSession("accesoUsuarios")=="2" || getSession("accesoUsuarios")=="6"){
	
	}else{
		
		if(getSession("linkCalendarioInduccionId")!=null){
			insertMenu(function(){
				$("#selTabContratista").val("9");
				 verAcordeTabContratista()
					completarBarraCarga();
			});
			
		}else{
			var dataParam = {
					empresaId : getSession("gestopcompanyid")
				};
			var hoy=obtenerFechaActual();
			$("#fechaInicioResFinalAudi").val(sumaFechaDias(-90,hoy));
			$("#fechaFinResFinalAudi").val(sumaFechaDias(31,hoy));
			
			$('#modalAnalisisObservacion').on('shown.bs.modal', function (e) {
				crearSelectOneMenuObligMultipleCompleto("selMultipleDivisionesFiltro", "", listDivisiones, 
						"id","nombre","#filtroUnidad","Todas ");
				$("#filtroUnidad").prepend("<h4>Ver Unidades</h4>");
				crearSelectOneMenuObligMultipleCompleto("selMultipleTipoReporteFiltro", "", listReporte, 
						"id","nombre","#filtroTipoReporte","Todos ");
				$("#filtroTipoReporte").prepend("<h4>Ver Reportes</h4>");
			})
			$("#fechaInicioProy").val(sumaFechaDias(-30,hoy));
			$("#fechaFinProy").val(sumaFechaDias(1,hoy));
			
			$("#fechaInicioResuDetaInsp").val(sumaFechaDias(-90,hoy));
			$("#fechaFinResuDetaInsp").val(sumaFechaDias(31,hoy));
			
			$("#fechaInicioResuInsp").val(sumaFechaDias(-90,hoy));
			$("#fechaFinResuInsp").val(sumaFechaDias(31,hoy));
			
			$("#fechaInicioEventoAudi").val(sumaFechaDias(-30,hoy));
			$("#fechaFinEventoAudi").val(sumaFechaDias(365,hoy));
			
			$("#fechaInicioEventoInsp").val(sumaFechaDias(-30,hoy));
			$("#fechaFinEventoInsp").val(sumaFechaDias(365,hoy));
				callAjaxPost(
						URL + '/contratista/filtro',
						dataParam,
						function(data) {
							listProyectoActividadAudi = data.proyectos;
							listContratistasActividadAudi = data.contratistas;
							listResponsablesActividadAudi = data.respTotal;
							var selRespAudiProyecto=crearSelectOneMenu("selRespAudiProyecto", "cargarAuditoriasProyecto(3)", data.respAudi, "", "id",
									"nombre", "Todos");
							if(data.numTrabajadores>0){
								selRespAudiProyecto=crearSelectOneMenuOblig("selRespAudiProyecto", "cargarAuditoriasProyecto(3)", data.respAudi, "", "id",
										"nombre")
							}
						 
							$("#divAudi > .form-inline").append("Responsable: "+selRespAudiProyecto)
								//
							var selRespInspProyecto=crearSelectOneMenu("selRespInspProyecto", "cargarAuditoriasProyecto(2)", data.respInsp, "", "id",
									"nombre", "Todos")
							if(data.numTrabajadores>0){
								selRespInspProyecto=crearSelectOneMenuOblig("selRespInspProyecto", "cargarAuditoriasProyecto(2)", data.respInsp, "", "id",
										"nombre")
							}
						 
							$("#divInspecc > .form-inline").append("Responsable: "+selRespInspProyecto);
							var listEstadoActividadAudi = [{id : 1,nombre:"Por implementar"},
							                               {id : 2,nombre:"Completado"},
							                               {id : 3,nombre:"Retrasado"}]
							var contenido ="";
							var listContenido = [
			                     {sugerencia:"",label:"Desde",inputForm:"<input type='date' id='fechaInicioActividadAudi' class='form-control'>"},
			                     {sugerencia:"",label:"Hasta",inputForm:"<input type='date' id='fechaFinActividadAudi' class='form-control'>"},
			                     {sugerencia:"",label:"Tipo de actividad",inputForm:"",divContainer: "divTipoActividadAudi"},
			                     {sugerencia:"",label:"Contratista",inputForm:"",divContainer: "divContratistaActividadAudi" },
			                     {sugerencia:"",label:"Responsable",inputForm: "",divContainer: "divResponsableActividadAudi" },
			                     {sugerencia:"",label:"Estado",inputForm: "",divContainer: "divEstadoActividadAudi" },
			                     
			                     {sugerencia:"",label:"<button class='btn btn-danger' type='button' onclick='ocultarFiltroActividadAuditoria()'><i class='fa fa-times'></i>Cancelar</button>",
			                    	 inputForm:"<button class='btn btn-success' type='button' onclick='cargarActividadesAuditoria()'><i class='fa fa-filter'></i>Apicar filtro</button>"},
							                     ];
							listContenido.forEach(function(val){
								contenido +=obtenerSubPanelModuloGeneral(val);
							});
							var modalOptions= {id:"modalFiltroActividadAudi",nombre:"<i class='fa fa-filter'></i>Filtros",
									contenido: contenido};
							crearModalPrincipalUsuario(modalOptions,function(){
								if($("#selTipoActividadAudi").length > 0){
									return;
								}
								crearSelectOneMenuObligMultipleCompleto("selTipoActividadAudi", "", listTipoActividadAudi, 
										"id","nombre","#divTipoActividadAudi","Todos");

								crearSelectOneMenuObligMultipleCompleto("selContratistaActividadAudi", "", data.contratistas, 
										"id","nombre","#divContratistaActividadAudi","Todos");
								crearSelectOneMenuObligMultipleCompleto("selResponsableActividadAudi", "", data.respTotal, 
										"id","nombre","#divResponsableActividadAudi","Todos");
								crearSelectOneMenuObligMultipleCompleto("selEstadoActividadAudi", "", listEstadoActividadAudi, 
										"id","nombre","#divEstadoActividadAudi","Todos");
								
							},false);
							$("#fechaInicioActividadAudi").val(sumaFechaDias(-600,hoy));
							$("#fechaFinActividadAudi").val(sumaFechaDias(365,hoy));
							
							
							//Calendario
							var listTipoActividadAudiCalendario = [{id : 3,nombre:"Auditoría"},
							                             //{id : 2,nombre:"Inspección"},
							                             {id : 94,nombre:"Hallazgos Libres"}];
							var listEstadoActividadAudiCalendario = [{id : 1,nombre:"Por implementar"},
							                               {id : 2,nombre:"Completado"},
							                               {id : 3,nombre:"Retrasado"}]
							var contenidoCalendario ="";
							var listContenidoCalendario = [
			                     //{sugerencia:"",label:"Desde",inputForm:"<input type='date' id='fechaInicioActividadAudi' class='form-control'>"},
			                    // {sugerencia:"",label:"Hasta",inputForm:"<input type='date' id='fechaFinActividadAudi' class='form-control'>"},
			                     {sugerencia:"",label:"Tipo de actividad",inputForm:"",divContainer: "divTipoActividadAudiCalendario"},
			                     {sugerencia:"",label:"Contratista",inputForm:"",divContainer: "divContratistaActividadAudiCalendario" },
			                     {sugerencia:"",label:"Responsable",inputForm: "",divContainer: "divResponsableActividadAudiCalendario" },
			                    // {sugerencia:"",label:"Estado",inputForm: "",divContainer: "divEstadoActividadAudi" },
			                     
			                     {sugerencia:"",label:"<button class='btn btn-danger' type='button' onclick='ocultarFiltroActividadCalendarioAuditoria()'><i class='fa fa-times'></i>Cancelar</button>",
			                    	 inputForm:"<button class='btn btn-success' type='button' onclick='cargarCalendarioActividadesAuditoria()'><i class='fa fa-filter'></i>Apicar filtro</button>"},
							                     ];
							listContenidoCalendario.forEach(function(val){
								contenidoCalendario +=obtenerSubPanelModuloGeneral(val);
							});
							var modalOptions= {id:"modalFiltroActividadAudiCalendario",nombre:"<i class='fa fa-filter'></i>Filtros",
									contenido: contenidoCalendario};
							crearModalPrincipalUsuario(modalOptions,function(){
								if($("#selTipoActividadAudiCalendario").length > 0){
									return;
								}
								crearSelectOneMenuObligMultipleCompleto("selTipoActividadAudiCalendario", "", listTipoActividadAudi, 
										"id","nombre","#divTipoActividadAudiCalendario","Todos");

								crearSelectOneMenuObligMultipleCompleto("selContratistaActividadAudiCalendario", "", data.contratistas, 
										"id","nombre","#divContratistaActividadAudiCalendario","Todos");
								crearSelectOneMenuObligMultipleCompleto("selResponsableActividadAudiCalendario", "", data.respTotal, 
										"id","nombre","#divResponsableActividadAudiCalendario","Todos");
								crearSelectOneMenuObligMultipleCompleto("selEstadoActividadAudiCalendario", "", listEstadoActividadAudi, 
										"id","nombre","#divEstadoActividadAudiCalendario","Todos");
								
							},false);
							
							//
							
							insertMenu(function(){
								$("#selTabContratista").val("8");

								crearFiltroProyecto(verAcordeTabContratista);
									
							});
						});
			
		}
	}
	
	
});
function verFiltroActividadesAuditoria(){
	$("#modalFiltroActividadAudi").modal("show");
}
function ocultarFiltroActividadAuditoria(){
	$("#modalFiltroActividadAudi").modal("hide");
}
function cargarActividadesAuditoria(){
	ocultarFiltroActividadAuditoria();
	var listTipos = $("#selTipoActividadAudi").val();
	var listTipoAuditoriaId=[];
	if(listTipos != null){
		listTipos.forEach(function(val,index){
			listTipoAuditoriaId.push(parseInt(val));
		});
	}
	var listContr= $("#selContratistaActividadAudi").val();
	var listContratistaId=[];
	if(listContr != null){
		listContr.forEach(function(val,index){
			listContratistaId.push(parseInt(val));
		});
	}
	var listRespp= $("#selResponsableActividadAudi").val();
	var listTrabajadorId=[];
	if(listRespp != null){
		listRespp.forEach(function(val,index){
			listTrabajadorId.push(parseInt(val));
		});
	}
	var listEst= $("#selEstadoActividadAudi").val();
	var listEstadoId=[];
	if(listEst != null){
		listEst.forEach(function(val,index){
			listEstadoId.push(parseInt(val));
		});
	}
	var dataParam ={
			isInforme : 2,
			listTipoAuditoriaId : (listTipoAuditoriaId.length == 0?null:listTipoAuditoriaId),
			listContratistaId : (listContratistaId.length == 0?null:listContratistaId),
			listTrabajadorId : (listTrabajadorId.length == 0?null:listTrabajadorId),
			listEstadoId : (listEstadoId.length == 0?null:listEstadoId),
			idCompany : getSession("gestopcompanyid"),
			fechaInicioPresentacion: $("#fechaInicioActividadAudi").val(),
			fechaFinPresentacion: $("#fechaFinActividadAudi").val(),
			
	}
	callAjaxPost(URL+"/auditoria",dataParam,function(data){
		$("#tblActivAudi tbody tr").remove();
		data.list.forEach(function(val){
			var rptaSi=val.rptaSi;
			var rptaParc=val.rptaParcial;
			var rptaNo=val.rptaNo;
			var rptaRP=val.rptaRP;
			var rptaFaltante=val.rptaRespondida;
			

			var puntaje=rptaSi*1+rptaParc*0.5+rptaRP*0.75+rptaNo*0;
			var puntajeMax=rptaSi+rptaParc+rptaRP+rptaNo;
			var auditoriaTipoAux = val.auditoriaTipo;
			$("#tblActivAudi tbody").append("<tr>" +

					"<td >" + val.auditoriaTipoNombre+ "</td>" +
					//"<td >" + val.auditoriaNombre+ "</td>" +
					"<td >" + val.auditoriaNombre+ "</td>" +
					"<td >" + val.contratistaNombre+ "</td>" +
					"<td >" + val.proyecto.titulo+ "</td>" +
					"<td >" + val.fechaPlanificadaNombre+ "</td>" +
					"<td >" + val.horaPlanificadaTexto+ "</td>" +
					"<td >" + val.trabajador.nombre+ "</td>" +
					"<td >" + val.auditoriaFechaTexto+ "</td>" +
					"<td   style='color:"+
					(auditoriaTipoAux==94  ?"":colorPuntaje(puntaje/puntajeMax))+";font-weight:bold'>"
					+(auditoriaTipoAux==94  ?"N.A.":
						pasarDecimalPorcentaje(puntaje/puntajeMax,2) )+ "</td>"+

					"<td   style='color:"+
					(auditoriaTipoAux==94  ?"red":"")+";font-weight:bold'>"
					+(auditoriaTipoAux==94  ?val.numHallazgos+" hallazgo(s)":
						"N.A.")+ "</td>"+
					"<td >" + val.estadoImplementacionNombre+ "</td>" +
						"" +
					"" +
					"</tr>");
		});
		goheadfixedY("#tblActivAudi","#wrapperAct")
	});
}

function procesarGuardarEncabezado(data){
	switch (data.CODE_RESPONSE) {
	case "05":
		
		llamarEncabezados();
		break;
	default:
		alert("Ocurrió un error al guardar la empresa!");
	}
}
function volverDivContratista(){
	$("#tabContratistas").find(".container-fluid").hide();
	$("#tabContratistas").find("#divGeneralContratista").show();
	$("#tabContratistas").find("h2").html("Información de Contratistas");
}

function listarContratistas() {
	idTipoEncabezado=1;
	contratistaId = 0;
	$("#btnGuardarContratista").hide();
	$("#btnCancelarContratista").hide();
	$("#btnEliminarContratista").hide();
	$("#btnNuevoContratista").show();
	$("#btnRevisar").hide();
	banderaEdicion = false;
	volverDivContratista();
	callAjaxPost(URL + '/contratista', 
			{empresaId:getSession("gestopcompanyid")}, procesarListarContratista);
}


function verResumenTrabajadoresContratistas(){
	$("#modalResumenTrabajadores").modal("show");
	$("#tblResumenTrabajadoresContratista tbody tr").remove();
	listFullContratista.forEach(function(val,index){
		val.trabajadores.forEach(function(val1,index1){
			var hasExamen=false,hasSctr=false;
			var textoExamen="<i class='fa fa-times' aria-hidden='true'> </i>" +
					"Sin examen médico",
					textoSctr="<i class='fa fa-times' aria-hidden='true'> </i> " +
							"Sin SCTR";
			if(val1.indicadorExamenMedico!=null){
				hasExamen=true;
				textoExamen="<i class='fa fa-check' aria-hidden='true'> </i>" +
						"Examen vigente: "+val1.indicadorExamenMedico
			}
			if(val1.indicadorSctr!=null){
				hasSctr=true;
				textoSctr="<i class='fa fa-check' aria-hidden='true'> </i>" +
						"SCTR vigente: "+val1.indicadorSctr;
				}
			$("#tblResumenTrabajadoresContratista tbody").append(
			"<tr > " +
			" <td>"+val.nombre+"</td>" +
			" <td>"+val1.nombre+"</td>" +
			" <td>"+val1.tipoDocumento.nombre+"</td>" +
			" <td>"+val1.nroDocumento+"</td>" +
			" <td>"+val1.area+"</td>" +
			" <td>"+val1.cargo+"</td>" +
			" <td>"+textoExamen+"</td>" +
			" <td>"+textoSctr+"</td>" +
			"" +
			"</tr>"		
			
			);
		});
	});
}
var listDivisiones;

var listReporte;
function procesarListarContratista(data) {

	switch (data.CODE_RESPONSE) {
	case "05":
		var list = data.list;
		listFullContratista=list; 
		listUnidades=data.listUnidades;
		listDivisiones=data.listDivisiones;
		
		listReporte=data.listReporte;
		
		 
		listResponsable=data.listResponsable;
		listResponsableSeguridad=data.listResponsableSeguridad;
		listProyectoSeguridad=data.listProyectoSeguridad;
		listPerfilContratistaAsociado=data.listPerfiles;
		$("#tblContratista tbody tr").remove(); 
		// var celdaEstilo=CELDA_ESTILO_PAR;
		var celdaEstilo;
		
		list.forEach(function(val,index) {
			// celdaEstilo=celdaEstilo==CELDA_ESTILO_PAR?CELDA_ESTILO_IMPAR:CELDA_ESTILO_PAR;
			$("#tblContratista tbody").append(
					"<tr onclick='javascript:editarContratista("
							+index+")' ><td id='"
							+ val.id
							+ "ruc' "
							+ celdaEstilo
							+ ">"
							+ val.ruc
							+ "</td>" +
									"<td id='"
							+ val.id
							+ "nombre' "
							+ celdaEstilo
							+ ">"
							+ val.nombre
							+ "</td>"
							+"<td id='"+ val.id+"pass' >"
							+ "*******"
							+ "</td>"
							+"<td id='"+ val.id+"perfil' >"
							+ val.perfil.nombre
							+ "</td>"
							
							+"<td id='"+ val.id+"direccion' >"
							+ val.direccion
							+ "</td>"
							 
							+"<td id='"+ val.id+"activi' >"
							+ val.actividad
							+ "</td>"
							+"<td id='"+ val.id+"segur' >"
							+ val.seguro
							+ "</td>"
							+"<td id='"+ val.id+"anio' >"
							+ val.anioInicio
							+ "</td>"
							+"<td id='"+ val.id+"contact' >"
							+ val.contacto
							+ "</td>"
							+"<td id='"+ val.id+"contactc' >"
							+ val.contactoCorreo
							+ "</td>"
							//+"<td id='contdoc"+ val.id+"' >"
							//+ val.numDocumentos
							//+ "</td>"
							+"<td id='"+ val.id+ "proy' "+ ">"
							+ val.numProyectosAsignados +"/"+ val.numProyectosTotal
							+ "</td>" 
							+"<td id='"+val.id+"obs'> "
							+ val.numObservacionesCerradas +"/"+ val.numObservacionesTotal
							+ "</td>" 
							+"<td id='"+val.id+"inc'> "
							+ val.numIncidentesRelacionados 
							+ "</td>"
							+"<td id='"+val.id+"logo'> "
							+ val.logoNombre 
							+ "</td>" 
							+"</tr>");
		})
		
		
		formatoCeldaSombreable(true);completarBarraCarga();
		break;
	default:
		alert("Ocurrió un error al traer las empresas!");
	}

}

function editarContratista(pindex) {
	if (!banderaEdicion) {
		contratistaId = listFullContratista[pindex].id;
		contratistaObj=listFullContratista[pindex];
		contratistaNombre= listFullContratista[pindex].nombre;
		var dataParam={
				id:contratistaId
		}
		callAjaxPost(URL + '/contratista/proyectos_observaciones/asociados', dataParam, function(data){
			if(data.CODE_RESPONSE=="05"){
				var proyectosAsociados=data.proyectosAsociados;
				var observacionesAsociadas=data.observacionesAsociadas;
				
				$("#tblProyAsoc tbody tr").remove();
				$("#tblObsAsoc tbody tr").remove();
				proyectosAsociados.forEach(function(val,index){ 
					$("#tblProyAsoc tbody").append("<tr>" +
							"<td>"+val.titulo+"</td>" +
							"<td><a href='"+val.contratistaEvidenciaUrl+"'>"+val.contratistaEvidenciaNombre+"</a></td>" +
							"<td>"+val.fechaInicioTexto+"</td>" +
							"<td>"+val.fechaFinTexto+"</td>" +
							"<td>"+val.estado.nombre+"</td>" +
							"" +
							"</tr>");
				});
				observacionesAsociadas.forEach(function(val,index){
					
					$("#tblObsAsoc tbody").append("<tr>" +
							"<td>"+val.descripcion+"</td>" +
							"<td>"+val.fechaTexto+"</td>" +
							"<td>"+val.reporte.nombre+"</td>" +
							"<td>"+(val.nivel==1?"Proceso":"Cerrado")+"</td>" + 
							"" +
							"</tr>");
				});
				$("#"+contratistaId+"obs").html("<a onclick='verObservacionesAsociadas()'>"
						 +$("#"+contratistaId+"obs").text() 
						+"</a>");
				$("#"+contratistaId+"proy").html("<a onclick='verProyectosAsociados()'>"
						+$("#"+contratistaId+"proy").text() 
						+"</a>"); 
			}
			
		});
		//
		
		var selPerfilContratista=crearSelectOneMenuOblig("selPerfilContratista",
				"",listPerfilContratistaAsociado,contratistaObj.perfil.id,"id","nombre")
		$("#"+contratistaId+"perfil").html(selPerfilContratista);
		//
		var textDocs=$("#contdoc"+contratistaId).text();
		
		$("#contdoc"+contratistaId).addClass("linkGosst")
		.on("click",function(){verDocumentosContratista();})
		.html("<i class='fa fa-files-o fa-2x' aria-hidden='true' ></i>"+textDocs);
		
		
		$("#btnRevisar").show();
		
		$("#"+contratistaId+"ruc").html("<input class='form-control' id='inputRuc' onkeydown='return IsNumeric(event);'  onkeyup='return IsNumericRestriccion(event);'>");
		nombrarInput("inputRuc",listFullContratista[pindex].ruc);
		
		$("#"+contratistaId+"nombre").html("<input class='form-control' id='inputRS'>");
		nombrarInput("inputRS",listFullContratista[pindex].nombre);
		$("#"+contratistaId+"pass").html("<input class='form-control' id='inputPassContr'>");
		nombrarInput("inputPassContr",contratistaObj.contrasenia);
		
		
		$("#"+contratistaId+"direccion").html("<input class='form-control' id='inputDom'>");
		nombrarInput("inputDom",listFullContratista[pindex].direccion);
		
		$("#"+contratistaId+"resp1").html("<input class='form-control' id='inputResp1'>");
		nombrarInput("inputResp1",listFullContratista[pindex].sstTrabajadorNombre);
		
		$("#"+contratistaId+"resp2").html("<input class='form-control' id='inputResp2'>");
		nombrarInput("inputResp2",listFullContratista[pindex].respTrabajadorNombre);
		
		$("#"+contratistaId+"activi").html("<input class='form-control' id='inputActividad'>");
		nombrarInput("inputActividad",listFullContratista[pindex].actividad);
		$("#"+contratistaId+"segur").html("<input class='form-control' id='inputSeguro'>");
		nombrarInput("inputSeguro",listFullContratista[pindex].seguro);
		$("#"+contratistaId+"anio").html("<input class='form-control' id='inputAnioInicio' onkeypress='return IsNumeric(event);'>");
		nombrarInput("inputAnioInicio",listFullContratista[pindex].anioInicio);
		
		$("#"+contratistaId+"contact").html("<input class='form-control' id='inputContacto'>");
		nombrarInput("inputContacto",listFullContratista[pindex].contacto);
		$("#"+contratistaId+"contactc").html("<input class='form-control' id='inputContactoCorreo'>");
		nombrarInput("inputContactoCorreo",listFullContratista[pindex].contactoCorreo);
		
		
		var selTipoUnidad = crearSelectOneMenu("selTipoUnidad", "",
				listUnidades, listFullContratista[pindex].unidad.id, "id", "nombre");
		$("#"+contratistaId+"unidad").html(selTipoUnidad);
		//
		var options=
		{container:"#"+listFullContratista[pindex].id+"logo",
				functionCall:function(){ },
				descargaUrl:"/contratista/logo/evidencia?id="+listFullContratista[pindex].id,
				esNuevo:false,
				idAux:"Contratista"+listFullContratista[pindex].id,
				evidenciaNombre:listFullContratista[pindex].logoNombre};
		crearFormEvidenciaCompleta(options);
		//
		formatoCeldaSombreableTabla(false,"tblContratista");
		$("#btnGuardarContratista").show();
		$("#btnCancelarContratista").show();
		$("#btnEliminarContratista").show();
		$("#btnNuevoContratista").hide();
		
		banderaEdicion = true;
		
		
		
		
	}

}
function verDocumentosContratista(){
	$("#tabContratistas").find(".container-fluid").hide();
	$("#tabContratistas").find("#divDocsContratista").show();
	 
	$("#tabContratistas").find("h2").html("<a onclick='volverDivContratista();'> Contratista '"+contratistaObj.nombre+"'</a> " +
			"> Documentos Adicionales");
	callAjaxPost(URL + '/contratista/examenes', {id:contratistaObj.id,
		examen:{tipo:{id:5}}}, function(data){
		$("#divDocsContratista table tbody tr").remove();
		data.examenes.forEach(function(val,index){
			var btnEvidencia="";
			btnEvidencia="<a class='btn btn-success' target='_blank' href='"+URL+"/contratista/examen/evidencia?id="+val.id+"'  " +
					"><i aria-hidden='true' class='fa fa-download'></i>Descargar</a>"
			$("#divDocsContratista table tbody")
			.append("<tr>" +
					"<td>"+val.tema+"</td>" +
					"<td>"+btnEvidencia+"</td>" +
					"" +
					"</tr>");
				
		})
	});

}
function verObservacionesAsociadas(){
	$("#mdAsociados").modal('show'); 
	$("#mdAsociados #mdProgFechaLabel").html("Resumen Observaciones - " + contratistaNombre);
	
	$("#proyAsociados").hide();
	$("#obsAsociados").show();
}
function verProyectosAsociados(){
	$("#mdAsociados").modal('show');
	$("#mdAsociados #mdProgFechaLabel").html("Resumen del estado de proyectos - " + contratistaNombre);
	
	$("#obsAsociados").hide();
	$("#proyAsociados").show();
}
function nuevoContratista() {
	if (!banderaEdicion) {
		$("#btnObjetivo").show();
		contratistaId = 0;
		var selTipoUnidad = crearSelectOneMenu("selTipoUnidad", "",
				listUnidades, "-1", "id", "nombre");
		var selPerfilContratista=crearSelectOneMenuOblig("selPerfilContratista",
				"",listPerfilContratistaAsociado,"","id","nombre")
		
$("#tblContratista tbody:first")
		.prepend(
				"<tr id='0'><td><input type='text' id='inputRuc' onkeydown='return IsNumeric(event);'  onkeyup='return IsNumericRestriccion(event);' class='form-control' placeholder='RUC' required='true' autofocus='true'></td>"
						+ "<td><input type='text' id='inputRS' class='form-control' placeholder='Razon Social' required='true'></td>"
						+ "<td>Automático</td>"
						+"<td>"+selPerfilContratista+"</td>"
						+ "<td><input type='text' id='inputDom' class='form-control' placeholder='Domicilio' required='true'></td>"
						 
						+ "<td><input type='text' id='inputActividad' class='form-control' placeholder='Actividad' required='true'></td>"
						+ "<td><input type='text' id='inputSeguro' class='form-control' placeholder='Seguro' required='true'></td>"
						+ "<td><input type='text' id='inputAnioInicio' onkeypress='return IsNumeric(event);' class='form-control' placeholder='Inicio' required='true'></td>"
						+ "<td><input type='text' id='inputContacto' class='form-control' placeholder='Contacto' required='true'></td>"
						+ "<td><input type='text' id='inputContactoCorreo' class='form-control' placeholder='Correo' required='true'></td>"
						
					
						+ "<td>...</td>"
						+ "<td>...</td>"
						+ "<td>...</td>"
						+ "<td id='0logo'>...</td>"
						+ "</tr>");
var options=
{container:"#"+contratistaId+"logo",
		functionCall:function(){ },
		descargaUrl:"",
		esNuevo:true,
		idAux:"Contratista"+contratistaId,
		evidenciaNombre:""};
crearFormEvidenciaCompleta(options);
formatoCeldaSombreableTabla(false,"tblContratista");
	
		
		$("#btnGuardarContratista").show();
		$("#btnCancelarContratista").show();
		$("#btnNuevoContratista").hide();
		banderaEdicion = true;
	} else {
		alert("Guarde primero.");
	}
}

function eliminarContratista() {
	var r = confirm("¿Está seguro de eliminar el contratista?");
	if (r == true) {
		var dataParam = {
			id : contratistaId
		};

		callAjaxPost(URL + '/contratista/delete', dataParam,
				procesarEliminarEmpresa);
	}

}

function cancelarNuevoContratista() {
	listarContratistas();
}

function guardarContratista() {

	var campoVacio = true;
	var muchoRuc=false;
	var nuevoContratista = 0;
	var contacto=$("#inputContacto").val();
	var contactoCorreo=$("#inputContactoCorreo").val();
	var actividad =$("#inputActividad").val();
	var seguro=$("#inputSeguro").val();
	var anioInicio=$("#inputAnioInicio").val();
var tipoUnidadId=$("#selTipoUnidad").val();
var contrasenia=$("#inputPassContr").val();
var perfil=$("#selPerfilContratista").val();
	if ($("#inputRuc").val().length == 0) {
		campoVacio = false;
	}
	if ($("#inputRuc").val().length > 12) {
		muchoRuc = true;
	}
	if ($("#inputRS").val().length == 0) {
		campoVacio = false;
	}
	if ($("#inputDom").val().length == 0) {
		campoVacio = false;
	}

	if (muchoRuc){
		alert("RUC no válido (máximo 12 dígitos)");
		return;
	}
	if (campoVacio) {

		var dataParam = {
			empresaId:getSession("gestopcompanyid"),perfil:{id:perfil},
			contacto:contacto,contrasenia:contrasenia,
			contactoCorreo:contactoCorreo,
			actividad:actividad,
			seguro:seguro,
			anioInicio:anioInicio,
			ruc : $("#inputRuc").val(),
			nombre : $("#inputRS").val(),
			direccion : $("#inputDom").val(),
			unidad:{id:(tipoUnidadId==-1?null:tipoUnidadId)},
			id : contratistaId
		};
		 console.log(dataParam);
		callAjaxPost(URL + '/contratista/save', dataParam, procesarGuardarContratista);
	} else {
		alert("Debe ingresar los campos de Razón Social, Domicilio y RUC.");
	}
}

function procesarGuardarContratista(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		
		if(data.mensaje.length>0){
			alert(data.mensaje)
		}else{
			guardarEvidenciaAuto(data.nuevoId,"fileEviContratista"+contratistaId
					,bitsLogoEmpresa,"/contratista/logo/evidencia/save",
					listarContratistas );
			$("#tblContratista tbody tr").remove(); 
		} 
		break;
	default:
		alert("Ocurrió un error al guardar la empresa!");
	}
}

function procesarEliminarEmpresa(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		listarContratistas(); 
		banderaEdicion = false;
		break;
	default:
		alert("Ocurrió un error al eliminar la empresa!");
	}
}

function mostrarCargarImagen(companyId) {
	$('#mdUpload').modal('show');
	$('#btnUploadLogo').attr("onclick",
			"javascript:uploadLogo(" + companyId + ");");
}
 
function llamarReporteObservaciones(){
	
	$("#modalAnalisisObservacion").modal("show");
	
	
	$("#divAnalisisObservacion").show();
	$("#divGraficoBarObservacion").hide();
	$("#divGraficoPieObservacion").hide();
}

function ocultarCuadrosIndicadores(){
	$("#divAnalisisObservacion").hide();
	$("#divGraficoBarObservacion").hide();
	$("#divGraficoPieObservacion").hide();
}
function generarTablaObservacion(){
	
	$("#divAnalisisObservacion").show();
	$("#divGraficoBarObservacion").hide();
	$("#divGraficoPieObservacion").hide();
	var head="",subHead="", body=""; 
	var listFullActualObj=[];
	var fechaInicio=$("#fechaInicioProy").val();
	var fechaFin=$("#fechaFinProy").val();
	var tipoAgrupacion=parseInt($("#slcTipoProy").val());
	
	var listUnidades = $("#selMultipleDivisionesFiltro").val();
	var listUnidadesId=[];
	if(listUnidades != null){
		listUnidades.forEach(function(val,index){
			listUnidadesId.push(parseInt(val));
		});
	}
	
	var listTipoReporte = $("#selMultipleTipoReporteFiltro").val();
	var listTipoReportesId=[];
	if(listTipoReporte != null){
		listTipoReporte.forEach(function(val,index){
			listTipoReportesId.push(parseInt(val));
		});
	}
	var tipoOrigenObservacionId = $("#slcOrigenObsProy").val();
	callAjaxPost(URL+"/contratista/observaciones/indicadores",
			{empresaId : getSession("gestopcompanyid"),
		tipoOrigenObservacionId : tipoOrigenObservacionId,
		listUnidadesId : (listUnidadesId.length==0?null:listUnidadesId),
		listTipoReportesId : (listTipoReportesId.length==0?null:listTipoReportesId),
		fechaInicioPresentacion : convertirFechaTexto(fechaInicio),
		fechaFinPresentacion : convertirFechaTexto(fechaFin)},function(data){
resizeDivWrapper("divAnalisisObservacion",300)
		listFullContratistaObj=data.contratistasObj;
		listUnidadesObj = data.listUnidadesObj;
		listTipoObj=data.listTipoObjeto;
		listFactorObj=data.listFactorObjeto;
		listPerdidaObj=data.listPerdidaObjeto;
		listResponsableObj=data.listResponsableObjeto;
		switch(tipoAgrupacion){
		case 1 :
			subHead="Contratista";
			listFullActualObj=listFullContratistaObj;
			break;
		case 3:
			subHead="Área";
			listFullActualObj=listUnidadesObj 
			break;
		case 4: 
			subHead="Tipo Reporte";
			listFullActualObj=listTipoObj 
			break;
		case 5:  
			subHead="Factor Seguridad";
			listFullActualObj=listFactorObj 
			break;	
		case 6: 
			subHead="Potencial de Perdida"
			listFullActualObj=listPerdidaObj 
			break;	
		case 7:
			subHead="Responsable ";
			listFullActualObj=listResponsableObj 
			break;	
		}
		
		var headTipoPerdida="";
		listPerdidaObj.forEach(function(val,index){
			headTipoPerdida+="<td class='tb-acc' style='background-color: #1f2b75  !important;'>"+val.nombre+"</td>";
		});
		head="<tr>" +
		"<td class='tb-acc'>"+subHead+"</td>" +
		"<td class='tb-acc' style='background-color: #136395 !important;'>Total Observaciones</td>" +
		
		headTipoPerdida+
		"<td class='tb-acc' style='background-color: #073755 !important;'>Proceso</td>" +
		"<td class='tb-acc' style='background-color: #073755 !important;'>Cerrado</td>" +
		
		"<td class='tb-acc' style='background-color: #6d6f67 !important'>Acciones Mejora Asignadas</td>" +
		"<td class='tb-acc' style='background-color: #6d6f67 !important'>Completadas</td>" +
		"<td class='tb-acc' style='background-color: #6d6f67 !important'>Por implementar</td>" +
		
		
		"</tr>"; 
		if(tipoAgrupacion==6){
			head="<tr>" +
			"<td class='tb-acc'>"+subHead+"</td>" +
			"<td class='tb-acc' style='background-color: #136395 !important;'>Total Observaciones</td>" + 
			"<td class='tb-acc' style='background-color: #073755 !important;'>Proceso</td>" +
			"<td class='tb-acc' style='background-color: #073755 !important;'>Cerrado</td>" +
			
			"</tr>"; 
		}
	 
		 
		listFullActualObj.forEach(function(val,index){
			var totalObs=0,totalCrit=0,totalMod=0,totalSig=0,totalTot=0;
			var totalCerrado=0,totalProceso=0;
			var totalAccionesCompletadas=0;
			var totalAcciones=0;
			var observaciones=val.observaciones;
				observaciones.forEach(function(val1,index1){
					   
						if(val1.acciones!=null){
							val1.acciones.forEach(function(val2,index2){
								totalAcciones++;
								if(val2.estadoId==2){
									totalAccionesCompletadas++;
								}
							});
						}
						totalObs+=1;
						switch(val1.perdida.id){
						case 1:
							totalCrit+=1;
							break;
						case 2:
							totalMod+=1;
							break;
						case 3:
							totalSig+=1;
							break;
						case 4:
							totalTot+=1;
							break;
							
						}
						switch(val1.nivel){
						
						case 2:
							totalCerrado+=1;
							break;
						default:
							totalProceso+=1;
							break;
						}
				});
				var auditorias=val.auditorias;
				if(auditorias == null){
					auditorias = [];
				}
				auditorias.forEach(function(val2,index2)
				{
					var hallazgos=val2.hallazgos;
					hallazgos.forEach(function(val3,index3)
					{
						totalObs+=1;
						switch(val3.nivel.id){
						case 1:
							totalCrit+=1;
							break;
						case 2:
							totalMod+=1;
							break;
						case 3:
							totalSig+=1;
							break;
						case 4:
							totalTot+=1;
							break;
							
						}
						var accionesMejora=val3.acciones;
						if(accionesMejora.length>1)
						{
							totalCerrado+=1;
						}
						else 
						{
							totalProceso+=1;
						}
						accionesMejora.forEach(function(val4,index4)
						{
							totalAcciones++;
							if(val4.estadoId==2){
								totalAccionesCompletadas++;
							}
						});
					});
				});
				 
					 
				var porc1=totalAccionesCompletadas/totalAcciones;
				var porc2=(totalAcciones-totalAccionesCompletadas)/totalAcciones;
				if(tipoAgrupacion==6){
					body=body+"<tr>" +
					"<td>"+val.nombre+"</td>" +
					"<td>"+totalObs+"</td>" + 
					"<td>"+totalProceso+"</td>" +
					"<td>"+totalCerrado+"</td>" +
							"</tr>";
				}else{
					body=body+"<tr>" +
					"<td>"+val.nombre+"</td>" +
					"<td>"+totalObs+"</td>" +
					"<td>"+totalCrit+"</td>" +
					"<td>"+totalMod+"</td>" +
					"<td>"+totalSig+"</td>" +
					"<td>"+totalTot+"</td>" +
					"<td>"+totalProceso+"</td>" +
					"<td>"+totalCerrado+"</td>" +
					
					"<td>"+totalAcciones+"</td>" +
					"<td>"+totalAccionesCompletadas+" ("+pasarDecimalPorcentaje(porc1,2)+")</td>" +
					"<td>"+(totalAcciones-totalAccionesCompletadas)+" ("+pasarDecimalPorcentaje(porc2,2)+")</td>" + 
					
							"</tr>";
				}
				
				val.totalObs=totalObs;
				val.totalCrit=totalCrit;
				val.totalMod=totalMod;
				val.totalSig=totalSig;
				val.totalTot=totalTot;
				val.totalObs=totalObs;
				val.totalProceso=totalProceso;
			 
			
		});
		
		
		$("#tblAnalisisObservacion thead").html(head);
		$("#tblAnalisisObservacion tbody").html(body);
		return listFullActualObj;
	});
	
}
function generarBarraObservacion(){
	var listaChamba=generarTablaObservacion();
	var listaCategorias=[];
	var listDrillSeries=[];
	listaChamba.forEach(function(val,index){ 
		listaCategorias.push({
			name:val.nombre,
			y:val.totalObs,
			
			drilldown:val.nombre
		}); 
var list={
        name: val.nombre,
        id: val.nombre,
        data: [
            [
                'CRÍTICO',
                val.totalCrit
            ],
            [
                'MODERADO',
                val.totalMod
            ],
            [
                'SIGNIFICATIVO',
                val.totalSig
            ],
            [
                'TOLERABLE',
                val.totalTot
            ] 
        ]
    };
listDrillSeries.push(list);

	});
	$("#divAnalisisObservacion").hide();
	$("#divGraficoBarObservacion").show();
	$("#divGraficoPieObservacion").hide();
	 Highcharts.setOptions({
	        lang: {
	            drillUpText: '<< Volver a General'
	        }
	    });
	Highcharts.chart('divGraficoBarObservacion', {
        chart: {
            type: 'bar'
        },
        title: {
            text: ''
        }, 
        xAxis: {
        	type: 'category'
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Observaciones',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            },
        	allowDecimals:false
        },
        tooltip: {
            valueSuffix: ''
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
        	 enabled: false,
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 80,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Total',
            colorByPoint: true,
            data: listaCategorias
        }],
        drilldown: {
            series: listDrillSeries
        }
        
    });  
}

function generarPieObservacion(){
	var listaChamba=generarTablaObservacion();
	var listaCategorias=[];
	var listDrillSeries=[];
	listaChamba.forEach(function(val,index){ 
		listaCategorias.push({
			name:val.nombre,
			y:val.totalObs,
			
			drilldown:val.nombre
		}); 
var list={
        name: val.nombre,
        id: val.nombre,
        data: [
            [
                'CRÍTICO',
                val.totalCrit
            ],
            [
                'MODERADO',
                val.totalMod
            ],
            [
                'SIGNIFICATIVO',
                val.totalSig
            ],
            [
                'TOLERABLE',
                val.totalTot
            ] 
        ]
    };
listDrillSeries.push(list);

	});
	$("#divAnalisisObservacion").hide();
	$("#divGraficoBarObservacion").hide();
	$("#divGraficoPieObservacion").show();
	
	Highcharts.setOptions({
        lang: {
            drillUpText: '<< Volver a General'
        }
    });
	Highcharts.chart('divGraficoPieObservacion', {
        chart: { 
            type: 'pie'
        },
        title: {
            text: '',
            style:{fontSize:"24px"}
        },
        tooltip: {
	            headerFormat:'<span style="font-size: 18px">{point.key}</span><br/>',
	            pointFormat:'<span style="color:{point.color}">\u25CF</span> {series.name}:  <b style="font-size:20px">{point.percentage:.1f}%</b><br/>'
	      
        },
        plotOptions: {
            pie: {
                allowPointSelect: false,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '{point.name}  (%{point.percentage:.2f})',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                        fontSize:"19px"
                    }
                }
            }
        },	
        legend: {enabled:false,
            itemStyle: {
                color: '#000000',
                fontSize: '20px'
            }
        },
        series: [{
            name: 'Observaciones',
            colorByPoint: true, 
            data: listaCategorias
        }],
        drilldown: {
            series: listDrillSeries
        }
    });
}
var importId;
function verModalImportar(tipoImport){ 
	importId=parseInt(tipoImport);
	var modal=$("#modalImportarGeneral");
	modal.modal("show");
	modal.find("#labelSelAux").hide();
	modal.find("#divSelAux").hide();
	var titulo="",msjAyuda="",place="",funcionTexto="";
	switch(importId){
	case 1:
		titulo="Importar Contratistas";
		msjAyuda="Copie desde un Excel, teniendo en cuenta: " +
		"<br/> (Formato Fecha : DD/MM/YYYY)";
		place="RUC  /  Razón Social  / " +
		"Domicilio  /  Actividad Económica  /  Compañía de Seguros  / " +
		"Año de Inicio de Actividades  /  Persona de Contacto  / Correo  ";
		funcionTexto="guardarImportContratistas()";
		break;
	case 2:
		titulo="Importar Unidades";
		msjAyuda="Copie desde un Excel";
		place="Nombre ";
		funcionTexto="guardarImportUnidades()";
		break;
	case 3:
		titulo="Importar Áreas";
		msjAyuda="Copie desde un Excel, teniendo en cuenta: " +
		"<br/> (Debe existir la unidad ingresada)";
		place="Unidad  /  Nombre ";
		funcionTexto="guardarImportAreas()";
		break;
	case 4:
		titulo="Importar Responsables";
		msjAyuda="Copie desde un Excel";
		place="Nombre  /  Correo ";
		funcionTexto="guardarImportResponsables()";
		break;
	case 5:
		titulo="Importar Listas y preguntas correspondientes";
		msjAyuda="Copie desde un Excel";
		place="Lista  /  Preguntas ";
		funcionTexto="guardarImportListas()";
		break;
	case 6:
		titulo="Importar Proyectos";
		msjAyuda="Copie desde un Excel"+
		"<br/> (Debe existir el responsable, tipo de evaluación y área)" +
		"<br/> (La fecha debe de estar en el formato dd/mm/YYYY)";
		place="Título  /  Descripción  /  Responsable  /  Área Asociada  /  Fecha Inicio  / Fecha Fin  / " +
				"Tipo de evaluación   ";
		funcionTexto="guardarImportProyectos()";
		break;
	}
	
	
	modal.find(".modal-title").html(titulo);
	modal.find(".control-label").html(msjAyuda);
	modal.find("#txtListado").prop("placeholder",place);
	modal.find("#btnGuardarNuevos").attr("onclick",funcionTexto);
}

function guardarImportContratistas(){
	var texto = $("#txtListado").val();
	var listExcel = texto.split('\n');
	var listContratistas = [];
	var listRucsRepetidos = [];
	var listFullNombres = "";
	var validado = "";

	if (texto.length < 10000) {
		for (var int = 0; int < listExcel.length; int++) {

			var listCells = listExcel[int].split('\t');
			var filaTexto = listExcel[int];
			if (filaTexto.trim().length < 1) {
				validado = "Existen filas vacias.";
				break;
			}

			if (validado.length < 1 && listCells.length != 8) {
				validado = "No coincide el numero de celdas."
						+ listCells.length;
				break;
			} else {
				var contr = {};
				for (var j = 0; j < listCells.length; j++) {
					var element = listCells[j];
					if (j === 0) {
						contr.ruc = element.trim();
						if ($.inArray(contr.ruc, listRucsRepetidos) === -1) {
							listRucsRepetidos.push(contr.ruc);
						} else {
							listFullNombres = listFullNombres + contr.ruc + ", ";
						}

					}
					
					if (j === 1){
						contr.nombre = element.trim();
					}
					if (j === 2){
						contr.direccion = element.trim();
					}	
					if (j === 3){
						contr.actividad = element.trim();
					}
					if (j === 4){
						contr.seguro = element.trim();
					}
					if (j === 5){
						contr.anioInicio = element.trim();
					}
					if (j === 6){
						contr.contacto = element.trim();
					}
					if (j === 7){
						contr.contactoCorreo = element.trim();
					}
					contr.empresaId = getSession("gestopcompanyid");
					 
					 
				}

				listContratistas.push(contr);
				if (listRucsRepetidos.length < listContratistas.length) {
					validado = "Existen RUCs repetidos." + listFullNombres;
					break;
				}
			}
		}
	} else {
		validado = "Ha excedido los 10000 caracteres permitidos.";
	}

	if (validado.length < 1) {
		var r = confirm("¿Está seguro que desea subir estos "
				+ listExcel.length + " elemento(s)?");
		if (r == true) {
			var dataParam = {
				listObject : listContratistas
			};
			callAjaxPost(URL + '/contratista/masivo/save', dataParam,
					funcionResultadoMasivoGeneral);
		}
	} else {
		alert(validado);
	}
}
function guardarImportUnidades(){
	var texto = $("#txtListado").val();
	var listExcel = texto.split('\n');
	var listObject = [];
	var listRucsRepetidos = [];
	var listFullNombres = "";
	var validado = "";

	if (texto.length < 10000) {
		for (var int = 0; int < listExcel.length; int++) {

			var listCells = listExcel[int].split('\t');
			var filaTexto = listExcel[int];
			if (filaTexto.trim().length < 1) {
				validado = "Existen filas vacias.";
				break;
			}

			if (validado.length < 1 && listCells.length != 1) {
				validado = "No coincide el numero de celdas."
						+ listCells.length;
				break;
			} else {
				var contr = {};
				for (var j = 0; j < listCells.length; j++) {
					var element = listCells[j];
					 
					if (j === 0) {
						contr.nombre = element.trim();
						if ($.inArray(contr.nombre, listRucsRepetidos) === -1) {
							listRucsRepetidos.push(contr.nombre);
						} else {
							listFullNombres = listFullNombres + contr.nombre + ", ";
						}

					}
					 
					contr.empresaId = getSession("gestopcompanyid");
					 
					 
				}

				listObject.push(contr);
				if (listRucsRepetidos.length < listObject.length) {
					validado = "Existen nombres repetidos." + listFullNombres;
					break;
				}
			}
		}
	} else {
		validado = "Ha excedido los 10000 caracteres permitidos.";
	}

	if (validado.length < 1) {
		var r = confirm("¿Está seguro que desea subir estos "
				+ listExcel.length + " elemento(s)?");
		if (r == true) {
			var dataParam = {
				listObject : listObject
			};
			callAjaxPost(URL + '/contratista/unidades/masivo/save', dataParam,
					funcionResultadoMasivoGeneral);
		}
	} else {
		alert(validado);
	}
}

function guardarImportAreas(){
	var texto = $("#txtListado").val();
	var listExcel = texto.split('\n');
	var listObject = [];
	var listRucsRepetidos = [];
	var listFullNombres = "";
	var validado = "";

	if (texto.length < 10000) {
		for (var int = 0; int < listExcel.length; int++) {

			var listCells = listExcel[int].split('\t');
			var filaTexto = listExcel[int];
			if (filaTexto.trim().length < 1) {
				validado = "Existen filas vacias.";
				break;
			}

			if (validado.length < 1 && listCells.length != 2) {
				validado = "No coincide el numero de celdas."
						+ listCells.length;
				break;
			} else {
				var areaAux = {};
				for (var j = 0; j < listCells.length; j++) {
					
					var element = listCells[j];console.log(element);
					if (j === 0) {
						for (var k = 0; k < listClasificacion.length; k++) {
							if (listClasificacion[k].nombre == element.trim()) {
								areaAux.clasificacion = {
									id : listClasificacion[k].id
								};
							}
						}

						if (!areaAux.clasificacion) {
							validado = "Unidad no reconocida.";
							break;
						}
					}
					if (j === 1) {
						areaAux.nombre = element.trim();
						if ($.inArray(areaAux.nombre, listRucsRepetidos) === -1) {
							listRucsRepetidos.push(areaAux.nombre);
						} else {
							listFullNombres = listFullNombres + areaAux.nombre + ", ";
						}

					}
					 
					 
					 
				}

				listObject.push(areaAux); 
				if (listRucsRepetidos.length < listObject.length) {
					validado = "Existen nombres repetidos." + listFullNombres;
					break;
				}
			}
		}
	} else {
		validado = "Ha excedido los 10000 caracteres permitidos.";
	}

	if (validado.length < 1) {
		var r = confirm("¿Está seguro que desea subir estos "
				+ listExcel.length + " elemento(s)?");
		if (r == true) {
			var dataParam = {
				listObject : listObject
			};
			callAjaxPost(URL + '/contratista/areas/masivo/save', dataParam,
					funcionResultadoMasivoGeneral);
		}
	} else {
		alert(validado);
	}
}

function guardarImportResponsables(){
	var texto = $("#txtListado").val();
	var listExcel = texto.split('\n');
	var listObject = [];
	var listRucsRepetidos = [];
	var listFullNombres = "";
	var validado = "";

	if (texto.length < 10000) {
		for (var int = 0; int < listExcel.length; int++) {

			var listCells = listExcel[int].split('\t');
			var filaTexto = listExcel[int];
			if (filaTexto.trim().length < 1) {
				validado = "Existen filas vacias.";
				break;
			}

			if (validado.length < 1 && listCells.length != 2) {
				validado = "No coincide el numero de celdas."
						+ listCells.length;
				break;
			} else {
				var contr = {};
				for (var j = 0; j < listCells.length; j++) {
					var element = listCells[j];
					 
					if (j === 0) {
						contr.nombre = element.trim();
						if ($.inArray(contr.nombre, listRucsRepetidos) === -1) {
							listRucsRepetidos.push(contr.nombre);
						} else {
							listFullNombres = listFullNombres + contr.nombre + ", ";
						}

					}
					if (j === 1) {
						contr.correo = element.trim();
					 

					}
					contr.empresaId = getSession("gestopcompanyid");
					 
					 
				}

				listObject.push(contr);
				if (listRucsRepetidos.length < listObject.length) {
					validado = "Existen nombres repetidos." + listFullNombres;
					break;
				}
			}
		}
	} else {
		validado = "Ha excedido los 10000 caracteres permitidos.";
	}

	if (validado.length < 1) {
		var r = confirm("¿Está seguro que desea subir estos "
				+ listExcel.length + " elemento(s)?");
		if (r == true) {
			var dataParam = {
				listObject : listObject
			};
			callAjaxPost(URL + '/contratista/responsables/masivo/save', dataParam,
					funcionResultadoMasivoGeneral);
		}
	} else {
		alert(validado);
	}
}

function guardarImportListas(){
	var texto = $("#txtListado").val();
	var listExcel = texto.split('\n');
	var listListas = [];
	var list={};
	var listOpciones=[];
	var validado="";
	if (texto.length < 20000) {
		console.log(listExcel);
		for (var int = 0; int < listExcel.length; int++) {
			
			var listCells = listExcel[int].split('\t');
			var filaTexto = listExcel[int];
			if (filaTexto.trim().length < 1) {
				validado = "Existen filas vacias.";
				break;
			}

			if (validado.length < 1 && listCells.length != 2) {
				validado = "No coincide el numero de celdas."
						+ listCells.length;
				break;
			} else { 
				var pregunta={
						nombre:"",listaAsociada:{}
				};
				for (var j = 0; j < listCells.length; j++) {
					var element = listCells[j];
					
					if(j == 1){
						pregunta.nombre=element.trim();
					} 
					if (j == 0) {
						
						if(element.trim()==0){
							
						}else{
							list = {
									empresa:{empresaId:getSession("gestopcompanyid")},
									nombre:element.trim(),
									preguntas:[]
							};
							
						}
						
					}
					
					
				}
			
				list.preguntas.push(pregunta);
				if(listCells[0].length>0){
					listListas.push(list);
				}else{
					
				}
				
			}
		}
	} else {
		validado = "Ha excedido los 10000 caracteres permitidos.";
	}

	if (validado.length < 1) {
		var r = confirm("¿Está seguro que desea subir estas "
				+ listListas.length + " lista(s)?");
		if (r == true) {
			
			console.log(listListas);
			callAjaxPost(URL + '/contratista/listas/masivo/save', listListas,
					funcionResultadoMasivoGeneral);
		}
	} else {
		alert(validado);
	}
}

function guardarImportProyectos(){
	var texto = $("#txtListado").val();
	var listExcel = texto.split('\n');
	var listObject = [];
	var listTitulosRepetidos = [];
	var listFullNombres = "";
	var validado = "";

	if (texto.length < 10000) {
		for (var int = 0; int < listExcel.length; int++) {

			var listCells = listExcel[int].split('\t');
			var filaTexto = listExcel[int];
			if (filaTexto.trim().length < 1) {
				validado = "Existen filas vacias.";
				break;
			}

			if (validado.length < 1 && listCells.length != 7) {
				validado = "No coincide el numero de celdas."
						+ listCells.length;
				break;
			} else {
				var proyectoObj = {};
				for (var j = 0; j < listCells.length; j++) {
					
					var element = listCells[j];console.log(element);
					if (j === 0) {
						proyectoObj.titulo = element.trim();
						if ($.inArray(proyectoObj.titulo, listTitulosRepetidos) === -1) {
							listTitulosRepetidos.push(proyectoObj.titulo);
						} else {
							listFullNombres = listFullNombres + proyectoObj.titulo + ", ";
						}

					}
					if (j === 1) {
						proyectoObj.descripcion = element.trim();
						

					}
					if (j === 2) {
						listResponsableProy.forEach(function(val,index){
							if (val.nombre.trim() == element.trim()) {
								proyectoObj.responsable = {
									id : val.id
								};
							}
						})

						if (!proyectoObj.responsable) {
							validado = "Responsable no reconocida.";
							break;
						}
					}
					if (j === 3) {
						listAreaProyecto.forEach(function(val,index){
							if (val.nombre == element.trim()) {
								proyectoObj.area = {
									id : val.id
								};
							}
						})

						if (!proyectoObj.area) {
							validado = "Área no reconocida.";
							break;
						}
					}
					if (j === 4) {
						try {
							var fechaTexto = element.trim();
							var parts = fechaTexto.split('/');
							var dateNac = new Date(parts[2], parts[1] - 1,
									parts[0]);
							proyectoObj.fechaInicio = dateNac;
						} catch (err) {
							validado = "Error en el formato de fecha de inicio.";
							break;
						}
					}
					 
					if (j === 5) {
						try {
							var fechaTexto = element.trim();
							var parts = fechaTexto.split('/');
							var dateNac = new Date(parts[2], parts[1] - 1,
									parts[0]);
							proyectoObj.fechaFin = dateNac;
						} catch (err) {
							validado = "Error en el formato de fecha de fin.";
							break;
						}
					}
					if (j === 6) {
						listTipoEvaluacion.forEach(function(val,index){
							if (val.nombre == element.trim()) {
								proyectoObj.lista = {
									id : val.id
								};
							}
						})

						if (!proyectoObj.lista) {
							validado = "Tipo de  evaluación no reconocida.";
							break;
						}
					}
				}
				var fechaInicio=convertirFechaNormal2(proyectoObj.fechaInicio);
				var fechaFin=convertirFechaNormal2(proyectoObj.fechaFin);
				var hoyDia=obtenerFechaActual();
				var difInicio= restaFechas(hoyDia,fechaInicio);
				var difFinal= restaFechas(hoyDia,fechaFin);
				var proyectoEstado={};
				if(difInicio>0){
					proyectoEstado={id:"1",nombre:"Pendiente"};
				}else{
					if(difFinal>0){
						proyectoEstado={id:"2",nombre:"En Curso"};
					}else{
						proyectoEstado={id:"3",nombre:"Completado"};
					}
				}
				
				proyectoObj.tipoIperc={id:1};
				proyectoObj.estado=proyectoEstado;
				proyectoObj.empresaId=getSession("gestopcompanyid");
				listObject.push(proyectoObj); 
				if (listTitulosRepetidos.length < listObject.length) {
					validado = "Existen nombres repetidos." + listFullNombres;
					break;
				}
			}
		}
	} else {
		validado = "Ha excedido los 10000 caracteres permitidos.";
	}

	if (validado.length < 1) {
		var r = confirm("¿Está seguro que desea subir estos "
				+ listExcel.length + " elemento(s)?");
		if (r == true) {
			var dataParam = {
				listObject : listObject
			};
			callAjaxPost(URL + '/contratista/proyectos/masivo/save', dataParam,
					funcionResultadoMasivoGeneral);
		}
	} else {
		alert(validado);
	}
}

function funcionResultadoMasivoGeneral(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		if (data.MENSAJE.length > 0) {
			alert(data.MENSAJE);
		} else {
			alert("Se guardaron exitosamente.");
			switch(importId){
			case 1:
				listarContratistas();
				break;
			case 2:
				cargarClasificacionsContratista();
				break;
			case 3:
				cargarTiposContratista();
				break;
			case 4:
				cargarResponsablesSeguridad();
				break;
			case 5:
				cargarListasContratista();
				break;
			case 6:
				cargarProyectosSeguridad();
				break;
			}
			
			$('#modalImportarGeneral').modal('hide');
		}
		break;
	default:
		alert("Ocurri&oacute; un error al guardar el trabajador!");
	}
}






