var responsableId,responsableObj={},trabajadorAsociadoResponsableId;
var banderaEdicion4=false;
var listClasificacion=[{id:1,nombre:"DAF"},{id:2,nombre:"DINF"}];

var listTrabajadoresResponsable;
var contadorListTrab;
var listFullResponsableSeguridad;
$(function(){
	$("#btnNuevoResponsable").attr("onclick", "javascript:nuevoResponsable();");
	$("#btnCancelarResponsable").attr("onclick", "javascript:cancelarNuevoResponsable();");
	$("#btnGuardarResponsable").attr("onclick", "javascript:guardarResponsable();");
	$("#btnEliminarResponsable").attr("onclick", "javascript:eliminarResponsable();");
})
/**
 * 
 */
function cargarResponsablesSeguridad() {
	$("#btnCancelarResponsable").hide();
	$("#btnNuevoResponsable").show();
	$("#btnEliminarResponsable").hide();
	$("#btnGuardarResponsable").hide();
	trabajadorAsociadoResponsableId=null;
	volverDivResponsable();
	callAjaxPost(URL + '/contratista/responsables', 
			 {empresaId:getSession("gestopcompanyid")}, function(data){
				if(data.CODE_RESPONSE=="05"){
					listFullResponsableSeguridad=data.listResponsableSeguridad;
					listTrabajadoresResponsable=data.trabajadores;
					banderaEdicion4=false;
					$("#tblResponsables tbody tr").remove();
					listFullResponsableSeguridad.forEach(function(val,index){
						var proyects=val.proyectos;
						var numActiv=0;
						proyects.forEach(function(val2,index2){
							if(val2.isActivo==1){
								numActiv++;
							}
						});
						$("#tblResponsables tbody").append(
								"<tr id='trtip"+val.id+"' onclick='editarResponsable("+index+")'>" +
//							
								"<td id='respnom"+val.id+"'>"+val.nombre+"</td>" +
								"<td id='respcorreo"+val.id+"'>"+val.correo+"</td>" +
									"<td id='tipobs"+val.id+"'>"+numActiv+" / "+proyects.length+"</td>" +
							
								"</tr>");
					});
					goheadfixedY("#tblResponsables","#wrapResp");
					formatoCeldaSombreableTabla(true,"tblResponsables");
				}else{
					console.log("NOPNPO")
				}
			});
	
}

function editarResponsable(pindex) {


	if (!banderaEdicion4) {
		formatoCeldaSombreableTabla(false,"tblResponsables");
		responsableId = listFullResponsableSeguridad[pindex].id; 
		responsableObj= listFullResponsableSeguridad[pindex]
		
		var descripcion=responsableObj.nombre;
		var correo=responsableObj.correo;
		  //
		$("#respnom" + responsableId)
		.html(	"<input type='text' id='inputNombreResppp' class='form-control'>");
		 
		 //
		$("#respcorreo" + responsableId)
		.html(	"<input type='text' id='inputCorreoResppp' class='form-control'>");
			$("#inputCorreoResppp").val(correo);
			
		$("#tipobs" + responsableId).html("<a onclick='verProyectosResponsable()'>"+
		$("#tipobs" + responsableId).text()+"</a>");
		banderaEdicion4 = true;
		$("#btnCancelarResponsable").show();
		$("#btnNuevoResponsable").hide();
		$("#btnEliminarResponsable").show();
		$("#btnGuardarResponsable").show();
		$("#btnGenReporte").hide();
		
		
		//
		trabajadorAsociadoResponsableId=responsableObj.trabajadorId;
		listTrabajadoresResponsable.forEach(function(val){
			if(responsableObj.trabajadorId!=null){
			if(val.trabajadorId==responsableObj.trabajadorId){
				$("#inputNombreResppp").val(val.nombre);
				
			}
			}
		});
		var listaTrabs=listarStringsDesdeArray(listTrabajadoresResponsable,"nombre");
		ponerListaSugerida("inputNombreResppp",listaTrabs,true);
		$("#inputNombreResppp").on("autocompleteclose", function(){
			var nombreTrab=$("#inputNombreResppp").val();
			listTrabajadoresResponsable.every(function(val) {
					if(val.nombre.trim()==nombreTrab.trim()){
						trabajadorAsociadoResponsableId=val.trabajadorId;
						$("#inputCorreoResppp").val(val.correo);
					return false;
					}
					return true;
				})
		});
		// 
		
	}
	
}
function volverDivResponsable(){
	$("#tabResponsable .container-fluid").hide();
	$("#divContainResponsables").show();
	$("#tabResponsable h2").html("Responsables de Seguridad Interno")
}
function verProyectosResponsable(){ 
	$("#tabResponsable .container-fluid").hide();
	$("#divProyResponsable").show();
	$("#tblProyResponsable tbody").html("");
	$("#tabResponsable h2").html("<a onclick='volverDivResponsable()'> Responsable "+responsableObj.nombre+"</a> > Proyectos")
	listFullResponsableSeguridad.forEach(function(val,index){ 
				 if(val.id==responsableId){ 
					var proyects=val.proyectos;
					proyects.every(function(val2,index2){
						
						$("#tblProyResponsable tbody").append(
								"<tr id=' "+val2.id+"'  >" +
								"<td id=' "+val2.id+"'>"+val2.titulo+"</td>" +
								"<td id=' "+val2.id+"'>"+val2.postulante.contratista.nombre+"</td>" + 
								"<td id=' "+val2.id+"'>"+val2.fechaInicioTexto+"</td>" + 
								"<td id=' "+val2.id+"'>"+val2.fechaFinTexto+"</td>" + 
								
								"<td id=' "+val2.id+"'>"+(val2.isActivo==1?"Activo":val2.estado.nombre)+"</td>" + 
								 
								"</tr>");
					 
						 
					 
							return true;
						 
					});
					 
				 }
			 
		 });	
}
function nuevoResponsable() {
	if (!banderaEdicion4) {
		responsableId = 0; 
		$("#tblResponsables tbody:first")
				.prepend(
						"<tr id='0'>"
					 
							
								+ "<td><input type='text' id='inputNombreResppp' class='form-control'></td>"
								+ "<td><input type='text' id='inputCorreoResppp' class='form-control'></td>"
								
								+ "<td>...</td>"
								
								+ "</tr>");
		var listaTrabs=listarStringsDesdeArray(listTrabajadoresResponsable,"nombre");
		ponerListaSugerida("inputNombreResppp",listaTrabs,true);
		$("#inputNombreResppp").on("autocompleteclose", function(){
			var nombreTrab=$("#inputNombreResppp").val();
			var correo;
			listTrabajadoresResponsable.every(function(val) {
					if(val.nombre.trim()==nombreTrab.trim()){
						correo=val.correo;
						trabajadorAsociadoResponsableId=val.trabajadorId;
						responsableObj.nombre=val.nombre;
					$("#inputCorreoResppp").val(correo);
					return false;
					}
					return true;
				})
		});
		formatoCeldaSombreableTabla(false,"tblResponsables");
		$("#btnCancelarResponsable").show();
		$("#btnNuevoResponsable").hide();
		$("#btnEliminarResponsable").hide();
		$("#btnGuardarResponsable").show();
		$("#btnGenReporte").hide();
		banderaEdicion4 = true;
	} else {
		alert("Guarde primero.");
	}
}

function cancelarResponsable() {
	cargarTiposContratista();
}

function eliminarResponsable() {
	var r = confirm("¿Está seguro de eliminar la programacion?");
	if (r == true) {
		var dataParam = {
				id : responsableId,
		};

		callAjaxPost(URL + '/contratista/responsable/delete', dataParam,
				function(data) {
					switch (data.CODE_RESPONSE) {
					case "05":
						cargarResponsablesSeguridad();
						break;
					default:
						alert("Ocurrió un error al eliminar la programacion!");
					}
				});
	}
}

function guardarResponsable() {

	var campoVacio = true;
	var perdida=$("#slcClasificacion").val();  
var nombre=$("#inputNombreResppp").val() ;
var correo=$("#inputCorreoResppp").val(); 
 
		if (campoVacio) {

			var dataParam = {
				id : responsableId, 
				nombre: nombre,
				correo:correo,
				trabajadorId:trabajadorAsociadoResponsableId,
				empresaId:getSession("gestopcompanyid")
			};

			callAjaxPost(URL + '/contratista/responsable/save', dataParam,
					function(data) {
						switch (data.CODE_RESPONSE) {
						case "05":
						
							cargarResponsablesSeguridad();
							break;
						default:
							console.log("Ocurrió un error al guardar la programacion!");
						}
					});
			
		} else {
			alert("Debe ingresar todos los campos.");
		}
	
	
}


function cancelarNuevoResponsable(){
	cargarResponsablesSeguridad();
}




