/**
 * 
 */
var listFullTrabajadoresContratista=[];
var listTipoDocumento=[];
var listTipoSangre=[];
var listSubTipoHojaVida=[];
var objTrabajadorContratista={id:0};
var indexTrabContratista;
var listActividadesCriticasTrabajador;
var listProyectosAsignar;
//
function toggleMenuOpcionTrabGeneral(obj){
	$(obj).parent(".tituloSubList").find("ul").toggle();
}
var funcionalidadesTrabajadorGeneral=[ 
{id:"opcGen1",nombre:"<i class='fa fa-plus-square'></i> Registrar nuevo trabajador",functionClick:function(data){$("#nuevoMovilTrabProy").find(".contenidoSubList").show() } },
{id:"opcGen2",nombre:"<i class='fa fa-download'></i> Descargar formato de importación",functionClick:function(data){verAyudaImportTrabajadoresContratista()} },
{id:"opcGen3",nombre:"<i class='fa fa-download'></i> Descargar formato de hoja de vida",functionClick:function(data){verAyudaImportHojaVidaTrabajadoresContratista()} },
{id:"opcGen4",nombre:"<i class='fa fa-upload'></i> Importar trabajadores",functionClick:function(data){$("#nuevoImportTrabContratista").find(".contenidoSubList").show() } }
                               ]
function marcarSubOpcionTrabajadorGeneral(pindex){
	$(".contenidoSubList").hide();
	funcionalidadesTrabajadorGeneral[pindex].functionClick();
	$(".listaGestionGosst").hide();
}
//
function toggleMenuOpcionTrab(obj,pindex){
	objTrabajadorContratista=listFullTrabajadoresContratista[pindex];
	indexTrabContratista=pindex;
	$(obj).parent(".eventoGeneral").find("ul").toggle();
	$(obj).parent(".eventoGeneral").siblings().find("ul").hide(); 	
}
var funcionalidadesTrabajador=[
                               
{id:"asdf",nombre:"<i class='fa fa-eye'></i> Ver Historial",functionClick:function(data){ },isTitulo:true },
{id:"opcHistorialExamen",nombre:"Historial Médico",functionClick:function(data){verHistorialExamenTrabajadorContratista(1)}  },
{id:"opcHistorialSctr",nombre:"Historial SCTR",functionClick:function(data){verHistorialExamenTrabajadorContratista(2)}  },
{id:"opcHistorialInducciones",nombre:"Historial Inducciones",functionClick:function(data){verHistorialExamenTrabajadorContratista(3)}  },

{id:"opcEditar",nombre:"<i class='fa fa-edit'></i> Editar Registro",functionClick:function(data){editarTrabajadorContratista()}  },


{id:"opcHistoasadrialExamen",nombre:"<i class='fa fa-plus-square'></i> Agregar Documento",functionClick:function(data){ } ,isTitulo:true },
{id:"opcHojanmn",nombre:"Nueva Hoja de Vida",functionClick:function(data){nuevaHojaVidaTrabajador()}  },
{id:"opcHojann",nombre:" Asignar actividades críticas",functionClick:function(data){asignarActividadesCriticasTrabajador()}  }
//,{id:"opcElimnar",nombre:"<i class='fa fa-trash'></i> Eliminar",functionClick:function(data){eliminarTrabajadorContratista()}  }
                               ]
function marcarSubOpcionTrabajador(pindex){ 
	funcionalidadesTrabajador[pindex].functionClick();
	$(".listaGestionGosst").hide();
}
function verLeyendaTrabajador(){
	$("#leyendaMovilTrabProy").show();
	$("#leyendaMovilTrabProy").find(".contenidoSubList").show() 
}
function ocultarLeyendaTrabajador(){
	$("#leyendaMovilTrabProy").hide();
	$("#leyendaMovilTrabProy").find(".contenidoSubList").hide() 
}
function verHistorialExamenTrabajadorContratista(examTipoId){
	var contratistaId=parseInt(getSession("contratistaGosstId"));
	var dataParam={ 
			id:(contratistaId),
			examen:{tipo:{id:examTipoId}}
			};
	callAjaxPost(URL + '/contratista/examenes', dataParam, function(data) {
		$(".divMovilHistorialTrabajador").html("");
		$(".divMovilHistorialTrabajador").hide();
		$("#divHistorialTrab"+objTrabajadorContratista.id).show();
		data.examenes.forEach(function(val,index){ 
			var trabajadorAuxId=objTrabajadorContratista.id;
			if(val.trabajadoresId.indexOf(trabajadorAuxId)!=-1){
				var textIn="<div class='detalleAccion '>" +
						"<i aria-hidden='true' class='fa fa-file'></i>" +val.evidenciaNombre+"<br>"+
						"<i aria-hidden='true' class='fa fa-hourglass-start'></i>Inicio: " +val.fechaTexto+"<br>"+
						"<i aria-hidden='true' class='fa fa-hourglass-end'></i>Vence: " +val.fechaFinVigenciaTexto+
						"</div>" +
						"<div class='opcionesAccion'><a target='_blank' " +
						"href='"+URL+"/contratista/examen/evidencia?id="+val.id+"  ' >Descargar </a></div>";
				$("#divHistorialTrab"+objTrabajadorContratista.id).append(textIn);
			}
		});
	})
}
 function asignarActividadesCriticasTrabajador(){
	$("#editarMovilActCritTrab").find(".tituloSubList")
	.html(textoBotonVolverContenido+"Nuevas Actividades Críticas "
		+" del trabajador '"	+objTrabajadorContratista.nombre+"' ");
	$(".divListPrincipal>div").hide();
	callAjaxPost(URL + '/contratista/trabajador/actividades_criticas', {id:objTrabajadorContratista.id}, function(data) {
		
		$("#editarMovilActCritTrab").show();
		$("#editarMovilActCritTrab").find("input").prop("checked",false);
		data.list.forEach(function(val,index){
			if(val.actividad){
				$("#checkActTr"+val.actividad.id).prop("checked",true)
			}
			
		});
		
	})
}
function guardarActividadesCriticasTrabajadorContratista(){
	var listActs=[];
	listActividadesCriticasTrabajador.forEach(function(val,index){
		if($("#checkActTr"+val.id).prop("checked")){
			listActs.push({id:val.id})
		}
	});
	callAjaxPost(URL + '/contratista/trabajador/actividades_criticas/save', {id:objTrabajadorContratista.id,
		actividadesCriticas:listActs}, function(data) {
			volverDivSubContenido();
			habilitarTrabajadoresContratista();
	})
	
}
function habilitarTrabajadoresContratista(){
	objTrabajadorContratista={id:0}
	var contratistaId=parseInt(getSession("contratistaGosstId"));
	var dataParam={ 
			id:(contratistaId),empresaId:getSession("gestopcompanyid")
			};
	callAjaxPost(URL + '/contratista/trabajadores', dataParam, function(data) {
		switch (data.CODE_RESPONSE) {
		case "05":
			var listPanelesPrincipal=[];
			var textIn= "";
			listFullTrabajadoresContratista=data.trabajadores;
			listTipoDocumento=data.tipoDocumento;
			listTipoSangre=data.tipoSangre;
			listSubTipoHojaVida=data.subTipoHoja;
			listProyectosAsignar=data.proyectos;
			
			perfilContratista=data.perfil;
			if(perfilContratista!=null){
			var textSecciones="";
			var listSecciones=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18];
			listSecciones.forEach(function(val){
				var noEsta=true;
				perfilContratista.secciones.forEach(function(val1){
					if(val1.id==val){
						noEsta=false;
					}
				});
				if(noEsta){
					textSecciones+=val+","
				}
			})
			proyectoObj.examenesPermitidosId=textSecciones;
			}
			$(".divListPrincipal").html("");
			listFullTrabajadoresContratista.forEach(function(val,index){
				var menuOpcion="<ul class='list-group listaGestionGosst' >";
				funcionalidadesTrabajador.forEach(function(val1,index1){
					var estiloExtra="";
					if(val1.isTitulo){
						estiloExtra="style='border: 2px solid black; background: #2e9e8f !important; color: white !important;cursor: auto;' ";
					}
					menuOpcion+="<li "+estiloExtra+" class='list-group-item' onclick='marcarSubOpcionTrabajador("+index1+")'>"+val1.nombre+" </li>"
				});
				menuOpcion+="</ul>";
				
				var claseGosst="gosst-neutral";
				var verificacionObj=verificarTrabajadorContratistaHabilitado(val);
				
				if(verificacionObj.isHabilitadoContratista ){
					claseGosst="gosst-aprobado"
				}
				var textoExam1=""
				var textoExam2="" 
				var textoExam3="" 
				var textoExam4="" 
				
				var textoExam6=""  
				
				var textoExam8=""
				var textHojaVida="" ;
				var linkHojaVida=" " ;
				if(perfilContratista!=null){
					var docsId=perfilContratista.secciones;
					docsId.forEach(function(val1){
						switch(parseInt(val1.id)){
						case 16:
							textHojaVida="<i class='fa fa-info' aria-hidden='true'></i>"+""+verificacionObj.eval4.textoEval.replace("<br>","")+"  <br>";
							linkHojaVida="<a style='font-weight: 900' onclick='cargarHojaVidaTrabajador(4,"+index+")'  >" +
					"<i class='fa fa-info' aria-hidden='true'></i>"+" "+ 
					"  Hoja Vida: "+val.numHojaVida4+" " +
							"<i aria-hidden='true' class='fa fa fa-angle-double-down'></i></a>";
							break;
						case 6:
							textoExam1="<i class='fa fa-medkit' aria-hidden='true'></i>" +verificacionObj.textoExamen.replace("<br>","")+"<br>";
							break;
						case 7:
							textoExam2="<i class='fa fa-folder-open-o' aria-hidden='true'></i>" +verificacionObj.textoSctr.replace("<br>","")+"<br>";
							break;
						case 9:
							textoExam3="<i class='fa fa-arrow-circle-o-down' aria-hidden='true'></i>" +verificacionObj.textoInduccion.replace("<br>","")+"<br>";
							break;
						case 11:
							textoExam4="<i class='fa fa-inbox' aria-hidden='true'></i>" +verificacionObj.textoCargo.replace("<br>","")+"<br>";
							break;
						case 12:
							textoExam6=""+verificacionObj.textoTregLong;
							break;
						case 8:
							textoExam8="<i class='fa fa-fire-extinguisher' aria-hidden='true'></i>" +verificacionObj.textoEpp.replace("<br>","")+"<br>";
							break;
						}
					});
					
				}else{
					textHojaVida="<i class='fa fa-info' aria-hidden='true'></i>"+""+verificacionObj.eval4.textoEval.replace("<br>","")+"  <br>";
					linkHojaVida="<a style='font-weight: 900' onclick='cargarHojaVidaTrabajador(4,"+index+")'  >" +
			"<i class='fa fa-info' aria-hidden='true'></i>"+" "+ 
			"  Hoja Vida: "+val.numHojaVida4+" " +
					"<i aria-hidden='true' class='fa fa fa-angle-double-down'></i></a>";
					textoExam1="<i class='fa fa-medkit' aria-hidden='true'></i>" +verificacionObj.textoExamen.replace("<br>","")+"<br>";
					textoExam2="<i class='fa fa-folder-open-o' aria-hidden='true'></i>" +verificacionObj.textoSctr.replace("<br>","")+"<br>";
					textoExam3="<i class='fa fa-arrow-circle-o-down' aria-hidden='true'></i>" +verificacionObj.textoInduccion.replace("<br>","")+"<br>";
					textoExam4="<i class='fa fa-inbox' aria-hidden='true'></i>" +verificacionObj.textoCargo.replace("<br>","")+"<br>";
					textoExam6=""+verificacionObj.textoTregLong;
					textoExam8="<i class='fa fa-fire-extinguisher' aria-hidden='true'></i>" +verificacionObj.textoEpp.replace("<br>","")+"<br>";
				}
				textIn+=("<div class='eventoGeneral "+claseGosst+"'>" +
						"<a class='efectoLink btn-gestion' onclick='toggleMenuOpcionTrab(this,"+index+")'>Ver Opciones &nbsp;"
						+"<i class='fa fa-angle-double-down' aria-hidden='true'></i></a> " +
						menuOpcion+
						"<i class='fa fa-user' aria-hidden='true'></i><strong>" +val.nombre+" , "+val.apellido+"</strong><br>"+
						"<i class='fa fa-building-o' aria-hidden='true'></i>" +val.area+"-"+val.cargo+"<br>"+ 
						"<i class='fa  fa-address-card-o' aria-hidden='true'></i>" +val.tipoDocumento.nombre+": "+val.nroDocumento+"<br>"+
						textoExam1+ 
						textoExam2+ 
						textoExam3+ 
						textoExam4+ 
						textoExam8+ 

						textoExam6+
						"Actividades Críticas ("+val.numActividadesCriticas+") <br>" +
						linkHojaVida+
						"</div>" +
						"<div class='divMovilHistorialTrabajador' id='divHistorialTrab"+val.id+"'>" +
						"" +
						"</div>");
			});
			 
			var selTipoDocTrab= crearSelectOneMenuOblig("selTipoDocTrab", "", listTipoDocumento, "", 
					"id","nombre")
			var selTipoSangreTrab= crearSelectOneMenuOblig("selTipoSangreTrab", "", listTipoSangre, "", 
					"id","nombre");
			var selTipoProyTrab= crearSelectOneMenuOblig("selTipoProyTrab", "", listProyectosAsignar, "", 
					"id","titulo","Ninguno");
			var listItemsFormTrab=[
            		{sugerencia:"",label:"Tipo Documento",divContainer:"divTipoDoc",inputForm:selTipoDocTrab},
            		{sugerencia:"",label:"N° de Documento",divContainer:"divNumDoc",inputForm:"<input class='form-control'   type='text' id='inputNroDocTrabajadorContr' placeholder='Ejm: 77485784 &nbsp(*)'>"},
            		 {sugerencia:"",label:"Nombre ",divContainer:"divNombreTrab",inputForm:"<input  class='form-control' type='text' id='inputNombreTrabajadorContr' placeholder='Ejm: Juan &nbsp(*)'>" } ,
            		 {sugerencia:"",label:"Apellidos ",divContainer:"divApelidoTrab",inputForm:"<input  class='form-control' type='text' id='inputApellidoTrabajadorContr' placeholder='Ejm: Perez Ramirez &nbsp(*)'>" } ,
            		 {sugerencia:"",label:"Área",divContainer:"divAreaTrab",inputForm:"<input class='form-control' type='text' id='inputAreaTrabajadorContr' placeholder='Ejm: Proyectos &nbsp(*)'>"},
            		 {sugerencia:"",label:"Cargo ",divContainer:"divCargoTrab",inputForm:"<input class='form-control' type='text' id='inputCargoTrabajadorContr' placeholder='Ejm: Operario &nbsp(*)'>"}, 
            		 {sugerencia:"",label:"Código Interno ",divContainer:"divCodigoTrab",inputForm:"<input class='form-control' type='text' id='inputCodigoTrabajadorContr' placeholder='Identificación usada en la empresa &nbsp(*)'>"}, 
            		  {sugerencia:"",label:"Fecha Nacimiento ",divContainer:"divFechaNacTrab",inputForm:"<input class='form-control' type='date' onchange='colocarEdadTrabContratista()' id='inputFechaTrabajadorContr' placeholder=' '>"}, 
            		 {sugerencia:"",label:"Edad",divContainer:"divEdadTrab",inputForm:" ",divContainer:"divEdadTrabajadorContr"}, 
            		 {sugerencia:"",label:"Lugar Nacimiento",divContainer:"divLugarNacTrab",inputForm:"<input class='form-control'   id='inputNacioTrabajadorContr' placeholder='Ejm: Lima - Perú'>"}, 
            		 {sugerencia:"",label:"Lugar Residencia",divContainer:"divLugarResiTrab",inputForm:"<input class='form-control'   id='inputResidenciaTrabajadorContr' placeholder='Residencia Actual &nbsp(*)'>"}, 
            		 {sugerencia:"",label:"Teléfono",divContainer:"divTelefTrab",inputForm:"<input class='form-control'   id='inputTelefonoTrabajadorContr' placeholder='Ejm: 01 4295954 &nbsp(*)'>"}, 
            		 {sugerencia:"",label:"Correo",divContainer:"divCorreoTrab",inputForm:"<input class='form-control'   id='inputCorreoTrabajadorContr' placeholder='Ejm: JuanPerez@gmail.com &nbsp(*)'>"}, 
            		 {sugerencia:"",label:"N° Seguro Essalud / EPS",divContainer:"divEssaludTrab",inputForm:"<input class='form-control'   id='inputNroSeguroTrabajadorContr' placeholder=' '>"}, 
            		 {sugerencia:"",label:"N° CIP",divContainer:"divCipTrab",inputForm:"<input class='form-control'   id='inputNroCipTrabajadorContr' placeholder=' '>"}, 
            		 {sugerencia:"",label:"Tipo Sanguíneo",divContainer:"divTipoSangreTrab",inputForm:selTipoSangreTrab},
              		 {sugerencia:"",label:"Contacto emergencia",divContainer:"divContaEmerTrab",inputForm:"<input class='form-control' id='inputContactoTrabajadorContr' placeholder='Telefono Fijo o Celular &nbsp(*)'>"}, 
            		 {sugerencia:"",label:"Telefono Contacto emergencia",divContainer:"divTelefEmergTrab",inputForm:"<input class='form-control' id='inputFonoContactoTrabajadorContr' placeholder='Telefono Fijo o Celular &nbsp(*)'>"}, 
            		 {sugerencia:"Aparecerá en el fotocheck",label:"Foto ",inputForm:" " ,divContainer:"divEviFoto"} ,
            		 {sugerencia:"",label:"Proyecto Asignado",inputForm:selTipoProyTrab,divContainer:"divProyTra"},
              		
            		 
            		 {sugerencia:"",label:"",divContainer:"divButtonSave",inputForm:"<button type='submit' class='btn btn-success' ><i aria-hidden='true' class='fa fa-paper-plane'></i>Guardar</button>"} 
             		];
			
			
			
			var listItemsFormHojaVidaTrab=[
           {sugerencia:"",label:"Tipo ",divContainer:"divSelSubTipoHoja"}, 
           {sugerencia:"",label:"Evidencia ",divContainer:"eviHojaVidaTrabajador"}, 
            {sugerencia:"",label:"Detalle",inputForm:"<input class='form-control'   id='inputObsHojaVida' placeholder=' '>"}, 
           {sugerencia:"Ej: duración de experiencia, tiempo de estudio",divContainer:"divTiempoHojaVida",label:"Tiempo asociado (años)",inputForm:"<input class='form-control' type='number'   id='inputTiempoHojaVida' placeholder=' '>"}, 
           {sugerencia:"",label:"",inputForm:"<button type='submit' class='btn btn-success' ><i aria-hidden='true' class='fa fa-paper-plane'></i>Guardar</button>"},
           {sugerencia:"",label:"",inputForm:"<button type='button' id='btnGuardarAgregarHojaVida' class='btn btn-success' ><i aria-hidden='true' class='fa fa-paper-plane'></i>Guardar y agregar otro</button>"}
                        			
                               ];
			var textFormTrabajador="";
			listItemsFormTrab.forEach(function(val,index){
				textFormTrabajador+=obtenerSubPanelModulo(val);
			});
			var textFormHojaVidaTrabajador="";
			listItemsFormHojaVidaTrab.forEach(function(val,index){
				textFormHojaVidaTrabajador+=obtenerSubPanelModulo(val);
			});
			var textFormActCritTrabajador="";
			var listItemsFormActCritTrab=[];
			listActividadesCriticasTrabajador=data.actividades_criticas;
			data.actividades_criticas.forEach(function(val,index){
				listItemsFormActCritTrab
				.push({sugerencia:"",label:"<input class='form-control' type='checkbox' id='checkActTr"+val.id+"'>",
						inputForm:"<strong>"+val.tipo.nombre+"</strong><br>"+val.nombre})
			});
			listItemsFormActCritTrab
			.push({sugerencia:"",label:"",
				inputForm:"<button type='submit' class='btn btn-success' ><i aria-hidden='true' class='fa fa-paper-plane'></i>Guardar</button>"} )
			 
      		
			listItemsFormActCritTrab.forEach(function(val,index){
				textFormActCritTrabajador+=obtenerSubPanelModulo(val);
			});
			
			var textFormImportTrabajador="<textarea style='resize:vertical;min-height: 150px' class='form-control' id='importTrabajadoresContratista' " +
					"placeholder='Tipo Documento - N° Documento - Nombre - Apellido  - Área - Cargo - Código Interno  -  " +
					"Fecha Nacimiento (dd/mm/AAAA)  -  Lugar Nacimiento  -  Lugar Residencia -  Teléfono  -  Correo   -  " +
					"N° Seguro Essalud /EPS  - N° CIP  -  Tipo Sanguíneo  -  Contacto emergencia  -  Teléfono Contacto emergnecia'>" +
					"</textarea>" +
					"<button type='submit' style='margin-top: 10px;' class='btn btn-success' ><i aria-hidden='true' class='fa fa-paper-plane'></i>Guardar</button>"
			var btnAyudaImport="";
			listPanelesPrincipal
			.push({id:"descargarHoja" ,clase:"",
				nombre:"",
				contenido:""} );
			
					listPanelesPrincipal.push(
							{id:"nuevoMovilTrabProy" ,clase:"",
								nombre:""+"Nuevo trabajador",
								contenido:"<form id='formNuevoTrabajador' class='eventoGeneral'>"+textFormTrabajador+"</form>"
							},
							{id:"nuevoImportTrabContratista" ,clase:"",
								nombre:""+btnAyudaImport+"Importar trabajadores",
								contenido:"<form id='formImportTrabajador' class='eventoGeneral'>"+textFormImportTrabajador+"</form>"
							},
							{id:"leyendaMovilTrabProy" ,clase:"contenidoFormVisible",
								nombre:""+"Leyenda de documentos de trabajadores",
								contenido:"" +
									"<div class='eventoGeneral'>" +
										""+iconoGosst.desaprobado+" : Sin registrar documento <br>" +
										""+iconoGosst.por_evaluar+" : Documento registrado , sin evaluar <br>" +
										""+iconoGosst.advertir+" : Documento evaluado sin aprobar, documento no vigente <br>" +
										""+iconoGosst.aprobado+" : Documento evaluado aprobado <br>" +
									"</div>"
							},
							
							{id:"editarMovilTrabProy" ,clase:"contenidoFormVisible",
								nombre:""+"Editar trabajador",
								contenido:"<form id='formEditarTrabajador' class='eventoGeneral'>"+textFormTrabajador+"</form>"
							},
							{id:"editarMovilHojaVidaTrab" ,clase:"contenidoFormVisible",
								nombre:""+"Editar asd",
								contenido:"<form id='formEditarHojaVidaTrabajador' class='eventoGeneral'>"+textFormHojaVidaTrabajador+"</form>"
							},
							{id:"editarMovilActCritTrab" ,clase:"contenidoFormVisible",
							nombre:""+"Editar asd",
							contenido:"<form id='formEditarActCriticaTrabajador' class='eventoGeneral'>"+textFormActCritTrabajador+"</form>"
							},
							{id:"trabContratista",clase:"divTrabajadorContratistaGeneral",
								nombre:"Trabajadores Registrados (" +listFullTrabajadoresContratista.length+")&nbsp <i onclick='ocultarLeyendaTrabajador()' onmouseenter='verLeyendaTrabajador()' class='fa fa-info-circle'></i>",
								contenido:textIn
							} 
					);
			
			agregarPanelesDivPrincipal(listPanelesPrincipal);
			if(perfilContratista!=null){
				$("#nuevoMovilTrabProy").find(".form-group").hide();
				$("#nuevoMovilTrabProy").find("#divButtonSave").parent().show();
				
				
				var camposId=perfilContratista.camposTrabajador ;
				camposId.forEach(function(val1){
					var divFind=""
					 
					$("#nuevoMovilTrabProy").find("#"+val1.divContainer).parent().show();
					$("#editarMovilTrabProy").find("#"+val1.divContainer).parent().show();
					
					
				});
				
			}
			
			$("#descargarHoja").find(".tituloSubList").hide();
			$("#nuevoImportTrabContratista").find(".tituloSubList").hide();
			$("#leyendaMovilTrabProy").find(".tituloSubList").hide();
			var menuOpcion="<ul class='list-group listaGestionGosst' >";
			funcionalidadesTrabajadorGeneral.forEach(function(val1,index1){
				menuOpcion+="<li class='list-group-item' onclick='marcarSubOpcionTrabajadorGeneral("+index1+")'>"+val1.nombre+" </li>"
			});
			menuOpcion+="</ul>";
			
			$("#nuevoMovilTrabProy").find(".tituloSubList").html("Agregar trabajador "
					+"<a class='efectoLink btn-gestion' onclick='toggleMenuOpcionTrabGeneral(this)'>Ver Opciones &nbsp;"
					+"<i class='fa fa-angle-double-down' aria-hidden='true'></i></a>"+menuOpcion);
			var textoBtn3=""
			$("#descargarHoja").find(".tituloSubList").html(textoBtn3+"Descargar Formato Hoja de Vida");
			
			var options=
			{container:"#divEviFoto",
					functionCall:function(){},
					descargaUrl: "",
					esNuevo:true,
					idAux:"FotoTrabajador0",
					evidenciaNombre:""};
			crearFormEvidenciaCompleta(options);
			
			
			$(".listaGestionGosst").hide();
			$(".contenidoFormVisible").hide();
			$("#formEditarActCriticaTrabajador").on('submit', function(e) {  
		        e.preventDefault(); 
		        guardarActividadesCriticasTrabajadorContratista();
		 });
			 $('#formNuevoTrabajador').on('submit', function(e) {  
			        e.preventDefault();
			        objTrabajadorContratista={id:0}
			        guardarTrabajadorContratista();
			 });
			 $('#formEditarTrabajador').on('submit', function(e) {
			        e.preventDefault(); 
			        guardarTrabajadorContratista();
			 });
			 $('#formImportTrabajador').on('submit', function(e) {  
			        e.preventDefault(); 
			        guardarTrabajadorImportContratista();
			 });
			 $('#formEditarHojaVidaTrabajador').on('submit', function(e) {  
			        e.preventDefault(); 
			        guardarHojaVidaTrabajador();
			 });
			 $("#btnGuardarAgregarHojaVida").on("click",function(e){
				
				 guardarHojaVidaTrabajador(nuevaHojaVidaTrabajador);
			 });
			break;
			default:
				alert("nop");
				break;
		}
	})
	
}
function verAyudaImportTrabajadoresContratista(){ 
		window.open(URL+"/contratista/plantilla/trabajador","_blank");
}
function verAyudaImportHojaVidaTrabajadoresContratista(){ 
	window.open(URL+"/empresa/documento/evidencia?tipoId=3&id="+getSession("gestopcompanyid")+"","_blank");
}
 function colocarEdadTrabContratista() {

	var edad = calcularEdadAuto($("#inputFechaTrabajadorContr").val());

	$("#divEdadTrabajadorContr" ).html(edad);
}
 function verificarNumUnicoTrabajador(nroDoc){
	 var numTrab=nroDoc;
	 var listNum=listarStringsDesdeArray(listFullTrabajadoresContratista,"nroDocumento");
	  
	 if(numTrab==objTrabajadorContratista.nroDocumento){
		  
	  }else{
		 if(listNum.indexOf(numTrab)!=-1){
			 $("#inputNroDocTrabajadorContr").val("");
			 return "Nro de documento ya registrado: "+nroDoc;
		 }
	  }
	 return "";
 }
function guardarTrabajadorImportContratista(){
	var texto = $("#importTrabajadoresContratista").val();
	var listExcelInicial = texto.split('\n');
	var listTrabs = [];
	var listDnisRepetidos = [];
	var listFullNombres = "";
	var validado = "";
	var listExcel=[];
	listExcelInicial.forEach(function(val,index){
		if(val.length>0){
			listExcel.push(val);
		} 
	});
	
	if (texto.length < 100000) {
		for (var int = 0; int < listExcel.length; int++) {

			var listCells = listExcel[int].split('\t');
			var filaTexto = listExcel[int];
			if (filaTexto.trim().length < 1) {
				validado = "Existen filas vacias.";
				break;
			}

			if (validado.length < 1 && listCells.length != 17) {
				validado = "No coincide el numero de celdas."
						+ listCells.length;
				break;
			} else {
				var trab = {};
				var tipoDoc = {};
				var tipoSangre={};
				for (var j = 0; j < listCells.length; j++) {
					var element = listCells[j]; 
					if (j === 0) {
						for (var k = 0; k < listTipoDocumento.length; k++) {
							if (listTipoDocumento[k].nombre == element.trim()) {
								tipoDoc.id = listTipoDocumento[k].id;
								trab.tipoDocumento = tipoDoc;
							}
						}

						if (!tipoDoc.id) {
							validado = "Tipo Documento no reconocido.";
							break;
						}
					}
					if (j === 1) {
						trab.nroDocumento = element.trim();
						if ($.inArray(trab.nroDocumento, listDnisRepetidos) === -1) {
							listDnisRepetidos.push(trab.nroDocumento);
						} else {
							listFullNombres = listFullNombres + trab.nroDocumento + ", ";
						}
						if(verificarNumUnicoTrabajador(trab.nroDocumento).length>0){
							validado = verificarNumUnicoTrabajador(trab.nroDocumento);
							break;
						}
							
					} 
					if (j === 2) {
						trab.nombre = element.trim();
					}
					if (j === 3) {
						trab.apellido = element.trim();
					} 
					if (j === 4) {
						trab.area = element.trim();
					} 
					if (j ===5) {
						trab.cargo = element.trim();
					} 
					if (j ===6) {
						trab.codigoInterno = element.trim();
					} 
					if(j===7){
						try {
							var fechaTexto = element.trim();
							var parts = fechaTexto.split('/');
							var dateNac = new Date(parts[2], parts[1] - 1,
									parts[0]);
							trab.fechaNacimiento = dateNac;
						} catch (err) {
							validado = "Error en el formato de fecha de nacimiento.";
							break;
						}
					}
					if (j === 8) {
						trab.lugarNacio= element.trim();
					} 
					if (j === 9) {
						trab.lugarResidencia= element.trim();
					}
					if (j === 10) {
						trab.telefono= element.trim();
					}
					if (j === 11) {
						trab.correo= element.trim();
					}
					if (j === 12) {
						trab.nroSeguro= element.trim();
					}
					if (j === 13) {
						trab.nroCip= element.trim();
					}
					if (j === 14) {
						for (var k = 0; k < listTipoSangre.length; k++) {
							if (listTipoSangre[k].nombre == element.trim()) {
								tipoSangre.id = listTipoSangre[k].id;
								trab.tipoSangre = tipoSangre;
							}
						}

						if (!tipoSangre.id) {
							validado = "Tipo Sanguineo no reconocido.";
							break;
						}
					}
					if (j === 15) {
						trab.contactoNombre= element.trim();
					}
					if (j === 16) {
						trab.contactoTelefono= element.trim();
						var textoCampos="";
						if(trab.nroDocumento.trim().length==0){
							textoCampos+=" Nro de documento"
						}
						if(trab.nombre.trim().length==0){
							textoCampos+=" Nombre y apellido"
						} 
						if(trab.area.trim().length==0){
							textoCampos+=" Área"
						}
						if(trab.cargo.length==0){
							textoCampos+=" Cargo"
						} 
						if(trab.telefono.length==0){
							textoCampos+=" Teléfono"
						}
						if(trab.correo.trim().length==0){
							textoCampos+=" Correo"
						}
						if(trab.contactoNombre.trim().length==0){
							textoCampos+=" Contacto de emergencia"
						}
						if(trab.contactoTelefono.trim().length==0){
							textoCampos+=" Teléfono de contacto"
						}
						if(textoCampos.length>0){
							validado="Hay trabajadores que no tienen: "+textoCampos;
							break;
						}
					} 
					
					trab.id=0;
					trab.contratista={id:getSession("contratistaGosstId")}
				}

				listTrabs.push(trab);
				if (listDnisRepetidos.length < listTrabs.length) {
					validado = "Existen N° documentos repetidos." + listFullNombres;
					break;
				}
			}
		}
	} else {
		validado = "Ha excedido los 100000 caracteres permitidos.";
	}

	if (validado.length < 1) {
		var r = confirm("¿Está seguro que desea subir estos "
				+ listExcel.length + " elemento(s)?");
		if (r == true) {
			var dataParam = {
				listTrabs : listTrabs
			};
			console.log(listTrabs);
	 	callAjaxPost(URL + '/contratista/trabajador/masivo/save', dataParam,
			 	funcionResultadoMasivoContratista);
		}
	} else {
		alert(validado);
	}

	
	
}
function funcionResultadoMasivoContratista(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		if (data.MENSAJE.length > 0) {
			alert(data.MENSAJE);
		} else {
			alert("Se guardaron exitosamente.");
			habilitarTrabajadoresContratista(); 
		}
		break;
	default:
		alert("Ocurri&oacute; un error al guardar el trabajador!");
	}
}
function editarTrabajadorContratista(){
	$(".divListPrincipal>div").hide();
	var editarDiv=$("#editarMovilTrabProy");
	editarDiv.show();
	editarDiv.find(".tituloSubList")
	.html(textoBotonVolverContenido+"Editar trabajador "+objTrabajadorContratista.nombre);
	
	editarDiv.find("#selTipoDocTrab").val(objTrabajadorContratista.tipoDocumento.id);
	editarDiv.find("#inputNroDocTrabajadorContr").val(objTrabajadorContratista.nroDocumento);
	editarDiv.find("#inputNombreTrabajadorContr").val(objTrabajadorContratista.nombre);
	editarDiv.find("#inputApellidoTrabajadorContr").val(objTrabajadorContratista.apellido);
	editarDiv.find("#inputAreaTrabajadorContr").val(objTrabajadorContratista.area);
	editarDiv.find("#inputCargoTrabajadorContr").val(objTrabajadorContratista.cargo);
	editarDiv.find("#inputFechaTrabajadorContr").val(convertirFechaInput(objTrabajadorContratista.fechaNacimiento));
	editarDiv.find("#divEdadTrabajadorContr").html(objTrabajadorContratista.edad);
	editarDiv.find("#inputNacioTrabajadorContr").val(objTrabajadorContratista.lugarNacio);
	editarDiv.find("#inputResidenciaTrabajadorContr").val(objTrabajadorContratista.lugarResidencia);
	editarDiv.find("#inputTelefonoTrabajadorContr").val(objTrabajadorContratista.telefono);
	editarDiv.find("#inputCorreoTrabajadorContr").val(objTrabajadorContratista.correo);
	editarDiv.find("#inputNroSeguroTrabajadorContr").val(objTrabajadorContratista.nroSeguro);
	editarDiv.find("#inputNroCipTrabajadorContr").val(objTrabajadorContratista.nroCip); 
	editarDiv.find("#inputCodigoTrabajadorContr").val(objTrabajadorContratista.codigoInterno); 
	
	editarDiv.find("#selTipoSangreTrab").val(objTrabajadorContratista.tipoSangre.id);
	editarDiv.find("#inputContactoTrabajadorContr").val(objTrabajadorContratista.contactoNombre); 
	editarDiv.find("#inputFonoContactoTrabajadorContr").val(objTrabajadorContratista.contactoTelefono); 
	var options=
	{container:"#editarMovilTrabProy #divEviFoto",
			functionCall:function(){},
			descargaUrl: "/contratista/trabajador/foto/evidencia?id="+objTrabajadorContratista.id,
			esNuevo:false,
			idAux:"FotoTrabajador"+objTrabajadorContratista.id,
			evidenciaNombre:objTrabajadorContratista.evidenciaFotoNombre};
	crearFormEvidenciaCompleta(options);
}
function guardarTrabajadorContratista(){
	
	var formDiv=$("#editarMovilTrabProy");
	if(objTrabajadorContratista.id==0){
		formDiv=$("#nuevoMovilTrabProy");
	}
	var campoVacio = true;
	var tipoDoc=formDiv.find("#selTipoDocTrab").val();  
var nroDoc=formDiv.find("#inputNroDocTrabajadorContr").val();
var nombre=formDiv.find("#inputNombreTrabajadorContr").val();
var apellido=formDiv.find("#inputApellidoTrabajadorContr").val();
var area=formDiv.find("#inputAreaTrabajadorContr").val();
var cargo=formDiv.find("#inputCargoTrabajadorContr").val();
var fechaNacimiento=formDiv.find("#inputFechaTrabajadorContr").val();
var lugarNacio=formDiv.find("#inputNacioTrabajadorContr").val();
var lugarResidencia=formDiv.find("#inputResidenciaTrabajadorContr").val();
var telefono=formDiv.find("#inputTelefonoTrabajadorContr").val();
var correo=formDiv.find("#inputCorreoTrabajadorContr").val();
var nroSeguro=formDiv.find("#inputNroSeguroTrabajadorContr").val();
var nroCip=formDiv.find("#inputNroCipTrabajadorContr").val();
var codigoInterno=formDiv.find("#inputCodigoTrabajadorContr").val();
var tipoSangre=formDiv.find("#selTipoSangreTrab").val();
var contactoNombre=formDiv.find("#inputContactoTrabajadorContr").val();
var contactoTelefono=formDiv.find("#inputFonoContactoTrabajadorContr").val();
var textoCampos="";
var proyecto=($("#selTipoProyTrab").val()==-1?null:$("#selTipoProyTrab").val());

if(perfilContratista==null){
	if(nroDoc.trim().length==0){
		textoCampos+=" Nro de documento"
	}
	if(nombre.trim().length==0){
		textoCampos+=" Nombre "
	} 
	if(area.trim().length==0){
		textoCampos+=" Área"
	}
	if(cargo.trim().length==0){
		textoCampos+=" Cargo"
	}
	if(convertirFechaTexto(fechaNacimiento)== null){
		textoCampos+=" Fecha de nacimiento"
	}
	if(telefono.trim().length==0){
		textoCampos+=" Teléfono"
	}
	if(correo.trim().length==0){
		textoCampos+=" Correo"
	}
	if(contactoNombre.trim().length==0){
		textoCampos+=" Contacto de emergencia"
	}
	if(contactoTelefono.trim().length==0){
		textoCampos+=" Teléfono de contacto"
	}

}
	textoCampos+=verificarNumUnicoTrabajador(nroDoc);
		if (textoCampos.length==0) {

			var dataParam = {
				id : objTrabajadorContratista.id, proyectoActual:{id:proyecto}, 
				tipoDocumento:{id:tipoDoc},
				nroDocumento:nroDoc,
				nombre:nombre,apellido:apellido,
				area:area,
				cargo:cargo,
				fechaNacimiento:convertirFechaTexto(fechaNacimiento),
				lugarNacio:lugarNacio,
				lugarResidencia:lugarResidencia,
				telefono:telefono,
				correo:correo,
				nroSeguro:nroSeguro,
				nroCip:nroCip,
				codigoInterno:codigoInterno,
				tipoSangre:{id:tipoSangre},
				contactoNombre:contactoNombre,
				contactoTelefono:contactoTelefono,
				contratista:{id:getSession("contratistaGosstId")}
			};
			
			callAjaxPost(URL + '/contratista/trabajador/save', dataParam,
					function(data) {
						switch (data.CODE_RESPONSE) {
						case "05": 
							guardarEvidenciaAuto(data.nuevoId,"fileEviFotoTrabajador"+ objTrabajadorContratista.id
									,bitsEvidenciaPostulante,"/contratista/trabajador/foto/evidencia/save",
									function(){
								verAlertaSistemaGosst(1,"El trabajador fue registrado correctamente");
								habilitarTrabajadoresContratista();
							}
									); 
							
							break;
						default:
							console.log("Ocurrió un error al guardar la programacion!");
						}
					});
			 
		} else {
			alert("Debe ingresar los campos: "+textoCampos);
		} 
	
}
function eliminarTrabajadorContratista(pindex) {
	 
	var r = confirm("¿Está seguro de eliminar el trabajador?");
	if (r == true) {
		var dataParam = {
				id :  objTrabajadorContratista.id,
		};

		callAjaxPost(URL + '/contratista/trabajador/delete', dataParam,
				function(data) {
					switch (data.CODE_RESPONSE) {
					case "05":
						habilitarTrabajadoresContratista();
						 
						break;
					default:
						alert("Ocurrió un error al eliminar la programacion!");
					}
				});
	}
}


 
