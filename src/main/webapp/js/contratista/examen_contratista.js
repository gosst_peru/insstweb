/**
 * 
 */
var listFullExamenesContratista=[];
var listTipoVigencia=[];
var listTrabajadoresExamen=[];
var objExamenContratista={id:0};
var examenTipoId=1;
var examenTipoNombre="";
var listTrabajadoresExamenTodos=[];
function toggleMenuOpcionExam(obj,pindex){
	objExamenContratista=listFullExamenesContratista[pindex]; 
	$(obj).parent(".eventoGeneral").find("ul").toggle();
	$(obj).parent(".eventoGeneral").siblings().find("ul").hide(); 	
}
var funcionalidadesExamen=[ 
/*{id:"opcDescarga",nombre:"<i class='fa fa-download'></i>&nbspDescargar",functionClick:function(data){
	window.open(URL+"/contratista/examen/evidencia?id="+objExamenContratista.id,'_blank')
	}  },*/
   {id:"opcEditarRenove",nombre:"<i class='fa fa-refresh'></i>&nbspRenovar",functionClick:function(data){renovarExamenContratista()}  },
   {id:"opcEditar",nombre:"<i class='fa fa-pencil-square-o'></i>&nbspEditar",functionClick:function(data){editarExamenContratista()}  },
    {id:"opcElimnar",nombre:"<i class='fa fa-trash-o'></i>&nbspEliminar",functionClick:function(data){eliminarExamenContratista()}  }
                               ]
function marcarSubOpcionExamen(pindex){
	funcionalidadesExamen[pindex].functionClick();
	$(".subDetalleAccion ul").hide();
	$(".listaGestionGosst").hide();
}
function habilitarExamenesContratista(pexamenTipoId){
	objExamenContratista={id:0};
	var listPanelesPrincipal=[];
	examenTipoId=parseInt(pexamenTipoId);
	var textoBtn="<a class='btn btn-success'  target='_blank' " +
			"href='"+URL+"/empresa/documento/evidencia?tipoId=1&id="+getSession("gestopcompanyid")+"'>" +
	"<i class='fa fa-download fa-2x' aria-hidden='true'></i></a>";
	var textoBtn2="<a class='btn btn-success'  target='_blank' " +
			"href='"+URL+"/empresa/documento/evidencia?tipoId=2&id="+getSession("gestopcompanyid")+"'>" +
	"<i class='fa fa-download fa-2x' aria-hidden='true'></i></a>";
	if(examenTipoId==1){
		examenTipoNombre=listipoexam[0].nombre;
	}else if(examenTipoId==2){
		examenTipoNombre="Registro SCTR";
	}else if(examenTipoId==4){
		
		listPanelesPrincipal
		.push({id:"descargarCargo" ,clase:"",
			nombre:"",
			contenido:""},
			{id:"descargarReg" ,clase:"",
				nombre:textoBtn+"",
				contenido:""})
		examenTipoNombre="Cargo RISST";
		
	}else if(examenTipoId==5){
		examenTipoNombre="Documento Adicional";
	}else if(examenTipoId==6){
		examenTipoNombre="T-Registro"
	}else if(examenTipoId==8){
		examenTipoNombre="Equipos de protección personal"
	}
	var contratistaId=parseInt(getSession("contratistaGosstId"));
	var dataParam={ 
			id:(contratistaId),
			examen:{tipo:{id:examenTipoId}}
			};
	callAjaxPost(URL + '/contratista/examenes', dataParam, function(data) {
		switch (data.CODE_RESPONSE) {
		case "05":
			
			var textIn= "";
			listFullExamenesContratista=data.examenes;
			listTipoVigencia=data.tipoVigencia;
			listTrabajadoresExamen=data.trabsExamen;
			listTrabajadoresExamenTodos=data.trabsTodos;
			$(".divListPrincipal").html("");
			listFullExamenesContratista.forEach(function(val,index){
				var menuOpcion="<ul class='list-group listaGestionGosst' >";
				funcionalidadesExamen.forEach(function(val1,index1){
					menuOpcion+="<li class='list-group-item' onclick='marcarSubOpcionExamen("+index1+")'>"+val1.nombre+" </li>"
				});
				menuOpcion+="</ul>";
				var estiloGosst="gosst-neutral";
				if(val.isVigente==1){
					//estiloGosst="gosst-aprobado"
				}
				var eval4=val.evaluacion;
				if(val.evaluacion!=null){
					if(eval4.fecha==null){
						eval4.textoFecha=iconoGosst.desaprobado+"Sin evaluar";
						eval4.textoEval=iconoGosst.advertir+"Sin evaluar";
					}else{
						if(eval4.observacion!=null){
							if(eval4.observacion.length>0){
								eval4.observacion=" ("+eval4.observacion+")"
							}
						}
						if(eval4.nota!=null){
							if(eval4.nota.id==1){
								eval4.textoEval=iconoGosst.aprobado+"Evaluado el: <br>"+
									eval4.fechaTexto+" <strong> "+eval4.observacion+"</strong>";
								estiloGosst="gosst-aprobado";
							}else {
								eval4.textoEval=eval4.nota.icono	+"Evaluado el: <br>"+
								eval4.fechaTexto+" <strong> "+eval4.observacion+"</strong>"
							}
						}
					}
				}else{
					eval4={textoEval:iconoGosst.advertir+"Sin evaluar"};
				}
				if(val.evidenciaNombre==""){
					val.evidenciaNombre="No hay evidencia";
				}
				if(pexamenTipoId!=5){
					if(pexamenTipoId==6){
						textIn+=("<div class='eventoGeneral "+estiloGosst+"'>" +
									"<a class='efectoLink btn-gestion' onclick='toggleMenuOpcionExam(this,"+index+")'>" +
									"Ver Opciones&nbsp<i class='fa fa-angle-double-down' aria-hidden='true'></i></a>" +
									menuOpcion+
									eval4.textoEval.replace("<br>","")+"<br>"+
									"<i class='fa  fa-users' aria-hidden='true'></i>" +val.numTrabajadores+" Trabajador(es)<br>"+ 
									"<i class='fa fa-calendar' aria-hidden='true'></i>" +val.fechaTexto+" "+val.horaTexto+"<br>"+
									"<i class='fa fa-download' aria-hidden='true'></i><a class='efectoLink' target='_blank' "
									 +"href='"+URL+"/contratista/examen/evidencia?id="+val.id+"'>"+val.evidenciaNombre+"</a>"+  
								"</div>");
					}else if(pexamenTipoId==8){
						textIn+=("<div class='eventoGeneral "+estiloGosst+"'>" +
									"<a class='efectoLink btn-gestion' onclick='toggleMenuOpcionExam(this,"+index+")'>" +
									"Ver Opciones&nbsp<i class='fa fa-angle-double-down' aria-hidden='true'></i></a>" +
									menuOpcion+
									eval4.textoEval.replace("<br>","")+"<br>"+
									"<i class='fa  fa-users' aria-hidden='true'></i><strong>" +val.tema+"</strong> "+val.numTrabajadores+" Trabajador(es)<br>"+ 
									"<i class='fa fa-calendar' aria-hidden='true'></i>" +val.fechaTexto+" "+val.horaTexto+"<br>"+
									"<i class='fa fa-download' aria-hidden='true'></i> <a class='efectoLink' target='_blank' "
									 +"href='"+URL+"/contratista/examen/evidencia?id="+val.id+"'>"+val.evidenciaNombre+"</a>"+ 
								"</div>");
					}else{
						textIn+=("<div class='eventoGeneral "+estiloGosst+"'>" +
									"<a class='efectoLink btn-gestion' onclick='toggleMenuOpcionExam(this,"+index+")'>" +
									"Ver Opciones&nbsp<i class='fa fa-angle-double-down' aria-hidden='true'></i></a>" + //Guia para modificacion
									menuOpcion+
									eval4.textoEval.replace("<br>","")+"<br>"+
									"<i class='fa fa-clock-o' aria-hidden='true'></i> Vigencia: " +val.vigencia.nombre+"<br>"+ 
									"<i class='fa  fa-users' aria-hidden='true'></i>" +val.numTrabajadores+" Trabajador(es)<br>"+ 
									"<i class='fa fa-calendar' aria-hidden='true'></i>" +val.fechaTexto+" "+val.horaTexto+"<br>"+
									"<i class='fa fa-download' aria-hidden='true'></i> <a class='efectoLink' target='_blank' "
									 +"href='"+URL+"/contratista/examen/evidencia?id="+val.id+"'>"+val.evidenciaNombre+"</a>"+
								"</div>");
					}
				}else{
					textIn+=("<div class='eventoGeneral "+estiloGosst+"'>" +
							"<a class='efectoLink btn-gestion' onclick='toggleMenuOpcionExam(this,"+index+")'>" +
							"Ver Opciones&nbsp<i class='fa-angle-double-down' aria-hidden='true'></i></a>" +
							menuOpcion+
							"<i class='fa fa-info' aria-hidden='true'></i> <strong>" +val.tema+"</strong><br>"+   
							"<i class='fa fa-download' aria-hidden='true'></i> ("+val.fechaRealTexto+")  <a class='efectoLink' target='_blank' "
									 +"href='"+URL+"/contratista/examen/evidencia?id="+val.id+"'>"+val.evidenciaNombre+"</a>"+ 
							"</div>");
				}
				
			});
			 
			var selVigenciaExam= crearSelectOneMenuOblig("selVigenciaExam", "", listTipoVigencia, "", 
					"id","nombre")
			var listItemsExam=[
   {sugerencia:"",label:"",inputForm:"<button type='button' onclick='marcarTodosTrabajadores()' class='btn btn-success'  ><i class='fa fa-check-square-o'></i> Seleccionar todos </button>",divContainer:""},
                 {sugerencia:"",label:"Trabajadores",inputForm:"",divContainer:"divSelTrabExam"},
 			 	 {sugerencia:"",label:"Vigencia",inputForm:selVigenciaExam},
            	 {sugerencia:"",label:"Fecha de inicio de vigencia",inputForm:"<input class='form-control' type='date' id='inputFechaExamenContr' placeholder=' '>"}, 
            	 {sugerencia:"Ej. documentos, escaneos",label:"Evidencia",inputForm:"",divContainer:"eviExamContratista"},
       			 {sugerencia:"",label:"",inputForm:"<button type='submit' class='btn btn-success' ><i aria-hidden='true' class='fa fa-paper-plane'></i>Enviar</button>"} 
             		];
			if(pexamenTipoId==8){
				listItemsExam=[
				               {sugerencia:"",label:"",inputForm:"<button type='button' onclick='marcarTodosTrabajadores()' class='btn btn-success'  ><i class='fa fa-check-square-o'></i> Seleccionar todos </button>",divContainer:""},
				                 {sugerencia:"",label:"Trabajadores",inputForm:"",divContainer:"divSelTrabExam"},
				                 {sugerencia:"",label:"Descripción",inputForm:"<input class='form-control'  id='inputTemaExamenContr' placeholder=' '>"}, 
				            	 
				            	 {sugerencia:"",label:"Fecha de registro",inputForm:"<input class='form-control' type='date' id='inputFechaExamenContr' placeholder=' '>"}, 
				            	 {sugerencia:"Ej. documentos, escaneos",label:"Evidencia",inputForm:"",divContainer:"eviExamContratista"},
				       			 {sugerencia:"",label:"",inputForm:"<button type='submit' class='btn btn-success' ><i aria-hidden='true' class='fa fa-paper-plane'></i>Guardar</button>"} 
			             		];
			}
			if(pexamenTipoId==5){
				listItemsExam=[ 
	            	 {sugerencia:"",label:"Tema",inputForm:"<input class='form-control'  id='inputTemaExamenContr' placeholder=' '>"}, 
	            	 {sugerencia:"Ej. PDF, word, escaneos",label:"Evidencia",inputForm:"",divContainer:"eviExamContratista"},
	       			 {sugerencia:"",label:"",inputForm:"<button type='submit' class='btn btn-success' ><i aria-hidden='true' class='fa fa-paper-plane'></i>Enviar</button>"} 
	             		];
			}
			if(pexamenTipoId==6){
				listItemsExam=[
                  {sugerencia:"",label:"",inputForm:"<button type='button' onclick='marcarTodosTrabajadores()' class='btn btn-success'  ><i class='fa fa-check-square-o'></i> Seleccionar todos </button>",divContainer:""},
                {sugerencia:"",label:"Trabajadores",inputForm:"",divContainer:"divSelTrabExam"}, 
            	 {sugerencia:"",label:"Fecha de inicio de vigencia",inputForm:"<input class='form-control' type='date' id='inputFechaExamenContr' placeholder=' '>"}, 
            	 {sugerencia:"Ej. documentos, escaneos",label:"Evidencia",inputForm:"",divContainer:"eviExamContratista"},
       			 {sugerencia:"",label:"",inputForm:"<button type='submit' class='btn btn-success' ><i aria-hidden='true' class='fa fa-paper-plane'></i>Enviar</button>"} 
             		];
			}
			var textFormExamen="";
			listItemsExam.forEach(function(val,index){
				textFormExamen+=obtenerSubPanelModulo(val);
			});
			listPanelesPrincipal.push(
					{id:"nuevoMovilExamen" ,clase:"",
							nombre:""+"Nuevo registro de "+examenTipoNombre,
							contenido:"<form id='formNuevoExamen' class='eventoGeneral'>"+textFormExamen+"</form>"},
					{id:"editarMovilExamen" ,clase:"",
						nombre:""+"Editar "+examenTipoNombre,
						contenido:"<form id='formEditarExamen' class='eventoGeneral'>"+textFormExamen+"</form>"},
					{id:"examContratista",clase:"divExamenContratistaGeneral",
							nombre:examenTipoNombre+" registrados (" +listFullExamenesContratista.length+")",
							contenido:textIn}     );
			
			agregarPanelesDivPrincipal(listPanelesPrincipal);
			$("#descargarCargo").find(".tituloSubList").html(textoBtn+"Descargar formato de cargo del Cliente");
			$("#descargarReg").find(".tituloSubList").html(textoBtn2+"Descargar Reglamento Interno SST del cliente");
			var options=
			{container:"#formNuevoExamen #eviExamContratista",
					functionCall:function(){ },
					descargaUrl:"",
					esNuevo:true,
					idAux:"ExamenContratista"+objExamenContratista.id,
					evidenciaNombre:""};
			crearFormEvidenciaCompleta(options);
			if(pexamenTipoId==1){
				$("#formNuevoExamen #eviExamContratista").append(
						"<small>Certificado de aptitud Médica</small>");
			}
			var slcTrabajadoresExamen=crearSelectOneMenuObligMultipleCompleto("slcTrabajadoresExamen"+objExamenContratista.id, "",
					listTrabajadoresExamen,  "id", "nombreCompleto","#nuevoMovilExamen #divSelTrabExam","Trabajadores ...");
			var listGrupoTrabajadores=[{nombre:"Sin asignar",subtipos:[]},
			                           {nombre:"Asignados / Sin Aprobar",subtipos:[]},
			                           {nombre:"Aprobados",subtipos:[]}];
			listTrabajadoresExamen.forEach(function(val){
				 var trabAux={id:val.id,nombre:val.nombreCompleto};
				 if(val.indicadorExamenMedico==null){
					 val.ordenExamenMedico=0;
				 }else{
					 if(val.vigenteExamenMedico!=1){
						 val.ordenExamenMedico=1;
					 }else{
						 val.ordenExamenMedico=2;
					 }
				 }
				 if(val.indicadorSctr==null){
					 val.ordenSctr=0;
				 }else{
					 if(val.vigenteSctr!=1){
						 val.ordenSctr=1;
					 }else{
						 val.ordenSctr=2;
					 }
				 }
				 if(val.indicadorEpp==null){
					 val.ordenEpp=0;
				 }else{
					 if(val.vigenteEpp!=1){
						 val.ordenEpp=1;
					 }else{
						 val.ordenEpp=2;
					 }
				 }
				 if(val.indicadorCargo==null){
					 val.ordenCargo=0;
				 }else{
					 if(val.vigenteCargo!=1){
						 val.ordenCargo=1;
					 }else{
						 val.ordenCargo=2;
					 }
				 }
				 if(val.indicadorRegistro==null){
					 val.ordenRegistro=0;
				 }else{
					 if(val.vigenteRegistro!=1){
						 val.ordenRegistro=1;
					 }else{
						 val.ordenRegistro=2;
					 }
				 }
			});
			listTrabajadoresExamen.forEach(function(val){
				 var trabAux={id:val.id,nombre:val.nombreCompleto};
				 switch(examenTipoId){
					case 1:
						listGrupoTrabajadores[val.ordenExamenMedico].subtipos.push(trabAux);
						break;
					case 2:
						listGrupoTrabajadores[val.ordenSctr].subtipos.push(trabAux);
						break;
					case 8:
						listGrupoTrabajadores[val.ordenEpp].subtipos.push(trabAux);
						break;
					case 4:
						listGrupoTrabajadores[val.ordenCargo].subtipos.push(trabAux);
						break;
					case 6:
						listGrupoTrabajadores[val.ordenRegistro].subtipos.push(trabAux);
						break;
					}
			});
			
			var listAux=[1,2,8,4,6]
			if(listAux.indexOf(examenTipoId)!=-1){
				crearSelectMultipleGrupoCompleto("slcTrabajadoresExamen"+objExamenContratista.id, "", listGrupoTrabajadores,
						"#nuevoMovilExamen #divSelTrabExam","Trabajadores ...")
			}
			
			$(".listaGestionGosst").hide();
			$("#editarMovilExamen").hide();
			 $('#formNuevoExamen').on('submit', function(e) {  
			        e.preventDefault();
			        objExamenContratista={id:0}
			        guardarExamenContratista();
			 });
			 $('#formEditarExamen').on('submit', function(e) {  
			        e.preventDefault(); 
			        guardarExamenContratista();
			 });
			break;
			default:
				alert("nop");
				break;
		}
	})
	
}
function marcarTodosTrabajadores(){
	listTrabajadoresExamen.forEach(function(val){
		val.selected=1;
	})
	var slcTrabajadoresExamen=crearSelectOneMenuObligMultipleCompleto
	("slcTrabajadoresExamen"+objExamenContratista.id, "",
			listTrabajadoresExamen,  "id", "nombreCompleto","#nuevoMovilExamen #divSelTrabExam","Trabajadores ...");
}
function renovarExamenContratista(){
	$(".divListPrincipal>div").hide();
	var editarDiv=$("#nuevoMovilExamen");
	objExamenContratista.id=0;
	editarDiv.show();
	editarDiv.find(".tituloSubList")
	.html(textoBotonVolverContenido+"Renovar "+examenTipoNombre+" del "+objExamenContratista.fechaTexto);
	//
	editarDiv.find(".tituloSubList").find("button").attr("onclick","habilitarExamenesContratista("+examenTipoId+")")
	var trabs=objExamenContratista.trabajadoresId;
	var idstrabs=trabs.split(",");
	listTrabajadoresExamenTodos.forEach(function(val1,index1){
		val1.selected=0;
			idstrabs.forEach(function(val,index){
			if(val==val1.id){
				val1.selected=1;
			}
		})
	});
	editarDiv.find("#divSelTrabExam").html("");
	var slcTrabajadoresExamen=crearSelectOneMenuObligMultipleCompleto("slcTrabajadoresExamen"+objExamenContratista.id, "",
			listTrabajadoresExamen,  "id", "nombreCompleto","#nuevoMovilExamen #divSelTrabExam","Trabajadores ...");
	//
	editarDiv.find("#selVigenciaExam").val(objExamenContratista.vigencia.id); 
	editarDiv.find("#inputFechaExamenContr").val("");
	editarDiv.find("#inputTemaExamenContr").val(objExamenContratista.tema); 
	 var options=
		{container:"#editarMovilExamen #eviExamContratista",
				functionCall:function(){ },
				descargaUrl:"",
				esNuevo:true,
				idAux:"ExamenContratista"+objExamenContratista.id,
				evidenciaNombre:""};
		crearFormEvidenciaCompleta(options);
}
function editarExamenContratista(){
	$(".divListPrincipal>div").hide();
	var editarDiv=$("#editarMovilExamen");
	editarDiv.show();
	editarDiv.find(".tituloSubList")
	.html(textoBotonVolverContenido+"Editar "+examenTipoNombre+" del "+objExamenContratista.fechaTexto);
	//
	var trabs=objExamenContratista.trabajadoresId;
	var idstrabs=trabs.split(",");
	listTrabajadoresExamenTodos.forEach(function(val1,index1){
		val1.selected=0;
			idstrabs.forEach(function(val,index){
			if(val==val1.id){
				val1.selected=1;
			}
		})
	});
	editarDiv.find("#divSelTrabExam").html("");
	var slcTrabajadoresExamen=crearSelectOneMenuObligMultipleCompleto("slcTrabajadoresExamen"+objExamenContratista.id, "",
			listTrabajadoresExamenTodos,  "id", "nombreCompleto","#editarMovilExamen #divSelTrabExam","Trabajadores ...");
	//
	editarDiv.find("#selVigenciaExam").val(objExamenContratista.vigencia.id); 
	editarDiv.find("#inputFechaExamenContr").val(convertirFechaInput(objExamenContratista.fecha));
	editarDiv.find("#inputTemaExamenContr").val(objExamenContratista.tema); 
	 var eviNombre=objExamenContratista.evidenciaNombre;
		var options=
		{container:"#editarMovilExamen #eviExamContratista",
				functionCall:function(){ },
				descargaUrl:"/contratista/examen/evidencia?id="+objExamenContratista.id,
				esNuevo:false,
				idAux:"ExamenContratista"+objExamenContratista.id,
				evidenciaNombre:eviNombre};
		crearFormEvidenciaCompleta(options);
}

function guardarExamenContratista(){
	
	var formDiv=$("#editarMovilExamen");
	if(objExamenContratista.id==0){
		formDiv=$("#nuevoMovilExamen");
	}
	var campoVacio = true;
	var vigencia=formDiv.find("#selVigenciaExam").val();  
	var fecha=formDiv.find("#inputFechaExamenContr").val();
	var tema=formDiv.find("#inputTemaExamenContr").val();
	var trabsId=formDiv.find("#slcTrabajadoresExamen"+objExamenContratista.id).val();
	var trabajadoresFinal=[];
	if(examenTipoId!=5){
	trabsId.forEach(function(val,index){
		trabajadoresFinal.push({id:val});
	});
	}
	if(trabsId.length==0){
		alert("El campo trabajadores es obligatorio");
		return;
	}
	if(examenTipoId==6 || examenTipoId==8){
		vigencia=6;
	}
	if(convertirFechaTexto(fecha)==null){
		alert("Fecha obligatoria");return;
	}
	var existe=comprobarFieInputExiste("fileEviExamenContratista"+objExamenContratista.id);
	if(!existe.isValido && objExamenContratista.id==0){
		alert(existe.mensaje);return;
	}
		if (campoVacio) {

			var dataParam = {
				id : objExamenContratista.id, 
				vigencia:{id:vigencia}, 
				fecha:convertirFechaTexto(fecha),
				trabajadores:trabajadoresFinal,tema:tema,
				tipo:{id:examenTipoId},
				contratista:{id:getSession("contratistaGosstId")}
			};

			callAjaxPost(URL + '/contratista/examen/save', dataParam,
					function(data) {
						switch (data.CODE_RESPONSE) {
						case "05":  
							guardarEvidenciaAuto(data.nuevoId,"fileEviExamenContratista"+objExamenContratista.id
									,bitsEvidenciaExamen,"/contratista/examen/evidencia/save",
									function(){
								verAlertaSistemaGosst(2,"El registro fue enviado correctamente");
								habilitarExamenesContratista(examenTipoId);
								} )
						 
							break;
						default:
							console.log("Ocurrió un error al guardar   !"+examenTipoNombre);
						}
					},null,null,null,null,false);
			 
		} else {
			alert("Debe ingresar todos los campos.");
		}
	
}


function eliminarExamenContratista(pindex) {
	 
	var r = confirm("¿Está seguro de eliminar el "+examenTipoNombre+"?");
	if (r == true) {
		var dataParam = {
				id :  objExamenContratista.id,
		};

		callAjaxPost(URL + '/contratista/examen/delete', dataParam,
				function(data) {
					switch (data.CODE_RESPONSE) {
					case "05":
						habilitarExamenesContratista(examenTipoId);
						break;
					default:
						alert("Ocurrió un error al eliminar la examen!");
					}
				});
	}
}



