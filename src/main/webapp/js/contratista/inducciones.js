var induccionObj={};
var banderaEdicion4i=false;  
var listFullInduccion;
var listTipoInduccion;
var listTipoVigenciaInduccion;
var listFullEvaluacionInduccion=[];
$(function(){
	$("#btnNuevoInduccion").attr("onclick", "javascript:nuevoInduccion();");
	$("#btnCancelarInduccion").attr("onclick", "javascript:cancelarNuevoInduccion();");
	$("#btnGuardarInduccion").attr("onclick", "javascript:guardarInduccion();");
	$("#btnEliminarInduccion").attr("onclick", "javascript:eliminarInduccion();");
	$("#btnGenReporteInd").attr("onclick", "verModalReporteInduccion();");
	$("#btnRegularizarInducc").attr("onclick", "verRegulizarInduccion();");

	$("#btnGenReporteIndFinal").attr("onclick", "generarReporteInduccionContratista();");
	var hoy=obtenerFechaActual();
	$("#fechaInicioEventoInducc").val(sumaFechaDias(-7,hoy));
	$("#fechaFinEventoInducc").val(sumaFechaDias(365,hoy));
	
})
/**
 * 
 */
var listContratistasRegularizar=[];
function verRegulizarInduccion(){
	$("#tabInduccion .container-fluid").hide();
	$("#divRegularizarInduccion").show();
	$("#tblTrabInduccion tbody").html("");
	$("#tabInduccion h2").html("<a onclick='volverDivInduccion()'> " +
			"Inducciones </a> >  Regularización");
	$("#btnGuardarRegularizarInduccion").attr("onclick","guardarRegularizacionInduccion()");
	callAjaxPost(URL + '/contratista', 
			{empresaId:getSession("gestopcompanyid")}, function(data){
				$("#tblRegInduccion tbody tr").remove();
					
				listContratistasRegularizar=data.list
		 crearSelectOneMenuObligMultipleCompleto("selMultipleContratistas", "verAcordeContratistaTrabs()", data.list, 
					"id","nombre","#divSelectContratistasReg","Seleccione Contratistas");
		 var options=
			{container:"#eviRegularizacionInduccion",
					functionCall:function(){ },
					descargaUrl:"",
					esNuevo:true,
					idAux:"InduccionReg",
					evidenciaNombre:""};
			crearFormEvidenciaCompleta(options);
			resizeDivWrapper("wrapperInduccReg",420)
			});
	
}
function verAcordeContratistaTrabs(){
	$("#tblRegInduccion tbody tr").remove();
	var listSelect=$("#selMultipleContratistas").val();
	listContratistasRegularizar.forEach(function(val,index){
		if(listSelect.indexOf(""+val.id)!=-1){
			val.trabajadores.forEach(function(val1,index1){
				$("#tblRegInduccion tbody")
				.append("<tr>" +
						"<td>"+"<input id='checkRegInduc"+val1.id+"' type='checkbox' class='form-control' >"+"</td>" +
						"<td>"+val.nombre+"</td>" +
						"<td>"+val1.tipoDocumento.nombre+": "+val1.nroDocumento+"</td>" +
						"<td>"+val1.nombreCompleto+"</td>" +
						"" +
						"" +
						"</tr>")
			});
		}
	});
}
function guardarRegularizacionInduccion(){
	var listTrabs=[];
	listContratistasRegularizar.forEach(function(val,index){
		var listSelect=$("#selMultipleContratistas").val();
		if(listSelect.indexOf(""+val.id)!=-1){
			val.trabajadores.forEach(function(val1,index1){
				var ischeck=$("#checkRegInduc"+val1.id).prop("checked");
				if(ischeck){
					listTrabs.push({
						evaluacion:{id:1},
						asistencia:{id:1},
						trabajador:{id:val1.id}
					})
				}
			});
		}
	});
	var fechaSend=convertirFechaTexto($("#dateRegularizacionInduccion").val());
	if(fechaSend==null){
		alert("Fecha Obligatoria");
		return;
	}
	var induccionSend={
		id:0,
		tipo:{id:3},
		fechaReal:fechaSend,
		fecha:fechaSend,
		hora:"12:00:00",
		cupo:100,
		vigencia:{id:4},
		tema:"REGULARIZACIÓN "+listTrabs.length+" TRABAJADOR(ES)",
		profesor:"",
		horas:"1",
		evaluaciones:listTrabs,
		empresa:{empresaId:getSession("gestopcompanyid")}
		
	};
	callAjaxPost(URL + '/contratista/induccion/regularizacion/save', 
			induccionSend, function(data){
		guardarEvidenciaAuto(data.nuevoId,"fileEviInduccionReg"
				,bitsEvidenciaExamen,"/contratista/examen/evidencia/save",
				function(){
			alert("Se guardó la inducción");
			cargarInduccionsSeguridad();
		})
			})
	console.log(listTrabs);
}
		
function cargarInduccionsSeguridad() {
	$("#btnCancelarInduccion").hide();
	$("#btnNuevoInduccion").show();
	$("#btnEliminarInduccion").hide();
	$("#btnGuardarInduccion").hide();
	$("#btnGenReporteInd").hide();
	$("#btnRegularizarInducc").show();
	induccionObj={};
	volverDivInduccion();
	callAjaxPost(URL + '/contratista/inducciones', 
			 {empresaId:getSession("gestopcompanyid") ,
		fechaInicioPresentacion:$("#fechaInicioEventoInducc").val(),
		fechaFinPresentacion:$("#fechaFinEventoInducc").val() }, function(data){
				if(data.CODE_RESPONSE=="05"){
					listFullInduccion=data.examenes;
					listTipoInduccion=data.tipo;
					listTipoVigenciaInduccion=data.vigencia;
					banderaEdicion4i=false;
					$("#tblInduccions tbody tr").remove();
					listFullInduccion.forEach(function(val,index){  
						 
						$("#tblInduccions tbody:first").append(
								"<tr id='trind"+val.id+"' onclick='editarInduccion("+index+")'>" +
								"<td id='indtipo"+val.id+"'>"+val.tipo.nombre+"</td>" + 
								"<td id='indfecha"+val.id+"'>"+val.fechaTexto+"</td>" + 
								"<td id='indtime"+val.id+"'>"+val.horaTexto+"</td>" + 
								"<td id='indvig"+val.id+"'>"+val.vigencia.nombre+"</td>" + 
								"<td id='indtema"+val.id+"'>"+val.tema+"</td>" +
								"<td id='indprofe"+val.id+"'>"+val.profesor+"</td>" +
								"<td id='indhoras"+val.id+"'>"+val.horas+"</td>" +
								"<td id='indcupo"+val.id+"'>"+val.cupo+"</td>" +
								
								"<td id='indtrabs"+val.id+"'>"+val.numTrabajadores+"</td>" +
								
								
								"<td id='eviinduc"+val.id+"'>"+val.evidenciaNombre+"</td>" +
								"<td id='fecharealind"+val.id+"'>"+val.fechaRealTexto+"</td>" +
								"<td id='indfechafin"+val.id+"'>"+val.fechaFinVigenciaTexto+"</td>" + 
								
								"</tr>");
					});

					formatoCeldaSombreableTabla(true,"tblInduccions"); 

					goheadfixedY("#tblInduccions","#wrapInduccEmpresa");
					listFullInduccion.forEach(function(val,index){ 
					if(parseInt(getSession("linkCalendarioInduccionId"))==val.id){
							editarInduccion(index);
							sessionStorage.removeItem("linkCalendarioInduccionId");
							}
					})
				}else{
					console.log("NOPNPO")
				}
			});
	
}

function editarInduccion(pindex) {  
	if (!banderaEdicion4i) {
		formatoCeldaSombreableTabla(false,"tblInduccions"); 
		induccionObj= listFullInduccion[pindex]
		
		var tipo=induccionObj.tipo.id;
		var vigencia=induccionObj.vigencia.id;
		var fecha=convertirFechaInput(induccionObj.fecha);
		var tema=induccionObj.tema;
		var profesor=induccionObj.profesor;
		var horas=induccionObj.horas;
		var cupo=induccionObj.cupo;
		var fechaReal=convertirFechaInput(induccionObj.fechaReal);
		var fechaFinVigencia=convertirFechaInput(induccionObj.fechaFinVigencia);
		
		  //
		var selTipoInduccionContratista=crearSelectOneMenuOblig("selTipoInduccionContratista","",listTipoInduccion 
		 , tipo, "id","nombre");
		$("#indtipo" + induccionObj.id).html(selTipoInduccionContratista); 
		 // 
		$("#indfecha" + induccionObj.id)
		.html(	"<input type='date' id='inputFechaInduc' class='form-control'>");
			$("#inputFechaInduc").val(fecha);
			 // 
			$("#fecharealind" + induccionObj.id)
			.html(	"<input type='date' id='inputFechaRealInduc' class='form-control'>");
				$("#inputFechaRealInduc").val(fechaReal);
		 //
				$("#indfechafin" + induccionObj.id)
				.html(	"<input type='date' id='inputFechaVigenciaInduc' class='form-control'>");
					$("#inputFechaVigenciaInduc").val(fechaFinVigencia);
			 //	
		$("#indtime" + induccionObj.id)
		.html(	"<input type='time' id='inputTimeInduc' class='form-control'>");
			$("#inputTimeInduc").val(induccionObj.hora);
		 //	
			
		var selTipoVigenciaInduccion=crearSelectOneMenuOblig("selTipoVigenciaInduccion","",listTipoVigenciaInduccion 
				, vigencia, "id","nombre");
		$("#indvig" + induccionObj.id).html(selTipoVigenciaInduccion); 
		 //
		$("#indtema" + induccionObj.id)
		.html(	"<input type='text' id='inputTemaInduc' class='form-control'>");
			$("#inputTemaInduc").val(tema);
		//
		$("#indprofe" + induccionObj.id)
		.html(	"<input type='text' id='inputProfeInduc' class='form-control'>");
			$("#inputProfeInduc").val(profesor); 
		//
		$("#indhoras" + induccionObj.id)
		.html(	"<input type='number' id='inputHorasInduc' class='form-control'>");
			$("#inputHorasInduc").val(horas);
		//
		$("#indcupo" + induccionObj.id)
		.html(	"<input type='number' id='inputCupoInduc' class='form-control'>");
			$("#inputCupoInduc").val(cupo);
		
			
		//
		var textTrab=$("#indtrabs"+induccionObj.id).text();
		
		$("#indtrabs"+induccionObj.id).addClass("linkGosst")
		.on("click",function(){verTrabajadoresInduccion();})
		.html("<i class='fa fa-list fa-2x' aria-hidden='true' ></i>"+textTrab);
		
		
		//
		var eviNombre=induccionObj.evidenciaNombre;
		var options=
		{container:"#eviinduc"+induccionObj.id,
				functionCall:function(){ },
				descargaUrl:"/contratista/examen/evidencia?id="+induccionObj.id,
				esNuevo:false,
				idAux:"InduccionEmp",
				evidenciaNombre:eviNombre};
		crearFormEvidenciaCompleta(options);
		 
		
		
		banderaEdicion4i = true;
		$("#btnCancelarInduccion").show();
		$("#btnNuevoInduccion").hide();
		$("#btnEliminarInduccion").show();
		$("#btnGuardarInduccion").show();
		$("#btnGenReporteInd").show();
		$("#btnRegularizarInducc").hide();
		
		
		
	}
	
}
function verTrabajadoresInduccion(){
	$("#tabInduccion .container-fluid").hide();
	$("#divTrabInduccion").show();
	$("#tblTrabInduccion tbody").html("");
	$("#tabInduccion h2").html("<a onclick='volverDivInduccion()'> " +
			"Induccion "+induccionObj.tema+"</a> > Trabajadores");
	$("#btnGuardarEvalsInduccion").attr("onclick","guardarEvaluacionInduccionTrabajadorGeneral()");
	callAjaxPost(URL + '/contratista/induccion/trabajadores', 
			 {id:induccionObj.id  }, function(data){
			  listFullEvaluacionInduccion=data.trabajadores;
			var tipoNota=data.nota;
			var tipoAsistencia=data.asistencia;
			
			listFullEvaluacionInduccion.every(function(val2,index2){
				var selAsistInduccion=crearSelectOneMenuOblig("selAsistInduccion","guardarEvaluacionInduccionTrabajador("+val2.id+")",
						tipoAsistencia , val2.asistencia.id, 
						"id","nombre");
				var selNotaInduccion=crearSelectOneMenuOblig("selNotaInduccion","guardarEvaluacionInduccionTrabajador("+val2.id+")",tipoNota , val2.evaluacion.id, 
						"id","nombre");
				$("#tblTrabInduccion tbody").append(
						"<tr id='evalTrabInd"+val2.id+"'  >" +
						"<td id=' "+val2.id+"'>"+val2.trabajador.contratista.nombre+"</td>" +
						"<td id=' "+val2.id+"'>"+val2.trabajador.apellido+" , "+val2.trabajador.nombre+"</td>" + 
						"<td id=' "+val2.id+"'>"+selAsistInduccion+"</td>" +  
						"<td id=' "+val2.id+"'>"+selNotaInduccion+"</td>" +  
						"<td id=' "+val2.id+"'>"+"<input class='form-control' id='inputObsEvalTrab"+val2.id+"'>"+"</td>" +  
						 
						"</tr>");
			 $("#inputObsEvalTrab"+val2.id).val(val2.observacion);
				 
			 
					return true;
				 
			});
					 
		 })
	 	
}
function guardarEvaluacionInduccionTrabajadorGeneral(){
	var evalTotal=[]
	listFullEvaluacionInduccion.forEach(function(val,index){
		var evalId=val.id;
		var evaluacion=$("#evalTrabInd"+evalId).find("#selNotaInduccion").val();
		var asistencia=$("#evalTrabInd"+evalId).find("#selAsistInduccion").val();
		var observacion=$("#evalTrabInd"+evalId).find("#inputObsEvalTrab"+evalId).val();
		var evalObj={id:evalId ,
			observacion:observacion,
			asistencia:{id:asistencia},
			evaluacion:{id:evaluacion}}
		evalTotal.push(evalObj)
	});
	
	callAjaxPostNoLoad(URL + '/contratista/induccion/trabajador/evaluacion/general/save', 
			evalTotal, function(data){
				volverDivInduccion();
				alert("Evaluaciones guardadas");
			 });
	
}
function guardarEvaluacionInduccionTrabajador(evalId){
	var evaluacion=$("#evalTrabInd"+evalId).find("#selNotaInduccion").val();
	var asistencia=$("#evalTrabInd"+evalId).find("#selAsistInduccion").val();
	var observacion=$("#evalTrabInd"+evalId).find("#inputObsEvalTrab").val();
	
	callAjaxPostNoLoad(URL + '/contratista/induccion/trabajador/evaluacion/save', 
			 {id:evalId ,
		observacion:observacion,
		asistencia:{id:asistencia},
		evaluacion:{id:evaluacion}}, function(data){
				 
			 });
}
function verModalReporteInduccion(){
	$("#modalReporteInduccion").modal("show");
	callAjaxPost(URL + '/contratista/induccion/trabajadores', 
			 {id:induccionObj.id  }, function(data){
				var contratistasSelInducc=[];
				var contratistasListaId=[];
				var trabajadores=data.trabajadores;
				trabajadores.forEach(function(val,index){
					var contratistaIdval=val.trabajador.contratista.id; 
					if(contratistasListaId.indexOf(contratistaIdval)==-1){
						contratistasListaId.push(contratistaIdval);
						contratistasSelInducc.push(val.trabajador.contratista);
					}
				});
				
			var selContratsitaInduc=	crearSelectOneMenuOblig("selContratsitaInduc", "", contratistasSelInducc, "",
					"id","nombre");
			$("#divSelContratistaInduccion").html(selContratsitaInduc);
				
			 })
	
}
function generarReporteInduccionContratista(){
	window.open(URL
			+ "/contratista/induccion/informe?induccionId="+induccionObj.id 
			+"&contratistaId="
			+$("#selContratsitaInduc").val() , '_blank');	
}
function volverDivInduccion(){
	$("#tabInduccion .container-fluid").hide();
	$("#divContainInduccion").show();
	$("#tabInduccion h2").html("Inducciones de Seguridad Interno")
}
 
function nuevoInduccion() {
	if (!banderaEdicion4i) {
		induccionObj.id = 0; 
		var selTipoInduccionContratista=crearSelectOneMenuOblig("selTipoInduccionContratista","",listTipoInduccion 
				 , "", "id","nombre");
		var selTipoVigenciaInduccion=crearSelectOneMenuOblig("selTipoVigenciaInduccion","",listTipoVigenciaInduccion 
				, "", "id","nombre"); 
		$("#tblInduccions tbody:first")
				.prepend(
						"<tr id='0'>" 
						+ "<td>"+selTipoInduccionContratista+"</td>"
						+ "<td><input type='date' id='inputFechaInduc' class='form-control'></td>"
						+ "<td><input type='time' id='inputTimeInduc' class='form-control'></td>"
						+ "<td>"+selTipoVigenciaInduccion+"</td>"
						+ "<td><input type='text' id='inputTemaInduc' class='form-control'></td>" 
						+ "<td><input type='text' id='inputProfeInduc' class='form-control'></td>"
						+ "<td><input type='number' id='inputHorasInduc' class='form-control'></td>"
						+ "<td><input type='number' id='inputCupoInduc' class='form-control'></td>"
						
						
						+"<td>...</td>"		
						+"<td id='eviinduc0'> . </td>"		
						+ "<td><input type='date' id='inputFechaRealInduc' class='form-control'></td>"
						+ "<td><input type='date' id='inputFechaVigenciaInduc' class='form-control'></td>"
							
								
								
								+ "</tr>");
		var options=
		{container:"#eviinduc0",
				functionCall:function(){ },
				descargaUrl:"",
				esNuevo:true,
				idAux:"InduccionEmp",
				evidenciaNombre:""};
		crearFormEvidenciaCompleta(options);
		$("#inputTimeInduc").val("12:00:00");
		$("#btnCancelarInduccion").show();
		$("#btnNuevoInduccion").hide();
		$("#btnEliminarInduccion").hide();
		$("#btnGuardarInduccion").show();
		$("#btnGenReporteInd").hide();
		$("#btnRegularizarInducc").hide();
		banderaEdicion4i = true;
		formatoCeldaSombreableTabla(false,"tblInduccions"); 
	} else {
		alert("Guarde primero.");
	}
}

function cancelarInduccion() {
	cargarTiposContratista();
}

function eliminarInduccion() {
	var r = confirm("¿Está seguro de eliminar la inducción?");
	if (r == true) {
		var dataParam = {
				id : induccionObj.id,
		};

		callAjaxPost(URL + '/contratista/induccion/delete', dataParam,
				function(data) {
					switch (data.CODE_RESPONSE) {
					case "05":
						cargarInduccionsSeguridad();
						break;
					default:
						alert("Ocurrió un error al eliminar la inducción!");
					}
				});
	}
}

function guardarInduccion() {

	var campoVacio = true;
	var tipo=$("#selTipoInduccionContratista").val();   
	var vigencia=$("#selTipoVigenciaInduccion").val();   
	var fecha=$("#inputFechaInduc").val();
	var fechaReal=$("#inputFechaRealInduc").val();   
	var fechaFinVigencia=$("#inputFechaVigenciaInduc").val();   
	var tema=$("#inputTemaInduc").val();   
	var profesor=$("#inputProfeInduc").val();   
	var horas=$("#inputHorasInduc").val();   
	var cupo=$("#inputCupoInduc").val();   
var time=$("#inputTimeInduc").val();
		if (campoVacio) {

			var dataParam = {
				id : induccionObj.id, 
				tipo: {id:tipo},hora:time,
				fecha:convertirFechaTexto(fecha),
				fechaTexto:convertirFechaNormal(convertirFechaTexto(fecha)),
				fechaReal:convertirFechaTexto(fechaReal),
				fechaFinVigencia:convertirFechaTexto(fechaFinVigencia),
				vigencia: {id:vigencia},
				tema:tema,
				profesor:profesor,cupo:cupo,
				horas:horas,
				empresa:{empresaId:getSession("gestopcompanyid"),name:getSession("gestopcompanyname")}
			};

			callAjaxPost(URL + '/contratista/induccion/save', dataParam,
					function(data) {
					switch (data.CODE_RESPONSE) {
					case "05":
					guardarEvidenciaAuto(data.nuevoId,"fileEviInduccionEmp"
							,bitsEvidenciaExamen,"/contratista/examen/regularizacion/evidencia/save",
							cargarInduccionsSeguridad)
						 
						break;
					default:
							console.log("Ocurrió un error al guardar la inducción!");
						}
					},function(){ 
						$("#btnCancelarInduccion").hide(); 
						$("#btnEliminarInduccion").hide();
						$("#btnGuardarInduccion").hide();
						$("#btnGenReporteInd").hide(); 
						$.blockUI({message:'<img src="../imagenes/gif/ruedita.gif"></img>cargando...'})
						
					},{},null,null,false);
			
		
		
			
			
			
			
		} else {
			alert("Debe ingresar todos los campos.");
		}
	
	
}


function cancelarNuevoInduccion(){
	cargarInduccionsSeguridad();
}




