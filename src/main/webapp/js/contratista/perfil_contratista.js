var perfilContratistaId,perfilContratistaObj={};
var banderaEdicionPerfil=false;

var listSeccionContratistaPerfil;
var listCampoTrabajadorContratistaPerfil;
var listRequisitosProyectoContratista;
var listFullPerfilesContratista;
$(function(){
	$("#btnNuevoPerfilContratista").attr("onclick", "javascript:nuevoPerfilContratista();");
	$("#btnCancelarPerfilContratista").attr("onclick", "javascript:cancelarNuevoPerfilContratista();");
	$("#btnGuardarPerfilContratista").attr("onclick", "javascript:guardarPerfilContratista();");
	$("#btnEliminarPerfilContratista").attr("onclick", "javascript:eliminarPerfilContratista();");
	resizeDivWrapper("wrapPerfil",300);
})
/**
 * 
 */
function cargarPerfilContratistas() {
	$("#btnCancelarPerfilContratista").hide();
	$("#btnNuevoPerfilContratista").show();
	$("#btnEliminarPerfilContratista").hide();
	$("#btnGuardarPerfilContratista").hide();
	volverDivPerfilContratista();
	callAjaxPost(URL + '/contratista/perfiles', 
			 {empresaId:getSession("gestopcompanyid")}, function(data){
				if(data.CODE_RESPONSE=="05"){
					listFullPerfilesContratista=data.list;
					listSeccionContratistaPerfil=data.seccion;
					listSeccionContratistaPerfil=listSeccionContratistaPerfil.filter(function(val){
						if(val.isAsociable==1){
							return true;
						}else{return false;}
					});
					listCampoTrabajadorContratistaPerfil=data.camposTrabajador;
					listRequisitosProyectoContratista=data.requisitosProyecto
					banderaEdicionPerfil=false;
					$("#tblPerfilContratista tbody tr").remove();
					listFullPerfilesContratista.forEach(function(val,index){
						var contratistas=val.contratistas;
						var numActiv=0;
						contratistas.forEach(function(val2,index2){
							if(val2.isActivo==1){
								numActiv++;
							}
						});
						$("#tblPerfilContratista tbody").append(
								"<tr id='trperf"+val.id+"' onclick='editarPerfilContratista("+index+")'>" +
//							
								"<td id='pernom"+val.id+"'>"+val.nombre+"</td>" +
								"<td id='persecc"+val.id+"'>"+val.seccionesNombre+"</td>" +
								"<td id='percampo"+val.id+"'>"+val.camposTrabajadorNombre+"</td>" +
								"<td id='perreq"+val.id+"'>"+val.requisitosProyectoNombre+"</td>" +
									"<td id='percondet"+val.id+"'>"+contratistas.length+"</td>" +
							
								"</tr>");
					});
					goheadfixedY("#tblPerfilContratista","#wrapPerfil");
					formatoCeldaSombreableTabla(true,"tblPerfilContratista");
				}else{
					console.log("NOPNPO")
				}
			});
	
}

function editarPerfilContratista(pindex) {


	if (!banderaEdicionPerfil) {
		formatoCeldaSombreableTabla(false,"tblPerfilContratista");
		perfilContratistaId = listFullPerfilesContratista[pindex].id; 
		perfilContratistaObj= listFullPerfilesContratista[pindex]
		
		var descripcion=perfilContratistaObj.nombre;
		  //
		$("#pernom" + perfilContratistaId)
		.html(	"<input type='text' id='inputNombrePerfil' class='form-control'>");
		 $("#inputNombrePerfil").val(descripcion);
		 
		var seccionesId=perfilContratistaObj.seccionesId.split(",")
		listSeccionContratistaPerfil.forEach(function(val){
			val.selected=0;
			seccionesId.forEach(function(val1){
				if(parseInt(val1)==val.id){
					val.selected=1;
				}
			})
		});	
		crearSelectOneMenuObligMultipleCompleto("slcPermiteSeccionesPerfil", "",
				listSeccionContratistaPerfil,  "id", "nombre","#persecc"+perfilContratistaId,"No se permite...");
								
		//
		var camposTrabajadorId=perfilContratistaObj.camposTrabajadorId.split(",")
		listCampoTrabajadorContratistaPerfil.forEach(function(val){
			val.selected=0;
			camposTrabajadorId.forEach(function(val1){
				if(parseInt(val1)==val.id){
					val.selected=1;
				}
			})
		});	
		crearSelectOneMenuObligMultipleCompleto("slcPermiteCampoTrabajadorPerfil", "",
				listCampoTrabajadorContratistaPerfil,  "id", "nombre","#percampo"+perfilContratistaId,"No se permite...");
		
		//
		var requisitosProyectoId=perfilContratistaObj.requisitosProyectoId.split(",")
		listRequisitosProyectoContratista.forEach(function(val){
			val.selected=0;
			requisitosProyectoId.forEach(function(val1){
				if(parseInt(val1)==val.id){
					val.selected=1;
				}
			})
		});	
		crearSelectOneMenuObligMultipleCompleto("slcPermiteReqProyectoPerfil", "",
				listRequisitosProyectoContratista,  "id", "nombre","#perreq"+perfilContratistaId,"No se permite...");
		
		
		
		$("#percondet" + perfilContratistaId).html("<a onclick='verContratistsPerfil()'>"+
				$("#percondet" + perfilContratistaId).text()+"</a>");
		
	
		banderaEdicionPerfil = true;
		
		
		
		$("#btnCancelarPerfilContratista").show();
		$("#btnNuevoPerfilContratista").hide();
		$("#btnEliminarPerfilContratista").show();
		$("#btnGuardarPerfilContratista").show();
		
		
		
		// 
		
	}
	
}
function volverDivPerfilContratista(){
	$("#tabPerfil .container-fluid").hide();
	$("#divContainPerfil").show();
	$("#tabPerfil h2").html("Perfiles de Proyectos")
}
function verContratistsPerfil(){ 
	$("#tabPerfil .container-fluid").hide();
	$("#divProyPerfilContratista").show();
	$("#tblProyPerfilContratista tbody").html("");
	$("#tabPerfil h2").html("<a onclick='volverDivPerfilContratista()'> Perfil "+perfilContratistaObj.nombre+"</a> > Proyectos")
	listFullPerfilesContratista.forEach(function(val,index){ 
				 if(val.id==perfilContratistaId){ 
					var proyects=val.proyectos;
					proyects.every(function(val2,index2){
						
						$("#tblProyPerfilContratista tbody:first").prepend(
								"<tr id=' "+val2.id+"'  >" +
								"<td id=' "+val2.id+"'>"+val2.titulo+"</td>" +
								"<td id=' "+val2.id+"'>"+val2.postulante.contratista.nombre+"</td>" + 
								"<td id=' "+val2.id+"'>"+val2.fechaInicioTexto+"</td>" + 
								"<td id=' "+val2.id+"'>"+val2.fechaFinTexto+"</td>" + 
								
								"<td id=' "+val2.id+"'>"+(val2.isActivo==1?"Activo":val2.estado.nombre)+"</td>" + 
								 
								"</tr>");
					 
						 
					 
							return true;
						 
					});
					 
				 }
			 
		 });	
}
function nuevoPerfilContratista() {
	if (!banderaEdicionPerfil) {
		perfilContratistaId = 0; 
		$("#tblPerfilContratista tbody")
				.append(
						"<tr id='0'>"
					 
							
								+ "<td><input type='text' id='inputNombrePerfil' class='form-control'></td>"
								+ "<td id='persecc"+perfilContratistaId+"'> ...</td>"
								+ "<td id='percampo"+perfilContratistaId+"'> ...</td>"
								+ "<td id='perreq"+perfilContratistaId+"'> ...</td>"
								+ "<td>...</td>"
								
								+ "</tr>");
		crearSelectOneMenuObligMultipleCompleto("slcPermiteSeccionesPerfil", "",
				listSeccionContratistaPerfil,  "id", "nombre","#persecc"+perfilContratistaId,"No se permite...");
								
								//
		crearSelectOneMenuObligMultipleCompleto("slcPermiteCampoTrabajadorPerfil", "",
				listCampoTrabajadorContratistaPerfil,  "id", "nombre","#percampo"+perfilContratistaId,"No se permite...");
		//
		crearSelectOneMenuObligMultipleCompleto("slcPermiteReqProyectoPerfil", "",
				listRequisitosProyectoContratista,  "id", "nombre","#perreq"+perfilContratistaId,"No se permite...");
		
		
		
		
		formatoCeldaSombreableTabla(false,"tblPerfilContratista");
		$("#btnCancelarPerfilContratista").show();
		$("#btnNuevoPerfilContratista").hide();
		$("#btnEliminarPerfilContratista").hide();
		$("#btnGuardarPerfilContratista").show();
		banderaEdicionPerfil = true;
	} else {
		alert("Guarde primero.");
	}
}

function cancelarPerfilContratista() {
	cargarTiposContratista();
}

function eliminarPerfilContratista() {
	var r = confirm("¿Está seguro de eliminar el perfil?");
	if (r == true) {
		var dataParam = {
				id : perfilContratistaId,
		};

		callAjaxPost(URL + '/contratista/perfil/delete', dataParam,
				function(data) {
					switch (data.CODE_RESPONSE) {
					case "05":
						cargarPerfilContratistas();
						break;
					default:
						alert("Ocurrió un error al eliminar la programacion!");
					}
				});
	}
}

function guardarPerfilContratista() {

	var campoVacio = true;
var nombre=$("#inputNombrePerfil").val() ;
var secciones=$("#slcPermiteSeccionesPerfil").val();
var listSeccion=[];
secciones.forEach(function(val,index){
	listSeccion.push({id:val});
});
var camposTrabajador=$("#slcPermiteCampoTrabajadorPerfil").val();
var listCampos=[];
camposTrabajador.forEach(function(val,index){
	listCampos.push({id:val});
});

var requisitosProyecto=$("#slcPermiteReqProyectoPerfil").val();
var listRequisitos=[];
requisitosProyecto.forEach(function(val,index){
	listRequisitos.push({id:val});
});


		if (campoVacio) {

			var dataParam = {
				id : perfilContratistaId,
				requisitosProyecto:listRequisitos,
				camposTrabajador:listCampos,
				secciones:listSeccion,
				nombre: nombre,

				empresa:{empresaId:getSession("gestopcompanyid")}
			};

			callAjaxPost(URL + '/contratista/perfil/save', dataParam,
					function(data) {
						switch (data.CODE_RESPONSE) {
						case "05":
						
							cargarPerfilContratistas();
							break;
						default:
							console.log("Ocurrió un error al guardar el perfil!");
						}
					});
			
		} else {
			alert("Debe ingresar todos los campos.");
		}
	
	
}


function cancelarNuevoPerfilContratista(){
	cargarPerfilContratistas();
}



