/**
 * 
 */
var textoBotonToggleContenido="<button class='btn btn-success'  onclick='cambiarDivSubContenido(this)'>" +
	"<i class='fa fa-arrows-v fa-2x' aria-hidden='true'></i></button>";
var textoBotonVolverContenido="<button class='btn btn-success' style='float:left'  onclick='volverDivSubContenido(this)'>" +
"<i class='fa fa-arrow-left fa-2x' aria-hidden='true'></i></button>";

function cambiarDivSubContenido(obj,proyectoId){
	 
	if(proyectoId!=null){
		proyectoId=parseInt(proyectoId);
		listFullProyectoSeguridad.forEach(function(val,index){
			if(val.id==proyectoId){
				proyectoObj=val;
			}
		});
	}
	$(obj).parent(".tituloSubList").parent(".divProyectoGeneral").siblings().find(".contenidoSubList").hide();
	$(obj).parent(".tituloSubList").siblings( ".contenidoSubList" ).toggle();
	
	
}
function volverDivSubContenido(obj){
	
	$(".divListPrincipal>div").show();
	
	$("#editarMovilDocProy").hide();
	$("#editarMovilFormProy").hide();
	$("#editarMovilActivProy").hide();
	$("#editarMovilObsProy").hide();
	$("#editarMovilAuditoriaProy").hide();
	$("#editarMovilIncidentProy").hide();
	$("#editarMovilTrabProy").hide();
	$("#editarMovilExamen").hide();
	$("#editarMovilTrabProy").hide();
	$("#editarMovilInduccion").hide();
	$("#editarMovilHojaVidaTrab").hide();
	$(".contenidoFormVisible").hide();
	
	$("#subRealizarInspecciones").hide();
	
	$(".divCursoExamenMovil").hide();
	if(lastHeight>0){
		$(window).scrollTop(lastHeight-$("#barraMenu").height()-60);
		lastHeight=0;
	}
	}
var menuDerecha=[
                 {id:1,icono:"fa-calendar",ident:"icoPendientes",onclick:"marcarIconoModuloContratista(1)",titulo:"Actividades",adicional:""},
                 {id:1,icono:" fa-user-circle",ident:"icoPerfil",onclick:"",titulo:"Perfil",adicional:getSession("gestopusername").substr(0,12)+"."},
                 {id:1,icono:" fa-question-circle ",ident:"icoPreguntas",onclick:"marcarIconoModuloContratista(14)",titulo:"Consultas",adicional:""}];
var listipoexam=[
                 {id:1,nombre:"Certificado Aptitud Médica"}]
var menuFuncionesIzquierda=[

{id:1,moduloId:22,icono:"fa-calendar",ident:"icoResumen",titulo:"Resumen de actividades",adicional:""}, 
{id:2,moduloId:22,icono:"fa-book",ident:"icoPostulado",titulo:"Proyectos en fase de postulación",adicional:""},
{id:3,moduloId:22,icono:"fa-book",ident:"icoCurso",titulo:"Proyectos asignados en curso",adicional:""},
{id:4,moduloId:22,icono:"fa-book",ident:"icoCompletado",titulo:"Proyectos asignados anteriores /completados",adicional:""} ,
{id:5,moduloId:22,icono:"fa-users",ident:"icoTrabajador",titulo:"Trabajadores",adicional:""},
{id:6,moduloId:22,icono:"fa-medkit",ident:"icoExamen",titulo:listipoexam[0].nombre,adicional:""},
{id:7,moduloId:22,icono:"fa-folder-open-o",ident:"icoSctr",titulo:"SCTR",adicional:""},
{id:8,moduloId:22,icono:"fa-fire-extinguisher",ident:"icoEpp",titulo:"Equipos de Protección Personal",adicional:""},
{id:18,moduloId:22,icono:"fa-flask",ident:"icoHojQui",titulo:"Hoja MSDS",adicional:""},
{id:9,moduloId:22,icono:"fa-arrow-circle-o-down",ident:"icoInduc",titulo:"Inducciones Programadas del Cliente",adicional:""}, 
{id:10,moduloId:21,icono:"fa-graduation-cap",ident:"icoSctr123",titulo:"Capacitaciones Online",adicional:""},
{id:11,moduloId:22,icono:"fa-inbox",ident:"icoCargoRe",titulo:"Cargo del RISST del Cliente",adicional:""},
{id:12,moduloId:22,icono:"fa-files-o",ident:"icoTreg",titulo:"T-Registro",adicional:""},
{id:16,moduloId:22,icono:"fa-sort-numeric-asc",ident:"",titulo:"Indicadores",adicional:""},
{id:13,moduloId:22,icono:"fa-bullhorn",ident:"icoComun",titulo:"Comunicación con el Cliente",adicional:""},


{id:14,moduloId:22,icono:"fa-hand-pointer-o",ident:"icoObs",titulo:"Sugerencias",adicional:""},
{id:15,moduloId:22,icono:"fa-user",ident:"icoPerfil",titulo:"Perfil "+getSession("gestopusername").substr(0,20),adicional:""},
{id:18,moduloId:22,icono:"fa-flask",ident:"icoHojaMsd",titulo:"Hojas MSDS ",adicional:""},
{id:17,moduloId:22,icono:"fa-eye",ident:"icoObs",titulo:"Reporte de Observaciones ",adicional:""}
				];


 
function marcarIconoModuloContratista(menuId,variableAux){
	$(document).scrollTop(0);
	var menuObject=[];
	menuFuncionesIzquierda.forEach(function(val,index){
		if(val.id==parseInt(menuId)){
			menuObject=val;
		}
	});
	 $("#menuExplicativo").hide();
	 $(".gosst-aviso").remove();
	 $(".divContainerGeneral").show();
		$(".divListModulos").find("li").removeClass("activeMenu");
		$(".divListPrincipal").html("");
		$(".divTituloFull").find("h4").html(menuObject.titulo)
		
		$(".divListModulos #liMenuContratista"+menuObject.id).addClass("activeMenu");
		var isPermitido=false;
		listaModulosPermitidos.forEach(function(val){
			var moduloPermitido=val.isPermitido;
			
			if(val.menuOpcionId==menuObject.moduloId && moduloPermitido==1){
				isPermitido=true;
			}
			var numerSubModulos=val.subMenus;
			numerSubModulos.forEach(function(val1){
				var subModuloPermitido=val1.isPermitido;
				
				if(val1.menuOpcionId==menuObject.moduloId && subModuloPermitido==1){
					isPermitido=true;
				}
			});
		});
		if(isPermitido){
			switch(menuObject.id){
			case 1:
				habilitarVistaActividadesContratista(null,variableAux);
				break;
			case 2:
				habilitarProyectosContratista(4)
				break;
			case 3:
				habilitarProyectosContratista(2)
				break;
			case 4:
				habilitarProyectosContratista(3)
				break;
			case 5:
				habilitarTrabajadoresContratista()
				break;
			case 6:
				habilitarExamenesContratista(1)
				break;
			case 7:
				habilitarExamenesContratista(2)
				break;
			case 8:
				habilitarExamenesContratista(8)
				break;
			case 9:
				habilitarInduccionesEmpresaContratista()
				break;
			case 10:
				habilitarCapacitacionOnlineUsuario()
				break;
			case 11:
				habilitarExamenesContratista(4)
				break;
			case 12:
				habilitarExamenesContratista(6)
				break;
			case 13:
				habilitarComunicadoContratista()
				break;
			case 14:
				habilitarSugerenciasContratista()
				break;
			case 15:
				habilitarPerfilContratista()
				break;
			case 16:
				habilitarTrabajoHombresContratista()
				break;
			case 17:
				habilitarObservacionesFast()
				break;
			case 18: 
				habilitarHojaQuimicaContratista(); 
				break;
			}
		}else{
			verSeccionRestringida();
		}
		
		
		
}

var numProyectosTipo4=0, numProyectosTipo2=0, numProyectosTipo3=0;
var perfilContratista=[];
function verIndicadoresProyectos(){ 
	var dataParam={
			empresaId:getSession("gestopcompanyid"),
			contratistaId:getSession("contratistaGosstId")
			};
	callAjaxPost(URL + '/contratista/proyectos', dataParam, function(data) {
		perfilContratista=data.perfil;
		data.proyectos.forEach(function(val,index){
			var condicionPostulante=true;
			if(val.postulante.contratista==null){
				condicionPostulante=true;
			}else {
				if(val.postulante.contratista.id!=getSession("contratistaGosstId")){
					condicionPostulante=false;
				}
				if(val.postulante.contratista.id==null){
					condicionPostulante=true;
				}
			}  
			var condicionAgregar4=(
					val.postulante.asignacion!=1 && val.estado.id==1); 
			if(condicionAgregar4 && condicionPostulante){
				numProyectosTipo4++;
			} 
			var condicionAgregar2=( 
						(val.estado.id==2
								&& val.postulante.asignacion==1) ); 
			if(condicionAgregar2 && condicionPostulante){
				numProyectosTipo2++;
			} 
			var condicionAgregar3=( 
						(val.estado.id==3
								&& val.postulante.asignacion==1) ); 
			if(condicionAgregar3 && condicionPostulante){
				numProyectosTipo3++;
			}
		});
		$("#menuFigureti").html("<ul class='list-group'></ul>");
		menuFuncionesIzquierda.forEach(function(val){
		$(".divListModulos").find("ul").append("" +
				"<li class='list-group-item' id='liMenuContratista"+val.id+"' onclick='marcarIconoModuloContratista("+val.id+")'>" +
				"<i class='fa "+val.icono+" ' aria-hidden='true'>"+
				"<span  ></span></i> "+val.titulo+
				"</li>");
		$("#menuFigureti").find("ul").append("" +
				"<li class='list-group-item' style='color: #2e9e8f'  id='liMenuContratista"+val.id+"' onclick='marcarIconoModuloContratista("+val.id+")'>" +
				"<i class='fa "+val.icono+" ' style='width:16px;' aria-hidden='true'>"+
				"<span  ></span></i> "+val.titulo+
				"</li>");
		});
		$("#menuMovilGosst").on("click",function(){
			$(".divContainerGeneral").toggle();
		});
		if(perfilContratista==null){
			$(".divListModulos #liMenuContratista2").remove();
			$(".divListModulos #liMenuContratista4").remove();
			
			$("#menuFigureti").find("#liMenuContratista2").remove();
			$("#menuFigureti").find("#liMenuContratista4").remove();
			//$(".divListModulos #liMenuContratista12").remove();
		}else{
			$(".divListModulos ul").html("");
			$("#menuFigureti").html("<ul class='list-group'></ul>");
			perfilContratista.secciones.forEach(function(val){
			$(".divListModulos").find("ul").append("" +
					"<li class='list-group-item' id='liMenuContratista"+val.id+"' onclick='marcarIconoModuloContratista("+val.id+")'>" +
					"<i class='fa "+val.icono+" ' aria-hidden='true'>"+
					"<span  ></span></i> "+val.nombre+""+
					"</li>");
			$("#menuFigureti").find("ul").append("" +
					"<li class='list-group-item' style='color: #2e9e8f'  id='liMenuContratista"+val.id+"' onclick='marcarIconoModuloContratista("+val.id+")'>" +
					"<i class='fa "+val.icono+" ' style='width:16px;' aria-hidden='true'>"+
					"<span  ></span></i> "+val.nombre+
					"</li>");
			});
			
			
		}
		if(parseInt(getSession("isProyectoSimple"))==1 ){
			$(".divListModulos #liMenuContratista12").remove();
			$("#menuFigureti").find("#liMenuContratista12").remove();
		}
		$(".divListModulos #liMenuContratista2").prepend(" ("+numProyectosTipo4+") ")
		$(".divListModulos #liMenuContratista3").prepend(" ("+numProyectosTipo2+") ")
		$(".divListModulos #liMenuContratista4").prepend(" ("+numProyectosTipo3+") ");
		
		});
}
function verLateralesFijosContratista(){
 $("#wrapperList").remove(); $("#divListaTabs").remove();
	$("#menuPrincipal").html("" +
			"<input placeholder='Buscar ' type='text' class='buscadorGosst' style='width:280px'>" +
			"Tu Gestor Online de Seguridad y Salud en el Trabajo te da la Bienvenida")
	.css({color:"white"});
	$(".buscadorGosst").css({"margin-top": "8px",
	    "border-radius": "21px","color":"black",
	    "background-color": "#ffffff"});
	$(".navbar").css({"min-height": "48px","margin-bottom": "0px","top":"0px"});
	var logo=$("#logoMenu");
	logo.css({"background-color":"#2e9e8f","margin-top":"-18px"})
	.attr("onclick","volverMenuInicio()");
	$("#barraMenu").css({"position": "fixed",
    "width": "100%"});
	$("#barraMenu").find(".navbar-right").html("<ul></ul> ");
	menuDerecha.forEach(function(val,index){
		var claseSub="class='iconoLink dropdown-toggle' data-toggle='dropdown'";
		if(val.ident!="icoPerfil"){
			claseSub="class='iconoLink'";
			
		}else{
			val.onclick="";
		}
		$("#barraMenu").find(".navbar-right").append("" +
				"<li id='"+val.ident+"' title='"+val.titulo+"'><a onclick='"+val.onclick+"'"+ 
				claseSub+"	>"+
				"<i class='fa "+val.icono+" fa-2x' aria-hidden='true'>"+
				"</i> "+val.adicional+"</a></li>"+
				"")
	});
	$("#icoPerfil").append(
			"<ul class='dropdown-menu' style='color :white'>" +
			"<li onclick='marcarIconoModuloContratista(15)'> <a><i class='fa fa-user-o ' aria-hidden='true'></i>Perfil</a></li>" +
			"<li onclick='marcarIconoModuloContratista(15)'> <a> <i class='fa fa-exchange ' aria-hidden='true'></i>Cambiar Contraseña</a></li>" +
			"<li class='dropdown-divider' role='presentation'></li>" +
			"<li onclick='cerrarSesion()'> <a> <i class='fa fa-power-off ' aria-hidden='true'></i>Salir</a></li>" +
			"" +
			"</ul>" +
			"" +
			"" +
			"</div>");
	var listInfoAdicional=[
{id:"infoTrabs",nombre:"Trabajadores",faIcon:"users",infoAyuda:"info de ayuda 123"},
                   {id:"infoDoc",nombre:"Documentos",faIcon:"files-o",infoAyuda:"Documentos que necesita revisar el contratante antes de aprobar el proyecto al contratista"},
                   {id:"infoPlan",nombre:"Programa SSOMA",faIcon:"sort-amount-asc",infoAyuda:"info de ayuda 2"},
                   {id:"infoForm",nombre:"Capacitaciones Online",faIcon:"graduation-cap",infoAyuda:"info de ayuda 3"},
                   {id:"infoObs",nombre:"Observaciones",faIcon:"eye",infoAyuda:"info de ayuda 7"},
                   {id:"infoAudi",nombre:"Auditorías",faIcon:"check-square-o",infoAyuda:"info de ayuda 4"},
                   {id:"infoIncid1",nombre:"Incidentes",faIcon:"warning",infoAyuda:"info de ayuda 6"},
                   {id:"infoIncid2",nombre:"Matriz de riesgos",faIcon:"crosshair",infoAyuda:"info de ayuda 612"},
                   {id:"infoIncid3",nombre:"Cargo RISST",faIcon:"warning",infoAyuda:"info de ayuda 62"}];
	var textInfoAdicional="";
	listInfoAdicional.forEach(function(val,index){
		textInfoAdicional+="<div class='panel panel-default'>"+
	      "<div class='panel-heading' data-toggle='collapse' data-parent='#accordion' href='#"+val.id+"'>"+
	        "<h4 class='panel-title' >"+
	          "<i aria-hidden='true' class='fa fa-"+val.faIcon+"'></i>"+val.nombre+
	        "</h4>"+
	      "</div>"+
	      "<div id='"+val.id+"' class='panel-collapse collapse'>"+
	        "<div class='panel-body'>"+
	        val.infoAyuda+
	        "</div>"+
	      "</div>"+
	    "</div>"
	});
	$("body").find("#tabProyectos").remove();
	$("body").append("" +
			"<div class='divTituloFull'><div class='divTituloGeneral'><h4>Mis Actividades SST</h4></div></div>" +
			"<div class='divContainerGeneral'>" +
			"" +
			"<div class='divListModulos'><ul class='list-group'></ul></div>" +
			"<div class='divListPrincipal'></div>" +
			"<div class='divListSecundaria'>" +
			
			"<div id='divObjetivos' style='min-height:120px'>" +
			"<div class='subDivTitulo'><i aria-hidden='true' class='fa  fa-info-circle'></i>Información adicional </div>" +
			"<div class='subDivContenido'>" +
			"<div class='panel-group' id='accordionContratista'>" + 
			textInfoAdicional+
			"</div>" +
			"" +
			"</div>" +
			"</div>" +
			 
			 
		"" +
		"</div>"+
			"</div>"  );
	
	verIndicadoresProyectos();
	insertarCalendarioContratistas();

}
function alterarTextoBoton(obj,pId,auxId,nombreObjeto){
	var boolCambiarFinal=true;
	switch(pId){
	case 1:
	boolCambiarFinal=$(".detalleDocumentosProyecto").is(":visible");
	break;
	case 2:
		boolCambiarFinal=$(".detalleTrabajadoresProyecto").is(":visible");
		break;
	case 3:
		boolCambiarFinal=$(".detalleActividadesProyecto"+auxId).is(":visible");
		break;
	case 4:
		boolCambiarFinal=$(".detalleFormacionProyecto").is(":visible");
		break;
	case 5:
		boolCambiarFinal=$(".detalleObservacionesProyecto").is(":visible");
		break;
	case 6:
		boolCambiarFinal=$(".detalleAuditoriasProyecto").is(":visible");
		break;
	case 7:
		boolCambiarFinal=$(".detalleIncidentesProyecto").is(":visible");
		break;
	case 8:
		boolCambiarFinal=$(".detalleInduccionProyecto").is(":visible");
		break;
	case 9:
		boolCambiarFinal=$(".detalleIpercProyecto").is(":visible");
		break;
	case 10:
		boolCambiarFinal=$(".detalleObservacionesContratista").is(":visible");
		break;
	case 11:
		boolCambiarFinal=$(".detalleObservacionesColaborador").is(":visible");
		break;
	case 12:
		boolCambiarFinal=$(".detalleInspeccionProyecto").is(":visible");
		break;
		
		
		
	}  
	if(!boolCambiarFinal){ 
		$(obj).html("Ver "+nombreObjeto+" <i class='fa fa-angle-double-down aria-hidden='true' ></i>"); 
	}else{ 
		$(obj).html("Ver "+ nombreObjeto+" <i class='fa fa-angle-double-up' aria-hidden='true' ></i>"); 
	}
}
function habilitarPerfilContratista(){

	var textoForm="";
	var listItemsForm=[
	{sugerencia:"",label:"Anterior Contraseña",inputForm:"<input type='password' class='form-control' id='inputOldPass' placeholder=' ' autofocus>"},
	{sugerencia:" ",label:"Nueva Contraseña",inputForm:"<input type='password' class='form-control' id='inpuNewPass' placeholder=' '>"}, 
	{sugerencia:" ",label:"Repetir Contraseña",inputForm:"<input type='password' class='form-control' id='inpuNewNewPass' placeholder=' '>"}, 
	{sugerencia:" ",label:" ",
		inputForm:"<button type='submit' class='btn btn-success' ><i aria-hidden='true' class='fa fa-paper-plane'></i>Cambiar</button>"} 
	];
	listItemsForm.forEach(function(val,index){
		textoForm+=obtenerSubPanelModulo(val);
	});
	$(".divListPrincipal").html("")
	var listPerfilPrincipal=[
			{id:"subListPassword" ,clase:"contenidoFormVisible",
			nombre:"Cambiar Contraseña",
			contenido:"<form class='eventoGeneral' id='nuevoPassTrabajador'>"+textoForm+"</form>"},
			{id:"subListInfoExtra" ,clase:"contenidoFormVisible",
				nombre:"Información General",
				contenido:" "},
			{id:"subListCerrarSession" ,clase:"contenidoFormVisible",
				nombre:"Cerrar Sesión ",contenido:""}
 							];
	
	agregarPanelesDivPrincipal(listPerfilPrincipal);
	$("#subListCerrarSession").find("button")
	.css({"background-color":"rgb(188, 63, 72)"})
	.html("<i class='fa fa-times fa-2x' aria-hidden='true'></i>")
	.on("click",function(){
		cerrarSesion();
	});
	$('#nuevoPassTrabajador').on('submit', function(e) {
        e.preventDefault();
    	var psw1 = $("#inputOldPass").val();
    	var psw2 = $("#inpuNewPass").val();
    	var psw3 = $("#inpuNewNewPass").val();
    	var band = true;
    	
    	if(psw3.length == 0 || psw2.length == 0 || psw1.length == 0){
    		band = false;
    	}
    	
    	if(band){
    		var dataParam = {
    				contratistaId : getSession("contratistaGosstId"),
    				userName : sessionStorage.getItem("gestopusername"),
    				sessionId : sessionStorage.getItem("gestopsessionId"),
    				accesoUsuarios:getSession("accesoUsuarios"),
    				psw1 : psw1.trim(),
    				psw2 : psw2.trim(),
    				psw3 : psw3.trim()
    			};

    			callAjaxPost(URL + '/login/password/save', dataParam,
    					function(data){
    				$("#inputOldPass").val("");
    				$("#inpuNewPass").val("");
    				$("#inpuNewNewPass").val("");
    				$("#subListPassword").find(".contenidoSubList").hide();
    				switch (data.CODE_RESPONSE) {
    				case "02":
    					alert("El password actual es incorrecto.!");
    					break;
    				case "03":
    					alert("El password se ha bloqueado por demasiados intentos fallidos.!");
    					break;
    				case "05":
    					alert("El password se guardo correctamente.!");
    					break;
    				case "07":
    					alert("La confirmacion del nuevo password es incorrecta.!");
    					break;
    				case "11":
    					alert("La contraseña no es correcta. Ingrese nuevamente");
    					break;
					default:
    					
    					alert("La contraseña ya es usada por otro contratista, por favor ingresa una diferente");
    				}
    			});		
    	} else {
    		alert("Debe ingresar todos los campos.");
    	}
	});
	callAjaxPost(URL + '/login/licencia', {},
			function(data){
		var licencia = data.licencia;
		var infoArray=[
		               {sugerencia:"",label:"Usuario",inputForm:getSession("gestopusername")},
		               {sugerencia:"",label:"Fecha de vigencia de cuenta",inputForm:licencia.fechaVencimiento},
		               {sugerencia:"",label:"Empresa Cliente",inputForm:licencia.empresaNombre},
		               {sugerencia:"",divContainer:"divCorreoContratista",label:"Correo",inputForm:"<strong>"+licencia.correo+"</strong>" +
		               		"<button onclick='modificarDatosContratista()' class='btn btn-success'><i class='fa fa-pencil-square-o'></i>Cambiar</button>"},
		               {sugerencia:"",label:"Logo Cliente",inputForm:insertarImagenParaTablaMovil(licencia.logo,"","100%")},
		               {sugerencia:"",label:"Logo Contratista",inputForm:insertarImagenParaTablaMovil(licencia.logoContratista,"","100%")+"<div id='logoContratista'></div>"}];
		var informacionTexto="";
		infoArray.forEach(function(val,index){
			informacionTexto+=obtenerSubPanelModulo(val) ;
		}); 
		$("#subListInfoExtra").find(".contenidoSubList")
		.html("<div class='eventoGeneral'><table class='table table-user-information'>" +
				"<tbody>" +
				informacionTexto+
				"</tbody>" +
				"</table>" +
				"</div>");
		var options=
		{container:"#logoContratista",
				functionCall:function(){guardarEvidenciaAuto(getSession("contratistaGosstId"),"fileEviContratista"
							,bitsLogoEmpresa,"/contratista/logo/evidencia/save",
							habilitarPerfilContratista ) },
				descargaUrl:"/contratista/logo/evidencia?id="+getSession("contratistaGosstId"),
				esNuevo:false,isImagen : true,
				idAux:"Contratista",
				evidenciaNombre:"Modificar Logo"};
		crearFormEvidenciaCompleta(options);
	});
	
}
function modificarDatosContratista(){
	$("#divCorreoContratista").html("<input class='form-control' id='inputCorreoContratista'>" +
			"<button class='btn btn-success' onclick='guardarDatosContratista()'><i class='fa fa-save'></i>Guardar</button>" +
			"<button class='btn btn-danger' onclick='habilitarPerfilContratista()'>Cancelar</button>")
	$("#inputCorreoContratista").val("");
}
function guardarDatosContratista(){
	var contactoCorreo=$("#inputCorreoContratista").val()
	callAjaxPost(URL+"/contratista/datos/save",{id:getSession("contratistaGosstId"),contactoCorreo:contactoCorreo},
			function(data){
		habilitarPerfilContratista()
	}
	)
}
var indexFunction;
function toggleMenuOpcionProy(obj,pindex,menu)
{ 
	indexFunction=pindex;       
	$(obj).parent(".containerProy"+menu+pindex).find("ul").toggle();
	$(obj).parent(".containerProy"+menu+pindex).siblings().find("ul").hide();
	switch (menu) {
	case "Trabajadores": 
		$(".containerProyActSSOMA"+pindex+" .listaGestionGosst").hide();
		$(".containerProyPlanEmergencia"+pindex+" .listaGestionGosst").hide();
		$(".containerProyPlanResiduo"+pindex+" .listaGestionGosst").hide(); 
		$(".containerProyDocSSOMA"+pindex+" .listaGestionGosst").hide();
		$(".containerProyObsContratista"+pindex+" .listaGestionGosst").hide();
		$(".containerProyObsCliente"+pindex+" .listaGestionGosst").hide();
		$(".containerProyIncidente"+pindex+" .listaGestionGosst").hide();
		$(".containerProyMatriz"+pindex+" .listaGestionGosst").hide();	
		break;
	case "ActSSOMA":
		$(".containerProyTrabajadores"+pindex+" .listaGestionGosst").hide(); 
		$(".containerProyPlanEmergencia"+pindex+" .listaGestionGosst").hide();
		$(".containerProyPlanResiduo"+pindex+" .listaGestionGosst").hide(); 
		$(".containerProyDocSSOMA"+pindex+" .listaGestionGosst").hide();
		$(".containerProyObsContratista"+pindex+" .listaGestionGosst").hide();
		$(".containerProyObsCliente"+pindex+" .listaGestionGosst").hide();
		$(".containerProyIncidente"+pindex+" .listaGestionGosst").hide();
		$(".containerProyMatriz"+pindex+" .listaGestionGosst").hide();	
		break;
	case "PlanEmergencia":
		$(".containerProyTrabajadores"+pindex+" .listaGestionGosst").hide();
		$(".containerProyActSSOMA"+pindex+" .listaGestionGosst").hide(); 
		$(".containerProyPlanResiduo"+pindex+" .listaGestionGosst").hide(); 
		$(".containerProyDocSSOMA"+pindex+" .listaGestionGosst").hide();
		$(".containerProyObsContratista"+pindex+" .listaGestionGosst").hide();
		$(".containerProyObsCliente"+pindex+" .listaGestionGosst").hide();
		$(".containerProyIncidente"+pindex+" .listaGestionGosst").hide();
		$(".containerProyMatriz"+pindex+" .listaGestionGosst").hide();	
		break;
	case "PlanResiduo":
		$(".containerProyTrabajadores"+pindex+" .listaGestionGosst").hide();
		$(".containerProyActSSOMA"+pindex+" .listaGestionGosst").hide();
		$(".containerProyPlanEmergencia"+pindex+" .listaGestionGosst").hide(); 
		$(".containerProyDocSSOMA"+pindex+" .listaGestionGosst").hide();
		$(".containerProyObsContratista"+pindex+" .listaGestionGosst").hide();
		$(".containerProyObsCliente"+pindex+" .listaGestionGosst").hide();
		$(".containerProyIncidente"+pindex+" .listaGestionGosst").hide();
		$(".containerProyMatriz"+pindex+" .listaGestionGosst").hide();	
		break;
	case "DocSSOMA":
		$(".containerProyTrabajadores"+pindex+" .listaGestionGosst").hide();
		$(".containerProyActSSOMA"+pindex+" .listaGestionGosst").hide();
		$(".containerProyPlanEmergencia"+pindex+" .listaGestionGosst").hide();
		$(".containerProyPlanResiduo"+pindex+" .listaGestionGosst").hide();  
		$(".containerProyObsContratista"+pindex+" .listaGestionGosst").hide();
		$(".containerProyObsCliente"+pindex+" .listaGestionGosst").hide();
		$(".containerProyIncidente"+pindex+" .listaGestionGosst").hide();
		$(".containerProyMatriz"+pindex+" .listaGestionGosst").hide();	
		break;
	case "ObsContratista":
		$(".containerProyTrabajadores"+pindex+" .listaGestionGosst").hide();
		$(".containerProyActSSOMA"+pindex+" .listaGestionGosst").hide();
		$(".containerProyPlanEmergencia"+pindex+" .listaGestionGosst").hide();
		$(".containerProyPlanResiduo"+pindex+" .listaGestionGosst").hide(); 
		$(".containerProyDocSSOMA"+pindex+" .listaGestionGosst").hide(); 
		$(".containerProyObsCliente"+pindex+" .listaGestionGosst").hide();
		$(".containerProyIncidente"+pindex+" .listaGestionGosst").hide();
		$(".containerProyMatriz"+pindex+" .listaGestionGosst").hide();	
		break;
	case "ObsCliente":
		$(".containerProyTrabajadores"+pindex+" .listaGestionGosst").hide();
		$(".containerProyActSSOMA"+pindex+" .listaGestionGosst").hide();
		$(".containerProyPlanEmergencia"+pindex+" .listaGestionGosst").hide();
		$(".containerProyPlanResiduo"+pindex+" .listaGestionGosst").hide(); 
		$(".containerProyDocSSOMA"+pindex+" .listaGestionGosst").hide();
		$(".containerProyObsContratista"+pindex+" .listaGestionGosst").hide(); 
		$(".containerProyIncidente"+pindex+" .listaGestionGosst").hide();
		$(".containerProyMatriz"+pindex+" .listaGestionGosst").hide();	
		break;
	case "Incidente":
		$(".containerProyTrabajadores"+pindex+" .listaGestionGosst").hide();
		$(".containerProyActSSOMA"+pindex+" .listaGestionGosst").hide();
		$(".containerProyPlanEmergencia"+pindex+" .listaGestionGosst").hide();
		$(".containerProyPlanResiduo"+pindex+" .listaGestionGosst").hide(); 
		$(".containerProyDocSSOMA"+pindex+" .listaGestionGosst").hide();
		$(".containerProyObsContratista"+pindex+" .listaGestionGosst").hide();
		$(".containerProyObsCliente"+pindex+" .listaGestionGosst").hide(); 
		$(".containerProyMatriz"+pindex+" .listaGestionGosst").hide();	
		break;
	case "Matriz":
		$(".containerProyTrabajadores"+pindex+" .listaGestionGosst").hide();
		$(".containerProyActSSOMA"+pindex+" .listaGestionGosst").hide();
		$(".containerProyPlanEmergencia"+pindex+" .listaGestionGosst").hide();
		$(".containerProyPlanResiduo"+pindex+" .listaGestionGosst").hide(); 
		$(".containerProyDocSSOMA"+pindex+" .listaGestionGosst").hide();
		$(".containerProyObsContratista"+pindex+" .listaGestionGosst").hide();
		$(".containerProyObsCliente"+pindex+" .listaGestionGosst").hide();
		$(".containerProyIncidente"+pindex+" .listaGestionGosst").hide(); 
		break;
	
	default:
		break;
	}
} 
var funcionalidadOpcProy=
	[
		{menu:"Trabajadores",opcion:
			[{id:"opcEditar",nombre:"<i class='fa fa-pencil-square-o'></i> Modificar",functionClick:
				function(data){nuevoTrabProyecto(indexFunction);}
			},
			{id:"opcVerTbl",nombre:"<i class='fa fa-table'></i> Ver Tabla Resumen",functionClick:
					function(data){verTablaResumenTrabProyecto(indexFunction);}
			}
			]},
		{menu:"ActSSOMA",opcion:
			[{id:"opcAgregar",nombre:"<i class='fa fa-plus-square'></i> Agregar Actividad",functionClick:
				function(data){nuevoActProyecto(indexFunction,1);}
			}
		]},
		{menu:"PlanEmergencia",opcion:
			[{id:"opcAgregar",nombre:"<i class='fa fa-plus-square'></i> Agregar P. Emergencia",functionClick:
				function(data){nuevoActProyecto(indexFunction,2);}
			}
		]},
		{menu:"PlanResiduo",opcion:
			[{id:"opcAgregar",nombre:"<i class='fa fa-plus-square'></i> Agregar P. Residuo",functionClick:
				function(data){nuevoActProyecto(indexFunction,4);}
			}
		]},
		{menu:"DocSSOMA",opcion:
			[{id:"opcAgregar",nombre:"<i class='fa fa-plus-square'></i> Agregar Doc. SSOMA",functionClick:
				function(data){nuevoActProyecto(indexFunction,5);}
			}
		]},
		{menu:"ObsContratista",opcion:
			[{id:"opcAgregarReporte",nombre:"<i class='fa fa-plus-square'></i> Agregar rep. interno",functionClick:
				function(data){nuevoObservacionInternaContratista(indexFunction);}
			},
			{id:"opcVerTabla",nombre:"<i class='fa fa-table'></i> Ver Tabla Resumen",functionClick:
				function(data){verTablaResumenObsProyecto(indexFunction);}
			}
		]},
		{menu:"ObsCliente",opcion:
			[{id:"opcAgregar",nombre:"<i class='fa fa-plus-square'></i> Agregar Observacion",functionClick:
				function(data){nuevaObservacionContratista(indexFunction);}
			}
		]},
		{menu:"Incidente",opcion:
			[{id:"opcAgregar",nombre:"<i class='fa fa-plus-square'></i> Agregar Incidente",functionClick:
				function(data){nuevoIncidenteProyecto(indexFunction);}
			}
		]},
		{menu:"Matriz",opcion:
			[{id:"opcAgregar",nombre:"<i class='fa fa-plus-square'></i> Agregar",functionClick:
				function(data){nuevoIpercProyecto(indexFunction);}
			},
			{id:"opcVerTabla",nombre:"<i class='fa fa-table'></i> Ver Tabla",functionClick:
				function(data){verTablaIpercProyecto(indexFunction);}
			},
			{id:"opcImpDatos",nombre:"<i class='fa fa-upload'></i> Importar Datos",functionClick:
				function(data){importarDetalleIpercProyecto(indexFunction);}
			}
		]},
	];
function marcarSubOpcionProy(pindex,pindex1,menu)
{  
	$(".containerProy"+menu+indexFunction+" .listaGestionGosst").hide(); 
	funcionalidadOpcProy[pindex].opcion[pindex1].functionClick();
}
var menuOpcionProy=[];
function ListarOpcionesProy(idpindex)
{
	funcionalidadOpcProy.forEach(function(val1,index1){
		var menu=val1.menu;
		var opciones=val1.opcion;
		menuOpcionProy[index1]="<ul class='listaGestionGosst list-group ' style='display:none;'>";
		opciones.forEach(function(val2,index2){
			menuOpcionProy[index1]+="<li id='"+val2.id+menu+idpindex+"'style='width:185px; top:-18px;'class='list-group-item' onclick='marcarSubOpcionProy("+index1+","+index2+",\""+menu+"\")'>"+val2.nombre+"</li>";
		});
		menuOpcionProy[index1]+="</ul>";
	}); 
}
function habilitarProyectosContratista(tipoProyecto){

	var contratistaId=parseInt(getSession("contratistaGosstId"));
	var dataParam={
			empresaId:getSession("gestopcompanyid"),
			contratistaId:(contratistaId>0?contratistaId:null)
			};
	callAjaxPost(URL + '/contratista/proyectos', dataParam, function(data) {
		switch (data.CODE_RESPONSE) {
		case "05":
			var listPanelesPrincipal=[];
			perfilContratista=data.perfil;
			listFullProyectoSeguridad=data.proyectos;
			listTipoActividades=data.tipo_actividades;
			data.proyectos.forEach(function(val,index){
				ListarOpcionesProy(index);
				var condicionPostulante=true;
				if(val.postulante.contratista==null){
					condicionPostulante=true;
				}else {
					if(val.postulante.contratista.id!=contratistaId){
						condicionPostulante=false;
					}
					if(val.postulante.contratista.id==null){
						condicionPostulante=true;
					}
				}
				tipoProyecto=parseInt(tipoProyecto);
				var condicionAgregar=(tipoProyecto==4?
						(val.postulante.asignacion!=1 && val.estado.id==1):
							(val.estado.id==tipoProyecto
									&& val.postulante.asignacion==1) );
				
				
				if(condicionAgregar && condicionPostulante){
					var textoAsignacion=(val.postulante.asignacion==1?"(Asignado)  ":"(No asignado)  ");
					var textoBoton="",textoBotonAct1="",textoBotonAct2="",textoBotonAct3="",textoBotonAct4="",textoBotonAct5="",
					textoBotonAudi="",textoBotonTrab,
					textoBotonObs="",textoBotonObsContr="",textoBotonForm="",textoBotonInci="",textoBotonInduccion="",textoBotonIperc="",
					textoBotonInspecc="";
					 
					if(val.postulante.estado.id==1){
						textoBoton="Desde: "+val.postulante.fechaPlanificadaTexto;
					}
					if(val.postulante.estado.id==2){
						textoBoton="Hasta: "+val.postulante.fechaRealTexto+"<br>"+
						"<button class='btn btn-success' style='padding:5px' onclick='verCompletarProyecto("+index+");alterarTextoBoton(this,1) '>" +
							"<i class='fa fa-arrow-down' aria-hidden='true'></i>Ver</button>";
					}
					if(val.postulante.estado.id==3){
						textoBoton="Hasta: "+val.postulante.fechaRealTexto+"<br>"+
						"<button class='btn btn-success' style='padding:5px' onclick='verSinEditarCompletarProyecto("+index+");alterarTextoBoton(this,1) '>" +
						"<i class='fa fa-arrow-down' aria-hidden='true'></i>Ver</button>" ;
					}
					if(val.postulante.asignacion==1){
						textoBotonObs=
						"<div class='row' style='margin-top: -40px;'>" +
							"<section class='col col-xs-12 containerProyObsContratista"+index+"'>" +
								"<a class='efectoLink btn-gestion' onclick='toggleMenuOpcionProy(this,"+index+",\"ObsContratista\");'>" +
										"Ver Opciones <i class='fa fa-angle-double-down'></i>" +
								"</a>"+menuOpcionProy[5]+
							"</section>"+
						"</div>";
						textoBotonObsContr=
						"<div class='row' style='margin-top: -40px;'>" +
							"<section class='col col-xs-12 containerProyObsCliente"+index+"'>" +
								"<a class='efectoLink btn-gestion' onclick='toggleMenuOpcionProy(this,"+index+",\"ObsCliente\");'>" +
										"Ver Opciones <i class='fa fa-angle-double-down'></i>" +
								"</a>"+menuOpcionProy[6]+
							"</section>"+
						"</div>";
						textoBotonAct1="" +
						"<div class='row' style='margin-top: -40px;'>" +
							"<section class='col col-xs-12 containerProyActSSOMA"+index+"'>" +
								"<a class='efectoLink btn-gestion' onclick='toggleMenuOpcionProy(this,"+index+",\"ActSSOMA\");'>" +
										"Ver Opciones <i class='fa fa-angle-double-down'></i>" +
								"</a>"+menuOpcionProy[1]+
							"</section>"+
						"</div>";
						textoBotonAct2="" +
						"<div class='row' style='margin-top: -40px;'>" +
							"<section class='col col-xs-12 containerProyPlanEmergencia"+index+"'>" +
								"<a class='efectoLink btn-gestion' onclick='toggleMenuOpcionProy(this,"+index+",\"PlanEmergencia\");'>" +
										"Ver Opciones <i class='fa fa-angle-double-down'></i>" +
								"</a>"+menuOpcionProy[2]+
							"</section>"+
						"</div>" ;
						textoBotonAct3="" +
						"<button class='btn btn-success' style='padding:5px' onclick='cargarActProyectosSeguridad("+index+",3);alterarTextoBoton(this,3,3)'>" +
						"<i class='fa fa-arrow-down' aria-hidden='true'></i>Ver</button>" +
						"<br><br>" +
						"<button class='btn btn-success' style='padding:5px' onclick='nuevoActProyecto("+index+",3)'>" +
						"<i class='fa fa-plus' aria-hidden='true'></i>Agregar</button>" +
						"";
						textoBotonAct4="" +
						"<div class='row' style='margin-top: -40px;'>" +
							"<section class='col col-xs-12 containerProyPlanResiduo"+index+"'>" +
								"<a class='efectoLink btn-gestion' onclick='toggleMenuOpcionProy(this,"+index+",\"PlanResiduo\");'>" +
										"Ver Opciones <i class='fa fa-angle-double-down'></i>" +
								"</a>"+menuOpcionProy[3]+
							"</section>"+
						"</div>";
						
						textoBotonAct5="" +
						"<div class='row' style='margin-top: -40px;'>" +
							"<section class='col col-xs-12 containerProyDocSSOMA"+index+"'>" +
								"<a class='efectoLink btn-gestion' onclick='toggleMenuOpcionProy(this,"+index+",\"DocSSOMA\");'>" +
										"Ver Opciones <i class='fa fa-angle-double-down'></i>" +
								"</a>"+menuOpcionProy[4]+
							"</section>"+
						"</div>";
						
						textoBotonTrab="" +
						"<div class='row' style='margin-top: -40px;'>" +
							"<section class='containerProyTrabajadores"+index+" col col-xs-12 '>" +
								"<a class='efectoLink btn-gestion' onclick='toggleMenuOpcionProy(this,"+index+",\"Trabajadores\");'>" +
										"Ver Opciones <i class='fa fa-angle-double-down'></i>" +
								"</a>"+menuOpcionProy[0]+
							"</section>"+
						"</div>";
						textoBotonAudi=""
						textoBotonInspecc=""
						
						textoBotonForm=""
						textoBotonInci=
						"<div class='row' style='margin-top: -40px;'>" +
							"<section class='col col-xs-12 containerProyIncidente"+index+"'>" +
								"<a class='efectoLink btn-gestion' onclick='toggleMenuOpcionProy(this,"+index+",\"Incidente\");'>" +
										"Ver Opciones <i class='fa fa-angle-double-down'></i>" +
								"</a>"+menuOpcionProy[7]+
							"</section>"+
						"</div>";
						textoBotonInduccion="<button class='btn btn-success' style='padding:5px' onclick='verCompletarInduccionesProyecto("+index+");alterarTextoBoton(this,8)'>" +
						"<i class='fa fa-arrow-down' aria-hidden='true'></i>Ver</button>";
						textoBotonIperc="" +
						"<div class='row' style='margin-top: -40px;'>" +
							"<section class='col col-xs-12 containerProyMatriz"+index+"'>" +
								"<a class='efectoLink btn-gestion' onclick='toggleMenuOpcionProy(this,"+index+",\"Matriz\");'>" +
										"Ver Opciones <i class='fa fa-angle-double-down'></i>" +
								"</a>"+menuOpcionProy[8]+
							"</section>"+
						"</div>";
						
					}
					if(val.estado.id==2){
						textoBoton="Hasta: "+val.postulante.fechaRealTexto+"<br>"
					}
					if(val.estado.id==3){
						textoBoton="Hasta: "+val.postulante.fechaRealTexto+"<br>"
						textoBotonTrab="";textoBotonAct1="",textoBotonAct2="",textoBotonAct3="",textoBotonAct4="";
						textoBotonAudi="";textoBotonInspecc="";
						textoBotonObs="",textoBotonObsContr="";textoBotonForm="";textoBotonInci="",textoBotonInduccion="",textoBotonIperc="";
					}
					var obsAct1=(val.postulante.observacionesActividades1!=null?iconoGosst.advertir+"<strong style='color : #c0504d'>"+val.postulante.observacionesActividades1+"</strong>":"");
					var obsAct2=(val.postulante.observacionesActividades2!=null? iconoGosst.advertir+"<strong style='color : #c0504d'>"+val.postulante.observacionesActividades2+"</strong>":"");
					var obsAct3=(val.postulante.observacionesActividades3!=null?iconoGosst.advertir+"<strong style='color : #c0504d'>"+val.postulante.observacionesActividades3+"</strong>":"");
					var obsAct4=(val.postulante.observacionesActividades4!=null?iconoGosst.advertir+"<strong style='color : #c0504d'>"+val.postulante.observacionesActividades4+"</strong>":"");
					var obsAct5=(val.postulante.observacionesActividades5!=null?iconoGosst.advertir+"<strong style='color : #c0504d'>"+val.postulante.observacionesActividades5+"</strong>":"");
					
					var filaAct1="<tr title='"+listTipoActividades[0].nombre+"' id='tr1PlanProyecto"+val.id+"'>" +
					"<td > "+" <i aria-hidden='true' class='fa fa-sort-amount-asc'></i>  "+listTipoActividades[0].nombre+"   " +
					"<div class='barraIndicadorProyecto' " +
							"style='width:"+pasarDecimalPorcentaje(val.postulante.indicadorActividadesDecimal1)+" ;" +
							"background-color:"+colorPuntajeGosst(val.postulante.indicadorActividadesDecimal1)+"'>" +
					"</div>" +
					"<div class='row'>" + 
						"<a class='efectoLink'  onclick='cargarActProyectosSeguridad("+index+",1);alterarTextoBoton(this,3,1,\"Programa\") '>" +
							"Ver Programa <i class='fa fa-angle-double-down'></i>" +
						"</a>"	+
					"</div>" + 
					"</td>" + 
					"<td class='celdaIndicadorProyecto'> "+val.postulante.indicadorActividades1+obsAct1+
					"</td>" +
					"<td>"+textoBotonAct1+ 
					"</td>"+
					"</tr>" +
					"<tr class='detalleActividadesProyecto1'></tr>";
					var filaAct2="<tr title=' ' id='tr2PlanProyecto"+val.id+"'>" +
					"<td > "+" <i aria-hidden='true' class='fa fa-sort-amount-asc'></i> "+listTipoActividades[1].nombre+""+"" +
					"<div class='barraIndicadorProyecto' " +
						"style='width:"+pasarDecimalPorcentaje(val.postulante.indicadorActividadesDecimal2)+" ;" +
								"background-color:"+colorPuntajeGosst(val.postulante.indicadorActividadesDecimal2)+"'>" +
					"</div>" +
					"<div class='row'>" + 
						"<a class='efectoLink'  onclick='cargarActProyectosSeguridad("+index+",2);alterarTextoBoton(this,3,2,\"P. Emergencia\") '>" +
							"Ver P. Emergencia <i class='fa fa-angle-double-down'></i>" +
						"</a>"	+
					"</div>" + 
				"</td>" + 
				"<td class='celdaIndicadorProyecto'> "+val.postulante.indicadorActividades2+obsAct2+" </td>" +
				"<td>"+textoBotonAct2+
					
				"</td>"+
			"</tr>" +
			"<tr class='detalleActividadesProyecto2'></tr>";
					var filaAct4="<tr title='' id='tr4PlanProyecto"+val.id+"'>" +
					"<td > "+" <i aria-hidden='true' class='fa fa-sort-amount-asc'></i> "+listTipoActividades[2].nombre+""+"" +
					"<div class='barraIndicadorProyecto' " +
						"style='width:"+pasarDecimalPorcentaje(val.postulante.indicadorActividadesDecimal4)+" ;" +
								"background-color:"+colorPuntajeGosst(val.postulante.indicadorActividadesDecimal4)+"'>" +
					"</div>" +
					"<div class='row'>" + 
						"<a class='efectoLink'  onclick='cargarActProyectosSeguridad("+index+",4);alterarTextoBoton(this,3,4,\"P. Residuo\") '>" +
							"Ver P. Residuos <i class='fa fa-angle-double-down'></i>" +
						"</a>"	+
					"</div>" + 
			"</td>" + 
			"<td class='celdaIndicadorProyecto'> "+val.postulante.indicadorActividades4+obsAct4+" </td>" +
			"<td>"+textoBotonAct4+  
			"</td>"+
		"</tr>" +
		"<tr class='detalleActividadesProyecto4'></tr>";
					var filaAct5="<tr title='"+listTipoActividades[3].nombre+"' id='tr5PlanProyecto"+val.id+"'>" +
					"<td > "+" <i aria-hidden='true' class='fa fa-sort-amount-asc'></i>"+listTipoActividades[3].nombre+" asociados"+"" +
					"<div class='barraIndicadorProyecto' " +
						"style='width:"+pasarDecimalPorcentaje(val.postulante.indicadorActividadesDecimal5)+" ;" +
								"background-color:"+colorPuntajeGosst(val.postulante.indicadorActividadesDecimal5)+"'>" +
					"</div>" +
					"<div class='row'>" + 
						"<a class='efectoLink'  onclick='cargarActProyectosSeguridad("+index+",5);alterarTextoBoton(this,3,5,\"Doc. SSOMA\") '>" +
							"Ver Doc. "+listTipoActividades[3].nombre+" <i class='fa fa-angle-double-down'></i>" +
						"</a>"	+
					"</div>" + 
			"</td>" + 
			"<td class='celdaIndicadorProyecto'> "+val.postulante.indicadorActividades5+obsAct5+" </td>" +
			"<td>"+textoBotonAct5+ 
			"</td>"+
		"</tr>" +
		"<tr class='detalleActividadesProyecto5'></tr>";
					
					var filaExtForm="<tr title='Extensión Capacitaciones Online Asignadas'>" +
					"<td > "+" <i aria-hidden='true' class='fa fa-graduation-cap'></i>Extensión Capacitaciones Online Asignadas"+"" +
					"<div class='barraIndicadorProyecto' " +
						"style='width:"+pasarDecimalPorcentaje(val.postulante.indicadorFormacionesDecimal)+" ;" +
								"background-color:"+colorPuntajeGosst(val.postulante.indicadorFormacionesDecimal)+"'>" +
					"</div>" +
					"<div class='row'>" + 
						"<a class='efectoLink'  onclick='verCompletarCursoProyecto("+index+");alterarTextoBoton(this,4,null,\"Ext. Cap.\") '>" +
							"Ver Ext. Cap. <i class='fa fa-angle-double-down'></i>" +
						"</a>"	+
					"</div>" + 
			"</td>" + 
			"<td class='celdaIndicadorProyecto'> "+val.postulante.indicadorFormaciones+" </td>" +
			"<td>"+textoBotonForm+"</td>"+
		"</tr>"+
		"<tr class='detalleFormacionProyecto'></tr>";
					var filaObsCont="<tr title='Observaciones al Contratista'>" +
					"<td > "+" <i aria-hidden='true' class='fa fa-eye'></i> Observaciones al Contratista"+"" +
					"<div class='barraIndicadorProyecto' " +
						"style='width:"+pasarDecimalPorcentaje(val.postulante.indicadorObservacionesDecimal)+" ;" +
								"background-color:"+colorPuntajeGosst(val.postulante.indicadorObservacionesDecimal)+"'>" +
					"</div>" + 
					"<div class='row'>" + 
						"<a class='efectoLink'  onclick='verCompletarObsProyecto("+index+");alterarTextoBoton(this,5,null,\"Obs. Contratista\") '>" +
							"Ver Obs. Contratista <i class='fa fa-angle-double-down'></i>" +
						"</a>"	+
					"</div>" + 
			"</td>" + 
			"<td > "+val.postulante.indicadorObservaciones+" </td>" +
			"<td>"+textoBotonObs+
			"</td>"+
		"</tr>" +
		"<tr class='detalleObservacionesProyecto'></tr>";
					var filaObsCLiente="<tr id='trObsCon"+val.id+"' title='Observaciones al Cliente'>" +
					"<td > "+" <i aria-hidden='true' class='fa fa-eye'></i> Observaciones al Cliente"+"" +
					"<div class='barraIndicadorProyecto' " +
						"style='width:"+pasarDecimalPorcentaje(val.postulante.indicadorObservacionesContratistaDecimal)+" ;" +
								"background-color:"+colorPuntajeGosst(val.postulante.indicadorObservacionesContratistaDecimal)+"'>" +
					"</div>" +
					"<div class='row'>" + 
						"<a class='efectoLink'  onclick='verCompletarObsContratista("+index+");alterarTextoBoton(this,10,null,\"Obs. Cliente\") '>" +
							"Ver Obs. Cliente <i class='fa fa-angle-double-down'></i>" +
						"</a>"	+
					"</div>" + 
				"</td>" + 
				"<td class='celdaIndicadorProyecto'> "+val.postulante.indicadorObservacionesContratista+" </td>" +
				"<td>"+textoBotonObsContr+ 
				"</td>"+
			"</tr>" +
			"<tr class='detalleObservacionesContratista'></tr>";
					var filaIperc="<tr title='Matriz de Riesgos' id='trIperProyecto"+val.id+"'>" +
					"<td > "+" <i aria-hidden='true' class='fa fa-crosshairs'></i> Matriz de Riesgos<br>"+val.tipoIperc.nombre+""+"" +
					"<div class='barraIndicadorProyecto' " +
						"style='width:"+pasarDecimalPorcentaje(val.postulante.indicadorIpercDecimal)+" ;" +
								"background-color:"+colorPuntajeGosst(val.postulante.indicadorIpercDecimal)+"'>" +
					"</div>" +
					"<div class='row'>" + 
						"<a class='efectoLink'  onclick='cargarIpercProyecto("+index+");alterarTextoBoton(this,9,null,\"Matriz de Riesgos\") '>" +
							"Ver Matriz de Riesgos<i class='fa fa-angle-double-down'></i>" +
						"</a>"	+
					"</div>" + 
						"</td>" + 
						"<td class='celdaIndicadorProyecto'> "+val.postulante.indicadorIperc+" </td>" +
						"<td>"+textoBotonIperc+ 
						"</td>"+
					"</tr>" +
					"<tr class='detalleIpercProyecto'></tr>" ;
					var filaAuditoria="<tr title='Auditorías'>" +
					"<td > "+" <i aria-hidden='true' class='fa fa-check-square-o'></i> Auditorías"+"" +
					"<div class='barraIndicadorProyecto' " +
						"style='width:"+pasarDecimalPorcentaje(val.postulante.indicadorAuditoriaDecimal)+" ;" +
								"background-color:"+colorPuntajeGosst(val.postulante.indicadorAuditoriaDecimal)+"'>" +
					"</div>" +
					"<div class='row'>" + 
						"<a class='efectoLink'  onclick='verCompletarAuditoriaGenral("+index+");alterarTextoBoton(this,6,null,\"Auditoria\") '>" +
							"Ver Auditoria <i class='fa fa-angle-double-down'></i>" +
						"</a>"	+
					"</div>" +
			"</td>" + 
			"<td > "+val.postulante.indicadorAuditoria+" </td>" +
			"<td>"+textoBotonAudi+"</td>"+
		"</tr>" +
		"<tr class='detalleAuditoriasProyecto'></tr>";
					var filaInspecciones="<tr title='Inspecciones'>" +
					"<td > "+" <i aria-hidden='true' class='fa fa-check-square-o'></i> Inspecciones"+"" +
					"<div class='barraIndicadorProyecto' " +
						"style='width:"+pasarDecimalPorcentaje(val.postulante.indicadorHallazgosDecimal)+" ;" +
								"background-color:"+colorPuntajeGosst(val.postulante.indicadorHallazgosDecimal)+"'>" +
					"</div>" +
					"<div class='row'>" + 
						"<a class='efectoLink'  onclick='verCompletarInspeccionesGenral("+index+");alterarTextoBoton(this,12,null,\"Inspecciones\") '>" +
							"Ver Inspecciones <i class='fa fa-angle-double-down'></i>" +
						"</a>"	+
					"</div>" +
			"</td>" + 
			"<td > "+val.postulante.indicadorHallazgos+" </td>" +
			"<td>"+textoBotonInspecc+"</td>"+
		"</tr>" +
		"<tr class='detalleInspeccionProyecto'></tr>";
					var filaIncidentes="<tr title='Incidentes'>" +
					"<td > "+" <i aria-hidden='true' class='fa fa-warning'></i> Incidentes"+"" +
					"<div class='barraIndicadorProyecto' " +
						"style='width:"+pasarDecimalPorcentaje(val.postulante.indicadorIncidentesDecimal)+" ;" +
								"background-color:"+colorPuntajeGosst(val.postulante.indicadorIncidentesDecimal)+"'>" +
					"</div>" +
					"<div class='row'>" + 
						"<a class='efectoLink'  onclick='verCompletarIncidenteProyecto("+index+");alterarTextoBoton(this,7,null,\"Incidentes\") '>" +
							"Ver Incidentes <i class='fa fa-angle-double-down'></i>" +
						"</a>"	+
					"</div>" +
				"</td>" + 
				"<td class='celdaIndicadorProyecto'> "+val.postulante.indicadorIncidentes+" </td>" +
				"<td>"+textoBotonInci+ 
				"</td>"+
			"</tr>" + 
			"<tr class='detalleIncidentesProyecto'></tr>";
					var filaTrabajador="<tr title='Trabajadores Aprobados/Asignados' id='trTrabProyecto"+val.id+"'>" +
						"<td > "+
							"<i aria-hidden='true' class='fa fa-sort-amount-asc'></i> Trabajadores Asignados"+"" +
							"<div class='barraIndicadorProyecto' " +
								"style='width:"+pasarDecimalPorcentaje(val.postulante.indicadorTrabajadoresDecimal)+" ;" +
										"background-color:"+colorPuntajeGosst(val.postulante.indicadorTrabajadoresDecimal)+"'>" +
							"</div>"+
							"<div class='row'>" + 
								"<a class='efectoLink'  onclick='cargarTrabsProyectosSeguridad("+index+");alterarTextoBoton(this,2,null,\"Trabajadores\") '>" +
									"Ver Trabajadores<i class='fa fa-angle-double-down'></i>" +
								"</a>"	+
							"</div>" + 
						"</td>" + 
						"<td class='celdaIndicadorProyecto'> "+val.postulante.indicadorTrabajadores+" </td>" +
						
						"<td>"+textoBotonTrab+
							
						"</td>"+
					"</tr>" +
					"<tr class='detalleTrabajadoresProyecto'></tr>";
					var filasRelevantesProyecto="" +
					filaTrabajador+
					filaAct1+
					filaAct2+
					filaAct4+
					filaAct5+
					filaExtForm+
					filaObsCont+
					filaObsCLiente+
					filaAuditoria+
					filaInspecciones+
					filaIncidentes+
					filaIperc;
					var textDocPostulado="<tr title='Documentos postulación' id='trDocProyecto"+val.id+"'>" +
						"<td > "
								+" <i aria-hidden='true' class='fa fa-files-o'></i> " +
								"Documentos postulación" +
								"<div class='barraIndicadorProyecto' " +
										"style='width:"+pasarDecimalPorcentaje(val.postulante.indicadorDocumentosDecimal)+" ;" +
										"background-color:"+colorPuntajeGosst(val.postulante.indicadorDocumentosDecimal)+"'>" +
								"</div><br>"+
								"<div class='row'>" + 
									"<a class='efectoLink'  onclick='verSinEditarCompletarProyecto("+index+");alterarTextoBoton(this,1,0,\"Documento\") '>" +
										"Ver Documento<i class='fa fa-angle-double-down'></i>" +
									"</a>"	 + 
								"</div>"		
						+"</td>" + 
						"<td > "+val.postulante.indicadorDocumentos+" </td>" +
						"<td style='width: 135px;'>"+textoBoton+"</td>"+
					"</tr>" +
					"<tr class='detalleDocumentosProyecto'></tr>";
					
					var filasProyectoFinal="";
					if(perfilContratista==null){
						textDocPostulado="";
						filasProyectoFinal=filasRelevantesProyecto;
					}else{
						filasProyectoFinal+=filaTrabajador;
						
						
						perfilContratista.requisitosProyecto.forEach(function(val){
							switch(val.id){
							case 1:
								filasProyectoFinal+=textDocPostulado;
								break;
							case 2:
								filasProyectoFinal+=filaAct1;
								break;
							case 3:
								filasProyectoFinal+=filaAct2;
								break;
							case 4:
								filasProyectoFinal+="";
								break;
							case 5:
								filasProyectoFinal+=filaAct4;
								break;
							case 6:
								filasProyectoFinal+=filaAct5;
								break;
								
							case 7:
								filasProyectoFinal+=filaExtForm;
								break;
							case 8:
								filasProyectoFinal+=filaObsCont;
								break;
							case 9:
								filasProyectoFinal+=filaObsCLiente;
								break;
							case 10:
								filasProyectoFinal+=filaAuditoria;
								break;
							case 11:
								filasProyectoFinal+=filaInspecciones;
								break;
							case 12:
								filasProyectoFinal+=filaIncidentes;
								break;
							case 13:
								filasProyectoFinal+=filaIperc;
								break;
							}
						});
					}
					var descripcionProy="<div  class='eventoGeneral'>" +
					"<div id='tituloEvento'>" +
					"<table class='table table-movil' id='proyectoTable"+val.id+"'>" +
					"<tbody>" +
					"<tr>" +
						"<td > "+"<i aria-hidden='true' class='fa fa-hourglass-start'></i>Fecha Inicio"+"</td>" + 
						"<td colspan='2'>  "+val.fechaInicioTexto+ "</td>" +
						 
					"</tr>" +
					"<tr>" +
						"<td > "+"<i aria-hidden='true' class='fa fa-hourglass-end'></i>Fecha Fin"+"</td>" + 
						"<td colspan='2'>   "+val.fechaFinTexto+"</td>" + 
					"</tr>" +
					"<tr>" +
						"<td > "+" <i aria-hidden='true' class='fa fa-info'></i> Descripción"+"</td>" + 
						"<td colspan='2'> "+textoAsignacion+val.descripcion+" </td>" + 
					"</tr>" +
					filasProyectoFinal+
					"</tbody>" +
					"</table>"+ 
					"</div>"+
					"</div>"; 
				listPanelesPrincipal.push(
						{id:"pro"+val.id,clase:"divProyectoGeneral",proyectoId:val.id,
							nombre:""+val.titulo+" ("+pasarDecimalPorcentaje(val.notaFinal)+")",
							contenido:descripcionProy});
				}
			});
			$(".divTituloFull h4").append(" ("+listPanelesPrincipal.length+")")
			var textoForm="",textFormFormacion="",textFormActividad="";
			var textObservacion="",textAuditorias="",textIncidentes="",textTrabajdor="",textIperc=""
				,textControlIperc="",textFinalIperc="",textoObsContratista="";
			var listItemsFinalIperc=[
           		 {sugerencia:"",label:"Evaluación actual",inputForm:"",divContainer:"divSeparador"},
           		
           		 {sugerencia:"",label:"Actividad",inputForm:"",divContainer:"divActivIperc"},
           		 {sugerencia:"",label:"Nivel de Riesgo",divContainer:"divNivRiesgoIperc"},
           		{sugerencia:"",label:"Controles",divContainer:"divControlesIperc"},
           		
           		{sugerencia:"",label:"Evaluación residual",inputForm:"",divContainer:"divSeparador"},
           		
           		{sugerencia:"",label:"Da",divContainer:"divSelProbEsp1ResidualIperc"},
           		{sugerencia:"",label:"Niv",divContainer:"divSelProbEsp2ResidualIperc"},
           		{sugerencia:"",label:"Nive",divContainer:"divSelProbEsp3ResidualIperc"},
           		{sugerencia:"",label:"Factor n",divContainer:"divSelProbEsp4ResidualIperc"},
           		{sugerencia:"",label:"",divContainer:"divSelProbEsp5ResidualIperc"},
           		{sugerencia:"",label:"",divContainer:"divSelProbEsp6ResidualIperc"},
           		{sugerencia:"",label:"",divContainer:"divSelProbEsp7ResidualIperc"},
           		{sugerencia:"",label:"",divContainer:"divSelProbEsp8ResidualIperc"},
           		{sugerencia:"",label:"",divContainer:"divSelProbEsp9ResidualIperc"},
           		
           		{sugerencia:"",label:"Nivel de probabilidad",divContainer:"divSelProbEspFinalResidualIperc"},
           		{sugerencia:"",label:"Consecuencia",divContainer:"divSelConsecEspFinalResidualIperc"},
           		{sugerencia:"",label:"Nivel de Riesgo",divContainer:"divSelRiesgoEspFinalResidualIperc"},
       		  {sugerencia:"",label:" ",inputForm:"<button type='submit' class='btn btn-success' ><i aria-hidden='true' class='fa fa-paper-plane'></i>Enviar</button>"}
       							];
		var listItemsIperc=[
	{sugerencia:"",label:"Datos iniciales",inputForm:"",divContainer:"divDatos"},
       		{sugerencia:"",label:"División",inputForm:"<input type='text' class='form-control' id='inputDiviIperc'>"},
       		{sugerencia:"",label:"Área",inputForm:"<input type='text' class='form-control' id='inputAreaIperc'>"},
       		{sugerencia:"",label:"Puesto de trabajo / Perfil",inputForm:"<input type='text' class='form-control' id='inputPuestoIperc'>"},
       		{sugerencia:"",label:"Actividad",inputForm:"<input type='text' class='form-control' id='inputActividadIperc'>"},
       		{sugerencia:"",label:"Tarea",inputForm:"<input type='text' class='form-control' id='inputTareaIperc'>"},
       		{sugerencia:"",label:"Lugar",inputForm:"<input type='text' class='form-control' id='inputLugarIperc'>"},
       		{sugerencia:"",label:"Tipo",divContainer:"divSelTipoIperc"},
       		{sugerencia:"",label:"Sub-Tipo",divContainer:"divSelSubTipoIperc"},
       		{sugerencia:"",label:"Frecuencia",divContainer:"divSelFrecuenciaIperc"},
       		{sugerencia:"",label:"Peligro alla",divContainer:"divPeligroIperc",inputForm:"<input type='text' class='form-control' id='inputPelIperc'>"},
       		{sugerencia:"",label:"Descripción",inputForm:"<input type='text' class='form-control' id='inputPelDescIperc'>"},
       		{sugerencia:"",label:"Riesgo",inputForm:"<input type='text' class='form-control' id='inputRiesgoIperc'>"},
       		{sugerencia:"",label:"Consec cto",divContainer:"divConsecIperc",inputForm:"<input type='text' class='form-control' id='inputConsecuenciaIperc'>"},
       		{sugerencia:"",label:"Requisito Legal",inputForm:"<input type='text' class='form-control' id='inputReqLegalIperc'>"},
       		
       		{sugerencia:"",label:"",inputForm:"",divContainer:""}, 
       		{sugerencia:"",label:"Controles Existentes",inputForm:"",divContainer:"divExistentes"},
       		{sugerencia:"",label:"Control de ingeniería",inputForm:"<input type='text' class='form-control' id='inputControl1Iperc'>"},
       		{sugerencia:"",label:"Control de administrativo",inputForm:"<input type='text' class='form-control' id='inputControl2Iperc'>"},
       		{sugerencia:"Puede ser personal o colectivo",label:"Control de Equipo de Protección",inputForm:"<input type='text' class='form-control' id='inputControl3Iperc'>"},
       		{sugerencia:"",label:"",inputForm:"",divContainer:""},
       		{sugerencia:"",label:"Evaluación actual",inputForm:"",divContainer:"divEvalActual"},
       		{sugerencia:"",label:"",divContainer:"divSelProbEsp1Iperc"},
       		{sugerencia:"",label:"",divContainer:"divSelProbEsp2Iperc"},
       		{sugerencia:"",label:"",divContainer:"divSelProbEsp3Iperc"},
       		{sugerencia:"",label:"",divContainer:"divSelProbEsp4Iperc"},
       		{sugerencia:"",label:"",divContainer:"divSelProbEsp5Iperc"},
       		{sugerencia:"",label:"",divContainer:"divSelProbEsp6Iperc"},
       		{sugerencia:"",label:"",divContainer:"divSelProbEsp7Iperc"},
       		{sugerencia:"",label:"",divContainer:"divSelProbEsp8Iperc"},
       		{sugerencia:"",label:"",divContainer:"divSelProbEsp9Iperc"},
       		
       		{sugerencia:"Calculado a partir de las opciones anteriores",label:"Nivel de probabilidad",divContainer:"divSelProbEspFinalIperc"},
       		{sugerencia:"",label:"Consecuencia (Seguridad y Salud)",divContainer:"divSelConsecEspFinalIperc"},
       		{sugerencia:"",label:"Consecuencia (Reputación)",divContainer:"divSelConsecRepuEspFinalIperc"},
       		{sugerencia:"",label:"Nivel de Riesgo",divContainer:"divSelRiesgoEspFinalIperc"},
       		{sugerencia:"",label:"¿Es significativo?",divContainer:"divSelSignificativoIperc"},
       		
       		 {sugerencia:"",label:" ",inputForm:"<button type='submit' class='btn btn-success' ><i aria-hidden='true' class='fa fa-paper-plane'></i>Enviar</button>"}
   							];
		var listItemsTrab=[
{sugerencia:"",label:" ",inputForm:"<button type='button' class='btn btn-success' onclick='marcarTodosTrabajadoresProyecto()' ><i aria-hidden='true' class='fa fa-check'></i>Agregar Todos</button>"},
		{sugerencia:"",label:"Trabajadores",inputForm:"",divContainer:"divSelTrabProy"},
		{sugerencia:"",label:"Trabajadores Inactivos",inputForm:"",divContainer:"divSelTrabProyInhab"},
		{sugerencia:" ",label:"Fecha de modificación",inputForm:"<h5>"+obtenerFechaActualNormal()+"</h5>"},
		 {sugerencia:"",label:" ",inputForm:"<button type='submit' class='btn btn-success' ><i aria-hidden='true' class='fa fa-paper-plane'></i>Enviar</button>"}
							];
		var listItemsForm=[
		           		{sugerencia:"",label:"Item a evaluar",inputForm:"<h5 id='trMovilItem'></h5>"},
		           		{sugerencia:"Ej: Necesito más información, ¿Involucra todo el proyecto?",label:"Observación del Contratista",inputForm:"<input type='text' class='form-control' id='observacionEval' placeholder=' '>"},
		           		{sugerencia:"Ej: Documento legal, Foto",label:"Evidencia",inputForm:"",divContainer:"eviDocumento"},
		           		{sugerencia:"",label:"Observación del Contratante",inputForm:"<h5 id='trMovilObservacion'></h5>"},
		           		{sugerencia:"",label:" ",inputForm:"<button type='submit' class='btn btn-success' ><i aria-hidden='true' class='fa fa-paper-plane'></i>Enviar</button>"}
		           							];
		var listItemsFormFormacion=[
                    	{sugerencia:"Si ya ha realizado la actividad, solo introduzca la evidencia",label:"Fecha planificada",inputForm:"<input type='date' class='form-control' id='inputFecConf' placeholder=' '>"},
                    	{sugerencia:"",label:"Observación",inputForm:"<input class='form-control' id='inputObsConf' placeholder=' '>"},
						{sugerencia:"",label:"Evidencia",inputForm:"",divContainer:"eviDocumentoFormacion"},
		           		{sugerencia:"",label:" ",inputForm:"<button type='submit' class='btn btn-success' ><i aria-hidden='true' class='fa fa-paper-plane'></i>Enviar</button>"}];
		
		var listItemsFormActividad=[ 
            		{sugerencia:"",label:"Categoria",divContainer:"divCategActiv",inputForm:" "},
            		{sugerencia:"",label:"Actividad",divContainer:"divDescActiv",inputForm:"<input type='text' class='form-control' id='inputDescActProy' placeholder=' '>"},
            		{sugerencia:"Si ya ha realizado la actividad, solo introduzca la evidencia",divContainer:"divFechaPlanActiv",label:"Fecha planificada",inputForm:"<input type='date' class='form-control' id='inputFecPlanActProy' placeholder=' '>"},
            		{sugerencia:"Completar estos campos una vez se haya completado la actividad",divContainer:"divMensajeActividad",label:"",inputForm:""} ,
            		{sugerencia:"",divContainer:"divSeparatorActividad",label:"",inputForm:""} ,
            		{sugerencia:"",label:"Evidencia",inputForm:"",divContainer:"eviActividad"} ,
            		
            		
            		{sugerencia:"valor del 0 al 100",divContainer:"divPuntajeActividad",label:"% de cumplimiento",inputForm:"<input min='0' max='100' step='1' type='number' class='form-control' id='inputPuntajeActProy' >"},
           		  {sugerencia:"",divContainer:"expoActividad",label:"Expositor",inputForm:"<input type='text' class='form-control' id='inputExpoActProy' placeholder=' '>"},
            		 {sugerencia:"",divContainer:"numTrabActividad",label:"# Trabajadores",inputForm:"<input type='number' class='form-control' id='inputNumTrabActProy' placeholder=' '>"},
            		 {sugerencia:"",divContainer:"duracionActividad",label:"Duración (minutos)",inputForm:"<input type='number' class='form-control' id='inputDuraActProy' placeholder=' '>"},
             		 {sugerencia:"",label:"Observación del Contratista",divContainer:"divObsActProy",inputForm:"<input type='text' class='form-control' id='inputObsActProy' placeholder=' '>"},
            		 {sugerencia:"",label:" ",inputForm:"<button type='submit' class='btn btn-success' ><i aria-hidden='true' class='fa fa-paper-plane'></i>Enviar</button>"} 
            		];
		var listItemsFormObservacion=[
{sugerencia:"",label:"Acción de mejora",inputForm:"<input type='text' class='form-control' id='inputDescAcc' placeholder=' '>"},
{sugerencia:"Si ya ha realizado la acción, solo introduzca la evidencia",label:"Fecha planificada",inputForm:"<input type='date' class='form-control' id='inputFecAcc' placeholder=' '>"},
{sugerencia:"Ej: El incidente fue causado por un peatón, fue un evento fortuito ",label:"Observación del contratista",inputForm:"<input type='text' class='form-control' id='inputObsAcc' placeholder=' '>"},
{sugerencia:"",label:"Evidencia",inputForm:" ",divContainer:"eviAccionMejora"}, 
{sugerencia:"",label:" ",inputForm:"<button type='submit' class='btn btn-success' ><i aria-hidden='true' class='fa fa-paper-plane'></i>Guardar</button>"},
{sugerencia:"",label:" ",inputForm:"<button type='button' id='btnAgregarNuevoAccion' class='btn btn-success' ><i aria-hidden='true' class='fa fa-paper-plane'></i>Guardar y agregar otro</button>"}
  		 			];
		var listItemsControl=[
{sugerencia:"",label:"Tipo de control",inputForm:" ",divContainer:"divSelTipoControlIperc"},
{sugerencia:" ",label:"Descripción",inputForm:"<input type='text' class='form-control' id='inputDescControl' placeholder=' '>"},
{sugerencia:"Si ya ha realizado la acción, solo introduzca la evidencia",label:"Fecha planificada",inputForm:"<input type='date' class='form-control' id='inputFechaControl' placeholder=' '>"},
{sugerencia:"",label:"Evidencia",inputForm:" ",divContainer:"eviControl"},  
{sugerencia:"",label:" ",inputForm:"<button type='submit' class='btn btn-success' ><i aria-hidden='true' class='fa fa-paper-plane'></i>Enviar</button>"} 
  		 			];
		var listItemsFormAuditorias=[ ];
		var listItemsFormIncidentes=[ ]; 
		 
		listItemsFinalIperc.forEach(function(val,index){
			textFinalIperc+=obtenerSubPanelModulo(val)
		})
		listItemsControl.forEach(function(val,index){
			textControlIperc+=obtenerSubPanelModulo(val)
		})
		listItemsIperc.forEach(function(val,index){
			textIperc+=obtenerSubPanelModulo(val)
		})
		listItemsTrab.forEach(function(val,index){
			textTrabajdor+=obtenerSubPanelModulo(val)
		})
		listItemsForm.forEach(function(val,index){
			textoForm+=obtenerSubPanelModulo(val)
		});
		listItemsFormFormacion.forEach(function(val,index){
			textFormFormacion+=obtenerSubPanelModulo(val);
		});
		listItemsFormActividad.forEach(function(val,index){
			textFormActividad+=obtenerSubPanelModulo(val);
		});
		listItemsFormObservacion.forEach(function(val,index){
			textObservacion+=obtenerSubPanelModulo(val);
			textoObsContratista+=obtenerSubPanelModulo(val);
		});
		listItemsFormAuditorias.forEach(function(val,index){
			textAuditorias+=obtenerSubPanelModulo(val);
		});
		listItemsFormIncidentes.forEach(function(val,index){
			textIncidentes+=obtenerSubPanelModulo(val);
		});
		var listItemsImportIperc=[ 
               {sugerencia:"",label:"Plantilla",inputForm:"<a class='btn btn-success' onclick='generarExcelAyudaIperc()'><i class='fa fa-download'> </i>Descargar</a> ",divContainer:""},  
               {sugerencia:"",label:"Datos",inputForm:"<textarea class='form-control' " +
               		"style='resize:vertical' id='textImportIperc'></textarea>",divContainer:"divAreaImportIperc"},
               {sugerencia:"",label:" ",inputForm:"<button type='submit' class='btn btn-success' ><i aria-hidden='true' class='fa fa-paper-plane'></i>Enviar</button>"} 
  		 			];
		var textImportIperc="";
		listItemsImportIperc.forEach(function(val,index){
			textImportIperc+=obtenerSubPanelModulo(val);
		});

		var listItemsImportIpercCinco=[
                   {sugerencia:"",label:"Tipo",divContainer:"divSelSubTipoIpercImport"},
              {sugerencia:"",label:"Plantilla",inputForm:"<a class='btn btn-success' onclick='generarExcelAyudaIpercCinco()'><i class='fa fa-download'> </i>Descargar</a> ",divContainer:""},  
              {sugerencia:"No incluir # de fila",label:"Datos",inputForm:"<textarea class='form-control' " +
              		"style='resize:vertical' id='textImportIpercCinco'></textarea>",divContainer:"divAreaImportIperc"},
              {sugerencia:"",label:" ",inputForm:"<button type='submit' class='btn btn-success' ><i aria-hidden='true' class='fa fa-paper-plane'></i>Enviar</button>"} 
					             		 			];
   		var textImportIpercCinco="";
   		listItemsImportIpercCinco.forEach(function(val,index){
   			textImportIpercCinco+=obtenerSubPanelModulo(val);
   		});

			agregarPanelesDivPrincipal(listPanelesPrincipal);
			data.proyectos.forEach(function(val){
				var actividadesId=val.actividadesPermitidasId.split(",");
				actividadesId.forEach(function(val1){
					$("#tr"+val1+"PlanProyecto"+val.id).remove();
				});
	
			});
			var listEditarPrincipal=[
{id:"importIpercProyecto" ,clase:"contenidoFormVisible",
	nombre:""+"Editar tr s",
	contenido:"<form id='formImportIperc' class='eventoGeneral'>"+textImportIperc+"</form>"},
	{id:"importIpercProyectoCinco" ,clase:"contenidoFormVisible",
		nombre:""+"Editar tr s",
		contenido:"<form id='formImportIpercCinco' class='eventoGeneral'>"+textImportIpercCinco+"</form>"},

			{id:"editarMovilTrabProy" ,clase:"contenidoFormVisible",
			nombre:""+"Editar trabajdresdsssss",
			contenido:"<form id='formEditarTrabajadores' class='eventoGeneral'>"+textTrabajdor+"</form>"},
			{id:"editarMovilDocProy" ,clase:"contenidoFormVisible",
				nombre:""+"Editar documento",
				contenido:"<form id='formEditarDocumento' class='eventoGeneral'>"+textoForm+"</form>"},
			{id:"editarMovilFormProy" ,clase:"contenidoFormVisible",
				nombre:""+"Editar formación",
				contenido:"<form id='formEditarFormacion' class='eventoGeneral'>"+textFormFormacion+"</form>"},
			{id:"editarMovilActivProy" ,clase:"contenidoFormVisible",
				nombre:""+"Editar act",
				contenido:"<form id='formEditarActividad' class='eventoGeneral'>"+textFormActividad+"</form>"},
			{id:"editarMovilObsProy" ,clase:"contenidoFormVisible",
				nombre:""+"Editar observacion",
				contenido:"<form id='formEditarObservacion' class='eventoGeneral'>"+textObservacion+"</form>"},
			{id:"editarMovilObsContr" ,clase:"contenidoFormVisible",
				nombre:""+"Editar observacion",
				contenido:"<form id='formEditarObservacionContr' class='eventoGeneral'>"+textoObsContratista+"</form>"},
				
				
				
			{id:"editarMovilAuditoriaProy" ,clase:"contenidoFormVisible",
				nombre:""+"Editar formación",
				contenido:"<form id='formEditarAuditoria' class='eventoGeneral'>"+textAuditorias+"</form>"},
			{id:"editarMovilIncidentProy" ,clase:"contenidoFormVisible",
				nombre:""+"Editar act",
				contenido:"<form id='formEditarIncidente' class='eventoGeneral'>"+textIncidentes+"</form>"},
			{id:"editarMovilIpercProy" ,clase:"contenidoFormVisible",
				nombre:""+"Editar iperc",
				contenido:"<form id='formEditarIperc' class='eventoGeneral'>"+textIperc+"</form>"},
			{id:"editarMovilControlIperc" ,clase:"contenidoFormVisible",
				nombre:""+"Editar iperc",
				contenido:"<form id='formEditarControlIperc' class='eventoGeneral'>"+textControlIperc+"</form>"},
			{id:"editarMovilFinalIperc" ,clase:"contenidoFormVisible",
				nombre:""+" ",
				contenido:"<form id='formEditarFinalIperc' class='eventoGeneral'>"+textFinalIperc+"</form>"}
				 								
			 							];
			agregarPanelesDivPrincipal(listEditarPrincipal);
			//
			var inputPeligro=$("#inputPelIperc"); 
			ponerListaSugerida("inputPelIperc", [],true);
			ponerListaSugerida("inputRiesgoIperc", [],true);
			ponerListaSugerida("inputConsecuenciaIperc", [],true);
			inputPeligro.on( "autocompleteclose", buscarSugeridoPeligro);
			
			//
			 $('#formEditarDocumento').on('submit', function(e) {  
			        e.preventDefault();   
			        guardarPostulanteEvaluacionOnline();
			 });
			 $('#formEditarFormacion').on('submit', function(e) { 
			        e.preventDefault();   
			        guardarConfirmacion();
			 });       
			 $('#formEditarActividad').on('submit', function(e) { 
			        e.preventDefault();   
			        guardarActProyecto();
			 });
			 $('#formEditarObservacion').on('submit', function(e) { 
			        e.preventDefault();   
			        guardarAccionAuditoria();
			 });
			 $('#formEditarTrabajadores').on('submit', function(e) { 
			        e.preventDefault();   
			        guardarTrabsProyecto();
			 });
			 $('#formEditarIperc').on('submit', function(e) { 
			        e.preventDefault();   
			        guardarIpercProyecto();
			 });
			 $('#formEditarControlIperc').on('submit', function(e) { 
			        e.preventDefault();   
			        guardarControlIperc();
			 });
			 $('#formEditarFinalIperc').on('submit', function(e) { 
			        e.preventDefault();   
			        guardarFinalIperc();
			 });
			 $('#formEditarObservacionContr').on('submit', function(e) { 
			        e.preventDefault();   
			        guardarAccionObservacionContratista();
			 });
			 
			 $('#importIpercProyecto').on('submit', function(e) { 
			        e.preventDefault();   
			        guardarImportIpercProyecto();
			 });
			 $('#importIpercProyectoCinco').on('submit', function(e) { 
			        e.preventDefault();   
			        guardarImportIpercProyectoCinco();
			 });
			 $("#btnAgregarNuevoAccion").on("click",function(e){
				e.preventDefault();
				guardarAccionAuditoria(function(){
					alert("Acción Guardada!")
					nuevaAccionAuditoria(indexObs);
				});
			 });
			 $(".contenidoFormVisible").hide();
			completarBarraCarga();
			break;
		}
	})
	
}
function marcarTodosTrabajadoresProyecto(){
	listFullTrabProyecto.forEach(function(val1){
		val1.isActivo=1;
			val1.selected=1
	});
	insertarSelectTrabajadoresProyecto();
}
function verificarProyectoId(proyectoIndex){
	indexPostulante=proyectoIndex;
	proyectoObj=listFullProyectoSeguridad[indexPostulante];
}
function toggleMenuOpcionActividadesContratista(obj,pindex){
	//objSugerenciaContratista=listFullSugerenciasContratistas[pindex];
	//indexSugContratista=pindex;
	$(obj).parent("#tituloEvento").find("ul").toggle();
	$(obj).parent("#tituloEvento").parent().siblings().find("ul").hide(); 	
}
function marcarSubOpcionActividadesContratista(pindex){ 
	funcionalidadesActividadesContratista[pindex].functionClick();
	$(".listaGestionGosst").hide();
}  
function habilitarVistaActividadesContratista(functionCallBack,fechaFiltro,deleteCalendar,habilitarBotones){
	var contadorImplementar=0,textImplementar="",
	contadorCompletado=0,textCompletado="",
	contadorRetrasado=0,textRetrasado="";
	var textFechaAppend="";
	textFechaAppend="";
	$(".divTituloGeneral h4")
	.html("Resumen de actividades");
	if(fechaFiltro==null){
		
	}else{
		var fechaTitulo=new Date(fechaFiltro);
		$(".divTituloGeneral h4")
		.append("<br> ("+fechaTitulo.getDate()+"-"+listMeses[(fechaTitulo.getMonth()+1)]+"-"+fechaTitulo.getFullYear()+")")
	}
	$(".divListPrincipal").html("")
var listFullEventosContratista=[]; 
	var dataParam={
			empresaId:getSession("gestopcompanyid"),
			contratistaId:getSession("contratistaGosstId")
			};
	callAjaxPost(URL + '/contratista/proyectos/resumen/contratista', dataParam, function(data) {
		switch (data.CODE_RESPONSE) {
		case "05":
			listFullProyectoSeguridad=data.proyectos;
			listCategoriaActividad=data.categorias_actividades;
			var indexAuxDoc=0,indexAuxTrab=0,indexAuxActiv=0,
			indexAuxForm=0,indexAuxObs=0,indexAuxAcc=0,indexInduccion=0;

			listEvaluacionPostulanteOnline=[];
			listFullTrabProyecto=[];
			listFullActProyectoSeguridad=[];
			listConfirmEvaluacionFull=[];
			detalleObservacionFull=[],detalleAccAuditoriaFull=[];
			detalleIncidenteFull=[];
			listFullInduccionesContratista=[];
			data.inducciones.forEach(function(val1,index1){
				if(val1.evidenciaNombre!=""){
					val1.evidenciaNombre=val1.evidenciaNombre+" ("+val1.fechaRealTexto+" )"
				}
				var eventoObj={
						evidenciaNombre: val1.evidenciaNombre,
						btnClick:"editarTrabajadoresInduccionContratista("+indexInduccion+") ",
						evidenciaUrl:"/contratista/examen/evidencia?id="+val1.id,
						textInicio:"("+"Inducción"+") Actividad a completar por el administrador",
						text:" "+val1.tema,
						proyectoIndex:"",
						proyectoNombre:"",
						fecha:(val1.fechaReal==null?val1.fecha:val1.fecha),
						fechaTexto:"Fecha Planificada: "+val1.fechaTexto,
						fechaRealTexto:""+val1.fechaRealTexto,
						estadoEvento:val1.estado.id,
						iconoNombre:"<i aria-hidden='true' class='fa fa-arrow-circle-o-down '></i>"};
				
				if(val1.fechaReal!=null || val1.fecha<new Date()){
					if(val1.numTrabajadores>0){ 
					listFullEventosContratista.push(eventoObj);
					indexInduccion++;
					listFullInduccionesContratista.push(val1);
					}
				}else{

					listFullEventosContratista.push(eventoObj);
					indexInduccion++;
					listFullInduccionesContratista.push(val1);
				}
			});
			listFullProyectoSeguridad=data.proyectos;
			listFullProyectoSeguridad.forEach(function(val,index){
				if(val.estado.id!=1){
					var indicadorPositivo=0,indicadorTotal=0;
					var estadoEvento="",claseEvento="";
					 
					val.trabajadores.forEach(function(val1,index1){
						if(val1.proyectoActual!=null){
							indicadorTotal++; 
						if(val1.indicadorExamenMedico!=null && val1.indicadorSctr!=null ){
							indicadorPositivo++;
							 estadoEvento=2;
							 claseEvento="gosst-aprobado";
						}else{
							 estadoEvento=1;
							 claseEvento="gosst-neutral"; 
						}  
						//indexAuxTrab++; 
						}
					});
					
				
					var eventoObj={
							evidenciaNombre: "",
							btnClick:"nuevoTrabProyecto("+index+")",
							evidenciaUrl:"",textInicio:"Trabajadores",
							text:indicadorPositivo+" de "+indicadorTotal+" trabajador(es) apto(s)",
							proyectoIndex:index,
							proyectoNombre:val.titulo,
							fecha:0,
							fechaTexto:"",
							iconoNombre:"<i aria-hidden='true' class='fa fa-medkit'></i>"};
					 
					if(indicadorPositivo==indicadorTotal && indicadorTotal>0){
						eventoObj.estadoEvento=2;
						eventoObj.clase="gosst-aprobado";
					}else{
						eventoObj.estadoEvento=1;
						 eventoObj.clase="gosst-neutral"; 
					} 
				//	listFullEventosContratista.push(eventoObj);
				}
				if(val.estado.id!=3 && val.postulante.estado.id==2){
					 
						val.documentos.forEach(function(val1,index1){
							var nota = "";
							listCumple.forEach(function(val2,index2){
								if(val2.id==val1.isAprobado){
									nota=val2.nombre;
								}
							});
							var eventoObj={
									evidenciaNombre: val1.evidenciaNombre,
									btnClick:"editarEvaluacionPostulanteOnline("+indexAuxDoc+")",
									evidenciaUrl:"/contratista/proyecto/postulante/evaluacion/evidencia?id="+val1.id,
									textInicio:"("+"Documento postulante"+" )",
									text:""+nota+" - "+val1.preguntaNombre,
									proyectoIndex:index,
									proyectoNombre:val.titulo,
									fecha:0,
									fechaReal:0,
									fechaTexto:"",
									iconoNombre:"<i aria-hidden='true' class='fa fa-files-o'></i>"};
							
							if(val1.isAprobado==1){ 
								eventoObj.estadoEvento=2;
								eventoObj.clase="gosst-aprobado";
							}else{
								eventoObj.estadoEvento=1;
								eventoObj.clase="gosst-neutral"; 
							} 
						//	listFullEventosContratista.push(eventoObj);
						//	indexAuxDoc++;
						//	listEvaluacionPostulanteOnline.push(val1);
						});
					 
				}
				if(val.estado.id!=1 ){ 
					val.actividades.forEach(function(val1,index1){
						if(val1.tipo.id==1){
						if(val1.evidenciaNombre!=""){
							val1.evidenciaNombre=val1.evidenciaNombre+" ("+val1.fechaRealTexto+" )"
						} 
						var eventoObj={
								evidenciaNombre: val1.evidenciaNombre,
								btnClick:"editarActProyecto("+indexAuxActiv+")",
								evidenciaUrl:"/contratista/proyecto/actividad/evidencia?id="+val1.id,
								textInicio:"("+val1.tipo.nombre+") ",
								text:val1.descripcion,
								proyectoIndex:index, 
								proyectoNombre:val.titulo,
								fecha:(val1.fechaReal==null?val1.fechaPlanificada:val1.fechaReal),
								 
								fechaTexto:"Fecha Planificada: "+val1.fechaPlanificadaTexto,
								fechaRealTexto:""+val1.fechaRealTexto,
								estadoEvento:val1.estado.id,
								iconoNombre:"<i aria-hidden='true' class='fa fa-sort-amount-asc'></i>"};
						
						 
						listFullEventosContratista.push(eventoObj);
						indexAuxActiv++;
						listFullActProyectoSeguridad.push(val1);
						}
					});
				 
				}
				if(val.estado.id!=1 ){
					 
					val.formaciones.forEach(function(val1,index1){

						
						if(val1.evaluacionId==1){
						val1.extensiones.forEach(function(val2,index2){
							if(val2.evidenciaNombre!=""){
								val2.evidenciaNombre=val2.evidenciaNombre+" ("+val2.fechaRealTexto+" )"
							}
							var eventoObj={
									evidenciaNombre: val2.evidenciaNombre,
									btnClick:"objConfirmEvaluacion=" +
											"listConfirmEvaluacionFull["+indexAuxForm+"];" +
													"editarConfirmacion("+indexAuxForm+")",
									evidenciaUrl:"/capacitacion/curso/evaluacion/confirmacion/evidencia?id="+val2.id,
									textInicio:"("+val2.estado.nombre+") Extensión Capacitación Online ",
									text:"("+val1.cursoNombre+")" ,
									proyectoIndex:index,
									proyectoNombre:val.titulo,
									fecha:(val2.fechaReal==null?val2.fechaPlanificada:val2.fechaReal),
									fechaTexto:"Fecha Planificada: "+val2.fechaPlanificadaTexto,
									fechaRealTexto:""+val2.fechaRealTexto,
									estadoEvento:val2.estado.id,
									iconoNombre:"<i aria-hidden='true' class='fa fa-graduation-cap'></i>"};
							
							 
							listFullEventosContratista.push(eventoObj);
							indexAuxForm++;
							listConfirmEvaluacionFull.push(val2);
						}); 
						} 
					}); 
				}
				if(val.estado.id!=1 ){ 
					val.observacionesList.forEach(function(val1,index1){
						val1.acciones.forEach(function(val2,index2){ 
						if(true){
							if(val2.evidenciaNombre!="" &&   val2.evidenciaNombre!="----" ){
								val2.evidenciaNombre=val2.evidenciaNombre+" ("+val2.fechaRealTexto+" )"
							}
							var eventoObj={
									evidenciaNombre: val2.evidenciaNombre,
									btnClick:"objAuditoria="+
											 "{index:"+index1+" };objAccAuditoria=" +
											"detalleAccAuditoriaFull["+indexAuxObs+"];" +
													"editarAccAuditoria("+indexAuxObs+")",
									evidenciaUrl:"/gestionaccionmejora/accionmejora/evidencia?accionMejoraId="+val2.id,
									textInicio:"("+"Acción de mejora"+") / Observación ("+val1.reporte.nombre+
									" - "+val1.factor.nombre+" - "+val1.descripcion+")",
									
									text:val2.descripcion ,
									proyectoIndex:index,
									proyectoNombre:val.titulo,
									fecha:(val2.fechaReal==null?val2.fechaRevision:val2.fechaReal),
									fechaTexto:"Fecha Planificada: "+val2.fechaRevisionTexto,
									fechaRealTexto:""+val2.fechaRealTexto,
									estadoEvento:val2.estadoId,
									iconoNombre:"<i aria-hidden='true' class='fa fa-eye'></i>"};
							
							 
							listFullEventosContratista.push(eventoObj);
							indexAuxObs++;
							val2.id=val2.accionMejoraId;
							val2.observacionId=val1.id;
							
							detalleAccAuditoriaFull.push(val2);
						}
						}); 
						detalleObservacionFull.push(val1);
					});
					val.accidentes.forEach(function(val1,index1){
						val1.acciones.forEach(function(val2,index2){
							if(val2.evidenciaNombre!="" &&   val2.evidenciaNombre!="----" ){
								val2.evidenciaNombre=val2.evidenciaNombre+" ("+val2.fechaRealTexto+" )"
							}
							var eventoObj={
									evidenciaNombre: val2.evidenciaNombre,
									btnClick:"objAuditoria="+
											 "{index:"+index1+" };objAccAuditoria=" +
											"detalleAccAuditoriaFull["+indexAuxObs+"];" +
													"editarAccAuditoria("+indexAuxObs+")",
									evidenciaUrl:"/gestionaccionmejora/accionmejora/evidencia?accionMejoraId="+val2.id,
									textInicio:"("+"Acción de mejora"+") / Incidente (  "+val1.accidenteTipoNombre+")",
									text:val2.descripcion ,
									proyectoIndex:index,
									proyectoNombre:val.titulo,
									fecha:(val2.fechaReal==null?val2.fechaRevision:val2.fechaReal),
									fechaTexto:"Fecha Planificada: "+val2.fechaRevisionTexto,
									fechaRealTexto:""+val2.fechaRealTexto,
									estadoEvento:val2.estadoId,
									iconoNombre:"<i aria-hidden='true' class='fa fa-warning'></i>"};
							
							 
							listFullEventosContratista.push(eventoObj);
							indexAuxObs++;
							val2.id=val2.accionMejoraId;
							val2.accidenteId=val1.accidenteId;
							
							detalleAccAuditoriaFull.push(val2);
						}); 
						detalleIncidenteFull.push(val1);
					});
					val.auditorias.forEach(function(val4,index4){
						val4.respuestas.forEach(function(val1,index1){
							if(val1.detalleAuditoriaId>0 && val1.bitAplica==1){
							val1.acciones.forEach(function(val2,index2){
								if(val2.evidenciaNombre!="" &&   val2.evidenciaNombre!="----" ){
									val2.evidenciaNombre=val2.evidenciaNombre+" ("+val2.fechaRealTexto+" )"
								}
								var eventoObj={
										evidenciaNombre: val2.evidenciaNombre,
										btnClick:"objAuditoria="+
												 "{index:"+index1+" };objAccAuditoria=" +
												"detalleAccAuditoriaFull["+indexAuxObs+"];" +
														"editarAccAuditoria("+indexAuxObs+")",
										evidenciaUrl:"/gestionaccionmejora/accionmejora/evidencia?accionMejoraId="+val2.id,
										textInicio:"("+"Acción de mejora"+") / Auditoría (  "+val4.auditoriaTipoNombre+")" ,
										text:
												   val1.verificacion +
												"<br>" +val2.descripcion,
										proyectoIndex:index,
										proyectoNombre:val.titulo,
										fecha:(val2.fechaReal==null?val2.fechaRevision:val2.fechaReal),
										fechaTexto:"Fecha Planificada: "+val2.fechaRevisionTexto,
										fechaRealTexto:""+val2.fechaRealTexto,
										estadoEvento:val2.estadoId,
										iconoNombre:"<i aria-hidden='true' class='fa fa-check-square-o'></i>"};
								
								 
								listFullEventosContratista.push(eventoObj);
								indexAuxObs++;
								val2.id=val2.accionMejoraId;
								val2.detalleAuditoriaId=val1.detalleAuditoriaId;
								
								detalleAccAuditoriaFull.push(val2);
							}); 
							detalleAuditoriaFull.push(val1);
							}
						});
						
					});	
				}
			});
			//////////////7
			listFullEventosContratista.sort(function(a,b){ 
				  return new Date(b.fecha) - new Date(a.fecha);
				});
			listFullEventosContratista.filter(function(val,index){
				var condicionFecha=false;
				
				if(fechaFiltro==null){
					condicionFecha=true;
				}else{
					var fechaAux=new Date(fechaFiltro);
					var fechaEvento=new Date(val.fecha);
					//fechaAux.setTime(fechaFiltro)
					if(fechaAux.getFullYear()==fechaEvento.getUTCFullYear()
							&& fechaAux.getMonth()==fechaEvento.getUTCMonth()
							&& fechaAux.getDate()==fechaEvento.getUTCDate()){
						condicionFecha=true;
					}
				}
				return condicionFecha;
			}).forEach(function(val,index){
				var respAux=val.responsable; 
				var iconoEvi=""+val.evidenciaNombre;
				if(val.evidenciaNombre=="" || val.evidenciaNombre=="----"){
					iconoEvi="Sin evidencia"
				}
				var evidenciaFinalTexto="";
				if(val.evidenciaNombre!=""  && val.evidenciaNombre!="----"){
					evidenciaFinalTexto="<a  id='evidenciaEvento"+val.id+"' href='"+URL+val.evidenciaUrl+" ' class='efectoLink' target='_blank'>" +
					iconoEvi+
					
					"</a>";
				}
				if(evidenciaFinalTexto==""){
					evidenciaFinalTexto="No hay evidencia";
				}
				var textoBoton="<button class='btn btn-success'  >" +
				"<i class='fa fa-external-link-square' aria-hidden='true'></i>Editar</button>";
				var textoButonAux="<button class='btn btn-success' id='irModulo"+val.id+"'>" +
				"<i class='fa fa-chevron-circle-right' aria-hidden='true'></i> " +
				"Editar</button>" ;
				textoButonAux="";
				textoBoton="";
				var divContainer;
				var menuOpcion="<ul class='list-group listaGestionGosst' >";
				menuOpcion+="<li class='list-group-item' onclick='verificarProyectoId("+val.proyectoIndex+");"+val.btnClick+"'> <i class='fa fa-trash-o'></i> &nbsp Editar </li>"
				
				menuOpcion+="</ul>";
				 var textIn="<div class='eventoGeneral '  >" +
					"<div id='tituloEvento'>"+
					"<a class='efectoLink btn-gestion' onclick='toggleMenuOpcionActividadesContratista(this,"+index+")'>" +
					"Ver Opciones&nbsp<i class='fa fa-angle-double-down' aria-hidden='true'></i></a>" +
					menuOpcion+
					"<table class='actividadMovil'>" +
						"<tbody>" +
							"<tr>" +
							" <td colspan='2' style='font-weight: 900; color: black;font-size: 16px;'> "+val.textInicio+" </td>" +
							"</tr>"+
							"<tr>"+
							"<td style='width:30px'>"+val.iconoNombre+"</td><td>"+val.text+"</td>" +
							"</tr>" +
							"<tr>" +
							"<td>"+""+"</td><td>"+"<strong >(Proyecto: " +val.proyectoNombre+")  </strong>"+"</td>" +
							"</tr>" +
							"<tr>" +
							"<td>"+"<i class='fa fa-calendar' aria-hidden='true'></i>"+"</td> <td>"+val.fechaTexto+"</td>" +
							"</tr>" +
							"<tr>" +
							"<td>"+"<i class='fa fa-calendar-check-o' aria-hidden='true'></i>"+"</td> <td>Fecha Real"+val.fechaRealTexto+"</td>" +
							"</tr>" +
							"<tr>" +
							"<td>"+"<i class='fa fa-download' aria-hidden='true'></i>"+"</td><td>"+evidenciaFinalTexto+"</td>" +
							"</tr>" +
						"</tbody>" +
					"</table>" + 
						"<div id='btnAcceso'>" +
						textoBoton+
						textoButonAux+
						"</div>"+
					"</div>" +
					"</div>" ;
						switch(val.estadoEvento){
						case 1:
							contadorImplementar++;
							textImplementar+=textIn;
							divContainer="subListImplementar";
							break;
						case 2:
							contadorCompletado++;
							textCompletado+=textIn;
							divContainer="subListCompletado";
							textoBoton="";
							break;
						case 3:
							contadorRetrasado++;
							textRetrasado+=textIn;  
							divContainer="subListRetrasado";
							break;
						}  
			});
			if(functionCallBack!=null){
				functionCallBack(listFullEventosContratista);
				}
			if(habilitarBotones==null){
				
			
			var listPanelesPrincipal=[];
		listPanelesPrincipal.push(
				{id:"subListImplementar",clase:"divProyectoGeneral",
					nombre:"Por Implementar  ("+contadorImplementar+")",
					contenido:textImplementar},
				{id:"subListCompletado",clase:"divProyectoGeneral",
					nombre:"Completado  ("+contadorCompletado+")",
					contenido:textCompletado},
				{id:"subListRetrasado",clase:"divProyectoGeneral",
					nombre:"Retrasado ("+contadorRetrasado+")",
					contenido:textRetrasado}); 
		if(contadorRetrasado+contadorImplementar>0){ 
			$("#alertaActividadTrabajador").remove();
			$("#icoPendientes a").append("<div class='numAlertas' id='alertaActividadTrabajador'>"+(contadorRetrasado+contadorImplementar)+"</div>")
		}
		agregarPanelesDivPrincipal(listPanelesPrincipal);
		$(".listaGestionGosst").hide();
		$(".divListPrincipal #subListImplementar .tituloSubList").css({"color":"#df9226"})
		$(".divListPrincipal #subListRetrasado .tituloSubList").css({"color":"#C0504D"})
		//$(".divListPrincipal #subListCompletado .tituloSubList").css({"color":"#9BBB59"})
		var textoForm="",textFormFormacion="",textFormActividad="";
		var textObservacion="",textAuditorias="",textIncidentes="",textTrabajdor="";
	var listItemsTrab=[
	{sugerencia:"",label:" ",inputForm:"<button type='button' class='btn btn-success' onclick='marcarTodosTrabajadoresProyecto()' ><i aria-hidden='true' class='fa fa-check'></i>Agregar Todos</button>"},
	{sugerencia:"",label:"Trabajadores",inputForm:"",divContainer:"divSelTrabProy"},
	{sugerencia:"",label:"Trabajadores Inactivos",inputForm:"",divContainer:"divSelTrabProyInhab"},
	{sugerencia:" ",label:"Fecha de modificación",inputForm:"<h5>"+obtenerFechaActualNormal()+"</h5>"},
	 {sugerencia:"",label:" ",inputForm:"<button type='submit' class='btn btn-success' ><i aria-hidden='true' class='fa fa-paper-plane'></i>Enviar</button>"}
						];
	var listItemsForm=[
	           		{sugerencia:"",label:"Item a evaluar",inputForm:"<h5 id='trMovilItem'></h5>"},
	           		{sugerencia:"Ej: Necesito más información, ¿Involucra todo el proyecto?",label:"Observación del Contratista",inputForm:"<input type='text' class='form-control' id='observacionEval' placeholder=' '>"},
	           		{sugerencia:"Ej: Documento legal, Foto",label:"Evidencia",inputForm:"",divContainer:"eviDocumento"},
	           		{sugerencia:"",label:"Observación del Contratante",inputForm:"<h5 id='trMovilObservacion'></h5>"},
	           		{sugerencia:"",label:" ",inputForm:"<button type='submit' class='btn btn-success' ><i aria-hidden='true' class='fa fa-paper-plane'></i>Enviar</button>"}
	           							];
	var listItemsFormFormacion=[
{sugerencia:"Si ya ha realizado la actividad, solo introduzca la evidencia",label:"Fecha planificada",inputForm:"<input type='date' class='form-control' id='inputFecConf' placeholder=' '>"},
 {sugerencia:"",label:"Evidencia",inputForm:"",divContainer:"eviDocumentoFormacion"},
	           		{sugerencia:"",label:" ",inputForm:"<button type='submit' class='btn btn-success' ><i aria-hidden='true' class='fa fa-paper-plane'></i>Enviar</button>"}];
	
	var listItemsFormActividad=[  
	                    		{sugerencia:"",label:"Categoria",divContainer:"divCategActiv",inputForm:" "},
	                    		{sugerencia:"",label:"Actividad",divContainer:"divDescActiv",inputForm:"<input type='text' class='form-control' id='inputDescActProy' placeholder=' '>"},
	                    		{sugerencia:"Si ya ha realizado la actividad, solo introduzca la evidencia",divContainer:"divFechaPlanActiv",label:"Fecha planificada",inputForm:"<input type='date' class='form-control' id='inputFecPlanActProy' placeholder=' '>"},
	                    		{sugerencia:"Completar estos campos una vez se haya completado la actividad",divContainer:"divMensajeActividad",label:"",inputForm:""} ,
	                    		{sugerencia:"",divContainer:"divSeparatorActividad",label:"",inputForm:""} ,
	                    		{sugerencia:"",label:"Evidencia",inputForm:"",divContainer:"eviActividad"} ,
	                    		
	                    		{sugerencia:"valor del 0 al 100",divContainer:"divPuntajeActividad",label:"% de cumplimiento",inputForm:"<input min='0' max='100' step='1' type='number' class='form-control' id='inputPuntajeActProy' >"},
	                      		 {sugerencia:"",divContainer:"expoActividad",label:"Expositor",inputForm:"<input type='text' class='form-control' id='inputExpoActProy' placeholder=' '>"},
	                    		 {sugerencia:"",divContainer:"numTrabActividad",label:"# Trabajadores",inputForm:"<input type='number' class='form-control' id='inputNumTrabActProy' placeholder=' '>"},
	                    		 {sugerencia:"",divContainer:"duracionActividad",label:"Duración (minutos)",inputForm:"<input type='number' class='form-control' id='inputDuraActProy' placeholder=' '>"},
	                     		 {sugerencia:"",label:"Observación del Contratista",divContainer:"divObsActProy",inputForm:"<input type='text' class='form-control' id='inputObsActProy' placeholder=' '>"},
	                    		 {sugerencia:"",label:" ",inputForm:"<button type='submit' class='btn btn-success' ><i aria-hidden='true' class='fa fa-paper-plane'></i>Enviar</button>"} 
	                    		];
	var listItemsFormObservacion=[
{sugerencia:"",label:"Acción de mejora",inputForm:"<input type='text' class='form-control' id='inputDescAcc' placeholder=' '>"},
{sugerencia:"Si ya ha realizado la acción, solo introduzca la evidencia",label:"Fecha planificada",inputForm:"<input type='date' class='form-control' id='inputFecAcc' placeholder=' '>"},
{sugerencia:"Ej: El incidente fue causado por un peatón, fue un evento fortuito ",label:"Observación del contratista",inputForm:"<input type='text' class='form-control' id='inputObsAcc' placeholder=' '>"},
{sugerencia:"",label:"Evidencia",inputForm:" ",divContainer:"eviAccionMejora"}, 
{sugerencia:"",label:" ",inputForm:"<button type='submit' class='btn btn-success' ><i aria-hidden='true' class='fa fa-paper-plane'></i>Enviar</button>"} 
		
	                              ];
	var listItemsFormAuditorias=[ ];
	var listItemsFormIncidentes=[ ];
	listItemsTrab.forEach(function(val,index){
		textTrabajdor+=obtenerSubPanelModulo(val)
	})
	listItemsForm.forEach(function(val,index){
		textoForm+=obtenerSubPanelModulo(val)
	});
	listItemsFormFormacion.forEach(function(val,index){
		textFormFormacion+=obtenerSubPanelModulo(val);
	});
	listItemsFormActividad.forEach(function(val,index){
		textFormActividad+=obtenerSubPanelModulo(val);
	});
	listItemsFormObservacion.forEach(function(val,index){
		textObservacion+=obtenerSubPanelModulo(val)
	});
	listItemsFormAuditorias.forEach(function(val,index){
		textAuditorias+=obtenerSubPanelModulo(val);
	});
	listItemsFormIncidentes.forEach(function(val,index){
		textIncidentes+=obtenerSubPanelModulo(val);
	});
	
	var listItemsExam=[
	                   {sugerencia:"",label:"Trabajadores inscritos",inputForm:"",divContainer:"divTrabCupoInduc"},
	                   {sugerencia:"",label:"Cupos disponibles",inputForm:"",divContainer:"divCupoInduc"},
	              	 {sugerencia:"",label:"Mis Trabajadores",inputForm:"",divContainer:"divSelTrabInduc"},
	   			 	 {sugerencia:"",label:"Vigencia",inputForm:"",divContainer:"divVigInduc"},
	              	 {sugerencia:"",label:"Fecha planificada",inputForm:" ",divContainer:"divFechaInduc"}, 
	              	 {sugerencia:"Ej. documentos, escaneos",label:"Evidencia",inputForm:"",divContainer:"eviExamContratista"},
	         			 {sugerencia:"",label:"",divContainer:"divSubmitButton",inputForm:"<button type='submit' class='btn btn-success' ><i aria-hidden='true' class='fa fa-paper-plane'></i>Enviar</button>"} 
	               		];
	  			var textInducciones="";
	  			listItemsExam.forEach(function(val,index){
	  				textInducciones+=obtenerSubPanelModulo(val);
	  			});
		var listEditarPrincipal=[
								{id:"editarMovilTrabProy" ,clase:"contenidoFormVisible",
								nombre:""+"Editar trabajdresdsssss",
								contenido:"<form id='formEditarTrabajadores' class='eventoGeneral'>"+textTrabajdor+"</form>"},
		 						{id:"editarMovilDocProy" ,clase:"contenidoFormVisible",
		 							nombre:""+"Editar documento",
		 							contenido:"<form id='formEditarDocumento' class='eventoGeneral'>"+textoForm+"</form>"},
	 							{id:"editarMovilFormProy" ,clase:"contenidoFormVisible",
		 							nombre:""+"Editar formación",
		 							contenido:"<form id='formEditarFormacion' class='eventoGeneral'>"+textFormFormacion+"</form>"},
	 							{id:"editarMovilActivProy" ,clase:"contenidoFormVisible",
		 							nombre:""+"Editar act",
		 							contenido:"<form id='formEditarActividad' class='eventoGeneral'>"+textFormActividad+"</form>"},
	 							{id:"editarMovilObsProy" ,clase:"contenidoFormVisible",
		 							nombre:""+"Editar observacion",
		 							contenido:"<form id='formEditarObservacion' class='eventoGeneral'>"+textObservacion+"</form>"},
	 							{id:"editarMovilAuditoriaProy" ,clase:"contenidoFormVisible",
		 							nombre:""+"Editar formación",
		 							contenido:"<form id='formEditarAuditoria' class='eventoGeneral'>"+textAuditorias+"</form>"},
	 							{id:"editarMovilIncidentProy" ,clase:"contenidoFormVisible",
		 							nombre:""+"Editar act",
		 							contenido:"<form id='formEditarIncidente' class='eventoGeneral'>"+textIncidentes+"</form>"},
	 							{id:"editarMovilInduccion" ,clase:"contenidoFormVisible",
			 							nombre:""+"Editar act",
			 							contenido:"<form id='formEditarInduccion' class='eventoGeneral'>"+textInducciones+"</form>"}];
		agregarPanelesDivPrincipal(listEditarPrincipal);
		var fechaAyuda=new Date();
		 $('#formEditarTrabajadores').on('submit', function(e) { 
		        e.preventDefault();   
		        guardarTrabsProyecto(
		        		function(){
		        			habilitarVistaActividadesContratista(organizarActividadesContratistas,fechaFiltro);
		        		});
		 });
		$('#formEditarDocumento').on('submit', function(e) {  
	        e.preventDefault();   
	        guardarPostulanteEvaluacionOnline(
	        		function(){
	        			habilitarVistaActividadesContratista(organizarActividadesContratistas,fechaFiltro);
	        		});
		 });
		 $('#formEditarFormacion').on('submit', function(e) { 
		        e.preventDefault();   
		        guardarConfirmacion(
		        		function(){
		        			habilitarVistaActividadesContratista(organizarActividadesContratistas,fechaFiltro);
		        		});
		 });       
		 $('#formEditarActividad').on('submit', function(e) { 
		        e.preventDefault();   
		        guardarActProyecto(
		        		function(){
		        			habilitarVistaActividadesContratista(organizarActividadesContratistas,fechaFiltro);
		        		});
		 });
		 $('#formEditarObservacion').on('submit', function(e) { 
		        e.preventDefault();   
		        guardarAccionAuditoria(
		        		function(){
		        			habilitarVistaActividadesContratista(organizarActividadesContratistas,fechaFiltro);
		        		});
		 });
		
		 $('#formEditarInduccion').on('submit', function(e) { 
		        e.preventDefault();   
		        guardarInduccionContratista(
		        		function(){
		        			habilitarVistaActividadesContratista(organizarActividadesContratistas,fechaFiltro);
		        		});
		 });
			$(".contenidoFormVisible").hide();
			
			if(deleteCalendar!=null){
				$("#subListImplementar:first").remove();
				$("#subListCompletado:first").remove();
				$("#subListRetrasado:first").remove();
			}
			completarBarraCarga();
			}
		break;
		}
	
	});
	
		
}

function obtenerSubPanelModulo(val){
	var estiloDefault="";
	if(val.inputForm=="" &&  val.sugerencia=="" && val.label==""){
		estiloDefault="    border-top: 1px solid #a5aaae;"
	}
	return "<div class='form-group row "+val.clase+"' style='"+estiloDefault+"'>"+
	   " <label for='colFormLabel' class='col-sm-4 col-form-label'>"+val.label+"</label>"+
	    "<div class='col-sm-8' id='"+val.divContainer+"'>"+
	     " "+val.inputForm+
	     "<small>"+val.sugerencia+" </small>"+
	    "</div>"+
	  "</div>";
}
function verSeccionRestringida(){
	$(".divListPrincipal")
	.append(
			"<div id='subListCapacitacion'  >" +
			"<div class='tituloSubList' style='background:#e8e6e6'>Sin acceso al módulo.<br> Más información consulte al administrador del sistema</div>" +
			"<div class='contenidoSubList'></div>" +
		"</div>" );
};
function agregarPanelesDivPrincipal(listPanelesPrincipal){
	if(listPanelesPrincipal.length==0){
		$(".divListPrincipal")
		.append(
				"<div id='divNoResultados'  >" +
				"<div class='tituloSubList'></div>" +
				"<div class='contenidoSubList'></div>" +
				"</div>" 
				);
		$("#divNoResultados" ).find(".tituloSubList").html( 
			//"<i aria-hidden='true' class='fa fa-list'></i>"+ 
			"Sin resultados"); 
	}
	listPanelesPrincipal.forEach(function(val,index){
		$(".divListPrincipal")
		.append(
				"<div id='"+val.id+"' class='"+val.clase+"' >" +
				"<div class='tituloSubList'></div>" +
				"<div class='contenidoSubList'></div>" +
				"</div>" 
				);
		var textoBtn="<button class='btn btn-success' style= 'padding:0px 4px 0px 4px'  onclick='cambiarDivSubContenido(this,"+val.proyectoId+")'>" +
	"<i class='fa fa-caret-down fa-3x' aria-hidden='true'></i></button>";
		$("#"+val.id).find(".tituloSubList").html(textoBtn
			//	+"<i aria-hidden='true' class='fa fa-list'></i>"
				+val.nombre); 
		$("#"+val.id).find(".contenidoSubList").html(
				val.contenido
		);
	})
	
}

