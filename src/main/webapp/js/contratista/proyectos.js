var ProyectoId=0;var proyectoObj=[];
var banderaEdicion4=false;
var banderaEdicionEvalOnline=false;
var listClasificacion=[{id:1,nombre:"DAF"},{id:2,nombre:"DINF"}];
var listFullProyectoSeguridad=[];
var listResponsableProy;
var listTipoIpercProy;

var listTipoEvaluacion;
var listAreaProyecto;
var listTrabajadores;
var contadorListTrab;
var proyectoEstado;
var indexPostulante;
var indexEval;
var listEvaluacionPostulanteOnline;
var listEvaluacionTotal=[];
var listTipoActividades=[];
var listTipoExamenes=[];
$(function(){
	$("#btnNuevoProyecto").attr("onclick", "javascript:nuevoProyecto();");
	$("#btnCancelarProyecto").attr("onclick", "javascript:cancelarNuevoProyecto();");
	$("#btnGuardarProyecto").attr("onclick", "javascript:guardarProyecto();");
	$("#btnEliminarProyecto").attr("onclick", "javascript:eliminarProyecto();");
	$("#btnEvaluacionTotalProyecto").attr("onclick", "javascript:evaluacionTotalProyecto();");
	$("#btnConsolidadoAudiProyecto").attr("onclick", "javascript:consolidadoAuditoriaContratista();");
	$("#btnResumenAudiProyecto").attr("onclick", "javascript:resumenAuditoriaContratista();");
	$("#btnResumenUnidadAudiProyecto").attr("onclick", "javascript:resumenUnidadAuditoriaContratista();");
	$("#btnResumenUnidadDetalAudiProyecto").attr("onclick", "javascript:resumenUnidadDetalleAuditoriaContratista();");
	
	
	
	if(getSession("accesoUsuarios")=="2" || getSession("accesoUsuarios")=="6"){
		$(".nav-tabs").remove();
		$(".tab-content").remove();
		 
		$("#btnCancelarEvalOnline").attr("onclick","volverPostulanteEvaluacionOnline()");
		$("#btnGuardarEvalOnline").attr("onclick","guardarPostulanteEvaluacionOnline()");
		
		insertMenuPrimeraVez(cargarProyectosSeguridad);
		
	}
	
	
	$("#btnClipboardProy").on("click",function(){
		var texto=obtenerDatosTablaEstandar("tblProyectos"); 
		 copiarAlPortapapeles(texto,"btnClipboardProy");
			alert("Se han guardado al clipboard la tabla " );
	});
	
	 
});
/**
 * 
 */
function getButonDescarga(url,evNombre){
	var btn="<a class='btn btn-success' target='_blank'" +
			" href='"+URL+url+"'>" +
			"<i class=' fa fa-download'></i>Descargar" +
			"</a>";
	if(evNombre==""){
		btn="";
	}
	return btn;
}
var actsVisibles=[];
var examsVisibles=[];
function verAcordeEstadoEvalDoc(){
	var listEstadoFinal=[];
	examsVisibles=[];
	actsVisibles=[];
	$("#selEstadoDocEval").val().forEach(function(val){
		listEstadoFinal.push(parseInt(val));
	})
	evalDocActividades.forEach(function(val){
		$("#docact"+val.id).remove();
		if(listEstadoFinal.indexOf(val.evaluacion.nota.id)!=-1){
			agregarFilaActividades(val)
		}
		if($(".fix-inner #docact"+val.id).is(":visible")){
			actsVisibles.push(val);
		}
	});
	
	evalDocExamenes.forEach(function(val){
		$("#docexam"+val.id).remove();
		if(listEstadoFinal.indexOf(val.evaluacion.nota.id)!=-1){
			agregarFilaExamenes(val)
		}
		if($(".fix-inner #docexam"+val.id).is(":visible")){
			examsVisibles.push(val);
		}
	});
}
var evalDocActividades=[],evalDocExamenes=[];
function agregarFilaActividades(val){
	var divAfter="#trActTipo"+val.tipo.id ;
	if(val.evaluacion==null){
		val.evaluacion={fechaTexto:"",nota:{id:3,icono:""} };
	}else{
		if(val.evaluacion.nota==null){
			val.evaluacion.nota={id:3,icono:""};
		}
	}
	var selEstadoObsTotal=crearSelectOneMenuOblig("selEvalTotalAct","verIconoAcuerdoEval(this)",
			listEvaluacionTotal,val.evaluacion.nota.id,"id","nombre");
	var detalle="";
	if(val.tipo.id==5){
		val.categoria.nombre ="";
	}else{
		val.observacion=" / "+val.observacion;
		val.categoria.nombre ="<strong>"+val.categoria.nombre+"</strong>";
	}
	if(val.tipo.id==1){
		detalle=val.estado.nombre;
		if(val.estado.id!=2){
			detalle+="<br> ("+val.fechaPlanificadaTexto+")";
		}
	}
	if(val.tipo.id==2 || val.tipo.id==4){
		if(val.evidenciaNombre==""){
			detalle="Pendiente de registrar";
		}else{
			detalle="Registrado"
		}
	}
	 $(divAfter).after("<tr id='docact"+val.id+"'>" +
	 		"<td>  "+
	 		val.categoria.nombre +"   "+val.descripcion +"   "+val.observacion	+"<br>"+
	 		val.evidenciaNombre+""+
	 		
	 		"</td>"+
	 		
	 		"<td id='tdFechaEval'> "+val.evidenciaFechaTexto+"</td>"+
	 		"<td  > "+getButonDescarga("/contratista/proyecto/actividad/evidencia?id="+val.id,val.evidenciaNombre)+"</td>"+
			"<td>"+detalle+"</td>"+
	 		"<td style='border-left: 3px solid #2e9e8f;'>" +
				"<label>"+val.evaluacion.nota.icono+"</label> "+selEstadoObsTotal+"</td>" +
	 		"<td>"+"<input class='form-control' id='inputObsEvalTotal'>"+"</td>"+
			"<td id='tdFechaEval'> "+val.evaluacion.fechaTexto+"</td>"+
			"</tr>");
	 $("#docact"+val.id).find("#inputObsEvalTotal").val(val.evaluacion.observacion)
}
function agregarFilaExamenes(val){
	var divAfter="#trExamTipo"+val.tipo.id ;
	if(val.evaluacion==null){
		val.evaluacion={fechaTexto:"",nota:{id:3,icono:""} };
	}else{
		if(val.evaluacion.nota==null){
			val.evaluacion.nota={id:3,icono:""};
		}
	}
	var detalle="";
	if(val.tipo.id==5){
		
	}else{
		detalle="Trabajadores: "+val.trabajadoresNombre+"<br>" +
				"Fin de vigencia: "+val.fechaFinVigenciaTexto
	}
	var selEstadoObsTotal=crearSelectOneMenuOblig("selEvalTotalAct","verIconoAcuerdoEval(this)",
			listEvaluacionTotal,val.evaluacion.nota.id,"id","nombre");
	 $(divAfter).after("<tr id='docexam"+val.id+"'>" +
	 		"<td>"+val.evidenciaNombre+"<br>"+val.tema+"</td>"+
	 		"<td id='tdFechaEval'> "+(val.evidenciaFechaTexto)+"</td>"+
	 		"<td  > "+getButonDescarga("/contratista/examen/evidencia?id="+val.id,val.evidenciaNombre)+"</td>"+
	 		"<td>"+detalle+"</td>"+
	 		"<td style='border-left: 3px solid #2e9e8f;'>" +
				"<label>"+val.evaluacion.nota.icono+"</label> "+selEstadoObsTotal+"</td>" +
	 		"<td>"+"<input class='form-control' id='inputObsEvalTotal'>"+"</td>"+
			"<td id='tdFechaEval'> "+val.evaluacion.fechaTexto+"</td>"+
			"</tr>");
	 $("#docexam"+val.id).find("#inputObsEvalTotal").val(val.evaluacion.observacion)
}
var listEstado=[{id:1,nombre:"Aprobado"},
                {id:2,nombre:"Desaprobado",selected:1},
                {id:3,nombre:"Pendiente",selected:1},
                {id:4,nombre:"Por re-evaluar",selected:1}];
function evaluacionTotalProyecto(){
	if(proyectoObj.postulante.contratista==null){
		alert("Debe asignar un contratista al proyecto antes de evaluar los documentos");
		return;
	}
	$("#tabProyectos .container-fluid").hide();
	$("#divEvaluacionTotalProyecto").show();
	
	crearSelectOneMenuObligMultipleCompleto("selEstadoDocEval", "verAcordeEstadoEvalDoc()", listEstado, "id",
			"nombre","#divEstadoDocsEvaluar","",null)
	$("#tabProyectos h2")
	.html("<a onclick='volverDivProyectos()'>Proyecto ("
			+proyectoObj.titulo+") </a> > Evaluación general");
	var listEvaluar=[
             	{nombre:listTipoActividades[0].nombre,divTr:"trActTipo1",isUnico:false},
             	{nombre:"Plan de emergencia",divTr:"trActTipo2",isUnico:false},
             	{nombre:"Plan de manejo de residuos",divTr:"trActTipo4",isUnico:false},
             	{nombre:listTipoActividades[3].nombre+" Asociados al proyecto",divTr:"trActTipo5",isUnico:false},
             	{nombre:"Matriz de Riesgo Integrado",divTr:"trProyIperc",isUnico:true,
             		divsubtr:"dociperc",evaluacion:proyectoObj.evaluacionIperc,
             		detalle:"Evaluaciones no significativas<br> "+proyectoObj.postulante.indicadorIperc},
             	{nombre:"Exámenes Médicos",divTr:"trExamTipo1",isUnico:false},
             	{nombre:"SCTR",divTr:"trExamTipo2",isUnico:false},
             	{nombre:"Cargo RISST",divTr:"trExamTipo4",isUnico:false},
         		{nombre:"T-Registro",divTr:"trExamTipo6",isUnico:false} ,
             	{nombre:"EPP",divTr:"trExamTipo8",isUnico:false} 
         		];
	
	$("#tblEvalTotalProy tbody").html("");
	var selTipoEvaluacionTotal=crearSelectOneMenuOblig("selTipoEvaluacionTotal","verIconoAcuerdoEval(this)",
			listEvaluacionTotal,"","id","nombre");
	listEvaluar.forEach(function(val,index){
		var filaAfter="";
		if(val.isUnico){
			filaAfter="<tr id='"+val.divsubtr+"'>" +
					"<td>General</td>" +
					"<td id='tdFecha'> "+""+"</td>"+
					"<td  > "+
					//"<a class='btn btn-success'>Ver detalles</a> "+
					"</td>"+
					"<td>"+val.detalle+"</td>"+
					"<td><label></label>"+selTipoEvaluacionTotal+"</td>" +
					"<td>"+"<input class='form-control' id='inputObsEvalTotal'>"+"</td>"+
					"<td id='tdFechaEval'> "+""+"</td>"+
					
					"</tr>"
		}
		$("#tblEvalTotalProy tbody").append("" +
				"<tr id='"+val.divTr+"'>" +
				"<td class='info' colspan='7' style='background: #d6e6e5f7;'>"+val.nombre+"</td>" +
				"</tr>"+
				filaAfter
				);
		if(val.isUnico){
			if(val.evaluacion==null){
				val.evaluacion={fechaTexto:"",nota:{icono:"",id:3},observacion:"" };
			}
			 $("#"+val.divsubtr).find("#selTipoEvaluacionTotal").val(val.evaluacion.nota.id)
			 $("#"+val.divsubtr).find("#inputObsEvalTotal").val(val.evaluacion.observacion)
			 $("#"+val.divsubtr).find("#tdFechaEval").html(val.evaluacion.fechaTexto)
			 $("#"+val.divsubtr).find("label").html(val.evaluacion.nota.icono)
			 
		} 
	});
	if(parseInt(getSession("isProyectoSimple"))==1 ){
		$("#trExamTipo6").remove();
	}
	if(parseInt(getSession("accesoUsuarios"))==3 || parseInt(getSession("accesoUsuarios"))==1){
		
	}else{
		$("#trExamTipo1").remove();
	}
	
	var docsId=proyectoObj.examenesPermitidosId.split(",");
	docsId.forEach(function(val1){
		switch(val1){
		case 6:
			$("#trExamTipo1").remove();
			break;
		case 7:
			$("#trExamTipo2").remove();
			break;
		case 9:
			$("#trExamTipo3").remove();
			break;
		case 11:
			$("#trExamTipo4").remove();
			break;
		case 12:
			$("#trExamTipo6").remove();
			break;
		case 8:
			$("#trExamTipo8").remove();
			break;
		}
	});
	var activiadesId=proyectoObj.actividadesPermitidasId.split(",");
	activiadesId.forEach(function(val1){
		$("#trActTipo"+val1).remove();
	});
	
	$("#btnGuardarEvalsProyecto").hide();
	callAjaxPostNoUnlock(URL + '/contratista/proyecto/actividades/revision', 
		{id:proyectoObj.id}, function(data){
			evalDocActividades=data.list;
			evalDocActividades.forEach(agregarFilaActividades);
			callAjaxPost(URL + '/contratista/examenes', 
					{
				id:(proyectoObj.postulante.contratista.id),
				proyectoActual:{id : proyectoObj.id},
				examen:{tipo:{id:null}}
				}, function(data){
					evalDocExamenes=data.examenes;
					evalDocExamenes.forEach(agregarFilaExamenes);
					goheadfixedY("#tblEvalTotalProy","#wrapEvalTotalProy");
					verAcordeEstadoEvalDoc();
					$("#btnGuardarEvalsProyecto").show();
			})
	})
	
	
}
function verIconoAcuerdoEval(obj){
	var notaId=parseInt($(obj).val());
	listEvaluacionTotal.every(function(val){
		if(val.id==notaId){
			$(obj).parent().find("label").html(val.icono)
			return false;
		}
		return true;
	});
}
function guardarEvaluacionesDocumentosProyecto(){
	var listActividades=[];
	var evalIperc={};
	var listExamenes=[];
	actsVisibles.forEach(function(val){
			listActividades.push({
				id:val.id,
				evaluacion:{
					nota:{id:$("#docact"+val.id).find("#selEvalTotalAct").val()},
					observacion:$("#docact"+val.id).find("#inputObsEvalTotal").val()
				}
			});
		
	}); 
	evalIperc={
			proyecto:{id:proyectoObj.id},
			nota:{
				id:$("#dociperc").find("#selTipoEvaluacionTotal").val(),
				icono:$("#dociperc").find("label").html()},
			observacion:$("#dociperc").find("#inputObsEvalTotal").val()
		}
	examsVisibles.forEach(function(val){
			listExamenes.push({
				id:val.id,
				evaluacion:{
					nota:{id:$("#docexam"+val.id).find("#selEvalTotalAct").val()},
					observacion:$("#docexam"+val.id).find("#inputObsEvalTotal").val()
				}
			});	
	});
	callAjaxPost(URL + '/contratista/proyecto/evaluacion_doc/save', 
						{id:proyectoObj.id,
					evaluacionIperc:evalIperc,
					actividades:listActividades,
					examenes:listExamenes}
						, function(data){
							if(data.CODE_RESPONSE=="05"){
								proyectoObj.evaluacionIperc=evalIperc;
								alert("Se guardaron las evaluaciones de documentos");
								volverDivProyectos();
							}
					})
	
}
function reiniciarFiltroProyectoGeneral(){
	$(".camposFiltroTablaProy").hide();
	$("#wrapProyecto").show();
	$("#btnVerFiltroProy").show();
	$("#btnAplicFiltroProy").hide();
	listFullProyectoSeguridad.forEach(function(val,index){
		 
			 $("#trprogen"+val.id).show()
		 
	});
}
function verFiltroProyectoGeneral(){
	$(".camposFiltroTablaProy").show();
	 $("#wrapProyecto").hide();
	$("#btnVerFiltroProy").hide();
	$("#btnAplicFiltroProy").show();

	$("#filtroVolverProyecto").show();
}
function aplicarFiltroProyectoGeneral(){
	
	  volverVistaFiltroProyecto();
	  
	cargarProyectosSeguridad();
	
}
function crearFiltroProyecto(functionCallback){
	
	  
	  callAjaxPost(URL+"/contratista/proyectos/filtro",{
		  trabajadorId : parseInt(getSession("trabajadorGosstId")),
		  empresaId : getSession("gestopcompanyid")},function(data){
		  var listEstadoAux=[{id:1,nombre:"Pendiente"},
			                   {id:2,nombre:"En curso"},
			                   {id:3,nombre:"Completado"}]
			var asd=crearSelectOneMenuOblig("selEstadoAuxProyecto", "cargarProyectosSeguridad()", listEstadoAux, "2", "id",
					"nombre");
			$("#divSelEstadoProyecto").prepend("Estado"+asd);	
		  
			$("#tabProyectos").show();
			$("#divProyectosFull").show();
			$(".camposFiltroTablaProy").show();
			$(".camposFiltroTablaProy").html(
					"<div class='contain-filtro' id='filtroAreaProy'>" +
					"<label for='checkFiltroArea'  >Áreas"+
					"</label>" +
					"<div id='divSelectFiltroArea'> " +
					"</div>" +
					"" +
					"</div>"+
					
					"<div class='contain-filtro' id='filtroRespProy'>" +
					"<label for='checkFiltroResp'  >Responsables Internos"+
					"</label>" +
					"<div id='divSelectFiltroResp'> " +
					"</div>" +
					"" +
					"</div>"+
					"<div class='contain-filtro' id='filtroContratistaProy'>" +
					"<label for='checkFiltroContra'  >Contratistas"+
					"</label>" +
					"<div id='divSelectFiltroContra'> " +
					"</div>" +
					"" +
					"</div>"+
					
					"<div class='contain-filtro' id='filtroNombreProy'>" +
					"<label for='checkFiltroNombre'  >Proyectos"+
					"</label>" +
					"<div id='divSelectFiltroProyNombre'> " +
					"</div>" +
					"" +
					"</div>"			
					
			);
			var listContraObj=data.listContratistas;
			var listNombreObj = data.listProyectos;
			var listResponsableProy = data.listResponsable;
			var listAreaObj=data.listAreas;
			
			crearSelectOneMenuObligMultipleCompleto("selMultipleAreaProy", "", listAreaObj, 
						"id","nombre","#divSelectFiltroArea","TODOS");
			
			   crearSelectOneMenuObligMultipleCompleto("selMultipleRespProy", "", listResponsableProy, 
						"id","nombre","#divSelectFiltroResp","TODOS");
			 crearSelectOneMenuObligMultipleCompleto("selMultipleContraProy", "", listContraObj, 
						"id","nombre","#divSelectFiltroContra","TODOS");
			 crearSelectOneMenuObligMultipleCompleto("selMultipleNombreProy", "", listNombreObj, 
						"id","titulo","#divSelectFiltroProyNombre","TODOS");
			 $("#tabProyectos").hide();
			 $("#divProyectosFull").hide();
			 
			 volverVistaFiltroProyecto();
				if(functionCallback != null){functionCallback()}
				
	  });

}
function volverVistaFiltroProyecto(){
	 $("#btnVerFiltroProy").show();
		$("#btnAplicFiltroProy").hide();
		$("#filtroVolverProyecto").hide();
		
		$(".camposFiltroTablaProy").hide();
		  $("#wrapProyecto").show();

}

function cargarProyectosSeguridad() {
	$("#btnCancelarProyecto").hide();
	$("#btnNuevoProyecto").show();
	$("#btnEliminarProyecto").hide();
	$("#btnGuardarProyecto").hide();
	$("#btnEvaluacionTotalProyecto").hide();
	$("#btnConsolidadoAudiProyecto").hide();
	
	
	
	$("#tabProyectos .container-fluid").hide();
	$("#divProyectosFull").show();
	$("#tabProyectos h2").html("Proyectos");
	$("#btnResObsProy").hide();
	var contratistaId=parseInt(getSession("contratistaGosstId"));
	
	var listArratResp=$("#selMultipleRespProy").val();
	var listFinalResp=[]; 
	if(listArratResp != null){
		listArratResp.forEach(function(val,index){
			listFinalResp.push(parseInt(val));
		}); 
	}
	
	
	var listArratContra=$("#selMultipleContraProy").val();
	var listFinalContra=[];

	if(listArratContra != null){
		listArratContra.forEach(function(val,index){
			listFinalContra.push(parseInt(val));
		}); 
	}
	var listArratNombre=$("#selMultipleNombreProy").val();
	var listFinalNombre=[];

	if(listArratNombre != null){
		listArratNombre.forEach(function(val,index){
			listFinalNombre.push(parseInt(val));
		});
	}
	
	
	var listArratArea=$("#selMultipleAreaProy").val();
	var listFinalArea=[];

	if(listArratArea != null){
		listArratArea.forEach(function(val,index){
			listFinalArea.push(parseInt(val));
		});
	}
	
	var dataParam={
			empresaId:getSession("gestopcompanyid"),
			listResponsablesId : (listFinalResp.length==0?null:listFinalResp),
			listContratistasId : (listFinalContra.length==0?null:listFinalContra),
			listProyectosId : (listFinalNombre.length==0?null:listFinalNombre),
			listAreasId : (listFinalArea.length==0?null:listFinalArea),
			
			estado:($("#selEstadoAuxProyecto").val()!=null?
			{id:$("#selEstadoAuxProyecto").val()}:null),
			contratistaId:(contratistaId>0?contratistaId:null)
			};
	callAjaxPost(URL + '/contratista/proyectos', dataParam, function(data) {
		switch (data.CODE_RESPONSE) {
		case "05":
			banderaEdicion4=false;
			ProyectoId=0;proyectoObj=[];
			$("#tblProyectos tbody tr").remove();
			listFullProyectoSeguridad=data.proyectos;
			listTipoEvaluacion=data.listTipoEvaluacion;
			listAreaProyecto=data.listAreaProyecto;
			listEvaluacionTotal=data.tipo_evaluacion;
			listTipoActividades=data.tipo_actividades;
			listTipoActividades.forEach(function(val){
				$("#headAct"+val.id).html(""+val.nombre);
				$("#headStatAct"+val.id).html("Status "+val.nombre)
			});
			listTipoExamenes=data.tipo_examen;
			switch(parseInt(getSession("accesoUsuarios"))){
			case 2:
					verLateralesFijosContratista();
					marcarIconoModuloContratista(3);
					completarBarraCarga();
				break;
			case 6:
				verLateralesFijosContratistaTrabajador();
				marcarIconoModuloContratistaTrabajador(0);
				break;
				
			default:
				
				listResponsableProy=data.listResponsableProy;
				listTipoIpercProy=data.tipo_iperc;
				$("#liAuditoria").hide();
				agregarTablaFullProyecto(listFullProyectoSeguridad);
				completarBarraCarga();
				
				break;
				
			}
			completarBarraCarga();
			
			break;
		default:
			alert("Ocurrió un error al traer las programaciones!");
		}
	});
	
}

function agregarTablaFullProyecto(listProyectos){
	
	$("#btnCancelarProyecto").hide();
$("#btnNuevoProyecto").show();
$("#btnEliminarProyecto").hide();
$("#btnGuardarProyecto").hide();
$("#btnEvaluacionTotalProyecto").hide();
$("#btnConsolidadoAudiProyecto").hide();

$("#btnResumenUnidadAudiProyecto").show();
$("#btnResumenUnidadDetalAudiProyecto").show();
$("#btnResumenAudiProyecto").show();

$("#tabProyectos .container-fluid").hide();
$("#divProyectosFull").show();
$("#tabProyectos h2").html("Proyectos");
$("#btnResObsProy").hide();

listFullProyectoSeguridad=listProyectos;
banderaEdicion4=false;
ProyectoId=0;proyectoObj=[];
$("#tblProyectos tbody tr").remove();
$("#divProyectosFull #btnConsolidadoInspecc").hide();
	listFullProyectoSeguridad.sort(function(a,b){
		return a.notaFinal - b.notaFinal;
		});
	listFullProyectoSeguridad.forEach(function(val,index){
		var indicadorGeneral="<i style='font-size: 18px;' class='fa fa-exclamation-circle' ></i>"
		var indicadorExtCap="",indicadorPrograma="",indicadorPlan="",indicadorResiduo="",indicadorDocumento="",
		indicadorObservacion="",indicadorObsContratista="",indicadorIncidente="",indicadorAudi="",
		indicadorInspeccion="",indicadorIperc="",indicadorTrabajador="";
		if(val.hasNotificacionExtensionCapacitacion==1){
			indicadorExtCap=indicadorGeneral;
		}
		if(val.hasNotificacionPrograma==1){
			indicadorPrograma=indicadorGeneral;
		}
		if(val.hasNotificacionPlan==1){
			indicadorPlan=indicadorGeneral;
		}
		if(val.hasNotificacionResiduo==1){
			indicadorResiduo=indicadorGeneral;
		}
		if(val.hasNotificacionDocumento==1){
			indicadorDocumento=indicadorGeneral;
		}
		if(val.hasNotificacionObservacion==1){
			indicadorObservacion=indicadorGeneral;
		}
		if(val.hasNotificacionObservacionContratista==1){
			indicadorObsContratista=indicadorGeneral;
		}
		if(val.hasNotificacionIncidente==1){
			indicadorIncidente=indicadorGeneral;
		}
		if(val.hasNotificacionAuditoria==1){
			indicadorAudi=indicadorGeneral;
		}
		if(val.hasNotificacionInspeccion==1){
			indicadorInspeccion=indicadorGeneral;
		}
		if(val.hasNotificacionIperc==1){
			indicadorIperc=indicadorGeneral;
		}
		if(val.hasNotificacionTrabajador==1){
			indicadorTrabajador=indicadorGeneral;
		}
		
		$("#tblProyectos tbody").append(
				"<tr id='trprogen"+val.id+"' onclick='editarProyecto("+index+")'>" +
//			
				"<td id='proytit"+val.id+"'>"+val.titulo+"</td>" +
				"<td id='proydesc"+val.id+"'>"+val.descripcion+"</td>" +
				"<td id='proyrp"+val.id+"'>"+val.responsable.nombre+"</td>" +
				"<td id='proyresp"+val.id+"'>"+val.responsablesSecundariosNombre+"</td>" +
				"<td id='proyarea"+val.id+"'>"+val.area.nombre+"</td>" +
				"<td id='proyfecinicio"+val.id+"'>"+val.fechaInicioTexto+"</td>" +
				"<td id='proyfecfin"+val.id+"'>"+val.fechaFinTexto+"</td>" +
				"<td id='proytipeval"+val.id+"'>"+val.lista.nombre+"</td>" +
				
				
				"<td id='proycontra"+val.id+"'>"+val.contratistaNombre+" ("+val.contratistaTotal+")"+"</td>" +
				"<td id='proytipip"+val.id+"'>"+val.tipoIperc.nombre+"</td>" +
				"<td id='proyinddoc"+val.id+"'>"+val.numDocumentosSinEvaluar+" / "+val.numDocumentosTotal +"</td>" +
				"<td id='proycap"+val.id+"'>"+val.capacitacionesTotal+"</td>" +
				"<td id='proyevi"+val.id+"'>"+val.cursosTotal+indicadorExtCap+"</td>" +
				"<td id='1proyact"+val.id+"'>"+val.actividadesTotal1+indicadorPrograma+"</td>" +
				"<td id='2proyact"+val.id+"'>"+val.actividadesTotal2+indicadorPlan+"</td>" + 
				"<td id='4proyact"+val.id+"'>"+val.actividadesTotal4+indicadorResiduo+"</td>" +
				//"<td id='5proyact"+val.id+"'>"+val.actividadesTotal5+indicadorDocumento+"</td>" +
				"<td id='proyobs"+val.id+"'>"+val.observaciones+indicadorObservacion+"</td>" +
				"<td id='proyobsctr"+val.id+"'>"+val.observacionesContratista+indicadorObsContratista+"</td>" +
				"<td id='proyinc"+val.id+"'>"+val.incidentesTotal+indicadorIncidente+"</td>" +
				"<td id='proyaudi"+val.id+"'>"+val.auditoriaRealizadas+indicadorAudi+"</td>" +
				"<td id='proyinsp"+val.id+"'>"+val.inspeccionRealizadas+indicadorInspeccion+"</td>" +
				
				"<td id='proyiperc"+val.id+"'>"+val.indicadorIperc+indicadorIperc+"</td>" +
				"<td id='proytrab"+val.id+"'>"+val.trabajadoresTotal+indicadorTrabajador+"</td>" +
				"<td id='proyestado"+val.id+"'>"+val.estado.nombre+"</td>" +
				
				"<td style='border-left:3px solid #afb5bb'> "+pasarDecimalPorcentaje(val.postulante.indicadorActividadesDecimal1)+" ("+val.postulante.indicadorActividades1+")</td>"+
				"<td> "+pasarDecimalPorcentaje(val.postulante.indicadorActividadesDecimal2)+" ("+val.postulante.indicadorActividades2+")</td>"+
				"<td> "+pasarDecimalPorcentaje(val.postulante.indicadorActividadesDecimal4)+" ("+val.postulante.indicadorActividades4+")</td>"+
				"<td> "+pasarDecimalPorcentaje(val.postulante.indicadorActividadesDecimal5)+" ("+val.postulante.indicadorActividades5+")</td>"+
				"<td> "+pasarDecimalPorcentaje(val.postulante.indicadorIpercDecimal)+" ("+val.postulante.indicadorIperc+")</td>"+
				"<td> "+pasarDecimalPorcentaje(val.postulante.indicadorTrabajadoresDecimal)+" ("+val.postulante.indicadorTrabajadores+")</td>"+
				"<td style='background-color:"+colorPuntajeGosst(val.notaFinal)+"'> "+pasarDecimalPorcentaje(val.notaFinal)+"</td>"+
						
		
		"</tr>");
	});
	goheadfixedY("#tblProyectos","#wrapProyecto");
	formatoCeldaSombreableTabla(true,"tblProyectos");
}
//INicio de hallazgo
var detalleHallazgoFull=[];
//Inicio de incidentes
 var detalleIncidenteFull=[];
//Inicio de accion de auditoria
var detalleObservacionFull=[];
//Inicio de accion de auditoria
var detalleAuditoriaFull=[];
var objAuditoria={};
var detalleAccAuditoriaFull=[];
var banderaEdicionAccAuditoria=false;
var objAccAuditoria={};
var objDetalleAuditoria={};
var funcionalidadesDetalleAudi=[
                    {id:"opcAgregar",nombre:"<i class='fa fa-plus-square'></i> Agregar Corrección Inmediata",functionClick:function(data){
                    	nuevaAccionDetalleAuditoria(indexDetalleAudi);
                    }  
                    }];
 
var funcionalidadesAccionMejora=[
                            {id:"opcEditar",nombre:"<i class='fa fa-pencil-square-o'></i>Editar",functionClick:function(data){editarAccAuditoria()}  },
                            {id:"opcElimnar",nombre:"<i class='fa fa-trash'></i>Eliminar",functionClick:function(data){eliminarAccionMejoraAudi();} }];
var lastHeight=0;
function marcarSubOpcionAccMejora(pindex){
	
	funcionalidadesAccionMejora[pindex].functionClick();
	$(".subDetalleAccion ul").hide();
	$(".subSubDetalleAccion ul").hide();
}
function marcarSubOpcionDetalleAuditoria(pindex){
	funcionalidadesDetalleAudi[pindex].functionClick();
	$(".subDetalleAccion ul").hide(); 	
}
var indexAccMejora;
function toggleMenuOpcionAccMejora(obj,pindex){
	lastHeight=$(obj).offset().top;
	objAccAuditoria=detalleAccAuditoriaFull[pindex]; 
	indexAccMejora=pindex;
	$(obj).parent(".subDetalleAccion").find("ul").toggle();
	$(obj).parent(".subDetalleAccion").siblings().find("ul").hide();
	$(obj).parent(".subSubDetalleAccion").find("ul").toggle();
	$(obj).parent(".subSubDetalleAccion").siblings().find("ul").hide(); 
}
var indexDetalleAudi;
function toggleMenuOpcionDetalleAudi(obj,pindex){
	objDetalleAuditoria=detalleAuditoriaFull[pindex]; 
	indexDetalleAudi=pindex;
	$(obj).parent(".subDetalleAccion").find("ul").toggle();
	$(obj).parent(".subDetalleAccion").siblings().find("ul").hide(); 	
}
//AVANZAR
function verCompletarAccioneObservacion(pindex){
	 
	pindex=defaultFor(pindex,objAuditoria.index);
	//
	 $(".subOpcionAccion").html("");
	 $(".subOpcionAccion").hide();
	
	// 
var audiObj={accionMejoraTipoId : 1,
		observacionId:detalleObservacionFull[pindex].id};

banderaEdicionAccAuditoria=false;
objAuditoria={index:pindex};
objAccAuditoria={observacionId:detalleObservacionFull[pindex].id};
callAjaxPost(URL + '/gestionaccionmejora/accionmejora', 
			audiObj, function(data){
				if(data.CODE_RESPONSE=="05"){ 
					$("#tblAccAudi tbody tr").remove();
					detalleAccAuditoriaFull=data.list;
					detalleAccAuditoriaFull.forEach(function(val,index){
						val.id=val.accionMejoraId;
						val.observacionId=objAccAuditoria.observacionId;
						 //
						var menuOpcion="<ul class='list-group' >";
						funcionalidadesAccionMejora.forEach(function(val,index){
							menuOpcion+="<li class='list-group-item' onclick='marcarSubOpcionAccMejora("+index+")'>"+val.nombre+" </li>"
						});
						menuOpcion+="</ul>"
						var textResp="";
						if(val.respuesta.length>0){
							textResp=iconoGosst.advertir+"Respuesta: "+val.respuesta+"<br>"
						}
						 $("#proyectoTable"+listFullProyectoSeguridad[indexPostulante].id)
							.find("#divMovilObservacion"+detalleObservacionFull[pindex].id+" .subOpcionAccion").show()
							.append("<div class='subDetalleAccion  gosst-neutral' style='border-color:"+val.estadoColor+"'>" +
									"<a class='efectoLink btn-gestion' onclick='toggleMenuOpcionAccMejora(this,"+index+")'>" +
										"Ver Opciones<i class='fa fa-angle-double-down' aria-hidden='true'></i>" +
									"</a>" +
									menuOpcion+
									"<strong>ACCIÓN DE MEJORA "+(index+1)+"</strong><br>"+
									"<i class='fa fa-info' aria-hidden='true'></i> Descripcion: " +val.descripcion+ "<br>"+
									"<i class='fa fa-calendar' aria-hidden='true'></i> Fecha de Planificada: " +val.fechaRevisionTexto+"<br>"+ 
									"<i class='fa fa-download'></i> Evidencia: "+
									(val.evidenciaNombre=="----"?
											"Sin Reigstro"
											:
											"<a class='efectoLink' href='"+URL+"/gestionaccionmejora/accionmejora/evidencia?accionMejoraId="+val.id+"'>" +
												val.evidenciaNombre+
											"</a>")+"<br><br>"+
									textResp+
									"</div> " );
						 
					});
					$(".subDetalleAccion ul").hide();
					formatoCeldaSombreableTabla(true,"tblAccAudi");
				}else{
					console.log("NOPNPO")
				}
			});
}
//AVANZAR
function verCompletarAccioneHallazgoContratista(pindex){
	 
	pindex=defaultFor(pindex,objAuditoria.index);
	//
	 $(".subOpcionDetalleAccion").html("");
	 $(".subOpcionDetalleAccion").hide();
	
	// 
var audiObj={accionMejoraTipoId : 1,
		hallazgoId:detalleHallazgoFull[pindex].id};

banderaEdicionAccAuditoria=false;
objAuditoria={index:pindex};
objAccAuditoria={hallazgoId:detalleHallazgoFull[pindex].id};
callAjaxPost(URL + '/gestionaccionmejora/accionmejora', 
			audiObj, function(data){
				if(data.CODE_RESPONSE=="05"){
					detalleAccAuditoriaFull=data.list;
					detalleAccAuditoriaFull.forEach(function(val,index){
						val.id=val.accionMejoraId;
						val.hallazgoId=objAccAuditoria.hallazgoId;
						 //
						var menuOpcion="<ul class='list-group' >";
						funcionalidadesAccionMejora.forEach(function(val,index){
							menuOpcion+="<li class='list-group-item' onclick='marcarSubOpcionAccMejora("+index+")'>"+val.nombre+" </li>"
						});
						menuOpcion+="</ul>"
						var textResp="";
						if(val.respuesta.length>0){
							textResp=iconoGosst.advertir+"<strong>Respuesta del Cliente: </strong>"+val.respuesta+"<br>"
						}
						var claseAux="gosst-neutral";
						if(val.respuesta=="" && val.estadoId==2){
							claseAux="gosst-aprobado"
						}
						 $("#proyectoTable"+listFullProyectoSeguridad[indexPostulante].id)
							.find("#subDivMovilDetalleAudi"+detalleHallazgoFull[pindex].id+" .subOpcionDetalleAccion").show()
							.append("<div class='subSubDetalleAccion  "+claseAux+"' style='border-color:"+val.estadoColor+"'>" +
									"<a class='efectoLink btn-gestion' onclick='toggleMenuOpcionAccMejora(this,"+index+")'>" +
									"Ver Opciones<i class='fa fa-angle-double-down' aria-hidden='true'></i></a>" +
									menuOpcion+ 
									"<strong>ACCIÓN DE MEJORA "+(index+1)+"</strong><br>"+
									"<strong><i class='fa fa-calendar' aria-hidden='true'></i> Fecha Planificada: </strong>" +val.fechaRevisionTexto+ "<br>"+
									"<strong><i class='fa fa-info' aria-hidden='true'></i> Descripción: </strong>" +val.descripcion+ "<br>"+
									textResp+
									"<i class='fa fa-download'></i>Evidencia: "+
									(val.evidenciaNombre=="----"?
											"Sin Registro":
											"<a class='efectoLink'  href='"+URL+"/gestionaccionmejora/accionmejora/evidencia?accionMejoraId="+val.id+"'>" +
												val.evidenciaNombre+
											"</a>")+ 
									"</div> " );
					});
					$(".subSubDetalleAccion ul").hide();
					formatoCeldaSombreableTabla(true,"tblAccAudi");
				}else{
					console.log("NOPNPO")
				}
			});
}
//FIn
function verCompletarAccioneAuditoria(pindex){
	
		pindex=defaultFor(pindex,objAuditoria.index);
		$(".subOpcionDetalleAccion").html("");
		$(".subOpcionDetalleAccion").hide();
		//$(".detalleDocumentosProyecto").html(""); 
			
	var audiObj={accionMejoraTipoId : 1,
			detalleAuditoriaId:detalleAuditoriaFull[pindex].detalleAuditoriaId};
	banderaEdicionAccAuditoria=false;
	objAuditoria={index:pindex};
	objAccAuditoria={detalleAuditoriaId:detalleAuditoriaFull[pindex].detalleAuditoriaId};
	callAjaxPost(URL + '/gestionaccionmejora/accionmejora', 
				audiObj, function(data){
					if(data.CODE_RESPONSE=="05"){  
						detalleAccAuditoriaFull=data.list;
						detalleAccAuditoriaFull.forEach(function(val,index){
							val.id=val.accionMejoraId;
							val.detalleAuditoriaId=objAccAuditoria.detalleAuditoriaId;
							 //
							var menuOpcion="<ul class='list-group' >";
							funcionalidadesAccionMejora.forEach(function(val,index){
								menuOpcion+="<li class='list-group-item' onclick='marcarSubOpcionAccMejora("+index+")'>"+val.nombre+" </li>"
							});
							var textResp="";
							if(val.respuesta.length>0){
								textResp=iconoGosst.advertir+" "+val.respuesta+"<br>"
							}
							var claseAux="gosst-neutral";
							if(val.respuesta=="" && val.estadoId==2){
								claseAux="gosst-aprobado"
							}
							menuOpcion+="</ul>"
							 $("#proyectoTable"+listFullProyectoSeguridad[indexPostulante].id)
								.find("#subDivMovilDetalleAudi"+detalleAuditoriaFull[pindex].detalleAuditoriaId+" .subOpcionDetalleAccion").show()
								.append("<div class='subSubDetalleAccion "+claseAux+"'  style='border-color:"+val.estadoColor+"'>" +
											"<a class='efectoLink btn-gestion' onclick='toggleMenuOpcionAccMejora(this,"+index+")'>" +
												"Ver Opciones<i class='fa  fa-angle-double-down' aria-hidden='true'></i>" +
											"</a>" +
											menuOpcion+
											"<strong>ACCIÓN DE MEJORA "+(index+1)+"</strong><br>"+
											textResp+
											"<i class='fa fa-info' aria-hidden='true'></i> Descripción: " +val.descripcion+ "<br>"+
											"<i class='fa fa-calendar' aria-hidden='true'></i> Fecha de Registro: " +val.fechaRevisionTexto+ "<br>"+
											"<i class='fa fa-download' aria-hidden='true'></i> Evidencia: " +
											(val.evidenciaNombre=="----"?
													"Sin Registro"
													:
													"<a class='efectoLink' href='"+URL+"/gestionaccionmejora/accionmejora/evidencia?accionMejoraId="+val.id+"'>" +
														val.evidenciaNombre+
													"</a>")+ "<br>"+
										"</div> " ); 
						});
						$(".subSubDetalleAccion ul").hide();
						formatoCeldaSombreableTabla(true,"tblAccAudi");
					}else{
						console.log("NOPNPO")
					}
				});
	
	
}
function verCompletarAccioneIncidente(pindex){
	pindex=defaultFor(pindex,objAuditoria.index);
	//
	 $(".subOpcionAccion").html("");
	 $(".subOpcionAccion").hide();
	
	// 
	$("#tabProyectos .container-fluid").hide();
	$("#divCompletarAccionAuditoriaProyecto").show();
	$("#tabProyectos h4")
	.html("<a onclick='volverDivProyectos()'>Proyecto ("
			+listFullProyectoSeguridad[indexPostulante].titulo+") </a> " +
					"> <a onclick='volverDivIncidentes()'> Incidente ("+detalleIncidenteFull[pindex].accidenteTipoNombre+") </a> " +
							"> Acciones de Mejora"); 
var audiObj={accionMejoraTipoId : 1,
		accidenteId:detalleIncidenteFull[pindex].accidenteId};

banderaEdicionAccAuditoria=false;
objAuditoria={index:pindex};
objAccAuditoria={accidenteId:detalleIncidenteFull[pindex].accidenteId};
callAjaxPost(URL + '/gestionaccionmejora/accionmejora', 
			audiObj, function(data){
				if(data.CODE_RESPONSE=="05"){ 
					$("#tblAccAudi tbody tr").remove();
					detalleAccAuditoriaFull=data.list;
					detalleAccAuditoriaFull.forEach(function(val,index){
						
						
						val.id=val.accionMejoraId;
						val.accidenteId=objAccAuditoria.accidenteId;
						 //
						var menuOpcion="<ul class='list-group' >";
						funcionalidadesAccionMejora.forEach(function(val,index){
							menuOpcion+="<li class='list-group-item' onclick='marcarSubOpcionAccMejora("+index+")'>"+val.nombre+" </li>"
						});
						menuOpcion+="</ul>"
						var textResp="";
						if(val.respuesta.length>0){
							textResp=iconoGosst.advertir+" "+val.respuesta+"<br>"
						}
						var claseAux="gosst-neutral";
						if(val.respuesta=="" && val.estadoId==2){
							claseAux="gosst-aprobado"
						}
						 $("#proyectoTable"+listFullProyectoSeguridad[indexPostulante].id)
							.find("#divMovilIncidente"+detalleIncidenteFull[pindex].accidenteId+" .subOpcionAccion").show()
							.append("<div class='subDetalleAccion  "+claseAux+"' style='border-color:"+val.estadoColor+"'>" +
										"<a class='efectoLink btn-gestion' onclick='toggleMenuOpcionAccMejora(this,"+index+")'>" +
											"Ver Opciones <i class='fa fa-angle-double-down' aria-hidden='true'></i>" +
										"</a>" +
										menuOpcion+
										"<strong>ACCIÓN DE MEJORA "+(index+1)+"</strong><br>"+ 
										"<i class='fa fa-calendar' aria-hidden='true'></i> Fecha de Registro: " +val.fechaRevisionTexto+"<br>"+ 
										"<i class='fa fa-info' aria-hidden='true'></i> Descripción: " +val.descripcion+ "<br>"+ 
										textResp+
										"<i class='fa fa-download' aria-hidden='true'></i> Evidencia: "+
										(val.evidenciaNombre=="----"?"Sin Registro"
												:
												"<a class='efectoLink' href='"+URL+"/gestionaccionmejora/accionmejora/evidencia?accionMejoraId="+val.id+"'>" +
													val.evidenciaNombre+
												"</a>")+"<br>"+
									"</div> " );
						 
					});
					$(".subDetalleAccion ul").hide(); 
				}else{
					console.log("NOPNPO")
				}
			});
}
function nuevaAccionAuditoria(observacionIndex){
	$("#inputFecAcc").attr("disabled",false);
	$("#editarMovilObsProy").find("form input").val("");
		objAccAuditoria={observacionId:detalleObservacionFull[observacionIndex].id};
		objAccAuditoria.id = 0;
		objAuditoria={index:observacionIndex};
		
		$(".divListPrincipal>div").hide();
		$("#editarMovilObsProy").show();
		$("#editarMovilObsProy").find(".tituloSubList")
		.html(textoBotonVolverContenido+"Nueva acción de mejora de observación de '"
				+detalleObservacionFull[observacionIndex].descripcion+"' ");
		var options=
		{container:"#eviAccionMejora",
				functionCall:function(){ },
				descargaUrl:"",
				esNuevo:true,
				idAux:"AccionMejoraProy",
				evidenciaNombre:""};
		crearFormEvidenciaCompleta(options);  
}
function nuevaAccionIncidente(incidenteIndex){
	$("#inputFecAcc").attr("disabled",false);
	$("#editarMovilObsProy").find("form input").val("");
	objAccAuditoria={accidenteId:detalleIncidenteFull[incidenteIndex].accidenteId};
	objAccAuditoria.id = 0;
	objAuditoria={index:incidenteIndex}; 
	$(".divListPrincipal>div").hide();
	$("#editarMovilObsProy").show();
	$("#editarMovilObsProy").find(".tituloSubList")
	.html(textoBotonVolverContenido+"Nueva acción de mejora de incidente    '"
			+detalleIncidenteFull[incidenteIndex].accidenteTipoNombre+"' ");
	var options=
	{container:"#eviAccionMejora",
			functionCall:function(){ },
			descargaUrl:"",
			esNuevo:true,
			idAux:"AccionMejoraProy",
			evidenciaNombre:""};
	crearFormEvidenciaCompleta(options);  
} 
function nuevaAccionDetalleAuditoria(detalleIndex){
	$("#inputFecAcc").attr("disabled",false);
	$("#editarMovilObsProy").find("form input").val("");
	objAccAuditoria={detalleAuditoriaId:detalleAuditoriaFull[detalleIndex].detalleAuditoriaId};
	objAccAuditoria.id = 0;
	objAuditoria={index:detalleIndex}; 
	$(".divListPrincipal>div").hide();
	$("#editarMovilObsProy").show();
	$("#editarMovilObsProy").find(".tituloSubList")
	.html(textoBotonVolverContenido+"Nueva acción de mejora de evaluacion de auditoría  '"
			+detalleAuditoriaFull[detalleIndex].verificacion+"' ");
	var options=
	{container:"#eviAccionMejora",
			functionCall:function(){ },
			descargaUrl:"",
			esNuevo:true,
			idAux:"AccionMejoraProy",
			evidenciaNombre:""};
	crearFormEvidenciaCompleta(options);  
}
function nuevaAccionHallazgoAuditoria(detalleIndex){
	lastHeight=$("#subDivMovilDetalleAudi"+detalleHallazgoFull[detalleIndex].id).offset().top;
	$("#inputFecAcc").attr("disabled",false);
	$("#editarMovilObsProy").find("form input").val("");
	objAccAuditoria={hallazgoId:detalleHallazgoFull[detalleIndex].id};
	objAccAuditoria.id = 0;
	objAuditoria={index:detalleIndex}; 
	$(".divListPrincipal>div").hide();
	$("#editarMovilObsProy").show();
	$("#editarMovilObsProy").find(".tituloSubList")
	.html(textoBotonVolverContenido+"Nueva acción de mejora de hallazgo  '"
			+detalleHallazgoFull[detalleIndex].descripcion+"' ");
	var options=
	{container:"#eviAccionMejora",
			functionCall:function(){ },
			descargaUrl:"",
			esNuevo:true,
			idAux:"AccionMejoraProy",
			evidenciaNombre:""};
	crearFormEvidenciaCompleta(options);  
}
function editarAccAuditoria(){
	if(!banderaEdicionAccAuditoria){
		$(".divListPrincipal>div").hide();
		$("#editarMovilObsProy").show();
		
		if(objAccAuditoria.observacionId>0){
			$("#editarMovilObsProy").find(".tituloSubList")
			.html(textoBotonVolverContenido+"Editar acción de la observación '"
					+detalleObservacionFull[objAuditoria.index].descripcion+"' ");
		}
		if(objAccAuditoria.accidenteId>0){
			$("#editarMovilObsProy").find(".tituloSubList")
			.html(textoBotonVolverContenido+"Editar acción del incidente '"
					+detalleIncidenteFull[objAuditoria.index].accidenteTipoNombre+"' ");
		}
		
		if(objAccAuditoria.detalleAuditoriaId>0){
			$("#editarMovilObsProy").find(".tituloSubList")
			.html(textoBotonVolverContenido+"Editar accion de '"
					+detalleAuditoriaFull[objAuditoria.index].verificacion+"' ");
		}
		if(objAccAuditoria.hallazgoId>0){
			$("#editarMovilObsProy").find(".tituloSubList")
			.html(textoBotonVolverContenido+"Editar accion de Hallazgo'"
					+detalleHallazgoFull[objAuditoria.index].descripcion+"' ");
		}
		
		objAccAuditoria.id=objAccAuditoria.accionMejoraId; 
	  
		$("#inputDescAcc").val(objAccAuditoria.descripcion);
		// 
		$("#inputFecAcc").val(convertirFechaInput(objAccAuditoria.fechaRevision));
		$("#inputFecAcc").attr("disabled",true);
		// 
		$("#inputObsAcc").val(objAccAuditoria.observacion);
		//
		 var eviNombre=objAccAuditoria.evidenciaNombre;
	var options=
	{container:"#eviAccionMejora",
			functionCall:function(){ },
			descargaUrl:"/gestionaccionmejora/accionmejora/evidencia?accionMejoraId="+objAccAuditoria.id,
			esNuevo:false,
			idAux:"AccionMejoraProy",
			evidenciaNombre:eviNombre};
	crearFormEvidenciaCompleta(options);
		 
		hallarEstadoImplementacionAccAuditoria();
		banderaEdicionAccAuditoria = false;
	}
}
function guardarAccionAuditoria(functionCallBack) {

	var campoVacio = false;
	var inputDesc = $("#inputDescAcc").val();

	var inputFecha =convertirFechaTexto( $("#inputFecAcc").val());
	var obsevacion=$("#inputObsAcc").val()
	  
	if(inputFecha==null){
		campoVacio=true;
	}
	hallarEstadoImplementacionAccAuditoria();
	if (!campoVacio) {

		var dataParam = {
				accionMejoraId : objAccAuditoria.id ,observacion:$("#inputObsAcc").val(),
			descripcion : inputDesc,
			fechaRevision : inputFecha,
			fechaReal:null,responsableNombre:"Contratista : "+getSession("gestopusername"),responsableMail:"@asd",
			accionMejoraTipoId : 1,
			estadoId : objAccAuditoria.accionEstado,
			companyId : getSession("gestopcompanyid"),
			gestionAccionMejoraId : null,
			detalleAuditoriaId:objAccAuditoria.detalleAuditoriaId,
			observacionId:objAccAuditoria.observacionId,
			accidenteId:objAccAuditoria.accidenteId,
			hallazgoId:objAccAuditoria.hallazgoId
		};
		 
		callAjaxPost(URL + '/gestionaccionmejora/accionmejora/save', dataParam,
				function(data){
			if(objAccAuditoria.observacionId>0){
				guardarEvidenciaAuto(data.nuevoId,"fileEviAccionMejoraProy" 
						,bitsEvidenciaAccionMejora,"/gestionaccionmejora/accionmejora/evidencia/save",function(){
					volverDivSubContenido();
					verCompletarObsProyecto(indexPostulante);
					$("#proyectoTable"+listFullProyectoSeguridad[indexPostulante].id)
					.find(".detalleObservacionesProyecto").show();
					if(functionCallBack!=null){ 
						functionCallBack();
					}
				},"accionMejoraId"); 
				
			}
			if(objAccAuditoria.detalleAuditoriaId>0){
				guardarEvidenciaAuto(data.nuevoId,"fileEviAccionMejoraProy" 
						,bitsEvidenciaAccionMejora,"/gestionaccionmejora/accionmejora/evidencia/save",function(){
					
					if(functionCallBack!=null){ 
						functionCallBack();
					}else{
						volverDivSubContenido();
						verCompletarEvaluacionesAuditoria();
						verCompletarAccioneAuditoria();
					}
				},"accionMejoraId"); 
			}
			if(objAccAuditoria.accidenteId>0){
				guardarEvidenciaAuto(data.nuevoId,"fileEviAccionMejoraProy"  
						,bitsEvidenciaAccionMejora,"/gestionaccionmejora/accionmejora/evidencia/save",function(){
					volverDivSubContenido();
					verCompletarIncidenteProyecto(indexPostulante);
					$("#proyectoTable"+listFullProyectoSeguridad[indexPostulante].id)
					.find(".detalleIncidentesProyecto").show() ;
					if(functionCallBack!=null){ 
						functionCallBack();
					}
				},"accionMejoraId"); 
			
			}
			if(objAccAuditoria.hallazgoId>0){
				guardarEvidenciaAuto(data.nuevoId,"fileEviAccionMejoraProy"  
						,bitsEvidenciaAccionMejora,"/gestionaccionmejora/accionmejora/evidencia/save",function(){
					volverDivSubContenido();
					verCompletarInspeccionesGenral(indexPostulante);
					$("#proyectoTable"+listFullProyectoSeguridad[indexPostulante].id)
					.find(".detalleInspeccionProyecto").show() ;
					verCompletarHallazgosContratista();
					if(functionCallBack!=null){ 
						functionCallBack();
					}
				},"accionMejoraId"); 
			
			}
		},null,null,null,null,false);
	} else {
		alert("Debe ingresar todos los campos.");
	}
}
function eliminarAccionMejoraAudi() {
	var r = confirm("¿Está seguro de eliminar la Accion de mejora?");
	if (r == true) {
		var dataParam = {
				accionMejoraId : objAccAuditoria.id 
		};

		callAjaxPost(URL + '/gestionaccionmejora/accionmejora/delete',
				dataParam, function(){
			if(objAccAuditoria.observacionId>0){ 
					volverDivSubContenido();
					verCompletarObsProyecto(indexPostulante);
					$("#proyectoTable"+listFullProyectoSeguridad[indexPostulante].id)
					.find(".detalleObservacionesProyecto").show() 
				 
				
			}
			if(objAccAuditoria.detalleAuditoriaId>0){

			}
			if(objAccAuditoria.accidenteId>0){
			 
					volverDivSubContenido();
					verCompletarIncidenteProyecto(indexPostulante);
					$("#proyectoTable"+listFullProyectoSeguridad[indexPostulante].id)
					.find(".detalleIncidentesProyecto").show();
			}
			
			if(objAccAuditoria.hallazgoId>0){
				verCompletarInspeccionesGenral(indexPostulante);
				$("#proyectoTable"+listFullProyectoSeguridad[indexPostulante].id)
				.find(".detalleInspeccionProyecto").show() ;
				verCompletarHallazgosContratista();
			}
			
			
		});
	}
}
function cancelarAccionMejoraAudi() {
	verCompletarAccioneAuditoria();
}
//Fin accion de mejora de auditoria 
function verCompletarInduccionesProyecto(pindex){
	pindex=defaultFor(pindex,indexPostulante); 
	indexPostulante=pindex;
	// 
	$("#proyectoTable"+listFullProyectoSeguridad[pindex].id).parent("#tituloEvento")
	.parent(".eventoGeneral").parent(".contenidoSubList")
	.parent(".divProyectoGeneral").siblings()
	.find(".detalleInduccionProyecto").hide();
	
	$("#proyectoTable"+listFullProyectoSeguridad[pindex].id)
	.find(".detalleInduccionProyecto").toggle();
	var contratistaId=parseInt(getSession("contratistaGosstId"));
	var dataParam={ 
			id:(contratistaId),
			proyectoActual:{id:listFullProyectoSeguridad[pindex].id}
			};
	callAjaxPost(URL + '/contratista/inducciones/contratista', dataParam, function(data) {
		var listInduccionesProyectos=data.examenes;
		$("#proyectoTable"+listFullProyectoSeguridad[pindex].id)
		.find(".detalleInduccionProyecto").html("<td colspan='3'></td>");

		listInduccionesProyectos.forEach(function(val,index){
		var textoBotonConfirm =" ",textoBotonAgregar="<a onclick='marcarIconoModuloContratista(7) '> " +
		"Programar inducción </a>";
 
		 //
		var claseNotaDocumento="gosst-neutral";
		if(val.numTrabajadores==listFullProyectoSeguridad[pindex].trabajadoresTotal){
			claseNotaDocumento="gosst-aprobado"
		}
		$("#proyectoTable"+listFullProyectoSeguridad[pindex].id)
		.find(".detalleInduccionProyecto td") 
		.append("<div id='divMovilFormacion"+val.id+"'>" +
				"<div class='detalleAccion "+claseNotaDocumento+"'> " +
				"<i aria-hidden='true' class='fa fa-info'></i>Tema: "+
				val.tema+ "<br>"+
				"<i aria-hidden='true' class='fa fa-info'></i>Evaluados: "+
				(val.numTrabajadores)+" Trabajador(es)<br>" +
						"<i aria-hidden='true' class='fa fa-calendar'></i> "+val.fechaTexto+"<br>" +
						"<i aria-hidden='true' class='fa fa-file-o'></i>"+val.evidenciaNombre+
						"</div>" +
				"<div class='opcionesAccion'>"+textoBotonAgregar+textoBotonConfirm+"</div>" +
					 
					"</div>")
		//
		 
	});
	
	
	});
	//
}
var listCursosProyectos;
var funcionalidadExtCap=[
	{id:"opcAgregar", nombre:"<i class='fa fa-plus-square'></i> Agregar extensión",functionClick:function(data){nuevaAccionEvaluacion(indexExtCap);}}
	];
function marcarSubOpcionExtCap(pindex){
	funcionalidadExtCap[pindex].functionClick();
$(".detalleAccion ul").hide(); 
}
var indexExtCap;
function toggleMenuOpcionExtCap(obj,pindex){ 
	indexExtCap=pindex;
	$(obj).parent(".detalleAccion").find("ul").toggle();
	$(obj).parent(".detalleAccion").siblings().find("ul").hide(); 
}
 function verCompletarCursoProyecto(pindex){
	pindex=defaultFor(pindex,indexPostulante); 
	var proyectoObj1 = {
			id : listFullProyectoSeguridad[pindex].id
	};
	indexPostulante=pindex;
	// 
	$("#proyectoTable"+listFullProyectoSeguridad[pindex].id).parent("#tituloEvento")
	.parent(".eventoGeneral").parent(".contenidoSubList")
	.parent(".divProyectoGeneral").siblings()
	.find(".detalleFormacionProyecto").hide();
	
	$("#proyectoTable"+listFullProyectoSeguridad[pindex].id)
	.find(".detalleFormacionProyecto").toggle()
	//
	callAjaxPost(URL + '/contratista/proyecto/cursos/evaluaciones', proyectoObj1, function(data) {
		listCursosProyectos=data.list;
		$("#proyectoTable"+listFullProyectoSeguridad[pindex].id)
		.find(".detalleFormacionProyecto").html("<td colspan='3'></td>");

	listCursosProyectos.forEach(function(val,index){
		var textoBotonConfirm =" ",textoBotonAgregar="<a onclick='marcarIconoModuloContratista(8) '> " +
		"Dar examen </a>";

		if(val.evaluacionId==1){
			textoBotonConfirm ="<button class='btn btn-success' style='padding:5px' " +
			"onclick='verConfirmacionesEvaluacion("+index+")'>" +
	"<i class='fa fa-external-link-square' aria-hidden='true'></i></button>";
			textoBotonAgregar="<a onclick='nuevaAccionEvaluacion("+index+")'> " +
			"Agregar";
			textoBotonConfirm="<a class='linkVerFormacion' onclick='verConfirmacionesEvaluacion("+index+")'> " +
					"Ver extensión ("+val.confirmacionEvaluacionTotal+")</a> ";
		}
		 //
		var claseNotaDocumento="gosst-neutral";
		if(val.evaluacionId==1){
			claseNotaDocumento="gosst-aprobado"
		}
		 var menuOpcion="<ul class='list-group listaGestionGosst' >";
			funcionalidadExtCap.forEach(function(val1,index1){
				menuOpcion+="<li class='list-group-item' onclick='marcarSubOpcionExtCap("+index1+")'>"+val1.nombre+" </li>"
			});
			menuOpcion+="</ul>"
		$("#proyectoTable"+listFullProyectoSeguridad[pindex].id)
		.find(".detalleFormacionProyecto td") 
		.append("<div id='divMovilFormacion"+val.id+"'>" +
					"<div class='detalleAccion "+claseNotaDocumento+"'> " +
						"<a class='efectoLink btn-gestion' onclick='toggleMenuOpcionExtCap(this,"+index+")'>" +
							"Ver Opciones <i class='fa fa-fa-angle-double-down'></i>"+
						"</a>"+
						menuOpcion+
						"<i aria-hidden='true' class='fa fa-graduation-cap'></i> Capacitación: "+
						val.cursoNombre+" <br>" +
						"<i class='fa fa-calendar'></i>Fecha Realizada: "+val.fechaRealizadaTexto+"<br>"+
						"<i  class='fa fa-star'></i>Estado: <strong> "+val.evaluacionNombre+"</strong> (" +
						pasarDecimalPorcentaje(val.puntajeAlcanzado)+")<br>" +
						"<a class='efectoLink'  onclick='verConfirmacionesEvaluacion("+index+")'>" +
							"Ver Extensión  ("+val.confirmacionEvaluacionTotal+") <i class='fa fa fa-angle-double-down'></i>"+
						"</a>"+
					"</div>" +
					"<div class='opcionesAccion'>"+//textoBotonAgregar+textoBotonConfirm+
					"</div>" +
					"<div class='subOpcionAccion'></div>" +
				"</div>");
		//
			$("#tblCursoProy tbody").append("<tr >" +
				"<td>" +val.cursoNombre+"</td>"+
				"<td>" +val.fechaRealizadaTexto+"</td>"+
				"<td><strong>"+val.evaluacionNombre+"</strong> (" +
				pasarDecimalPorcentaje(val.puntajeAlcanzado)+")</td>"+
				"<td>" +val.confirmacionEvaluacionTotal+textoBotonConfirm+ "</td>"+
				"</tr>");
	});  
	$(".detalleAccion ul").hide(); 
	});
}
//Inicio de incidentes del proyecto (vista contratista)
var funcionalidadIncidente=[
	{id:"opcAgregar", nombre:"<i class='fa fa-plus-square'></i> Agregar acción de mejora",functionClick:function(data){nuevaAccionIncidente(indexIncidente);}}
	];
function marcarSubOpcionIncidente(pindex){
	funcionalidadIncidente[pindex].functionClick();
	$(".detalleAccion ul").hide(); 
}
var indexIncidente;
function toggleMenuOpcionIncidente(obj,pindex){ 
	indexIncidente=pindex;
	$(obj).parent(".detalleAccion").find("ul").toggle();
	$(obj).parent(".detalleAccion").siblings().find("ul").hide(); 
}
function verCompletarIncidenteProyecto(pindex){
	pindex=defaultFor(pindex,indexPostulante);
	indexPostulante=pindex;
	//
	 
	$("#proyectoTable"+listFullProyectoSeguridad[pindex].id).parent("#tituloEvento")
	.parent(".eventoGeneral").parent(".contenidoSubList")
	.parent(".divProyectoGeneral").siblings()
	.find(".detalleIncidentesProyecto").hide();
	
	$("#proyectoTable"+listFullProyectoSeguridad[pindex].id)
	.find(".detalleIncidentesProyecto").toggle() 
	//
	
	
	
	var proyectoObj1 = {
			id : listFullProyectoSeguridad[pindex].id
	};
	
	callAjaxPost(URL + '/contratista/proyecto/incidentes', proyectoObj1,
			function(data){
		detalleIncidenteFull = data.list;
		$("#proyectoTable"+listFullProyectoSeguridad[pindex].id)
		.find(".detalleIncidentesProyecto").html("<td colspan='3'></td>");
		
		var numAprob=0;var numTotal=0;
		detalleIncidenteFull.forEach(function(val1,index1){
			var textoBotonAct="<button class='btn btn-success' style='padding:5px' onclick='verCompletarAccioneIncidente("+index1+")'>" +
			"<i class='fa fa-external-link-square' aria-hidden='true'></i></button>";
			var iconoDescarga=
				"<a href='"+URL+"/contratista/proyecto/incidente/evidencia?id="+val1.accidenteId+"' target='_blank'>" +
						"<i class='fa fa-download fa-2x' aria-hidden='true'></i></a>";
			if(val1.evidenciaNombre=="----"){
				iconoDescarga="";
			}
			 var menuOpcion="<ul class='list-group listaGestionGosst' >";
				funcionalidadIncidente.forEach(function(val2,index2){
					menuOpcion+="<li class='list-group-item' onclick='marcarSubOpcionIncidente("+index2+")'>"+val2.nombre+" </li>"
			});
			menuOpcion+="</ul>"
			var claseNotaDocumento="gosst-neutral";numTotal++;
			if(val1.evaluacion.id == 1){
				claseNotaDocumento="gosst-aprobado";numAprob++;
			}
			$("#proyectoTable"+listFullProyectoSeguridad[pindex].id)
			.find(".detalleIncidentesProyecto td")
			.append("<div id='divMovilIncidente"+val1.accidenteId+"'>" +
						"<div class='detalleAccion "+claseNotaDocumento+"'> " +
							"<a class='efectoLink btn-gestion' onclick='toggleMenuOpcionIncidente(this,"+index1+")'>" +
								"Ver Opciones <i class='fa fa-fa-angle-double-down'></i>"+
							"</a>"+
							menuOpcion+
							"<strong> INCIDENTE "+(index1+1)+"</strong><br>"+ 
							"<i class='fa fa-user'></i> Trabajador Afectado: "+
							(val1.trabajadorContratista.nombre==""?"No hubo afectados"
									:
									val1.trabajadorContratista.nombre)+"<br>" +
							"<i class='fa fa-calendar'></i> Fecha de Registro: "+val1.fecha+"<br>"+
						 	"<i class='fa fa-calendar'></i> Tipo de Incidente: "+val1.accidenteTipoNombre+"<br>"+
						 	"<i class='fa fa-info'></i> Descripción: "+val1.accidenteDescripcion+" "+
						 	(val1.accidenteTipoId==1?
									val1.accidenteClasificacionNombre+""+
									(val1.accidenteClasificacionId==2?" / "+val1.accidenteSubClasificacionNombre:"")
									:
							"")+"<br>"+
							"<i class='fa fa-medkit'></i><strong>Días perdidos</strong>  "+val1.diasPerdidos+"<br>"+
							"<i class='fa fa-search'></i><strong>Estado de Investigación: </strong>"+val1.accidenteInformeNombre+"<br>"+
							"<i aria-hidden='true' class='fa fa-download'></i> Evidencia: "+
								(val1.evidenciaNombre==""?"Sin Registro"
										:
									"<a class='efectoLink' href='"+URL+"/contratista/proyecto/incidente/evidencia?id="+val1.accidenteId+"' >" +
										val1.evidenciaNombre+
									"</a>")+ "<br>" +
							"<a class='efectoLink' onclick='verCompletarAccioneIncidente("+index1+")'>" +
								"Ver Acción de Mejora ("+val1.indicadorAcciones+") <i class='fa fa-angle-double-down'></i>" +
							"</a> "+
						"</div>" +
						"<div class='opcionesAccion'>" +
						"</div>" +
					"<div class='subOpcionAccion'></div>");
			//
		});
		$("#proyectoTable"+listFullProyectoSeguridad[pindex].id).find(".celdaIndicadorProyecto").html(numAprob+" / "+numTotal);
		$(".detalleAccion ul").hide(); 
	});
}
//FIn.
var funcionalidadObs=[
	{id:"opcAgregar", nombre:"<i class='fa fa-plus-square'></i> Agregar acción de mejora",functionClick:function(data){nuevaAccionAuditoria(indexObs);}}
	];
function marcarSubOpcionObs(pindex){
	funcionalidadObs[pindex].functionClick();
	$(".detalleAccion ul").hide(); 
}
var indexObs;
function toggleMenuOpcionObs(obj,pindex){ 
	indexObs=pindex;
	$(obj).parent(".detalleAccion").find("ul").toggle();
	$(obj).parent(".detalleAccion").siblings().find("ul").hide(); 
}
function verCompletarObsProyecto(pindex){
	pindex=defaultFor(pindex,indexPostulante);
	// 
	$("#proyectoTable"+listFullProyectoSeguridad[pindex].id).parent("#tituloEvento")
	.parent(".eventoGeneral").parent(".contenidoSubList")
	.parent(".divProyectoGeneral").siblings()
	.find(".detalleObservacionesProyecto").hide();
	
	$("#proyectoTable"+listFullProyectoSeguridad[pindex].id)
	.find(".detalleObservacionesProyecto").toggle() 
	//
	var proyectoObj1 = {
			id : listFullProyectoSeguridad[pindex].id
	};
	indexPostulante=pindex;
	callAjaxPost(URL + '/contratista/proyecto/observaciones', proyectoObj1,
			function(data){
		var list = data.list;
		detalleObservacionFull=list;
		$("#proyectoTable"+listFullProyectoSeguridad[pindex].id)
		.find(".detalleObservacionesProyecto").html("<td colspan='3'></td>");

		list.forEach(function(val1,index1){
			var claseNotaDocumento="gosst-neutral";
			if(val1.nota == null){
				val.nota ={id:3}
			}
			if(val1.nota.id == 1){
				claseNotaDocumento="gosst-aprobado"
			}
			var textAcciones=/*"<i aria-hidden='true' class='fa fa-minus'></i>"+
								"<a onclick='nuevaAccionAuditoria("+index1+")'>Agregar acción mejora </a>" +
								"<i aria-hidden='true' class='fa fa-minus'></i>" +
								"<a onclick='verCompletarAccioneObservacion("+index1+")'> Ver ("+val1.indicadorAcciones+")</a>"+
								*/"</div>" +
								"<div class='subOpcionAccion'></div>";
			if(val1.reporte.id==3){
				textAcciones="";
			}
			 var menuOpcion="<ul class='list-group listaGestionGosst' >";
			funcionalidadObs.forEach(function(val2,index2){
				menuOpcion+="<li class='list-group-item' onclick='marcarSubOpcionObs("+index2+")'>"+val2.nombre+" </li>"
			});
			menuOpcion+="</ul>"
			$("#proyectoTable"+listFullProyectoSeguridad[pindex].id)
			.find(".detalleObservacionesProyecto td")
			.append("<div id='divMovilObservacion"+val1.id+"'>" +
					"<div class='detalleAccion "+claseNotaDocumento+"'> " +
						"<a class='efectoLink btn-gestion' onclick='toggleMenuOpcionObs(this,"+index1+")'>" +
							"Ver Opciones <i class='fa fa-fa-angle-double-down'></i>"+
						"</a>"+
						menuOpcion+
						"<strong>OBSERVACIÓN "+(index1+1)+"</strong><br>"+
						"<i aria-hidden='true' class='fa fa-eye'></i> Tipo de Observacion: " +
						"<strong>"+
						val1.tipo.nombre+"</strong><br>"+
						"<i aria-hidden='true' class='fa fa-list-ul'></i> Tipo de Reporte: "+
						val1.reporte.nombre+"<br>"+
						"<i aria-hidden='true' class='fa fa-lock'></i> Factor de Seguridad: "+
						val1.factor.nombre+"<br>"+
						"<i aria-hidden='true' class='fa fa-file-text-o'></i> Descripcion: "+
						val1.descripcion+"<br>" +
						(val1.nivel==1?
								"<i class='fa fa-star-half-o'></i> Estado: Proceso<br>"
								:
								"<i class='fa fa-star'></i> Estado: Cerrado<br>")+
						"<i aria-hidden='true' class='fa fa-calendar'></i> Fecha del Reporte: "+val1.fechaTexto+"<br>"+
						(val1.evidenciaNombre=="----"?
								"<i class='fa fa-download'></i>Evidencia: Sin Registro"
								:
								"<i class='fa fa-download'></i> Evidencia: "+
								"<a class='efectoLink' href='"+URL+"/contratista/observacion/evidencia?observacionId="+val1.id+" ' >" +
									val1.evidenciaNombre+
								"</a>" )+"<br>"+ 
								"<a class='efectoLink' onclick='verCompletarAccioneObservacion("+index1+")'>" +
									"Ver Acción de Mejora ("+val1.indicadorAcciones+") <i class='fa fa-angle-double-down'></i>" +
								"</a>"+
					"</div>" +
				"<div class='opcionesAccion'>" +
				//"<a target='_blank' href='"+URL+"/contratista/observacion/evidencia?observacionId="+val1.id+" ' >Descargar</a>" +
					textAcciones
				);
			//
			
			var textoBotonAct="<button class='btn btn-success' style='padding:5px' onclick='verCompletarAccioneObservacion("+index1+")'>" +
			"<i class='fa fa-external-link-square' aria-hidden='true'></i></button>";
			$("#tblObservacion tbody").append(
					"<tr id='trobs" + val1.id +"' >"
							+ "<td id='desc"
							+ val1.id + "'>"
							+val1.descripcion +"</td>"
							+ "<td id='fec"
							+ val1.id + "'>"
							+val1.fechaTexto +"</td>"
							+ "<td id='tipre"
							+ val1.id + "'>"
							+val1.reporte.nombre +"</td>"
							+ "<td id='factor"
							+ val1.id + "'>"
							+val1.factor.nombre +"</td>"
							+ "<td id='pot"
							+ val1.id + "'>"
							+val1.perdida.nombre +"</td>"
							+ "<td id='tdevi"
							+ val1.id
							+ "'>"+val1.evidenciaNombre+"</td>"  
							+ "<td id='tdacciones"
							+ val1.id
							+ "'>"+val1.indicadorAcciones+textoBotonAct+"</td>" 
							+ "<td id='estObs"
							+ val1.id + "'>"
						  +"</td>"
							+"</tr>");
		});
		$(".detalleAccion ul").hide(); 
	});

}
function hallarEstadoImplementacionAccAuditoria(){
	var fechaPlanificada=$("#inputFecAcc").val();
	var fechaReal=convertirFechaInput(objAccAuditoria.fechaReal);
	var fechaHoy=obtenerFechaActual();
	if(fechaReal!=''){
		objAccAuditoria.accionEstado=2;
		objAccAuditoria.accionEstadoNombre="COMPLETADO";
	}else{
		
		if(fechaPlanificada!=""){
			var dif=restaFechas(fechaHoy,fechaPlanificada)
			if(dif<0){
				objAccAuditoria.accionEstado=3;
				objAccAuditoria.accionEstadoNombre="RETRASADO";
			}else{
				objAccAuditoria.accionEstado=1;
				objAccAuditoria.accionEstadoNombre="EN PROCESO";
			}
			
		}else{
			
			objAccAuditoria.accionEstado=1;
			objAccAuditoria.accionEstadoNombre="EN PROCESO";
			
		}
	}
	
	$("#actpes"+objAccAuditoria.id).html(objAccAuditoria.accionEstadoNombre);
}
function volverDivIncidentes(){
	
	$("#tabProyectos .container-fluid").hide();
	$("#divCompletarIncidenteProyecto").show();
	$("#tabProyectos h4")
	.html("<a onclick='volverDivProyectos()'>Proyecto ("
			+listFullProyectoSeguridad[indexPostulante].titulo+") </a> > Incidentes Relacionados");
	verCompletarIncidenteProyecto();
}
function volverDivObservaciones(){
	$("#tabProyectos .container-fluid").hide();
	$("#divCompletarObservacionProyecto").show();
	$("#tabProyectos h4")
	.html("<a onclick='volverDivProyectos()'>Proyecto ("
			+listFullProyectoSeguridad[indexPostulante].titulo+") </a> > Observaciones Encontradas");
	verCompletarObsProyecto();
}
function volverDivAuditoriaGeneral(){
	$("#tabProyectos .container-fluid").hide();
	$("#divCompletarAuditoriaProyecto").show();
	$("#tabProyectos h4")
	.html("<a onclick='volverDivProyectos()'>Proyecto ("
			+listFullProyectoSeguridad[indexPostulante].titulo+") </a> > Resultados Auditoría");
	verCompletarAuditoriaGenral();
}
var listFullAuditoriaGeneral=[];
var objAuditoriaGeneral={};
function volverDivAuditoria(){
	$("#tabProyectos .container-fluid").hide();
	$("#divCompletarAuditoriaProyecto").show();
	$("#tabProyectos h4")
	.html("<a onclick='volverDivProyectos()'>Proyecto ("
			+listFullProyectoSeguridad[indexPostulante].titulo+") </a> " 
			+"> <a onclick='volverDivAuditoriaGeneral()'>Resultados Auditoría</a>"
			+" > Evaluaciones");
	verCompletarEvaluacionesAuditoria(objAuditoriaGeneral.index);
}
function verCompletarInspeccionesGenral(pindex){
pindex=defaultFor(pindex,indexPostulante);
	
	// 
	$("#proyectoTable"+listFullProyectoSeguridad[pindex].id).parent("#tituloEvento")
	.parent(".eventoGeneral").parent(".contenidoSubList")
	.parent(".divProyectoGeneral").siblings()
	.find(".detalleInspeccionProyecto").hide();
	
	$("#proyectoTable"+listFullProyectoSeguridad[pindex].id)
	.find(".detalleInspeccionProyecto").toggle() 
	//  
	var proyectoObj1 = {
			idCompany : getSession("gestopcompanyid"),
			auditoriaClasifId : 2,estadoImplementacion : 1,
			proyectoId : listFullProyectoSeguridad[pindex].id
	};
	indexPostulante=pindex;
	callAjaxPost(URL + '/auditoria/contratistas', proyectoObj1,
			function(data){
		var list = data.list;
		var numRpta=data.listRpta;
		listFullAuditoriaGeneral=list;
		$("#proyectoTable"+listFullProyectoSeguridad[pindex].id)
		.find(".detalleInspeccionProyecto").html("<td colspan='3'></td>"); 
		list.forEach(function(val1,index1){ 
			
			var claseNotaDocumento="gosst-neutral";
			if(val1.evaluacion.id == 1){
				claseNotaDocumento = "gosst-aprobado"
			}
			var opciones="";
			var textoIndicador="";
			 
				opciones="<a onclick='verCompletarHallazgosContratista("+index1+")'> Ver Hallazgos ("+val1.numHallazgos+") </a> ";
				textoIndicador=val1.auditoriaNombre; 
			$("#proyectoTable"+listFullProyectoSeguridad[pindex].id)
			.find(".detalleInspeccionProyecto td")
			.append("<div id='divMovilAuditoria"+val1.auditoriaId+"'>" +
						"<div class='detalleAccion "+claseNotaDocumento+"' > " + 
							"<strong>INSPECCIÓN "+(index1+1)+"</strong><br>"+
							(false=="---"?"":
								"<a class='efectoLink' href='"+URL+"/contratista/auditoria/informe?auditoriaId="+val1.auditoriaId+"'  target='_blank'> "+
							"<i class='fa fa-download'></i> Descargar informe " +
							""+
						"</a><br>")+
						"<i class='fa fa-info'></i><strong>Código Informe:</strong> "+ val1.nroCaso+"<br>"+
							"<i class='fa fa-calendar'></i><strong>Fecha Planificada:</strong> "+ val1.fechaPlanificadaNombre+"<br>"+
							"<i aria-hidden='true' class='fa fa-info'></i><strong>Descripcion:</strong> "+
							textoIndicador+ "<br>" +
							"<i class='fa fa-search'></i><strong>Tipo de Inspeccion:</strong> "+val1.auditoriaTipoNombre+ "<br>" +
							"<a class='efectoLink' onclick='verCompletarHallazgosContratista("+index1+")'> " +
									"Ver Hallazgos ("+val1.numHallazgos+")" +
								"<i class='fa fa-angle-double-down'></i>"+
							"</a><br>"+
						"</div>" +
						"<div class='opcionesAccion'>" + 
						//opciones+
						"</div>" +
					"<div class='subOpcionAccion'></div>")
		});
	});
}
function verCompletarHallazgosContratista(pindex){
	pindex=defaultFor(pindex,objAuditoriaGeneral.index);
	var val1=listFullAuditoriaGeneral[pindex];
	objAuditoriaGeneral.index=pindex;
	$(".subOpcionAccion").hide();
	$(".subOpcionAccion").html("");
			var dataParam = {
					auditoriaId : val1.auditoriaId,
					auditoriaTipo : val1.auditoriaTipo
				};
			detalleAuditoriaFull=[]
				callAjaxPost(URL + '/auditoria/hallazgos', dataParam,
						function(data1){
					if(data1.CODE_RESPONSE=="05"){
					detalleHallazgoFull=data1.list;
					var bodyText=""; 
					detalleHallazgoFull.forEach(function(val2,index2){
						 
						var btnOpcion="<button class='btn btn-success' onclick='toggleMenuOpcionHallazgo(this,"+index2+")'>" +
									"<i class='fa  fa-caret-down' aria-hidden='true'></i></button>"; 
						 
						var textEvi=" ";
						var btnDescarga="<a target='_blank' href='"+URL+"/auditoria/hallazgo/evidencia?hallazgoId="+val2.id+"'>" +
								"<i class='fa fa-download'></i>Descargar</a><i class='fa fa-minus'></i>";
						if(val2.evidenciaNombre=="----"
							|| val2.evidenciaNombre==""){
							textEvi="";btnDescarga="";
						} 
						var aprobado="gosst-neutral"	 
						$("#proyectoTable"+listFullProyectoSeguridad[indexPostulante].id)
						.find("#divMovilAuditoria"+listFullAuditoriaGeneral[pindex].auditoriaId+" .subOpcionAccion").show()
						.append("<div id='subDivMovilDetalleAudi"+val2.id+"'>" +
									"<div class='subDetalleAccion  "+aprobado+"'   >" +
										"<strong>HALLAZGO "+(index2+1)+"</strong><br>"+
										"<strong><i class='fa fa-info' aria-hidden='true'></i> Descripción: </strong>" +val2.descripcion+ "<br>"+
										"<strong><i class='fa fa-home' aria-hidden='true'></i> Lugar específico:</strong> " +val2.lugar+ "<br>"+
										"<i class='fa fa-download'></i> Evidencia: "+
										(val2.evidenciaNombre=="----"?
										"Sin Registro":
										"<a class='efectoLink'  href='"+URL+"/auditoria/hallazgo/evidencia?hallazgoId="+val2.id+"'>" +
											val2.evidenciaNombre+
										"</a>")+ "<br>"+ 
										"<a class='efectoLink' onclick='verCompletarAccioneHallazgoContratista("+index2+")'>" +
											"Ver Acción de Mejora  ("+val2.numAcciones+") <i class='fa fa-angle-double-down'></i>"+
										"</a>"+
									"</div> " +
									"<div class='opcionesDetalleAccion'>" +
										//btnDescarga+
										//"<a onclick='verCompletarAccioneHallazgoContratista("+index2+")'> Ver ("+val2.numAcciones+")</a>" +
										//"<i class='fa fa-minus'></i>" +
										"<a style='color:#2e9e8f;font-weight: 800;'onclick='nuevaAccionHallazgoAuditoria("+index2+")'>  Agregar Acción de Mejora </a>" +
										"<div class='subOpcionDetalleAccion'>" + 
										"</div>"+
									"</div>" +
								"</div>" );
					});
					$(".subDetalleAccion ul").hide();  
					}else{
						alert("No tiene acceso a esta funcionalidad")
					}
				});
}
function verCompletarAuditoriaGenral(pindex){
	pindex=defaultFor(pindex,indexPostulante);
	
	// 
	$("#proyectoTable"+listFullProyectoSeguridad[pindex].id).parent("#tituloEvento")
	.parent(".eventoGeneral").parent(".contenidoSubList")
	.parent(".divProyectoGeneral").siblings()
	.find(".detalleAuditoriasProyecto").hide();
	
	$("#proyectoTable"+listFullProyectoSeguridad[pindex].id)
	.find(".detalleAuditoriasProyecto").toggle() 
	// 
	var auditoriaClasifProy=3;	 
	var proyectoObj1 = {
			idCompany : getSession("gestopcompanyid"),
			auditoriaClasifId : auditoriaClasifProy,estadoImplementacion : 1,
			proyectoId : listFullProyectoSeguridad[pindex].id
	};
	indexPostulante=pindex;
	callAjaxPost(URL + '/auditoria/contratistas', proyectoObj1,
			function(data){
		var list = data.list;
		var numRpta=data.listRpta;
		listFullAuditoriaGeneral=list;
		$("#proyectoTable"+listFullProyectoSeguridad[pindex].id)
		.find(".detalleAuditoriasProyecto").html("<td colspan='3'></td>");
		
		$("#tblAuditoriasGeneral tbody tr").remove();
		list.forEach(function(val1,index1){
			var rptaSi=numRpta[index1].rptaSi;
			var rptaParc=numRpta[index1].rptaParcial;
			var rptaNo=numRpta[index1].rptaNo;
			var rptaRP=numRpta[index1].rptaRP;
			var rptaFaltante=numRpta[index1].rptaRespondida;
			var puntaje=rptaSi*1+rptaParc*0.5+rptaRP*0.75+rptaNo*0;
			var puntajeMax=rptaSi+rptaParc+rptaRP+rptaNo;
			var pregTotal=val1.numPreguntas;
			var nivelAvance2=rptaSi+"/"+puntajeMax; 
			
			var claseNotaDocumento="gosst-neutral";
			if(val1.evaluacion.id == 1){
				claseNotaDocumento = "gosst-aprobado";
			}
			var textColor="style='border-color:"+colorPuntajeGosst(puntaje/puntajeMax)+" !important'";
			var opciones="<a onclick='verCompletarEvaluacionesAuditoria("+index1+")'> Ver Detalle ("+nivelAvance2+")</a>";
			var textoIndicador=pasarDecimalPorcentaje(puntaje/puntajeMax,2);
			
			$("#proyectoTable"+listFullProyectoSeguridad[pindex].id)
			.find(".detalleAuditoriasProyecto td")
			.append("<div id='divMovilAuditoria"+val1.auditoriaId+"'>" +
					"<div class='detalleAccion "+claseNotaDocumento+"' > " + 
						"<strong>AUDITORIA "+(index1+1)+"</strong><br>"+
						(val1.evidenciaNombre=="---"?"":"<a class='efectoLink' href='"+URL+"/auditoria/evidencia?auditoriaId="+val1.auditoriaId+"'  target='_blank'> "  +
						"<i class='fa fa-download'></i> Descargar informe " +
						""+
					"</a><br>")+
					
						"<i class='fa fa-info'></i><strong>Código Informe:</strong> "+ val1.nroCaso+"<br>"+
						"<i aria-hidden='true' class='fa fa-thumbs-o-up'></i>Conformidad: "+
						textoIndicador+ "<br>" +
						"<i aria-hidden='true' class='fa fa-info'></i>Descripcion:"+
						val1.auditoriaTipoNombre+ "<br>" +
						"<i aria-hidden='true' class='fa fa-calendar'></i>Fecha: "+val1.fechaPlanificadaNombre+"<br>"+
						"<a class='efectoLink'  onclick='verCompletarEvaluacionesAuditoria("+index1+");'>" +
							"Ver Resultados ("+nivelAvance2+") <i class='fa fa-angle-double-down'></i>" +
						"</a>"	+
					"</div>" +
				"<div class='subOpcionAccion'></div>");
		});
	});
	
}
function verCompletarAuditoriaColabGenral(pindex,pidAudi){
	pindex=defaultFor(pindex,indexPostulante);
	
	// 
	$("#proyectoTable"+listFullProyectoSeguridad[pindex].id).parent("#tituloEvento")
	.parent(".eventoGeneral").parent(".contenidoSubList")
	.parent(".divProyectoGeneral").siblings()
	.find(".detalleAuditoriasProyecto").hide();
	
	$("#proyectoTable"+listFullProyectoSeguridad[pindex].id)
	.find(".detalleAuditoriasProyecto").toggle() 
	// 
	var auditoriaClasifProy=3;
	var trabajadorAsociado=getSession("trabajadorGosstId");
	 
	var proyectoObj1 = {
			idCompany : getSession("gestopcompanyid"),
			auditoriaClasifId : auditoriaClasifProy,
			proyectoId : listFullProyectoSeguridad[pindex].id,
			trabajador:{trabajadorId:trabajadorAsociado}
	};
	indexPostulante=pindex;
	callAjaxPost(URL + '/auditoria/contratistas', proyectoObj1,
			function(data){
		var list = data.list;
		var numRpta=data.listRpta;
		listFullAuditoriaGeneral=list;
		$("#proyectoTable"+listFullProyectoSeguridad[pindex].id)
		.find(".detalleAuditoriasProyecto").html("<td colspan='3'></td>");
		$("#proyectoTable"+listFullProyectoSeguridad[pindex].id)
		.find(".detalleAuditoriasProyecto td")
		.append("<div  >" +
				"<div class='detalleAccion    > " + 
				"<i aria-hidden='true' class='fa fa-caret-right'></i>"+
				"Se le ha asignado realizar "+listFullAuditoriaGeneral.length+" auditoria(s)"+
				 "</div>"  
			  )
		$("#tblAuditoriasGeneral tbody tr").remove();
		list.forEach(function(val1,index1){
			var rptaSi=numRpta[index1].rptaSi;
			var rptaParc=numRpta[index1].rptaParcial;
			var rptaNo=numRpta[index1].rptaNo;
			var rptaRP=numRpta[index1].rptaRP;
			var rptaFaltante=numRpta[index1].rptaRespondida;
			var puntaje=rptaSi*1+rptaParc*0.5+rptaRP*0.75+rptaNo*0;
			var puntajeMax=rptaSi+rptaParc+rptaRP+rptaNo;
			var pregTotal=val1.numPreguntas;
			var nivelAvance2=rptaSi+"/"+pregTotal; 
			
			var claseNotaDocumento="gosst-neutral";
			
			var opciones="<a onclick='verCompletarEvaluacionesAuditoriaColab("+index1+")'> Ver Detalle ("+nivelAvance2+")</a>";
			var textoIndicador=pasarDecimalPorcentaje(puntaje/puntajeMax,2);
			console.log(val1);
			$("#proyectoTable"+listFullProyectoSeguridad[pindex].id)
			.find(".detalleAuditoriasProyecto td")
			.append("<div id='divMovilAuditoria"+val1.auditoriaId+"'>" +
					"<div class='detalleAccion "+claseNotaDocumento+"' style='border-color:"+colorPuntajeGosst(puntaje/puntajeMax)+" !important'> " + 
					"<i aria-hidden='true' class='fa fa-caret-right'></i>"+
					val1.auditoriaTipoNombre+ "<br>" +
					"<i aria-hidden='true' class='fa fa-calendar'></i>"+
					val1.fechaPlanificadaNombre+" "+val1.horaPlanificadaTexto+ "<br>" +
						"<i aria-hidden='true' class='fa fa-caret-right'></i>"+
						textoIndicador+ "<br>" +
						
						"<i aria-hidden='true' class='fa fa-info'></i> Acc. correctiva completadas / programadas : "+
						val1.accionesDirectasTotal+ "<br>" +
						
					 "</div>" +
					"<div class='opcionesAccion'>" + 
						opciones+
					"</div>" +
					"<div class='subOpcionAccion'></div>")
		});
		if(parseInt(pidAudi) > 0){
			list.forEach(function(val,index){
				if(val.auditoriaId == parseInt(pidAudi)){
					verCompletarEvaluacionesAuditoriaColab(index,1);
				}
			});
		}
	});
	
}
function verCompletarInspeccionColabGenral(pindex,pidAux){
	pindex=defaultFor(pindex,indexPostulante);
	
	// 
	$("#proyectoTable"+listFullProyectoSeguridad[pindex].id).parent("#tituloEvento")
	.parent(".eventoGeneral").parent(".contenidoSubList")
	.parent(".divProyectoGeneral").siblings()
	.find(".detalleInspeccionProyecto").hide();
	
	$("#proyectoTable"+listFullProyectoSeguridad[pindex].id)
	.find(".detalleInspeccionProyecto").toggle() 
	// 
	var auditoriaClasifProy=2;
	var trabajadorAsociado=getSession("trabajadorGosstId");
	var proyectoObj1 = {
			idCompany : getSession("gestopcompanyid"),
			auditoriaClasifId : auditoriaClasifProy,
			proyectoId : listFullProyectoSeguridad[pindex].id,
			trabajador:{trabajadorId:trabajadorAsociado}
	};
	indexPostulante=pindex;
	callAjaxPost(URL + '/auditoria/contratistas', proyectoObj1,
			function(data){
		listFullAuditoriaGeneral=data.list;
		$("#proyectoTable"+listFullProyectoSeguridad[pindex].id)
		.find(".detalleInspeccionProyecto").html("<td colspan='3'></td>");
		$("#proyectoTable"+listFullProyectoSeguridad[pindex].id)
		.find(".detalleInspeccionProyecto td")
		.append("<div  >" +
				"<div class='detalleAccion    > " + 
				"<i aria-hidden='true' class='fa fa-caret-right'></i>"+
				"Se le ha asignado realizar "+listFullAuditoriaGeneral.length+" inspeccion(es)"+
				 "</div>"  
			  )
		listFullAuditoriaGeneral.forEach(function(val1,index1){	
			var claseNotaDocumento="gosst-neutral";
			
			var opciones=" ";
			var textoIndicador="";
				opciones="<a onclick='verCompletarHallazgos("+index1+")'> Ver Hallazgos ("+val1.numHallazgos+") </a>" +
						"<i class='fa fa-minus' ></i>" +
						"<a onclick='agregarHallazgoInspeccion("+index1+")'> Agregar </a>";
				textoIndicador=val1.auditoriaNombre;
			 
			$("#proyectoTable"+listFullProyectoSeguridad[pindex].id)
			.find(".detalleInspeccionProyecto td")
			.append("<div id='divMovilAuditoria"+val1.auditoriaId+"'>" +
					"<div class='detalleAccion "+claseNotaDocumento+"'    > " + 
					"<i aria-hidden='true' class='fa fa-caret-right'></i>"+
					textoIndicador+ "<br>" +
					"<i aria-hidden='true' class='fa fa-caret-right'></i>"+
					val1.auditoriaTipoNombre+ "<br>" +
					 val1.fechaPlanificadaNombre+"</div>" +
				"<div class='opcionesAccion'>" + 
				opciones+
						"</div>" +
						"<div class='subOpcionAccion'></div>")
		});
		if(parseInt(pidAux)>0){
			listFullAuditoriaGeneral.forEach(function(val1,index1){	
				if(val1.auditoriaId==parseInt(pidAux)){
					agregarHallazgoInspeccion(index1);
				}
			})
		}
		
	});
	
}
function verResumenAccionesAuditoria(pindex){
	var val1=listFullAuditoriaGeneral[pindex];
	pindex=defaultFor(pindex,objAuditoriaGeneral.index);
	$("#tabProyectos .container-fluid").hide();
	$("#divResumenAccionesAuditoria").show();
	objAuditoriaGeneral.index=pindex;
	$("#tabProyectos h4")
	.html("<a onclick='volverDivProyectos()'>Proyecto ("+
			listFullProyectoSeguridad[indexPostulante].titulo+") </a> " +
			"> <a onclick='verCompletarAuditoriaGenral("+indexPostulante+")'> Auditoría ("+val1.auditoriaTipoNombre+")</a> " +
			"> Resumen Acciones");
	
			var dataParam = {
					auditoriaId : val1.auditoriaId
				};
			detalleAuditoriaFull=[]
				callAjaxPost(URL + '/auditoria/verificaciones/acciones', dataParam,
						function(data1){
					var detail=data1.list;
					var bodyText="";
					$("#tblResumenAccion tbody tr").remove();
					detail.forEach(function(val2,index2){
						var iconoDescarga=
							"<a href='"+URL+"/gestionaccionmejora/accionmejora/evidencia?accionMejoraId="+val2.accionMejoraId+"'>" +
									"<i class='fa fa-download fa-2x' aria-hidden='true'></i></a>";
						if(val2.evidenciaNombre=="----"){
							iconoDescarga="";
						}
						 $("#tblResumenAccion tbody").append(
								 "<tr>" +
								 "<td >"+val2.descripcion+"</td>" +
									"<td >"+val2.fechaRevisionTexto+"</td>" +
									"<td >"+val2.fechaRealTexto+"</td>" +
									"<td >"+val2.observacion+"</td>" +
									"<td >"+val2.evidenciaNombre+iconoDescarga+"</td>" +
									"<td >"+val2.estadoCumplimientoNombre+"</td>" +
								 "" +
								 "</tr>");
						
					});
					
					
					
				});
}
//Resumen Incidentes
function volverVistaIncidentes(){
	verResumenIncidentesAuditoria();
}
var incidentesList=[];
var banderaEdicionEvaluacionAcc=false;
var listTipoEvaluacionGeneral=[];
function verResumenIncidentesAuditoria(){
	
	$("#tabProyectos .container-fluid").hide();
	$("#divResumenIncidentesProy").show();banderaEdicionEvaluacionAcc=false;
	$("#tabProyectos h2")
	.html("<a onclick='volverDivProyectos()'>Proyecto ("+
			proyectoObj.titulo+") </a> " +
			"> Resumen Incidentes");
			var dataParam = {
					id : proyectoObj.id
				};
				callAjaxPost(URL + '/contratista/proyecto/incidentes', dataParam,
						function(data1){
					incidentesList=data1.list;
					listTipoEvaluacionGeneral=data1.tipo_evaluacion
					var bodyText="";
					$("#tblResumenIncidentes tbody tr").remove();
					incidentesList.forEach(function(val2,index2){
						var iconoDescarga=
							"<a class='btn btn-success' " +
							"href='"+URL+"/contratista/proyecto/incidente/evidencia?id="+val2.accidenteId+"' " +
									"target='_blank'>" +
									"<i class='fa fa-download  ' aria-hidden='true'></i>Descargar</a>";
						if(val2.evidenciaNombre==""){
							iconoDescarga="";
						}
						var textoBotonAct=
							"<button class='btn btn-success' " +
							"style='padding:5px' onclick='verVistaAccioneIncidente("+index2+")'>" +
						"<i class='fa fa-external-link-square' aria-hidden='true'></i></button>";
						 var textEval="<button onclick='modificarEvaluacionAccidente("+index2+")' class='btn btn-success'>" +
								"Modificar evaluación <i class='fa fa-envelope-o'></i>";
						 var eviEval=
								"<a class='btn btn-success' " +
								"href='"+URL+"/contratista/proyecto/incidente/evaluacion/evidencia?id="+val2.accidenteId+"' " +
										"target='_blank'>" +
										"<i class='fa fa-download  ' aria-hidden='true'></i>Descargar</a>";
						 if(val2.evaluacion.evidenciaNombre==""){
							 eviEval="";
							}	 
							 
						 $("#tblResumenIncidentes tbody").append(
								 "<tr>" +
								 "<td >"+val2.accidenteTipoNombre+"</td>" +
									 "<td >"+val2.accidenteSubTipoNombre+"</td>" +
									 "<td >"+val2.accidenteDescripcion+"</td>" + 
									 "<td >"+val2.accidenteClasificacionNombre+"</td>" + 
									 "<td >"+val2.trabajadorContratista.nombre+"<br>"+val2.nombreAccidentadosInterno+"</td>" + 
									"<td >"+val2.fecha+"</td>" + 
									"<td >"+val2.evidenciaNombre+iconoDescarga+"</td>" + 
									 "<td  >"+val2.indicadorAcciones+textoBotonAct+"</td>" +
									 
									 "<td  id='acceval"+val2.accidenteId+"'>"+val2.evaluacion.icono+
									 val2.evaluacion.nombre+textEval+"</td>" +
									 "<td  id='accevalObs"+val2.accidenteId+"'>"+val2.evaluacion.observacion+"</td>" + 
									"<td  id='accevalEvi"+val2.accidenteId+"'>"+val2.evaluacion.evidenciaNombre+eviEval+"</td>" + 
										 
									 
									 
								 "</tr>");
						
					});
					
					
					
				});
} 
function modificarEvaluacionAccidente(pindex){
	 if(!banderaEdicionEvaluacionAcc){
		 banderaEdicionEvaluacionAcc=true;
	 var accObj=incidentesList[pindex];
	 var btnVolver="<button onclick='volverModificarEvaluacionAccidente("+pindex+")' class='btn btn-danger'>" +
		" <i class='fa fa-arrow-left'></i>" +
		"</button>";
	 var btnGuardar="<button onclick='guardarModificarEvaluacionAccidente("+pindex+")' class='btn btn-success'>" +
		" <i class='fa fa-floppy-o'></i>" +
		"</button>";
	 var selEvalAccidente=crearSelectOneMenuOblig("selEvalAccidente", "", listTipoEvaluacionGeneral, accObj.evaluacion.id, "id",
				"nombre");
	 $("#acceval"+accObj.accidenteId).html(selEvalAccidente+btnVolver+btnGuardar)
	 $("#accevalObs"+accObj.accidenteId)
	 .html("<input type='text' id='inputRespAccEval' class='form-control'> ");
	 var options=
		{container:"#accevalEvi"+accObj.accidenteId,
				functionCall:function(){ },
				descargaUrl: "/contratista/proyecto/incidente/evaluacion/evidencia?id="+accObj.accidenteId,
				esNuevo:false,
				idAux:"EvalAccidente",
				evidenciaNombre:accObj.evaluacion.evidenciaNombre};
		crearFormEvidenciaCompleta(options);
	 $("#inputRespAccEval").focus();
	 $("#inputRespAccEval").val(accObj.evaluacion.observacion);
	 }else{
		 alert("Hay una respuesta en edición")
	 }
}
function volverModificarEvaluacionAccidente(pindex){
	//var accObj=listFullAccionesAsociadas[pindex];
	verResumenIncidentesAuditoria();
}
function guardarModificarEvaluacionAccidente(pindex){
	 var accObj=incidentesList[pindex];
	 var accSend={accidenteId:accObj.accidenteId,
			 evaluacion:
				 {id:   $("#selEvalAccidente").val(),
					 observacion:$("#inputRespAccEval").val()
				 }
					 }
	 callAjaxPost(URL + '/contratista/proyecto/incidente/evaluacion/save', 
			 accSend, function(data){
		 guardarEvidenciaAuto(accObj.accidenteId,"fileEviEvalAccidente",bitsEvidenciaVerificacion,"/contratista/proyecto/incidente/evaluacion/evidencia/save",
				 verResumenIncidentesAuditoria )
		  
	 });  
}




var listFullAccionesAsociadas=[];
var banderaEdicionRespuestaAcc=false;
function verVistaAccioneIncidente(pindex){
	$("#tabProyectos .container-fluid").hide();
	$("#divAccionesIncidente").show();
	$("#tabProyectos h2")
	.html("<a onclick='volverDivProyectos()'> Proyecto ("+proyectoObj.titulo+") </a>" +
			" > <a onclick='volverVistaIncidentes()'> Incidente ("+incidentesList[pindex].accidenteTipoNombre+") </a>" +
					" > Acciones de Mejora");
var audiObj={accionMejoraTipoId : 1,
		accidenteId:incidentesList[pindex].accidenteId};
callAjaxPost(URL + '/gestionaccionmejora/accionmejora', 
			audiObj, function(data){
				if(data.CODE_RESPONSE=="05"){ 
					$("#tblAccIncidente tbody tr").remove();
					banderaEdicionRespuestaAcc=false;
					listFullAccionesAsociadas=data.list;
					listFullAccionesAsociadas.forEach(function(val,index){
						val.id=val.accionMejoraId;
						val.indexAccidente=pindex;
						var iconoDescarga=
							"<a href='"+URL+"/gestionaccionmejora/accionmejora/evidencia?accionMejoraId="+val.accionMejoraId+"'>" +
									"<i class='fa fa-download fa-2x' aria-hidden='true'></i></a>";
						if(val.evidenciaNombre=="----"){
							iconoDescarga="";
						}
						$("#tblAccIncidente tbody").append(
								"<tr id='trtip"+val.id+"'  >" +
//							
								"<td id='actpdesc"+val.id+"'>"+val.descripcion+"</td>" +
								"<td id='actpfp"+val.id+"'>"+val.fechaRevisionTexto+"</td>" +
								"<td id='actpfr"+val.id+"'>"+val.fechaRealTexto+"</td>" +
								"<td id='actpobs"+val.id+"'>"+val.observacion+"</td>" +
								"<td id='actpresp"+val.id+"'>"+val.respuesta+
								"<button onclick='modificarRespuestaAccionMejoraAccidente("+index+")' class='btn btn-success'>" +
								"Modificar respuesta <i class='fa fa-envelope-o'></i>" +
								"</button>"+"</td>" +
								"<td id='actpevi"+val.id+"'>"+val.evidenciaNombre+iconoDescarga+"</td>" +
								"<td id='actpes"+val.id+"'>"+val.estadoCumplimientoNombre+"</td>" +
								
								 
								"</tr>");
					});
				}else{
					console.log("NOPNPO")
				}
			});
}
function modificarRespuestaAccionMejoraAccidente(pindex){
	 if(!banderaEdicionRespuestaAcc){
		 banderaEdicionRespuestaAcc=true;
	 var accObj=listFullAccionesAsociadas[pindex];
	 var btnVolver="<button onclick='volverModificarRespuestaAccidente("+pindex+")' class='btn btn-danger'>" +
		" <i class='fa fa-arrow-left'></i>" +
		"</button>";
	 var btnGuardar="<button onclick='guardarModificarRespuestaAccidente("+pindex+")' class='btn btn-success'>" +
		" <i class='fa fa-floppy-o'></i>" +
		"</button>";
	 $("#actpresp"+accObj.id)
	 .html("<input type='text' id='inputRespAccASoc' class='form-control'> " +
	 		""+btnVolver+btnGuardar);
	 $("#inputRespAccASoc").focus();
	 $("#inputRespAccASoc").val(accObj.respuesta);
	 }else{
		 alert("Hay una respuesta en edición")
	 }
}
function volverModificarRespuestaAccidente(pindex){
	var accObj=listFullAccionesAsociadas[pindex];
	 verVistaAccioneIncidente(accObj.indexAccidente);
}
function guardarModificarRespuestaAccidente(pindex){
	 var accObj=listFullAccionesAsociadas[pindex];
	 var accSend={accionMejoraId:accObj.id,
			 respuesta:$("#inputRespAccASoc").val()}
	 callAjaxPost(URL + '/gestionaccionmejora/accionmejora/respuesta/save', 
			 accSend, function(data){
		 
		 verVistaAccioneIncidente(accObj.indexAccidente);
	 });  
}
function verCompletarEvaluacionesAuditoria(pindex){
	pindex=defaultFor(pindex,objAuditoriaGeneral.index);
	var val1=listFullAuditoriaGeneral[pindex];
	$("#tabProyectos .container-fluid").hide();
	$("#divCompletarAuditoriaProyecto").show();
	objAuditoriaGeneral.index=pindex;
	$("#tabProyectos h4")
	.html("<a onclick='volverDivProyectos()'>Proyecto ("+
			listFullProyectoSeguridad[indexPostulante].titulo+") </a> " +
			"> <a onclick='verCompletarAuditoriaGenral("+indexPostulante+")'> Auditoría ("+val1.auditoriaTipoNombre+")</a> " +
			"> Evaluaciones");
	$(".subOpcionAccion").html("");
	$(".subOpcionAccion").hide();
			var dataParam = {
					auditoriaId : val1.auditoriaId,
					auditoriaTipo : val1.auditoriaTipo
				};
			detalleAuditoriaFull=[]
				callAjaxPost(URL + '/auditoria/verificaciones', dataParam,
						function(data1){
					detalleAuditoriaFull=data1.list;
					var bodyText="";
					$("#tblAudi tbody tr").remove();
					data1.list.forEach(function(val2,index2){
						var menuOpcion="<ul class='list-group' >";
						funcionalidadesDetalleAudi.forEach(function(val,index){
							menuOpcion+="<li class='list-group-item' onclick='marcarSubOpcionDetalleAuditoria("+index+")'>"+val.nombre+" </li>"
						});
						menuOpcion+="</ul>";
						var btnOpcion="<a class='efectoLink btn-gestion' onclick='toggleMenuOpcionDetalleAudi(this,"+index2+")'>" +
										"Ver Opciones <i class='fa fa-angle-double-down' aria-hidden='true'></i>" +
									"</a>"; 
						var iconoestado;
						var resultadoText=(val2.bitSi==1?"Cumple":
							(val2.bitParc==1?"Cumple Parcialmente":
								val2.bitNo?"No Cumple":"")); 
						
						
						if(val2.bitSi==1)
						{
							iconoestado="fa-star";
							resultadoText="Cumple";
						}
						else if(val2.bitParc==1)
						{
							iconoestado="fa-star-half-o";
							resultadoText="Cumple Parcialmente";
						}
						else if(val2.bitNo)
						{
							iconoestado="fa-star-o";
							resultadoText="No Cumple";
						}
						var textEvi="";
						if(val2.evidenciaNombre=="----"){
							textEvi="Sin Registro" +
									"<br>";
						}
						else 
						{
							textEvi="<a class='efectoLink' href='"+URL+"/auditoria/verificaciones/evidencia?verificacionId="+
										val2.verificacionId+"&auditoriaId="+val1.auditoriaId+" '>" +
										val2.evidenciaNombre+ 
									"</a><br>";								
						}
						if(val2.detalleAuditoriaId>0 && val2.bitAplica==1 && val2.respuesta != null){
							var puntajeDetalle=(val2.bitParc*0.5+val2.bitSi*1+val2.bitNo*0)
							var colorAux=colorPuntajeGosst(puntajeDetalle);
						
						$("#proyectoTable"+listFullProyectoSeguridad[indexPostulante].id)
						.find("#divMovilAuditoria"+listFullAuditoriaGeneral[pindex].auditoriaId+" .subOpcionAccion").show()
						.append("<div id='subDivMovilDetalleAudi"+val2.detalleAuditoriaId+"'>" +
									"<div class='subDetalleAccion  gosst-neutral'  style='border-color:"+colorAux+" !important'>" +
										btnOpcion+ 
										menuOpcion+
										"<strong>RESULTADO "+(index2+1)+"</strong><br>"+
										"<i class='fa fa-info' aria-hidden='true'></i>Descripcion: " +val2.verificacion+" ("+val2.comentario+" )<br>"+
										"<i class='fa "+iconoestado+"'></i>Estado: <strong>" +resultadoText+ "</strong><br>"+
										"<i class='fa fa-download'></i> Evidencia: "+
										textEvi+
										"<a class='efectoLink' onclick='verCompletarAccioneAuditoria("+index2+");'>" +
											"Ver Acción de Mejora ("+val2.indicadorAcciones+")<i class='fa fa-angle-double-down' aria-hidden='true'></i>" +
										"</a>"+
									"</div> " +
									"<div class='opcionesDetalleAccion'>" +
										/*"<a target='_blank' href='"+URL+"/auditoria/verificaciones/evidencia?" +
												"verificacionId="+val2.verificacionId+"&auditoriaId="+val1.auditoriaId+" ' >Descargar" +
										"</a>" +
										"<i aria-hidden='true' class='fa fa-minus'></i>"+
										"<a onclick='nuevaAccionDetalleAuditoria("+index2+")'> Agregar acción  mejora</a>" +
										"<i aria-hidden='true' class='fa fa-minus'></i>" +
											"<a onclick='verCompletarAccioneAuditoria("+index2+")'> Ver  ("+val2.indicadorAcciones+")</a>" +*/
										"<div class='subOpcionDetalleAccion'>" + 
										"</div>"+
									"</div>" +
								"</div>" );
						}
					});
					$(".subDetalleAccion ul").hide(); 
				});
}
function verCompletarEvaluacionesAuditoriaColab(pindex,detalleIndex){
	pindex=defaultFor(pindex,objAuditoriaGeneral.index);
	var val1=listFullAuditoriaGeneral[pindex];
	objAuditoriaGeneral=val1;
	objAuditoriaGeneral.index=pindex;
	$(".subOpcionAccion").hide();
	$(".subOpcionAccion").html("");
			var dataParam = {
					auditoriaId : val1.auditoriaId,
					auditoriaTipo : val1.auditoriaTipo
				};
			detalleAuditoriaFull=[]
				callAjaxPost(URL + '/auditoria/verificaciones', dataParam,
						function(data1){
					detalleAuditoriaFull=data1.list;
					var bodyText="";
					detalleAuditoriaFull.forEach(function(val2,index2){
						var menuOpcion="<ul class='list-group' >";
						funcionalidadesDetalleAudi.forEach(function(val,index){
							menuOpcion+="<li class='list-group-item' onclick='marcarSubOpcionDetalleAuditoria("+index+")'>"+val.nombre+" </li>"
						});
						menuOpcion+="</ul>";
						var btnOpcion=
								"<a class='efectoLink' onclick='toggleMenuOpcionDetalleAudi(this,"+index2+")'>" +
									"Ver Opciones <i class='fa fa-angle-double-down' aria-hidden='true'></i>" +
								"</a>"; 
						var resultadoText=(val2.bitSi==1?"Cumple":
							(val2.bitParc==1?"Cumple Parcialmente":
								val2.bitNo?"No Cumple":"")); 
						var textEvi=" ";
						if(val2.evidenciaNombre=="----"){
							textEvi="";
						}
						if(val2.detalleAuditoriaId>0  ){
						
						}
							var btnAplica="<a onclick='cambiarAplicaEmpresaEvaluacionInspeccionColab("+index2+",1)'>" +
							"Aplica evaluación" +
							"</a>";
							var puntajeDetalle=(val2.bitParc*0.5+val2.bitSi*1+val2.bitNo*0)
							var colorAux="";
							if(val2.bitAplica==1 && val2.detalleAuditoriaId>0){
								btnAplica="<a onclick='cambiarAplicaEmpresaEvaluacionInspeccionColab("+index2+",0)'>" +
										"No aplica evaluación" +
										"</a>" +
										"<i class='fa fa-minus'></i>" +
										"<a onclick='iniciarEvaluacionAuditoriaColaborador("+index2+")'>Evaluar </a>";
								colorAux=colorPuntajeGosst(puntajeDetalle);
							}
							
						$("#proyectoTable"+listFullProyectoSeguridad[indexPostulante].id)
						.find("#divMovilAuditoria"+listFullAuditoriaGeneral[pindex].auditoriaId+" .subOpcionAccion").show()
						.append("<div id='subDivMovilDetalleAudi"+val2.detalleAuditoriaId+"'>" +
								"<div class='subDetalleAccion  gosst-neutral'  style='border-color:"+colorAux+" !important'>" +
								//btnOpcion+
								//menuOpcion+
								"<i class='fa fa-info' aria-hidden='true'></i>" +val2.verificacion+" ("+val2.comentario+" )<br>"+
								" <strong>" +resultadoText+ "</strong><br>"+
								"<i class='fa fa-file' aria-hidden='true'></i>" +val2.evidenciaNombre+ 
								"</div> " +
								"<div class='opcionesDetalleAccion'>" +
								"<a target='_blank' href='"+URL+"/auditoria/verificaciones/evidencia?" +
										"verificacionId="+val2.verificacionId+"&auditoriaId="+val1.auditoriaId+" ' >Descargar</a>" +
								"<i aria-hidden='true' class='fa fa-minus'></i>"+
									" "+btnAplica+"" +
									"<i aria-hidden='true' class='fa fa-minus'></i>" +
									"<a onclick='verCompletarAccioneAuditoriaColaborador("+index2+")'> Ver Acciones("+val2.indicadorAcciones+")</a>" +
									"<div class='subOpcionDetalleAccion'>" + 
									"</div>"+
								"</div>" +
									"</div>" );
					});
					if(parseInt(detalleIndex) > 0){
						iniciarEvaluacionAuditoriaColaborador(0);
					}
					$(".subDetalleAccion ul").hide();
			});
}
function iniciarEvaluacionAuditoriaColaborador(evalIndex){
	var contenidoVal=detalleAuditoriaFull[evalIndex];
	evaluacionInspeccionObj=contenidoVal;
	$(".divListPrincipal>div").hide();
	var listItemsEvalInsp=[
	                   		 {sugerencia:"",label:"Si"
	                   			 ,inputForm:"<input onclick='verAcordeRespuesta()' class='form-control' type='radio' " +
	                   			 		"name='rpta"+contenidoVal.verificacionId+"' " +
	                   			 				"id='boxRptaSi"+contenidoVal.verificacionId+"' value='1'> ",divContainer:"divPregSi"}, 
	                   		{sugerencia:""
	                   			,inputForm:"<input onclick='verAcordeRespuesta()' class='form-control' type='radio' " +
	                   					"name='rpta"+contenidoVal.verificacionId+"' " +
	                   							"id='boxRptaParc"+contenidoVal.verificacionId+"' value='0.5'> ",label:"Parcial" ,divContainer:"divPregParcial"},
	                   		{sugerencia:"",
	                   				inputForm:"<input onclick='verAcordeRespuesta()' class='form-control' " +
	                   						"type='radio' name='rpta"+contenidoVal.verificacionId+"' " +
	                   								"id='boxRptaNo"+contenidoVal.verificacionId+"' value='0'> ",label:"No",divContainer:"divPregNo"},
	                   						
	                   		{sugerencia:"",inputForm:"<input class='form-control' type='checkbox' id='checkPotencial"+contenidoVal.verificacionId+"'  > ",label:"¿Riesgo Potencial?",divContainer:"divPregPotecial"},
	                   		//{sugerencia:"" ,label:"Referencia Legal",inputForm:"<input class='form-control' id='inputRefLegalIns"+contenidoVal.verificacionId+"'> ",divContainer:""},
	                   		{sugerencia:"",label:"Comentario",inputForm:"<input class='form-control' id='inputComentarioInsp"+contenidoVal.verificacionId+"'> ",divContainer:""},
	                   		{sugerencia:""
	                   			,inputForm:" ",label:"Evidencia" ,divContainer:"divEvidenciaEvalIns"+contenidoVal.verificacionId},
                   			{sugerencia:""
	                   			,inputForm:"<button type='submit' class='btn btn-success' >" +
	                   					"<i aria-hidden='true' class='fa fa-paper-plane'></i>Guardar</button>",label:"" ,divContainer:""}
	                   		
	                   		 ];
	var textEval="";
	listItemsEvalInsp.forEach(function(val,index){
		textEval+=obtenerSubPanelModulo(val); 
	});
	
	var listEditarPrincipal=[ 
	{id:"editarMovilEvalAudiColab" ,clase:"contenidoFormVisible",
		nombre:"",
		contenido:"<form id='formEditarEvalAudiColab' class='eventoGeneral'>"+textEval+"</form>"}
	];
	$("#editarMovilEvalAudiColab").remove();
	agregarPanelesDivPrincipalColaborador(listEditarPrincipal);
	$("#editarMovilEvalAudiColab").find(".tituloSubList")
	.html(textoBotonVolverContenidoColaborador+"Evaluar "+contenidoVal.verificacion);
	
	$("#editarMovilEvalAudiColab").show();
	 $('#formEditarEvalAudiColab').on('submit', function(e) {
	        e.preventDefault();   
	        guardarEvaluacionAuditoriaProyectoColaborador();
	 });
	 
		$("#boxRptaSi"+contenidoVal.verificacionId).prop("checked",(contenidoVal.bitSi==1))
		$("#boxRptaParc"+contenidoVal.verificacionId).prop("checked",(contenidoVal.bitParc==1))
		$("#boxRptaNo"+contenidoVal.verificacionId).prop("checked",(contenidoVal.bitNo==1))
		$("#checkPotencial"+contenidoVal.verificacionId).prop("checked",(contenidoVal.bitRiesgo==1))
		$("#inputRefLegalIns"+contenidoVal.verificacionId).val(contenidoVal.referenciaLegal);
		$("#inputComentarioInsp"+contenidoVal.verificacionId).val(contenidoVal.comentario);
		var eviNombre=contenidoVal.evidenciaNombre;
		var options=
		{container:"#divEvidenciaEvalIns"+contenidoVal.verificacionId,
				functionCall:function(){ },
				descargaUrl: "/auditoria/verificaciones/evidencia?" +
						"auditoriaId="+objAuditoriaGeneral.auditoriaId+"&verificacionId"+contenidoVal.verificacionId,
				esNuevo:true,
				idAux:"EvalInspeccion"+contenidoVal.verificacionId,
				evidenciaNombre:eviNombre};
		crearFormEvidenciaCompleta(options);
}
function guardarEvaluacionAuditoriaProyectoColaborador(){
	var bitRespuesta= parseFloat($("input[name='rpta"+evaluacionInspeccionObj.verificacionId+"']:checked").val());
	var dataParam = {
			verificacionId : evaluacionInspeccionObj.verificacionId,
			bitAplica : 1,
			bitRiesgo : ($("#checkPotencial"+evaluacionInspeccionObj.verificacionId).prop("checked")?1:0),
			bitRespuesta : bitRespuesta,
			detalleAuditoriaId:evaluacionInspeccionObj.detalleAuditoriaId,
			referenciaLegal :$("#inputRefLegalIns"+evaluacionInspeccionObj.verificacionId).val(),
			comentario :$("#inputComentarioInsp"+evaluacionInspeccionObj.verificacionId).val(),
			auditoriaId : objAuditoriaGeneral.auditoriaId
		}; 
		callAjaxPost(URL + '/auditoria/verificaciones/respuesta/save', dataParam,
				function(data){
			guardarEvidenciaAuto(data.nuevoId,"fileEviEvalInspeccion"+evaluacionInspeccionObj.verificacionId
					,bitsEvidenciaVerificacion,"/auditoria/verificaciones/evidencia/save",
					function(data){
				volverDivSubContenido();
				//verCompletarAuditoriaColabGenral();
				verCompletarEvaluacionesAuditoriaColab();
			} ,"verificacionId");
			 
		});
}
function cambiarAplicaEmpresaEvaluacionInspeccionColab(indexEvalIns,isAplica){
	var r=true;
	if(isAplica==0){
		r=confirm("¿está seguro que esta evaluación no aplica?")
	}
if(r){
	var evaluacionInspeccionColabObj=detalleAuditoriaFull[indexEvalIns];
	var dataParam = {
			verificacionId : evaluacionInspeccionColabObj.verificacionId,
			bitAplica : isAplica,
			bitRiesgo : evaluacionInspeccionColabObj.bitRiesgo,
			bitRespuesta : evaluacionInspeccionColabObj.bitRespuesta,
			
			referenciaLegal :evaluacionInspeccionColabObj.referenciaLegal,
			comentario : evaluacionInspeccionColabObj.comentario,
			auditoriaId : objAuditoriaGeneral.auditoriaId
		}; 
		callAjaxPost(URL + '/auditoria/verificaciones/respuesta/save', dataParam,
				function(data){
			verCompletarEvaluacionesAuditoriaColab();
		});
}
}
function verCompletarAccioneAuditoriaColaborador(pindex){
	pindex=defaultFor(pindex,objAuditoria.index);
	//
	 $(".subOpcionDetalleAccion").html("");
	 $(".subOpcionDetalleAccion").hide();
	
	 objDetalleAuditoria=detalleAuditoriaFull[pindex];
	 objDetalleAuditoria.id=objDetalleAuditoria.detalleAuditoriaId;
var audiObj={accionMejoraTipoId : 1,
		detalleAuditoriaId:objDetalleAuditoria.id};

banderaEdicionAccAuditoria=false;
objAuditoria={index:pindex};
objAccAuditoria={detalleAuditoriaId:objDetalleAuditoria.id};

callAjaxPost(URL + '/gestionaccionmejora/accionmejora', 
			audiObj, function(data){
				if(data.CODE_RESPONSE=="05"){
					detalleAccAuditoriaFull=data.list; 
					detalleAccAuditoriaFull.forEach(function(val,index){
						val.id=val.accionMejoraId;
						val.detalleAuditoriaId=objDetalleAuditoria.id;
						 //
						var menuOpcion="<ul class='list-group' >";
						funcionalidadesAccionMejoraColaborador.forEach(function(val1,index1){
							menuOpcion+="<li class='list-group-item' onclick='marcarSubOpcionAccMejoraColaborador("+index1+")'>"+val1.nombre+" </li>"
						});
						menuOpcion+="</ul>"
						var textResp="";
						if(val.respuesta.length>0){
							textResp=iconoGosst.advertir+" "+val.respuesta+"<br>"
						}
						var claseAux="gosst-neutral";
						if(val.respuesta=="" && val.estadoId==2){
							claseAux="gosst-aprobado"
						}
						 $("#proyectoTable"+listFullProyectoSeguridad[indexPostulante].id)
							.find("#subDivMovilDetalleAudi"+objDetalleAuditoria.id+" .subOpcionDetalleAccion").show()
							.append("<div class='subDetalleAccion "+claseAux+"' >" +
									"<button class='btn btn-success' onclick='toggleMenuOpcionAccMejoraColaborador(this,"+index+")'>" +
									"<i class='fa  fa-caret-down' aria-hidden='true'></i></button>" +
									menuOpcion+
									"<i class='fa fa-info' aria-hidden='true'></i>" +val.descripcion+ "<br>"+
									"<i class='fa fa-clock-o' aria-hidden='true'></i>" +val.fechaRevisionTexto+ 
									"<i class='fa fa-file' aria-hidden='true'></i>" +val.evidenciaNombre+"<br>"+
									textResp+
									""+
									"</div> " );
						 
					});
					$(".subDetalleAccion ul").hide(); 
				}else{
					console.log("NOPNPO")
				}
			});
}
var listFullTrabajadoresProyectos=[];
function resaltarObservacionTexto(texto){
	var asd="";
	if(texto!=""){
		asd="<strong>("+texto+")</strong>"
	} 
	return asd;
}
function verificarTrabajadorContratistaHabilitado(val){
	var hasExamen=false,hasSctr=false,hasInduccion=false,hasCargo=false,hasTreg=false,hasEpp=false;
	var textoExamen=iconoGosst.desaprobado+"Sin examen médico",
	textoSctr=iconoGosst.desaprobado+"Sin SCTR",
	textoInduccion=iconoGosst.desaprobado+"Sin Inducciones aprobadas ",
	textoCargo=iconoGosst.desaprobado+"Sin Cargo",
	textoTreg=iconoGosst.desaprobado+"Sin T-registro",textoEpp=iconoGosst.desaprobado+"Sin Equipo de Protección Personal",
	
	textoTregLong="<i class='fa fa-files-o' aria-hidden='true'></i>"+iconoGosst.desaprobado+" Sin examen médico<br>",
	textoExamLong="<i class='fa fa-medkit' aria-hidden='true'></i>"+iconoGosst.desaprobado+" Sin T-registro<br>",
	textoSctrLong="<i class='fa fa-folder-open-o' aria-hidden='true'></i>"+iconoGosst.desaprobado+" Sin SCTR<br>",
	textoInducLong="<i class='fa fa-arrow-circle-o-down' aria-hidden='true'></i>"+iconoGosst.desaprobado+" Sin Inducciones aprobadas<br>",
	textoCargoLong="<i class='fa fa-inbox' aria-hidden='true'></i>"+iconoGosst.desaprobado+" Sin Cargo<br>",
	textoEppLong="<i class='fa fa-files-o' aria-hidden='true'></i>"+iconoGosst.desaprobado+" Sin Equipo de Protección Personal<br>";
	var textoSctrVigilante=""+textoSctr;
	var textoExamenVigilante=""+textoExamen;
	var textoInduccionVigilante=""+textoInduccion;
	 
	var obsExam= resaltarObservacionTexto(val.observacionExamenMedico);
	var obsSctr =	 resaltarObservacionTexto(val.observacionSctr);
	var obsInduc  =	 resaltarObservacionTexto(val.observacionInduccion);
	var obsCargo  =	 resaltarObservacionTexto(val.observacionCargo);
	var obsReg  =	 resaltarObservacionTexto(val.observacionRegistro);
	var obsEpp  =	 resaltarObservacionTexto(val.observacionEpp);
		 
	if(val.indicadorExamenMedico!=null){
		var iconoVigilante=""+iconoGosst.desaprobado;
		var iconoHelp=""+iconoGosst.por_evaluar;
		var textNoVigente=" no";
		if(val.vigentePuroExamenMedico==1){
			textNoVigente="";
		}
		if(val.notaExamenMedico==1){
			obsExam="";
		}
		if(val.vigenteExamenMedico==1){
			hasExamen=true;
			iconoHelp=""+iconoGosst.aprobado
			iconoVigilante=""+iconoGosst.aprobado;
			obsExam="";
		}else{
			iconoHelp=""+iconoGosst.advertir;
			if(val.notaExamenMedico==null || val.notaExamenMedico==3)	{
				iconoHelp=""+iconoGosst.por_evaluar;
			}
		}
		textoExamen=iconoHelp+"Examen"+textNoVigente+" vigente: <br>"+val.indicadorExamenMedico+" "+obsExam
		textoExamenVigilante=iconoVigilante+"Examen"+textNoVigente+" vigente: <br>"+val.indicadorExamenMedico+" "+obsExam
	}
	if(val.indicadorSctr!=null){
		var iconoVigilante=""+iconoGosst.desaprobado;
		var iconoHelp=""+iconoGosst.por_evaluar;
		var textNoVigente=" no";
		if(val.vigentePuroSctr==1){
			textNoVigente="";
		}
		if(val.notaSctr==1){
			obsExam="";
		}
		if(val.vigenteSctr==1){
			hasSctr=true;
			iconoHelp=""+iconoGosst.aprobado
			iconoVigilante=""+iconoGosst.aprobado;
			obsSctr="";
		}else{
			iconoHelp=""+iconoGosst.advertir;
			if(val.notaSctr==null || val.notaSctr==3)	{
				iconoHelp=""+iconoGosst.por_evaluar;
			}
		}
		textoSctr=iconoHelp+"SCTR"+textNoVigente+" vigente: <br>"+val.indicadorSctr+" "+obsSctr;
		textoSctrVigilante=iconoVigilante+"SCTR"+textNoVigente+" vigente: <br>"+val.indicadorSctr+" "+obsSctr;
	}
	if(val.indicadorInduccion!=null){
		var iconoVigilante=""+iconoGosst.desaprobado;
		var iconoHelp=""+iconoGosst.por_evaluar;
		var textNoVigente=" no";
		if(val.vigenteInduccion==1){
			textNoVigente="";
		}
		if(val.vigenteInduccion==1){
			hasInduccion=true;
			iconoHelp=""+iconoGosst.aprobado;
			iconoVigilante=""+iconoGosst.aprobado;
			obsInduc=""
		}
		
		textoInduccion=iconoHelp+"Inducción"+textNoVigente+" vigente: <br>"+val.indicadorInduccion+" "+obsInduc;
		textoInduccionVigilante=iconoVigilante+"Inducción"+textNoVigente+" vigente: <br>"+val.indicadorInduccion+" "+obsInduc;
	}
	//
	if(val.indicadorCargo!=null){
		var iconoHelp=""+iconoGosst.por_evaluar;
		var textNoVigente=" no";
		if(val.vigentePuroCargo==1){
			textNoVigente="";
		}
		if(val.notaCargo==1){
			obsExam="";
		}
		if(val.vigenteCargo==1){
			hasCargo=true;
			iconoHelp=""+iconoGosst.aprobado;
			obsCargo=""
		}else{
			iconoHelp=""+iconoGosst.advertir;
			if(val.notaCargo==null || val.notaCargo==3)	{
				iconoHelp=""+iconoGosst.por_evaluar;
			}
		}
		textoCargo=iconoHelp+"Cargo"+textNoVigente+" vigente: <br>"+val.indicadorCargo+" "+obsCargo;
		}
	if(val.indicadorRegistro!=null){
		var iconoHelp=""+iconoGosst.por_evaluar;
		var textNoVigente=" no";
		if(val.vigentePuroRegistro==1){
			textNoVigente="";
		}
		if(val.notaRegistro==1){
			obsExam="";
		}
		if(val.vigenteRegistro==1){
			hasTreg=true;
			iconoHelp=""+iconoGosst.aprobado;
			obsReg="";
		}else{
			iconoHelp=""+iconoGosst.advertir;
			if(val.notaRegistro==null || val.notaRegistro==3)	{
				iconoHelp=""+iconoGosst.por_evaluar;
			}
		}
		
		textoTreg=iconoHelp+"T-registro"+textNoVigente+" vigente "+" "+obsReg ;
		textoTregLong=
			"<i class='fa fa-files-o' aria-hidden='true'></i>"
			+iconoHelp+"T-registro"+textNoVigente+" vigente "+" "+obsReg+"<br>";
	}
	//
	if(val.indicadorEpp!=null){
		var iconoHelp=""+iconoGosst.por_evaluar;
		if(val.vigentePuroEpp==1){
			textNoVigente="";
		}
		if(val.notaEpp==1){
			obsExam="";
		}
		if(val.vigenteEpp==1){
			hasEpp=true;
			iconoHelp=""+iconoGosst.aprobado;
			obsEpp="";
		}else{
			iconoHelp=""+iconoGosst.advertir;
			if(val.notaEpp==null || val.notaEpp==3)	{
				iconoHelp=""+iconoGosst.por_evaluar;
			}
		}
		textoEpp=iconoHelp+"EPP"+textNoVigente+" vigente <br>"+" "+obsEpp;
		}
	//
	var eval4=val.evaluacionHojaVida;
	var textHojaVida="Hoja Vida: "+val.numHojaVida4+" ";
	var hasHojaVida=false;
	if(val.evaluacionHojaVida!=null){
		if(eval4.fechaActualizacion>eval4.fecha){
			eval4.textoFecha=iconoGosst.advertir+textHojaVida+" Modificado el:<br>"+eval4.fechaActualizacionTexto;
			eval4.textoFechaShort=iconoGosst.advertir+" Modificado el:<br>"+eval4.fechaActualizacionTexto;
		}else{
			eval4.textoFecha=iconoGosst.aprobado+textHojaVida+" Sin modificaciones"
			eval4.textoFechaShort=iconoGosst.aprobado+" Sin modificaciones";
		}
		if(eval4.fecha==null){
			eval4.textoFecha="";
			eval4.textoEval=iconoGosst.por_evaluar+textHojaVida+" Sin evaluar";
			eval4.textoFechaShort="";
			eval4.textoEvalShort=iconoGosst.por_evaluar+" Sin evaluar";
		}else{
			if(eval4.tipo.id==1){
				eval4.textoEval=iconoGosst.aprobado+textHojaVida+" Evaluado el: <br>"+eval4.fechaTexto;
				eval4.textoEvalShort=iconoGosst.aprobado+" Evaluado el: <br>"+eval4.fechaTexto;
					hasHojaVida=true;
			}else{
				eval4.observacionStrong=resaltarObservacionTexto(eval4.observacion);
				eval4.textoEval=iconoGosst.advertir+textHojaVida+" Evaluado el: <br>"+eval4.fechaTexto+"  "+eval4.observacionStrong
				eval4.textoEvalShort=iconoGosst.advertir+" Evaluado el: <br>"+eval4.fechaTexto+"  "+eval4.observacionStrong
				}
		}  
	}
	
	if(val.numHojaVida4==0){
		eval4={textoEvalShort:iconoGosst.desaprobado+" Sin Hoja de Vida ",
				textoEval:iconoGosst.desaprobado+" Sin Hoja de Vida ",
				textoFecha:"",textoFechaShort:""};
	}
	//
	if(parseInt(getSession("isProyectoSimple"))==1){
		hasTreg=true;textoTregLong="";
	}
	var textHabili=""+iconoGosst.desaprobado+"  "
	var isHabilitado=false;
	 
	if(proyectoObj.examenesPermitidosId==null){
		proyectoObj.examenesPermitidosId="";
	}
	var docsId=proyectoObj.examenesPermitidosId.split(",");
	docsId.forEach(function(val1){
		switch(parseInt(val1)){
		case 16:
			hasHojaVida=true;
			break;
		case 6:
			hasSctr=true;
			break;
		case 7:
			hasExamen=true;
			break;
		case 9:
			hasInduccion=true;
			break;
		case 11:
			hasCargo=true;
			break;
		case 12:
			hasTreg=true;
			break;
		case 8:
			hasEpp=true;
			break;
		}
	});
	if(hasSctr && hasExamen && hasInduccion && hasCargo && hasTreg && hasHojaVida && hasEpp){
		isHabilitado= true;
	}else{
		isHabilitado= false;
	}
	var isHabilitadoContratista=false;
	if(hasSctr && hasExamen && hasInduccion && hasCargo && hasTreg ){
		isHabilitadoContratista= true;
	}else{
		isHabilitadoContratista= false;
	}
	//
	if(proyectoObj.evaluacionManual==0){
		
	}else{
		if(val.notaManual==1){
			isHabilitado=true;
		}else{
			isHabilitado=false;
		}
	}
	//
	return {isHabilitado:isHabilitado,isHabilitadoContratista:isHabilitadoContratista,
			textoExamen:textoExamen,textoEpp:textoEpp,
			textoSctr:textoSctr,
			textoInduccion:textoInduccion,
			textoCargo:textoCargo,
			textoTreg:textoTreg,textoTregLong:textoTregLong,
			
			textoSctrVigilante:textoSctrVigilante,
			textoExamenVigilante:textoExamenVigilante,
			textoInduccionVigilante:textoInduccionVigilante,
			
			
			eval4:eval4}
}
function verCompletarTrabajadoresProyecto(){ 
	 var proyectoObjSend={
		 id:proyectoObj.id,
		 contratistaId:proyectoObj.postulante.contratista.id
	 }
	callAjaxPost(URL + '/contratista/proyecto/trabajadores', 
			proyectoObjSend, procesarResumenTrabajadoresProyecto);
}
function procesarResumenTrabajadoresProyecto(data){
	if(data.CODE_RESPONSE=="05"){ 
		$("#tblTrabProyecto tbody tr").remove();
		listFullTrabajadoresProyectos=data.list;
		var numTotal=0,numPositivo=0;
		listFullTrabajadoresProyectos.forEach(function(val,index){
			if(val.proyectoActual!=null && val.isActivo==1){
				numTotal++;
				var claseGosst="gosst-neutral";
				var textHabili=""+iconoGosst.desaprobado+"  "
				var verificacionObj=verificarTrabajadorContratistaHabilitado(val);
				var textBton="<br><button class='btn btn-success' " +
				"onclick='cambioManualTrabajadorProyecto("+index+")''>" +
				"<i class='fa fa-refresh'></i> Cambio Manual</button>";
				if(proyectoObj.evaluacionManual==0){
					textBton="";
				}else{
					if(val.notaManual==1){
						textBton="<br><button class='btn btn-success' " +
						"onclick='cambioManualTrabajadorProyecto("+index+",0)''>" +
						"<i class='fa fa-refresh'></i> Cambio Manual</button>";
					}else{
						textBton="<br><button class='btn btn-success' " +
						"onclick='cambioManualTrabajadorProyecto("+index+",1)''>" +
						"<i class='fa fa-refresh'></i> Cambio Manual</button>";
					}
				}
				val.isHabilitado=(verificacionObj.isHabilitado?0:1);
				
				if(verificacionObj.isHabilitado){
					claseGosst="gosst-aprobado";
					numPositivo++;
					textHabili=""+iconoGosst.aprobado+" "
				}
				
				
			$("#tblTrabProyecto tbody").append(
					"<tr id='trproy"+val.id+"'>" +
					"<td class='tdIconoEval' id='asd"+val.id+"'>"+textHabili+""+textBton+"</td>" +
					
					"<td id='asd"+val.id+"'>"+val.nombre+"</td>" +
					"<td id='asd"+val.id+"'>"+val.apellido+"</td>" +
					"<td id='asd"+val.id+"'>"+val.area+"</td>" +
					"<td id='asd"+val.id+"'>"+val.cargo+"</td>" +
					"<td id='asd"+val.id+"'>"+val.tipoDocumento.nombre+"</td>" +
					"<td id='asd"+val.id+"'>"+val.nroDocumento+"</td>" +
					"<td class='linkGosst' onclick='verHistorialExamenesTrabajador(1,"+index+")'  id='asd"+val.id+"'>"+verificacionObj.textoExamen+"</td>" +
					"<td class='linkGosst' onclick='verHistorialExamenesTrabajador(2,"+index+")' id='asd"+val.id+"'>"+verificacionObj.textoSctr+"</td>" +
					"<td class='linkGosst' onclick='verHistorialExamenesTrabajador(3,"+index+")' id='asd"+val.id+"'>"+verificacionObj.textoInduccion+"</td>" +
					"<td  class='linkGosst' onclick='verHistorialExamenesTrabajador(4,"+index+")'id='asd"+val.id+"'>"+verificacionObj.textoCargo+"</td>" +
					"<td class='linkGosst' onclick='verHistorialExamenesTrabajador(6,"+index+")' id='asd"+val.id+"'>"+verificacionObj.textoTreg+"</td>" +
					"<td class='linkGosst' onclick='verHistorialExamenesTrabajador(8,"+index+")' id='asd"+val.id+"'>"+verificacionObj.textoEpp+"</td>" +
					"<td id='hojtrpr"+val.id+"'>"+""+"</td>" + 
					
					"</tr>"); 
			$("#hojtrpr"+val.id).addClass("linkGosst")
			.on("click",function(){verEvaluacionHojaVidaTrabajador(4,index);})
			.html(" "+val.numHojaVida4+"<br>"+verificacionObj.eval4.textoFechaShort+"<br>"+verificacionObj.eval4.textoEvalShort); 
			}
		});
		var docsId=proyectoObj.examenesPermitidosId.split(",");
		docsId.forEach(function(val1){
			switch(parseInt(val1)){
			case 6:
				ocultarColumna(8,"tblTrabProyecto")
				break;
			case 7:
				ocultarColumna(9,"tblTrabProyecto")
				break;
			case 9:
				ocultarColumna(10,"tblTrabProyecto")
				break;
			case 11:
				ocultarColumna(11,"tblTrabProyecto")
				break;
			case 12:
				ocultarColumna(12,"tblTrabProyecto")
				break;
			case 8:
				ocultarColumna(13,"tblTrabProyecto")
				break;
			case 16:
				ocultarColumna(14,"tblTrabProyecto")
				break;
			}
		});
		goheadfixedY("#tblTrabProyecto","#wrapTrabProyecto");
		//
		var buttonDescarga="<a target='_blank' class='btn btn-success'" +
		" href='"+URL+"/'>" +
		"<i class='fa fa-download'></i>Descargar</a>"
		var buttonClip="<button type='button' id='btnClipboardTrabProy'   style='margin-right:10px'" +
				" class='btn btn-success clipGosst' onclick='obtenerTablaTrabajadoresProyecto()'>" +
						"<i class='fa fa-clipboard'></i>Clipboard</button>"
		var buttonFiltro="<button type='button' class='btn btn-success ' id='btnIniciarFiltro' style='margin-right:10px'" +
		" onclick='iniciarFiltroTrabProyecto()'>" +
					"<i class='fa fa-filter'></i>Ver filtros</button>";
		var buttonFiltroAplicar="<button type='button' class='btn btn-success ' id='btnAplicarFiltro' style='margin-right:10px'" +
		" onclick='aplicarFiltroTrabProyecto()'>" +
					"<i class='fa fa-filter'></i>Aplicar filtros</button>";
		var buttonFiltroRefresh="<button type='button' class='btn btn-success ' id='btnReiniciarFiltro' style='margin-right:10px'" +
		" onclick='reiniciarFiltroTrabProyecto()'>" +
					"<i class='fa fa-refresh'></i>Refresh</button>";
		var buttonBuscar="<input class='form-control' id='buscarTrabProy' placeholder='DNI,nombre o apellido'>" +
				"<button type='button' class='btn btn-success ' style='margin-right:10px'" +
		" onclick='buscarTrabajadorProyecto()'>" +
					"<i class='fa fa-search'></i>Buscar</button>";
		var btnManual="" +
				"<button type='button' class='btn btn-success ' style='margin-right:10px'" +
				" onclick='cambiarEvaluacionManualProyecto(1)'>" +
				"<i class='fa fa-pencil-square-o'></i>Evaluar Manualmente</button>";
		var btnAuto="" +
		"<button type='button' class='btn btn-success ' style='margin-right:10px'" +
		" onclick='cambiarEvaluacionManualProyecto(0)'>" +
		"<i class='fa fa-check-square-o'></i>Evaluar Automáticamente</button>";
		var btnEval="";
		if(proyectoObj.evaluacionManual==1){
			btnEval=btnAuto;
		}else{
			btnEval=btnManual;
		}
		$("#divTrabProyecto").find("form").remove();
		$("#divTrabProyecto").find(".camposFiltroTabla").remove();
		$("#divTrabProyecto").prepend(
				"<form class='form-inline'>"+
				buttonBuscar+buttonFiltroRefresh+buttonFiltroAplicar+buttonFiltro+btnEval+buttonClip+"</form>"+
				"<div class='camposFiltroTabla'>"+
					"<div class='contain-filtro' id='filtroProyTrab'>" +
						"<input type='checkbox' id='checkFiltroProyTrab' checked>" +
						"<label for='checkFiltroProyTrab'  >Trabajadores "+
						"</label>" +
						"<div id='divSelectFiltroProyTrab'> " +
						"</div>" + 
					"</div>"+	
					" </div>" );
		var listOpciones=[{id:1000,nombre:"Habilitado"},
		                  {id:0,nombre:"Sin Habilitar"},
		                  {id:2,nombre:"Sin SCTR"},
		                  {id:1,nombre:"Sin Examen Médico"},
		                  {id:3,nombre:"Sin Inducción"},
		                  {id:4,nombre:"Sin Cargo"},
		                  {id:6,nombre:"Sin T-registro"},{id:8,nombre:"Sin EPP"},
		                  {id:100,nombre:"Sin Hoja de Vida"}];
		if(parseInt(getSession("isProyectoSimple"))==1){
			listOpciones=[{id:1000,nombre:"Habilitado"},
		                  {id:0,nombre:"Sin Habilitar"},
		                  {id:2,nombre:"Sin SCTR"},
		                  {id:1,nombre:"Sin Examen Médico"},
		                  {id:3,nombre:"Sin Inducción"},
		                  {id:4,nombre:"Sin Cargo"},{id:8,nombre:"Sin EPP"},
		                  {id:100,nombre:"Sin Hoja de Vida"}];
		}
		crearSelectOneMenuObligMultipleCompleto("selMultipleTrabProy", "", listOpciones, 
				"id","nombre","#divSelectFiltroProyTrab","Seleccione criterios");
		
		$(".camposFiltroTabla").hide();
		
		$("#btnAplicarFiltro").hide();
		$("#buscarTrabProy").focus();
		$("#divTrabProyecto form").on("submit",function(e){
				e.preventDefault();
				buscarTrabajadorProyecto();
		});
		
		
		
		//
		$("#tabProyectos h2").html("<a onclick='volverDivProyectos()'>Proyecto ("+proyectoObj.titulo+")</a> " +
				"> Trabajadores del Contratista ( "+numPositivo+" de "+numTotal+" )")
		$(".tdIconoEval>i").css({"font-size":"30px"})
		if(parseInt(getSession("isProyectoSimple"))==1){
			ocultarColumna(11,"tblTrabProyecto");
		}
	}else{
		console.log("NOPNPO")
	}
}
function cambiarEvaluacionManualProyecto(isManual){
	var proyectoObjSend={id:proyectoObj.id,evaluacionManual:isManual};
	proyectoObj.evaluacionManual=isManual;
	callAjaxPost(URL + '/contratista/proyecto/evaluacion_manual', 
			proyectoObjSend, verCompletarTrabajadoresProyecto);
}
function cambioManualTrabajadorProyecto(indexTrab,isAprobado){
	var trabObjSend={id:listFullTrabajadoresProyectos[indexTrab].id,
			notaManual:isAprobado,proyectoActual:{id:proyectoObj.id}};
	listFullTrabajadoresProyectos[indexTrab].notaManual=isAprobado;
	callAjaxPost(URL + '/contratista/proyecto/trabajador/evaluacion_manual', 
			trabObjSend, verCompletarTrabajadoresProyecto);
}
function procesarResumenTrabajadoresProyectoVistaContratista(data){
	if(data.CODE_RESPONSE=="05"){ 
		$("#tblTrabProyecto tbody tr").remove();
		listFullTrabajadoresProyectos=data.list;
		var numTotal=0,numPositivo=0;
		listFullTrabajadoresProyectos.forEach(function(val,index){
			if(val.proyectoActual!=null && val.isActivo==1){
				numTotal++;
				var claseGosst="gosst-neutral";
				var textHabili=""+iconoGosst.desaprobado+"  "
				var verificacionObj=verificarTrabajadorContratistaHabilitado(val);
				val.isHabilitado=(verificacionObj.isHabilitado?0:1);
				if(verificacionObj.isHabilitado){
					claseGosst="gosst-aprobado";
					numPositivo++;
					textHabili=""+iconoGosst.aprobado+" "
				}
			$("#tblTrabProyecto tbody").append(
					"<tr id='trproy"+val.id+"'>" +
					"<td class='tdIconoEval' id='asd"+val.id+"'>"+textHabili+"</td>" +
					
					"<td id='asd"+val.id+"'>"+val.nombre+"</td>" +
					"<td id='asd"+val.id+"'>"+val.apellido+"</td>" +
					"<td id='asd"+val.id+"'>"+val.area+"</td>" +
					"<td id='asd"+val.id+"'>"+val.cargo+"</td>" +
					"<td id='asd"+val.id+"'>"+val.tipoDocumento.nombre+"</td>" +
					"<td id='asd"+val.id+"'>"+val.nroDocumento+"</td>" +
					"<td >"+verificacionObj.textoExamen+"</td>" +
					"<td >"+verificacionObj.textoSctr+"</td>" +
					"<td >"+verificacionObj.textoInduccion+"</td>" +
					"<td >"+verificacionObj.textoCargo+"</td>" +
					"<td >"+verificacionObj.textoTreg+"</td>" +
					"<td >"+verificacionObj.textoEpp+"</td>" +
					"<td id='hojtrpr"+val.id+"'>"+""+"</td>" + 
					
					"</tr>"); 
			$("#hojtrpr"+val.id)
			.html(" "+val.numHojaVida4+"<br>"+verificacionObj.eval4.textoFechaShort+"<br>"+verificacionObj.eval4.textoEvalShort); 
			}
		});
		
		$("#tabProyectos h2").append(" ( "+numPositivo+" de "+numTotal+" )")
		$(".tdIconoEval i").css({"font-size":"30px"});
		var docsId=proyectoObj.examenesPermitidosId.split(",");
		docsId.forEach(function(val1){
			switch(parseInt(val1)){
			case 6:
				ocultarColumna(8,"tblTrabProyecto")
				break;
			case 7:
				ocultarColumna(9,"tblTrabProyecto")
				break;
			case 9:
				ocultarColumna(10,"tblTrabProyecto")
				break;
			case 11:
				ocultarColumna(11,"tblTrabProyecto")
				break;
			case 12:
				ocultarColumna(12,"tblTrabProyecto")
				break;
			case 8:
				ocultarColumna(13,"tblTrabProyecto")
				break;
			case 16:
				ocultarColumna(14,"tblTrabProyecto")
				break;
			}
		});
		if(parseInt(getSession("isProyectoSimple"))==1){
			ocultarColumna(11,"tblTrabProyecto");
		}
	}else{
		console.log("NOPNPO")
	}
}
function volverDivTrabajadoresProyectos(){
	$("#tabProyectos .container-fluid").hide();
	$("#divTrabProyecto").show();
	$("#tabProyectos h2").html("<a onclick='volverDivProyectos()'>Proyecto ("+proyectoObj.titulo+")</a> > Trabajadores del Contratista");
	 
}
function verHistorialExamenesTrabajador(tipoId,pindex){
	var nombreExam="";
	switch(parseInt(tipoId)){
	case 1 :
		nombreExam="Exámen Médico";
		break;
	case 2 :
		nombreExam="SCTR";
		break;
	case 3 :
		nombreExam="Inducción";
		break;
	case 5 :
		nombreExam="Cargo RISST";
		break;
	case 6 :
		nombreExam="T-Registro";
		break;
		
	}
	$("#tabProyectos .container-fluid").hide();
	$("#divExamsTrabajador").show();
	$("#tabProyectos h2").html("<a onclick='volverDivProyectos()'>Proyecto ("+proyectoObj.titulo+")</a> " +
			"> <a onclick='volverDivTrabajadoresProyectos()' >Trabajador "+listFullTrabajadoresProyectos[pindex].nombre+" </a>" +
					"> Historial "+nombreExam);
	var dataParam={ 
			id:(proyectoObj.postulante.contratista.id),
			examen:{tipo:{id:tipoId}}
			};
	callAjaxPost(URL + '/contratista/examenes', dataParam, function(data) {
		$("#tblExamenTrab tbody tr").remove();
		var trabajadorAuxId=listFullTrabajadoresProyectos[pindex].id;
		var num=0;
		data.examenes.forEach(function(val,index){
			
			if(val.trabajadoresId.indexOf(trabajadorAuxId)!=-1){
				var btnDescarga="<br><a class='btn btn-success' target='_blank' " +
				"href='"+URL+"/contratista/examen/evidencia?id="+val.id+"  ' ><i class='fa fa-download'></i> Descargar </a>";
				if(val.evidenciaNombre==""){
					btnDescarga="";
					val.evidenciaNombre="Sin registrar";
				}
				var textoEstilo=" ";
				if(num==0){
					textoEstilo="style='    border-left: 9px solid #2e9e8f;'";
					num++;
				}
				if(val.evaluacion==null){
					val.evaluacion={observacion:""}
				}
				var textIn=" <tr>" +
						"<td "+textoEstilo+"> " +val.fechaTexto+"</td>"+
						"<td> " +val.fechaFinVigenciaTexto+"<br><strong>"+val.evaluacion.observacion+"</strong></td>"+
						"<td> " +val.evidenciaNombre+btnDescarga+"</td>"+
						"</tr>"
						;
				$("#tblExamenTrab tbody").append(textIn);
			}
		});
	});
}
var evaluacionHojaVidaObj;
function verEvaluacionHojaVidaTrabajador(idTipo,pindex){
	$("#tabProyectos .container-fluid").hide();
	$("#divEvalTrabProyecto").show();
	$("#tabProyectos h2").html("<a onclick='volverDivProyectos()'>Proyecto ("+proyectoObj.titulo+")</a> " +
			"> <a onclick='volverDivTrabajadoresProyectos()' >Trabajador "+listFullTrabajadoresProyectos[pindex].nombre+" </a>" +
					"> Hoja de Vida ");
	 
	var objTrabajadorContratistaSend={id:listFullTrabajadoresProyectos[pindex].id}
	callAjaxPost(URL + '/contratista/trabajador/hojavidas', 
			objTrabajadorContratistaSend, function(data){
				if(data.CODE_RESPONSE=="05"){
					$("#tblEvalTrabProy tbody tr").remove();
					
					switch(idTipo){
					case 1:
						evaluacionHojaVidaObj=listFullTrabajadoresProyectos[pindex].evaluacionExperiencia;
						break;
					case 2:
						evaluacionHojaVidaObj=listFullTrabajadoresProyectos[pindex].evaluacionFormacion;
						break;
					case 3:
						evaluacionHojaVidaObj=listFullTrabajadoresProyectos[pindex].evaluacionAptitud;
						break;
					case 4:
						evaluacionHojaVidaObj=listFullTrabajadoresProyectos[pindex].evaluacionHojaVida;
						break;
					}
					$("#btnGuardarEvalHojaVida").attr("onclick","guardarEvaluacionHojaVidaTrabajador("+idTipo+")")
					var selTipoNotaHojaVida=crearSelectOneMenuOblig("selTipoNotaHojaVida", "", data.nota, evaluacionHojaVidaObj.tipo.id, 
							"id","nombre");
					$("#divSelTipoNotaHojaVida").html("<label>Evaluación</label>"+selTipoNotaHojaVida)
					$("#divObsHojaVida").html("<label>Observación</label><input class='form-control' id='obsHojaVidaTrabajador'>")
					$("#divFechaHojaVida").html("Última vez evaluado: <strong>"+evaluacionHojaVidaObj.fechaTexto+"</strong>")
					$("#divFechaActualizacionHojaVida").html("Modificado por contratista: <strong>"+evaluacionHojaVidaObj.fechaActualizacionTexto+"</strong>")
					$("#obsHojaVidaTrabajador").val(evaluacionHojaVidaObj.observacion);
					
					data.list.forEach(function(val,index){  
						if(val.tipo.id==idTipo){  
						$("#tblEvalTrabProy tbody").append("<tr>" +
								"<td> "+val.tipo.nombre+"    </td>" +
								"<td> "+val.observacion+"    </td>" +
								"<td> "+val.evidenciaNombre+"    </td>" +
								"<td> "+"<a class='btn btn-success' " +
										"href='" +
										""+URL+"/contratista/trabajador/hojavida/evidencia?id="+val.id+"' target='_blank'  " +
										"" +
										"'><i aria-hidden='true' class='fa fa-download'></i>Descargar</a> "+"    </td>" +
								"" +
								"</tr>");
						
						}
					});
				}
		})
}
function guardarEvaluacionHojaVidaTrabajador(tipoId){
	evaluacionHojaVidaObj.observacion=$("#obsHojaVidaTrabajador").val();
	evaluacionHojaVidaObj.tipo.id=$("#selTipoNotaHojaVida").val();
	evaluacionHojaVidaObj.tipoHoja={id:tipoId};
	callAjaxPost(URL + '/contratista/trabajador/hojavida/evaluacion/save', 
			evaluacionHojaVidaObj, function(data){
				if(data.CODE_RESPONSE=="05"){
					volverDivTrabajadoresProyectos();
					verCompletarTrabajadoresProyecto();
				}
	})
	
}
function verCompletarActividadesProyecto(tipoId){
	$("#btnCancelarActProyecto").hide();
	$("#btnNuevoActProyecto").hide();
	$("#btnEliminarActProyecto").hide();
	$("#btnGuardarActProyecto").hide();
	estadoActividadProy=[];
	proyectoObj.tipoActividad=tipoId;
	callAjaxPost(URL + '/contratista/proyecto/actividades', 
			{id:proyectoObj.id,tipoActividad:tipoId}, function(data){
				if(data.CODE_RESPONSE=="05"){ 
					$("#tblActProyecto tbody tr").remove();
					data.list.forEach(function(val,index){
						var textIn="";
					if(val.tipo.id==tipoId){
						 textIn="<td id='actpdesc"+val.id+"'>"+val.categoria.nombre +" "+val.descripcion+"</td>" +
						"<td id='actpfp"+val.id+"'>"+val.fechaPlanificadaTexto+"</td>" +
						"<td id='actpfr"+val.id+"'>"+val.fechaRealTexto+"</td>" +
						"<td id='actpevi"+val.id+"'>"+""+"</td>" +
						"<td id='actpes"+val.id+"'>"+val.estado.nombre+"</td>" +
						"<td id='actpobs"+val.id+"'>"+val.observacion+"</td>" ;
						
						
					
						$("#tblActProyecto tbody").append(
								"<tr id='trtip"+val.id+"'  >" +
//							
								textIn+
								 
								"</tr>");
						var textEvi=val.evidenciaNombre;
						if(textEvi.length>0){
							$("#actpevi"+val.id).addClass("linkGosst")
							.on("click",function(){
								location.href= URL
									+ "/contratista/proyecto/actividad/evidencia?id="
									+ val.id;	
							})
							.html("<i class='fa fa-download fa-2x' aria-hidden='true' ></i>"+textEvi);
							
						}
					}
				}); 
					if(tipoId==1){
						$('#tblActProyecto td:nth-child(1),#tblActProyecto th:nth-child(1)').show(); 
						$('#tblActProyecto td:nth-child(2),#tblActProyecto th:nth-child(2)').show(); 
						$('#tblActProyecto td:nth-child(5),#tblActProyecto th:nth-child(5)').show(); 
					}else{ 
						$('#tblActProyecto td:nth-child(1),#tblActProyecto th:nth-child(1)').hide(); 
						$('#tblActProyecto td:nth-child(2),#tblActProyecto th:nth-child(2)').hide(); 
						$('#tblActProyecto td:nth-child(5),#tblActProyecto th:nth-child(5)').hide(); 
						
					}
			}else{
				console.log("NOPNPO")
			}
		});
}
function verSinEditarCompletarProyecto(pindex){
	var dataParam = {
			id : listFullProyectoSeguridad[pindex].contratistaId
	};
	//
	$("#proyectoTable"+listFullProyectoSeguridad[pindex].id).parent("#tituloEvento")
	.parent(".eventoGeneral").parent(".contenidoSubList")
	.parent(".divProyectoGeneral").siblings()
	.find(".detalleDocumentosProyecto").hide();
//
	indexPostulante=pindex; 
	$("#proyectoTable"+listFullProyectoSeguridad[pindex].id)
	.find(".detalleDocumentosProyecto").toggle()
	 
	callAjaxPost(
			URL + '/contratista/proyecto/postulante/evaluacion',
			dataParam,
			function(data) {
				switch (data.CODE_RESPONSE) {
				case "05": 
					listEvaluacionPostulanteOnline=data.list;
					$("#tblEvaluacion tbody tr").remove(); 
					$("#proyectoTable"+listFullProyectoSeguridad[pindex].id)
					.find(".detalleDocumentosProyecto").html("<td colspan='3'></td>");
					listEvaluacionPostulanteOnline.forEach(function(val,index){
				 
					var evalId=val.id;
					var pregunta=val.preguntaNombre;
					var preguntaId=val.preguntaId;
					var observacionPostulante=val.observacionPostulante;
					var eviNombre=(val.evidenciaNombre=="Sin Registrar"?"Sin Archivo<br>"
									: 
									"<a class='efectoLink' href='"+URL+"/contratista/proyecto/postulante/evaluacion/evidencia?id="+val.id+" ' >"+
										val.evidenciaNombre+"<br>"+
									"</a>");
					var observacion=val.observacion;
					var nota = "";
					var iconoestado;
					listCumple.forEach(function(val1,index1){ 
						if(val1.id==val.isAprobado){
							nota=val1.nombre;
							iconoestado="fa-star";
						}
						else
						{
							iconoestado="fa-star-half-o";
						}
					});
					 
					 //
					var claseNotaDocumento="gosst-neutral";
					if(val.isAprobado==1){
						claseNotaDocumento="gosst-aprobado"
					}
					$("#proyectoTable"+listFullProyectoSeguridad[pindex].id)
					.find(".detalleDocumentosProyecto td")
					.append("<div class='detalleAccion "+claseNotaDocumento+"'> " + 
								"<div class='row'>" +
									"<section class='col col-xs-3'>" +
										"<strong><i class='fa fa-question-circle-o'></i> Pregunta:</strong><br>"+
										"<strong><i class='fa "+iconoestado+"'></i> Estado:</strong><br>"+
										"<strong><i class='fa fa-eye'></i> Observación:</strong> <br>"+
										"<strong><i class='fa fa-download'></i> Evidencia:</strong> "+
									"</section>"+
									"<section class='col col-xs-5'>" +
										pregunta+"<br>" +
										nota+"<br>"+
										observacion+"<br>"+
										eviNombre+
									"</section>"+ 
								"</div>"+
							"</div>" /*+
						"<div class='opcionesAccion'>" +
						"<a target='_blank' href='"+URL+"/contratista/proyecto/postulante/evaluacion/evidencia?id="+val.id+"'>Descargar </a>" +
						 "</div>"*/);
					//
					  
				});
					break;
				default:
					alert("Ocurrió un error al cargar el arbol de trabajadores!");
				}
			});
}
function verCompletarProyecto(pindex,hideBoolean){
	hideBoolean=defaultFor(hideBoolean,true);
	var dataParam = {
			id : listFullProyectoSeguridad[pindex].contratistaId
	};
	//
	
	$("#proyectoTable"+listFullProyectoSeguridad[pindex].id).parent("#tituloEvento")
	.parent(".eventoGeneral").parent(".contenidoSubList")
	.parent(".divProyectoGeneral").siblings()
	.find(".detalleDocumentosProyecto").hide();
//
	indexPostulante=pindex;
	if(hideBoolean){
		$("#proyectoTable"+listFullProyectoSeguridad[pindex].id)
		.find(".detalleDocumentosProyecto").toggle()
	}
	
	 
	callAjaxPost(
			URL + '/contratista/proyecto/postulante/evaluacion',
			dataParam,
			function(data) {
				switch (data.CODE_RESPONSE) {
				case "05": 
					listEvaluacionPostulanteOnline=data.list;
					$("#tblEvaluacion tbody tr").remove(); 
					$("#proyectoTable"+listFullProyectoSeguridad[pindex].id)
					.find(".detalleDocumentosProyecto").html("<td colspan='3'></td>");
					listEvaluacionPostulanteOnline.forEach(function(val,index){
				 
					var evalId=val.id;
					var pregunta=val.preguntaNombre;
					 var preguntaId=val.preguntaId;
						var observacionPostulante=val.observacionPostulante;
						var eviNombre="("+val.fechaTexto+") "+val.evidenciaNombre;
					var observacion=val.observacion;
					var nota =" Sin evaluar";
					var icono= iconoGosst.advertir
					if(val.isAprobado==1){
						icono=iconoGosst.aprobado;
					}
					if(val.isAprobado==0){
						icono=iconoGosst.desaprobado;
					}
					if(val.isAprobado!=null){
					listCumple.forEach(function(val1,index1){
						if(val1.id==val.isAprobado){
							nota=val1.nombre;
						}
					});
					}
					 //
					var claseNotaDocumento="gosst-neutral";
					if(val.isAprobado==1){
						claseNotaDocumento="gosst-aprobado"
					}
					$("#proyectoTable"+listFullProyectoSeguridad[pindex].id)
					.find(".detalleDocumentosProyecto td")
					.append("<div class='detalleAccion "+claseNotaDocumento+"'> " +
							"<i aria-hidden='true' class='fa fa-caret-right'></i>"+
							pregunta+"<br>" +
							"<i aria-hidden='true' class='fa fa-file'></i>"+
							eviNombre+"<br>" +
							"<strong> ("+icono+nota+")</strong>"+"<br>Observación: "+observacion+"</div>" +
						"<div class='opcionesAccion'>" +
						"<a target='_blank' href='"+URL+"/contratista/proyecto/postulante/evaluacion/evidencia?id="+val.id+"'>Descargar </a>" +
						"<i class='fa fa-minus' aria-hidden='true'></i>" +
						"<a onclick='editarEvaluacionPostulanteOnline("+index+")'> Editar</a> </div>")
					//
					  
				});
					break;
				default:
					alert("Ocurrió un error al cargar el arbol de trabajadores!");
				}
			});
}
function editarEvaluacionPostulanteOnline(pindex){
	$("#editarMovilDocProy").find(".tituloSubList")
	.html(textoBotonVolverContenido+"Editar documento de '"
			+listFullProyectoSeguridad[indexPostulante].titulo+"' ");
	var val=listEvaluacionPostulanteOnline[pindex];
	indexEval=pindex;
	var evalId=val.id;
	//
	$(".divListPrincipal>div").hide();
	$("#editarMovilDocProy").show();
	$("#trMovilItem").html(val.preguntaNombre);
 
	var eviNombre=val.evidenciaNombre;
	var options=
	{container:"#eviDocumento",
			functionCall:function(){ },
			descargaUrl: "/contratista/proyecto/postulante/evaluacion/evidencia?id="+evalId,
			esNuevo:false,
			idAux:"DocumentoProy",
			evidenciaNombre:eviNombre};
	crearFormEvidenciaCompleta(options);
	//
	 var preguntaId=val.preguntaId;
		var observacionPostulante=val.observacionPostulante;
		
	var observacionInput="<input id='observacionEval' class='form-control'>";
	  
	$("#trMovilObservacion").html(val.observacion);
	$("#observacionEval").val(observacionPostulante)

}
function volverPostulanteEvaluacionOnline(){
	verCompletarProyecto(indexPostulante)
}
function guardarPostulanteEvaluacionOnline(functionCallBack){
	var observacion=$("#observacionEval").val();
	var actual=listEvaluacionPostulanteOnline[indexEval];
	var dataParam={
			observacion:actual.observacion,
			observacionPostulante:observacion,
			isAprobado:actual.isAprobado,
			preguntaId:actual.preguntaId,
			postulanteId:listFullProyectoSeguridad[indexPostulante].contratistaId,
			id:actual.id
	};
	
	callAjaxPost(URL + '/contratista/proyecto/postulante/evaluacion/save', dataParam,
			function(data) {
				switch (data.CODE_RESPONSE) {
				case "05": 	
				guardarEvidenciaAuto(data.idNuevo,"fileEviDocumentoProy",
						bitsEvidenciaHallazgo,"/contratista/proyecto/postulante/evaluacion/evidencia/save",function(){
					volverDivSubContenido();
					verCompletarProyecto(indexPostulante,false);
					 
					if(functionCallBack!=null){
						functionCallBack();
					}
				});
					break;
				default:
					console.log("Ocurrió un error al guardar el postulante!");
				}
			} ,null,null,null,null,false);
}
function volverDivProyectos(){
	$("#tabProyectos .container-fluid").hide();
	$("#divProyectosFull").show();
	$("#tabProyectos h4").html("Proyectos");
	$("#tabProyectos h2").html("Proyectos");
	$("#divConfigImpacto").hide();
	$("#divListClasif").hide();
	if(parseInt(sessionStorage.getItem("contratistaGosstId"))>0){
		cargarProyectosSeguridad();
	}
}
function editarProyecto(pindex) {
	if (!banderaEdicion4) {
		formatoCeldaSombreableTabla(false,"tblProyectos");
		ProyectoId = listFullProyectoSeguridad[pindex].id;
		proyectoObj=listFullProyectoSeguridad[pindex];
		var slcResponsableProy = crearSelectOneMenu("slcResponsableProy", "",
				listResponsableProy, listFullProyectoSeguridad[pindex].responsable.id, "id", "nombre");
		$("#divProyectosFull #btnConsolidadoInspecc").attr("onclick", "javascript:consolExcelHallazgosLibres("+getSession("gestopcompanyid")+","+ProyectoId+","+94+");");
		$("#divProyectosFull #btnConsolidadoInspecc").show();
		
		var titulo=listFullProyectoSeguridad[pindex].titulo; 
		var descripcion=listFullProyectoSeguridad[pindex].descripcion; 
		var fechaInicio=listFullProyectoSeguridad[pindex].fechaInicio;
		var fechaFin=listFullProyectoSeguridad[pindex].fechaFin;
		var respSst=listFullProyectoSeguridad[pindex].responsableSST;
		var respSstCorreo=listFullProyectoSeguridad[pindex].responsableCorreo;
		 
		
		var selAreaProyecto = crearSelectOneMenuOblig("selAreaProyecto", "",
				listAreaProyecto, listFullProyectoSeguridad[pindex].area.id, "id", "nombre");
		$("#proyarea" + ProyectoId).html(selAreaProyecto);
		///
		
		var selTipoIpercProy = crearSelectOneMenuOblig("selTipoIpercProy", "",
				listTipoIpercProy, listFullProyectoSeguridad[pindex].tipoIperc.id, "id", "nombre");
		//$("#proytipip" + ProyectoId).html(selTipoIpercProy);
		///
		
		$("#proytit" + ProyectoId).html(
		"<input type='text' id='inputProyectoTitulo' class='form-control'>");
		$("#inputProyectoTitulo").val(titulo);

		$("#proydesc" + ProyectoId).html(
		"<input type='text' id='inputProyectoDesc' class='form-control'>");
		$("#inputProyectoDesc").val(descripcion);

		$("#proyrp" + ProyectoId).html(
				slcResponsableProy);
		//
		var respSec=proyectoObj.responsablesSecundariosId.split(",");
		listResponsableProy.forEach(function(val){
			val.selected=0;
			respSec.forEach(function(val1){
				if(parseInt(val1)==val.id){
					val.selected=1;
				}
			})
			
		});
		var slcTrabajadoresProyecto=crearSelectOneMenuObligMultipleCompleto("slcRespSecundarios", "",
				listResponsableProy,  "id", "nombre","#proyresp"+ProyectoId,"Secundarios...");
		 
		//
		$("#proyfecinicio" + ProyectoId).html(
		"<input type='date' id='inputProyectoInicio' style='width:170px'  onchange='hallarEstadoProyecto()' class='form-control'>");
		var today2 = convertirFechaInput(fechaInicio);
		$("#inputProyectoInicio").val(today2);

		$("#proyfecfin" + ProyectoId).html(
		"<input type='date' id='inputProyectoFin' style='width:170px'  onchange='hallarEstadoProyecto()' class='form-control'>");
		var today1 = convertirFechaInput(fechaFin);
		$("#inputProyectoFin").val(today1);
		//
		var textPostulante=$("#proycontra"+ProyectoId).text();
		$("#proycontra"+ProyectoId).addClass("linkGosst")
		.on("click",function(){verPostulantesProyecto();})
		.html("<i class='fa fa-list fa-2x' aria-hidden='true' ></i>"+textPostulante);
		//
		$("#proyrs" + ProyectoId).html(
		"<input type='text' id='inputProyectoRespSst' class='form-control'>");
		$("#inputProyectoRespSst").val(respSst);

		$("#proyrsc" + ProyectoId).html(
		"<input type='text' id='inputProyectoRespCorreo' class='form-control'>");
		$("#inputProyectoRespCorreo").val(respSstCorreo);
		for(var i=1;i<=5;i++){ 
			var textAct=$("#"+i+"proyact"+ProyectoId).html(); 
			$("#"+i+"proyact"+ProyectoId).addClass("linkGosst")
			.attr("onclick","verActividadesProyecto("+i+")")
			.html("<i class='fa fa-list fa-2x' aria-hidden='true' ></i>"+textAct); 
		}
		//
		var textIncidente=$("#proyinc"+ProyectoId).html();
		$("#proyinc"+ProyectoId).addClass("linkGosst")
		.on("click",function(){verResumenIncidentesAuditoria();})
		.html("<i class='fa fa-warning fa-2x' aria-hidden='true' ></i>"+textIncidente);
		//
		var textObs=$("#proyobs"+ProyectoId).html();
		$("#proyobs"+ProyectoId).addClass("linkGosst")
		.on("click",function(){verObservaciones(0);})
		.html("<i class='fa fa-eye fa-2x' aria-hidden='true' ></i>"+textObs);
		//
		var textObs=$("#proyobsctr"+ProyectoId).html();
		$("#proyobsctr"+ProyectoId).addClass("linkGosst")
		.on("click",function(){verObservaciones(1);})
		.html("<i class='fa fa-eye fa-2x' aria-hidden='true' ></i>"+textObs);
		//
		var textAudi=$("#proyaudi"+ProyectoId).html();
		$("#proyaudi"+ProyectoId).addClass("linkGosst")
		.on("click",function(){cargarAuditoriasProyecto(3);})
		.html("<i class='fa fa-balance-scale  fa-2x' aria-hidden='true' ></i>"+textAudi);
		//
		var textIns=$("#proyinsp"+ProyectoId).html();
		$("#proyinsp"+ProyectoId).addClass("linkGosst")
		.on("click",function(){cargarAuditoriasProyecto(2);})
		.html("<i class='fa fa-search  fa-2x' aria-hidden='true' ></i>"+textIns);
		//
		
		var textIper=$("#proyiperc"+ProyectoId).html();
		$("#proyiperc"+ProyectoId).addClass("linkGosst")
		.on("click",function(){verEvaluacionIpercProyecto();})
		.html("<i class='fa fa-list fa-2x' aria-hidden='true' ></i>"+textIper);
		//
		var textForm=$("#proycap"+ProyectoId).text();
		$("#proycap"+ProyectoId).addClass("linkGosst")
		.on("click",function(){verFormacionesProyecto();})
		.html("<i class='fa fa-graduation-cap fa-2x' aria-hidden='true' ></i>"+textForm);
		////
		var textForm1=$("#proyevi"+ProyectoId).html();
		$("#proyevi"+ProyectoId).addClass("linkGosst")
		.on("click",function(){verCursosProyecto();})
		.html("<i class='fa fa-graduation-cap fa-2x' aria-hidden='true' ></i>"+textForm1);
		////
		var textTrab=$("#proytrab"+ProyectoId).html();
		$("#proytrab"+ProyectoId).addClass("linkGosst")
		.on("click",function(){verTrabajadoresProyecto();})
		.html("<i class='fa fa-users fa-2x' aria-hidden='true' ></i>"+textTrab);
			
		 
		 
		banderaEdicion4 = true;
		hallarEstadoProyecto();
		
		//$("#btnConsolidadoAudiProyecto").show();
		
		
		$("#btnResumenUnidadAudiProyecto").hide();
		$("#btnResumenUnidadDetalAudiProyecto").hide();
		$("#btnResumenAudiProyecto").hide();

		
		
		$("#btnCancelarProyecto").show(); 
		$("#btnNuevoProyecto").hide();
		$("#btnEliminarProyecto").show();
		$("#btnGuardarProyecto").show();
		$("#btnEvaluacionTotalProyecto").show();
		$("#btnResObsProy").show();
		$("#btnGenReporte").hide();
	}
	
}
function verTablaResumenObservacionProyecto(){
	$("#tabProyectos .container-fluid").hide();
	$("#divResObsProy").show();
	$("#tabProyectos h2").html("<a onclick='volverDivProyectos()'>Proyecto ("+
			proyectoObj.titulo+")</a> > Resumen de observaciones");
	//
	$(".camposFiltroTabla").show();
	crearSelectOneMenuObligMultipleCompleto("selMultipleObsProy", "", selEstadoObs, 
			"id","nombre","#divSelectFiltroObs","Seleccione estados");
	
	$(".camposFiltroTabla").hide();
	$("#btnAplicarFiltro").hide();
	
	$("#divObsProyTabla form").on("submit",function(e){
		e.preventDefault(); 
	});
	resizeDivWrapper("wrapperObsProy",300);
	var proyectoObjSend={
			 id:proyectoObj.id 
		 }
		callAjaxPost(URL + '/contratista/proyecto/observaciones', 
				proyectoObjSend, procesarResumenobservacionesProyectoVistaContratista);
}
function verActividadesProyecto(tipoId){
	$("#tabProyectos .container-fluid").hide();
	$("#divActProyecto").show();
	var nombreTipoActividad="";
	switch(tipoId){
	case 1:
		nombreTipoActividad=listTipoActividades[0].nombre
		break;
	case 2:
		nombreTipoActividad="Plan de emergencia"
		break;
	case 3:
		nombreTipoActividad="Programa"
		break;
	case 4:
		nombreTipoActividad="Plan de manejo de residuos"
		break;
	case 5:
		nombreTipoActividad=listTipoActividades[3].nombre
		break;	
	}
	$("#tabProyectos h2").html("<a onclick='volverDivProyectos()'>Proyecto ("+proyectoObj.titulo+")</a> > "+nombreTipoActividad+"");
	 
	verCompletarActividadesProyecto(tipoId);
	//cargarActProyectosSeguridad();
}
function verTrabajadoresProyecto(){
	$("#tabProyectos .container-fluid").hide();
	$("#divTrabProyecto").show();
	$("#tabProyectos h2").html("<a onclick='volverDivProyectos()'>Proyecto ("+proyectoObj.titulo+")</a> > Trabajadores del Contratista");
	 
	verCompletarTrabajadoresProyecto();
}
var listFormacionProyecto;
function verFormacionesProyecto(){
	
	$("#btnGuardarCapProy").attr("onclick","guardarFormacionesProyecto()");
	$("#tabProyectos .container-fluid").hide();
	$("#divCapProyecto").show();
	$("#tabProyectos h2").html("<a onclick='volverDivProyectos()'>Proyecto ("+proyectoObj.titulo+")</a> > Capacitaciones");
	
	listFormacionProyecto=[];
	llamarFormacionesProyecto();
}
function llamarFormacionesProyecto(){
	var dataParam = {
			id : ProyectoId,
			empresaId : getSession("gestopcompanyid")
	};

	callAjaxPostNoLoad(URL + '/contratista/proyecto/formaciones', dataParam, function(data) {
		listFormacionProyecto=data.list;
	$("#tblFormProy tbody").html("");
	listFormacionProyecto.forEach(function(val,index){
		var isAsignado=(val.isAsociado==1?"checked":"");
		if(val.contratistaAsociado==null){
			val.contratistaAsociado={nombre:"",isAprobado:0};
		}
		var numVigentes=(val.contratistaAsociado.isAprobado==1?"Aprobado":"");
		 
		$("#tblFormProy tbody").append("<tr >" +
				"<td>" +val.capacitacionNombre+"</td>"+
				"<td>" +"<input id='formProy"+val.capacitacionId+"' class='form-control' type='checkbox' "+isAsignado+">"+
			 "</tr>")
	});
	
	
	});
}
function volverFormacionesProyecto(){
	$("#tabProyectos .container-fluid").hide();
	$("#divCapProyecto").show();
	$("#tabProyectos h2").html("<a onclick='volverDivProyectos()'>Proyecto ("+proyectoObj.titulo+")</a>" +
			"> Formaciones");
}
function verCursosProyecto(){
	$("#btnGuardarCursoProy").attr("onclick","guardarCursosProyecto()");
	$("#tabProyectos .container-fluid").hide();
	$("#divCursoProyecto").show();
	$("#tabProyectos h2").html("<a onclick='volverDivProyectos()'>Proyecto ("+proyectoObj.titulo+")</a> > Extensión de Capacitaciones Online a Trabajadores");
	
	listCursosProyectos=[];
	llamarCursosProyecto();
}
function llamarCursosProyecto(){
	var dataParam = {
			id : ProyectoId 
	};

	callAjaxPostNoLoad(URL + '/contratista/proyecto/cursos/evaluaciones', dataParam, function(data) {
		listCursosProyectos=data.list;
	$("#tblCursoProy tbody").html("");
	listCursosProyectos.forEach(function(val,index){
		var textoBoton ="<button class='btn btn-danger' style='padding:5px' " +
				"onclick='eliminarCursoProyecto("+val.id+")'>" +
		"<i class='fa fa-times' aria-hidden='true'></i></button>";
		var textoBotonConfirm ="<button class='btn btn-success' style='padding:5px' " +
		"onclick='verConfirmacionesEvaluacionAdministrador("+index+")'>" +
"<i class='fa fa-external-link-square' aria-hidden='true'></i></button>";

		 
		$("#tblCursoProy tbody").append("<tr >" +
				"<td>" +val.cursoNombre+"</td>"+
				"<td>" +val.fechaRealizadaTexto+"</td>"+
				"<td>" +val.evaluacionNombre+"</td>"+
				"<td>" +val.confirmacionEvaluacionTotal+textoBotonConfirm+"</td>"+
				"<td>" +textoBoton+"</td>"+
				"</tr>")
	});
	
	
	});
}
function eliminarCursoProyecto(evaltrabajadorId){
	var r=confirm("¿Seguro de eliminar esta evaluación?");
	
	if(r){
		var trabObj={id:evaltrabajadorId};
		callAjaxPostNoLoad(URL + '/capacitacion/curso/trabajadores/evaluacion/delete', trabObj, function(data) {
			llamarCursosProyecto();
		});	
	}
}
var listConfirmacionCursosProyectos=[];
var evalObj={}
function verConfirmacionesEvaluacionAdministrador(pindex){
	evalObj=listCursosProyectos[pindex];
	$("#tabProyectos .container-fluid").hide();
	$("#divConfirmacionCursoProyecto").show();
	$("#tabProyectos h2").html("<a onclick='volverDivProyectos()'>Proyecto ("+proyectoObj.titulo+")</a> " +
			"><a onclick='verCursosProyecto()'>Formación ("+listCursosProyectos[pindex].cursoNombre+") </a>"+
			"> Extensión");
	
	listConfirmacionCursosProyectos=[];
	llamarConfirmacionCursosProyecto();
}
function llamarConfirmacionCursosProyecto(){
	var audiObj={ 
			id:evalObj.id};   
	callAjaxPost(URL + '/capacitacion/curso/evaluacion/confirmaciones', 
				audiObj, function(data){
					if(data.CODE_RESPONSE=="05"){ 
						$("#tblConfirmacion tbody tr").remove();
					 
						 data.list.forEach(function(val,index){
							 var textoBoton ="<button class='btn btn-danger' style='padding:5px' " +
								"onclick='eliminarConfirmacionDirectaCursoProyecto("+val.id+")'>" +
						"<i class='fa fa-times' aria-hidden='true'></i></button>";
								var iconoDescarga=
									"<a href='"+URL+"/capacitacion/curso/evaluacion/confirmacion/evidencia?id="+val.id+"'>" +
											"<i class='fa fa-download fa-2x' aria-hidden='true'></i></a>";
								if(val.evidenciaNombre==""){
									iconoDescarga="";
								}
							$("#tblConfirmacion tbody").append(
									"<tr id='trtip"+val.id+"' >" +
 								 "<td >"+val.fechaPlanificadaTexto+"</td>" +
									 "<td >"+val.fechaRealTexto+"</td>" +
									"<td >"+val.evidenciaNombre+iconoDescarga+"</td>" +
									"<td >"+val.estado.nombre+"</td>" +
									"<td >"+textoBoton+"</td>" +
									
									 
									"</tr>");
						});
					}else{
						console.log("NOPNPO")
					}
				});
}
function eliminarConfirmacionDirectaCursoProyecto(pid){
	var r = confirm("¿Está seguro de eliminar la extensión?");
	if (r == true) {
		var dataParam = {
				id : pid
		};

		callAjaxPost(URL + '/capacitacion/curso/evaluacion/confirmacion/delete',
				dataParam, llamarConfirmacionCursosProyecto);
	}
}
function verEvaluacionProyectoFormaciones(pindex){
	var dataParam={capacitacionId:listFormacionProyecto[pindex].capacitacionId,
			proyecto:{id:ProyectoId}};
	var vigAux=[],asigAux=[];
	callAjaxPost(
			URL + '/capacitacion/contratistas/formacion',
			dataParam,function(data){
				if(data.CODE_RESPONSE=="05"){
					var contratistasPermitidos=listFormacionProyecto[pindex].contratistas;
					var idPermitido=listarStringsDesdeArray(contratistasPermitidos,"id");
					var contratistasEvaluados=data.contratistasEvaluados;
					contratistasEvaluados=contratistasEvaluados.filter(function(val,index){
						 if(idPermitido.indexOf(val.contratista.id)!=-1){
							 return true;
						 }else{
							 return false;
						 }
					});
					
					$("#tabProyectos h2").html("<a onclick='volverDivProyectos()'>Proyecto ("+proyectoObj.titulo+")</a>" +
							"> <a onclick='volverFormacionesProyecto()'>Formación ("+listFormacionProyecto[pindex].capacitacionNombre+") </a>" +
									"> Evaluaciones");
					
					$("#tabProyectos .container-fluid").hide();
					$("#divEvalFormacionProy").show();
					
					$("#tblEvalFormacion tbody tr").remove();
					vigAux=[];
					asigAux=[];
					contratistasPermitidos.forEach(function(val,index){
					$("#tblEvalFormacion tbody").append("" +
								
					"<tr id='ft"+val.id+"'>" +
					
					"<td>"+val.nombre+"</td>" +
					"<td>"+"---"+"</td>" +
					"<td>"+"---"+"</td>" +
					"</tr>")	;
						$("#ft"+val.id+" td").css({
							"background-color":"white"
						})
					});
					contratistasEvaluados.forEach(function(val,index){
						$("#ft"+val.contratista.id).remove()	;
						$("#tblEvalFormacion tbody").prepend("" +
								
								"<tr id='ft"+val.contratista.id+"'>" +
								
								"<td>"+val.contratista.nombre+"</td>" +
								"<td>"+val.resultadoUltimo+" -"+val.estadoEvaluacion.nombre+"</td>" +
								"<td>"+val.diasRestantes+"</td>" +
								"</tr>");
						var colorNota="";
						if(val.estadoEvaluacion.id==1){
							colorNota="#9BBB59";
						}else{
							colorNota="#C0504D";
						}
						$("#ft"+val.contratista.id+" td").css({
							"background-color":colorNota
						})
						
						});
				}else{
					console.log("NOP")
				}
			})
}
function guardarFormacionesProyecto(){
	var listEnviar=[];
	
	listFormacionProyecto.forEach(function(val,index){
		val.proyecto={};
		if($("#formProy"+val.capacitacionId).prop("checked")){
			val.proyecto.id=ProyectoId;
			val.isAsociado=1;
		}else{
			val.proyecto.id=ProyectoId;
			val.isAsociado=0;
		}
	});
 
	callAjaxPost(URL + '/contratista/proyecto/formaciones/save', listFormacionProyecto, function(data) {
		alert("Asignaciones guardadas")
		volverDivProyectos();
	},null);
}
function nuevoProyecto() {
	if (!banderaEdicion4) {
		ProyectoId = 0;
		var slcResponsableProy = crearSelectOneMenuOblig("slcResponsableProy", "",
				listResponsableProy, "-1", "id", "nombre");
		//
		var  slcTiposEval = crearSelectOneMenuOblig("slcTiposEval", "",
				listTipoEvaluacion,"" , "id", "nombre");
		
		//
		var selAreaProyecto = crearSelectOneMenuOblig("selAreaProyecto", "",
				listAreaProyecto, "", "id", "nombre");
		//
		var selTipoIpercProy = crearSelectOneMenuOblig("selTipoIpercProy", "",
				listTipoIpercProy, "", "id", "nombre");
		$("#tblProyectos:first tbody")
				.prepend(
						"<tr id='0'>" 
						+"<td><input type='text' id='inputProyectoTitulo' class='form-control' autofocus='true'></td>"
						+"<td><input type='text' id='inputProyectoDesc' class='form-control'></td>"		
						+ "<td>"+slcResponsableProy+"</td>"
						+ "<td id='proyresp0'>"+""+"</td>"
						 + "<td>"+selAreaProyecto+"</td>"
						+"<td><input type='date' id='inputProyectoInicio' onchange='hallarEstadoProyecto()' class='form-control'></td>"
						+"<td><input type='date' id='inputProyectoFin' onchange='hallarEstadoProyecto()' class='form-control'></td>"
						+"<td>"+slcTiposEval+"</td>"
						
						+"<td>...</td>"
						+ "<td>"+selTipoIpercProy+"</td>"
						+ "<td>...</td>"
						+ "<td>...</td>"
						+ "<td>...</td>"
								+ "<td>...</td>"
								+ "<td>...</td>"
								+ "<td>...</td>"
						+ "<td>...</td>"
						+ "<td>...</td>"
						+ "<td>...</td>"
								+ "<td>...</td>"
								+ "<td>...</td>"
								
								+ "<td>...</td>"
								+ "<td>...</td>"
								+ "<td id='proyestado0'>...</td>"
								+ "<td>...</td>"
								+ "<td>...</td>"
										+ "<td>...</td>"
										+ "<td>...</td>"
										+ "<td>...</td>"
										
										+ "<td>...</td>"
										+ "<td>...</td>"
								+ "</tr>");
		listResponsableProy.forEach(function(val){val.selected=0});
		var slcTrabajadoresProyecto=crearSelectOneMenuObligMultipleCompleto("slcRespSecundarios", "",
				listResponsableProy,  "id", "nombre","#proyresp"+ProyectoId,"Secundarios...");
		
		$("#btnCancelarProyecto").show();
		$("#btnNuevoProyecto").hide();
		$("#btnEliminarProyecto").hide();
		$("#btnGuardarProyecto").show();
		$("#btnGenReporte").hide();
		
		$("#btnResumenUnidadAudiProyecto").hide();
		$("#btnResumenUnidadDetalAudiProyecto").hide();
		$("#btnResumenAudiProyecto").hide();

		banderaEdicion4 = true;
		formatoCeldaSombreableTabla(false,"tblProyectos");
	} else {
		alert("Guarde primero.");
	}
}
function cancelarProyecto() {
	cargarTiposContratista();
}
function eliminarProyecto() {
	var r = confirm("¿Está seguro de eliminar el proyecto?");
	if (r == true) {
		var dataParam = {
				id : ProyectoId,
		};

		callAjaxPost(URL + '/contratista/proyecto/delete', dataParam,
				function(data) {
					switch (data.CODE_RESPONSE) {
					case "05":
						cargarProyectosSeguridad();
						break;
					default:
						alert("Ocurrió un error al eliminar la programacion!");
					}
				});
	}
}
function guardarProyecto() {

	var campoVacio = true;
	var titulo=$("#inputProyectoTitulo").val();
	var descripcion=$("#inputProyectoDesc").val();
	var responsableSST=$("#inputProyectoRespSst").val();
	var responsableCorreo=$("#inputProyectoRespCorreo").val(); 

	var fechaInicio = convertirFechaTexto($("#inputProyectoInicio").val()); 
	var fechaFin = convertirFechaTexto($("#inputProyectoFin").val());
	var listaId=$("#slcTiposEval").val();

 

var responsableId=$("#slcResponsableProy").val();
var areaId=$("#selAreaProyecto").val();
var tipoIperc=$("#selTipoIpercProy").val();

var respId=$("#slcRespSecundarios").val();
var responsablesFinal=[];
respId.forEach(function(val,index){
	responsablesFinal.push({id:val});
});
		if (campoVacio) {

			var dataParam = {
				id : ProyectoId,responsablesSecundarios:responsablesFinal,
				responsableCorreo:responsableCorreo,
				descripcion:descripcion,
				titulo:titulo,
				estado:proyectoEstado,
				responsableSST:responsableSST,
				responsable:{id:responsableId},
				fechaInicio:fechaInicio,
				fechaFin:fechaFin,
				lista:{id:listaId},
				area:{id:areaId},
				tipoIperc:{id:tipoIperc},
empresaId : getSession("gestopcompanyid")
			};

			callAjaxPost(URL + '/contratista/proyecto/save', dataParam,
					function(data) {
						switch (data.CODE_RESPONSE) {
						case "05":
							var nuevoId=	data.nuevoId;
							if(ProyectoId>0){
								cancelarNuevoProyecto();
							}else{
								agregarNuevoRegistroProyecto(nuevoId);
							}
							break;
						default:
							console.log("Ocurrió un error al guardar la programacion!");
						}
					});
			
		
		
			
			
			
			
		} else {
			alert("Debe ingresar todos los campos.");
		}
	
	
}

function agregarNuevoRegistroProyecto(valNuevo){
	callAjaxPost(URL + '/contratista/proyectos', {empresaId:getSession("gestopcompanyid"),id:valNuevo}, function(data) {
		switch (data.CODE_RESPONSE) {
		case "05":
			var listFinal=listFullProyectoSeguridad.concat(data.proyectos);
			 
			agregarTablaFullProyecto(listFinal)
			break;
		}
	})
}
function cancelarNuevoProyecto(){
	callAjaxPost(URL + '/contratista/proyectos', {empresaId:getSession("gestopcompanyid"),id:ProyectoId}, function(data) {
		switch (data.CODE_RESPONSE) {
		case "05":
			var listFinal=[];
			data.proyectos.forEach(function(val){
				listFullProyectoSeguridad.forEach(function(val1,index1){
					if(val1.id==val.id){
						listFinal.push(val)
					}else{
						listFinal.push(val1)
					}
				});
			});
			if(ProyectoId==0){
				listFinal=listFullProyectoSeguridad;
			}
			agregarTablaFullProyecto(listFinal)
			break;
		}
	})
}
function hallarEstadoProyecto(){
	var fechaInicio=$("#inputProyectoInicio").val();
	var fechaFin=$("#inputProyectoFin").val();
	var hoyDia=obtenerFechaActual();
	var difInicio= restaFechas(hoyDia,fechaInicio);
	var difFinal= restaFechas(hoyDia,fechaFin);
	if(difInicio>0){
		proyectoEstado={id:"1",nombre:"Pendiente"};
	}else{
		if(difFinal>0){
			proyectoEstado={id:"2",nombre:"En Curso"};
		}else{
			proyectoEstado={id:"3",nombre:"Completado"};
		}
	}
	
	$("#proyestado"+ProyectoId).html(proyectoEstado.nombre+"");
	
}

