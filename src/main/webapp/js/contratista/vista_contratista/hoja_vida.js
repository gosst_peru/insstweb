var hojaVidaTrabajadorId;
var hojaVidaTrabajadorObj=[];
var banderaEdicion69=false;
var listFullHojaVidaTrabajador;
var estadoHojaVida;

 var tipoHojaTrabContratista;
/**
 * 
 */
 function toggleMenuOpcionHojaVida(obj,pindex){
	 hojaVidaTrabajadorObj=listFullHojaVidaTrabajador[pindex]; 
		$(obj).parent(".detalleAccion").find("ul").toggle();
		$(obj).parent(".detalleAccion").siblings().find("ul").hide(); 	
	}
var funcionalidadesHojaTrabajador=[ 
/*{id:"opcDescarga",nombre:"<i class='fa fa-download'></i>&nbspDescargar",functionClick:function(data){
	window.open(URL+"/contratista/trabajador/hojavida/evidencia?id="+hojaVidaTrabajadorObj.id,'_blank') }  },*/

{id:"opcEditar",nombre:"<i class='fa fa-pencil-square-o'></i>&nbspEditar",functionClick:function(data){editarHojaVidaTrabajador()}  },
{id:"opcElimnar",nombre:"<i class='fa fa-trash-o'></i>&nbspEliminar",functionClick:function(data){eliminarHojaVidaTrabajador()}  }
                               ]
function marcarSubOpcionHojaVidaTrabajador(pindex){
	funcionalidadesHojaTrabajador[pindex].functionClick();
	$(".listaGestionGosst").hide();
}
function cargarHojaVidaTrabajador(tipoHoja,pindex) { 
	pindex=defaultFor(pindex,indexTrabContratista);
	tipoHoja=defaultFor(tipoHoja,tipoHojaTrabContratista);
	 //
	$(".divMovilHistorialTrabajador").html("");	
	$("#divHistorialTrab"+listFullTrabajadoresContratista[pindex].id)
	.siblings(".divMovilHistorialTrabajador")
	  .hide();
	
	$("#divHistorialTrab"+listFullTrabajadoresContratista[pindex].id)
						.show()
	//
	var trabId=listFullTrabajadoresContratista[pindex].id; 
	var objTrabajadorContratistaSend = {
			id : trabId 
	};
	objTrabajadorContratista.id=trabId;
	indexTrabContratista=pindex;
	tipoHojaTrabContratista=tipoHoja;
	callAjaxPost(URL + '/contratista/trabajador/hojavidas', 
			objTrabajadorContratistaSend, function(data){
				if(data.CODE_RESPONSE=="05"){
					var menuOpcion="<ul class='list-group listaGestionGosst' >";
					funcionalidadesHojaTrabajador.forEach(function(val1,index1){
						menuOpcion+="<li class='list-group-item' onclick='marcarSubOpcionHojaVidaTrabajador("+index1+")'>"+val1.nombre+" </li>"
					});
					menuOpcion+="</ul>";
					listFullHojaVidaTrabajador=data.list;
					banderaEdicion69=false;
					var indicadorPositivo=0,indicadorTotal=0; 
					$("#divHistorialTrab"+listFullTrabajadoresContratista[pindex].id)
					.html("");
					listFullHojaVidaTrabajador.forEach(function(val,index){
						if(val.tipo.id==tipoHojaTrabContratista){
							
						
						 var eviNombre=(val.evidenciaNombre==""?"<strong>(Sin Archivo)</strong>": "<a  class='efectoLink' target='_blank' "
							 +"href='"+URL+"/contratista/trabajador/hojavida/evidencia?id="+val.id+"'>"+val.evidenciaNombre+"</a>");
						  var textoTiempo="";
						  if(val.tipo.id==1){
							  textoTiempo="<i class='fa fa-clock-o' aria-hidden='true' ></i>Tiempo Asociado: "+val.tiempo +" año(s)<br>";
						  }
						 
						$("#divHistorialTrab"+listFullTrabajadoresContratista[pindex].id)
						.append("<div class='detalleAccion gosst-neutral'  > " +
								"<a class='efectoLink btn-gestion' onclick='toggleMenuOpcionHojaVida(this,"+index+")'>" +
								"Ver Opciones&nbsp<i class='fa fa-angle-double-down' aria-hidden='true'></i></a>" 
								 +menuOpcion+
								"<i aria-hidden='true' class='fa fa-info'></i>"+
								"<strong>"+val.tipo.nombre +"</strong>"+ " ("+val.observacion+"" +")<br>" +
								textoTiempo+
								"<i class='fa fa-download' aria-hidden='true' ></i>"+eviNombre +
								" </div>" +
							"")
							
						}
					});
					$(".listaGestionGosst").hide();
					//$("#trPlanProyecto"+objTrabajadorContratista.id+" .celdaIndicadorProyecto").html(indicadorPositivo+" / "+listFullHojaVidaTrabajador.length)
					formatoCeldaSombreableTabla(true,"tblActProyecto");
				}else{
					console.log("NOPNPO")
				}
			});
	
}

function editarHojaVidaTrabajador() {


	if (!banderaEdicion69) { 
		$("#editarMovilHojaVidaTrab").find(".tituloSubList")
		.html(textoBotonVolverContenido+"Editar   '"+hojaVidaTrabajadorObj.tipo.nombre+"' "+
			" del trabajador "	+listFullTrabajadoresContratista[indexTrabContratista].nombre+"' "); 
		 //
		$(".divListPrincipal>div").hide();
		$("#editarMovilHojaVidaTrab").show(); 
		
		
		$("#inputObsHojaVida").val(hojaVidaTrabajadorObj.observacion); 
		$("#inputTiempoHojaVida").val(hojaVidaTrabajadorObj.tiempo);
		
		var eviNombre=hojaVidaTrabajadorObj.evidenciaNombre;
		var options=
		{container:"#eviHojaVidaTrabajador",
				functionCall:function(){ },
				descargaUrl: "/contratista/trabajador/hojavida/evidencia?id="+hojaVidaTrabajadorObj.id,
				esNuevo:false,
				idAux:"HojaVida",
				evidenciaNombre:eviNombre};
		crearFormEvidenciaCompleta(options); 
			//
		$("#divSelSubTipoHoja").html(hojaVidaTrabajadorObj.tipo.nombre+"<br><small></small>")
		   
		verAcordeTipoHojaVida();
				
		banderaEdicion69 = false;   
	}
	
}
function verAcordeTipoHojaVida(){
	var tipoId=0;
	var listAyuda=[
{tipoId:1,ayuda:"Ej: Trabajo en Altura (superior a 1.8 metros), Supervisor en construcciones"},
{tipoId:2,ayuda:"Ej: Estudios universitarios, formación en institutos, diplomados"},
{tipoId:3,ayuda:"Ej: Inspeccion minuciosa con  escalamiento de estructuras, " +
		"Capacidad para escalar estructuras y realizar trabajos en altura, " +
		"Capacidad visual y auditiva para desarrollar con normalidad las tareas asignadas. "}]
	if(hojaVidaTrabajadorObj.id==0){
		tipoId=$("#selTipoHojaVida").val();
	}else{
		tipoId=hojaVidaTrabajadorObj.tipo.id;
	}
	listAyuda.forEach(function(val,index){
		if(val.tipoId==tipoId){
			$("#divSelSubTipoHoja").find("small").html(val.ayuda);
		}
	});
	if(tipoId==1){
		$("#divTiempoHojaVida").parent().show();
	}else{
		$("#divTiempoHojaVida").parent().hide();
	}
	$("#divTiempoHojaVida").parent().hide();
	$("#divSelSubTipoHoja").parent().hide();
	
}
function nuevaHojaVidaTrabajador() {
	if (!banderaEdicion69) {
		hojaVidaTrabajadorObj = {id:0,evidenciaNombre:""};
		
		$("#editarMovilHojaVidaTrab").find(".tituloSubList")
		.html(textoBotonVolverContenido+"Nueva Hoja de Vida "
			+" del trabajador '"	+objTrabajadorContratista.nombre+"' ");
		var options=
		{container:"#eviHojaVidaTrabajador",
				functionCall:function(){ },
				descargaUrl: "",
				esNuevo:true,
				idAux:"HojaVida",
				evidenciaNombre:""};
		crearFormEvidenciaCompleta(options );
		var sug="Se recomienda usar el formato registrado por la empresa";
		$(".divListPrincipal>div").hide();
		$("#editarMovilHojaVidaTrab").show();
		$("#inputObsHojaVida").val("");
		$("#inputTiempoHojaVida").val("");
		$("#divSelSubTipoHoja").html(crearSelectOneMenuOblig("selTipoHojaVida", "verAcordeTipoHojaVida()", listSubTipoHojaVida, 
				"","id","nombre")+"<small></small>" );
		$("#eviHojaVidaTrabajador").append("<small>"+sug+"</small>")
		verAcordeTipoHojaVida();
		
		banderaEdicion69 = false;
	} else {
		alert("Guarde primero.");
	}
}

function cancelarActProyecto() {
	cargarTiposContratista();
}

function eliminarHojaVidaTrabajador() { 
	var r = confirm("¿Está seguro de eliminar la hoja de vida?");
	if (r == true) {
		var dataParam = {
				id : hojaVidaTrabajadorObj.id,
		};

		callAjaxPost(URL + '/contratista/trabajador/hojavida/delete', dataParam,
				function(data) {
					switch (data.CODE_RESPONSE) {
					case "05":
						habilitarTrabajadoresContratista();
						break;
					default:
						alert("Ocurrió un error al eliminar la programacion!");
					}
				});
	}
}

function guardarHojaVidaTrabajador(functionCallBack) {

	var campoVacio = true;
	var tipo=$("#selTipoHojaVida").val();   
var observacion=$("#inputObsHojaVida").val();
var tiempo=$("#inputTiempoHojaVida").val();
var inputFileImage = document.getElementById("fileEviHojaVida");
var file = inputFileImage.files[0];
if(!file){
	if(hojaVidaTrabajadorObj.evidenciaNombre!=""){
		campoVacio=true;
	}else{
		campoVacio=false;
	}
	
}
		if (campoVacio) {

			var dataParam = {
				id : hojaVidaTrabajadorObj.id, 
				tipo: {id:4}, 
				observacion:observacion, 
				tiempo:tiempo, 
				trabajador:{id:objTrabajadorContratista.id}
			};

			callAjaxPost(URL + '/contratista/trabajador/hojavida/save', dataParam,
					function(data) {
						switch (data.CODE_RESPONSE) {
						case "05": 
							guardarEvidenciaAuto(data.nuevoId,"fileEviHojaVida"
									,bitsEvidenciaArchivoCurso,"/contratista/trabajador/hojavida/evidencia/save"
									,function(rpta){
								
								if(functionCallBack!=null){
									 alert("Guardado");
									functionCallBack();
								}else{
									volverDivSubContenido();
									habilitarTrabajadoresContratista();
								}
							});
						 
							break;
						default:
							console.log("Ocurrió un error al guardar la hoja vida!");
						}
					},null,null,null,null,false);
			 
		} else {
			alert("Debe ingresar evidencia");
		}
	
	
}


function cancelarNuevoActProyecto(){
	cargarActProyectosSeguridad();
}

 


