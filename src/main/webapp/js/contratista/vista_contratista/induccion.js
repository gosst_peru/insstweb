/**
 * 
 */
var listTrabajadoresInduccion;
var objInduccionContratista;
var listFullInduccionesContratista;
var funcionalidadesInduccion=[ 
{id:"opcDescarga",nombre:"Descargar",functionClick:function(data){
	window.open(URL+"/contratista/induccion/informe?contratistaId="+getSession("contratistaGosstId") +
			"&induccionId="+objInduccionContratista.id,'_blank')
	}  },
       {id:"opcEditar",nombre:"<i class='fa fa-pencil-square-o'></i> Inscribir trabajadores",functionClick:function(data){editarTrabajadoresInduccionContratista()}  
       },
       {id:"opcEditasdfr",nombre:"<i class='fa fa-check-square-o'></i> Evaluación trabajadores",functionClick:function(data){verEvaluacionInduccionTrabajadores()} 
       
       }
      ];
var funcionalidadesInduccionCompleta=[ 
{id:"opcDescarga",nombre:"<i class='fa fa-download'></i> Descargar",functionClick:function(data){
	window.open(URL+"/contratista/induccion/informe?contratistaId="+getSession("contratistaGosstId") +
			"&induccionId="+objInduccionContratista.id,'_blank')}  
},
      
       {id:"opcEditasdfr",nombre:"<i class='fa fa-check-square-o'></i> Evaluación trabajadores",functionClick:function(data){verEvaluacionInduccionTrabajadores()} 
       
       }
      ];
function verEvaluacionInduccionTrabajadores(){
	$(".divListPrincipal>div").hide();
	var divEval=$("#inducEvaluacion");
	divEval.show();
	divEval.find(".tituloSubList").html(textoBotonVolverContenido+
				"Evaluaciones de Induccion del "+objInduccionContratista.fechaTexto+
				" Tema: "+objInduccionContratista.tema);
	divEval.find(".contenidoSubList").html("");
	
	var dataParam={
			id:objInduccionContratista.id,
			contratista:{id:getSession("contratistaGosstId")}
	}
	callAjaxPost(URL + '/contratista/induccion/trabajadores', dataParam, function(data) {
		var textIn="";
		var evala=data.trabajadores;
		listTrabajadoresInduccion=data.trabsExamen;
		var textTrabs="";
		listTrabajadoresInduccion.forEach(function(val,index){
			val.selected=0;
			
			evala.forEach(function(val1,index1){
				var estiloGosst="gosst-neutral";
				if(val1.trabajador.id==val.id){
					if(val1.evaluacion.id==1){
						estiloGosst="gosst-aprobado";
					}
					textIn+=("<div class='eventoGeneral "+estiloGosst+"'>" +
							 
							"<i class='fa  fa-info' aria-hidden='true'></i> <strong> " +val.nombreCompleto+"</strong><br>"+ 
							"<i class='fa  fa-info' aria-hidden='true'></i> Asistencia: " +val1.asistencia.nombre +"<br>"+ 
							"<i class='fa  fa-info' aria-hidden='true'></i> Evaluación : " +val1.evaluacion.nombre+" hora(s) <br>"+
							"</div>");
				}
			});  
			
		});
		divEval.find(".contenidoSubList").html(""+textIn);
	});
	
	
}
function marcarSubOpcionInduccion(pindex){
	funcionalidadesInduccion[pindex].functionClick();
	$(".subDetalleAccion ul").hide();
	$(".listaGestionGosst").hide();
	
	
}
function marcarSubOpcionInduccionCompleta(pindex){
	funcionalidadesInduccionCompleta[pindex].functionClick();
	$(".subDetalleAccion ul").hide();
	$(".listaGestionGosst").hide();
	
	
}
function toggleMenuOpcionInduccion(obj,pindex){
	objInduccionContratista=listFullInduccionesContratista[pindex]; 
	$(obj).parent(".eventoGeneral").find("ul").toggle();
	$(obj).parent(".eventoGeneral").siblings().find("ul").hide(); 	
}
function habilitarInduccionesEmpresaContratista(){
	objInduccionContratista={id:0};  
	var contratistaId=parseInt(getSession("contratistaGosstId"));
	var dataParam={ 
			id:(contratistaId) 
			};
	callAjaxPost(URL + '/contratista/inducciones/contratista', dataParam, function(data) {
		switch (data.CODE_RESPONSE) {
		case "05":
			var listPanelesPrincipal=[];
			var textIn= "",textInImplementar="",textInCompletado="";
			var induccionesImplementar=0,induccionesCompletadas=0;
			listFullInduccionesContratista=data.examenes; 
			listTrabajadoresInduccion=data.trabsExamen;
			$(".divListPrincipal").html("");
			listFullInduccionesContratista.forEach(function(val,index){
				
				var estiloGosst="gosst-neutral";				
				if(val.isVigente==1){
					estiloGosst="gosst-aprobado"
				}
				var hoy=new Date();
				 //
				if(val.hora==null){
			    	val.hora="00:00:00"
			    }
			   var aF=convertirFechaInput(val.fecha).split("-");
			   var aH=(val.hora).split(":");
			   var	fecha=new Date(aF[0],parseInt(aF[1])-1,aF[2],aH[0]-2,aH[1]);
				// 
			    var dias=0
			    var horas=0
			    var minutos=0
			    var segundos=0
			    var textHora="";
			    var menuOpcion="<ul class='list-group listaGestionGosst' >";
				 var diferencia=(fecha.getTime()-hoy.getTime())/1000;
		        dias=Math.floor(diferencia/86400)
		        diferencia=diferencia-(86400*dias)
		        horas=Math.floor(diferencia/3600)
		        diferencia=diferencia-(3600*horas)
		        minutos=Math.floor(diferencia/60)
		        diferencia=diferencia-(60*minutos)
		        segundos=Math.floor(diferencia);
		        if ( fecha>hoy && val.fechaReal==null){
		            textHora="<i class='fa fa-calendar' aria-hidden='true'></i>" +
		            		"<strong>Quedan "+dias+" dia(s) " +horas+" hora(s) "+minutos+" minuto(s) para inscribirse</strong> <br>";
		            funcionalidadesInduccion.forEach(function(val1,index1){
						menuOpcion+="<li class='list-group-item' onclick='marcarSubOpcionInduccion("+index1+")'>"+val1.nombre+" </li>"
					});
		        }else{
		        	funcionalidadesInduccionCompleta.forEach(function(val1,index1){
						menuOpcion+="<li class='list-group-item' onclick='marcarSubOpcionInduccionCompleta("+index1+")'>"+val1.nombre+" </li>"
					});
		        }
		       
		        menuOpcion+="</ul>";
		        var textFilaEvidencia="";
		        if(val.evidenciaNombre.length>0){
		        	textFilaEvidencia="<i class='fa fa-file' aria-hidden='true'></i> " +val.evidenciaNombre+"<br>";
		        }
		        var textNormal="<i class='fa fa-calendar' aria-hidden='true'></i>" +val.fechaTexto+" "+val.horaTexto+"";
		        var textResalta="<strong style='color: red'>" +
		        		"<i class='fa fa-calendar' aria-hidden='true'></i>" +val.fechaTexto+" "+val.horaTexto+"</strong>";
				textIn=("<div class='eventoGeneral "+estiloGosst+"'>" +
					"<a class='efectoLink btn-gestion ' onclick='toggleMenuOpcionInduccion(this,"+index+")'>Ver Opciones <i class='fa fa-angle-double-down'></i></a>" +
					"<i class='fa  fa-caret-down' aria-hidden='true'></i></button>" +
					menuOpcion+
					"<i class='fa  fa-info' aria-hidden='true'></i> Tema: " +val.tema+"<br>"+ 
					"<i class='fa  fa-info' aria-hidden='true'></i> Profesor: " +val.profesor +"<br>"+ 
					"<i class='fa  fa-info' aria-hidden='true'></i> Duración : " +val.horas+" hora(s) <br>"+ 
					
					"<i class='fa fa-clock-o' aria-hidden='true'></i> Vigencia: " +val.vigencia.nombre+"<br>"+ 
					"<i class='fa  fa-users' aria-hidden='true'></i>" +val.numTrabajadores+" Trabajador(es) registrados por su empresa<br>"+
					( (val.fechaReal!=null || fecha<hoy) ?textNormal:textResalta)+
					"<br>"+
					""+textHora+
					textFilaEvidencia+ 
					"</div>");
				if(val.fechaReal!=null || fecha<hoy){
					if(val.numTrabajadores>0){
					textInCompletado+=textIn;
					induccionesCompletadas++;
					}
				}else{
					textInImplementar+=textIn;
					induccionesImplementar++;
				}
				
			}); 
			var listItemsExam=[
                 {sugerencia:"",label:"Trabajadores inscritos",inputForm:"",divContainer:"divTrabCupoInduc"},
                 {sugerencia:"",label:"Cupos disponibles",inputForm:"",divContainer:"divCupoInduc"},
            	 {sugerencia:"",label:"Mis Trabajadores",inputForm:"",divContainer:"divSelTrabInduc"},
 			 	 {sugerencia:"",label:"Vigencia",inputForm:"",divContainer:"divVigInduc"},
            	 {sugerencia:"",label:"Fecha planificada",inputForm:" ",divContainer:"divFechaInduc"}, 
            	 {sugerencia:"Ej. documentos, escaneos",label:"Evidencia",inputForm:"",divContainer:"eviExamContratista"},
       			 {sugerencia:"",label:"",divContainer:"divSubmitButton",inputForm:"<button type='submit' class='btn btn-success' ><i aria-hidden='true' class='fa fa-paper-plane'></i>Enviar</button>"} 
             		];
			var textFormExamen="";
			listItemsExam.forEach(function(val,index){
				textFormExamen+=obtenerSubPanelModulo(val);
			});
			listPanelesPrincipal.push( 
					{id:"editarMovilInduccion" ,clase:"contenidoFormVisible",
						nombre:""+"Editar asd",
						contenido:"<form id='formEditarInduccion' class='eventoGeneral'>"+textFormExamen+"</form>"},
					{id:"inducImplementarContratista",clase:"divInduccionContratistaGeneral",
							nombre:"Inducciones Por Llevar (" +induccionesImplementar+")",
							contenido:textInImplementar},
					{id:"inducCompletadasContratista",clase:"divInduccionContratistaGeneral",
						nombre:"Inducciones Completadas (" +induccionesCompletadas+")",
						contenido:textInCompletado},
					{id:"inducEvaluacion",clase:"contenidoFormVisible",
						nombre:" ",
						contenido:""});
			
			agregarPanelesDivPrincipal(listPanelesPrincipal);
			  
 
			$(".listaGestionGosst").hide();
			$(".contenidoFormVisible").hide();
			 
			 $('#formEditarInduccion').on('submit', function(e) {  
			        e.preventDefault(); 
			        guardarInduccionContratista();
			 });
			break;
			default:
				alert("nop");
				break;
		}
	})
}

function editarTrabajadoresInduccionContratista(indexInduccion){
	if(indexInduccion!=null){
		objInduccionContratista=listFullInduccionesContratista[indexInduccion];
	}
	//
	var dataParam={
			id:objInduccionContratista.id,
			contratista:{id:getSession("contratistaGosstId")}
	}
	callAjaxPost(URL + '/contratista/induccion/trabajadores', dataParam, function(data) {
		$(".divListPrincipal>div").hide();
		var editarDiv=$("#editarMovilInduccion");
		editarDiv.show();
		editarDiv.find(".tituloSubList")
		.html(textoBotonVolverContenido+
				"Editar Induccion del "+objInduccionContratista.fechaTexto+
				" Tema: "+objInduccionContratista.tema);
		var evala=data.trabajadores;
		listTrabajadoresInduccion=data.trabsExamen;
		var textTrabs="";
		listTrabajadoresInduccion.forEach(function(val,index){
			val.selected=0;
			evala.forEach(function(val1,index1){
				
				if(val1.trabajador.id==val.id){
					textTrabs+=""+val.nombre+" ("+val1.asistencia.nombre+" - "+val1.evaluacion.nombre+") <br>"
					val.selected=1;
					val.nombre=val.nombre;
				}
			});  
			
		});
		editarDiv.find("#divSelTrabInduc").html("");
		var cuposDisponibles=objInduccionContratista.cupo-objInduccionContratista.numTrabajadoresInscritos;
		var cuposDisponiblesTrabajador=objInduccionContratista.cupo-objInduccionContratista.numTrabajadoresInscritos+objInduccionContratista.numTrabajadores;
		var difDias=restaFechas(convertirFechaInput(objInduccionContratista.fecha),obtenerFechaActualNoUTC());
		
		var eviNombre=objInduccionContratista.evidenciaNombre;
		 var linkDescarg="<a target='_blank' class='btn btn-success' " +
		 		"href='"+URL+"/contratista/examen/evidencia?id="+objInduccionContratista.id+"'>Descargar</a>"
		
		if(false){
			editarDiv.find("#divSelTrabInduc").html(textTrabs);
			$("#formEditarInduccion #divSubmitButton").hide();
			editarDiv.find("#eviExamContratista").html(eviNombre+"<br>"+linkDescarg);
		}else{
			$("#formEditarInduccion #divSubmitButton").show();
			var slcTrabajadoresInduccion=crearSelectOneMenuObligMultipleCompleto("slcTrabajadoresInduccion"+objInduccionContratista.id, "",
					listTrabajadoresInduccion,  "id", "nombreCompleto","#editarMovilInduccion #divSelTrabInduc","Trabajadores ...",cuposDisponiblesTrabajador);
			
			editarDiv.find("#eviExamContratista").html("<strong>Disponible luego de inducción</strong>");
		}
		//
 editarDiv.find("#divTrabCupoInduc").html(objInduccionContratista.numTrabajadoresInscritos); 
	 editarDiv.find("#divCupoInduc").html(cuposDisponibles); 
		editarDiv.find("#divVigInduc").html(objInduccionContratista.vigencia.nombre); 
		editarDiv.find("#divFechaInduc").html((objInduccionContratista.fechaTexto+" "+objInduccionContratista.horaTexto));
		 
		 
	})
	
}

function guardarInduccionContratista(functionCallBack){
	var formDiv=$("#editarMovilInduccion"); 
	 
	var campoVacio = true; 
	var trabsId=formDiv.find("#slcTrabajadoresInduccion"+objInduccionContratista.id).val();
	var trabajadoresFinal=[];
	trabsId.forEach(function(val,index){
		trabajadoresFinal.push(
				{trabajador:{id:val}  });
	});
	if(trabajadoresFinal.length==0){
		alert("Campo trabajadores Obligatorio"); return;
	}
	if (campoVacio) {

			var dataParam = {
				id : objInduccionContratista.id,   
				evaluaciones:trabajadoresFinal,
				fechaTexto:objInduccionContratista.fechaTexto,
				contratista:{id:getSession("contratistaGosstId")}
			};

			callAjaxPost(URL + '/contratista/induccion/trabajadores/save', dataParam,
					function(data) {
						switch (data.CODE_RESPONSE) {
						case "05":
							if(data.mensaje.length>0){
								alert(data.mensaje);
								$.unblockUI();
								return;
							}
							if(functionCallBack!=null){
								functionCallBack();
							}else{habilitarInduccionesEmpresaContratista(); }
							
							break;
						default:
							console.log("Ocurrió un error al guardar   !");
						}
					},null,null,null,null,false);
			 
		} else {
			alert("Debe ingresar todos los campos.");
		}
	
}