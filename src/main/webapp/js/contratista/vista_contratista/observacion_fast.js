/**
 * 
 */
var objObservacionContratista={};
var listObservacionesContratistaGeneral=[];
var listTipoFactoresObsGeneral=[];
var listTipoPerdidaObsGeneral=[];
var listTipoReporteObsGeneral=[];
var listTipoProyectoObsGeneral=[];
var listTipoDirigidoObsGeneral=[];
function habilitarObservacionesFast(){
	var textoConfidencial="Estimado colaborador: " 
		+"<br><br>En conformidad con la Ley N° 29783 “Ley de Seguridad y Salud en el trabajo”, el siguiente reporte es de carácter anónimo y los análisis serán confidenciales y evaluados de manera global. Para garantizar ello, el equipo GOSST ha tomado las medidas necesarias para proteger la información que ingresen los colaboradores, no pudiendo relacionarse un reporte particular con un colaborador determinado. Una vez guardada, no quedará un rastro electrónico que permita asociar a los colaboradores con sus reportes. Cualquier duda sobre este aspecto no dude en consultarnos al correo adrian.cox@aswan.pe" 
		+"<br><br>Muchas gracias por su participación. "
		+"<br><br>GOSST"
		+"<br><br>Tu Seguridad, Nuestra Cultura." +
		"<br><br><button class='btn btn-success' onclick='eliminarAvisoObsGosst()'>Esconder aviso</button>";

	var dataParam={
			id:getSession("contratistaGosstId"),empresaId:getSession("gestopcompanyid")
			};
	objObservacionContratista={id:0};
	var listPanelesPrincipal=[];
	
	callAjaxPost(URL + '/contratista/observaciones/general', dataParam, function(data) {
		switch (data.CODE_RESPONSE) {
		case "05":
			var dataEmpresa=data.empresa;
			var textIn= "";
			var textSinAtender="",textAtenderSinRespuesta="",textAtenderConRespuesta="";
			var numSin=0,numConSinRespuesta=0,numConConRespuesta=0;
			listObservacionesContratistaGeneral=data.list;
			$(".divListPrincipal").html("");
			listTipoFactoresObsGeneral=data.listFactores;
			listTipoPerdidaObsGeneral=data.listPerdida;
			listTipoReporteObsGeneral=data.listReporte;
			listTipoProyectoObsGeneral=data.proyectos;
			listTipoDirigidoObsGeneral=data.listDirigido;
			var slcTipoDirigidoObs=crearSelectOneMenuOblig("slcTipoDirigidoObs", "",
					listTipoDirigidoObsGeneral, "", "id", "nombre");
			var slcTipoProyectoObs=crearSelectOneMenuOblig("slcTipoProyectoObs", "",listTipoProyectoObsGeneral, 
					 "", "id","titulo");
			var slcTipoReporteObs=crearSelectOneMenuOblig("slcTipoReporteObs", "",listTipoReporteObsGeneral, 
					 "", "id","nombre");
			var slcFactorObsGeneral=crearSelectOneMenuOblig("slcFactorObsGeneral", "",listTipoFactoresObsGeneral, 
					 "", "id","nombre");
			listTipoPerdidaObsGeneral=listTipoPerdidaObsGeneral.filter(function(val){
				if(val.id!=1){
					return true;
				}else{
					return false;
				}
			});
			var slcTipoPerdidaObs=crearSelectOneMenuOblig("slcTipoPerdidaObs", "",listTipoPerdidaObsGeneral, 
					 "", "id","nombre");
			var listItemsExam=[
				 {sugerencia:"",label:"Fecha",inputForm:""+obtenerFechaActualNormal()+" "+obtenerHoraActual(),divContainer:""},
				{sugerencia:"",label:"Dirigido a",inputForm:slcTipoDirigidoObs,divContainer:""},
				{sugerencia:"",label:"Proyecto",inputForm:slcTipoProyectoObs,divContainer:""},
				{sugerencia:"",label:"Tipo de reporte",inputForm:slcTipoReporteObs,divContainer:""},
				 {sugerencia:"",label:"Descripción",inputForm:"<input   class='form-control' id='inputDescObsGeneral'>"},
				 {sugerencia:"",label:"Factor",inputForm:slcFactorObsGeneral,divContainer:""},
					
				{sugerencia:"",label:"Nivel de riesgo",inputForm:slcTipoPerdidaObs,divContainer:"divNivelRiesgo"}, 
       			 {sugerencia:"",label:"Evidencia",inputForm:"",divContainer:"eviObservacionGeneral"},
       			 {sugerencia:"",label:"",inputForm:"<button type='submit' class='btn btn-success' ><i aria-hidden='true' class='fa fa-paper-plane'></i>Enviar</button>"} 
             		];
			var textFormExamen="";
			listItemsExam.forEach(function(val,index){
				textFormExamen+=obtenerSubPanelModulo(val);
			});
			listPanelesPrincipal.push(
					{id:"nuevoMovilObsGeneral" ,clase:"",
							nombre:""+"Nueva reporte de observación",
							contenido:"<form id='formNuevoObsGeneral' class='eventoGeneral'>"+textFormExamen+"</form>"}				
			
			);
			
			agregarPanelesDivPrincipal(listPanelesPrincipal);
			//verAcordeTipoReporteActo();
			var options=
			{container:"#eviObservacionGeneral",
					functionCall:function(){ },
					descargaUrl: "",
					esNuevo:true,
					idAux:"ObsGeneral"+objObservacionContratista.id,
					evidenciaNombre:""};
			crearFormEvidenciaCompleta(options);
			$(".listaGestionGosst").hide();
			$(".contenidoFormVisible").hide();
			 $('#formNuevoObsGeneral').on('submit', function(e) {  
			        e.preventDefault();
			        objObservacionContratista={id:0}
			        guardarObservacionGeneral();
			 });
			 
			break;
			default:
				alert("nop");
				break;
		}
	})
}
function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}
function obtenerHoraActual() {
    var d = new Date(); 
    var h = addZero(d.getHours());
    var m = addZero(d.getMinutes());
    var s = addZero(d.getSeconds());
    return  h + ":" + m + ":" + s;
}
function guardarObservacionGeneral(){
	var formDiv=$("#editarMovilObsGeneral");
	if(objObservacionContratista.id==0){
		formDiv=$("#nuevoMovilObsGeneral");
	}
	var campoVacio = true;
	var descripcion=formDiv.find("#inputDescObsGeneral").val();  
	var fecha=new Date();
	var tipo=parseInt(formDiv.find("#slcTipoReporteObs").val());
	var factor=parseInt(formDiv.find("#slcFactorObsGeneral").val());
	var perdida=formDiv.find("#slcTipoPerdidaObs").val();
	var proyectoId=formDiv.find("#slcTipoProyectoObs").val(); 
	var lugar=formDiv.find("#inputLugarObsGeneral").val();
	var contratistaObj={id:getSession("contratistaGosstId")};
	var quienId=formDiv.find("#slcTipoDirigidoObs").val();
	if(parseInt(quienId)==0){
		contratistaObj={id:null}
	}
	var isactivo=comprobarFieInputExiste("fileEviObsGeneral"+objObservacionContratista.id);
	if(isactivo.isValido){
		
	}else{
		alert(isactivo.mensaje)
		return;
	}
	var nivel=1;
	if(tipo==3){
		nivel=2;
	}
		if (campoVacio) {

			var dataParam = {
					id : objObservacionContratista.id,
					proyectoId: proyectoId ,tipo:{id:1},
					descripcion:descripcion,
					nivel:nivel,
					fecha:fecha,hora:obtenerHoraActual(),
					perdida:{id:perdida},
					factor:{id:factor},
					reporte:{id:tipo}, 
					contratistaAsociado:contratistaObj
			}

			callAjaxPost(URL + '/contratista/observacion/save', dataParam,
					function(data) {
						switch (data.CODE_RESPONSE) {
						case "05":  
							
							guardarEvidenciaAuto(data.nuevoId,"fileEviObsGeneral"+objObservacionContratista.id
									,bitsEvidenciaAccionMejora,"/contratista/observacion/evidencia/save",
									function(){
								alert("Se registró correctamente su observación")
								habilitarObservacionesFast();
							},"observacionId"); 
							break;
						default:
							console.log("Ocurrió un error al guardar   !");
						}
					});
			 
		} else {
			alert("Debe ingresar todos los campos.");
		}
}