/**
 * 
 */
var listFullAuditoriaResumenFinal;
function elegirAuditoriaResumen(pindex){
	auditoriaObj = listFullAuditoriaResumenFinal[pindex];
	auditoriaId = auditoriaObj.auditoriaId
}
function cargarFiltrosResumenAuditoriaFinal(tipo){
	if(tipo > 0){
		auditoriaClasif=tipo;
	}
	
	var dataParamAudi = {
			idCompany : getSession("gestopcompanyid"),
			auditoriaClasifId : auditoriaClasif
		};
	callAjaxPost(URL + '/auditoria/filtro', dataParamAudi,
			function(data){
		var listGerencia=data.listGerencia;
		var listAMA=data.listAMA;
		
		$("#selEvalAudiRes").remove();$("#selEvalAMAAudiRes").remove();
		$("#tabResumenAuditoria form label").remove();
		
		var selTipoEvaluacionTotal=crearSelectOneMenuOblig("selEvalAudiRes","cargarResumenAuditoriaFinal()",
				listGerencia,3,"id","nombre");
		$("#tabResumenAuditoria form").append("<label>Filtro Validación por Gerencia: </label>"+selTipoEvaluacionTotal);
		var selTipoEvaluacionAMATotal=crearSelectOneMenuOblig("selEvalAMAAudiRes","cargarResumenAuditoriaFinal()",
				listAMA,"","id","nombre");
		$("#tabResumenAuditoria form").append("<label id='labelFiltroAMA'> Filtro por evaluación de A.M.A: </label>"+selTipoEvaluacionAMATotal);
		
		cargarResumenAuditoriaFinal(tipo);
		
	});	
}
function volverResumenAuditoriaTab(){
	$("#tabResumenAuditoria .container-fluid").hide();
	$("#divGeneResumenAudi").show();
	$("#tabResumenAuditoria h2").html("Resumen Auditoría / Inspecciones");
}
function cargarResumenAuditoriaFinal(tipo){
	volverResumenAuditoriaTab()
	if(tipo > 0){
		auditoriaClasif=tipo;
	}
	var evalAMAid = 0;
	if(parseInt( $("#selEvalAudiRes").val() ) == 1){
		evalAMAid = $("#selEvalAMAAudiRes").val();
		$("#selEvalAMAAudiRes").show();
		$("#labelFiltroAMA").show();
	}else{
		$("#selEvalAMAAudiRes").hide();
		$("#labelFiltroAMA").hide();
		evalAMAid=0;
	}
	var dataParamAudi = {
			idCompany : getSession("gestopcompanyid"),
			estadoImplementacion : $("#selEvalAudiRes").val(),
			estadoEvaluacionAMA:(evalAMAid==0?null:evalAMAid),
			auditoriaClasifId : auditoriaClasif,
			isInforme : 1,
			fechaInicioPresentacion:$("#fechaInicioResFinalAudi").val(),
			fechaFinPresentacion:$("#fechaFinResFinalAudi").val()
		};
	
	callAjaxPost(URL + '/auditoria', dataParamAudi,
			function(data){
		$("#tblResFinalAudi tbody tr").remove();
		listTipoEvaluacionGeneral=data.tipo_evaluacion;
		listTipoEvaluacionGerencia=data.tipo_evaluacion_gerencia;
		listFullAuditoriaResumenFinal=data.list;
		listFullAuditoriaResumenFinal.forEach(function(val,index){
			var rptaSi=val.rptaSi;
			var rptaParc=val.rptaParcial;
			var rptaNo=val.rptaNo;
			var rptaRP=val.rptaRP;
			var puntaje=rptaSi*1+rptaParc*0.5+rptaRP*0.75+rptaNo*0;
			var puntajeMax=rptaSi+rptaParc+rptaRP+rptaNo;
			var textEvi="---";
			if(val.evidenciaNombre != '---'){
				var textSuspensivo="";
				if(val.evidenciaNombre.length > 12){
					textSuspensivo="...";
				}
				textEvi="<a class='efectoLink' target='_blank' href='"+URL+"/auditoria/evidencia?auditoriaId="+val.auditoriaId+"'>"
					+"<i class='fa fa-download'></i>"
					+val.evidenciaNombre.substr(0,12)+textSuspensivo+"</a>"
					 ;
			};
			var celdaResultado="<td  style='color:"+
			colorPuntaje(puntaje/puntajeMax)+";font-weight:bold'>"
			+ pasarDecimalPorcentaje(puntaje/puntajeMax,2) 
			+"<br> ("+puntajeMax+" / "+val.numPreguntas +")" +
					"<br><br><a  onclick='elegirAuditoriaResumen("+index+");getInformeExcelAuditoriaProyecto()' " +
						"class='efectoLink'><i class='fa fa-list'></i>Ver Informe Excel</a></td>"+
			"" ;
			var textAcciones="<a class='efectoLink' onclick='elegirAuditoriaResumen("+index+");verModalAccionesAsociadasAuditoria()'>"
			+val.accionesDirectasTotal+"</a>";
			$("#tdResultRes").html("Resultado")
			if(auditoriaClasif == 2){
				celdaResultado ="<td>"+val.numHallazgos+
				"<br><a  onclick='elegirAuditoriaResumen("+index+");getInformeWebAuditoriaProyecto()' " +
						"class='efectoLink'><i class='fa fa-list'></i>Ver Informe </a>"+"</td>";
				textAcciones="<a class='efectoLink' onclick='elegirAuditoriaResumen("+index+");verModalAccionesAsociadasInspecciones()'>"+
					val.numAccionesCompletasHallazgoProyecto+"/"+val.numAccionesHallazgoProyecto+"</a>";
				$("#tdResultRes").html("Hallazgos")
			}
			var btnNotificar = "<br><a onclick='notificarAuditoriaContratistaResumen("+val.auditoriaId+")'" +
					"class='efectoLink' onclick=''><i class='fa fa-envelope-o'></i> Notificar contratista</a>";
			if(val.evaluacionGerencia.id != 1){
			    btnNotificar="";
			}
			if(val.evaluacionGerencia.id == 1 && val.evaluacion.id == 1){
			    btnNotificar=""; 
			}
		$("#tblResFinalAudi tbody")
		.append("<tr>" +
				"<td>"+val.nroCaso+"</td>" +"<td>"+val.auditoriaNombre+"</td>" +
				"<td>"+val.auditoriaFechaTexto+" ("+val.horaPlanificadaTexto+")</td>" +
				"<td>"+val.contratistaNombre+"</td>" +
				"<td>"+val.proyecto.titulo+"</td>" +
				"<td>"+val.trabajador.nombre+"</td>" +
				celdaResultado+
				"<td>"+textEvi+"</td>"+
				"<td>" +val.evaluacionGerencia.icono+
					"<a class='efectoLink' onclick='elegirAuditoriaResumen("+index+");validarAuditoriaGerencia(1)'>"+
					val.evaluacionGerencia.nombre+"</a>" +
							"<br>("+val.usuarioGerencia.userName+")</td>" +
				"<td>"+val.fechaNotificarTexto+"<br>"+(val.contratistaContacto==null?"":"-"+val.contratistaContacto+"-")+btnNotificar+"</td>" +
				"<td>"+ 
				textAcciones+" </td>" +
				
				"<td>" +val.evaluacion.icono+
				"<a class='efectoLink' onclick='elegirAuditoriaResumen("+index+");validarAuditoriaResponsable(1)'>"+
				val.evaluacion.nombre+"</a>" +"<br>("+val.trabajador.nombre+")</td>"+
				"<td>"+val.evaluacion.fechaTexto+"</td>" +
				 
				"</tr>")	
		});
		
	});
}
function notificarAuditoriaContratistaResumen(pauditoriaId){
	callAjaxPost(URL + '/auditoria/notificar', {auditoriaId:pauditoriaId},
			function(data){
		if(data.CODE_RESPONSE=="05"){
			alert("Se notifico al contratista "+data.contratista.nombre+" al correo " +
					data.contratista.contactoCorreo+". Revise el signo de admiración en la vista " +
							"de proyectos para ver si el contratista toma acciones.")
		}else{
			alert("No se pudo notificar al contratista, correo no válido")
		}
		cargarResumenAuditoriaFinal();
		
	});
}
function getInformeWebAuditoriaProyecto(){
	$("#modalResHallazgo").modal("show");
	
	var dataParam = {
			
			auditoriaId : auditoriaId
		};
		
		callAjaxPost(URL + '/auditoria/hallazgos', dataParam,
				function(data){
			
			$("#tblHallazgoResumenFinal tbody tr").remove();
			var isCompleto = true;
			data.list.forEach(function(val){
				$("#tblHallazgoResumenFinal tbody").append(
						"<tr >"

						+ "<td>" + val.descripcion + "</td>"
						+ "<td>" + val.tipoReporte.nombre + "</td>"						
						+ "<td>" + val.nivel.nombre + "</td>"

						+ "<td>" + val.factor.nombre + "</td>"
						
						+ "<td>" + val.causa
						+ "</td>" 
						+ "<td>" + val.lugar
						+ "</td>" 
												
						+ "<td >" + val.seccionesNombre
						+ "</td>" 
						 
						+ "<td  >"
						+ (isCompleto?(val.evidenciaNombre=="----"?"Sin registrar":"<a class='efectoLink' target='_blank' href='"+URL+"/auditoria/hallazgo/evidencia?hallazgoId="+val.id+"'  >" +
								"<i class='fa fa-download'></i>"+val.evidenciaNombre+"</a>"):val.evidenciaNombre)+"</td>"
						+ "</tr>"
				
				);
			});
			
			
		});
}