var banderaEdicion2=false;
var estadoId=0,estadoObj;
var listFullEstado,listFullTipoEstado,listFullContratista;   

$(function(){
	$("#btnNuevoEstados").attr("onclick", "javascript:nuevoEstado();");
	$("#btnCancelarEstados").attr("onclick", "javascript:cancelarEstado();");
	$("#btnGuardarEstados").attr("onclick", "javascript:guardarEstado();");
	$("#btnEliminarEstados").attr("onclick", "javascript:eliminarEstado();"); 
}) 
function verEstados(){ 
	$("#tabComunicado .container-fluid").hide();
	$("#tabComunicado #divComunicado").hide(); 
	$("#tabComunicado #divEstados").show();
	$("#tabComunicado").find("h2")
	.html("<a onclick=' volverComunicado()'>Comunicado </a>"
			+"> Registro de contratistas:");
	cargarEstados();
}
function cargarEstados() {
	$("#btnCancelarEstados").hide();
	$("#btnNuevoEstados").show();
	$("#btnEliminarEstados").hide();
	$("#btnGuardarEstados").hide();  
	callAjaxPost(URL + '/contratista/comunicado/evaluacion', 
			{
				id:comunicadoObj.id
			}, 
		function(data){
			switch (data.CODE_RESPONSE) {
			case "05":
				banderaEdicion2=false;
				listFullEstado=data.list;  
				listFullTipoEstado=data.listTipoEstado;  
				$("#tblEstados tbody tr").remove();
				listFullEstado.forEach(function(val,index){ 
					if(val.id!=null)
					{
						var nombEviTemp="";
						
						if(val.evidenciaNombre=="")
						{
							nombEviTemp="No Hay Evidencia";
						}else{
							nombEviTemp=val.evidenciaNombre; 
						}
						$("#tblEstados tbody").append(
							"<tr id='trart"+val.id+"' onclick='editarEstado("+index+")'>"
								+"<td id='contrnombre"+val.id+"'>"+val.contratista.nombre+"</td>" 
								+"<td id='controbs"+val.id+"'>"+val.observaciones+"</td>" 
								+"<td id='contrestado"+val.id+"'>"+val.evaluacion.nombre+"</td>"
								+"<td id='contrevi"+val.id+"'>"+nombEviTemp+"</td>"
							+"</tr>");
					}
				});
				completarBarraCarga();
				formatoCeldaSombreableTabla(true,"tblEstados");
				break;
			default:
				alert("nop");
				break;
			} 
		}); 
}
function editarEstado(pindex) 
{
	if(!banderaEdicion2){
		estadoObj=listFullEstado[pindex]; 
		estadoId=estadoObj.id; 
		var descargaEvidencia=URL+"/contratista/comunicado/reporteestado/evidencia?id="+estadoId;
		
		var contratistaNombre=estadoObj.contratista.nombre; 
		$("#contrnombre"+estadoId).val(contratistaNombre);
		
		var obs=estadoObj.observaciones;
		$("#controbs"+estadoId).html("<textarea id='inputObs' class='form-control'></textarea>"	);
		$("#inputObs").val(obs);  
		
		var tipo=estadoObj.evaluacion.id;
		var selTipoEstado=crearSelectOneMenuOblig("inputTipoEstado","",listFullTipoEstado,"","id","nombre");
		$("#contrestado"+estadoId).html(selTipoEstado); 
		$("#inputTipoEstado").val(tipo);  
		
		if(estadoObj.evidenciaNombre=="")
		{
			//No es necesario el link <a> si no hay evidencia
			$("#contrevi"+estadoId).html("No Hay Evidencia" ); 
		}
		else 
		{
			$("#contrevi"+estadoId).html("<a class='efectoLink' href='"+descargaEvidencia+"'>"+ estadoObj.evidenciaNombre+"</a>" ); 			
		}
		//$("#contrevi"+estadoId).prop("disabled",false); 
	
		$("#btnCancelarEstados").show();
		$("#btnNuevoEstados").hide();
		$("#btnEliminarEstados").hide();
		$("#btnGuardarEstados").show(); 
		banderaEdicion2=true;
		formatoCeldaSombreableTabla(false,"tblEstados");
	}

}
function guardarEstado()
{
	var campoVacio=true;
	var idcomuncontr=estadoObj.id;
	var estado=	$("#inputTipoEstado").val();  
	var obs=$("#inputObs").val();  
	if(campoVacio){
		var dataParam=
		{
			id:idcomuncontr,  
			evaluacion:{id: estado},    
			observaciones:obs
		};
		callAjaxPost(URL+'/contratista/comunicado/evaluacion/save',dataParam,
				cargarEstados);
	}
	else{
		alert("Debe ingresar todos los campos.");
	}
}

function cancelarEstado(){
	cargarEstados();
}


