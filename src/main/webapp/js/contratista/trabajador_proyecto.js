var trabajadorProyectoId;
var trabajadorProyectoObj=[];
var banderaEdicion79=false;
var listFullTrabProyecto;
var estadoActividadProy;
$(function(){


})
/**
 * 
 */

function cargarTrabsProyectosSeguridad(pindex) {
	pindex=defaultFor(pindex,indexPostulante);
	 //
	$(".detalleTrabajadoresProyecto").html("");
	$("#editarMovilTrabProy").find(".tituloSubList")
	.html(textoBotonVolverContenido+"Editar trabajadores del proyecto '"
			+listFullProyectoSeguridad[pindex].titulo+"' ");
	
	$("#proyectoTable"+listFullProyectoSeguridad[pindex].id).parent("#tituloEvento")
	.parent(".eventoGeneral").parent(".contenidoSubList")
	.parent(".divProyectoGeneral").siblings()
	.find(".detalleTrabajadoresProyecto").hide();
	
	$("#proyectoTable"+listFullProyectoSeguridad[pindex].id)
						.find(".detalleTrabajadoresProyecto").toggle()
	//
	var proyectoObjSend = {
			id : listFullProyectoSeguridad[pindex].id,
			contratistaId:getSession("contratistaGosstId")
	};
	proyectoObj.id=listFullProyectoSeguridad[pindex].id;
	indexPostulante=pindex;
	callAjaxPost(URL + '/contratista/proyecto/trabajadores', 
			proyectoObjSend, function(data){
				if(data.CODE_RESPONSE=="05"){
					listFullTrabProyecto=data.list;
					banderaEdicion79=false;
					var indicadorPositivo=0,indicadorTotal=0; 
					$("#proyectoTable"+listFullProyectoSeguridad[pindex].id)
					.find(".detalleTrabajadoresProyecto").html("<td colspan='3'></td>");
					listFullTrabProyecto.forEach(function(val,index){
						if(val.proyectoActual!=null && val.isActivo==1){
							val.selected=1;
							indicadorTotal++;
						var claseGosst="gosst-neutral";  
						//
						var verificacionObj=verificarTrabajadorContratistaHabilitado(val);
						if(proyectoObj.evaluacionManual==0){
							
						}else{
							if(val.notaManual==1){
								verificacionObj.isHabilitado=true;
							}else{
								verificacionObj.isHabilitado=false;
							}
						}
						if(verificacionObj.isHabilitado){
							claseGosst="gosst-aprobado";
							indicadorPositivo++;
						}
						var textoExam1="<i class='fa fa-medkit' aria-hidden='true'></i>" +verificacionObj.textoExamen.replace("<br>","")+"<br>"
						var textoExam2="<i class='fa fa-folder-open-o' aria-hidden='true'></i>" +verificacionObj.textoSctr.replace("<br>","")+"<br>" 
						var textoExam3="<i class='fa fa-arrow-circle-o-down' aria-hidden='true'></i>" +verificacionObj.textoInduccion.replace("<br>","")+"<br>" 
						var textoExam4="<i class='fa fa-inbox' aria-hidden='true'></i>" +verificacionObj.textoCargo.replace("<br>","")+"<br>" 
						
						var textoExam6="" +verificacionObj.textoTregLong+"" 
						
						var textoExam8="<i class='fa fa-fire-extinguisher' aria-hidden='true'></i>" +verificacionObj.textoEpp.replace("<br>","")+"<br>"
						var textHojaVida="<i class='fa fa-info' aria-hidden='true'></i>"+""+verificacionObj.eval4.textoEval.replace("<br>","")+"  <br>" ;
						var docsId=proyectoObj.examenesPermitidosId.split(",");
						docsId.forEach(function(val1){
							switch(parseInt(val1)){
							case 16:
								textHojaVida="";
								break;
							case 6:
								textoExam1="";
								break;
							case 7:
								textoExam2="";
								break;
							case 9:
								textoExam3="";
								break;
							case 11:
								textoExam4="";
								break;
							case 12:
								textoExam6="";
								break;
							case 8:
								textoExam8="";
								break;
							}
						});
						$("#proyectoTable"+listFullProyectoSeguridad[pindex].id)
						.find(".detalleTrabajadoresProyecto td") 
						.append( 
						
							"<div class='detalleAccion "+claseGosst+"'>" +
						 
							"<i class='fa fa-user' aria-hidden='true'></i>" +val.nombreCompleto+"<br>"+
							"<i class='fa fa-building-o' aria-hidden='true'></i>" +val.area+"-"+val.cargo+"<br>"+ 
							"<i class='fa  fa-address-card-o' aria-hidden='true'></i>" +val.tipoDocumento.nombre+": "+val.nroDocumento+"<br>"+
							textoExam1+
							textoExam2+
							textoExam3+
							textoExam4+
							
							textoExam6+
							
							textoExam8+
							textHojaVida +
							"<a class='efectoLink' onclick='generarFotocheckTrabajadorContratista("+index+")'><i class='fa fa-download'></i>  Generar Fotocheck</a>" +
								
							"</div>"+
							"<div class='opcionesAccion'>" +
						//	"<a onclick='eliminarTrabProyecto("+index+")'> Eliminar</a>" +
					//			"<i class='fa fa-minus' aria-hidden='true'></i>" +
					//	"<a onclick='editarActProyecto("+index+")'> Editar</a>" +
								" </div>");
							
						}
					});
					$("#trTrabProyecto"+proyectoObj.id+" .celdaIndicadorProyecto").html(indicadorPositivo+" / "+indicadorTotal)
					
				}else{
					console.log("NOPNPO")
				}
			});
	
}
function generarFotocheckTrabajadorContratista(pindex){
	window.open(URL+"/contratista/trabajador/fotocheck?id="
			+listFullTrabProyecto[pindex].id+"&nombre="
			+listFullTrabProyecto[pindex].nombre,"_blank")
}

function nuevoTrabProyecto(pindex) {
	if (!banderaEdicion79) {
		trabajadorProyectoId = 0;
		proyectoObj.id=listFullProyectoSeguridad[pindex].id;
		indexPostulante=pindex;
		var proyectoObjSend = {
				id : listFullProyectoSeguridad[pindex].id,
				contratistaId:getSession("contratistaGosstId")
		};
		
		callAjaxPost(URL + '/contratista/proyecto/trabajadores', 
				proyectoObjSend, function(data){
					if(data.CODE_RESPONSE=="05"){
						
						listFullTrabProyecto=data.list;
						$("#editarMovilTrabProy").find(".tituloSubList")
						.html(textoBotonVolverContenido+"Nuevo trabajador del proyecto '"
								+listFullProyectoSeguridad[pindex].titulo+"' ");
						$(".divListPrincipal>div").hide();
						$("#editarMovilTrabProy").show();
						insertarSelectTrabajadoresProyecto();
					
				}
		})
		banderaEdicion79 = false;
	} else {
		alert("Guarde primero.");
	}
}
function insertarSelectTrabajadoresProyecto(){
	var listActivo=[];
	var listInactivo=[];
	listFullTrabProyecto.forEach(function(val,index){
		if(val.proyectoActual!=null ){
			listActivo.push(val);
			if(val.isActivo==1){
				val.selected=1;
			}
		}
	})
	listActivo.forEach(function(val,index){
		if(val.proyectoActual!=null){
			var auxTrab={id:val.id,nombreCompleto:val.nombreCompleto,selected:1};
			var select=0;
			if(val.isActivo==0){
				auxTrab.selected=1;
			}else{
				auxTrab.selected=0;
			}
			listInactivo.push(auxTrab);
		}
	})
var editarDiv=$("#editarMovilTrabProy");
editarDiv.find("#divSelTrabProy").html("");
var slcTrabajadoresProyecto=crearSelectOneMenuObligMultipleCompleto("slcTrabajadoresProyecto", "",
		listFullTrabProyecto,  "id", "nombreCompleto","#editarMovilTrabProy #divSelTrabProy","Trabajadores ...");
var slcTrabajadoresProyectoInhabilitados=crearSelectOneMenuObligMultipleCompleto("slcTrabajadoresProyectoInhabilitados", "",
		listInactivo,  "id", "nombreCompleto","#editarMovilTrabProy #divSelTrabProyInhab","Trabajadores Inhabilitados...");
	$("#slcTrabajadoresProyecto").chosen().change( function(ev,val){
		listFullTrabProyecto.forEach(function(val1){
			if(val1.id==parseInt(val.selected)){
				val1.isActivo=1
				val1.selected=1
				insertarSelectTrabajadoresProyecto();
			}
			if(val1.id==parseInt(val.deselected)){
				val1.isActivo=0
				val1.selected=0
				insertarSelectTrabajadoresProyecto();
			};
		});
	
	});
	$("#slcTrabajadoresProyectoInhabilitados").chosen().change( function(ev,val){
		listFullTrabProyecto.forEach(function(val1){
			if(val1.id==parseInt(val.selected)){
				val1.isActivo=0
				val1.selected=0
				insertarSelectTrabajadoresProyecto();
			}
			if(val1.id==parseInt(val.deselected)){
				val1.isActivo=1;
				val1.selected=0
				insertarSelectTrabajadoresProyecto();
			};
		});
	
	});
}
function eliminarTrabProyecto(pindex) {
	trabajadorProyectoId = listFullTrabProyecto[pindex].id; 
	var r = confirm("¿Está seguro de eliminar la actividad?");
	if (r == true) {
		var dataParam = {
				id : trabajadorProyectoId,
		};

		callAjaxPost(URL + '/contratista/proyecto/actividad/delete', dataParam,
				function(data) {
					switch (data.CODE_RESPONSE) {
					case "05":
						cargarTrabsProyectosSeguridad();
						break;
					default:
						alert("Ocurrió un error al eliminar la programacion!");
					}
				});
	}
}

function guardarTrabsProyecto() {

	var campoVacio = true;
	var trabsId=$("#slcTrabajadoresProyecto").val();
	var trabajadoresFinal=[];
	trabsId.forEach(function(val,index){
		trabajadoresFinal.push({id:val,isActivo:1});
	});
	var trabsInactivoId=$("#slcTrabajadoresProyectoInhabilitados").val();
	trabsInactivoId.forEach(function(val,index){
		trabajadoresFinal.push({id:val,isActivo:0});
	});
		if (campoVacio) {

			var proyectoObject = {
				id : proyectoObj.id,
				contratistaId:getSession("contratistaGosstId"),
				trabajadores: trabajadoresFinal 
			};

			callAjaxPost(URL + '/contratista/proyecto/trabajadores/save', proyectoObject,
					function(data) {
						switch (data.CODE_RESPONSE) {
						case "05": 
						
								volverDivSubContenido();
								cargarTrabsProyectosSeguridad()
						
							break;
						default:
							console.log("Ocurrió un error al guardar la programacion!");
						}
					},null,null,null,null,false);
			 
		} else {
			alert("Debe ingresar todos los campos.");
		}
	
	
}
function ordenarTrabProyecto(tipoOrden,isAscendente){
	var factorAscendente=1;
	var isNuevoAscendete=0;
	if(parseInt(isAscendente)==0){
		factorAscendente=-1;
		isNuevoAscendete=1
	}else{
		factorAscendente=1;
		isNuevoAscendete=0
	}
	listFullTrabajadoresProyectos=listFullTrabajadoresProyectos.filter(function(val){
		if(val.proyectoActual!=null && val.isActivo==1){
			return true
		}else{return false;}
	});
	switch(parseInt(tipoOrden)){
	case 1:
		listFullTrabajadoresProyectos.sort(function(a,b){
			
		    return (a.isHabilitado - b.isHabilitado)*factorAscendente;
			
		});
		break;
	case 2:
		listFullTrabajadoresProyectos.sort(function(a,b){
			if(a.nombre.toUpperCase() < b.nombre.toUpperCase()) return -1*factorAscendente;
		    if(a.nombre.toUpperCase() > b.nombre.toUpperCase()) return 1*factorAscendente;
		    return 0;
			});
		break;
	case 3:
		listFullTrabajadoresProyectos.sort(function(a,b){
			if(a.apellido.toUpperCase() < b.apellido.toUpperCase()) return -1*factorAscendente;
		    if(a.apellido.toUpperCase() > b.apellido.toUpperCase()) return 1*factorAscendente;
		    return 0;
			});
		break;
	}
	var data={CODE_RESPONSE:"05",list:listFullTrabajadoresProyectos};
	if(getSession("accesoUsuarios")=="2"){
		procesarResumenTrabajadoresProyectoVistaContratista(data);
	}else{
		procesarResumenTrabajadoresProyecto(data)
	}
	$(".fix-head #iconSortTrab"+tipoOrden+"").attr("onclick","ordenarTrabProyecto("+tipoOrden+","+isNuevoAscendete+")")
	$("#iconSortTrab"+tipoOrden+"").attr("onclick","ordenarTrabProyecto("+tipoOrden+","+isNuevoAscendete+")")
	
}
var tituloAnteriorContratista="";
function verTablaResumenTrabProyecto(index){
	
	$(".divContainerGeneral").hide();
	var buttonDescarga="<a target='_blank' class='btn btn-success'" +
			" href='"+URL+"/'>" +
			"<i class='fa fa-download'></i>Descargar</a>"
	var buttonClip="<button type='button' id='btnClipboardTrabProy'   style='margin-right:10px'" +
			" class='btn btn-success clipGosst' onclick='obtenerTablaTrabajadoresProyecto()'>" +
					"<i class='fa fa-clipboard'></i>Clipboard</button>"
	var buttonVolver="<button type='button' class='btn btn-success ' style='margin-right:10px'" +
			" onclick='volverVistaMovil()'>" +
						"<i class='fa fa-sign-out fa-rotate-180'></i>Volver</button>";
	var buttonFiltro="<button type='button' class='btn btn-success ' id='btnIniciarFiltro' style='margin-right:10px'" +
	" onclick='iniciarFiltroTrabProyecto()'>" +
				"<i class='fa fa-filter'></i>Ver filtros</button>";
	var buttonFiltroAplicar="<button type='button' class='btn btn-success ' id='btnAplicarFiltro' style='margin-right:10px'" +
	" onclick='aplicarFiltroTrabProyecto()'>" +
				"<i class='fa fa-filter'></i>Aplicar filtros</button>";
	var buttonFiltroRefresh="<button type='button' class='btn btn-success ' id='btnReiniciarFiltro' style='margin-right:10px'" +
	" onclick='reiniciarFiltroTrabProyecto()'>" +
				"<i class='fa fa-refresh'></i>Refresh</button>";
	var buttonBuscar="<input placeholder='DNI,nombre o apellido' class='form-control' id='buscarTrabProy'>" +
			"<button type='button' class='btn btn-success ' style='margin-right:10px'" +
	" onclick='buscarTrabajadorProyecto()'>" +
				"<i class='fa fa-search'></i>Buscar</button>";
	$("body")
	.append("<div id='divTrabProyTabla' style='padding-left: 40px;'>" +
			"<form class='form-inline'>"+
			buttonVolver+buttonBuscar+buttonFiltroRefresh+buttonFiltroAplicar+buttonFiltro+buttonClip+"</form>"+
			"<div class='camposFiltroTabla'>"+
				"<div class='contain-filtro' id='filtroProyTrab'>" +
					"<input type='checkbox' id='checkFiltroProyTrab' checked>" +
					"<label for='checkFiltroProyTrab'  >Trabajadores "+
					"</label>" +
					"<div id='divSelectFiltroProyTrab'> " +
					"</div>" + 
				"</div>"+	
				" </div>" +
			"<div class='wrapper' id='wrapperTrabProy'>" +
			"<table  style='min-width:2400px ' " +
			"id='tblTrabProyecto' class='table table-striped table-bordered table-hover fixed'>" +
			"<thead>" +
				"<tr>" +
				"<td class='tb-acc' style='width:140px'>  Habilitado <i id='iconSortTrab1' class='fa fa-sort' onclick='ordenarTrabProyecto(1,1)'></i></td>" +
				"<td class='tb-acc' style='' >Nombre <i id='iconSortTrab2' class='fa fa-sort' onclick='ordenarTrabProyecto(2,1)'></i></td>" + 
				"<td class='tb-acc' style=''  >Apellido <i id='iconSortTrab3' class='fa fa-sort' onclick='ordenarTrabProyecto(3,1)'></i></td>" + 
				"<td class='tb-acc' style='width:190px'>Área</td>" +
				"<td class='tb-acc' style='width:120px'>Cargo</td>" +
				"<td class='tb-acc' style='width:120px'>Tipo Documento</td>" +
				"<td class='tb-acc' style='width:150px'>N° Documento</td>" +
				"<td class='tb-acc' style='width:150px'>Examen Médico</td>" +
				"<td class='tb-acc' style='width:150px'>SCTR</td>" +
				"<td class='tb-acc' style='width:150px'>Inducción</td>" +
				"<td class='tb-acc' style='width:150px'>Cargo reglamento</td>" +
				"<td class='tb-acc' style='width:150px'>T-registro</td>" +
				"<td class='tb-acc' style='width:150px'>EPP</td>" +
				"<td class='tb-acc' style='width:150px'>Hoja de Vida</td>" + 
				"" +
				"</tr>" +
			"</thead>" +
			"<tbody></tbody>" +
			"</table>" +
			"</div>" +
			" </div>");
	var listOpciones=[{id:1000,nombre:"Habilitado"},
	                  {id:0,nombre:"Sin Habilitar"},
	                  {id:2,nombre:"Sin SCTR"},
	                  {id:1,nombre:"Sin Examen Médico"},
	                  {id:3,nombre:"Sin Inducción"},
	                  {id:4,nombre:"Sin Cargo"},
	                  {id:6,nombre:"Sin T-registro"},{id:8,nombre:"Sin EPP"},
	                  {id:100,nombre:"Sin Hoja de Vida"}];
	if(parseInt(getSession("isProyectoSimple"))==1){
		listOpciones=[{id:1000,nombre:"Habilitado"},
	                  {id:0,nombre:"Sin Habilitar"},
	                  {id:2,nombre:"Sin SCTR"},
	                  {id:1,nombre:"Sin Examen Médico"},
	                  {id:3,nombre:"Sin Inducción"},
	                  {id:4,nombre:"Sin Cargo"},{id:8,nombre:"Sin EPP"},
	                  {id:100,nombre:"Sin Hoja de Vida"}];
	}
	crearSelectOneMenuObligMultipleCompleto("selMultipleTrabProy", "", listOpciones, 
			"id","nombre","#divSelectFiltroProyTrab","Seleccione criterios");
	
	$(".camposFiltroTabla").hide();
	
	$("#btnAplicarFiltro").hide();
	$("#buscarTrabProy").focus();
	  $("#divTrabProyTabla form").on("submit",function(e){
			e.preventDefault();
			buscarTrabajadorProyecto();
		});
	var contratistaSendId=getSession("contratistaGosstId");
	if(parseInt(getSession("accesoUsuarios"))==0){
		if(proyectoObj.postulante!=null){
		contratistaSendId=proyectoObj.postulante.contratista.id;
		}else{
			alert("No se ha asignado contratista")
			return;
		}
	}
	tituloAnteriorContratista=$(".divTituloGeneral h4").text();
	$(".divTituloGeneral h4").html("Proyecto '"+proyectoObj.titulo+"' " +
			"> Contratista "+proyectoObj.contratistaNombre+" > Resumen de trabajadores");
	resizeDivWrapper("wrapperTrabProy",300);
	 var proyectoObjSend={
			 id:proyectoObj.id,
			 contratistaId:contratistaSendId
		 }
		callAjaxPost(URL + '/contratista/proyecto/trabajadores', 
				proyectoObjSend, procesarResumenTrabajadoresProyectoVistaContratista);
}

function obtenerTablaTrabajadoresProyecto(){
	
	var texto=obtenerDatosTablaEstandarNoFija("tblTrabProyecto"); 
	 copiarAlPortapapeles(texto,"btnClipboardTrabProy");
		alert("Se han guardado al clipboard la tabla " );
	
}
function buscarTrabajadorProyecto(){
	var trabNombre=igualarStringBuscar($("#buscarTrabProy").val());
	
	listFullTrabajadoresProyectos.forEach(function(val,index){
		if(igualarStringBuscar(val.nombre).indexOf(trabNombre)!=-1
				|| igualarStringBuscar(val.apellido).indexOf(trabNombre)!=-1
				|| igualarStringBuscar(val.nroDocumento).indexOf(trabNombre)!=-1){
			$("#trproy"+val.id).show();
		}else{
			$("#trproy"+val.id).hide();
		}
		
	}); 
}

function reiniciarFiltroTrabProyecto(){
	listFullTrabajadoresProyectos.forEach(function(val,index){ 
			$("#trproy"+val.id).show(); 
	});
	$("#buscarTrabProy").val("").focus();
	$(".camposFiltroTabla").hide();
	$("#wrapperTrabProy").show();
	$("#wrapTrabProyecto").show();
}

function iniciarFiltroTrabProyecto(){
	$(".camposFiltroTabla").show();
	$("#wrapperTrabProy").hide();
	
	$("#btnIniciarFiltro").hide();
	$("#btnAplicarFiltro").show();
	
	$("#wrapTrabProyecto").hide();
}

function aplicarFiltroTrabProyecto(){
	$(".camposFiltroTabla").hide();
	$("#wrapperTrabProy").show();
	$("#btnAplicarFiltro").hide();
	$("#btnIniciarFiltro").show();
	
	$("#wrapTrabProyecto").show();
	
	var listAplicados=$("#selMultipleTrabProy").val();
	if(listAplicados==null){
		reiniciarFiltroTrabProyecto();
		return;
	}
	listFullTrabajadoresProyectos.forEach(function(val,index){
		var searchExam=(listAplicados.indexOf("1")!=-1)
		var searchSctr=(listAplicados.indexOf("2")!=-1)
		var searchInduccion=(listAplicados.indexOf("3")!=-1)
		var searchCargo=(listAplicados.indexOf("4")!=-1)
		var searchRegistro=(listAplicados.indexOf("6")!=-1)
		var searchEpp=(listAplicados.indexOf("8")!=-1)
		var searchHojaVida=(listAplicados.indexOf("100")!=-1);
		var searchHabilitado=(listAplicados.indexOf("0")!=-1);
		var searchSiHabilitado=(listAplicados.indexOf("1000")!=-1);
		var hasSiHabilitado=true,hasHabilitado=true,hasExamen=true,hasSctr=true,hasInduccion=true,hasCargo=true,hasTreg=true,hasHojaVida=true,hasEpp=true;
		//
		var verifObj=verificarTrabajadorContratistaHabilitado(val);
		if(proyectoObj.evaluacionManual==0){
			
		}else{
			if(val.notaManual==1){
				verifObj.isHabilitado=true;
			}else{
				verifObj.isHabilitado=false;
			}
		}
		if(searchHabilitado){
			if(!verifObj.isHabilitado){
				hasHabilitado=false;
			}
		}else{
			hasHabilitado=false;
		}
		//
		if(searchSiHabilitado){
			if(verifObj.isHabilitado){
				hasSiHabilitado=false;
			}
		}else{
			hasSiHabilitado=false;
		}
		//
		if(searchExam){
			if(val.vigenteExamenMedico!=1){
				hasExamen=false;
			}
		}else{
			hasExamen=false;
		}
		if(searchSctr){
			if(val.vigenteSctr!=1 ){
				hasSctr=false;
			}
		}else{
			hasSctr=false;
		}
		//
		if(searchInduccion){
			if(val.vigenteInduccion!=1 ){
				hasInduccion=false;
			}
		}else{
			hasInduccion=false;
		}
		//
		if(searchCargo){
			if(val.vigenteCargo!=1 ){
				hasCargo=false;
			}
		}else{
			hasCargo=false;
		}
		//
		if(searchRegistro){
			if(val.vigenteRegistro!=1 ){
				hasTreg=false;
			}
		}else{
			hasTreg=false;
		}
		//
		if(searchEpp){
			if(val.vigenteEpp!=1 ){
				hasEpp=false;
			}
		}else{
			hasEpp=false;
		}
		//
		if(searchHojaVida){
			if(val.evaluacionHojaVida==null){
				hasHojaVida=false;
			}else{
				if(val.evaluacionHojaVida.fecha==null){
					hasHojaVida=false;
				}else{
					if(val.evaluacionHojaVida.tipo.id==1){
						hasHojaVida=true;
					}else{
						hasHojaVida=false;
					}
				}
			}
			 
		}else{
			hasHojaVida=false;
		}
		
		
		if(!hasHabilitado && !hasExamen && !hasSctr && !hasInduccion
				&& !hasCargo && !hasTreg && !hasHojaVida && !hasEpp && !hasSiHabilitado){
			$("#trproy"+val.id).show();
		}else{
			$("#trproy"+val.id).hide();
		}
		
	});
	
}





