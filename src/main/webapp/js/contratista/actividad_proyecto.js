var actividadProyectoId;
var actividadProyObj=[];
var listCategoriaActividad=[];
var banderaEdicion69=false;
var listFullActProyectoSeguridad;
var estadoActividadProy; 
var tipoActividadId;
var funcionalidadActividad=[
		{id:"opcEditar", nombre:"<i class='fa fa-pencil-square-o'></i> Editar",functionClick:function(data){editarActProyecto(indexActividad);}},
		{id:"opcEliminar", nombre:"<i class='fa fa-trash'></i> Eliminar",functionClick:function(data){eliminarActProyecto(indexActividad);}}
	];
 function marcarSubOpcionActividad(pindex){
	 funcionalidadActividad[pindex].functionClick();
	$(".detalleAccion ul").hide(); 
}
var indexActividad;
function toggleMenuOpcionActividad(obj,pindex){ 
		indexActividad=pindex;
		$(obj).parent(".detalleAccion").find("ul").toggle();
		$(obj).parent(".detalleAccion").siblings().find("ul").hide(); 
}
function cargarActProyectosSeguridad(pindex,ptipoId,verDiv) { 
	pindex=defaultFor(pindex,indexPostulante);
	ptipoId=defaultFor(ptipoId,tipoActividadId)
	//
	$(".detalleActividadesProyecto"+ptipoId).html("");
	
	
	$("#proyectoTable"+listFullProyectoSeguridad[pindex].id).parent("#tituloEvento")
	.parent(".eventoGeneral").parent(".contenidoSubList")
	.parent(".divProyectoGeneral").siblings()
	.find(".detalleActividadesProyecto"+ptipoId).hide();
	if(verDiv){
		$("#proyectoTable"+listFullProyectoSeguridad[pindex].id)
		.find(".detalleActividadesProyecto"+ptipoId).show()
	}else{
		$("#proyectoTable"+listFullProyectoSeguridad[pindex].id)
		.find(".detalleActividadesProyecto"+ptipoId).toggle()
	}
	
	//
	var proyectoObjSend = {
			id : listFullProyectoSeguridad[pindex].id
	};
	proyectoObj.id=listFullProyectoSeguridad[pindex].id;
	indexPostulante=pindex;
	tipoActividadId=ptipoId;
	callAjaxPost(URL + '/contratista/proyecto/actividades', 
			proyectoObjSend, function(data){
				if(data.CODE_RESPONSE=="05"){
					listFullActProyectoSeguridad=data.list;
					listCategoriaActividad=data.categorias;
					banderaEdicion69=false;
					var indicadorPositivo=0,indicadorTotal=0;
					$("#tblActProyecto tbody tr").remove();
					$("#proyectoTable"+listFullProyectoSeguridad[pindex].id)
					.find(".detalleActividadesProyecto"+tipoActividadId).html("<td colspan='3'></td>");
					listFullActProyectoSeguridad.forEach(function(val,index){
						if(val.tipo.id==tipoActividadId){
						 var eviNombre=(
								 val.evidenciaNombre==""?"<strong><i aria-hidden='true' class='fa fa-download'></i> Evidencia:</strong> Sin Registro<br>"
									: 
									"<strong><i aria-hidden='true' class='fa fa-download'></i> Evidencia: </strong>" +
									"<a class='efectoLink' href='"+URL+"/contratista/proyecto/actividad/evidencia?id="+val.id+" ' >"+
										val.evidenciaNombre+"<br>"+
									"</a>");
						  var estilo="gosst-neutral";
						  var textObs=val.observacion;
						  if(val.evaluacion==null){
							  val.evaluacion={nota:{},observacion:""};
						  }
						  var textObs="<br>"+iconoGosst.advertir+"Observación: <strong>"+val.evaluacion.observacion+"</strong>";
						  
						  if(val.evaluacion.observacion==""){
							  textObs="";
						  }
						 if(val.evaluacion.nota.id==1){
							 indicadorPositivo++;
							 estilo="gosst-aprobado";
							 textObs="";
						 }else{
							
						 }
						 var menuOpcion="<ul class='list-group listaGestionGosst' >";
							funcionalidadActividad.forEach(function(val1,index1){
								menuOpcion+="<li class='list-group-item' onclick='marcarSubOpcionActividad("+index1+")'>"+val1.nombre+" </li>"
							});
							menuOpcion+="</ul>"
						 var textIn="";
							if(val.evaluacion.nota.icono==null){
								val.evaluacion={observacion:"",nota:{nombre:"Pendiente",icono:""}}
							}
							if(index==25){console.log(val.evaluacion)}
						 if(val.tipo.id==1){ 
							 textIn=
									"<a class='efectoLink btn-gestion' onclick='toggleMenuOpcionActividad(this,"+index+")'>" +
										"Ver Opciones <i class='fa fa-fa-angle-double-down'></i>"+
									"</a>"+
									menuOpcion+
							 		"<strong><i aria-hidden='true' class='fa fa-info'></i> Descripcion: </strong>"+
								 	val.descripcion+"<br>" +
								 	"<strong><i aria-hidden='true' class='fa fa-calendar'></i> Fecha Planificada:</strong> "+
								 	val.fechaPlanificadaTexto+"<br>"+
								 	"<strong><i aria-hidden='true' class='fa fa-list-ul'></i> Categoria:</strong>"+
								 	val.categoria.nombre+"<br>" +
								 	"<i class='fa fa-pencil-square-o'></i>Estado de evaluación: "+val.evaluacion.nota.icono+val.evaluacion.nota.nombre+"  <br>"+
								 	eviNombre;
								;
						 }else{
							 var tituloObs="Observación"
							 if(tipoActividadId==5){
								 tituloObs="Título";
							 }else{
								 
							 }
							 textIn=
									"<a class='efectoLink btn-gestion' onclick='toggleMenuOpcionActividad(this,"+index+")'>" +
										"Ver Opciones <i class='fa fa-fa-angle-double-down'></i>"+
									"</a>"+
									menuOpcion+
									"<i aria-hidden='true' class='fa fa-search'></i>"+tituloObs+": "+
								 	val.observacion+"<br>" +
								 	"<i aria-hidden='true' class='fa fa-calendar'></i>Fecha de Subida : "+
							 		val.fechaRealTexto+"<br>"+
							 		"<i class='fa fa-pencil-square-o'></i>Estado de evaluación: "+val.evaluacion.nota.icono+val.evaluacion.nota.nombre+"  <br>"+
								 	eviNombre;
						 }
						$("#proyectoTable"+listFullProyectoSeguridad[pindex].id)
						.find(".detalleActividadesProyecto"+tipoActividadId+" td") 
						.append("<div class='detalleAccion "+estilo+"' > " +
								textIn+textObs+
										"</div>" +
							"<div class='opcionesAccion'>" +
							/*"<a onclick='eliminarActProyecto("+index+")'> Eliminar</a>" +
									"<i class='fa fa-minus' aria-hidden='true'></i>" +
							"<a onclick='editarActProyecto("+index+")'> Editar</a>"+*/
							"</div>"); 
						}
					});
					$(".detalleAccion ul").hide(); 
					$("#trPlanProyecto"+proyectoObj.id+" .celdaIndicadorProyecto").html(indicadorPositivo+" / "+listFullActProyectoSeguridad.length)
					
				}else{
					console.log("NOPNPO")
				}
			});
	
}

function editarActProyecto(pindex) {


	if (!banderaEdicion69) {
		$("#editarMovilActivProy").find(".tituloSubList")
		.html(textoBotonVolverContenido+"Editar");
		actividadProyectoId = listFullActProyectoSeguridad[pindex].id; 
		actividadProyObj=listFullActProyectoSeguridad[pindex];
		tipoActividadId=actividadProyObj.tipo.id;
		var descripcion=actividadProyObj.descripcion;
		var fechaPlan=convertirFechaInput(actividadProyObj.fechaPlanificada);
		var observacion=actividadProyObj.observacion;
		var expositor=actividadProyObj.expositor;
		var duracion=actividadProyObj.duracion;
		var numTrabajadores=actividadProyObj.numTrabajadores;
		//
		$(".divListPrincipal>div").hide();
		$("#editarMovilActivProy").show(); 
		
		
		$("#inputDescActProy").val(descripcion);
		$("#inputFecPlanActProy").val(fechaPlan);
		$("#inputObsActProy").val(observacion);
		
		$("#inputExpoActProy").val(expositor);
		$("#inputNumTrabActProy").val(numTrabajadores);
		$("#inputDuraActProy").val(duracion);
		
		$("#expoActividad").parent().hide();
		$("#numTrabActividad").parent().hide();
		$("#duracionActividad").parent().hide();
		$("#divSeparatorActividad").parent().hide();
		$("#divMensajeActividad").parent().hide();
		$("#divPuntajeActividad").parent().hide();
		
		$("#divDescActiv").parent().hide();
		$("#divFechaPlanActiv").parent().hide();
		$("#divCategActiv").parent().hide();
		if(actividadProyObj.tipo.id==1){
			$("#divDescActiv").parent().show();
			$("#divFechaPlanActiv").parent().show();
			$("#divCategActiv").parent().show();
			var selCategoriaActividad=crearSelectOneMenuOblig("selCategoriaActividad", "verAcordeCategoriaActividad()", 
					listCategoriaActividad, actividadProyObj.categoria.id, "id","nombre")
			$("#divCategActiv").html(selCategoriaActividad);
			verAcordeCategoriaActividad();
		}

		$("#divObsActProy").parent().find("label").html("Observación del contratista");
		if(actividadProyObj.tipo.id==5){
			$("#divObsActProy").parent().find("label").html("Título del documento");
		}
		var eviNombre=actividadProyObj.evidenciaNombre;
		var options=
		{container:"#eviActividad",
				functionCall:function(){ },
				descargaUrl: "/contratista/proyecto/actividad/evidencia?id="+actividadProyObj.id,
				esNuevo:false,
				idAux:"ActividadProy",
				evidenciaNombre:eviNombre};
		crearFormEvidenciaCompleta(options);
		  
				hallarEstadoImplementacionActividad();
			//
			  
				
		banderaEdicion69 = false;   
	}
	
}

function nuevoActProyecto(pindex,ptipoId) {
	callAjaxPost(URL + '/contratista/proyecto/actividades', 
			{id:proyectoObj.id}, function(data){  
			listCategoriaActividad=data.categorias; 
				actividadProyectoId = 0;
				var nombreTipoActividad="";
				$("#expoActividad").parent().hide();
				$("#numTrabActividad").parent().hide();
				$("#duracionActividad").parent().hide();
				$("#divSeparatorActividad").parent().hide();
				$("#divMensajeActividad").parent().hide();
				$("#divPuntajeActividad").parent().hide();
				
				$("#divDescActiv").parent().hide();
				$("#divFechaPlanActiv").parent().hide();
				$("#divCategActiv").parent().hide();
				$("#divObsActProy").parent().find("label").html("Observación del contratista");
				switch(ptipoId){
				case 1:
					$("#divSeparatorActividad").parent().show();
					$("#divMensajeActividad").parent().show();
					$("#divPuntajeActividad").parent().show();
					
					$("#divDescActiv").parent().show();
					$("#divFechaPlanActiv").parent().show();
					$("#divCategActiv").parent().show();
					nombreTipoActividad=listTipoActividades[0].nombre
					break;
				case 2:
					nombreTipoActividad="Plan de emergencia"
					break;
				case 3:
					nombreTipoActividad="Plan SSOMA"
					break;
				case 4:
					nombreTipoActividad="Plan de manejo de residuos"
					break;
				case 5:
					nombreTipoActividad=listTipoActividades[3].nombre
						$("#divObsActProy").parent().find("label").html("Título del documento");
					break;	
				}
				verAcordeCategoriaActividad();
				tipoActividadId=ptipoId;
				proyectoObj.id=listFullProyectoSeguridad[pindex].id;
				indexPostulante=pindex;
				$("#editarMovilActivProy").find(".tituloSubList")
				.html(textoBotonVolverContenido+"Nuevo "+nombreTipoActividad+" del proyecto '"
						+listFullProyectoSeguridad[pindex].titulo+"' ");
				var selCategoriaActividad=crearSelectOneMenuOblig("selCategoriaActividad", "verAcordeCategoriaActividad()", 
						listCategoriaActividad, "", "id","nombre")
				$("#divCategActiv").html(selCategoriaActividad);
				$("#editarMovilActivProy").find("form input").val("");
				var options=
				{container:"#eviActividad",
						functionCall:function(){ },
						descargaUrl: "",
						esNuevo:true,
						idAux:"ActividadProy",
						evidenciaNombre:""};
				crearFormEvidenciaCompleta(options);
				$(".divListPrincipal>div").hide();
				$("#editarMovilActivProy").show(); 
				
						 
	});
	
}
function verAcordeCategoriaActividad(){
	var catId=parseInt($("#selCategoriaActividad").val());
	$("#expoActividad").parent().hide();
	$("#numTrabActividad").parent().hide();
	$("#duracionActividad").parent().hide();
	$("#divPuntajeActividad").parent().hide();
	switch(catId){
	case 3:
	case 6:
	case 8:
		$("#expoActividad").parent().show();
		$("#numTrabActividad").parent().show();
		$("#duracionActividad").parent().show();
		break;
		
	case 2:
	case 9:
		$("#divPuntajeActividad").parent().show();
		break;
	}
}
function cancelarActProyecto() {
	cargarTiposContratista();
}

function eliminarActProyecto(pindex) {
	actividadProyectoId = listFullActProyectoSeguridad[pindex].id; 
	var r = confirm("¿Está seguro de eliminar la actividad?");
	if (r == true) {
		var dataParam = {
				id : actividadProyectoId,
		};

		callAjaxPost(URL + '/contratista/proyecto/actividad/delete', dataParam,
				function(data) {
					switch (data.CODE_RESPONSE) {
					case "05":
						cargarActProyectosSeguridad(indexPostulante,tipoActividadId,true);
						break;
					default:
						alert("Ocurrió un error al eliminar la programacion!");
					}
				});
	}
}

function guardarActProyecto(functionCallBack) {

	var campoVacio = true;
	var descripcion=$("#inputDescActProy").val();  
var fecha=$("#inputFecPlanActProy").val();
var observacion=$("#inputObsActProy").val();
var categoria={id:$("#selCategoriaActividad").val()};
var expositor=$("#inputExpoActProy").val();
var duracion=$("#inputDuraActProy").val();
var numTrabajadores=$("#inputNumTrabActProy").val();
var puntaje=$("#inputPuntajeActProy").val();
hallarEstadoImplementacionActividad();
		if (campoVacio) {

			var dataParam = {
					puntaje:puntaje,
				id : actividadProyectoId,expositor:expositor,duracion:duracion,numTrabajadores:numTrabajadores,
				categoria:categoria,
				descripcion: descripcion,
				tipo:{id:tipoActividadId},
				fechaPlanificada:convertirFechaTexto(fecha),
				observacion:observacion,
				estado:estadoActividadProy,
				proyecto:{id:proyectoObj.id}
			};

			callAjaxPost(URL + '/contratista/proyecto/actividad/save', dataParam,
					function(data) {
						switch (data.CODE_RESPONSE) {
						case "05": 
							guardarEvidenciaAuto(data.nuevoId,"fileEviActividadProy"
									,bitsEvidenciaArchivoCurso,"/contratista/proyecto/actividad/evidencia/save"
									,function(){
								volverDivSubContenido();
								cargarActProyectosSeguridad(indexPostulante,tipoActividadId,true);
								if(functionCallBack!=null){
									functionCallBack();
								}
							});
						 
							break;
						default:
							console.log("Ocurrió un error al guardar la programacion!");
						}
					},null,null,null,null,false);
			 
		} else {
			alert("Debe ingresar todos los campos.");
		} 
}


function cancelarNuevoActProyecto(){
	cargarActProyectosSeguridad();
}


function hallarEstadoImplementacionActividad(){
	var fechaPlanificada=$("#inputFecPlanActProy").val();
	var fechaReal=convertirFechaInput(actividadProyObj.fechaReal);
	var fechaHoy=obtenerFechaActual();
	if(fechaReal!=''){
		estadoActividadProy={id:2,
		nombre:"Completado"};
	}else{
		
		if(fechaPlanificada!=""){
			var dif=restaFechas(fechaHoy,fechaPlanificada);
			console.log(dif);
			if(dif<0){
				estadoActividadProy={id:3,
						nombre:"Retrasado"};
			}else{
				estadoActividadProy={id:1,
						nombre:"Por Implementar"};
			}
			
		}else{
			estadoActividadProy={id:1,
					nombre:"Por Implementar"};
			
		}
	}
	
	$("#actpes"+actividadProyectoId).html(estadoActividadProy.nombre);
	
}


