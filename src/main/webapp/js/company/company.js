/** *************variables de empresa*********** */
var banderaEdicion;
var idCompanyEdicion;
var encabezadosTotal;
var encabezadoId;
var idTipoEncabezado;
var listFullEmpresa;
var listObjetivos;
var listFullTablaIndicadores;
$(document).ready(function() {
	banderaEdicion = false;
	idCompanyEdicion = 0;
	$("#btnGuardar").hide();
	$("#btnCancelar").hide();
	$("#btnEliminar").hide();
	$("#modalEncabezadosEmpresa .nav-tabs li a").css({
		"border":"0.5px solid"
		
	});

	$("#btnClipTabla").on("click",function(){
		obtenerTablaClipboard();
		
	});
	insertMenu();
	listarEmpresa();
	
});
function procesarGuardarEncabezado(data){
	switch (data.CODE_RESPONSE) {
	case "05":
		
		llamarEncabezados();
		break;
	default:
		alert("Ocurrió un error al guardar la empresa!");
	}
}
function guardarEncabezado(){
	var codigo=$("#codigoDocInput").val();
	
	var revision=$("#revisionDocInput").val();
	var mes2 = $("#fechaDocInput").val().substring(5, 7) - 1;
	var fechatemp2 = new Date($("#fechaDocInput").val().substring(0, 4), mes2,
			$("#fechaDocInput").val().substring(8, 10));

	var inputFecha = $("#fechaDocInput").val();
	if(!$("#fechaDocInput").val()){
		
		inputFecha=null;
		}
	var encabezadoObj={
			encabezadoId:encabezadoId,
			codigoDocumento:codigo,
			fechaActualizacionDocumento:inputFecha,
			revisionDocumento:revision,
			idCompany:idCompanyEdicion,
			tipoEncabezadoDocumento:idTipoEncabezado
					
	};
	callAjaxPost(URL + '/empresa/encabezado/save', encabezadoObj, procesarGuardarEncabezado);

}
function verEncabezados(pidTipoEncabezado){
	
	var codigo="";
	var fecha=null;
	var revision="";
	idTipoEncabezado=pidTipoEncabezado;
	encabezadoId=0;
	if(encabezadosTotal[idTipoEncabezado]){
		encabezadoId=encabezadosTotal[idTipoEncabezado].encabezadoId;
		 codigo=encabezadosTotal[idTipoEncabezado].codigoDocumento;
		 fecha=encabezadosTotal[idTipoEncabezado].fechaActualizacionDocumento;
		 revision=encabezadosTotal[idTipoEncabezado].revisionDocumento;
		
	}

	$("#codigoDocInput").val(codigo);
	$("#fechaDocInput").val(fecha);
	$("#revisionDocInput").val(revision);

}
function llamarObjetivos(){
	var dataParam = {
			empresaId :idCompanyEdicion,grupoId:1
		};
	
	callAjaxPost(URL + '/empresa/indicadores', dataParam, procesarIndicadorEmpresa);
	
}
function llamarEncabezados(){
	
	$("#modalEncabezadosEmpresa").modal("show");
	var companyObj={
			idCompany:idCompanyEdicion
	}
	
	callAjaxPost(URL + '/empresa/encabezados', companyObj, procesarListarEncabezados);
}
function procesarListarEncabezados(data){
	switch (data.CODE_RESPONSE) {
	case "05":
		encabezadoId=0;
		
		encabezadosTotal=[];
		encabezadosTotal.push(data.encabezadosInsp);
		encabezadosTotal.push(data.encabezadosInsp); 
		encabezadosTotal.push(data.encabezadosAudi); 
		encabezadosTotal.push(data.encabezadosAcci);
		encabezadosTotal.push(data.encabezadosInci);
		encabezadosTotal.push(data.encabezadosEnfe);
		encabezadosTotal.push(data.encabezadosActo);
		encabezadosTotal.push(data.encabezadosForm);
		encabezadosTotal.push(data.encabezadosEpp);
		encabezadosTotal.push(data.encabezadosMdf);
		encabezadosTotal.push(data.encabezadosExam);
		encabezadosTotal.push(data.encabezadosPuesto);
		encabezadosTotal.push(data.encabezadosAccion);
		verEncabezados(idTipoEncabezado)
		
	
		break;
	default:
		alert("Ocurrió un error al traer las encabezados!");
	}
	
	
}
function listarEmpresa() {
	idTipoEncabezado=1;
	idCompanyEdicion = 0;
	deshabilitarBotonesEdicion();
	$("#btnEncabezado").hide();
	$("#btnObjetivo").hide();
	banderaEdicion = false;
	//var dataParam = {};
	if( isMobile.any() ) {
		callAjaxPost(URL + '/empresa', {}, procesarListarEmpresaMovil,function(){},function(){});
		
	}else{
		callAjaxPost(URL + '/empresa', {}, procesarListarEmpresa);
	};
	
}
function mensajeLimiteEmpresa(){
	$("#modalLimiteEmpresa .modal-body").html
	("Estimado usuario:<br> ha excedido " +
			"el número de empresas permitido " +
			"en su licencia ("+getSession("numEmpresas")+"). " +
					"Contáctese con ventas@aswan.pe para mayor información<br>Atentamente,<br><br>El equipo Aswan")
	$("#modalLimiteEmpresa").modal('show');
	
}
function procesarListarEmpresaMovil(data){
	switch (data.CODE_RESPONSE) {
	case "05":
		$("#containerEmpresa").remove();  
		var list = data.list;
	var numEmpresaActual=data.list.length;
		
		var acceso=comprobarNumEmpresasUnidades(getSession("numEmpresas"),numEmpresaActual);
		 
		$("#nuevoEventoGosst").off("click");
		if(!acceso){
			 
			$("#nuevoEventoGosst").on("click",function(e){e.preventDefault();mensajeLimiteEmpresa()});
		}else{
			$("#nuevoEventoGosst").on("click",function(){
				$("#graficoResumenEventos").hide();
				$("#nuevoEventoGosst").hide();
				$("#buscarEventoGosst").hide(); 
				$("#empresaGosstMovil").css( { "top":"0px"});
				 
				var inputEvi="<div class='input-group'>"+
			   " <label class='input-group-btn'>"+
			    "<span class='btn btn-primary'>"+
			    "<i class='fa fa-camera' aria-hidden='true'></i>"+
			      "  Buscar Logo&hellip; <input style='display: none;' "+
			       " type='file' name='fileEviMovil' id='fileEviMovil' "+
			"accept='image/jpg,image/png,image/jpeg,image/gif'>"+
			   " </span>"+
			"</label>"+
			"<input type='text' class='form-control' readonly>"+
				" </div>";
				$("#barraMenu").hide();
				$("#empresaGosstMovil").append("<div class='principalMovil'><label>Datos Principales</label>" +
						"<div class='formNuevoEvento1'>" +
						"<input type='text' id='inputRuc' onkeydown='return IsNumeric(event);'  onkeyup='return IsNumericRestriccion(event);' class='form-control' placeholder='RUC' > " +
						"</div>"+
						
						
						"<div class='formNuevoEvento1'>" + 
						 "<input type='text' id='inputRS' class='form-control' placeholder='Razon Social'  >" +
						 "</div>"+
						 "<div class='formNuevoEvento1'> " +
							inputEvi+
							"</div>"+
						 "<div class='formNuevoEvento1' >" +
						 "<input type='text' id='inputDom' class='form-control' placeholder='Domicilio'  >"+
						"</div>"+
						 "</div>"+
						 "<div class='secundarioMovil'>"+
						 "<label>Datos Secundarios</label><div class='formNuevoEvento2'>" +
						 "<input type='text' id='inputRub' class='form-control' placeholder='Rubro' ></div>"+
						 
						"<div class='formNuevoEvento2'>" +
			            "<input type='text' id='inputAseg' class='form-control' placeholder='Aseguradora' required='true'>"+ 
						"</div>"+
						"<div class='formNuevoEvento2'>" +
						 "<input type='text' id='inputAnInic' onkeypress='return IsNumeric(event);' class='form-control' placeholder='A&ntilde;o Inicio' required='true'  >"+			"</div>"+
						"<div class='formNuevoEvento2'>" +
					 
						"</div>" +
						"</div>"+
						"<div class='cancelarFormMovil' >" +
						"<div><i class='fa fa-ban fa-2x' aria-hidden='true'></i>Cancelar</div>" +
						"</div>  "+
						"<div class='seguirFormMovil'  >" +
						"<div>Guardar <i class='fa fa-chevron-circle-right fa-2x' aria-hidden='true'></i></div>" +
						"</div>");
				 
				 
				$("#empresaGosstMovil .form-control:not(select)")
					.on("focus",function(){
						var inputId=$(this).prop("id");
						var textoSuperior="";
						switch(inputId){
						case "inputNom":textoSuperior=" - Descripción";
							break;
						case "inputLug":textoSuperior=" - Lugar"
							break;
						}
						$("#empresaGosstMovil .principalMovil").hide();
						$("#empresaGosstMovil .secundarioMovil").hide();
						$("#empresaGosstMovil .seguirFormMovil").hide();
						$("#empresaGosstMovil .cancelarFormMovil").hide();
						$(this).parent().parent().show().addClass("solitarioMovil");
						$(this).parent().parent().find("label:first").append(textoSuperior);
						$(".formNuevoEvento2").hide();
						$(".formNuevoEvento1").hide();
						$(this).parent().show();
					})
					.on("focusout",function(e){
					   //do stuff here
						$("#empresaGosstMovil .principalMovil").show().removeClass("solitarioMovil")
						.find("label:first").html("Datos Principales");
						$("#empresaGosstMovil .secundarioMovil").show().removeClass("solitarioMovil")
						.find("label:first").html("Datos Secundarios");
						$("#empresaGosstMovil .seguirFormMovil").show();
						$("#empresaGosstMovil .cancelarFormMovil").show();
						$(".formNuevoEvento2").show();
						$(".formNuevoEvento1").show();
					}) ;
				$(".cancelarFormMovil").on("click",function(){
					$("#empresaGosstMovil .principalMovil").remove();
					$("#empresaGosstMovil .secundarioMovil").remove();
					$("#empresaGosstMovil .seguirFormMovil").remove();
					$("#empresaGosstMovil .cancelarFormMovil").remove();
					$("#empresaGosstMovil label").remove();

					$("#empresaGosstMovil").css({ "top":"25px"});
					
					$("#graficoResumenEventos").hide();
					$("#nuevoEventoGosst").show();
					$("#buscarEventoGosst").hide();
					$("#barraMenu").show();
				});
				 
				
				$(".seguirFormMovil").on("click", function(){
					var valEvi=$("#fileEviMovil").val();
					var campoVacio=false;
					if (  $("#inputRuc").val().length == 0
								|| $("#inputRS").val().length == 0
								|| $("#inputDom").val().length == 0
								|| $("#inputRub").val().length == 0
								|| $("#inputAseg").val().length == 0
								|| $("#inputAnInic").val().length == 0
								) {
						campoVacio = true;
					}
				  
					if(!campoVacio){
						guardarEmpresaMovil();
					}else{
					alert("Faltan Completar Campos");
					}
					 
					});
				
				
				
				$(".formNuevoEvento1").show();
				$(".formNuevoEvento2").show();
			});
		}
		completarBarraCarga(); 
		break;
	default:
		alert("Ocurrió un error al traer las actos inseguros!");
	}
}
function procesarListarEmpresa(data) {

	switch (data.CODE_RESPONSE) {
	case "05":
		var list = data.list;
		$("#empresaGosstMovil").remove();
		listFullEmpresa=list;
		 listObjetivos=data.objetivos;
		$("#tblObjetivo tbody tr").remove();
		for(index=0;index<listObjetivos.length;index++){
			var indicador=listObjetivos[index].indicador.split("/");
		$("#tblObjetivo tbody").append(
		"<tr> " +
		" <td>"+listObjetivos[index].campo+"</td> " +
		" <td style='text-align:left'>"+listObjetivos[index].objetivo+"</td> " +
		" <td id='tdobj"+index+"'></td> " +
		" <td><input id='objetivo"+index+"' type='number' class='form-control' ></td> " +
		"   </tr>"		
		);
		if(index!=3){
			escribirFraccionSola("tdobj"+index,indicador[0],indicador[1]);
			$("#objetivo"+index).attr("max",100);
			$("#objetivo"+index).attr("step",1);
			$("#objetivo"+index).attr("min",1);
			$("#objetivo"+index).val("100");
		}else{
			$("#tdobj"+index).html(indicador[0]);
			$("#objetivo"+index).val("12");
		}
		
		}
		$("#modalObjetivoEmpresa .modal-title").html("Objetivos de SST propuestos por GOSST");
		var numEmpresaActual=data.list.length;
		
		var acceso=comprobarNumEmpresasUnidades(getSession("numEmpresas"),numEmpresaActual);
		 
		if(!acceso){
			$("#btnNuevo").attr("onclick", "mensajeLimiteEmpresa()");
		}else{
			$("#btnNuevo").attr("onclick", "nuevaEmpresa()");
		}
		
		
		$("#tblEmpresas tbody tr").remove();
		// var celdaEstilo=CELDA_ESTILO_PAR;
		var celdaEstilo;
		
		
		for (index = 0; index < list.length; index++) {
			// celdaEstilo=celdaEstilo==CELDA_ESTILO_PAR?CELDA_ESTILO_IMPAR:CELDA_ESTILO_PAR;
			$("#tblEmpresas tbody").append(
					"<tr onclick='javascript:editarEmpresa("
							+ list[index].idCompany
							+ ","+index+")' id='"
							+ list[index].idCompany
							+ "'><td id='"
							+ list[index].idCompany
							+ "ruc' "
							+ celdaEstilo
							+ ">"
							+ list[index].ruc
							+ "</td><td id='"
							+ list[index].idCompany
							+ "name' "
							+ celdaEstilo
							+ ">"
							+ list[index].name
							+ "</td><td id='"
							+ list[index].idCompany
							+ "address' "
							+ celdaEstilo
							+ ">"
							+ list[index].address
							+ "</td><td id='"
							+ list[index].idCompany
							+ "category' "
							+ celdaEstilo
							+ ">"
							+ list[index].category
							+ "</td><td id='"
							+ list[index].idCompany
							+ "insurance' "
							+ celdaEstilo
							+ ">"
							+ list[index].insurance
							+ "</td><td id='"
							+ list[index].idCompany
							+ "actInitYear' "
							+ celdaEstilo
							+ ">"
							+ list[index].activityInitialYear
							+ "</td><td id='"
							+ list[index].idCompany
							+ "logo' "
							+ celdaEstilo
							+ ">"
							+ insertarImagenParaTabla(list[index].logo, "50px",
									"50px") + "</td></tr>");
		}
		if(list.length==0){
			var divObjectBienvenida={
					divId:"",
					titulo:"¡Bienvenido!",
					cuerpo:"Tutorial de creación de empresa<br><br><button id='startTutorial' class='btn btn-success'>Empezar</button>"};
			crearOverlaySobreDiv(divObjectBienvenida);
			$("#startTutorial").on("click",function(){
				$(".mensajeBienvenida").remove();
				var divObject={
						divId:"btnNuevo",
						titulo:"Creando una empresa",
						cuerpo:"Empecemos registrando los datos de la empresa que vamos a gestionar."};
				resaltarDivTutorial(divObject);
			});
		
		}else{
			if(tutorialOn){
				var divObjectBienvenida={
						divId:"containerEmpresa",
						titulo:"¡Listo!",
						cuerpo:"Creaste tu primera empresa, ahora podrás acceder " +
								"al módulo Despliegue Organizacional y " +
								"registrar las unidades de negocio que necesites<br><br>" +
								"<button id='followTutorial' class='btn btn-success'>Ir al Despliegue</button>"};
				siguientePasoTutorial(divObjectBienvenida);
				tutorialOn=false;
				$("#followTutorial").on("click",function(){
					window.location.href =URL+ 'web/pages/mdf.html';	
				});
			}
		}
		
		formatoCeldaSombreable(true);
		completarBarraCarga();
		break;
	default:
		alert("Ocurrió un error al traer las empresas!");
	}

}

function editarEmpresa(idCompany,pindex) {
	if (!banderaEdicion) {
		idCompanyEdicion = idCompany;
		
		$("#btnEncabezado").show();
		$("#btnObjetivo").show();
		formatoCeldaSombreable(false);
		
		var objForm=listFullEmpresa[pindex].objetivoFormacion;
		var objExam=listFullEmpresa[pindex].objetivoExam;
		var objEpp=listFullEmpresa[pindex].objetivoEpp;
		var objPart=listFullEmpresa[pindex].objetivoParticipacion;
		var objDisp=listFullEmpresa[pindex].objetivoDisponibilidad;
		var objIperc=listFullEmpresa[pindex].objetivoIperc;
		
		$("#objetivo0").val(objForm*100);
		$("#objetivo1").val(objExam*100);
		$("#objetivo2").val(objEpp*100);
		$("#objetivo3").val(objPart);
		$("#objetivo4").val(objDisp*100);
		$("#objetivo5").val(objIperc*100);
		
		var name = $("#" + idCompany + "ruc").text();
		$("#" + idCompany + "ruc")
				.html(
						"<input type='text' id='inputRuc' class='form-control' placeholder='Razon Social' onkeydown='return IsNumeric(event);'  onkeyup='return IsNumericRestriccion(event);' value='"
								+ name + "'>");
		nombrarInput("inputRuc",name);
		var name = $("#" + idCompany + "name").text();
		$("#" + idCompany + "name")
				.html(
						"<input type='text' " +
						"id='inputRS' " +
						"class='form-control' " +
						"placeholder='Razon Social' " +
						"required='true'>");
		
		nombrarInput("inputRS",name);

		var address = $("#" + idCompany + "address").text();
		$("#" + idCompany + "address")
				.html(
						"<input type='text' id='inputDom' class='form-control' placeholder='Domicilio' required='true'>");
		$("#inputDom").attr("value", address);

		var category = $("#" + idCompany + "category").text();
		$("#" + idCompany + "category")
				.html(
						"<input type='text' id='inputRub' class='form-control' placeholder='Rubro' required='true'>");
		$("#inputRub").attr("value", category);

		var insurance = $("#" + idCompany + "insurance").text();
		$("#" + idCompany + "insurance")
				.html(
						"<input type='text' id='inputAseg' class='form-control' placeholder='Aseguradora' required='true'>");
		$("#inputAseg").attr("value", insurance);

		var activityInitialYear = $("#" + idCompany + "actInitYear").text();
		$("#" + idCompany + "actInitYear")
				.html(
						"<input type='text' id='inputAnInic' name='inputAnInic' class='form-control' onkeypress='return IsNumeric(event);' "
								+ "ondrop='return false;' onpaste='return false;' placeholder='Anio Inicio' style='width:100px;' value='"
								+ activityInitialYear + "'>");

		$("#" + idCompany + "logo")
				.html(
						"<a href='"
								+ URL
								+ "/empresa/logo?companyId="
								+ idCompany
								+ "' "
								+ "target='_blank'>Descargar</a>"
								+ "<br/><a href='#' onclick='javascript:mostrarCargarImagen("
								+ idCompany + ")'  id='subirimagen" + idCompany
								+ "'>Subir</a>");
		habilitarBotonesEdicion();

		banderaEdicion = true;
	}

}

function nuevaEmpresa() {
	
	
	if (!banderaEdicion) {
				
		$("#btnObjetivo").show();
		$("#tblEmpresas tbody")
				.append(
						"<tr id='0'><td><input type='text' id='inputRuc' onkeydown='return IsNumeric(event);'  onkeyup='return IsNumericRestriccion(event);'  class='form-control' placeholder='RUC' required='true' autofocus='true'></td>"
								+ "<td><input type='text' id='inputRS' class='form-control' placeholder='Razon Social' required='true'></td>"
								+ "<td><input type='text' id='inputDom' class='form-control' placeholder='Domicilio' required='true'></td>"
								+ "<td><input type='text' id='inputRub' class='form-control' placeholder='Rubro' required='true'></td>"
								+ "<td><input type='text' id='inputAseg' class='form-control' placeholder='Aseguradora' required='true'></td>"
								+ "<td><input type='text' id='inputAnInic' onkeypress='return IsNumeric(event);' class='form-control' placeholder='A&ntilde;o Inicio' required='true'  ></td>"
								+ "<td>...</td>" + "</tr>");
		idCompanyEdicion = 0;
		habilitarBotonesNuevo();
		banderaEdicion = true;
		if(tutorialOn){
			$("#btnCancelar").hide();
			$("#btnObjetivo").hide();
			var divObject={
					divId:"containerEmpresa",
					titulo:"Registrando Datos de Empresa",
					cuerpo:"Estos son datos que se usarán en los reportes GOSST"};
			siguientePasoTutorial(divObject);
		}
	} else {
		alert("Guarde primero.");
	}
}

function eliminarEmpresa() {
	var r = confirm("¿Está seguro de eliminar la empresa?");
	if (r == true) {
		var dataParam = {
			idCompany : idCompanyEdicion
		};

		callAjaxPost(URL + '/empresa/delete', dataParam,
				procesarEliminarEmpresa);
	}

}

function cancelarNuevaEmpresa() {
	listarEmpresa();
}

function guardarEmpresa() {

	var campoVacio = true;
	var nuevaEmpresa = 0;

	if ($("#inputRuc").val().length == 0) {
		campoVacio = false;
	}
	if ($("#inputRS").val().length == 0) {
		campoVacio = false;
	}
	if ($("#inputDom").val().length == 0) {
		campoVacio = false;
	}
	if ($("#inputRub").val().length == 0) {
		campoVacio = false;
	}
	if ($("#inputAseg").val().length == 0) {
		campoVacio = false;
	}
	if ($("#inputAnInic").val().length == 0) {
		campoVacio = false;
	}

	var objetivoFormacion=$("#objetivo0").val();
	var objetivoExam=$("#objetivo1").val();
	var objetivoEpp=$("#objetivo2").val();
	var objetivoParticipacion=$("#objetivo3").val();
	var objetivoDisponibilidad=$("#objetivo4").val();
	var objetivoIperc=$("#objetivo5").val();
	if (campoVacio) {

		var dataParam = {
			ruc : $("#inputRuc").val(),
			name : $("#inputRS").val(),
			address : $("#inputDom").val(),
			category : $("#inputRub").val(),
			insurance : $("#inputAseg").val(),
			activityInitialYear : $("#inputAnInic").val(),
			idCompany : idCompanyEdicion,
			objetivoFormacion:objetivoFormacion/100,
			objetivoExam:objetivoExam/100,
			objetivoEpp:objetivoEpp/100,		
			objetivoParticipacion:objetivoParticipacion,
			objetivoDisponibilidad:objetivoDisponibilidad/100,			
			objetivoIperc:objetivoIperc/100
		};
		
		callAjaxPost(URL + '/empresa/save', dataParam, procesarGuardarEmpresa);
	} else {
		alert("Debe ingresar todos los campos.");
	}
}

function procesarGuardarEmpresa(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		
		$("#tblEmpresas tbody tr").remove();
		listarEmpresa();
		deshabilitarBotonesEdicion();
		if(tutorialOn){
			
		}else{
			location.reload();
		}
		
		
		banderaEdicion = false;
		break;
	default:
		alert("Ocurrió un error al guardar la empresa!");
	}
}

function procesarEliminarEmpresa(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		listarEmpresa();
		deshabilitarBotonesEdicion();
		cargarEmpresasxUsuario();
		banderaEdicion = false;
		break;
	default:
		alert("Ocurrió un error al eliminar la empresa!");
	}
}

function mostrarCargarImagen(companyId) {
	$('#mdUpload').modal('show');
	$('#mdUpload .modal-title:first').html("Subir logo  ");

	$('#mdUpload .modal-title:last').html(" (Tamaño recomendable: 100x150)");
	$('#btnUploadLogo').attr("onclick",
			"javascript:uploadLogo(" + companyId + ");");
}

function uploadLogo(companyId) {
	var inputFileImage = document.getElementById("fileEvi");
	var file = inputFileImage.files[0];
	var data = new FormData();

	if (file.size > 512000) {
		alert("Lo sentimos, solo se pueden subir archivos menores a 500Kb.");
	} else {
		data.append("fileEvi", file);
		data.append("companyId", companyId);

		var url = URL + '/empresa/logo/save';
		$.blockUI({
			message : 'cargando...'
		});
		$
				.ajax({
					url : url,
					type : 'POST',
					contentType : false,
					data : data,
					processData : false,
					cache : false,
					success : function(data, textStatus, jqXHR) {
						$.unblockUI();
						switch (data.CODE_RESPONSE) {
						case "06":
							sessionStorage.clear();
							document.location.replace(data.PATH);
							break;
						default:
							console.log('Se subio el archivo correctamente.');
							$('#mdUpload').modal('hide');
						}

					},
					error : function(jqXHR, textStatus, errorThrown) {
						$.unblockUI();
						alert("Ups, tenemos problemas tecnicos. Intente más tarde. Error: "
								+ errorThrown);
						console.log('xhRequest: ' + jqXHR + "\n");
						console.log('ErrorText: ' + textStatus + "\n");
						console.log('thrownError: ' + errorThrown + "\n");
					}
				});
	}
}
function obtenerTablaClipboard() {
	
	new Clipboard('#btnClipTabla', {
		text : function(trigger) {
			
			return listFullTablaIndicadores;
		}
	});
	alert("Se han guardado los indicadores al Clipboard")
}



function procesarIndicadorEmpresa(data){

	if(data.CODE_RESPONSE=="05"){
var indicadorFormacion=	0;
var indicadorAcumulado=0;
		var indicadorEntregaEpp=	0;
		var indicadorDisponibilidad=	0;
		var indicadorParticipacion=	0;
		var indicadorExamenMedico=	0;
for(index=0;index<data.indFormacion.length;index++){
	indicadorAcumulado=data.indFormacion[index].cumplimientoFormacion+indicadorAcumulado;
	var size=data.indFormacion.length ;
	(index+1==size ? indicadorFormacion=indicadorAcumulado/size : 0)
}
indicadorAcumulado=0;
for(index=0;index<data.indEpp.length;index++){
	var entregadosVigentes= data.indEpp[index].entregasLastYear;
	var asignados=data.indEpp[index].eppAsignados;
	if( asignados>0){
		indicadorAcumulado=(entregadosVigentes/asignados)+indicadorAcumulado;
	}else{
		indicadorAcumulado=1+indicadorAcumulado;
	}
	
	var size=data.indEpp.length ;
	(index+1==size ? indicadorEntregaEpp=indicadorAcumulado/size : 0)
}
indicadorAcumulado=0;
for(index=0;index<data.indAcc.length;index++){
	indicadorAcumulado=data.indAcc[index].indicadorDiasDescanso+indicadorAcumulado;
	var size=data.indAcc.length ;
	(index+1==size ? indicadorDisponibilidad=indicadorAcumulado/size : 0)
}
indicadorAcumulado=0;
for(index=0;index<data.indEvento.length;index++){
	indicadorAcumulado=data.indEvento[index].numEventosReportados+indicadorAcumulado;
	var size=data.indEvento.length ;
	(index+1==size ? indicadorParticipacion=indicadorAcumulado/size : 0)
}
indicadorAcumulado=0;
for(index=0;index<data.indExam.length;index++){
	indicadorAcumulado=data.indExam[index].vigenciaMedicaPuntaje+indicadorAcumulado;
	var size=data.indExam.length ;
	(index+1==size ? indicadorExamenMedico=indicadorAcumulado/size : 0)
}

	var	 evalRealizadas=data.indIperc[0].evalRealizadas;
	var	 evalRiesgosas=data.indIperc[0].evalRiesgosas;
		
		var indicadorIperc=(evalRealizadas==0 ? 0:(evalRealizadas-evalRiesgosas)/evalRealizadas)	;
		
		var objIndicadores=[indicadorFormacion, 
		                    indicadorExamenMedico, 
		                    indicadorEntregaEpp, 
				               indicadorParticipacion, 
				               indicadorDisponibilidad,
				               indicadorIperc];
		
		 listFullTablaIndicadores="";
		listFullTablaIndicadores="Campo" +"\t"
		+"Objetivo" +"\t"
		+"Indicador" +"\t"
		+"Meta" +"\t"
		+"Resultado" +"\t"+"\n";
		for (var index = 0; index < listObjetivos.length; index++){
			
			listFullTablaIndicadores=listFullTablaIndicadores
			+listObjetivos[index].campo+"\t"
			+listObjetivos[index].objetivo+"\t"
			+listObjetivos[index].indicador+"\t"
			+$("#objetivo"+index).val()+"\t"
			+(objIndicadores[index])+"\t"
			+"\n"

		}

		$("#modalObjetivoEmpresa").modal("show");
	}else{
		console.log("T")
	}
			

			
}

