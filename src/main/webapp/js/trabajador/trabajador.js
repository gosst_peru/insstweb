var banderaEdicion;
var trabajadorId;
var positionId;
var sangreTipoId;
var generoId;
var listPosition;
var listSangre;
var listGenero;
//Listas intertek
var listTipoDoc;
var listEstadoCivil;
var listTipoAfp;
var listTipoEps;
var listDepa;
var listLocacion;
//
var listDiscapacidad;
var fechaIngreso;
var fechaRetiro;
var fechaNacimiento;
var tipoDiscapacidad;
var tipoDuracionTrab;
var contador2 = 0;

var listTrabajadores,listTrabajadoresActivos;
var listFullTrabajadores=[];
var indicadoresSeguridad;
var contadorListTrab;
var listDuracion;
var listFullIndicadoresTrabajadores=[];
var listFullIndicadoresTrabajadoresAux=[];
var accesoGarantizado;
var passwordTrabajador;
//var de filtros
var toggleLocacion=true;
var arrayFiltroLocacion=[];
var arrayFiltroSangre=[];
var columnasOcultar=[7,13,14,15,16,19,25,26,27,28,29,30,31,38,39,40];
$(document).ready(function() {
	$("#btnGuardarModalContacto").on("click",function(){
		$("#modalContactos").modal("hide");
	});
	$("#resetBuscadorRetiro").on("click",function(){
		$("#buscadorRetiroInput").val("");
		filtrarModalRetiro();
	});
	$('#buscadorRetiroInput').keyup(function(e){
		 
		 
		
		filtrarModalRetiro();
	});
	 
	
	$('#mdEvent').on(
			'show.bs.modal',
			function(e) {
				cargarModalEvent();
				var trabNombre = $("#inputNom").val();
				$("#mdEvent .modal-title")
						.html(
								"Informe resumen de " + "actividades relacionadas "
										+ "la Seguridad y Salud en el Trabajo de '"
										+ trabNombre + "' al "
										+ obtenerFechaActualNormal());
			});
	$("#txtListado").on("paste",function(){
		var textoPasted=window.clipboardData.getData('Text');
		$(this).val(textoPasted.substring(0,textoPasted.length - 1));
		console.log(textoPasted)
	});
	$('#buscadorTrabajadorInput').keyup(function(e) {
		if (e.keyCode == 13) {
			$(this).trigger("enterKey");
		}
	});
	$('#buscadorTrabajadorInput').bind("enterKey", function(e) {
		
		cargarPrimerEstado();
	
	});
	$("#buscadorTrabajadorInput").focus();

	$("#resetBuscador").attr("onclick", "resetBuscador()"); 
insertMenu(cargarPrimerEstado);
	
	
	if(getSession("linkCalendarioGrupoId")!=null){
	
		programarFecha();
		
	}
	listFullIndicadoresTrabajadores=[];
	accesoGarantizado=false;
	$('#inputClaveTrabajador').keyup(function(e){
	    if(e.keyCode == 13)
	    {
	        $(this).trigger("enterKey");
	    }
	});
	$('#inputClaveTrabajador').bind("enterKey",function(e){
		
		cargarPrimerEstado(llamarIndicadores);
		
		});
	
	
		
});
function llamarIndicadores(){
	var palabraClave = $("#buscadorTrabajadorInput").val();
	if (palabraClave.length == 0) {
		palabraClave = null;
	} else {
		palabraClave = ("%" + palabraClave + "%").toUpperCase();
	}
	var dataParam = {
			mdfId : getUrlParameter("mdfId"),
			palabraClave:palabraClave
		};
	if(getSession("trabajadorGosstId")>0){
		dataParam = {
				trabajadorId :getSession("trabajadorGosstId")
			};
		}
	 
callAjaxPostNoLoad(URL + '/trabajador/puntajes', dataParam,
			procesarPuntajesSeguridadTrabajadores,function(){
	listFullTrabajadores.forEach(function(val,index){ 
		$("#tr"+val.trabajadorId).find("#tdprom").html("Loading...");
	});
	});
}

function resetBuscador() {
	
	$("#buscadorTrabajadorInput").val("");
	cargarPrimerEstado();
	
}
 
function llamarCeldaTabla(pindex){
	pindex = defaultFor(pindex,null);
	
	
	if(pindex==null){
		$("#0").remove();
	}else{
		 
		var celda= "<td id='tdprom' "
		
		+ ">"
		+(listFullIndicadoresTrabajadores.length>0 ? 
				listFullIndicadoresTrabajadores[pindex].promedio :"..."		)
			
	
		+"</td>"
		+obtenerRestoTablaTrabajador(listFullTrabajadores[pindex])
		;
		$("#tr" + listFullTrabajadores[pindex].trabajadorId)
		.html(celda)
		.addClass("info");
		if(listFullTrabajadores[pindex].fechaRetiro!=null){
		$("#tblTrab tbody #tr"+listFullTrabajadores[pindex].trabajadorId+" td").css(
			{
				"background-color":"#E6E6E6"
			}
				
			);	
		}
		
		if(parseInt(sessionStorage.getItem("perfilIntertek"))==1){
			 $("#btnEstadisticaTrab").remove();
			columnasOcultar.forEach(function(val){
				$('#tblTrab td:nth-child('+val+'),#tblTrab th:nth-child('+val+')').hide();
			});
			 
		}
		
	}
	formatoCeldaSombreable(true);
	banderaEdicion = false;
	trabajadorId = 0;
	positionId = 0;
	sangreTipoId = 0;
	generoId = 0;
	tipoDiscapacidad = 0;
	listTrabajadores = "";
	listTrabajadoresActivos="",
	contadorListTrab = 0;
	tipoDuracionTrab = 0;
	
	removerBotones();
	crearBotones();
	$("#fsBotones")
	.append(
			"<button id='btnEstadisticaTrab' type='button' class='btn btn-success' title='Ver Estadisticas'>"
					+ "<i class='fa fa-bar-chart fa-2x'></i>" + "</button>");
	$("#btnEstadisticaTrab").on("click",function(){
		iniciarEstadisticasTrabajador()
		
	});
	$("#fsBotones")
			.append(
					"<button id='btnVistaEventos' type='button' class='btn btn-success' title='Ver Eventos'>"
							+ "<i class='fa fa-table fa-2x'></i>" + "</button>");
	$("#btnNuevo")
	.after(
			"<button id='btnGrupoTrabajador' type='button' class='btn btn-success' title='Agrupar trabajadores'>"
					+ "<i class='fa fa-users fa-2x'></i>" + "</button>");
	
	$("#btnGrupoTrabajador").on("click",function(){
		programarFecha();
	});
	
	$("#fsBotones")
	.append(
			"<button id='btnClipTabla' type='button' class='btn btn-success' title='Tabla Clipboard' >"
					+ "<i class='fa fa-clipboard fa-2x'></i>" + "</button>");
	$("#fsBotones")
	.append(
			"<button id='btnIndicadores' type='button' class='btn btn-success' title='Cargar Indicadores' >"
					+ "<i class='fa fa-star-o fa-2x'></i>" + "</button>");
	$("#fsBotones")
	.append(
			"<button id='btnRetiroTrabajadores' type='button' class='btn btn-success' title='Retiro Múltiple' >"
					+ "<i class='fa fa-clock-o  fa-2x'></i>" + "</button>");
	
	$("#fsBotones")
	.append("<button id='btnReportExcel' type='button' "
	  + " class='btn btn-success btn-excel' >"
		+"<i class='fa fa-file-excel-o fa-2x'></i></button>");
	$("#btnReportExcel").on("click",function(){
		window.open(URL
				+ "/trabajador/reporte/excel?mdfId="
				+ getUrlParameter("mdfId") + '&nada=_blank');	
	});
	deshabilitarBotonesEdicion();
	$("#btnVistaEventos").hide();
	
	$("#btnNuevo").attr("onclick", "javascript:nuevoTrabajador();");
	$("#btnCancelar").attr("onclick", "javascript:cancelarTrabajador();");
	$("#btnGuardar").attr("onclick", "javascript:guardarTrabajador();");
	$("#btnEliminar").attr("onclick", "javascript:eliminarTrabajador();");
	$("#btnVistaEventos").attr("onclick", "javascript:verVistaEventos();");
	$("#btnUpload").attr("onclick", "javascript:importarDatos();");
	$("#btnClipTabla").attr("onclick", "javascript:obtenerTablaClipboard();");
	$("#btnIndicadores").attr("onclick", "javascript:llamarIndicadores();");
	$("#btnRetiroTrabajadores").attr("onclick", "javascript:verModalRetiro();");
	
	if(parseInt(sessionStorage.getItem("accesoUsuarios")) == 0){
		 $("#btnNuevo").remove();
		 $("#btnGrupoTrabajador").remove();
		 $("#btnUpload").remove();
		 $("#btnEliminar").remove();
		 $("#btnRetiroTrabajadores").remove();
		 $("#btnReportExcel").remove();
	}
}
function cargarPrimerEstado(functionCallBack) {
	
	var palabraClave = $("#buscadorTrabajadorInput").val();
	if (palabraClave.length == 0) {
		palabraClave = null;
	} else {
		palabraClave = ("%" + palabraClave + "%").toUpperCase();
	}
	llamarCeldaTabla();
	 
	var dataParam = {
		mdfId : getUrlParameter("mdfId"),
		palabraClave:palabraClave,
		trabajadorId:parseInt(sessionStorage.getItem("trabajadorGosstId")),
		empresaId:sessionStorage.getItem("gestopcompanyid")
	};
	 
	$.blockUI({message:'<img src="../imagenes/gif/ruedita.gif"></img>cargando...'});
	$.ajax({
		url : URL + '/trabajador',
		type : 'post',
		async : true,
//		crossDomain: true,
		xhrFields: {
            withCredentials: true
        },
		data : JSON.stringify(dataParam),
		contentType : "application/json",
		success : function(data, textStatus, jqXHR) {
			
		
			switch (data.CODE_RESPONSE) {
			case "06":
				sessionStorage.clear();
				document.location.replace(data.PATH);
				break;
			case "07":
				//sessionStorage.clear();
				alert("Sesion expirada");
				//document.location.replace(data.PATH);
				break;
			case "09":
				alert("No tiene permiso para ingresar a este módulo");
				break
			default:
				if (isMobile.any()) {
					procesarDataMovilDescargadaPrimerEstado(data);
				} else {
					procesarDataDescargadaPrimerEstado(data);
					
				}
				
			
			if(functionCallBack){
				functionCallBack();
			}else{
				$.unblockUI();
			}		
			}

		},
		error : function(jqXHR, textStatus, errorThrown) {
			$.unblockUI();
			console.log("Ups, tenemos problemas tecnicos. Intente más tarde. Error: "
					+ errorThrown);
		
			var responseText = jQuery.parseJSON(jqXHR.responseText);
			if(responseText.CODE_RESPONSE == "07")
				alert("No tiene permiso!!!");
			//document.location.replace(responseText.PATH);
		}
	});
	


}
var indFormacion;
var indEpp;
var indExam;
function procesarPuntajesSeguridadTrabajadores(data){
	switch (data.CODE_RESPONSE) {
	case "05":
		
		indicadoresSeguridad = data.indicadoresSeguridad;

		var indEvento = data.indEvento;
		var indAcc = data.indAcc;
		 indEpp = data.indEpp;
		 indExam = data.indExam;
		 indFormacion = data.indFormacion;
		 listFullIndicadoresTrabajadores=[];
		for (var index = 0; index < listFullTrabajadores.length; index++){
			var indicadorForTrab=0;
			var indicadorEntrega=0;
			var indicadorAcc=1;
			var sumaDiasDescanso=0;
			var diasTrabajados=0;
			var indicadorPart=0;
			var indicadorExamTrab=0;
			listFullIndicadoresTrabajadoresAux.push({
				indicadorFormacion :0,
				indicadorEntregaEpp:1,
				indicadorDisponibilidad:1, 
			indicadorParticipacion:0,
				indicadorExamenMedico:0,
				id:listFullTrabajadores[index].trabajadorId
			})
			
			
			// ////////
			for (index1 = 0; index1 < indEvento.length; index1++) {
				if (listFullTrabajadores[index].trabajadorId == indEvento[index1].trabajadorId) {
					indicadorPart = indEvento[index1].numEventosReportados;
					index1 = indEvento.length;

				}

			}
			// ////////indFormacion
			indFormacion.every(function(val1,index1){
				if (listFullTrabajadores[index].trabajadorId == val1.trabajadorId) {
				    
				indicadorForTrab=(val1.capacitacionesAsignadas>0?
						 (val1.capacitacionesValidas/val1.capacitacionesAsignadas):1);
					return false;
				}else{
					return true;
				}
			 });
			 
			//////////
			indExam.every(function(val1,index1){
				
				if (listFullTrabajadores[index].trabajadorId == val1.trabajadorId) {
					indicadorExamTrab=(val1.resultadoMedicoId );
					return false;
				}else{
					return true;
				}
			 });
			 
			// //////////
			indEpp.every(function(val1,index1){
				if (listFullTrabajadores[index].trabajadorId == val1.trabajadorId) {
				indicadorEntrega=(val1.eppsAsignados>0?
						 (val1.eppsValidos/val1.eppsAsignados):1);
				return false;
				}else{
					return true;
				}
			});
			 
			// //////////
			for (index5 = 0; index5 < indAcc.length; index5++) {
				if (listFullTrabajadores[index].trabajadorId == indAcc[index5].trabajadorId) {
					indicadorAcc = indAcc[index5].indicadorDiasDescanso;
					sumaDiasDescanso=indAcc[index5].sumaDiasDescanso;
					diasTrabajados=   indAcc[index5].diasTrabajados
					index5 = indAcc.length;

				}

			}
			// ///////////
			listFullIndicadoresTrabajadoresAux[index].indicadorFormacion = indicadorForTrab;
			listFullIndicadoresTrabajadoresAux[index].indicadorEntregaEpp = indicadorEntrega;
			listFullIndicadoresTrabajadoresAux[index].indicadorDisponibilidad = indicadorAcc;
			listFullIndicadoresTrabajadoresAux[index].sumaDiasDescanso = sumaDiasDescanso;
			listFullIndicadoresTrabajadoresAux[index].diasTrabajados = diasTrabajados;
			listFullIndicadoresTrabajadoresAux[index].indicadorParticipacion = indicadorPart;
			listFullIndicadoresTrabajadoresAux[index].indicadorExamenMedico = indicadorExamTrab;
			
			// //////////
			var promedio = (indicadorForTrab + indicadorExamTrab
					+ indicadorEntrega + indicadorAcc + (indicadorPart / indicadoresSeguridad.indicadorMinimoParticipacion)

			) / 5;
	
			 
		$("#tr"+listFullTrabajadores[index].trabajadorId+" #tdprom").html(
				  pasarDecimalPorcentaje(promedio, 2)
									 	
				);
			 
		listFullIndicadoresTrabajadores.push(
				{
					id:listFullTrabajadores[index].trabajadorId,
					promedio:pasarDecimalPorcentaje(promedio, 2)
				}
				
		 
			)
		}
		break;
		default:
			alert("No pOS");
		break;
}
	
}

function procesarDataDescargadaPrimerEstado(data){

   resizeDivWrapper("wrapper",190);
    
	switch (data.CODE_RESPONSE) {
	case "05":
		$("#trabajadoresGosstMovil").hide();
		$("#trabajadoresGosstDesktop").show();
		var list = data.list;
		listPosition = data.listPosition;
		listPosition=listPosition.filter(function(data){
			if(data.isPerfil==0 || data.isPerfil ==null){
				return data;
			}
		});
		listSangre = data.listSangre;
		listGenero = data.listGenero;
		listDiscapacidad = data.listDisc;
		listDuracion = data.listDuracion;
		
		 listTipoDoc=data.listTipoDoc;
		 listEstadoCivil=data.listEstadoCivil;
		 listTipoAfp=data.listTipoAfp;
		 listTipoEps=data.listTipoEps;
		 listDepa=data.listDepa;
		 listLocacion=data.listLocacion;
		 
		listFullTrabajadores = list; 
		var puestos=[];
		$("#tblTrab tbody tr").remove();
		for (var index = 0; index < list.length; index++) {
			////
			var puestoAgregado=false;
			puestos.forEach(function(val,index1){
				if(val.id==list[index].puesto.positionId){
					puestoAgregado=true;
				}
				
			});
			if(!puestoAgregado){
				puestos.push({
					id:list[index].puesto.positionId,
					nombre:list[index].puesto.positionName,
					activos:0,
					inactivos:0
				});
				
			}
			
			puestos.forEach(function(val,index1){
				if(val.id==list[index].puesto.positionId){
					if(list[index].fechaRetiro == null){
						val.activos+=1
					}else{
						val.inactivos+=1
					}
				
				}
				
			});
			listTrabajadoresActivos=puestos;
			///
			if (listFullTrabajadores[index].fechaRetiro == null) {
				listTrabajadores = listTrabajadores
						+ listFullTrabajadores[index].trabajadorNombre + "\n";
				contadorListTrab = contadorListTrab + 1;
				
			}else{
				
			}
			var promedioTrab="...";
			 
				listFullIndicadoresTrabajadores.forEach(function(val1,index1){
					if(list[index].trabajadorId==val1.id){
						
						promedioTrab=val1.promedio;
						 
					}
					 
				});
			$("#tblTrab tbody").append(

					"<tr id='tr" + list[index].trabajadorId
							+ "' onclick='javascript:editarTrabajador("
							+ list[index].trabajadorId + ","
							+ list[index].puesto.positionId + ","
							+ list[index].sangre.sangreTipoId + ","
							+ list[index].genero.generoId + ","
							+ list[index].fechaIngreso + ","
							+ list[index].fechaRetiro + ","
							+ list[index].fechaNacimiento + ","
							+ list[index].tipoDiscapacidad + ","
							+ list[index].tipoDuracion.id + "," + index
							+ ")'  "

							+ ">" + "<td id='tdprom' "
							
							+ ">"
							+ promedioTrab
								
						
							+"</td>"

							+obtenerRestoTablaTrabajador(list[index])

							+ "</tr>");
			
			if(list[index].fechaRetiroTexto!=''){
				$("#tblTrab tbody #tr"+list[index].trabajadorId+" td").css(
						{
							"background-color":"#E6E6E6"
						}
							
						);	
			}
			
		}
		if(parseInt(sessionStorage.getItem("perfilIntertek"))==1){
			 $("#btnEstadisticaTrab").remove();
			columnasOcultar.forEach(function(val){
				$('#tblTrab td:nth-child('+val+'),#tblTrab th:nth-child('+val+')').hide();
			});
			 
		}
		completarBarraCarga();
		agregarFiltroGosst("filtroLocalTrabajador","menuFiltroLocacion",listLocacion,"id","nombre",
				toggleLocacion,arrayFiltroLocacion,function(lista){
			arrayFiltroLocacion=lista
			listFullTrabajadores.forEach(function(val,index){
				if( arrayFiltroLocacion.indexOf(parseInt(val.locacion.id))!=-1
						&& arrayFiltroSangre.indexOf(parseInt(val.sangre.sangreTipoId))!=-1){
					$("#tr"+val.trabajadorId).show();
					}else{ 
					$("#tr"+val.trabajadorId).hide();
				}
			});
		});
		agregarFiltroGosst("filtroSangreTrabajador","menuFiltroSangre",listSangre,"sangreTipoId","sangreTipoNombre",
				toggleLocacion,arrayFiltroSangre,function(lista){
			arrayFiltroSangre=lista;
			listFullTrabajadores.forEach(function(val,index){
				if( arrayFiltroLocacion.indexOf(parseInt(val.locacion.id))!=-1
						&& arrayFiltroSangre.indexOf(parseInt(val.sangre.sangreTipoId))!=-1){
					$("#tr"+val.trabajadorId).show();
					}else{ 
					$("#tr"+val.trabajadorId).hide();
				}
			});
		});
		
		arrayFiltroLocacion=listarStringsDesdeArray(listLocacion, "id");
		arrayFiltroSangre=listarStringsDesdeArray(listSangre, "sangreTipoId");
		if(parseInt(sessionStorage.getItem("accesoUsuarios")) == 0){
			 $("#btnNuevo").remove();
			 $("#btnGrupoTrabajador").remove();
			 $("#btnUpload").remove();
			 $("#btnEliminar").remove();
			 $("#btnRetiroTrabajadores").remove();
			 $("#btnReportExcel").remove();
		}
		if(parseInt(sessionStorage.getItem("accesoUsuarios")) ==1){
		
			
			
			accesoGarantizado=true;
		}else{
		
			
		}
		formatoCeldaSombreableTabla(true,"tblTrab");

		break;
	default:
		alert("Ocurrió un error al traer los Trabajadores!");
	}
	if (contador2 == 0) {
		goheadfixed('#tblTrab');
	}
	contador2 = 1;
}
function verificarDni(){
	var existe=false
	listFullTrabajadores.forEach(function(val,index){
		if(val.dni==$("#inputDni").val() && val.trabajadorId!=trabajadorId){
			existe=true;
		}
	});
	if(existe){
		alert("DNI ya registrado");
		$("#inputDni").val("");
	}
}
function editarTrabajador(ptrabajadorId, ppositionId, psangreTipoId, pgeneroId,
		pfechaIngreso, pfechaRetiro, pfechaNacimiento, ptipoDiscapacidad,
		ptipoDuracionTrab, pindex) {
	if (!banderaEdicion) {
		trabajadorId = ptrabajadorId;
		positionId = ppositionId;
		sangreTipoId = psangreTipoId;
		generoId = pgeneroId;
		fechaIngreso = pfechaIngreso;
		fechaRetiro = listFullTrabajadores[pindex].fechaRetiro;
		fechaNacimiento = pfechaNacimiento;
		tipoDiscapacidad = ptipoDiscapacidad;
		tipoDuracionTrab = ptipoDuracionTrab;
		
//      $("#tr"+trabajadorId).addClass("fila-edicion");
		
		var indicadorFormacion = listFullTrabajadores[pindex].indicadorFormacion;
		var indicadorEntregaEpp = listFullTrabajadores[pindex].indicadorEntregaEpp;
		var indicadorDisponibilidad = listFullTrabajadores[pindex].indicadorDisponibilidad;
		var indicadorParticipacion = listFullTrabajadores[pindex].indicadorParticipacion;
		var indicadorExamenMedico = listFullTrabajadores[pindex].indicadorExamenMedico;

		var observaciones =listFullTrabajadores[pindex].observacion;
		var pass =listFullTrabajadores[pindex].contrasenia;
		$("#tdobs" + trabajadorId).html(
				"<input type='text' id='inputObs' " + "class='form-control' "
						+ "style='width:120px' " + "placeholder='Observación' "
						+ ">");
		nombrarInput("inputObs",observaciones);
		$("#tdpass" + trabajadorId).html(
				"<input type='text' id='inputPass' " + "class='form-control' "
						+ "  " + "placeholder='Pass' "
						+ ">");
		nombrarInput("inputPass",pass);
		formatoCeldaSombreable(false);
		
		$("#tr"+listFullTrabajadores[pindex].trabajadorId+" #tdprom").html(
				"<a onclick='verRadialTrabajador(" + pindex
									+ ")'>" + $("#tr"+listFullTrabajadores[pindex].trabajadorId+" #tdprom:first").text()
									+ "</a>"		
				);	
		var name = $("#tddni" + trabajadorId).text();
		$("#tddni" + trabajadorId).html(
				"<input type='text' id='inputDni' onkeyup='verificarDni()' " + "class='form-control' "
						+ " " + "placeholder='' "
						+ " value='" + name + "'>");
		//
		var docTipo=listFullTrabajadores[pindex].doc.id;
		var selTipoDoc = crearSelectOneMenuOblig("selTipoDoc", "", listTipoDoc,
				docTipo, "id", "nombre");
		$("#tddoc"+trabajadorId).html(selTipoDoc);
		
		var civilTipo=listFullTrabajadores[pindex].estadoCivil.id;
		var selEstadoCivil = crearSelectOneMenuOblig("selEstadoCivil", "", listEstadoCivil,
				civilTipo, "id", "nombre");
		$("#tdcivil"+trabajadorId).html(selEstadoCivil);
		
		var afpTipo=listFullTrabajadores[pindex].afp.id;
		var selTipoAfp = crearSelectOneMenuOblig("selTipoAfp", "", listTipoAfp,
				afpTipo, "id", "nombre");
		$("#tdafp"+trabajadorId).html(selTipoAfp);
		
		var epsTipo=listFullTrabajadores[pindex].eps.id;
		var selTipoEps = crearSelectOneMenuOblig("selTipoEps", "", listTipoEps,
				epsTipo, "id", "nombre");
		$("#tdeps"+trabajadorId).html(selTipoEps);
		//
		var name1 = $("#tdnom" + trabajadorId).text();

		$("#tdnom" + trabajadorId)
				.html("<input type='text' id='inputNom' class='form-control' placeholder='Nombre' value='"
								+ name1 + "'>");
		//
		
		var apellidPaterno= $("#tdapP" + trabajadorId).text();

		$("#tdapP" + trabajadorId)
				.html("<input type='text' id='inputApellidoP' class='form-control' placeholder='Ap. Paterno' value='"
								+ apellidPaterno + "'>");
		//
		var apellidMaterno= $("#tdapM" + trabajadorId).text();
		$("#tdapM" + trabajadorId)
				.html("<input type='text' id='inputApellidoM' class='form-control' placeholder='Ap. Materno' value='"
								+ apellidMaterno + "'>");
		
		//
		var locacion=listFullTrabajadores[pindex].locacion.id;
		var selTipoLocacion = crearSelectOneMenuOblig("selTipoLocacion", "", listLocacion,
				locacion, "id", "nombre");
		$("#tdloca"+trabajadorId).html(selTipoLocacion);
		//
		var conyugue= $("#tdcony" + trabajadorId).text();
		$("#tdcony" + trabajadorId)
				.html("<input type='text' id='inputConyugueNombre' class='form-control' placeholder='Conyugue' value='"
								+ conyugue + "'>");
		//
		var mailPersonal= $("#tdmailp" + trabajadorId).text();
		$("#tdmailp" + trabajadorId)
				.html("<input type='text' id='inputCorreoPersonal' class='form-control' placeholder='Correo Personal' value='"
								+ mailPersonal + "'>");
		//
		var fonoCasa=listFullTrabajadores[pindex].telefonoCasa;
		$("#inputTelefonoCasa").val(fonoCasa);
		var fonoPersonal=listFullTrabajadores[pindex].telefonoPersonal;
		$("#inputTelefonoPersonal").val(fonoPersonal);
		var fonoOtro=listFullTrabajadores[pindex].telefonoOtro;
		$("#inputTelefonoOtro").val(fonoOtro);
		var direccionCasa=listFullTrabajadores[pindex].direccion;
		$("#inputDireccionCasa").val(direccionCasa);
		
		var distritoCasa=listFullTrabajadores[pindex].distrito;
		$("#inputDistritoCasa").val(distritoCasa);
		var provinciaCasa=listFullTrabajadores[pindex].provincia;
		$("#inputProvinciaCasa").val(provinciaCasa);
		var depa=listFullTrabajadores[pindex].departamento.id;
		var selDepartamento = crearSelectOneMenuOblig("selDepartamento", "", listDepa,
				depa, "id", "nombre");
		$("#tdDepa").html(selDepartamento);
		
		var textInfo=$("#tdinfo"+trabajadorId).text();
		$("#tdinfo"+trabajadorId).addClass("linkGosst")
		.on("click",function(){verInfoExtraTrabajador();})
		.html("<i class='fa fa-list-ul fa-2x' aria-hidden='true' ></i>"+textInfo);
		
		var eviNombre=listFullTrabajadores[pindex].evidenciaVerificacionNombre;
		crearFormEvidencia("Veri",false,"#tdEviVeri");
		$("#btnDescargarFileVeri").attr("onclick","location.href='"+ URL
				+ "/trabajador/verificacion/evidencia?trabajadorId="
				+ trabajadorId+"'");
		$("#fileEvi"+"Veri").on("change",function(){
			$("#btnDescargarFile").remove();
			$("#tdEviVeri").prepend("<div id='msgEvi'><i class='fa fa-spin fa-spinner'></i>Subiendo archivo</div>")
			guardarEvidenciaMultipleAuto(trabajadorId,"fileEviVeri"
					,bitsCese,"/trabajador/verificacion/evidencia/save",function(){
					$("#msgEvi").html("Completado");
			});
			
			
		});
		$("#inputEviVeri").val(eviNombre);
		//
		
		var textContact=listFullTrabajadores[pindex].numContactos;
		$("#tdContacTrab").addClass("linkGosst")
		.on("click",function(){verContactosTrabajador();})
		.html("<i class='fa fa-list-ul fa-2x' aria-hidden='true' ></i>"+textContact);
		
		//
		var name4 = $("#tdsuc" + trabajadorId).text();
		$("#tdsuc" + trabajadorId).html(
				"<input type='text' id='inputSuc' " + "style='width:190px' "
						+ "class='form-control' placeholder='Correo Coorporativo' value='"
						+ name4 + "'>");
		var name5 = $("#tddiscap" + trabajadorId).text();
		$("#tddiscap" + trabajadorId)
				.html(
						"<input type='text' "
								+ "style='width:100px' "
								+ "id='inputDiscap' class='form-control' placeholder='Discapacidad' value='"
								+ name5 + "'>");
		var name6 = $("#tdrestr" + trabajadorId).text(); 
		var name7 = $("#tdedad" + trabajadorId).text();

		$("#tdfeing" + trabajadorId).html(
				"<input type='date' " + "style='width:150px' "
						+ "id='inputFecIng' class='form-control'>");

		var myDate = new Date(fechaIngreso);
		var day = ("0" + myDate.getUTCDate()).slice(-2);
		var month = ("0" + (myDate.getUTCMonth() + 1)).slice(-2);
		var today = myDate.getUTCFullYear() + "-" + (month) + "-" + (day);
		$("#inputFecIng").val(today);
		
		$("#tdferet" + trabajadorId).html(
				"<input type='date'" + " style='width:150px' "
						+ " id='inputFecRet' " + "style='width:100px' "
						+ "class='form-control'>");

		var myDate = new Date(fechaRetiro);
		var day = ("0" + myDate.getUTCDate()).slice(-2);
		var month = ("0" + (myDate.getUTCMonth() + 1)).slice(-2);
		var today = myDate.getUTCFullYear() + "-" + (month) + "-" + (day);
		$("#inputFecRet").val(today);
		
		
      $("#tdfecharec"+trabajadorId)
      .html("<input type='date' style='width:150px' id='inputFechaRec' " +
      	           "style='width:100px' class='form-control'>");
      
      if(listFullTrabajadores[pindex].fechaRecepcion != null){
         var myDate = new Date(listFullTrabajadores[pindex].fechaRecepcion);
         var day = ("0" + myDate.getUTCDate()).slice(-2);
         var month = ("0" + (myDate.getUTCMonth() + 1)).slice(-2);
         var today = myDate.getUTCFullYear() + "-" + (month) + "-" + (day);
         $("#inputFechaRec").val(today);
      }	
			
		if(parseInt(sessionStorage.getItem("accesoUsuarios")) == 0){
			$("#inputFecRet").prop("disabled",true);
		}
		$("#tdnac" + trabajadorId).html(
				"<input type='date' id='inputFecNac' " + "style='width:150px' "
						+ "onchange=' colocarEdad(" + trabajadorId + ")' "
						+ "class='form-control'>");

		var myDate = new Date(fechaNacimiento);
		var day = ("0" + myDate.getUTCDate()).slice(-2);
		var month = ("0" + (myDate.getUTCMonth() + 1)).slice(-2);
		var today = myDate.getUTCFullYear() + "-" + (month) + "-" + (day);
		$("#inputFecNac").val(today);

		if (fechaIngreso == null) {
			$("#inputFecIng").val("");
		}
		if (fechaRetiro == null) {
			$("#inputFecRet").val("");
		}
		if (fechaNacimiento == null) {
			$("#inputFecNac").val("");
		}
		/** ************************************************************************* */
		var selPosTipo = crearSelectOneMenu("selPosTipo", "", listPosition,
				positionId, "positionId", "positionNameShort");
		$("#tdpos" + trabajadorId).html(selPosTipo);

		var selTipoSang = crearSelectOneMenu("selTipoSang", "", listSangre,
				sangreTipoId, "sangreTipoId", "sangreTipoNombre");
		$("#tdsang" + trabajadorId).html(selTipoSang);

		var selGenero = crearSelectOneMenu("selGenero", "", listGenero,
				generoId, "generoId", "generoNombre");
		$("#tdgen" + trabajadorId).html(selGenero);

		var selTipoDisc = crearSelectOneMenu("selTipoDisc", "",
				listDiscapacidad, tipoDiscapacidad, "tipoDiscapacidad",
				"tipoDiscapacidadNombre");
		var selTipoDuracion = crearSelectOneMenu("selTipoDuracion", "",
				listDuracion, tipoDuracionTrab, "id", "nombre");
		$("#tddur" + trabajadorId).html(selTipoDuracion);

		// $("#tdtipod" + trabajadorId).html(selTipoDisc);

		banderaEdicion = true;
		habilitarBotonesEdicion();
		$("#btnVistaEventos").show();
	$("#btnGrupoTrabajador").hide();
			goheadfixed('#tblTrab');
			
			//INtertek
			var textCont=$("#tdcont"+trabajadorId).text();
			$("#tdcont"+trabajadorId).addClass("linkGosst")
			.on("click",function(){verContratosTrabajador();})
			.html("<i class='fa fa-list-ul fa-2x' aria-hidden='true' ></i>"+textCont);
			
			var textSueldo=$("#tdsueldo"+trabajadorId).text();
			$("#tdsueldo"+trabajadorId).addClass("linkGosst")
			.on("click",function(){verSueldosTrabajador();})
			.html("<i class='fa fa-list-ul fa-2x' aria-hidden='true' ></i>"+textSueldo);
			var textHijo=$("#tdhijo"+trabajadorId).text();
			$("#tdhijo"+trabajadorId).addClass("linkGosst")
			.on("click",function(){verHijosTrabajador();})
			.html("<i class='fa fa-list-ul fa-2x' aria-hidden='true' ></i>"+textHijo);
		
			var textCese=$("#tdcese"+trabajadorId).text();
			$("#tdcese"+trabajadorId).addClass("linkGosst")
			.on("click",function(){verCesesTrabajador();})
			.html("<i class='fa fa-list-ul fa-2x' aria-hidden='true' ></i>"+textCese);
		
			var textCharlaA=$("#tdcharlaA"+trabajadorId).text();
			$("#tdcharlaA"+trabajadorId).addClass("linkGosst")
			.on("click",function(){verCharlasTrabajador(1);})
			.html("<i class='fa fa-list-ul fa-2x' aria-hidden='true' ></i>"+textCharlaA);
			
			
			var textCharlaC=$("#tdcharlaC"+trabajadorId).text();
			$("#tdcharlaC"+trabajadorId).addClass("linkGosst")
			.on("click",function(){verCharlasTrabajador(3);})
			.html("<i class='fa fa-list-ul fa-2x' aria-hidden='true' ></i>"+textCharlaC);
			
			var textCharlaD=$("#tdcharlaD"+trabajadorId).text();
			$("#tdcharlaD"+trabajadorId).addClass("linkGosst")
			.on("click",function(){verCharlasTrabajador(4);})
			.html("<i class='fa fa-list-ul fa-2x' aria-hidden='true' ></i>"+textCharlaD);
			
			var textVac=$("#tdvac"+trabajadorId).text();
			$("#tdvac"+trabajadorId).addClass("linkGosst")
			.on("click",function(){verVacacionesTrabajador();})
			.html("<i class='fa fa-list-ul fa-2x' aria-hidden='true' ></i>"+textVac);

			var textMemo=$("#tdmemo"+trabajadorId).text();
			$("#tdmemo"+trabajadorId).addClass("linkGosst")
			.on("click",function(){verMemosTrabajador();})
			.html("<i class='fa fa-list-ul fa-2x' aria-hidden='true' ></i>"+textMemo);
			var textEstudio=$("#tdestud"+trabajadorId).text();
			$("#tdestud"+trabajadorId).addClass("linkGosst")
			.on("click",function(){verEstudiosTrabajador();})
			.html("<i class='fa fa-list-ul fa-2x' aria-hidden='true' ></i>"+textEstudio);
	}
}

function nuevoTrabajador() {
   if(!banderaEdicion){
      var selPuesto = crearSelectOneMenu("selPosTipo","",listPosition,
		         "-1","positionId","positionNameShort");

      var selTipoSang = crearSelectOneMenu("selTipoSang","",listSangre,
		           "-1","sangreTipoId","sangreTipoNombre");

      var selGenero = crearSelectOneMenu("selGenero","",listGenero,
			 "-1","generoId", "generoNombre");
		
      var selTipoDisc = crearSelectOneMenu("selTipoDisc","",listDiscapacidad,
	                   "-1","tipoDiscapacidad","tipoDiscapacidadNombre");
		
      var selTipoDuracion = crearSelectOneMenu("selTipoDuracion","",listDuracion,
	                       "-1","id","nombre");
      //
      var selTipoDoc = crearSelectOneMenuOblig("selTipoDoc","",listTipoDoc,
			  "","id","nombre");
		
      var selEstadoCivil = crearSelectOneMenuOblig("selEstadoCivil","",listEstadoCivil,
			      "","id","nombre");
		
      var selTipoAfp = crearSelectOneMenuOblig("selTipoAfp","",listTipoAfp,
		          "","id","nombre");
      
      var selTipoEps = crearSelectOneMenuOblig("selTipoEps","",listTipoEps,
			  "","id","nombre");
	
      var selTipoLocacion = crearSelectOneMenuOblig("selTipoLocacion","",listLocacion,
	                       "","id","nombre");
      //
      $("#tblTrab tbody:first")
      .prepend("<tr id='0'>"
								+ "<td>"
								+ "..."
								+ "</td>"
								+"<td>"+selTipoDoc+"</td>"
								+ "<td><input type='text' id='inputDni' onkeyup='verificarDni()' class='form-control' placeholder='' autofocus='true'></td>"
								
								+ "<td><input type='text' id='inputNom' class='form-control' placeholder='Nombres '></td>"
								+"<td>"+"<input class='form-control' id='inputApellidoP'  placeholder='Ap. Paterno'>"+"</td>"
								+"<td>"+"<input class='form-control' id='inputApellidoM'  placeholder='Ap. Materno'>"+"</td>"
								+"<td>...</td>"
								+"<td>...</td>"
								+ "<td>"
								+ selTipoDuracion
								+ "</td>"

								+ "<td><input type='date' id='inputFecIng' class='form-control'></td>"
								+"<td>...</td>"
								+"<td>"+selTipoLocacion+"</td>"
								+"<td>...</td>"
								+"<td>...</td>"
								+"<td>...</td>"
								
								+"<td>...</td>"
		+ "<td><input type='date' id='inputFecRet' class='form-control'></td>"
		+ "<td><input type='date' id='inputFechaRec' class='form-control'></td>"
								+"<td>...</td>"
								+ "<td>"
								+ selGenero
								+ "</td>"
								+ "<td>"
								+ "<input type='date' id='inputFecNac' class='form-control' onchange=' colocarEdad(0)'>"
								+ "</td>"
								+ "<td id='edadTrab0'></td>"
								+"<td>"+selEstadoCivil+"</td>"
								+ "<td>"
								+ selTipoSang
								+ "</td>"
								
								+"<td>...</td>"
								+"<td>...</td>"
								+"<td>...</td>"
								
								+"<td>"+selTipoAfp+"</td>"
								+"<td>"+selTipoEps+"</td>"
								+"<td>"+"<input type='text' id='inputConyugueNombre' class='form-control' placeholder='Conyugue' >"+"</td>"
								+"<td  id='tdinfo0'> Info</td>"
								+ "<td><input type='text' id='inputSuc' class='form-control' placeholder='Correo Coorporativo'></td>"
								+"<td>"+"<input type='text' id='inputCorreoPersonal' class='form-control' placeholder='Correo Personal'>"+"</td>"
								
								
								+ "<td>"
								+ "Pendiente"
								+ "</td>"
								+ "<td>---</td>"
								+ "<td><input type='text' id='inputDiscap' class='form-control' placeholder='Discapacidad'></td>"
								+"<td><input type='text' id='inputObs' " + "class='form-control' "
								+ "style='width:120px' " + "placeholder='Observación' "
								+ "></td>"
								+"<td>...</td>"
								+"<td>...</td>"
								+ "<td>---</td>"
								+ "</tr>");
		if(parseInt(sessionStorage.getItem("perfilIntertek"))==1){
			
			columnasOcultar.forEach(function(val){
				$('#tblTrab td:nth-child('+val+'),#tblTrab th:nth-child('+val+')').hide();
			});
			 
		}
		llevarTablaFondo("tblTrab:first");
		trabajadorId = 0;
		habilitarBotonesNuevo();
		$("#btnVistaEventos").hide();
		var textInfo=$("#tdinfo"+trabajadorId).text();
		$("#tdinfo"+trabajadorId).addClass("linkGosst")
		.on("click",function(){verInfoExtraTrabajador();})
		.html("<i class='fa fa-list-ul fa-2x' aria-hidden='true' ></i>"+textInfo);
		
		
		
		banderaEdicion = true;
	} else {
		alert("Guarde primero.");
	}
}

function cancelarTrabajador() {
	var indexAux=null;
	listFullTrabajadores.every(function(val,index){
		if(trabajadorId==val.trabajadorId){
			indexAux=index;
			return false;
		}else{
			return true;
		}
	});
	llamarCeldaTabla(indexAux);
}

function eliminarTrabajador() {
	var r = confirm("¿Está seguro de eliminar al trabajador?");
	if (r == true) {
		var dataParam = {
			trabajadorId : trabajadorId
		};

		callAjaxPost(URL + '/trabajador/delete', dataParam,
				procesarResultadoEliminarTrab);
	}
}

function procesarResultadoEliminarTrab(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		cargarPrimerEstado();
		break;
	default:
		alert("El trabajador tiene eventos asociados.");
	}
}

function guardarTrabajador() {

	var campoVacio = true;
	var observaciones=$("#inputObs").val();
	var selPosTipo = $("#selPosTipo option:selected").val();
	var inputDni = $("#inputDni").val();
	var inputNom = $("#inputNom").val();
	var selGenero = $("#selGenero").val();
	var selTipoSang = $("#selTipoSang option:selected").val();
	// var selTipoDisc = $("#selTipoDisc option:selected").val();
	var selTipoDuracion = $("#selTipoDuracion").val();
	var inputSuc = $("#inputSuc").val();
	var inputDiscap = $("#inputDiscap").val();
	// var inputRestr = $("#inputRestr").val();
	var inputEdad = parseInt($("#edadTrab" + trabajadorId).html());

	fechaIngreso = $("#inputFecIng").val();
	var mes1 = $("#inputFecIng").val().substring(5, 7) - 1;
	var fechatemp1 = new Date($("#inputFecIng").val().substring(0, 4), mes1, $(
			"#inputFecIng").val().substring(8, 10));
	var inputFecIng = fechatemp1;
	
	fechaRetiro = $("#inputFecRet").val()
	var mes2 = $("#inputFecRet").val().substring(5, 7) - 1;
	var fechatemp2 = new Date($("#inputFecRet").val().substring(0, 4), mes2, $(
			"#inputFecRet").val().substring(8, 10));
	var inputFecRet = fechatemp2;
	
	var fechaRecepcion = $("#inputFechaRec").val();
	var mes3 = $("#inputFechaRec").val().substring(5, 7) - 1;
	var fechatemp3 = new Date($("#inputFechaRec").val().substring(0, 4), mes3, $(
			       "#inputFechaRec").val().substring(8, 10));
	var inputFechaRec = fechatemp3;
	
	fechaNacimiento = $("#inputFecNac").val()
	var mes2 = $("#inputFecNac").val().substring(5, 7) - 1;
	var fechatemp2 = new Date($("#inputFecNac").val().substring(0, 4), mes2, $(
			"#inputFecNac").val().substring(8, 10));

	var inputFecNac = fechatemp2;

	if (!fechaIngreso) {
		inputFecIng = null
	}
	if (!fechaRetiro) {
		inputFecRet = null
	}
	
   if(!fechaRecepcion){
      inputFechaRec = null;
   }
	
	if (!fechaNacimiento) {
		inputFecNac = null
	}
	if (selPosTipo == '-1' || inputDni.length == 0 || inputNom.length == 0
			|| selGenero == '-1') {
		campoVacio = false;
	}

	if (parseInt(selTipoSang) == -1) {
		selTipoSang = null;

	}
	if (parseInt(selTipoDuracion) == -1) {
		selTipoDuracion = null;

	}
	// if(parseInt(selTipoDisc)==-1){selTipoDisc=null; }
	passwordTrabajador=$("#inputPass").val();
	if(trabajadorId==0){
		passwordTrabajador=inputDni
	}
	var docId=$("#selTipoDoc").val();
	var civilId=$("#selEstadoCivil").val();
	var afpId=$("#selTipoAfp").val();
	var epsId=$("#selTipoEps").val();
	
	//
	var apellidoP=$("#inputApellidoP").val();
	var apellidoM=$("#inputApellidoM").val();
	var locacion=$("#selTipoLocacion").val();
	var conyugue=$("#inputConyugueNombre").val();
	var correoPersonal=$("#inputCorreoPersonal").val();
	var telefonoCasa=$("#inputTelefonoCasa").val();
	var fonoPersonal=$("#inputTelefonoPersonal").val();
	var fonoOtro=$("#inputTelefonoOtro").val();
	var direccionCasa=$("#inputDireccionCasa").val();
	
	var provincia=$("#inputProvinciaCasa").val();
	var distrito=$("#inputDistritoCasa").val();
	var departamento=$("#selDepartamento").val();
	//
	if (campoVacio) {

		var dataParam = {
			trabajadorId : trabajadorId,
			departamento:{id:departamento,nombre:findValueArray(listDepa,"id","nombre",departamento) },
			distrito:distrito,
			provincia:provincia,
			direccion:direccionCasa,
			telefonoOtro:fonoOtro,
			telefonoPersonal:fonoPersonal,
			apellidoPaterno:apellidoP,
			apellidoMaterno:apellidoM,
			conyugue:conyugue,
			locacion:{id:locacion,nombre:findValueArray(listLocacion,"id","nombre",locacion) },
			dni : inputDni,
			doc:{id:docId,nombre:findValueArray(listTipoDoc,"id","nombre",docId) },
			estadoCivil:{id:civilId,nombre:findValueArray(listEstadoCivil,"id","nombre",civilId) },
			afp:{id:afpId,nombre:findValueArray(listTipoAfp,"id","nombre",afpId) },
			eps:{id:epsId,nombre:findValueArray(listTipoEps,"id","nombre",epsId) },
			contrasenia:passwordTrabajador,
			trabajadorNombre : inputNom,
			genero : {
				generoId : selGenero,
				generoNombre:findValueArray(listGenero,"generoId","generoNombre",selGenero) 
			},
			sangre : {
				sangreTipoId : selTipoSang,
				sangreTipoNombre:findValueArray(listSangre,"sangreTipoId","sangreTipoNombre",selTipoSang) 
			},
			correo : inputSuc,
			correoPersonal: correoPersonal,
			telefonoCasa:telefonoCasa,
			discapacidad : inputDiscap,
			edad : inputEdad,
			puesto : {
				positionId : selPosTipo,
				positionName:findValueArray(listPosition,"positionId","positionName",selPosTipo) 
			},
			mdfId : getUrlParameter("mdfId"),
			fechaIngreso : inputFecIng,
			fechaRetiro : inputFecRet,
			fechaRecepcion : inputFechaRec,
			fechaNacimiento : inputFecNac,
			fechaIngresoTexto : convertirFechaNormal(inputFecIng),
			fechaRetiroTexto : convertirFechaNormal(inputFecRet),
			fechaNacimientoTexto : convertirFechaNormal(inputFecNac),
			tipoDuracion : {
				id : selTipoDuracion,
				nombre:findValueArray(listDuracion,"id","nombre",selTipoDuracion) 
			},
			observacion:observaciones,
			tipoDiscapacidadNombre:$("#tdtipod" + trabajadorId).text(),
			restriccion:$("#tdrestr" + trabajadorId).text()
		}; 
		
		formatoCeldaSombreableTabla(true,"tblTrab");
		callAjaxPostNoUnlock(URL + '/trabajador/save', dataParam,
				procesarResultadoGuardarTrabajador,null,{},function(){
					
						
					cargarPrimerEstado();
				});
	} else {
		alert("Debe ingresar todos los campos.");
	}
}

function procesarResultadoGuardarTrabajador(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		if(data.trabajadorId==0){
			cargarPrimerEstado();
		}else{
			var trabajadorGuardado=data.trabajadorGuardado;
			var indexAux=null;
			listFullTrabajadores.every(function(val,index){
				if(trabajadorGuardado.trabajadorId==val.trabajadorId){
					listFullTrabajadores[index]=trabajadorGuardado;
					indexAux=index;
					return false;
				}else{
					return true;
				}
			});
			llamarCeldaTabla(indexAux);
		}
		 $.blockUI({
			 message:"<i class='fa fa-check-square' style='color:green' aria-hidden='true'></i> Guardado",
			 timeout:1000
		 });
		break;
	default:
		alert("Ya existe un trabajador con ese DNI");
	cargarPrimerEstado();
			break;
	}
}

function verVistaEventos() {
	$('#mdEvent').modal('show');
}



function cargarModalEvent() {
	var dataParam = {
		trabajadorId : trabajadorId
	};

	callAjaxPost(URL + '/trabajador/eventos', dataParam,
			procesarResultadoEventos);
}

function procesarResultadoEventos(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		var list = data.list;
		var listNegativa = data.listEventosNegativos;
		$("#tabCapa table tbody tr").remove();
		$("#tabExamen table tbody tr").remove();
		$("#tabEntrega table tbody tr").remove();
		$("#tabActos table tbody tr").remove();
		$("#tabSancion table tbody tr").remove();
		$("#tabParticipacion table tbody tr").remove();
		$("#tabGrupos table tbody tr").remove();
		for (index = 0; index < list.length; index++) {
			switch (list[index].eventoTipoId) {
			case 1:
				$("#tabCapa table tbody").append(

						"<tr id='tr" + index + "'>" + "<td>"
								+ list[index].eventoNombre + "</td>"

								+ "<td>" + list[index].estadoNombre + "</td>"

								+ "<td>" + list[index].fechaTexto + "</td>"

								+ "<td><a href='" + URL
								+ list[index].urlEvidencia
								+ "' target='_blank' >"
								+ list[index].nombreEvidencia + "</a></td>"

								+ "</tr>");
				break;
			case 2:
				$("#tabExamen table tbody").append(

						"<tr id='tr" + index + "'>" + "<td>"
								+ list[index].eventoNombre + "</td>"

								+ "<td>" + list[index].estadoNombre + "</td>"

								+ "<td>" + list[index].fechaTexto + "</td>"

								+ "<td><a href='" + URL
								+ list[index].urlEvidencia
								+ "' target='_blank' >"
								+ list[index].nombreEvidencia + "</a></td>"

								+ "</tr>");
				break;
			case 3:
				$("#tabEntrega table tbody").append(

						"<tr id='tr" + index + "'>" + "<td>"
								+ list[index].eventoNombre + "</td>"

								+ "<td>" + list[index].estadoNombre + "</td>"

								+ "<td>" + list[index].fechaTexto + "</td>"

								+ "<td><a href='" + URL
								+ list[index].urlEvidencia
								+ "' target='_blank' >"
								+ list[index].nombreEvidencia + "</a></td>"

								+ "</tr>");
				break;
			case 7:
				$("#tabParticipacion table tbody").append(

						"<tr id='tr" + index + "'>" + "<td>"
								+ list[index].eventoNombre + "</td>"

								+ "<td>" + list[index].estadoNombre + "</td>"

								+ "<td>" + list[index].fechaTexto + "</td>"

								+ "</tr>");
				break;
			case 8:
				$("#tabGrupos table tbody").append(

						"<tr id='tr" + index + "'>" 
								+ "<td>"+ list[index].eventoNombre + "</td>"
								+ "<td>" + list[index].fechaInicioTexto + "</td>"
								+ "<td>" + list[index].fechaFinTexto + "</td>"
								+ "<td>" + list[index].estadoNombre + "</td>"

								
								+ "<td><a href='" + URL
								+ list[index].urlEvidencia
								+ "' target='_blank' >"
								+ list[index].nombreEvidencia + "</a></td>"
								+ "</tr>");
				break;
			}

		}
		var hModalAux=$(window).height()*0.6;
		$("#tabCapa").css(
				{"height":hModalAux+"px",
				"overflow-x": "auto",
				"overflow-y": "auto"}	
				
		);
		for (index = 0; index < listNegativa.length; index++) {
			switch (listNegativa[index].eventoTipoId) {
			case 4:
				$("#tabActos table tbody").append(

						"<tr id='tr" + index + "'>" + "<td>"
								+ listNegativa[index].tipoNombre + "</td>"

								+ "<td>" + listNegativa[index].eventoNombre
								+ "</td>"

								+ "<td>" + listNegativa[index].fechaTexto
								+ "</td>" + "<td>"
								+ listNegativa[index].diasDescanso + "</td>"
								+ "<td><a href='" + URL
								+ listNegativa[index].urlEvidencia
								+ "' target='_blank' >"
								+ listNegativa[index].nombreEvidencia
								+ "</a></td>"

								+ "</tr>");
				break;
			case 5:
				$("#tabSancion table tbody").append(

						"<tr id='tr" + index + "'>" + "<td>"
								+ listNegativa[index].tipoNombre + "</td>"

								+ "<td>" + listNegativa[index].eventoNombre
								+ "</td>"

								+ "<td>" + listNegativa[index].fechaTexto
								+ "</td>"

								+ "<td><a href='" + URL
								+ listNegativa[index].urlEvidencia
								+ "' target='_blank' >"
								+ listNegativa[index].nombreEvidencia
								+ "</a></td>"

								+ "</tr>");
				break;
			case 6:
				$("#tabActos table tbody").append(

						"<tr id='tr" + index + "'>" + "<td>"
								+ listNegativa[index].tipoNombre + "</td>"

								+ "<td>" + listNegativa[index].eventoNombre
								+ "</td>"

								+ "<td>" + listNegativa[index].fechaTexto
								+ "</td>" + "<td>"
								+ listNegativa[index].diasDescanso + "</td>"
								+ "<td><a href='" + URL
								+ listNegativa[index].urlEvidencia
								+ "' target='_blank' >"
								+ listNegativa[index].nombreEvidencia
								+ "</a></td>"

								+ "</tr>");
				break;
			}

		}
		break;
	default:
		alert("Ocurrió un error al cargar los eventos!");
	}
}
function importarDatos() {
	$("#btnGuardarNuevos").attr("onclick",
			"javascript:guardarMasivoCopiaExcel();");

	$('#mdImpDatos').modal('show');
	var selPosTipo = crearSelectOneMenu("selPosTipoImp", "", listPosition,
			positionId, "positionId", "positionNameShort","Sin Definir");
	$("#divSelPuesto").html(selPosTipo);
}

function guardarMasivoCopiaExcel() {
	var texto = $("#txtListado").val();
	var listExcel = texto.split('\n');
	var listTrabs = [];
	var listDnisRepetidos = [];
	var listFullNombres = "";
	var validado = "";

	if (texto.length < 90000) {
		for (var int = 0; int < listExcel.length; int++) {

			var listCells = listExcel[int].split('\t');
			var filaTexto = listExcel[int];
			if (filaTexto.trim().length < 1) {
				validado = "Existen filas vacias.";
				break;
			}

			if (validado.length < 1 && listCells.length != 27) {
				validado = "No coincide el numero de celdas."+listCells[3]
						+ listCells.length;
				break;
			} else {
				var trab = {};
				trab.afp={id:null};
				trab.eps={id:null};
				trab.departamento={id:null};
				trab.locacion={id:null};
				trab.estadoCivil={id:null};
				trab.contactoPrimero={};
				var genero = {};
				for (var j = 0; j < listCells.length; j++) {
					var element = listCells[j];
					if (j === 0) {
						trab.dni = element.trim();
						if ($.inArray(trab.dni, listDnisRepetidos) === -1) {
							listDnisRepetidos.push(trab.dni);
						} else {
							listFullNombres = listFullNombres + trab.dni + ", ";
						}

					}
					//
					if (j === 1)
						trab.apellidoPaterno = element.trim();
					if (j === 2)
						trab.apellidoMaterno= element.trim();
					if (j === 3)
						trab.trabajadorNombre = element.trim();
					//
					if (j === 4){
						//NOP
					}
					//
					if (j === 5) {
						for (var k = 0; k < listGenero.length; k++) {
							if (listGenero[k].generoNombre.toUpperCase() == element.trim().toUpperCase()) {
								genero.generoId = listGenero[k].generoId;
								trab.genero = genero;
							}
						}

						if (!genero.generoId) {
							validado = "Genero no reconocido."+trab.trabajadorNombre;
							break;
						}
					}
					if (j === 6) {
						try {
							var fechaTexto = element.trim();
							var parts = fechaTexto.split('/');
							var dateNac = new Date(parts[2], parts[1] - 1,
									parts[0]);
							trab.fechaNacimiento = dateNac;
						} catch (err) {
							validado = "Error en el formato de fecha de nacimiento.";
							break;
						}
					}
					//
					if (j === 7) {
						listEstadoCivil.forEach(function(val,index){
							if(val.nombre.toUpperCase()== element.trim().toUpperCase()){
								trab.estadoCivil={id:val.id}
							}
						});
						

						if (!trab.estadoCivil.id) {
							validado = "Estado Civil no reconocido."+trab.trabajadorNombre;
							break;
						}
					}
					//
					if (j === 8) {
						try {
							var fechaTexto = element.trim();
							var parts = fechaTexto.split('/');
							var dateIngreso = new Date(parts[2], parts[1] - 1,
									parts[0]);
							trab.fechaIngreso = dateIngreso;
						} catch (err) {
							validado = "Error en el formato de fecha de ingreso.";
							break;
						}

					}
					
					//
					if (j === 9) {
					//NOP
					}
					if (j === 10) {
						//NOP
						}
					if (j === 11) {
						//NOP
						}
					//
					if (j === 12) {
						listLocacion.forEach(function(val,index){
							if(val.nombre== element.trim()){
								trab.locacion={id:val.id}
							}
						});
						

						if (!trab.locacion.id) {
							validado = "Locacion no reconocido.";
							break;
						}

					}
					//
					if (j === 13) {
						listTipoAfp.forEach(function(val,index){
							if(val.nombre== element.trim()){
								trab.afp={id:val.id}
							}
						});
						

						if (!trab.afp.id) {
							trab.afp={id:null}
						}
					}
					//
					if (j === 14) {
						listTipoEps.forEach(function(val,index){
							if(val.nombre== element.trim()){
								trab.eps={id:val.id}
							}
						});
						

						if (!trab.eps.id) {
							trab.eps={id:null}
						}
					}
					//
					if (j === 15) {
						//NOP
					}
					//
					if (j === 16) {
						element=element.trim().replace("S/.","");
						element=element.replace("S/","");
						trab.numSueldo = element.replace(",","");
					}
					//
					if (j === 17) {
						trab.telefonoCasa = element.trim();
					}
					//
					if (j === 18) {
						trab.telefonoPersonal = element.trim();
					}
					//Inicio contacto
					if (j === 19) {
						trab.contactoPrimero.nombre = element.trim();
					}
					//
					if (j === 20) {
						trab.contactoPrimero.telefonoCasa = element.trim();
					}
					//Fin contacto
					if (j === 21) {
						trab.contactoPrimero.telefonoCelular = element.trim();
					}
					//
					if (j === 22) {
						trab.direccion = element.trim();
					}
					//
					if (j === 23) {
						trab.distrito = element.trim();
					}
					//
					if (j === 24) {
						trab.provincia = element.trim();
					}
					//
					if (j === 25) {
						listDepa.forEach(function(val,index){
							if(val.nombre.toUpperCase()== element.trim().toUpperCase()){
								trab.departamento={id:val.id}
							}
						});
						

						if (!trab.departamento.id) {
							trab.departamento={id:null}
						}
					}
					//
					if (j === 26) {
						trab.correoPersonal = element.trim();
					}
					//
					
					

					trab.mdfId = getUrlParameter("mdfId");
					trab.puesto={
							positionId:($("#selPosTipoImp").val()=="-1"?null:$("#selPosTipoImp").val())
					}
				}
				trab.trabajadorId=0;
				listFullTrabajadores.every(function(val,index){
					if(val.dni==trab.dni){
						trab.trabajadorId=val.trabajadorId;
						return false;
					}else{
						return true;
					}
				});
				listTrabs.push(trab);
				if (listDnisRepetidos.length < listTrabs.length) {
					validado = "Existen dnis repetidos." + listFullNombres;
					break;
				}
			}
		}
	} else {
		validado = "Ha excedido los 40000 caracteres permitidos.";
	}

	if (validado.length < 1) {
		var r = confirm("¿Está seguro que desea subir estos "
				+ listExcel.length + " elemento(s)?");
		if (r == true) {
			var dataParam = {
				listTrabs : listTrabs
			};
			callAjaxPost(URL + '/trabajador/masivo/update', dataParam,
					funcionResultadoMasivo);
		}
	} else {
		alert(validado);
	}
}

function funcionResultadoMasivo(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		if (data.MENSAJE.length > 0) {
			alert(data.MENSAJE);
		} else {
			alert(data.MENSAJE);
			alert("Se guardaron exitosamente.");
			cargarPrimerEstado();
			$('#mdImpDatos').modal('hide');
		}
		break;
	default:
		alert("Ocurri&oacute; un error al guardar el trabajador!");
	}
}

function colorTrabajadorInactivo(fechaSalida) {
	var color;
	if (fechaSalida != null) {
		color = "#E6E6E6";
	} else {
		color = "";
	}

	return "style='background-color:" + color + ";'"

}

function colocarEdad(trabId) {

	var edad = calcularEdadAuto($("#inputFecNac").val());

	$("#edadTrab" + trabId).html(edad);
}

function obtenerListaNombresTrabajador() {

	new Clipboard('#btnSelect', {
		text : function(trigger) {
			return listTrabajadores
		}
	});
	alert("Se han guardado al clipboard " + contadorListTrab
			+ " trabajador(es)");

}
function obtenerListaTrabajadorActivo() {
	var clipoard="Nombre Puesto \t ";
	listTrabajadoresActivos.forEach(function(val,index){
		clipoard+=(val.id!=-1?val.nombre:"Sin Puesto")+"\t";
	});
	clipoard+="\n Activos \t"
	listTrabajadoresActivos.forEach(function(val1,index1){
		clipoard+=val1.activos+"\t";
		
	});
	clipoard+="\n Inactivos \t"
	listTrabajadoresActivos.forEach(function(val2,index2){
		clipoard+=val2.inactivos+"\t";
		
	});
	
	
	new Clipboard('#btnSelectPuesto', {
		text : function(trigger) {
			return clipoard
		}
	});
	alert("Se han guardado al clipboard el número de trabajador(es) activos e inactivos");

}
function obtenerTablaClipboard() {
	var listFullTablaTrabajador=obtenerDatosTablaEstandar("tblTrab"); 
	 
	copiarAlPortapapeles(listFullTablaTrabajador);
	alert("Se han guardado al clipboard la tabla de este módulo" );

}




function verRadialTrabajador(index) {

	$("#containerModal").html("");
	$("#modalRadial").modal("show");
	var indexAux=index;
	var indicadorFormacion = 0;
var indicadorEntregaEpp = 0;
var indicadorDisponibilidad = 1;
var sumaDiasDescanso=0;
var diasTrabajados=0;
var indicadorParticipacion = 0;
var indicadorExamenMedico = 0;
listFullIndicadoresTrabajadoresAux.forEach(function(val1,index1){
	if(val1.id==trabajadorId){
		 indicadorFormacion = listFullIndicadoresTrabajadoresAux[index1].indicadorFormacion;
		 indicadorEntregaEpp = listFullIndicadoresTrabajadoresAux[index1].indicadorEntregaEpp;
		 indicadorDisponibilidad = listFullIndicadoresTrabajadoresAux[index1].indicadorDisponibilidad;
		 sumaDiasDescanso=listFullIndicadoresTrabajadoresAux[index1].sumaDiasDescanso;
		 diasTrabajados=listFullIndicadoresTrabajadoresAux[index1].diasTrabajados;
		 indicadorParticipacion = listFullIndicadoresTrabajadoresAux[index1].indicadorParticipacion;
		 indicadorExamenMedico = listFullIndicadoresTrabajadoresAux[index1].indicadorExamenMedico;
	}
	
});


var indicadorMinimoFormacion = indicadoresSeguridad.indicadorMinimoFormacion;
var indicadorMinimoEntregaEpp = indicadoresSeguridad.indicadorMinimoEntregaEpp;
var indicadorMinimoDisponibilidad = indicadoresSeguridad.indicadorMinimoDisponibilidad;
var indicadorMinimoParticipacion = indicadoresSeguridad.indicadorMinimoParticipacion;
var indicadorMinimoExamenMedico = indicadoresSeguridad.indicadorMinimoExamenMedico;
	// /////////
	//BUscanod el trab id
	 
	
	var objTrab = {
		trabajadorId : trabajadorId,
		palabraEspacio:"<br>"
	}
	callAjaxPost(URL + '/trabajador/indicadores', objTrab,
			function(data){
		switch(data.CODE_RESPONSE){
		case "05":
			var formNecesarias=data.indFormacion;
			$("#tblFormInd tbody tr").remove();
			var primerDia = new Date();
			primerDia=convertirFechaNormal2(primerDia);
					var capacitacionesTrab=formNecesarias[0].caps;
					var indicadorRangoActual=1;
					for(var index22=0;index22<capacitacionesTrab.length;index22++){
						
						var programs=capacitacionesTrab[index22].programsCap;
						var textoFechaVigencia="----";
						var textoFechaPendiente="";
						var vecesAprobadas="-----";
						var horaProgramada="-----";
						var numAprobadas=0;
						var progAnteriores=0;
						var colorFondo="white";
						for(var index3=0;index3<programs.length;index3++){
							if(programs[index3].fechaPlaTexto==null){
								programs[index3].fechaPlaTexto=primerDia;
							}
							if(programs[index3].fechaRealTexto==null){
								programs[index3].fechaRealTexto=programs[index3].fechaPlaTexto;
							}
							var enRangoInicial=fechaEnRango(primerDia,programs[index3].fechaPlaTexto,programs[index3].fechaRealTexto)
							if(restaFechas(primerDia,programs[index3].fechaPlaTexto)<0  ){
								progAnteriores=progAnteriores+1;
								textoFechaVigencia=convertirFechaNormal(programs[index3].fechaRealTexto);
							}
							if(restaFechas(primerDia,programs[index3].fechaPlaTexto)<0 &&programs[index3].tipoNotaTrabId==1 ){
								numAprobadas=numAprobadas+1;
							}
							if(index3+1==programs.length){
								vecesAprobadas=numAprobadas+"/"+progAnteriores
							}
							if(restaFechas(primerDia,programs[index3].fechaPlaTexto)>0){
								textoFechaPendiente=textoFechaPendiente+convertirFechaNormal(programs[index3].fechaPlaTexto)+" ,";
								horaProgramada=programs[index3].horaPlanificada;
							}
							
							if(programs[index3].tipoNotaTrabId==1 &&enRangoInicial ){
							colorFondo="#9BBB59";
							textoFechaVigencia=convertirFechaNormal(programs[index3].fechaRealTexto);
							horaProgramada="-----";
							};
						};
						$("#tblFormInd tbody").append(
								"<tr  " +
								"id='f"+capacitacionesTrab[index22].capacitacionId+"' '>" +
								"<td>"+capacitacionesTrab[index22].capacitacionNombre+"</td>" +
								"<td>"+textoFechaVigencia+"</td>" +
								"<td>"+textoFechaPendiente+"</td>" +
								"<td>"+vecesAprobadas+"</td>" +
								
								"</tr>"		
								);
						$("#tblFormInd tbody #f"+capacitacionesTrab[index22].capacitacionId+" td").css(
								{"background-color":colorFondo}
									
								);	
						
					}
			var examVigencia=data.indExam;
			$("#tblExamSum tbody tr").remove();
			
					var primerDia = new Date();
					primerDia=convertirFechaNormal2(primerDia);
					var examenTrab=examVigencia[0].exams;
					var indicadorRangoActual=0;
					for(var index22=0;index22<examenTrab.length;index22++){
						
						var programs=examenTrab[index22].programsExams;
						var textoFechaVigencia="----";
						var textoFechaPendiente="";
						var vecesAprobadas="-----";
						var colorFondo="white";
						for(var index3=0;index3<programs.length;index3++){
							if(programs[index3].fechaInicialTexto==null){
								programs[index3].fechaInicialTexto=primerDia;
							}
							if(programs[index3].fechaFinTexto==null){
								programs[index3].fechaFinTexto=programs[index3].fechaInicialTexto;
							}
							var enRangoInicial=fechaEnRango(primerDia,programs[index3].fechaInicialTexto,programs[index3].fechaFinTexto)
							if(restaFechas(primerDia,programs[index3].fechaInicialTexto)<0  ){
								
								textoFechaVigencia=convertirFechaNormal(programs[index3].fechaFinTexto);
							}
							
							
							if(restaFechas(primerDia,programs[index3].fechaInicialTexto)>0){
								textoFechaPendiente=textoFechaPendiente+convertirFechaNormal(programs[index3].fechaInicialTexto)+" ,"
							}
							
							if((programs[index3].tipoNotaTrabId==1 && enRangoInicial) || programs[index3].progTrabId >0){
								
							colorFondo="#9BBB59";
							textoFechaVigencia=convertirFechaNormal(programs[index3].fechaFinTexto);
							
							}else{
								textoFechaVigencia="";
							};
							var programaTipExId=programs[index3].programaTipExId;
							var eviNombre=programs[index3].evidenciaNombre;
							var downloadEvi="<a style=' margin-left: 10px;' target='_blank' href='"+URL
							+ "/examenmedico/programacion/evidencia?programaTipExId="
							+ programaTipExId+"''><i class='fa  fa-download'></i></a>";
							if(eviNombre.length==0){
								downloadEvi="";
							}
							$("#tblExamSum tbody").append(
									"<tr  " +
									"id='ex"+examenTrab[index22].examenMedicoId+"' '>" +
									"<td>"+examenTrab[index22].examenMedicoNombre+"</td>" +
									"<td>"+programs[index3].centro.nombre+"</td>" +
									"<td>"+programs[index3].vigenciaNombre+"</td>" +
									"<td>"+programs[index3].resultadoNombre+"</td>" +
									
									"<td>"+textoFechaVigencia+"</td>" +
									"<td>"+textoFechaPendiente+"</td>" +
									"<td>"+eviNombre+downloadEvi+"</td>" +
									"</tr>"		
									);
							colorFondo="white"
							$("#tblExamSum tbody #ex"+examenTrab[index22].examenMedicoId+" td").css(
									{"background-color":colorFondo}
										
									);	
							
							
						};
						
						
					}
			
			$("#tblEppSum tbody tr").remove();
			var entregaEpps=data.indEpp;
					var primerDia = new Date();
					primerDia=convertirFechaNormal2(primerDia);
					var eppsTrab=entregaEpps[0].epps;
					var indicadorRangoActual=1;
					for(var index22=0;index22<eppsTrab.length;index22++){
						
						var programs=eppsTrab[index22].programsEpp;
						var textoFechaVigencia="----";
						var textoFechaPendiente="";
						var vecesAprobadas="----";
						var colorFondo="white";
						for(var index3=0;index3<programs.length;index3++){
							if(programs[index3].fechaPlanificadaTexto==null){
								programs[index3].fechaPlanificadaTexto=primerDia;
							}
							if(programs[index3].fechaPlanificadaTexto==null){
								programs[index3].fechaPlanificadaTexto=programs[index3].fechaPlanificadaTexto;
							}
							var enRangoInicial=fechaEnRango(primerDia,programs[index3].fechaPlanificadaTexto,programs[index3].fechaEntregaTexto)
							if(restaFechas(primerDia,programs[index3].fechaPlanificadaTexto)<0  ){
								progAnteriores=progAnteriores+1;
								textoFechaVigencia=convertirFechaNormal(programs[index3].fechaEntregaTexto);
							}
							
							if(index3+1==programs.length){
								vecesAprobadas=numAprobadas+"/"+progAnteriores
							}
							if(restaFechas(primerDia,programs[index3].fechaPlanificadaTexto)>0){
								textoFechaPendiente=textoFechaPendiente+convertirFechaNormal(programs[index3].fechaPlanificadaTexto)+" ,"
							}
							
							if(programs[index3].tipoNotaTrabId!=null &&enRangoInicial ){
							colorFondo="#9BBB59";
							textoFechaVigencia=convertirFechaNormal(programs[index3].fechaEntregaTexto);
							};
						};
						$("#tblEppSum tbody").append(
								"<tr  " +
								"id='f"+eppsTrab[index22].equipoSeguridadId+"' '>" +
								"<td>"+eppsTrab[index22].equipoSeguridadNombre+"</td>" +
								"<td>"+programs.length+"</td>" +
								"<td>"+textoFechaVigencia+"</td>" +
								"<td>"+textoFechaPendiente+"</td>" +
								
								"</tr>"		
								);
						$("#tblEppSum tbody #f"+eppsTrab[index22].equipoSeguridadId+" td").css(
								{"background-color":colorFondo}
									
								);	
						
					}
					
				

			
			
			
			
			///////
			$("#tblPartSum tbody").html(
			"<tr>" +
			"<td>"+indicadorParticipacion+"</td>" +
					"<td>"+indicadoresSeguridad.indicadorMinimoParticipacion+"</td>" +
			"</tr>"		
			);
			var diastrabajados=diasTrabajados;
			var diasdescanso=sumaDiasDescanso;
			$("#tblDispSum tbody").html(
					"<tr>" +
					"<td>"+diasdescanso+"</td>" +
					"<td>"+diastrabajados+"</td>" +
					"</tr>"		
					);
			break;
			default:
				alert("error al traer indicadores de trabajador");
			break;
		}
		
	});
	// /////////
	$("#modalRadial #mdEventLabel").html(
			"Promedio no ponderado de Metas SST de "
					+ listFullTrabajadores[index].trabajadorNombre

	);
	
	$('#containerModal')
			.highcharts(
					{

						chart : {
							polar : true,
							type : 'area'
						},

						title : {
							text : '',
							x : -80
						},

						pane : {
							size : '80%'
						},

						xAxis : {
							categories : [ 'Vigencia Promedio de Formacion',
									'Vigencia Promedio de EPP',
									'Disponibilidad Laboral',
									'Participacion en el SGSST',
									'Vigencia Examen Médico' ],
							tickmarkPlacement : 'on',
							lineWidth : 0
						},

						yAxis : {
							gridLineInterpolation : 'polygon',
							lineWidth : 0,
							min : 0,
							tickInterval : 0.25,
							max : 1
						},

						tooltip : {
							shared : true,
							pointFormat : '<span style="color:{series.color}">{series.name}: <b>{point.y}</b><br/>',
							valueDecimals : 2
						},

						legend : {
							align : 'right',
							enabled : false,
							verticalAlign : 'top',
							y : 70,
							layout : 'vertical'
						},
					    colors:["#9BBB59","#4F81BD"],

						series : [
								{  zIndex:3,
							    	  fillColor: {
					                      linearGradient: {
					                          x1: 0,
					                          y1: 0,
					                          x2: 0,
					                          y2: 1
					                      },
					                      stops: [
					                          [0, "#9BBB59"],
					                          [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
					                      ]
					                  },
									name : 'Promedio Actual',
									data : [
											indicadorFormacion,
											indicadorEntregaEpp,
											indicadorDisponibilidad,
											 ((indicadorParticipacion/indicadorMinimoParticipacion)>1?1:(indicadorParticipacion/indicadorMinimoParticipacion)),
											indicadorExamenMedico ],
									pointPlacement : 'on'
								},
								{
									name : 'Meta',
									 fillColor: {
						                    linearGradient: {
						                        x1: 0,
						                        y1: 0,
						                        x2: 0,
						                        y2: 0.05
						                    },
						                    stops: [
						                        [0, "#4F81BD"],
						                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(-0.9).get('rgba')]
						                    ]
						                },
									data : [ indicadorMinimoFormacion,
											indicadorMinimoEntregaEpp,
											indicadorMinimoDisponibilidad, 1,
											indicadorMinimoExamenMedico ],
									pointPlacement : 'on'
								} ]

					});
/////////////
	var hModalAux=$(window).height()*0.6;
	$("#tabFormSum").css(
			{"height":hModalAux+"px",
			"overflow-x": "auto",
			"overflow-y": "auto"}	
			
	);
	$("#tabEntregaSum").css(
			{"height":hModalAux+"px",
			"overflow-x": "auto",
			"overflow-y": "auto"}	
			
	);
	$("#tabExamSum").css(
			{"height":hModalAux+"px",
			"overflow-x": "auto",
			"overflow-y": "auto"}	
			
	);
	$("#tabFormSum").css(
			{"height":hModalAux+"px",
			"overflow-x": "auto",
			"overflow-y": "auto"}	
			
	);
	////////////
}

function obtenerAyudaIndicador() {
	$("#modalAyudaIndicador").modal("show");
	escribirFraccion("ayudaForm", "Vigencia Promedio de Formación",
			"# de formaciones APROBADAS, vigentes y asignadas",
			"# total de formaciones asignadas");
	escribirFraccion("ayudaEpp", "Vigencia Promedio de EPP",
			"EPP's entregados, asignados y vigentes", "EPP´s asignados");
	$("#ayudaExam").html(
			"Vigencia de Examen Médico= (1) Si está apto o apto con restricciones,"
					+ " (0) si está pendiente, no apto o sin realizar");
	escribirFraccion("ayudaDisp", "Disponibilidad Laboral",
			"Días trabajados - Días de descanso médico ", "Días trabajados");
	$("#ayudaDisp").append(";Días trabajados=52*5");
	$("#ayudaPart")
			.html(
					"Participación en el SGSST =#Reportes de A/C subestándar en el año");

}

function verModalRetiro(){
$("#modalUpdateTrabajador").modal("show");
$("#fechaRetiroMasivo").val(obtenerFechaActual());
var filaTrabajador=$("#trabajadoresList tbody");

$("#trabajadoresList").parent(".wrapper").css({"height":$(window).height()-300+"px"}); 
$("#btnGuardarRetiros").attr("onclick","retiroMasivoTrabajadores()");
filaTrabajador.html("");
	listFullTrabajadores.forEach(function(val,index){
		if(val.fechaRetiro==null){
			
			filaTrabajador.append("<tr id='filaRetiro"+val.trabajadorId+"'>" +
					"<td><input class='form-control' type='checkbox' id='checkRetiro"+val.trabajadorId+"'></td>" +
					"<td>"+val.trabajadorNombre+"</td>" +
					"</tr>")
		}
		
		
	});
	
}
function filtrarModalRetiro(){
	var trabBuscar=$("#buscadorRetiroInput").val();
	trabBuscar=trabBuscar.toUpperCase();
	var inicioBusqueda=true;
	if(trabBuscar.length==0){
		inicioBusqueda=false;
	}
	listFullTrabajadores.forEach(function(val,index){
		if(val.fechaRetiro==null){
			if(inicioBusqueda){
				var trabNombre=val.trabajadorNombre.toUpperCase();
				if(trabNombre.indexOf(trabBuscar)!=-1){
					$("#filaRetiro"+val.trabajadorId).show();
				}else{
					$("#filaRetiro"+val.trabajadorId).hide();
				}
			}else{
				$("#filaRetiro"+val.trabajadorId).show();
			}
		}
		
		
	});
	
	
	
	
	
}
function retiroMasivoTrabajadores(){
	var listTrabs=[];
	var fechaRetiroAux = $("#fechaRetiroMasivo").val()
	var mes2 = $("#fechaRetiroMasivo").val().substring(5, 7) - 1;
	var fechatemp2 = new Date($("#fechaRetiroMasivo").val().substring(0, 4), mes2, $(
			"#fechaRetiroMasivo").val().substring(8, 10));

	var inputFecRet = fechatemp2;
	listFullTrabajadores.forEach(function(val,index){
		if($("#checkRetiro"+val.trabajadorId).prop("checked")){ 
			
			var trabajadorObj={	};
			trabajadorObj=val;
			trabajadorObj.fechaRetiro=inputFecRet;
			val.fechaRetiroTexto=fechaRetiroAux;
			val.fechaRetiro=inputFecRet;
			listTrabs.push(trabajadorObj)
		}
		
		
	});
	var dataParam = {
			listTrabs : listTrabs
		};
		callAjaxPostNoUnlock(URL + '/trabajador/masivo/update', dataParam,
				funcionResultadoUpdateMasivo,function(){
			listTrabs.forEach(function(val,index){
				listFullTrabajadores.forEach(function(val1,index1){
					if(val.trabajadorId==val1.trabajadorId){
						llamarCeldaTabla(index1);
					}
				})
				
					$("#tblTrab tbody #tr"+val.trabajadorId+" td").css(
							{
								"background-color":"#E6E6E6"
							}
								
							);	
				
				
			})
			$.blockUI({message:'<img src="../imagenes/gif/ruedita.gif"></img>Guardando Trabajadores...'});
		},dataParam);
}

function funcionResultadoUpdateMasivo(data){
	if(data.CODE_RESPONSE="05"){
		$("#modalUpdateTrabajador").modal("hide");
		$.blockUI({
			timeout:   1500, 
			message : '<i class="fa fa-check-square" style="color:green" aria-hidden="true"></i> Guardado '
		});
		
	};
	
}

function obtenerRestoTablaTrabajador(trabObject){
    
   var restoFila = "" +
          "<td id='tddoc"+trabObject.trabajadorId+"'>"+trabObject.doc.nombre+"</td>" +
	  "<td id='tddni"+trabObject.trabajadorId+"'>"+trabObject.dni+"</td>" +
	  "<td id='tdnom"+trabObject.trabajadorId+"'>"+trabObject.trabajadorNombre+"</td>" +
          "<td id='tdapP"+trabObject.trabajadorId+"'>"+trabObject.apellidoPaterno+"</td>" +
	  "<td id='tdapM"+trabObject.trabajadorId+"'>"+trabObject.apellidoMaterno+"</td>" +
	  "<td id='tdcont"+trabObject.trabajadorId+"'>"+trabObject.numContratos+"</td>" +
	  "<td id='tdptoact"+trabObject.trabajadorId+"'>"+trabObject.puestoActual+"</td>" +
	  "<td id='tddur"+trabObject.trabajadorId+"'>"+trabObject.tipoDuracion.nombre+"</td>" +
	  "<td id='tdfeing"+trabObject.trabajadorId+"'>"+trabObject.fechaIngresoTexto+"</td>" +
	  "<td id='tdperm"+trabObject.trabajadorId+"'>"+hallarPermanencia(trabObject.fechaIngreso)+"</td>" +
	  "<td id='tdloca"+trabObject.trabajadorId+"'>"+trabObject.locacion.nombre+"</td>" +
	  "<td id='tdsueldo"+trabObject.trabajadorId+"'>"+trabObject.numSueldo+"</td>" +
	  "<td id='tdhijo"+trabObject.trabajadorId+"'>"+trabObject.numHijos+"</td>" +
	  "<td id='tdbono"+trabObject.trabajadorId+"'>"+(trabObject.numHijosMenores>0?"Sí":"No")+"</td>" +
	  "<td id='tdcese"+trabObject.trabajadorId+"'>"+trabObject.numCeses+"</td>" +
	  "<td id='tdferet"+trabObject.trabajadorId+"'>"+trabObject.fechaRetiroTexto+"</td>" +
	  "<td id='tdfecharec"+trabObject.trabajadorId+"'>"+trabObject.fechaRecepcionTexto+"</td>" +
          "<td id='tdvac"+trabObject.trabajadorId+"'>"+trabObject.numVacaciones+"</td>" +
	  "<td id='tdgen"+trabObject.trabajadorId+"'>"+trabObject.genero.generoNombre+"</td>" +
	  "<td id='tdnac"+trabObject.trabajadorId+"'>"+trabObject.fechaNacimientoTexto+"</td>" +
	  "<td id='edadTrab"+trabObject.trabajadorId+"'>"+trabObject.edad+"</td>" +
	  "<td id='tdcivil"+trabObject.trabajadorId+"'>"+trabObject.estadoCivil.nombre+"</td>" +
	  "<td id='tdsang"+trabObject.trabajadorId+"'>"+trabObject.sangre.sangreTipoNombre+"</td>" + 
	  "<td id='tdcharlaA"+trabObject.trabajadorId+"'>"+trabObject.numCharlaA+"</td>" +
	  "<td id='tdcharlaC"+trabObject.trabajadorId+"'>"+trabObject.numCharlaC+"</td>" +
	  "<td id='tdcharlaD"+trabObject.trabajadorId+"'>"+trabObject.numCharlaD+"</td>" +
	  "<td id='tdafp"+trabObject.trabajadorId+"'>"+trabObject.afp.nombre+"</td>" +
	  "<td id='tdeps"+trabObject.trabajadorId+"'>"+trabObject.eps.nombre+"</td>" +
	  "<td id='tdcony"+trabObject.trabajadorId+"'>"+trabObject.conyugue+"</td>" +
	  "<td id='tdinfo"+trabObject.trabajadorId+"'>"+trabObject.telefonoCasa+"</td>" +
	  "<td id='tdsuc"+trabObject.trabajadorId+"'>"+trabObject.correo+"</td>" +
	  "<td id='tdmailp"+trabObject.trabajadorId+"'>"+trabObject.correoPersonal+"</td>" +
	  "<td id='tdtipod"+trabObject.trabajadorId+"'>"+trabObject.tipoDiscapacidadNombre+"</td>" + 
	  "<td id='tdrestr"+trabObject.trabajadorId+"'>"+trabObject.restriccion+"</td>" +
	  "<td id='tddiscap"+trabObject.trabajadorId+"'>"+trabObject.discapacidad+"</td>" +
	  "<td id='tdobs"+trabObject.trabajadorId+"'>"+trabObject.observacion+"</td>" +
	  "<td id='tdestud"+trabObject.trabajadorId+"'>"+trabObject.numEstudios+"</td>" +
	  "<td id='tdmemo"+trabObject.trabajadorId+"'>"+trabObject.numMemos+"</td>" +
	  "<td id='tdpass"+trabObject.trabajadorId+"'>"+"**********"+"</td>";
	
   return restoFila;
}
