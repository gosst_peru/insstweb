var reunionId;
var fechaEntrega;
var banderaEdicion3;
var listTrabajadoresReunion;
var listTrabsReuSeleccionados;
var listEquipos;
var listTiposReunion;
var listEstados;
var listaFullReuniones;
var reunion;
var reunionNombre;
var numPaginaActual;
var numPaginaTotal;


$(function(){
	$("#btnClipTablaReunion").on("click",function(){
		var listFullTablaProgramacion=""; 
		listFullTablaProgramacion=	obtenerDatosTablaEstandarNoFija("tblReu");
		copiarAlPortapapeles(listFullTablaProgramacion,"btnClipTablaReunion");
		
		alert("Se han guardado al clipboard la tabla de reuniones" );

	});
	
	
	
	$("#btnGuardarListTrabs").on("click",function(){
		cancelarListTrabs();
		guardarReunionSeguridad();
	});
})

function programarReunion() {
	$("#mdProgFecha .modal-body").hide();
	$("#mdProgFecha .modal-title").html("<a onclick='volverGruposTrabajo()'>Grupo</a> > Reuniones");
	$("#modalReunion").show(); 
	cargarModalReunionSeguridad();
}





function cambiarPaginaTablaReunion(){
	
	//Definiendo variables
	
	var list = listaFullReuniones;
	var totalSize=list.length;
	
	var partialSize=10;
	numPaginaTotal=Math.ceil(totalSize/partialSize);
	var inicioRegistro=(numPaginaActual*partialSize)-partialSize;
	var finRegistro=(numPaginaActual*partialSize)-1;
	
	//aplicando logica de compaginacion
	
	$('.izquierda_flecha').show();
	 $('.derecha_flecha').show();
	 if(numPaginaTotal==0){
			numPaginaTotal=1;
		}
		$("#labelFlechas").html(numPaginaActual+" de "+numPaginaTotal);
	 if(numPaginaActual==1){
			$('.izquierda_flecha').hide();
	 }
	
	 if(numPaginaActual==numPaginaTotal){
		 $('.derecha_flecha').hide();
	 }
	 
	if(finRegistro>totalSize){
		finRegistro=totalSize-1;
	}
	
$("#tblReu tbody tr").remove();
	
	$("#tblReu tbody tr").css("font-size","11px");
	$("#tblReu thead tr").css("font-size","11px");
}

function cancelarListTrabs(){
	$("#mdProgFecha .modal-body").hide();
	$("#mdProgFecha .modal-title").html("<a onclick='volverGruposTrabajo()'>Grupo</a> > Reuniones");
	$("#modalReunion").show(); 
}
function cargarModalReunionSeguridad() {
	
	$("#btnAgregarReu").attr("onclick", "javascript:nuevaReunionSeguridad();");
	$("#btnCancelarReu").attr("onclick", "javascript:cancelarReunionSeguridad();");
	$("#btnGuardarReu").attr("onclick", "javascript:guardarReunionSeguridad();");
	$("#btnEliminarReu").attr("onclick", "javascript:eliminarReunionSeguridad();");
	$("#btnGenReporte").attr("onclick","javascript:generarHojaRuta();");
	

	$("#btnCancelarReu").hide();
	$("#btnAgregarReu").show();
	$("#btnEliminarReu").hide();
	$("#btnGuardarReu").hide();
	$("#btnGenReporte").hide();
	
	$("#mdReuniones .modal-body").hide();
	$("#modalReunion").show();
	reunionId = 0;
	numPaginaActual=1;
	listTrabsReuSeleccionados = null;
	banderaEdicion3 = false;
	listaFullReuniones=[];
	var dataParam = {
			grupoSeguridadId : grupoSeguridadId
	};

	callAjaxPost(URL + '/trabajador/reuniones', dataParam, function(data) {
		switch (data.CODE_RESPONSE) {
		case "05":
			listTiposReunion = data.listTiposReunion;
			var list=data.list;
			listaFullReuniones=data.list;
			$("#tblReu tbody tr").remove();
			for (var index = 0; index < list.length; index++) {

				
				$("#tblReu tbody").append(
						"<tr id='tr" + list[index].id
								+ "' onclick='javascript:editarReunionSeguridad("
								+index+")' >"
								+ "<td id='tdtiporeu"
								+ list[index].id + "'>"
								+ list[index].tipo.nombre + "</td>"
								+ "<td id='tdnumage"
								+ list[index].id + "'>"
								+ list[index].numAgenda + "</td>"
								+ "<td id='tdnumdes"
								+ list[index].id + "'>"
								+ list[index].numDesarrollo + "</td>"
								+ "<td id='tdfechapla"
								+ list[index].id + "'>"
								+ list[index].fechaPlanificadaTexto + "</td>"
								+ "<td id='tdhorapla"
								+ list[index].id + "'>"
								+ list[index].horaPlanificadaTexto + "</td>"
								+ "<td id='tdfechareal"
								+ list[index].id + "'> "
								+ list[index].fechaRealizadaTexto + "</td>"
								+ "<td id='tdtrabreua" + list[index].id
								+ "'>"+list[index].numTrabajadoresAsociados+"</td>"
								+ "<td id='tdacus" + list[index].id
								+ "'>"+list[index].numAcuerdosAsociados+"</td>"
								+ "<td id='tdobsreu" + list[index].id
								+ "'>"+list[index].observacion+"</td>"
								+ "<td id='tdevireu" + list[index].id
								+ "'>"+list[index].evidenciaNombre+"</td>"
								
								+ "<td id='tdestadoreu" + list[index].id
								+ "'>"+list[index].estado.estadoCumplientoNombre+"</td>"
								
								
								+"</tr>");
			
		}
			formatoCeldaSombreableTabla(true,"tblReu");
if(getSession("linkCalendarioReunionId")!=null){
				
	listaFullReuniones.every(function(val,index3){
					
					if(val.id==parseInt(getSession("linkCalendarioReunionId"))){
						
						editarReunionSeguridad(index3);
						if(getSession("linkCalendarioAcuerdoId")!=null){
							cargarAcuerdosReunion();
						}
						sessionStorage.removeItem("linkCalendarioReunionId");
						return false;
					}else{
						return true;
					}
				});
				
			}
			break;
		default:
			alert("Ocurrió un error al traer las reuniones!");
		}
	});
}

function editarReunionSeguridad (pindex) {

	if (!banderaEdicion3) {
		listTrabajadoresReunion = [];
		listTrabsReuSeleccionados = [];
		reunionId = listaFullReuniones[pindex].id;
		var fechaPlanificada = listaFullReuniones[pindex].fechaPlanificada;
		var fechaRealizada = listaFullReuniones[pindex].fechaRealizada;

		var numTrabajadores=listaFullReuniones[pindex].numTrabajadoresAsociados;
		var numAcuerdos=listaFullReuniones[pindex].numAcuerdosAsociados;
		
		var tipoId=listaFullReuniones[pindex].tipo.id;
		var observaciones=listaFullReuniones[pindex].observacion;
		var horaPlanificada=listaFullReuniones[pindex].horaPlanificada;

		var textAgenda=$("#tdnumage"+reunionId).text();
		var textDesarrollo=$("#tdnumdes"+reunionId).text();
		formatoCeldaSombreableTabla(false,"tblReu");
			var selMotivo = crearSelectOneMenu("selTipoReu", "",
					listTiposReunion, tipoId, "id",
					"nombre");
		
		$("#tdtiporeu"+reunionId).html(selMotivo);
		
		$("#tdobsreu" + reunionId).html(
		"<input type='text' id='inputObsReu' class='form-control'>");
		
		
	$("#tdtrabreua" + reunionId)
				.html(
						"<a id='linkTrabEntr' " +
						"href='#' " +
						"onclick='javascript:cargarTrabajadoresReunion();'>"+$("#tdtrabreua" + reunionId).text()+"</a><span><i class='fa fa-user' aria-hidden='true'></i></span>");
	$("#tdacus" + reunionId)
	.html(
			"<a id='linkAcus' " +
			"href='#' " +
			"onclick='javascript:cargarAcuerdosReunion();'>"+$("#tdacus" + reunionId).text()+"</a><span><i class='fa fa-user' aria-hidden='true'></i></span>");


		
		$("#tdfechapla" + reunionId).html(
				"<input type='date' id='inputPlan' class='form-control' onchange='hallarEstadoReunion("+reunionId+")'>");
		$("#tdfechareal" + reunionId).html(
				"<input type='date' id='inputReal' class='form-control' onchange='hallarEstadoReunion("+reunionId+")'>");
		
		$("#tdhorapla" + reunionId).html(
				"<input type='time' id='inputHoraPla' " +
				" class='form-control'>");
		$("#tdnumage"+reunionId).addClass("linkGosst")
		.on("click",function(){verAgendaReunion();})
		.html("<i class='fa fa-bookmark fa-2x' aria-hidden='true' ></i>"+textAgenda);
		
		$("#tdnumdes"+reunionId).addClass("linkGosst")
		.on("click",function(){verDesarrolloReunion();})
		.html("<i class='fa fa-list fa-2x' aria-hidden='true' ></i>"+textDesarrollo);
		$("#inputObsReu").val(observaciones);
		$("#inputPlan").val(convertirFechaInput(fechaPlanificada));
		$("#inputReal").val(convertirFechaInput(fechaRealizada));
		$("#inputHoraPla").val(horaPlanificada);
		
		var nombreEvidencia=$("#tdevireu" + reunionId).text();
		$("#tdevireu" + reunionId)
		.html(
				"<a href='"
						+ URL
						+ "/trabajador/reunion/evidencia?reunionId="
						+ reunionId
						+ "' "
						+ "target='_blank'>" +
						nombreEvidencia+	"</a>"
						+ "<br/><a href='#' onclick='javascript:subirEvidencia("
						+ reunionId + ",1)'  id='subirimagen"
						+ reunionId + "'>Subir</a>");
		banderaEdicion3 = true;
		$("#btnCancelarReu").show();
		$("#btnAgregarReu").hide();
		$("#btnEliminarReu").show();
		$("#btnGuardarReu").show();
		$("#btnGenReporte").show();
		cargarTrabajadoresReunion(1);
		hallarEstadoReunion(reunionId);
		$("#modalReunion").show();
		$("#modalTrabsReu").hide();
	}
}

function cargarTrabajadoresReunion(accionId) {
	if(accionId==null){
		$("#mdProgFecha .modal-body").hide();
		$("#mdProgFecha .modal-title").html("<a onclick='volverGruposTrabajo()'>Grupo</a> > <a onclick='volverReunionesGrupo()'>Reunión </a> > Participantes");
		$("#modalTrabsReu").show(); 
		$("#importTrabReunion").hide();
		$("#importTrabReunion").keyup(function(){
			var texto = $(this).val();
			var listExcel = texto.split('\n');
			var validado="";
			var listTrabs=[];
			listTrabajadoresReunion.forEach(function(val,index){
				 
					$("#list-trab"+val.trabajadorId).prop("checked",false);
				
			});
			for (var int = 0; int < listExcel.length; int++) {
				var trab={};
				
				var filaTexto = listExcel[int];
				filaTexto=filaTexto.toUpperCase();
			
				listTrabajadoresReunion.forEach(function(val,index){
					var trabNombre=val.nombre;
					trabNombre=trabNombre.trim().toUpperCase();
					if(trabNombre.indexOf(filaTexto)!=-1 && filaTexto.length>0){
						$("#list-trab"+val.trabajadorId).prop("checked",true);
					}
				});
					 
					
				
			};
		});
		$("#btnShowImport").on("click",function(){
			$("#importTrabReunion").toggle();
		});
		
	}
	
	if (listTrabsReuSeleccionados.length === 0) {
		
		$("#list-trabs").html("");
		var dataParam = {
			id : reunionId
		};

		callAjaxPost(
				URL + '/trabajador/reunion/trabajadores',
				dataParam,
				function(data) {
					switch (data.CODE_RESPONSE) {
					case "05":
					
						listTrabajadoresReunion = data.list;
						for (index = 0; index < listTrabajadoresReunion.length; index++) {
							
							if (listTrabajadoresReunion[index].participaReunion == 1) {
								
								$("#list-trabs").append(
										"<li class='list-group-item'>"
												+ "<input type='checkbox' class='check' "
												+ "id='list-trab"
												+ listTrabajadoresReunion[index].trabajadorId + "'" 
												+ "checked>"
												+ listTrabajadoresReunion[index].nombre + "</li>");
								listTrabsReuSeleccionados
								.push({trabajadorId:listTrabajadoresReunion[index].id});
							}else{
								$("#list-trabs").append(
										"<li class='list-group-item'>"
												+ "<input type='checkbox' class='check' "
												+ "id='list-trab"
												+ listTrabajadoresReunion[index].trabajadorId + "'>"
												+ listTrabajadoresReunion[index].nombre + "</li>");
							}
						
					}
						
						$("#list-trabs").prepend(
								"<li class='list-group-item'>"
										+ "<input type='checkbox' class='' "
										+ "id='list-trab-all'>"
										+ "<strong>Todos</strong>" + "</li>");
						$("#list-trab-all").on("click",function(){
							var marcado=$(this).prop("checked");
							$("#list-trabs .check").prop("checked",marcado);
						});
						
						break;
					default:
						alert("Ocurrió un error al cargar los trabs de reuniones!");
					}
				});
	}
	
}

function nuevaReunionSeguridad() {
	if (!banderaEdicion3) {
		var selMotivo = crearSelectOneMenu("selTipoReu", "",
				listTiposReunion, "", "id",
				"nombre");
		
		$("#tblReu tbody")
				.append(
						"<tr id='0'>"
						+ "<td>"+selMotivo+"</td>"
						+ "<td>...</td>"
						+ "<td>...</td>"
					
						+ "<td><input type='date' id='inputPlan' class='form-control' onchange='hallarEstadoReunion(0)'></td>"
						+"<td>"
						+"<input type='time' value='12:00:00' id='inputHoraPla' " +
						" class='form-control'>"
						+"</td>"
						+ "<td><input type='date' id='inputReal' class='form-control' onchange='hallarEstadoReunion(0)'></td>"
						+ "<td>...</td>"
						+ "<td>...</td>"
						+ "<td><input type='text' id='inputObsReu' class='form-control' ></td>"
						+ "<td>...</td>"
						+ "<td id='tdestadoreu0'>...</td>"
						+"</tr>");
		reunionId = 0;
		$("#btnCancelarReu").show();
		$("#btnAgregarReu").hide();
		$("#btnEliminarReu").hide();
		$("#btnGuardarReu").show();
	
		formatoCeldaSombreableTabla(false,"tblReu");
		banderaEdicion3 = true;
	} else {
		alert("Guarde primero.");
	}
}

function cancelarReunionSeguridad() {
	cargarModalReunionSeguridad();
}

function eliminarReunionSeguridad() {
	var r = confirm("¿Está seguro de eliminar la reunión?");
	if (r == true) {
		var dataParam = {
			id : reunionId,
		};

		callAjaxPost(URL + '/trabajador/reunion/delete', dataParam,
				function(data) {
					switch (data.CODE_RESPONSE) {
					case "05":
						cargarModalReunionSeguridad();
						break;
					default:
						alert("Ocurrió un error al eliminar la reunion!");
					}
				});
	}
}

function guardarReunionSeguridad() {
$("#modalReunion button:not(#btnClipTablaReunion)").hide();
	var campoVacio = true;
var tipoId=$("#selTipoReu").val();
var inputObsReu=$("#inputObsReu").val();
	var mes1 = $("#inputPlan").val().substring(5, 7) - 1;
	var fechatemp1 = new Date($("#inputPlan").val().substring(0, 4), mes1, $(
			"#inputPlan").val().substring(8, 10));

	var inputPlan = fechatemp1;
	
	var mes2 = $("#inputReal").val().substring(5, 7) - 1;
	var fechatemp2 = new Date($("#inputReal").val().substring(0, 4), mes2, $(
			"#inputReal").val().substring(8, 10));

	var inputReal = fechatemp2;
	var horaPlan=$("#inputHoraPla").val();
	if(!$("#inputHoraPla").val()){
		
		horaPlan=null;
	}
if(!$("#inputPlan").val()){
		
	inputPlan=null;
	}
if(!$("#inputReal").val()){
	inputReal=null;
};

if(tipoId=="-1" || inputPlan==null ){
	campoVacio=false;
};

var listTrabsSelec=[];

$("#list-trabs .check").each(function() {
	if ($(this).is(':checked')) {
		var id = $(this).attr('id');
		listTrabsSelec.push({trabajadorId:id.substring(9, id.length)});
	}
	console.log($(this).attr('id'));
});
	if (campoVacio) {

		var dataParam = {
				fechaPlanificada : inputPlan,
			fechaRealizada:inputReal,horaPlanificada:horaPlan,
			observacion:inputObsReu,
			id : reunionId,
			grupoSeguridadId : grupoSeguridadId,
			trabs: listTrabsSelec,
			estado:{
				estadoCumpId:reunion
			},
			tipo:{
				id:parseInt(tipoId)
			}
		}; 
		callAjaxPost(URL + '/trabajador/reunion/save', dataParam,
				function(data) {
					switch (data.CODE_RESPONSE) {
					case "05":
						cargarModalReunionSeguridad();
						break;
					default:
						alert("Ocurrió un error al guardar la reunion!");
					}
				},loadingCelda,"tblReu #tr"+reunionId);
	} else {
		alert("Debe ingresar la fecha planificada y el tipo de reunion");
	}
}



function subirEvidencia(idObjeto,tipoId) {
	$('#mdUpload').modal('show');
	switch(tipoId){
	case 1:
		$('#btnUploadArchivo').attr("onclick",
				"javascript:procesarEvidencia(" + idObjeto + "," + tipoId + ");");
		break;
	case 2:
		$('#btnUploadArchivo').attr("onclick",
				"javascript:procesarEvidenciaAcuerdo(" + idObjeto + "," + tipoId + ");");
		break;
	}

}

function procesarEvidencia(progEquipoId,tipoId) {
	var inputFileImage = document.getElementById("fileEvi");
	var file = inputFileImage.files[0];
	var data = new FormData();
console.log(file.size );console.log(progEquipoId);
	if (file.size > bitsEvidenciaReunion) {
		alert("Lo sentimos, solo se pueden subir archivos menores a "+bitsEvidenciaReunion/1000000+" MB");
	} else {
		data.append("fileEvi", file);
		data.append("reunionId", progEquipoId);
		
		var url = URL + '/trabajador/reunion/evidencia/save';
		$.blockUI({
			message : 'cargando...'
		});
		$
				.ajax({
					url : url,
					xhrFields: {
			            withCredentials: true
			        },
					type : 'POST',
					contentType : false,
					data : data,
					processData : false,
					cache : false,
					success : function(data, textStatus, jqXHR) {
						$.unblockUI();
						switch (data.CODE_RESPONSE) {
						case "06":
							sessionStorage.clear();
							document.location.replace(data.PATH);
							break;
						default:
							console.log('Se subio el archivo correctamente.');
							$('#mdUpload').modal('hide');
						}

					},
					error : function(jqXHR, textStatus, errorThrown) {
						$.unblockUI();
						alert("Ups, tenemos problemas tecnicos. Intente más tarde. Error: "
								+ errorThrown);
						console.log('xhRequest: ' + jqXHR + "\n");
						console.log('ErrorText: ' + textStatus + "\n");
						console.log('thrownError: ' + errorThrown + "\n");
					}
				});
	}
}

function hallarEstadoReunion(progId){
	var fechaPlanificada=$("#inputPlan").val();
	var fechaReal=$("#inputReal").val();
	var fechaHoy=obtenerFechaActual();
	if(fechaReal!=''){
		reunion=2;
		reunionNombre="Completado";
	}else{
		
		if(fechaPlanificada!=""){
			var dif=restaFechas(fechaHoy,fechaPlanificada)
			if(dif<0){
				reunion=3;
				reunionNombre="Retrasado";
			}else{
				reunion=1;
				reunionNombre="Por implementar";
			}
			
		}else{
			
			reunion=1;
			reunionNombre="Por implementar";
			
		}
	}
	
	$("#tdestadoreu"+progId).html(reunionNombre);
	
}


function generarHojaRuta(){
	var ss=$("#inputReal").val(); 
	if(!$("#inputReal").val()){
		alert("No se puede generar un reporte de una reunión no completada!");
		return false;
	};
	switch(parseInt($("#selTipo").val())){
	
	case 1:
		window.open(URL
				+ "/trabajador/reunion/informe/completo?reunionId="
				+ reunionId 
			);	
		break;
	case 2:
		window.open(URL
				+ "/trabajador/reunion/informe/completo?reunionId="
				+ reunionId 
			);	
		break;
	default:
		window.open(URL
				+ "/trabajador/reunion/informe/basico?reunionId="
				+ reunionId 
			);	
		break;
	
	
	}
	
}




function verAgendaReunion(){
	$("#mdProgFecha .modal-body").hide();
	$("#mdProgFecha .modal-title").html("<a onclick='volverGruposTrabajo()'>Grupo</a> > <a onclick='volverReunionesGrupo()'>Reunión </a> > Agenda");
	$("#modalAgenda").show(); 
	llamarAgendasReunion();
}
function verDesarrolloReunion(){
	$("#mdProgFecha .modal-body").hide();
	$("#mdProgFecha .modal-title").html("<a onclick='volverGruposTrabajo()'>Grupo</a> > <a onclick='volverReunionesGrupo()'>Reunión </a> > Desarrollo");
	$("#modalDesarrollo").show(); 
	llamarDesarrollosReunion();
}
function volverReunionesGrupo(){
	$("#mdProgFecha .modal-body").hide();
	$("#modalReunion").show();
	$("#mdProgFecha .modal-title").html("<a onclick='volverGruposTrabajo()'>Grupo</a> > Reuniones");
	
}

function volverGruposTrabajo(){
	$("#mdProgFecha .modal-body").hide();
	$("#modalGrupo").show();
	$("#mdProgFecha .modal-title").html("Grupos de Trabajadores");
}