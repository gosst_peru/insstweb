var acuerdoId;
var fechaEntrega;
var banderaEdicion4;
var listEquipos;
var listTiposAcu;
var listEstados;
var listFullAcuerdos;
var acuerdoEstadoId;
var acuerdoEstadoNombre;
var gestionAccionMejoraId;

$(function(){ 
	$("#mdVerif").load("agregaraccionmejora.html");
	$("#btnClipTablaAcuerdo").on("click",function(){
		var listFullTablaProgramacion=""; 
		listFullTablaProgramacion=	obtenerDatosTablaEstandar("tblAcu");
		copiarAlPortapapeles(listFullTablaProgramacion,"btnClipTablaAcuerdo");
		
		alert("Se han guardado al clipboard la tabla de acuerdos" );

	});
})
var tablaFijaAcuerdo=false;
 
function cargarAcuerdosReunion() {
	$("#mdProgFecha .modal-body").hide();
	$("#mdProgFecha .modal-title").html("<a onclick='volverGruposTrabajo()'>Grupo</a> > <a onclick='volverReunionesGrupo()'>Reunión </a> > Acuerdos");
	
	$('#modalAcuerdo').show('slow',function(){
		cargarModalAcuerdoSeguridad();
		
	});
	
	
	
}


function volverReuniones(){
	$("#modalReunion").show();
	$("#modalTrabsReu").hide();	
	$("#modalAcuerdo").hide();
}
function cargarModalAcuerdoSeguridad() {
	$("#btnVolverReuniones").attr("onclick", "javascript:volverReuniones();");

	$("#btnAgregarAcuerdo").attr("onclick", "javascript:nuevaAcuerdoSeguridad();");
	$("#btnCancelarAcuerdo").attr("onclick", "javascript:cancelarAcuerdoSeguridad();");
	$("#btnGuardarAcuerdo").attr("onclick", "javascript:guardarAcuerdoSeguridad();");
	$("#btnEliminarAcuerdo").attr("onclick", "javascript:eliminarAcuerdoSeguridad();");
	

	$("#btnCancelarAcuerdo").hide();
	$("#btnAgregarAcuerdo").show();
	$("#btnEliminarAcuerdo").hide();
	$("#btnGuardarAcuerdo").hide();

	$("#modalReunion").hide();
	$("#modalTrabsReu").hide();
	$("#modalAcuerdo").show(500,function(){
		acuerdoId = 0;
		gestionAccionMejoraId=null;
		banderaEdicion4 = false;
		listFullAcuerdos=[];
		var dataParam = {
				reunionId : reunionId
		}; 
		callAjaxPost(URL + '/trabajador/acuerdos', dataParam, function(data) {
			switch (data.CODE_RESPONSE) {
			case "05":
				listTiposAcu = data.listTiposAcuerdo;
				var list=data.list;
				listFullAcuerdos=data.list; 
				$("#tblAcu tbody tr").remove();
				for (var index = 0; index < list.length; index++) {
	if(list[index].tipo.id!=1){
		$("#tblAcu tbody").append(
				"<tr id='tr" + list[index].id
						+ "' onclick='javascript:editarAcuerdoSeguridad("
						+index+")' >"
						+ "<td id='tdtipoacu"
						+ list[index].id + "'>"
						+ list[index].tipo.nombre + "</td>"
						+ "<td id='tddescacu" + list[index].id
						+ "'>"+list[index].descripcion+"</td>"
						+ "<td id='tdrespacu" + list[index].id
						+ "'>"+list[index].responsable+"</td>"
						
						
						+ "<td id='tdfechaplaacu"
						+ list[index].id + "'>"
						+ list[index].fechaPlanificadaTexto + "</td>"
						+ "<td id='tdhoraplaacu"
						+ list[index].id + "'> "
						+ list[index].horaPlanificadaTexto + "</td>"
						+ "<td id='tdfecharealacu"
						+ list[index].id + "'> "
						+ list[index].fechaRealizadaTexto + "</td>"
						+ "<td id='tdinv" + list[index].id
						+ "'>"+list[index].inversion+"</td>"
						+ "<td id='tdobsacu" + list[index].id
						+ "'>"+list[index].observacion+"</td>"
						+ "<td id='tdeviacu" + list[index].id
						+ "'>"+list[index].evidenciaNombre+"</td>"
						
						+ "<td id='tdestadoacu" + list[index].id
						+ "'>"+list[index].estado.estadoCumplientoNombre+"</td>"
						
						
						+"</tr>");
	}else{
		
		$("#tblAcu tbody").append(
				"<tr id='tr" + list[index].id
						+ "' onclick='javascript:editarAcuerdoSeguridad("
						+index+")' >"
						+ "<td id='tdtipoacu"
						+ list[index].id + "'>"
						+ list[index].tipo.nombre + "</td>"
						+ "<td id='tddescacu" + list[index].id
						+ "'>"+list[index].resumenAccion+"</td>"
						+ "<td id='tdrespacu" + list[index].id
						+ "'>"+""+"</td>"
						
						
						+ "<td id='tdfechaplaacu"
						+ list[index].id + "'>"
						+ ""+ "</td>"
						+ "<td id='tdhoraplaacu"
						+ list[index].id + "'> "
						+ ""+ "</td>"
						+ "<td id='tdfecharealacu"
						+ list[index].id + "'> "
						+ "" + "</td>"
						+ "<td id='tdinv" + list[index].id
						+ "'>"+""+"</td>"
						+ "<td id='tdobsacu" + list[index].id
						+ "'>"+""+"</td>"
						+ "<td id='tdeviacu" + list[index].id
						+ "'>"+""+"</td>"
						
						+ "<td id='tdestadoacu" + list[index].id
						+ "'>"+""+"</td>"
						
						
						+"</tr>");
	}
					
				
			}
				 
				if(!tablaFijaAcuerdo){
					tablaFijaAcuerdo=true;
					$("#wrapper3").css("height",$(window).height()*0.6-50+"px");
					goheadfixedY('#tblAcu',"#wrapper3");
				}
				if(getSession("linkCalendarioAcuerdoId")!=null){
					
					listFullAcuerdos.every(function(val,index3){
									
									if(val.id==parseInt(getSession("linkCalendarioAcuerdoId"))){
										
										editarAcuerdoSeguridad(index3);
										
										sessionStorage.removeItem("linkCalendarioAcuerdoId");
										return false;
									}else{
										return true;
									}
								});
								
							}
				formatoCeldaSombreableTabla(true,"tblAcu");
				break;
			default:
				alert("Ocurrió un error al traer las reuniones!");
			}
		});
	});
	
}

function editarAcuerdoSeguridad (pindex) {

	if (!banderaEdicion4) {
		
		acuerdoId = listFullAcuerdos[pindex].id;
		var fechaPlanificada = listFullAcuerdos[pindex].fechaPlanificada;
		var fechaRealizada = listFullAcuerdos[pindex].fechaRealizada;

		var tipoId=listFullAcuerdos[pindex].tipo.id;
		var observaciones=listFullAcuerdos[pindex].observacion;
		var horaPlanificada=listFullAcuerdos[pindex].horaPlanificada;
		var descripcion=listFullAcuerdos[pindex].descripcion;
		var inversion=listFullAcuerdos[pindex].inversion;
		var respAcu=listFullAcuerdos[pindex].responsable;
		
		var funcAux="javascript:cambiarAcordeTipoAcuerdo("+pindex+")";
		
		formatoCeldaSombreableTabla(false,"tblAcu");
			var selMotivo = crearSelectOneMenu("selTipoAcu",funcAux ,
					listTiposAcu, tipoId, "id",
					"nombre");
		
		$("#tdtipoacu"+acuerdoId).html(selMotivo);
		
		$("#tdobsacu" + acuerdoId).html(
		"<input type='text' id='inputObsAcu' class='form-control'>");
		
		$("#tddescacu" + acuerdoId).html(
		"<input type='text' id='inputDesc' class='form-control' style='width: 361px;'>");
		
		$("#tdinv" + acuerdoId).html(
		"<input type='number' id='inputInv' class='form-control'>");
		
		$("#tdrespacu" + acuerdoId).html(
		"<input type='text' id='inputRespAcu' class='form-control'>");
		
		$("#tdfechaplaacu" + acuerdoId).html(
				"<input type='date' id='inputPlanAcu' class='form-control' onchange='hallarEstadoAcuerdo("+acuerdoId+")'>");
		$("#tdfecharealacu" + acuerdoId).html(
				"<input type='date' id='inputRealAcu' class='form-control' onchange='hallarEstadoAcuerdo("+acuerdoId+")'>");
		
		$("#tdhoraplaacu" + acuerdoId).html(
				"<input type='time' id='inputHoraPlaAcu' " +
				" class='form-control'>");
	
		$("#inputObsAcu").val(observaciones);
		$("#inputDesc").val(descripcion);
		$("#inputInv").val(inversion);
		$("#inputRespAcu").val(respAcu);
		$("#inputPlanAcu").val(convertirFechaInput(fechaPlanificada));
		$("#inputRealAcu").val(convertirFechaInput(fechaRealizada));
		$("#inputHoraPlaAcu").val(horaPlanificada);
		var listNombresTrabs=listarStringsDesdeArray(listTrabajadoresReunion,"nombre");

		ponerListaSugerida("inputRespAcu",listNombresTrabs,true);
		var nombreEvidencia=listFullAcuerdos[pindex].evidenciaNombre;
		$("#tdeviacu" + acuerdoId)
		.html(
				"<a href='"
						+ URL
						+ "/trabajador/acuerdo/evidencia?acuerdoId="
						+ acuerdoId
						+ "' "
						+ "target='_blank'>" +
						nombreEvidencia+	"</a>"
						+ "<br/><a href='#' onclick='javascript:subirEvidencia("
						+ acuerdoId + ",2)'  id='subirimagen"
						+ acuerdoId + "'>Subir</a>");
		banderaEdicion4 = true;
		$("#btnCancelarAcuerdo").show();
		$("#btnAgregarAcuerdo").hide();
		$("#btnEliminarAcuerdo").show();
		$("#btnGuardarAcuerdo").show();
		
		hallarEstadoAcuerdo(acuerdoId);
		cambiarAcordeTipoAcuerdo(pindex);
	}
}


function nuevaAcuerdoSeguridad() {
	
	if (!banderaEdicion4) {
		formatoCeldaSombreableTabla(false,"tblAcu");
		var selMotivo = crearSelectOneMenu("selTipoAcu", "cambiarAcordeTipoAcuerdo()",
				listTiposAcu, "", "id",
				"nombre");
		
		$("#tblAcu tbody")
				.append(
						"<tr id='0'>"
						+ "<td>"+selMotivo+"</td>"
						
						+ "<td><input type='text' id='inputDesc' class='form-control' ></td>"
						+ "<td><input type='text' id='inputRespAcu' class='form-control' ></td>"
						
						+ "<td><input type='date' id='inputPlanAcu' class='form-control' onchange='hallarEstadoAcuerdo(0)'></td>"
						+"<td>"
						+"<input type='time' value='12:00:00' id='inputHoraPlaAcu' " +
						" class='form-control'>"
						+"</td>"
						+ "<td><input type='date' id='inputRealAcu' class='form-control' onchange='hallarEstadoAcuerdo(0)'></td>"
						+ "<td><input type='number' id='inputInv' class='form-control' ></td>"
						+ "<td><input type='text' id='inputObsAcu' class='form-control' ></td>"
					
				
						+ "<td>...</td>"
						+ "<td id='tdestadoacu0'>...</td>"
						+"</tr>");
		acuerdoId = 0;
		$("#btnCancelarAcuerdo").show();
		$("#btnAgregarAcuerdo").hide();
		$("#btnEliminarAcuerdo").hide();
		$("#btnGuardarAcuerdo").show();
	
		var listNombresTrabs=listarStringsDesdeArray(listTrabajadoresReunion,"nombre");
		ponerListaSugerida("inputRespAcu",listNombresTrabs,true);
		banderaEdicion4 = true;
	} else {
		alert("Guarde primero.");
	}
}

function cancelarAcuerdoSeguridad() {
	cargarModalAcuerdoSeguridad();
}

function eliminarAcuerdoSeguridad() {
	var r = confirm("¿Está seguro de eliminar la reunión?");
	if (r == true) {
		var dataParam = {
			id : acuerdoId,
		};

		callAjaxPost(URL + '/trabajador/acuerdo/delete', dataParam,
				function(data) {
					switch (data.CODE_RESPONSE) {
					case "05":
						cargarModalAcuerdoSeguridad();
						break;
					default:
						alert("Ocurrió un error al eliminar la reunion!");
					}
				});
	}
}

function guardarAcuerdoSeguridad() {

	var campoVacio = true;
var tipoId=$("#selTipoAcu").val();
if(parseInt(tipoId)!=1){

	var inputObsAcu=$("#inputObsAcu").val();
		var mes1 = $("#inputPlanAcu").val().substring(5, 7) - 1;
		var fechatemp1 = new Date($("#inputPlanAcu").val().substring(0, 4), mes1, $(
				"#inputPlanAcu").val().substring(8, 10));

		var inputPlanAcu = fechatemp1;
		
		var mes2 = $("#inputRealAcu").val().substring(5, 7) - 1;
		var fechatemp2 = new Date($("#inputRealAcu").val().substring(0, 4), mes2, $(
				"#inputRealAcu").val().substring(8, 10));

		var inputRealAcu = fechatemp2;
		var horaPlan=$("#inputHoraPlaAcu").val();
		if(!$("#inputHoraPlaAcu").val()){
			
			horaPlan=null;
		}
	if(!$("#inputPlanAcu").val()){
			
		inputPlanAcu=null;
		}
	if(!$("#inputRealAcu").val()){
		inputRealAcu=null;
	};

	if(tipoId=="-1" || inputPlanAcu==null ){
		campoVacio=false;
	};
	var descripcion=$("#inputDesc").val();
	var inversion=$("#inputInv").val();
	var responsable=$("#inputRespAcu").val();
		if (campoVacio) {

			var dataParam = {
				fechaPlanificada : inputPlanAcu,
				fechaRealizada:inputRealAcu,
				horaPlanificada:horaPlan,
				inversion:parseFloat(inversion),
				descripcion:descripcion,
				observacion:inputObsAcu,
				id : acuerdoId,
				reunionId : reunionId,
				estado:{
					estadoCumpId:acuerdoEstadoId
				},
				tipo:{
					id:parseInt(tipoId)
				},
				gestionAccionId:gestionAccionMejoraId,
				responsable:responsable
			};

			callAjaxPost(URL + '/trabajador/acuerdo/save', dataParam,
					function(data) {
						switch (data.CODE_RESPONSE) {
						case "05":
							cargarModalAcuerdoSeguridad();
							break;
						default:
							alert("Ocurrió un error al guardar el acuerdo!");
						}
					});
		} else {
			alert("Debe ingresar la fecha planificada y el tipo de acuerdo");
		}
}else{
	if(tipoId=="-1"  ){
		campoVacio=false;
	};
	
	if (campoVacio) {

		var dataParam = {
			fechaPlanificada : null,
			fechaRealizada:null,
			horaPlanificada:null,
			inversion:parseFloat(0.0),
			descripcion:"",
			observacion:"",
			id : acuerdoId,
			reunionId : reunionId,
			estado:{
				estadoCumpId:1
			},
			tipo:{
				id:parseInt(tipoId)
			},
			gestionAccionId:null,
			responsable:""
		};

		callAjaxPost(URL + '/trabajador/acuerdo/save', dataParam,
				function(data) {
					switch (data.CODE_RESPONSE) {
					case "05":
						cargarModalAcuerdoSeguridad();
						break;
					default:
						alert("Ocurrió un error al guardar el acuerdo!");
					}
				});
	} else {
		alert("Debe ingresar  el tipo de acuerdo");
	}
}
}





function procesarEvidenciaAcuerdo(progEquipoId,tipoId) {
	var inputFileImage = document.getElementById("fileEvi");
	var file = inputFileImage.files[0];
	var data = new FormData();

	if (file.size > bitsEvidenciaAcuerdo) {
		alert("Lo sentimos, solo se pueden subir archivos menores a "+bitsEvidenciaAcuerdo/1000000+" MB");
	} else {
		data.append("fileEvi", file);
		data.append("acuerdoId", progEquipoId);
		
		var url = URL + '/trabajador/acuerdo/evidencia/save';
		$.blockUI({
			message : 'cargando...'
		});
		$
				.ajax({
					url : url,
					xhrFields: {
			            withCredentials: true
			        },
					type : 'POST',
					contentType : false,
					data : data,
					processData : false,
					cache : false,
					success : function(data, textStatus, jqXHR) {
						$.unblockUI();
						switch (data.CODE_RESPONSE) {
						case "06":
							sessionStorage.clear();
							document.location.replace(data.PATH);
							break;
						default:
							console.log('Se subio el archivo correctamente.');
							$('#mdUpload').modal('hide');
						}

					},
					error : function(jqXHR, textStatus, errorThrown) {
						$.unblockUI();
						alert("Ups, tenemos problemas tecnicos. Intente más tarde. Error: "
								+ errorThrown);
						console.log('xhRequest: ' + jqXHR + "\n");
						console.log('ErrorText: ' + textStatus + "\n");
						console.log('thrownError: ' + errorThrown + "\n");
					}
				});
	}
}

function hallarEstadoAcuerdo(progId){
	var fechaPlanificada=$("#inputPlanAcu").val();
	var fechaReal=$("#inputRealAcu").val();
	var fechaHoy=obtenerFechaActual();
	if(fechaReal!=''){
		acuerdoEstadoId=2;
		acuerdoEstadoNombre="Completado";
	}else{
		
		if(fechaPlanificada!=""){
			var dif=restaFechas(fechaHoy,fechaPlanificada)
			if(dif<0){
				acuerdoEstadoId=3;
				acuerdoEstadoNombre="Retrasado";
			}else{
				acuerdoEstadoId=1;
				acuerdoEstadoNombre="Por implementar";
			}
			
		}else{
			
			acuerdoEstadoId=1;
			acuerdoEstadoNombre="Por implementar";
			
		}
	}
	
	$("#tdestadoacu"+progId).html(acuerdoEstadoNombre);
	
}



function cambiarAcordeTipoAcuerdo(pindex){
	var tipoId=$("#selTipoAcu").val();
	if(pindex==null){
		acuerdoId=0;
		if(parseInt(tipoId)==1){
$("#tblAcu input").hide();	
		}else{
			$("#tblAcu input").show();
			
		}
	}else{
		acuerdoId = listFullAcuerdos[pindex].id;
		if(parseInt(tipoId)!=1){

			var fechaPlanificada = listFullAcuerdos[pindex].fechaPlanificada;
			var fechaRealizada = listFullAcuerdos[pindex].fechaRealizada;

			var tipoId=listFullAcuerdos[pindex].tipo.id;
			var observaciones=listFullAcuerdos[pindex].observacion;
			var horaPlanificada=listFullAcuerdos[pindex].horaPlanificada;
			var descripcion=listFullAcuerdos[pindex].descripcion;
			var inversion=listFullAcuerdos[pindex].inversion;
			var respAcu=listFullAcuerdos[pindex].responsable;
			
			
			
			$("#tdobsacu" + acuerdoId).html(
			"<input type='text' id='inputObsAcu' class='form-control'>");
			
			$("#tddescacu" + acuerdoId).html(
			"<input type='text' id='inputDesc' class='form-control'>");
			
			$("#tdinv" + acuerdoId).html(
			"<input type='number' id='inputInv' class='form-control'>");
			
			$("#tdrespacu" + acuerdoId).html(
			"<input type='text' id='inputRespAcu' class='form-control'>");
			
			$("#tdfechaplaacu" + acuerdoId).html(
					"<input type='date' id='inputPlanAcu' class='form-control' onchange='hallarEstadoAcuerdo("+acuerdoId+")'>");
			$("#tdfecharealacu" + acuerdoId).html(
					"<input type='date' id='inputRealAcu' class='form-control' onchange='hallarEstadoAcuerdo("+acuerdoId+")'>");
			
			$("#tdhoraplaacu" + acuerdoId).html(
					"<input type='time' id='inputHoraPlaAcu' " +
					" class='form-control'>");

			
			$("#inputDesc").val(descripcion);
			$("#inputInv").val(inversion);
			$("#inputRespAcu").val(respAcu);
			$("#inputPlanAcu").val(convertirFechaInput(fechaPlanificada));
			$("#inputRealAcu").val(convertirFechaInput(fechaRealizada));
			$("#inputHoraPlaAcu").val(horaPlanificada);
			$("#inputObsAcu").val(observaciones);
			var listNombresTrabs=listarStringsDesdeArray(listTrabajadoresReunion,"nombre");

			ponerListaSugerida("inputRespAcu",listNombresTrabs,true);
			var nombreEvidencia=listFullAcuerdos[pindex].evidenciaNombre;
			$("#tdeviacu" + acuerdoId)
			.html(
					"<a href='"
							+ URL
							+ "/trabajador/acuerdo/evidencia?acuerdoId="
							+ acuerdoId
							+ "' "
							+ "target='_blank'>" +
							nombreEvidencia+	"</a>"
							+ "<br/><a href='#' onclick='javascript:subirEvidencia("
							+ acuerdoId + ",2)'  id='subirimagen"
							+ acuerdoId + "'>Subir</a>");
		}else{
			
			
			$("#tdobsacu" + acuerdoId).html(
			"");
			
			$("#tddescacu" + acuerdoId).html(
					listFullAcuerdos[pindex].estado.icono+"<a href='#' onclick='javascript:agregarAccionMejora(5, "
							+ listFullAcuerdos[pindex].id + ",1);'>"+listFullAcuerdos[pindex].nombreAccionMejora+"");
			
			$("#tdinv" + acuerdoId).html(
			"");
			
			$("#tdrespacu" + acuerdoId).html(
			"");
			
			$("#tdfechaplaacu" + acuerdoId).html(
					"");
			$("#tdfecharealacu" + acuerdoId).html(
					"");
			
			$("#tdhoraplaacu" + acuerdoId).html(
					"");
			
			$("#tdeviacu" + acuerdoId)
			.html(
					"");

			
		}
	}
	
	
	
}
