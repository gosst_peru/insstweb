var banderaEdicion;
var sugeridoId;
var sugeridoFecha;
var sugeridoFechaProx;
var contador2 = 0;
var procEstado;
var procEstadoNombre;
var listFullSugerido;
var accesoGarantizado;
var tiposSugeridos=[{id:1,nombre:"De Gestión"},{id:2,nombre:"Operativos"}];
$(document).ready(function() {
	accesoGarantizado=false;
	insertMenu();
	
	$('#inputClave').keyup(function(e){
	    if(e.keyCode == 13)
	    {
	        $(this).trigger("enterKey");
	    }
	});
	$('#inputClave').bind("enterKey",function(e){
		cargarPrimerEstado();
		
		});
});

function cargarPrimerEstado() {
	banderaEdicion = false;
	sugeridoId = 0;
	sugeridoFecha=null;

	removerBotones();
	crearBotones();
	deshabilitarBotonesEdicion();
	$("#fsBotones")
	.append(
			"<button id='btnClipTabla' type='button' class='btn btn-success' title='Tabla Clipboard' >"
					+ "<i class='fa fa-clipboard fa-2x'></i>" + "</button>");
	

	$("#btnUpload").attr("onclick", "javascript:importarDatos();");
	$("#btnUpload").hide();
	$("#btnNuevo").attr("onclick", "javascript:nuevoSugerido();");
	$("#btnCancelar").attr("onclick", "javascript:cancelarSugerido();");
	$("#btnGuardar").attr("onclick", "javascript:guardarSugerido();");
	$("#btnEliminar").attr("onclick", "javascript:eliminarSugerido();");
	$("#btnImportar").attr("onclick", "javascript:importarDatos();");
$("#btnImportar").hide();
	$("#btnClipTabla").on("click", function(){
		new Clipboard('#btnClipTabla', {
			text : function(trigger) {
				return obtenerDatosTablaEstandar("tblSugSeg")
			}
		});
		
		alert("Se han guardado al clipboard la tabla de este módulo" );
	});
	
	var dataParam = {
			idCompany : sessionStorage.getItem("gestopcompanyid"),
			address:$("#inputClave").val()
	};

	 callAjaxPost(URL + '/procedimientoseguridad/sugeridos', dataParam,
	 procesarDataDescargadaPrimerEstado);
}

function procesarDataDescargadaPrimerEstado(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		var list = data.listSugerida;
		var acceso=data.acceso;
		listFullSugerido=list;
		$("#tblSugSeg tbody tr").remove();

		for (index = 0; index < list.length; index++) {

			$("#tblSugSeg tbody").append(

					"<tr id='tr" + list[index].id
							+ "' onclick='javascript:editarSugerido("
						+index+")' >"
							+ "<td id='tdcod" + list[index].id
							+ "'>" + list[index].codigoSugerido +"</td>"
							+ "<td id='tdtipo" + list[index].id
							+ "'>" + list[index].tipoSugeridoNombre +"</td>"
							+ "<td id='tdres" + list[index].id
							+ "'>" + list[index].resumen +"</td>"
							+ "<td id='tdapp" + list[index].id
							+ "'>" + list[index].aplicacion +"</td>"
							+ "<td id='tdversion" + list[index].id
							+ "'>" + list[index].version + "</td>"
							+ "<td id='tdfecha" + list[index].id
							+ "'>" + list[index].fechaActualizacion + "</td>"
							+ "<td id='tdevi" + list[index].id
							+ "'>"+list[index].evidenciaNombre+"</td>"
							+ "</tr>");
		}
		formatoCeldaSombreable(true);
		if(parseInt(acceso)>0 || accesoGarantizado){
			completarBarraCarga();
			$("#inputClave").remove();
			accesoGarantizado=true;
		}else{
			$("#inputClave").after("Clave incorrecta, que mal hacck");
			
		}
	
		
		break;
	default:
		alert("Ocurrió un error al traer los sugeridos!");
	}
	if (contador2 == 0) {
		goheadfixed('table.fixed');
	}
	contador2 = contador2 + 1;
}

function editarSugerido(pindex) {
	if (!banderaEdicion) {
		
		sugeridoId = listFullSugerido[pindex].id;
		sugeridoFecha = listFullSugerido[pindex].fechaDate;
		var tipoId=listFullSugerido[pindex].tipoSugeridoId;
		var codigo=listFullSugerido[pindex].codigoSugerido;
		var resumen=listFullSugerido[pindex].resumen;
		var aplicacion=listFullSugerido[pindex].aplicacion;
		var version=listFullSugerido[pindex].version;
		var evidenciaNombre=listFullSugerido[pindex].evidenciaNombre;
		
		formatoCeldaSombreable(false);$("#btnClipTabla").hide();
		$("#tdcod" + sugeridoId)
				.html(
						"<input type='text' id='inputCodigo' class='form-control' placeholder='Codigo' autofocus='true' ' value='"
								+ codigo + "'>");
		
		$("#tdres" + sugeridoId)
		.html(
				"<input type='text' id='inputResumen' class='form-control' placeholder='Resumen'  ' value='"
						+ resumen + "'>");
		$("#tdapp" + sugeridoId)
		.html(
				"<input type='text' id='inputAplicacion' class='form-control' placeholder='Objetivo'  ' value='"
						+ aplicacion + "'>");
		$("#tdversion" + sugeridoId)
		.html(
				"<input type='text' id='inputVersion' class='form-control' placeholder='Version' ' value='"
						+ version + "'>");
		
		var slcTipo=crearSelectOneMenu("slcTipo","",tiposSugeridos,tipoId,"id","nombre");
		$("#tdtipo" + sugeridoId)
				.html(slcTipo);
		
		$("#tdfecha" + sugeridoId)
				.html(
						"<input type='date' id='inputFec'  class='form-control'>");

		var today = convertirFechaInput(sugeridoFecha)
		$("#inputFec").val(today);
		
		
		$("#tdevi" + sugeridoId)
		.html(
				"<a href='"
						+ URL
						+ "/procedimientoseguridad/sugerido/evidencia?sugeridoId="
						+ sugeridoId
						+ "' "
						+ "target='_blank'>"+evidenciaNombre+"</a>"
						+ "<br/><a href='#' onclick='javascript:mostrarCargarImagen("
						+ sugeridoId + ")'  id='subirimagen"
						+ sugeridoId + "'>Subir</a>");
		
		banderaEdicion = true;
		habilitarBotonesEdicion();
	}
}

function nuevoSugerido() {
	if (!banderaEdicion) {
		var slcTipo=crearSelectOneMenu("slcTipo","",tiposSugeridos,"-1","id","nombre");
		$("#tblSugSeg tbody:first")
				.append(
						"<tr id='0'>" 
						+ "<td><input type='text' id='inputCodigo' placeholder='Codigo' class='form-control' autofocus='true'></td>"		
						+"<td>"+slcTipo+"</td>"
						+ "<td><input type='text' id='inputResumen' placeholder='Titulo' class='form-control' autofocus='true'></td>"		
						+ "<td><input type='text' id='inputAplicacion' placeholder='Obketivo' class='form-control' autofocus='true'></td>"		
						+ "<td><input type='text' id='inputVersion' placeholder='Versión' class='form-control' autofocus='true'></td>"		
						+ "<td><input type='date' id='inputFec' placeholder='Fecha' class='form-control' autofocus='true'></td>"		
						
					
						+ "<td>...</td>" 
								
						+ "</tr>");
		llevarTablaFondo("wrapper");
		sugeridoId = 0;
		habilitarBotonesNuevo();
		banderaEdicion = true;
	} else {
		alert("Guarde primero.");
	}
}

function cancelarSugerido() {
	cargarPrimerEstado();
}

function eliminarSugerido() {
	var r = confirm("¿Está seguro de eliminar la sugierodo?");
	if (r == true) {
		var dataParam = {
				id : sugeridoId,
		};

		callAjaxPost(URL + '/procedimientoseguridad/sugerido/delete', dataParam,
				procesarResultadoeliminarSugerido);
	}
}

function procesarResultadoeliminarSugerido(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		cargarPrimerEstado();
		break;
	default:
		alert("Ocurrió un error al eliminar el sugeirdo!");
	}
}

function guardarSugerido() {

	var campoVacio = true;
	var inputCodigo=$("#inputCodigo").val();
	var inputAplicacion = $("#inputAplicacion").val();
	var inputResumen = $("#inputResumen").val();
	var tipoId=$("#slcTipo").val();
	var inputVersion = $("#inputVersion").val();
	var fecha1=$("#inputFec").val();
	var mes = $("#inputFec").val().substring(5, 7) - 1;
	var fechatemp = new Date($("#inputFec").val().substring(0, 4), mes, $(
			"#inputFec").val().substring(8, 10));
	
	var inputFec = fechatemp;
	

	
	
	if (!fecha1) {
		inputFec = null
	}
	

	if (inputResumen.length == 0 || inputCodigo.length == 0
			|| inputVersion.length == 0 ||!fecha1) {
		campoVacio = false;
	}
	if (campoVacio) {

		var dataParam = {
			id : sugeridoId,
			codigoSugerido : inputCodigo,
			tipoSugeridoId:tipoId,
			resumen : inputResumen,
			aplicacion : inputAplicacion,
			version: inputVersion,
			fechaDate:inputFec
		};

		 callAjaxPost(URL + '/procedimientoseguridad/sugerido/save', dataParam,
		 procesarResultadoGuardarProSeg);
	} else {
		alert("Debe ingresar todos los campos.");
	}
}

function procesarResultadoGuardarProSeg(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		cargarPrimerEstado();
		break;
	default:
		alert("Ocurrió un error al guardar la sugerido!");
	}
}

function mostrarCargarImagen(sugeridoId) {
	$('#mdUpload').modal('show');
	$('#btnUploadArchivo').attr("onclick",
			"javascript:uploadArchivo(" + sugeridoId + ");");
}

function uploadArchivo(sugeridoId) {
	var inputFileImage = document.getElementById("fileEvi");
	var file = inputFileImage.files[0];
	var data = new FormData();

	if (file.size > bitsEvidenciaProcedimiento) {
		alert("Lo sentimos, solo se pueden subir " +
				"archivos menores a "+bitsEvidenciaProcedimiento/1000000+" MB");
	} else {
		data.append("fileEvi", file);
		data.append("sugeridoId", sugeridoId);
		
		var url = URL + '/procedimientoseguridad/sugerido/evidencia/save';
		$.blockUI({
			message : 'cargando...'
		});
		$
				.ajax({
					url : url,
					xhrFields: {
			            withCredentials: true
			        },
					type : 'POST',
					contentType : false,
					data : data,
					processData : false,
					cache : false,
					success : function(data, textStatus, jqXHR) {
						$.unblockUI();
						switch (data.CODE_RESPONSE) {
						case "06":
							sessionStorage.clear();
							document.location.replace(data.PATH);
							break;
						default:
							console.log('Se subio el archivo correctamente.');
							$('#mdUpload').modal('hide');
						}

					},
					error : function(jqXHR, textStatus, errorThrown) {
						$.unblockUI();
						alert("Ups, tenemos problemas tecnicos. Intente más tarde. Error: "
								+ errorThrown);
						console.log('xhRequest: ' + jqXHR + "\n");
						console.log('ErrorText: ' + textStatus + "\n");
						console.log('thrownError: ' + errorThrown + "\n");
					}
				});
	}
}

function importarDatos() {
	$("#btnGuardarNuevos").attr("onclick",
			"javascript:guardarMasivoCopiaExcel();");

	$('#mdImpDatos').modal('show');
	
}
function guardarMasivoCopiaExcel() {
	var texto = $("#txtListado").val();
	var listExcel = texto.split('\n');
	var listRecurso = [];
	var listProcRepe = [];
	var listFullNombres = "";
	var validado = "";
	if (texto.length < 10000) {
		for (var int = 0; int < listExcel.length; int++) {

			var listCells = listExcel[int].split('\t');
			var filaTexto = listExcel[int];
			if (filaTexto.trim().length < 1) {
				validado = "Existen filas vacias.";
				break;
			}

			if (validado.length < 1 && listCells.length != 4) {
				validado = "No coincide el numero de celdas. ("
						+ listCells.length+"!=4 )";
				break;
			} else {
				var recurs = {};
				for (var j = 0; j < listCells.length; j++) {
					var element = listCells[j];
					if (j === 0) {
						
						recurs.procedimientoSeguridadCodigo = element.trim();
					
						}
					if (j === 1) {
						recurs.procedimientoSeguridadNombre = element.trim();
						if ($.inArray(recurs.procedimientoSeguridadNombre, listProcRepe) === -1) {
							listProcRepe.push(recurs.procedimientoSeguridadNombre);
						} else {
							listFullNombres = listFullNombres + recurs.procedimientoSeguridadNombre + ", ";
						}

					}
					
					if (j === 2) {
						
						recurs.procedimientoSeguridadResumen = element.trim();
					
				}
					
					
					if (j === 3) {
						recurs.version = element.trim();
						
					}
					var hoy=new Date();
					recurs.horaPlanificada="10:00:00";
					recurs.sugeridoFecha=hoy;
					
					 var fFecha = Date.UTC(hoy.getUTCFullYear(), hoy.getUTCMonth(), hoy.getUTCDate())+(86400000*365); // 86400000 son los milisegundos que tiene un día

					recurs.sugeridoFechaProx=fFecha;
					recurs.idCompany = sessionStorage.getItem("gestopcompanyid");
				
				}

				listRecurso.push(recurs);
				if (listProcRepe.length < listRecurso.length) {
					validado = "Existen procedimientos repetidos." + listFullNombres;
					break;
				}
			}
		}
	} else {
		validado = "Ha excedido los 10000 caracteres permitidos.";
	}

	if (validado.length < 1) {
		var r = confirm("¿Está seguro que desea subir estos "
				+ listExcel.length + " elemento(s)?");
		if (r == true) {
			var dataParam = {
				listRecurso : listRecurso
			};
			console.log(listRecurso);
			callAjaxPost(URL + '/procedimientoseguridad/masivo/save', dataParam,
					function (data) {
				switch (data.CODE_RESPONSE) {
				case "05":
					
						alert("Se guardaron exitosamente.");
						cargarPrimerEstado();
						$('#mdImpDatos').modal('hide');
					
					break;
				default:
					alert("Ocurri&oacute; un error al guardar los proce!");
				}
			});
		}
	} else {
		alert(validado);
	}
}





