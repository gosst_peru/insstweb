var banderaEdicion;
var capacitacionId;
var capacitacionTipo;
var clasificacionId;
var listClasificacion;
var listCapacitacionTipo;
var listTiposVigencia;
var listPerfilIntertek = [];
var contador2 = 0;

var asigAux=[];
var vigAux=[];
var listFullFormaciones;
var listTrabajdores;
var listanombretab;
var capacitacionObj;
$(document).ready(function() {

	insertMenu();
	cargarPrimerEstado();
	resizeDivGosst("wrapper");
	resizeDivGosst("wrapperEval");
	$("#btnClipboardSmart").on("click",function(){
		var texto=obtenerDatosTablaEstandarNoFija("tblTrabFormacion");
	 copiarAlPortapapeles(texto,"btnClipboardSmart");
		alert("Se han guardado al clipboard la tabla " );

	
	});
	$(document).on("click","#btnClipTabla",function(){
		
				var fullDatosTabla="";
				 $(".fix-head #"+"tblCap"+" thead tr").each(function (index){   
			         $(this).children("th").each(function (index2){
			        	 if(index2<7){
			              fullDatosTabla =fullDatosTabla+ $(this).text()+"\t";
			        	 }
			         })
			         fullDatosTabla=fullDatosTabla+"\n"
			     });
				 
					for (index = 0; index < listFullFormaciones.length; index++) {
						fullDatosTabla=fullDatosTabla
						+listFullFormaciones[index].capacitacionNombre.replace("\t","") +"\t"
						+listFullFormaciones[index].capacitacionTipoNombre.replace("\t","") +"\t"
						+listFullFormaciones[index].clasificacionNombre.replace("\t","") +"\t"
						+listFullFormaciones[index].responsable.replace("\t","") +"\t"
						+listFullFormaciones[index].tema.replace("\t","") +"\t"
						+listFullFormaciones[index].horasProgramadas +"\t"
						+(listFullFormaciones[index].vigencia.id==null?
								"---":listFullFormaciones[index].vigencia.nombre)				
						+ "\n";			
				
					}
				

					 
					
					 copiarAlPortapapeles(fullDatosTabla); 
				
		
		
		alert("Se han guardado al clipboard la tabla de este módulo" );
		
	});
	$("#btnSmartProg").on("click",function(){
		listTrabajadoresPlanisSelecccionados=asigAux.filter(function(age){
			  return (vigAux.indexOf(age)==-1?true:false);
			});
	
	
		var r=true;
		r = confirm("Desea programar para mañana un evento de formación para "+
				listTrabajadoresPlanisSelecccionados.length+" trabajador(es)?");
		if (r) {
			var fechaManana=sumaFechaDias(1,obtenerFechaActual());
			var mes1 = fechaManana.substring(5, 7) - 1;
			var fechatemp1 = new Date(fechaManana.substring(0, 4), mes1, 
					fechaManana.substring(8, 10));

			var inputFecPla = fechatemp1;
			var dataParam = {
				programacionCapId : 0,
				fechaPlanificada : inputFecPla,
				fechaRealizada : null,
				inversionProgramacionCap:0,
				capacitacionId : capacitacionId,
				horaPlanificada:null,
				trabajadoresPlanificados: listTrabajadoresPlanisSelecccionados,
				progCapEstado:1,
				idCompany : sessionStorage.getItem("gestopcompanyid")
			};

			callAjaxPost(URL + '/capacitacion/programacion/save', dataParam,
					function(data) {
						switch (data.CODE_RESPONSE) {
						case "05":
							alert("Evento Creado!")
							$("#modalFormacionTrabajadores").modal("hide");
							break;
						default:
							console.log("Ocurrió un error al guardar la programacion!");
						}
					},function(){ 
						$("#mensajeGuardando").remove();
						$("#tblTrabFormacion").before("<p id='mensajeGuardando'>Guardando...</p>")},null);
			
		
		
		} 
	})
});

function cargarPrimerEstado() {
	banderaEdicion = false;
	capacitacionId = 0;
	capacitacionTipo = 0;
	clasificacionId = 0;
	
	removerBotones();
	crearBotones();
	
	$("#fsBotones")
			.append("<button id='btnProgFecha' type='button' class='btn btn-success' title='Programar Fecha'>"
							+ "<i class='fa fa-calendar fa-2x'></i>"
							+ "</button>");
	$("#fsBotones")
	.append("<button id='btnClipTabla' type='button' class='btn btn-success' title='Tabla Clipboard' >"
					+ "<i class='fa fa-clipboard fa-2x'></i>" + "</button>");
	$("#btnUpload").attr("onclick", "javascript:importarDatos();");
	
	deshabilitarBotonesEdicion();
	$("#btnProgFecha").hide();

	$("#btnNuevo").attr("onclick", "javascript:nuevoCapacitacion();");
	$("#btnCancelar").attr("onclick", "javascript:cancelarCapacitacion();");
	$("#btnGuardar").attr("onclick", "javascript:guardarCapacitacion();");
	$("#btnEliminar").attr("onclick", "javascript:eliminarCapacitacion();");
	$("#btnProgFecha").attr("onclick", "javascript:programarFecha();");
	$("#btnImportar").attr("onclick", "javascript:importarDatos();");
	var dataParam = {
		idCompany : getSession("gestopcompanyid"),
		perfilIntertek : {id : getSession("perfilIntertek")}
	};
	var dataParam2 = {
		idCompany : getSession("gestopcompanyid") 
	};
	callAjaxPost(URL + '/capacitacion', dataParam,
			procesarDataDescargadaPrimerEstado);
	callAjaxPost(URL + '/accidente/trab', dataParam2,
			function (data){
		switch (data.CODE_RESPONSE) {
			
			case "05":
			
				 listTrabajdores=data.listDniNombre;
				
				
			
				
				break;
			default:
				alert("Ocurrió un error al traer los trabajadores!");
			}

		listanombretab=listarStringsDesdeArray(listTrabajdores, "nombre");
		},function(){});
}

function nombreClasifCap(idTipo,nombreTipo){
	if(idTipo==4){
		return nombreTipo
	}else{
		return "Ninguna"
	}
}

function procesarDataDescargadaPrimerEstado(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
	    listFullFormaciones = data.list; 
		listClasificacion = data.listCla;
		listCapacitacionTipo = data.listCapTipo;
		listCapacitacionTipo=listCapacitacionTipo.filter(function(val,index){
			if(val.selCapId==5){
				return false;
			}else{
				return true;
			}
		});
		listTiposVigencia=data.listTiposVigencia;
		listPerfilIntertek = data.listPerfilIntertek;
		$("#h2Titulo").html("<a onclick='javascript:cargarPrimerEstado();' href='#'>Capacitaciones</a>");
		$("#tblCap tbody tr").remove();
		listFullFormaciones.forEach(function(val,index){
			if(val.capacitacionTipoId!=5){
			var listAprobadosReal=dividirLista(val.listTrabVigentesAprobados);
			$("#tblCap tbody").append(
					"<tr id='tr" + val.capacitacionId
							+ "' onclick='javascript:editarCapacitacion("
							+ index+")' >"

							+ "<td id='tdnom" + val.capacitacionId
							+ "'>" + val.capacitacionNombre + "</td>"
							+ "<td id='tdtipo" + val.capacitacionId
							+ "'>" + val.capacitacionTipoNombre
							+ "</td>" + "<td id='tdcla"
							+ val.capacitacionId + "'>"
							+ val.clasificacionNombre + "</td>"
							+ "</td>" + "<td id='tdperin"
							+ val.capacitacionId + "'>"
							+ val.perfilIntertek.nombre + "</td>"
							+ "<td id='tdres" + val.capacitacionId
							+ "'>" + val.responsable + "</td>"
							+ "<td id='tdtem" + val.capacitacionId
							+ "'>" + val.tema + "</td>"
							+ "<td id='tdhor" + val.capacitacionId
							+ "'>" + val.horasProgramadas + "</td>"
							+ "<td id='tdvig" + val.capacitacionId
							+ "'>" + val.vigencia.nombre + "</td>"
							+ "<td id='tdTrabAsig" + val.capacitacionId
							+ "'>" + listAprobadosReal.length + "/"+val.numTrabAsignados+"" +
									"</td>"
							+ "<td id='tdProg" + val.capacitacionId
							+ "'>" + val.numProgramacionesRealizadas + "/"+val.numProgramacionesPlaneadas + "</td>"
							+ "<td id='tdvid" + val.capacitacionId
							+ "'>" + val.urlVideo + "</td>" + "</tr>");
			} 
		});
		 
		formatoCeldaSombreableTabla(true,"tblCap");completarBarraCarga();
		
		if(getSession("linkCalendarioCapId")!=null){
			var indexIpercAux=0;
			listFullFormaciones.every(function(val,index3){
				
				if(val.capacitacionId==parseInt(getSession("linkCalendarioCapId"))){
					editarCapacitacion(index3);
					programarFecha();
					
					sessionStorage.removeItem("linkCalendarioCapId");
					return false;
				}else{
					return true;
				}
			});
			
		}
		break;
	default:
		alert("Ocurrió un error al traer las capacitaciones!");
	}
	
	
	if (contador2 == 0) {
		goheadfixed('table.fixed');
	}
	contador2 = contador2 + 1;
}
function verEstadoCapacitacionesTrabajadores(){
	var dataParam={capacitacionId:capacitacionId};
	callAjaxPost(
			URL + '/capacitacion/trabajador/formacion',
			dataParam,function(data){
				if(data.CODE_RESPONSE=="05"){
					var trabajadoresAsignados=data.trabajadoresAsignados;
					var trabajadoresVigentes=data.trabajadoresVigentes;
					var trabajadoresPlanificados=data.trabajadoresPlanificados;
					
					$("#modalFormacionTrabajadores").modal("show");
					$("#modalFormacionTrabajadores .modal-title").html("Resumen de capacitación: "+capacitacionObj.capacitacionNombre+" - "+capacitacionObj.tema);
					
					
					$("#tblTrabFormacion tbody tr").remove();
					vigAux=[];
					asigAux=[]; 
					for(index=0;index<trabajadoresAsignados.length;index++){
						if(trabajadoresAsignados[index].estadoPlanificacion!=1){
							asigAux.push(trabajadoresAsignados[index].trabajadorId);	
						}
						
						$("#tblTrabFormacion tbody").append("" +
								
					"<tr id='ft"+trabajadoresAsignados[index].trabajadorId+"'>" +
					"<td>"+trabajadoresAsignados[index].areaNombre+"</td>" +
					"<td>"+trabajadoresAsignados[index].trabajadorNombre+"</td>" +
					"<td>"+"---"+"</td>" +
					"<td>"+"---"+"</td>" +
					"</tr>")	;
						$("#ft"+trabajadoresAsignados[index].trabajadorId+" td").css({
							"background-color":"#C0504D"
						})
					}
					for(index=0;index<trabajadoresPlanificados.length;index++){
						if(trabajadoresPlanificados[index].fechaFutura.trim()!=""){
							vigAux.push(trabajadoresPlanificados[index].trabajadorId);
							$("#ft"+trabajadoresPlanificados[index].trabajadorId).remove();
							$("#tblTrabFormacion tbody").prepend("" +
									
									"<tr id='ft"+trabajadoresPlanificados[index].trabajadorId+"'>" +
									"<td>"+trabajadoresPlanificados[index].areaNombre+"</td>" +
									"<td>"+trabajadoresPlanificados[index].trabajadorNombre+"</td>" +
									"<td>"+trabajadoresPlanificados[index].fechaFutura+"</td>" +
									"<td>"+"----"+"</td>" +
									"</tr>");
							
							
							
							$("#ft"+trabajadoresPlanificados[index].trabajadorId+" td").css({
								"background-color":"#F79646"
							})
						}
						
					}
					for(index=0;index<trabajadoresVigentes.length;index++){
						vigAux.push(trabajadoresVigentes[index].trabajadorId);
						$("#ft"+trabajadoresVigentes[index].trabajadorId).remove()	;
						$("#tblTrabFormacion tbody").prepend("" +
								
								"<tr id='ft"+trabajadoresVigentes[index].trabajadorId+"'>" +
								"<td>"+trabajadoresVigentes[index].areaNombre+"</td>" +
								"<td>"+trabajadoresVigentes[index].trabajadorNombre+"</td>" +
								"<td>"+trabajadoresVigentes[index].fechaFutura+"</td>" +
								"<td>"+trabajadoresVigentes[index].diasRestantes+"</td>" +
								"</tr>");
						$("#ft"+trabajadoresVigentes[index].trabajadorId+" td").css({
							"background-color":"#9BBB59"
						})
					}
					
				}else{
					console.log("NOP")
				}
			})
}
function editarCapacitacion(pindex) {
	if (!banderaEdicion) {
		capacitacionObj=listFullFormaciones[pindex];
		capacitacionId = listFullFormaciones[pindex].capacitacionId ;
		capacitacionTipo = listFullFormaciones[pindex].capacitacionTipoId;
		clasificacionId = listFullFormaciones[pindex].clasificacionId;
		var vigenciaId=listFullFormaciones[pindex].vigencia.id;
		formatoCeldaSombreableTabla(false,"tblCap");
		var name = $("#tdnom" + capacitacionId).text();
		var iconoSmart=" <i class='fa fa-magic' aria-hidden='true'></i>";
		$("#tdTrabAsig"+capacitacionId).append(iconoSmart);
		$("#tdTrabAsig"+capacitacionId+" i").on("click",function(){
			verEstadoCapacitacionesTrabajadores()
		});
		$("#mdProgFechaLabel").html("Programaci&oacute;n de '"+name+"'");
		$("#tdnom" + capacitacionId)
				.html("<input type='text' id='inputCap' class='form-control' placeholder='Capacitacion'  value='"
								+ name + "'>");
		var name1 = $("#tdres" + capacitacionId).text();
		$("#tdres" + capacitacionId)
				.html("<input type='text' id='inputRes' class='form-control' placeholder='Responsable' value='"
								+ name1 + "'>");
		ponerListaSugerida("inputRes",listanombretab,true);
		var name2 = $("#tdtem" + capacitacionId).text();
		$("#tdtem" + capacitacionId)
				.html("<input type='text' id='inputTem' class='form-control' placeholder='Tema'  value='"
								+ name2 + "'>");
		var name3 = $("#tdhor" + capacitacionId).text();
		$("#tdhor" + capacitacionId)
				.html("<input type='number' id='inputHor' class='form-control' placeholder='Hora programada'  value='"
								+ name3 + "'>");
		var name4 = $("#tdvid" + capacitacionId).text();
		$("#tdvid" + capacitacionId)
				.html(
						"<input type='text' id='inputUrlVideo' class='form-control' placeholder='Url Video' value='"
								+ name4 + "'>");
		/** ************************************************************************* */
		var selCapTipo = crearSelectOneMenu("selCapTipo", "",
				listCapacitacionTipo, capacitacionTipo, "selCapId", "nombre");
		$("#tdtipo" + capacitacionId).html(selCapTipo);

		var selClaTipo = crearSelectOneMenu("selClaTipo", "",
				listClasificacion, clasificacionId, "clasificacionId", "nombre");
		$("#tdcla" + capacitacionId).html(selClaTipo);

		var selPerilIntertek = crearSelectOneMenu("selPerilIntertek", "",
			listPerfilIntertek, capacitacionObj.perfilIntertek.id, "id", "nombre");
	$("#tdperin" + capacitacionId).html(selPerilIntertek);

		var selTipoVig = crearSelectOneMenu("selTipoVig", "",
				listTiposVigencia, vigenciaId, "id", "nombre");
		$("#tdvig" + capacitacionId).html(selTipoVig);
		banderaEdicion = true;
		habilitarBotonesEdicion();
		$("#btnProgFecha").show();
		
		
		
	}
}

function nuevoCapacitacion() {
	if (!banderaEdicion) {
		var selCapTipo = crearSelectOneMenu("selCapTipo", "",
				listCapacitacionTipo, "-1", "selCapId", "nombre");
		var selClaTipo = crearSelectOneMenu("selClaTipo", "",
				listClasificacion, "-1", "clasificacionId", "nombre");
		var selTipoVig = crearSelectOneMenu("selTipoVig", "",
				listTiposVigencia, "-1", "id", "nombre");
		var selPerilIntertek = crearSelectOneMenu("selPerilIntertek", "",
			listPerfilIntertek, "", "id", "nombre");
		$("#tblCap tbody:first")
				.append(
						"<tr id='tr0'>"
								+ "<td><input type='text' id='inputCap' class='form-control' placeholder='Formaci&oacute;n' required='true' autofocus='true'></td>"
								+ "<td>"
								+ selCapTipo
								+ "</td>"
								+ "<td>"
								+ selClaTipo
								+ "</td>"
								+ "<td>"
								+ selPerilIntertek
								+ "</td>"
								+ "<td><input type='text' id='inputRes' class='form-control' placeholder='Responsable' required='true' autofocus='true'></td>"
								+ "<td><input type='text' id='inputTem' class='form-control' placeholder='Tema' required='true' autofocus='true'></td>"
								+ "<td><input type='number' id='inputHor' class='form-control' placeholder='Hora Programada' required='true'></td>"
								+ "<td>"
								+ selTipoVig
								+ "</td>"
								+ "<td>...</td>"
								+ "<td>...</td>"
								+ "<td><input type='text' id='inputUrlVideo' class='form-control' placeholder='Url Video' required='true'></td>"
								+ "</tr>");
		$("#inputCap").focus();
		formatoCeldaSombreableTabla(false,"tblCap");
		ponerListaSugerida("inputRes",listanombretab,true);
		capacitacionId = 0;
		habilitarBotonesNuevo();
		banderaEdicion = true;
	} else {
		alert("Guarde primero.");
	}
}

function cancelarCapacitacion() {
	cargarPrimerEstado();
}

function eliminarCapacitacion() {
	var r = confirm("¿Está seguro de eliminar la capacitacion?");
	if (r == true) {
		var dataParam = {
			capacitacionId : capacitacionId,
		};

		callAjaxPost(URL + '/capacitacion/delete', dataParam,
				procesarResultadoEliminarCapacitacion);
	}
}

function procesarResultadoEliminarCapacitacion(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		cargarPrimerEstado();
		break;
	default:
		alert("Ocurrió un error al eliminar el Capacitacion!");
	}
}

function guardarCapacitacion() {

	var campoVacio = true;
	var capacitacionTipo = $("#selCapTipo option:selected").val();
	var clasificacionTipo = $("#selClaTipo option:selected").val();
	var perfilIntertekId = $("#selPerilIntertek").val();
	var selTipoVig=$("#selTipoVig").val();
	var inputCap = $("#inputCap").val();
	var inputRes = $("#inputRes").val();
	var inputTem = $("#inputTem").val();
	var inputHor = $("#inputHor").val();
	var inputUrlVideo = $("#inputUrlVideo").val();

	if (capacitacionTipo == '-1' || clasificacionTipo == '-1'
			|| inputCap.length == 0 || inputRes.length == 0
			|| inputTem.length == 0 || inputHor.length == 0
			|| inputUrlVideo.legth == 0) {
		campoVacio = false;
	}
if(selTipoVig=="-1"){
	selTipoVig=null;
}
	if (campoVacio) {

		var dataParam = {
			capacitacionId : capacitacionId,
			perfilIntertek : {id : perfilIntertekId},
			clasificacionId : clasificacionTipo,
			capacitacionNombre : inputCap,
			responsable : inputRes,
			tema : inputTem,
			horasProgramadas : inputHor,
			capacitacionTipoId : capacitacionTipo,
			urlVideo : inputUrlVideo,
			vigencia:{id:selTipoVig},
			idCompany : sessionStorage.getItem("gestopcompanyid")
		};
var dataParamBefore="tr"+capacitacionId;
		callAjaxPost(URL + '/capacitacion/save', dataParam,
				procesarResultadoGuardarCap,loadingCelda,dataParamBefore);
	} else {
		alert("Debe ingresar todos los campos.");
	}
}

function procesarResultadoGuardarCap(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		cargarPrimerEstado();
		break;
	default:
		alert("Ocurrió un error al guardar la capacitacion!");
	}
}

function importarDatos() {
	$("#btnGuardarNuevos").attr("onclick",
			"javascript:guardarMasivoCopiaExcel();");

	$('#mdImpDatos').modal('show');
	
}
function guardarMasivoCopiaExcel() {
	var texto = $("#txtListado").val();
	var listExcel = texto.split('\n');
	var listRecurso = [];
	var listFormacionRepe = [];
	var listFullNombres = "";
	var validado = "";
	if (texto.length < 10000) {
		for (var int = 0; int < listExcel.length; int++) {

			var listCells = listExcel[int].split('\t');
			var filaTexto = listExcel[int];
			if (filaTexto.trim().length < 1) {
				validado = "Existen filas vacias.";
				break;
			}

			if (validado.length < 1 && listCells.length != 6) {
				validado = "No coincide el numero de celdas. ("
						+ listCells.length+"!=6 )";
				break;
			} else {
				var recurs = {};
				var tipo = {};
				var clas={};
				for (var j = 0; j < listCells.length; j++) {
					var element = listCells[j];
					if (j === 0) {
						recurs.capacitacionNombre = element.trim();
						if ($.inArray(recurs.capacitacionNombre, listFormacionRepe) === -1) {
							listFormacionRepe.push(recurs.capacitacionNombre);
						} else {
							listFullNombres = listFullNombres + recurs.capacitacionNombre + ", ";
						}

					}
					
					
					if (j === 1) {
						for (var k = 0; k < listCapacitacionTipo.length; k++) {
							if (listCapacitacionTipo[k].nombre == element.trim()) {
								recurs.capacitacionTipoId = listCapacitacionTipo[k].selCapId;
							}
						}

						if (!recurs.capacitacionTipoId) {
							validado = "Tipo de Formación no reconocido.";
							break;
						}
					}
					
					if (j === 2) {
						for (var k = 0; k < listClasificacion.length; k++) {
							if (listClasificacion[k].nombre == element.trim()) {
								recurs.clasificacionId = listClasificacion[k].clasificacionId;
							}
						}

						if (!recurs.clasificacionId) {
							validado = "Clasificación de Formación no reconocida.";
							break;
						}
					}
					if (j === 3) {
						recurs.responsable = element.trim();
					}
					if (j === 4) {
						recurs.tema = element.trim();
					}
					if (j === 5) {
						recurs.horasProgramadas = element.trim();
						if(isNaN(parseFloat(recurs.horasProgramadas ))){
							validado="Horas ingresadas no reconocidas"
						}
					}
					recurs.idCompany = sessionStorage.getItem("gestopcompanyid");
				
				}

				listRecurso.push(recurs);
				if (listFormacionRepe.length < listRecurso.length) {
					validado = "Existen fomaciones repetidos." + listFullNombres;
					break;
				}
			}
		}
	} else {
		validado = "Ha excedido los 10000 caracteres permitidos.";
	}

	if (validado.length < 1) {
		var r = confirm("¿Está seguro que desea subir estos "
				+ listExcel.length + " elemento(s)?");
		if (r == true) {
			var dataParam = {
				listRecurso : listRecurso
			};
			console.log(listRecurso);
			callAjaxPost(URL + '/capacitacion/masivo/save', dataParam,
					function (data) {
				switch (data.CODE_RESPONSE) {
				case "05":
					
						alert("Se guardaron exitosamente.");
						cargarPrimerEstado();
						$('#mdImpDatos').modal('hide');
					
					break;
				default:
					alert("Ocurri&oacute; un error al guardar el trabajador!");
				}
			});
		}
	} else {
		alert(validado);
	}
}
