/**
 * 
 */
var listaFullCursosCap;
var cursoCapacitacionId;
var banderaEdicionCurso;
var estadoCurso;
var cursoCapacitacionEdicion;
function programarCurso(){
	$("#mdCursoFecha").modal("show");
	$("#mdCursoFecha .modal-title").html(capacitacionNombre+" > Cursos");
	$(".modal-body").hide();
	$("#modalCurso").show();
	 
	$("#btnAgregarCurso").attr("onclick","nuevoCurso()");
	$("#btnCancelarCurso").attr("onclick","llamarCursosFormacion()");
	$("#btnEliminarCurso").attr("onclick","eliminarCurso()");
	$("#btnGuardarCurso").attr("onclick","guardarCurso()");
	llamarCursosFormacion();
}

function llamarCursosFormacion(){
	$("#btnCancelarCurso").hide();
	$("#btnGuardarCurso").hide();
	$("#btnEvaluarCurso").hide();
	$("#btnEliminarCurso").hide();
	$("#btnAgregarCurso").show();
	cursoCapacitacionId=0;
	banderaEdicionCurso=false;
	estadoCurso={id:1,nombre:"Por implementar"};
var dataParam={capacitacionId:capacitacionId};
	callAjaxPost(URL + '/capacitacion/cursos', dataParam, function(data) {
		
			var list = data.list;
			listaFullCursosCap=data.list;
			$("#tblCurso tbody").html("");
			listaFullCursosCap.forEach(function(val,index){
				$("#tblCurso tbody").append("<tr onclick='javascript:editarCurso("+
						index+")' >"+
						"<td id='curNom"+
						list[index].id + "'>"+
						list[index].nombre + "</td>" +
						"<td id='curDesc"+
						list[index].id + "'>"+
						list[index].descripcion + "</td>" +
						"<td id='curObj"+
						list[index].id + "'>"+
						list[index].numObjetivos + "</td>" +
						"<td id='curFechaInicio"+
						list[index].id + "'>"+
						list[index].fechaInicioTexto + "</td>" +
						"<td id='curFechaFin"+
						list[index].id + "'>"+
						list[index].fechaFinTexto + "</td>" +
						"<td id='curEvi"+
						list[index].id + "'>"+
						list[index].evidenciaNombre + "</td>" +
						"<td id='curArch"+
						list[index].id + "'>"+
						list[index].numArchivos + "</td>" +
						"<td id='curEnl"+
						list[index].id + "'>"+
						list[index].numEnlaces + "</td>" +
						"<td id='curPre"+
						list[index].id + "'>"+
						list[index].preguntas.length + "</td>" +
						"<td id='curTiem"+
						list[index].id + "'>"+
						list[index].tiempoSolucion + "</td>" +
						"<td id='curPunt"+
						list[index].id + "'>"+
						list[index].puntajeMinimo + "</td>" +
						"<td id='curAprob"+
						list[index].id + "'>"+
						list[index].numAprobados+" / "+list[index].numEvaluado + "</td>" +
						"<td id='curEst"+
						list[index].id + "'>"+
						list[index].estado.nombre + "</td>" +
						"</tr>")
			});
			if(getSession("linkCalendarioProgCursoId")!=null){
				var indexIpercAux=0;
				listaFullCursosCap.every(function(val,index3){
					
					if(val.id==parseInt(getSession("linkCalendarioProgCursoId"))){
						
						editarCurso(index3);
						sessionStorage.removeItem("linkCalendarioProgCursoId");
						return false;
					}else{
						return true;
					}
				});
				
			}
			formatoCeldaSombreableTabla(true,"tblCurso");
		
	});
}


function editarCurso(pindex){
	if(!banderaEdicionCurso){
		banderaEdicionCurso=true;
		$("#btnCancelarCurso").show();
		$("#btnGuardarCurso").show();
		$("#btnEvaluarCurso").show();
		$("#btnEliminarCurso").show();
		$("#btnAgregarCurso").hide();
		formatoCeldaSombreableTabla(false,"tblCurso");
	cursoCapacitacionId=listaFullCursosCap[pindex].id;
	cursoCapacitacionEdicion=listaFullCursosCap[pindex];
	var nombre=listaFullCursosCap[pindex].nombre;
	var desc= listaFullCursosCap[pindex].descripcion;
	var textObj=$("#curObj"+cursoCapacitacionId).text();
	var textArch=$("#curArch"+cursoCapacitacionId).text();
	var textEnl=$("#curEnl"+cursoCapacitacionId).text();
	var textPre=$("#curPre"+cursoCapacitacionId).text();
	var textAprob=$("#curAprob"+cursoCapacitacionId).text();
	var nombreEvidencia=$("#curEvi"+cursoCapacitacionId).text();
	var fechaI=convertirFechaInput(listaFullCursosCap[pindex].fechaInicio);
	var fechaF=convertirFechaInput(listaFullCursosCap[pindex].fechaFin);
	var tiempo= listaFullCursosCap[pindex].tiempoSolucion;
	var puntaje= listaFullCursosCap[pindex].puntajeMinimo;
	
	$("#curNom"+cursoCapacitacionId).html("<input class='form-control' id='inputCurNom'>");
	$("#curDesc"+cursoCapacitacionId).html("<input class='form-control' id='inputCurDesc'>");
	$("#curObj"+cursoCapacitacionId).addClass("linkGosst")
	.on("click",function(){verObjetivosCurso();})
	.html("<i class='fa fa-star fa-2x' aria-hidden='true' ></i>"+textObj);
	
	$("#curFechaInicio"+cursoCapacitacionId).html("<input class='form-control' onchange='hallarEstadoCurso("+cursoCapacitacionId+")'  id='inputCurFechaInicio' type='date'>")
	$("#curFechaFin"+cursoCapacitacionId).html("<input class='form-control' onchange='hallarEstadoCurso("+cursoCapacitacionId+")'  id='inputCurFechaFin' type='date'>")

	$("#curEvi" + cursoCapacitacionId)
	.html(
			"<a id='linkEvidencia' " +
			"href='"+URL 
			+ "/capacitacion/programacion/evidencia?programacionCapId="+cursoCapacitacionId+"' target='_blank' >" +
			nombreEvidencia+	"</a><br/>" +
			"<a id='linkEvidencia' href='#' onclick='javascript:editarEvidenciaCurso();'>Subir</a>");
	
	$("#curArch"+cursoCapacitacionId).addClass("linkGosst")
	.on("click",function(){verArchivosCurso();})
	.html("<i class='fa fa-file-text fa-2x' aria-hidden='true' ></i>"+textArch);
	$("#curEnl"+cursoCapacitacionId).addClass("linkGosst")
	.on("click",function(){verEnlacesCurso();})
	.html("<i class='fa fa-external-link fa-2x' aria-hidden='true' ></i>"+textEnl);
	$("#curPre"+cursoCapacitacionId).addClass("linkGosst")
	.on("click",function(){verPreguntasCurso();})
	.html("<i class='fa fa-question-circle fa-2x' aria-hidden='true' ></i>"+textPre);
	$("#curAprob"+cursoCapacitacionId).addClass("linkGosst")
	.on("click",function(){verEvaluacionCurso();})
	.html("<i class='fa fa-check-square fa-2x' aria-hidden='true' ></i>"+textAprob);
	
	
	$("#curTiem"+cursoCapacitacionId).html("<input class='form-control' id='inputCurTiem' type='number'>");
	$("#curPunt"+cursoCapacitacionId).html("<input class='form-control' id='inputCurPunt' type='number' onkeyup='return comprobarPorcentaje(event)'>");
	
	$("#inputCurNom").val(nombre);
	$("#inputCurDesc").val(desc);
	$("#inputCurFechaInicio").val(fechaI);
	$("#inputCurFechaFin").val(fechaF);
	$("#inputCurTiem").val(tiempo);
	$("#inputCurPunt").val(puntaje);
	hallarEstadoCurso();
	}
}


function nuevoCurso() {
	if (!banderaEdicionCurso) {
		$("#btnCancelarCurso").show();
		$("#btnGuardarCurso").show();
		$("#btnEvaluarCurso").hide();
		$("#btnEliminarCurso").hide();
		$("#btnAgregarCurso").hide();
		cursoCapacitacionId = 0;
		$("#tblCurso tbody")
				.append(
						"<tr id='tr0'>"
						+"<td><input class='form-control' id='inputCurNom'></td>"
						+"<td><input class='form-control' id='inputCurDesc'></td>"
						+"<td>...</td>"
						+"<td><input class='form-control' id='inputCurFechaInicio'  onchange='hallarEstadoCurso("+cursoCapacitacionId+")'  type='date'></td>"
						+"<td><input class='form-control' id='inputCurFechaFin'  onchange='hallarEstadoCurso("+cursoCapacitacionId+")'  type='date'></td>"
						+"<td>...</td>"
						+"<td>...</td>"
						+"<td>...</td>"
						+"<td>...</td>"
						+"<td><input class='form-control' id='inputCurTiem' type='number'></td>"
						+"<td><input class='form-control' id='inputCurPunt' type='number' onkeyup='return comprobarPorcentaje(event)'></td>"
						+"<td id='curEst0'></td>"
						+""
								+ "</tr>");
		formatoCeldaSombreableTabla(false,"tblCurso");
		banderaEdicionCurso = true;
	} else {
		alert("Guarde primero.");
	}
}

function guardarCurso(){
	var nombre=$("#inputCurNom").val();
	var descripcion=$("#inputCurDesc").val();
	var fechaInicio=convertirFechaTexto($("#inputCurFechaInicio").val());
	var fechaFin=convertirFechaTexto($("#inputCurFechaFin").val());
	var tiempo=parseFloat($("#inputCurTiem").val());
	var puntaje=parseFloat($("#inputCurPunt").val());
	
	var cursoObj={
			id:cursoCapacitacionId,
			nombre:nombre,
			descripcion:descripcion,
			fechaInicio:fechaInicio,
			fechaFin:fechaFin,
			tiempoSolucion:tiempo,
			puntajeMinimo:puntaje,
			estado:estadoCurso,
			capacitacionId:capacitacionId
	}
	console.log(cursoObj);
	callAjaxPost(URL + '/capacitacion/curso/save', cursoObj,
			function(data) {
				
				
			llamarCursosFormacion();
				
			},loadingCelda,"tblCurso #tr"+cursoCapacitacionId);
	
	
	
	
	
	
	
	
}

function eliminarCurso(){
	var cursoObj={
			id:cursoCapacitacionId
	}
	var r=confirm("¿Está seguro de eliminar este curso?");
	if(r){
		callAjaxPost(URL + '/capacitacion/curso/delete', cursoObj,
				function(data) {
					
					
				llamarCursosFormacion();
					
				});
	}
	
}
function editarEvidenciaCurso(){
	$(".modal-body").hide();
	$("#modalUploadCurso").show();
}
function cancelarUploadEvidenciaCurso(){
	$(".modal-body").hide();
	$("#modalCurso").show();
}
function uploadEvidenciaCurso(){
	var inputFileImage = document.getElementById("fileEviCurso");
	var file = inputFileImage.files[0];
	var data = new FormData();

	if(file.size>bitsEvidenciaCurso){
		alert("Lo sentimos, solo se pueden subir archivos menores a "+bitsEvidenciaCurso/1000000+" MB");	
	}else{
		data.append("fileEvi", file);
		data.append("programacionCapId", cursoCapacitacionId);
		
		var url = URL + '/capacitacion/programacion/evidencia/save';
		$.blockUI({message:'cargando...'});
		$.ajax({
			url : url,
			xhrFields: {
	            withCredentials: true
	        },
			type : 'POST',
			contentType : false,
			data : data,
			processData : false,
			cache : false,
			success : function(data, textStatus, jqXHR) {
				switch (data.CODE_RESPONSE) {
				case "06":
					sessionStorage.clear();
					document.location.replace(data.PATH);
					break;
				default:
					console.log('Se subio el archivo correctamente.');
				$.unblockUI();
				$(".modal-body").hide();
				$("#modalCurso").show();
				}
				

			},
			error : function(jqXHR, textStatus, errorThrown) {
				alert("Ups, tenemos problemas tecnicos. Intente más tarde. Error: "
						+ errorThrown);
				console.log('xhRequest: ' + jqXHR + "\n");
				console.log('ErrorText: ' + textStatus + "\n");
				console.log('thrownError: ' + errorThrown + "\n");
				$.unblockUI();
			}
		});
	}
		
}
function verArchivosCurso(){
	$(".modal-body").hide();
	$("#modalArchivosCurso").show();
	$("#mdCursoFecha .modal-title").html("<a onclick='volverCursoCapacitacion()'>Curso '"+cursoCapacitacionEdicion.nombre+"'</a> > Archivos");
	
	$("#btnAgregarArchivo").attr("onclick","nuevoArchivo()");
	$("#btnCancelarArchivo").attr("onclick","llamarArchivosCurso()");
	$("#btnEliminarArchivo").attr("onclick","eliminarArchivo()");
	$("#btnGuardarArchivo").attr("onclick","guardarArchivo()");
	llamarArchivosCurso();
}
function verEnlacesCurso(){
	$(".modal-body").hide();
	$("#modalEnlacesCurso").show();
	$("#mdCursoFecha .modal-title").html("<a onclick='volverCursoCapacitacion()'>Curso '"+cursoCapacitacionEdicion.nombre+"'</a> > Enlaces");
	
	$("#btnAgregarEnlace").attr("onclick","nuevoEnlace()");
	$("#btnCancelarEnlace").attr("onclick","llamarEnlacesCurso()");
	$("#btnEliminarEnlace").attr("onclick","eliminarEnlace()");
	$("#btnGuardarEnlace").attr("onclick","guardarEnlace()");
	llamarEnlacesCurso();
}

function verPreguntasCurso(){
	$(".modal-body").hide();
	$("#modalPreguntasCurso").show();
	$("#mdCursoFecha .modal-title").html("<a onclick='volverCursoCapacitacion()'>Curso '"+cursoCapacitacionEdicion.nombre+"'</a> > Preguntas");
	
	$("#btnAgregarPregunta").attr("onclick","nuevoPregunta()");
	$("#btnCancelarPregunta").attr("onclick","llamarPreguntasCurso()");
	$("#btnEliminarPregunta").attr("onclick","eliminarPregunta()");
	$("#btnGuardarPregunta").attr("onclick","guardarPregunta()");
	llamarPreguntasCurso();
}

function verObjetivosCurso(){
	$(".modal-body").hide();
	$("#modalObjetivosCurso").show();
	$("#mdCursoFecha .modal-title").html("<a onclick='volverCursoCapacitacion()'>Curso '"+cursoCapacitacionEdicion.nombre+"'</a> > Objetivos");
	
	$("#btnAgregarObjetivo").attr("onclick","nuevoObjetivo()");
	$("#btnCancelarObjetivo").attr("onclick","llamarObjetivosCurso()");
	$("#btnEliminarObjetivo").attr("onclick","eliminarObjetivo()");
	$("#btnGuardarObjetivo").attr("onclick","guardarObjetivo()");
	llamarObjetivosCurso();
}

function volverCursoCapacitacion(){
	$(".modal-body").hide();
	$("#modalCurso").show();
	$("#mdCursoFecha .modal-title").html(capacitacionNombre+" > Cursos");
}

function verEvaluacionCurso(){
	
	$(".modal-body").hide();
	$("#modalEvaluadosCurso").show();
	$("#mdCursoFecha .modal-title").html("<a onclick='volverCursoCapacitacion()'>Curso '"+cursoCapacitacionEdicion.nombre+"'</a> > Evaluaciones");
	var dataParam={id:cursoCapacitacionId}
	callAjaxPost(URL + '/capacitacion/curso/trabajadores/evaluacion', dataParam, function(data) {
		
		var list = data.list; 
		$("#tblEvalsCurso tbody").html("");
		list.forEach(function(val,index){
			var certificadoNombre="";
			if(val.evaluacionId==1){
				certificadoNombre=
					"<i onclick='javascript:descargarCertificadoCurso("+cursoCapacitacionId+","+val.trabajador.trabajadorId+")' " +
					"class='fa fa-certificate fa-2x aprobado-gosst' " +
					"title='Descargar Certificado'></i>" ;
			}
			$("#tblEvalsCurso tbody").append("<tr  >"+
					"<td>"+val.trabajador.puesto.matrixName + "</td>" +
					"<td>"+val.trabajador.puesto.divisionName  + "</td>" +
					"<td>"+val.trabajador.puesto.areaName  + "</td>" +
					"<td>"+val.trabajador.puesto.nombre + "</td>" +
					"<td>"+val.trabajador.trabajadorNombre + "</td>" +
					"<td>"+val.fechaRealizadaTexto + "</td>" +
					"<td>"+val.evaluacionNombre + "</td>" +
					"<td>"+val.puntajeAlcanzadoTexto + "</td>" +
					"<td>"+certificadoNombre + "</td>" +
				"</tr>")
		});
	
		formatoCeldaSombreableTabla(true,"tblObjCurso");
	
});
}


function hallarEstadoCurso(progId){
	var fechaPlanificada=$("#inputCurFechaInicio").val();
	var fechaReal=$("#inputCurFechaFin").val();
var fechaHoy=obtenerFechaActual();
	
			var dif=restaFechas(fechaHoy,fechaReal)
			if(dif>=0){
				estadoCurso={
					id:1,
					nombre: "Por Implementar"
				}
			}else{
				estadoCurso={
						id:2,
						nombre: "Completado"
					}
			}
			
		
	
	$("#curEst"+progId).html(estadoCurso.nombre);
	
}








