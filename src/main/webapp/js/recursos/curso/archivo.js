/**
 * 
 */
var archivoCursoId;
var banderaEdicionArch;
var listFullArchivos;
$(document).on('change', ':file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
    $("#btnDescargarFileCurso").hide();
  });
 $(document).on('fileselect',"input:file", function(event, numFiles, label) {
	 
	          var input = $(this).parents('.input-group').find(':text'),
	              log = numFiles > 1 ? numFiles + ' files selected' : label;

	          if( input.length ) {
	              input.val(log);
	          } else {
	              if( log ) console.log(log);
	          }

	      });
function llamarArchivosCurso(){
	archivoCursoId=0;
	banderaEdicionArch=false;
	listFullArchivos=[];
	$("#btnAgregarArchivo").show();
	$("#btnCancelarArchivo").hide();
	$("#btnEliminarArchivo").hide();
	$("#btnGuardarArchivo").hide();
	var dataParam={id:cursoCapacitacionId}
	callAjaxPost(URL + '/capacitacion/curso/archivos', dataParam, function(data) {
		
		var list = data.list;
		listFullArchivos=data.list;
		$("#tblArchCurso tbody").html("");
		listFullArchivos.forEach(function(val,index){
			$("#tblArchCurso tbody").append("<tr onclick='javascript:editarArchivo("+
					index+")' >"+
					"<td id='archFile"+
					list[index].id + "'>"+
					(list[index].nombre) + "</td>" +
					"<td id='archObs"+
					list[index].id + "'>"+
					list[index].observacion + "</td>" +
					"</tr>")
		});
	
		formatoCeldaSombreableTabla(true,"tblArchCurso");
	
});
	
}
function editarArchivo(pindex){
	if(!banderaEdicionArch){
		banderaEdicionArch=true;
		$("#btnAgregarArchivo").hide();
		$("#btnCancelarArchivo").show();
		$("#btnEliminarArchivo").show();
		$("#btnGuardarArchivo").show();
		formatoCeldaSombreableTabla(false,"tblArchCurso");
		archivoCursoId=listFullArchivos[pindex].id;
		var observacion=listFullArchivos[pindex].observacion;
		var inputEvi="<button class='btn btn-success' style='float:left' id='btnDescargarFileCurso'>" +
		"<i class='fa fa-download' aria-hidden='true'></i>Descargar</button><div class='input-group'>" +
			"<label class='input-group-btn'>"+
             "<span class='btn btn-primary'>"+
              "<i class='fa fa-upload fa-lg'></i>   Subir Nuevo&hellip; <input style='display: none;'"+ 
               "type='file' name='fileEviArchivo' id='fileEviArchivo' accept='image/jpg,image/png,image/jpeg,image/gif'>"+
                "</span>"+
                "</label>"+
                "<input type='text' id='inputEviText' class='form-control' readonly>"+
       		 	"</div>";
		var nombre=listFullArchivos[pindex].nombre;
		$("#archObs"+archivoCursoId).html("<input class='form-control' id='inputArchObs'>");
		$("#archFile"+archivoCursoId).html(inputEvi);
		$("#btnDescargarFileCurso").attr("onclick","location.href='"+ URL
				+ "/capacitacion/curso/archivo/evidencia?archivoId="
				+ archivoCursoId+"'");
	$("#inputArchObs").val(observacion);
	$("#inputEviText").val(nombre);
	}
}
function nuevoArchivo(){
	$("#btnAgregarArchivo").hide();
	$("#btnCancelarArchivo").show();
	$("#btnEliminarArchivo").hide();
	$("#btnGuardarArchivo").show();
	banderaEdicionArch=true;
	archivoCursoId = 0;
	var inputEvi="<div class='input-group'>" +
				"<label class='input-group-btn'>"+
	             "<span class='btn btn-primary'>"+
	              "<i class='fa fa-upload fa-lg'></i>   Subir Nuevo&hellip; <input style='display: none;'"+ 
	               "type='file' name='fileEviArchivo' id='fileEviArchivo' accept='image/jpg,image/png,image/jpeg,image/gif'>"+
	                "</span>"+
	                "</label>"+
	                "<input type='text' id='inputEviText' class='form-control' readonly>"+
           		 	"</div>";
	$("#tblArchCurso tbody")
			.append(
					"<tr id='tr0'>"
					+"<td>"+inputEvi+"</td>"
					+"<td><input class='form-control' id='inputArchObs'></td>"
					+ "</tr>");
	formatoCeldaSombreableTabla(false,"tblArchCurso");
}

function eliminarArchivo(){
	var ArchivoObj={
			id:archivoCursoId
	}
	var r=confirm("¿Está seguro de eliminar este Archivo?");
	if(r){
		callAjaxPost(URL + '/capacitacion/curso/archivo/delete', ArchivoObj,
				function(data) {
					
					
			llamarArchivosCurso();
					
				});
	}
	
}

function guardarArchivo(){
	var observacion=$("#inputArchObs").val();
	var campoVacio=false;
	if(observacion.length==0){
		campoVacio=true;
	}
	
	if(campoVacio){
		alert("Complete el campo")
	}else{
		var ArchivoObj={
				id:archivoCursoId,
				observacion:observacion,
				cursoCapacitacionId:cursoCapacitacionId
		}
		
		callAjaxPostNoUnlock(URL + '/capacitacion/curso/archivo/save', ArchivoObj,
				procesarResultadoGuardarArchivoCurso);
	}
	
	
}


function procesarResultadoGuardarArchivoCurso(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		var archivoIdAux=data.idNuevo;
		var inputFileImage = document.getElementById("fileEviArchivo");
		var file = inputFileImage.files[0];
		var data = new FormData();
		
		if(file){
			if (file.size > bitsEvidenciaArchivoCurso) {
				alert("Lo sentimos, solo se pueden subir archivos menores a "+bitsEvidenciaArchivoCurso/1000000+" MB");
				llamarArchivosCurso();
			} else {
				data.append("fileEvi", file);
				data.append("archivoId", archivoIdAux);
			
				var url = URL + '/capacitacion/curso/archivo/evidencia/save';
				 
				$.ajax({
							url : url,
							xhrFields: {
					            withCredentials: true
					        },
							type : 'POST',
							contentType : false,
							data : data,
							processData : false,
							cache : false,
							success : function(data, textStatus, jqXHR) {
							 
								switch (data.CODE_RESPONSE) {
								case "06":
									sessionStorage.clear();
									document.location.replace(data.PATH);
									break;
								default:
								 
									llamarArchivosCurso();
								}
	
							},
							error : function(jqXHR, textStatus, errorThrown) {
								$.unblockUI();
								alert("Ups, tenemos problemas tecnicos. Intente más tarde. Error: "
										+ errorThrown);
								console.log('xhRequest: ' + jqXHR + "\n");
								console.log('ErrorText: ' + textStatus + "\n");
								console.log('thrownError: ' + errorThrown + "\n");
							}
						});
			}
		
		}else{
			llamarArchivosCurso();
		}
		
		
		break;
	default:
		alert("Ocurrió un error al guardar el acto inseguro!");
	}
}

