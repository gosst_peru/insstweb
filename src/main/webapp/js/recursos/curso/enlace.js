/**
 * 
 */
var EnlaceCursoId;
var banderaEdicionObj;
var listFullEnlaces;
function llamarEnlacesCurso(){
	EnlaceCursoId=0;
	banderaEdicionObj=false;
	listFullEnlaces=[];
	$("#btnAgregarEnlace").show();
	$("#btnCancelarEnlace").hide();
	$("#btnEliminarEnlace").hide();
	$("#btnGuardarEnlace").hide();
	var dataParam={id:cursoCapacitacionId}
	callAjaxPost(URL + '/capacitacion/curso/enlaces', dataParam, function(data) {
		
		var list = data.list;
		listFullEnlaces=data.list;
		$("#tblEnlCurso tbody").html("");
		listFullEnlaces.forEach(function(val,index){
			$("#tblEnlCurso tbody").append("<tr onclick='javascript:editarEnlace("+
					index+")' >"+
					"<td id='enlNom"+
					list[index].id + "'>"+
					list[index].nombre + "</td>" +
					"<td id='enlObs"+
					list[index].id + "'>"+
					list[index].observacion + "</td>" +
					"</tr>")
		});
	
		formatoCeldaSombreableTabla(true,"tblEnlCurso");
	
});
	
}
function editarEnlace(pindex){
	if(!banderaEdicionObj){
		banderaEdicionObj=true;
		$("#btnAgregarEnlace").hide();
		$("#btnCancelarEnlace").show();
		$("#btnEliminarEnlace").show();
		$("#btnGuardarEnlace").show();
		formatoCeldaSombreableTabla(false,"tblEnlCurso");
		EnlaceCursoId=listFullEnlaces[pindex].id;
		var nombre=listFullEnlaces[pindex].nombre;
		var observacion=listFullEnlaces[pindex].observacion;

		$("#enlNom"+EnlaceCursoId).html("<input class='form-control' id='inputEnlNom'>");
		$("#inputEnlNom").val(nombre);
		$("#enlObs"+EnlaceCursoId).html("<input class='form-control' id='inputEnlObs'>");
		$("#inputEnlObs").val(observacion);}
}
function nuevoEnlace(){
	$("#btnAgregarEnlace").hide();
	$("#btnCancelarEnlace").show();
	$("#btnEliminarEnlace").hide();
	$("#btnGuardarEnlace").show();
	banderaEdicionObj=true;
	
	EnlaceCursoId = 0;
	$("#tblEnlCurso tbody")
			.append(
					"<tr id='tr0'>"
					+"<td><input class='form-control' id='inputEnlNom'></td>" +
							"<td><input class='form-control' id='inputEnlObs'></td>"
							+ "</tr>");
	formatoCeldaSombreableTabla(false,"tblEnlCurso");
}

function eliminarEnlace(){
	var EnlaceObj={
			id:EnlaceCursoId
	}
	var r=confirm("¿Está seguro de eliminar este Enlace?");
	if(r){
		callAjaxPost(URL + '/capacitacion/curso/enlace/delete', EnlaceObj,
				function(data) {
					
					
			llamarEnlacesCurso();
					
				});
	}
	
}

function guardarEnlace(){
	var nombre=$("#inputEnlNom").val();
	var observacion=$("#inputEnlObs").val();
var campoVacio=false;
	if(nombre.length==0){
		campoVacio=true;
	}
	
	if(campoVacio){
		alert("Complete el campo")
	}else{
		var EnlaceObj={
				id:EnlaceCursoId,
				nombre:nombre,
				observacion:observacion,
				cursoCapacitacionId:cursoCapacitacionId
		}
		
		callAjaxPost(URL + '/capacitacion/curso/enlace/save', EnlaceObj,
				function(data) {
					
					
			llamarEnlacesCurso();
					
				});
	}
	
	
	
	
	
	
	
	
}




