/**
 * 
 */
var PreguntaCursoId;
var banderaEdicionObj;
var listFullPreguntas;
var preguntaCursoEdicion;
function llamarPreguntasCurso(){
	PreguntaCursoId=0;
	banderaEdicionObj=false;
	listFullPreguntas=[];
	$("#btnAgregarPregunta").show();
	$("#btnCancelarPregunta").hide();
	$("#btnEliminarPregunta").hide();
	$("#btnGuardarPregunta").hide();
	$("#btnImportarPreguntas").attr("onclick","importarPreguntasCurso()")
	var dataParam={id:cursoCapacitacionId}
	callAjaxPost(URL + '/capacitacion/curso/preguntas', dataParam, function(data) {
		
		var list = data.list;
		listFullPreguntas=data.list;console.log(listFullPreguntas);
		$("#tblPregCurso tbody").html("");
		listFullPreguntas.forEach(function(val,index){
			$("#tblPregCurso tbody").append("<tr id='trPreg"+list[index].id+"' onclick='javascript:editarPregunta("+
					index+")' >"+
					"<td id='preNom"+
					list[index].id + "'>"+
					list[index].nombre + "</td>" +
					"<td id='preNumOpc"+
					list[index].id + "'>"+
					list[index].numOpciones + "</td>" +
					"<td id='preJust"+
					list[index].id + "'>"+
					list[index].justificacion + "</td>" +
					"<td id='preImg"+
					list[index].id + "'>"+
					list[index].imagenNombre + "</td>" +
					"</tr>")
		});
	
		formatoCeldaSombreableTabla(true,"tblPregCurso");
	
});
	
}
function editarPregunta(pindex){
	if(!banderaEdicionObj){
		banderaEdicionObj=true;
		$("#btnAgregarPregunta").hide();
		$("#btnCancelarPregunta").show();
		$("#btnEliminarPregunta").show();
		$("#btnGuardarPregunta").show();
		formatoCeldaSombreableTabla(false,"tblPregCurso");
		PreguntaCursoId=listFullPreguntas[pindex].id;
		preguntaCursoEdicion=listFullPreguntas[pindex];
		var nombre=listFullPreguntas[pindex].nombre;
		var justificacion=listFullPreguntas[pindex].justificacion;
		var textOpc=$("#preNumOpc"+PreguntaCursoId).text();
		var imagenNombre=listFullPreguntas[pindex].imagenNombre;
		var inputEvi="<a target='_blank'  class='btn btn-success' style='float:left; width: 100%;"+
			"margin-bottom: 5px;' id='btnDescargarImagenCurso'>" +
		"<i class='fa fa-download' aria-hidden='true'></i>Descargar</a><div class='input-group'>" +
			"<label class='input-group-btn'>"+
             "<span class='btn btn-primary'>"+
              "<i class='fa fa-upload fa-lg'></i>   Subir Nuevo&hellip; <input style='display: none;'"+ 
               "type='file' name='fileEviImagen' id='fileEviImagen' accept='image/jpg,image/png,image/jpeg,image/gif'>"+
                "</span>"+
                "</label>"+
                "<input type='text' id='inputEviImagen' class='form-control' readonly>"+
       		 	"</div>";
		$("#preNumOpc"+PreguntaCursoId).addClass("linkGosst")
		.on("click",function(){verOpcionesPregunta();})
		.html("<i class='fa fa-list-ul fa-2x' aria-hidden='true' ></i>"+textOpc);
		
		$("#preNom"+PreguntaCursoId).html("<input class='form-control' id='inputPreNom'>");
		$("#inputPreNom").val(nombre);
		$("#preJust"+PreguntaCursoId).html("<input class='form-control' id='inputPreJust'>");
		$("#inputPreJust").val(justificacion);
		
		$("#preImg"+PreguntaCursoId).html(inputEvi);
		$("#btnDescargarImagenCurso").attr("href", URL
				+ "/capacitacion/curso/pregunta/imagen?preguntaId="
				+ PreguntaCursoId+" ");
		$("#inputEviImagen").val(imagenNombre);
	}
}
function verOpcionesPregunta(){
	$(".modal-body").hide();
	$("#modalOpcionsCurso").show();
	$("#mdCursoFecha .modal-title").html("<a onclick='volverCursoCapacitacion()'>Curso '"+cursoCapacitacionEdicion.nombre+"'</a> " +
			"> <a onclick='volverPreguntasCurso()'>Curso '"+preguntaCursoEdicion.nombre+"'</a> >Opciones ");
	
	$("#btnAgregarOpcion").attr("onclick","nuevoOpcion()");
	$("#btnCancelarOpcion").attr("onclick","llamarOpcionesPregunta()");
	$("#btnEliminarOpcion").attr("onclick","eliminarOpcion()");
	$("#btnGuardarOpcion").attr("onclick","guardarOpcion()");
	llamarOpcionesPregunta();
}
function volverPreguntasCurso(){
	$(".modal-body").hide();
	$("#modalPreguntasCurso").show();
	$("#mdCursoFecha .modal-title").html("<a onclick='volverCursoCapacitacion()'>Curso '"+cursoCapacitacionEdicion.nombre+"'</a> > Preguntas");
	
}
function nuevoPregunta(){
	$("#btnAgregarPregunta").hide();
	$("#btnCancelarPregunta").show();
	$("#btnEliminarPregunta").hide();
	$("#btnGuardarPregunta").show();
	banderaEdicionObj=true;
	var inputEvi="<div class='input-group'>" +
	"<label class='input-group-btn'>"+
     "<span class='btn btn-primary'>"+
      "<i class='fa fa-upload fa-lg'></i>   Subir Nuevo&hellip; <input style='display: none;'"+ 
       "type='file' name='fileEviImagen' id='fileEviImagen' accept='image/jpg,image/png,image/jpeg,image/gif'>"+
        "</span>"+
        "</label>"+
        "<input type='text' id='inputEviImagen' class='form-control' readonly>"+
		 	"</div>";
	PreguntaCursoId = 0;
	$("#tblPregCurso tbody")
			.append(
					"<tr id='trPreg0'>"
					+"<td><input class='form-control' id='inputPreNom'></td>"
					+"<td>...</td>"
					+"<td>...</td>"
					+"<td>"+inputEvi+"</td>"
							+ "</tr>");
	formatoCeldaSombreableTabla(false,"tblPregCurso");
}

function eliminarPregunta(){
	var PreguntaObj={
			id:PreguntaCursoId
	}
	var r=confirm("¿Está seguro de eliminar este Pregunta?");
	if(r){
		callAjaxPost(URL + '/capacitacion/curso/pregunta/delete', PreguntaObj,
				function(data) {
					
					
			llamarPreguntasCurso();
					
				});
	}
	
}

function guardarPregunta(){
	var nombre=$("#inputPreNom").val();
	var justificacion=$("#inputPreJust").val();
	var campoVacio=false;
	if(nombre.length==0){
		campoVacio=true;
	}
	
	if(campoVacio){
		alert("Complete el campo")
	}else{
		var PreguntaObj={
				id:PreguntaCursoId,
				nombre:nombre,
				justificacion:justificacion,
				cursoCapacitacionId:cursoCapacitacionId
		}
		
		callAjaxPostNoLoad(URL + '/capacitacion/curso/pregunta/save', PreguntaObj,
				function(data) {
			var preguntaIdAux=data.idNuevo;
			
			var inputFileImage = document.getElementById("fileEviImagen");
			var file = inputFileImage.files[0];
			var data = new FormData();
			if(file){
				if (file.size > bitsEvidenciaImagenPregunta ) {
					alert("Lo sentimos, solo se pueden subir archivos menores a "+bitsEvidenciaImagenPregunta/1000000+" MB");
					llamarPreguntasCurso();
				} else {
					data.append("fileEvi", file);
					data.append("preguntaId", preguntaIdAux);
					
					var url = URL + '/capacitacion/curso/pregunta/imagen/save';
					 
					$.ajax({
								url : url,
								xhrFields: {
						            withCredentials: true
						        },
								type : 'POST',
								contentType : false,
								beforeSend:function(){
									loadingCelda("tblPregCurso #trPreg"+PreguntaCursoId)
								},
								data : data,
								processData : false,
								cache : false,
								success : function(data, textStatus, jqXHR) {
								 
									switch (data.CODE_RESPONSE) {
									case "06":
										sessionStorage.clear();
										document.location.replace(data.PATH);
										break;
									default:
									 
										llamarPreguntasCurso();
									}
		
								},
								error : function(jqXHR, textStatus, errorThrown) {
									$.unblockUI();
									alert("Ups, tenemos problemas tecnicos. Intente más tarde. Error: "
											+ errorThrown);
									console.log('xhRequest: ' + jqXHR + "\n");
									console.log('ErrorText: ' + textStatus + "\n");
									console.log('thrownError: ' + errorThrown + "\n");
								}
							});
				}
			
			}else{
				llamarPreguntasCurso();
			}	
					
		 
				});
	}
	
	
}


function importarPreguntasCurso(){
	
	$("#btnGuardarPregNuevos").attr("onclick",
	"javascript:guardarMasivoPreguntasExcel();");

$('#mdImpPregs').modal('show');
$('#mdImpPregs .modal-body').show();
	
}

function guardarMasivoPreguntasExcel(){
	var texto = $("#txtListadoPreg").val();
	var listExcel = texto.split('\n');
	var listPregs = [];
	var preg={};
	var listOpciones=[];
	var validado="";
	if (texto.length < 10000) {
		for (var int = 0; int < listExcel.length; int++) {
			
			var listCells = listExcel[int].split('\t');
			var filaTexto = listExcel[int];
			if (filaTexto.trim().length < 1) {
				validado = "Existen filas vacias.";
				break;
			}

			if (validado.length < 1 && listCells.length != 3) {
				validado = "No coincide el numero de celdas."
						+ listCells.length;
				break;
			} else {
				
				var genero = {};
				var opcion={
						nombre:"",isCorrecto:0
				};
				for (var j = 0; j < listCells.length; j++) {
					var element = listCells[j];
					
					if(j == 1){
						opcion.nombre=element.trim();
					}
					if(j == 2){
						opcion.isCorrecto=parseInt(element.trim());
					}
					
					if (j == 0) {
						
						if(element.trim()==0){
							
						}else{
							preg = {
									cursoCapacitacionId:cursoCapacitacionId,
									nombre:element.trim(),
									opciones:[],justificacion:""
							};
							
						}
						
					}
					
					
				}
			
				preg.opciones.push(opcion);
				if(listCells[0].length>0){
					listPregs.push(preg);
				}else{
					
				}
				
			}
		}
	} else {
		validado = "Ha excedido los 10000 caracteres permitidos.";
	}

	if (validado.length < 1) {
		var r = confirm("¿Está seguro que desea subir estas "
				+ listPregs.length + " pregunta(s)?");
		if (r == true) {
			
			console.log(listPregs);
			callAjaxPost(URL + '/capacitacion/curso/preguntas/import', listPregs,
				funcionResultadoMasivoPreguntas);
		}
	} else {
		alert(validado);
	}
}

function funcionResultadoMasivoPreguntas(){
	 
	$('#mdImpPregs').modal('hide');
	llamarPreguntasCurso();
	
}



































