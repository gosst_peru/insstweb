/**
 * 
 */
var opcionPreguntaId;
var banderaEdicionOpc;
var listFullOpciones;
function llamarOpcionesPregunta(){
	opcionPreguntaId=0;
	banderaEdicionOpc=false;
	listFullOpciones=[];
	$("#btnAgregarOpcion").show();
	$("#btnCancelarOpcion").hide();
	$("#btnEliminarOpcion").hide();
	$("#btnGuardarOpcion").hide();
	var dataParam={id:PreguntaCursoId}
	callAjaxPost(URL + '/capacitacion/curso/pregunta/opciones', dataParam, function(data) {
		
		var list = data.list;
		listFullOpciones=data.list;
		$("#tblOpcPreg tbody").html("");
		listFullOpciones.forEach(function(val,index){
			$("#tblOpcPreg tbody").append("<tr  >"+
					"<td id='opcNom"+
					list[index].id + "'  onclick='javascript:editarOpcion("+
					index+")'>"+
					list[index].nombre + "</td>" +
					"<td id='opcCorr"+
					list[index].id + "'>"+
					" <input type='radio' name='opcPreg' class='form-control' onchange='javascript:guardarOpcionCorrecta("+index+")' "+
					 (list[index].isCorrecto==1?"checked":"" )+ "></td>" +
					"</tr>")
		});
	
		formatoCeldaSombreableTabla(true,"tblOpcPreg");
	
});
	
}
function editarOpcion(pindex){
	if(!banderaEdicionOpc){
		banderaEdicionOpc=true;
		$("#btnAgregarOpcion").hide();
		$("#btnCancelarOpcion").show();
		$("#btnEliminarOpcion").show();
		$("#btnGuardarOpcion").show();
		formatoCeldaSombreableTabla(false,"tblOpcPreg");
		opcionPreguntaId=listFullOpciones[pindex].id;
		var nombre=listFullOpciones[pindex].nombre;

		$("#opcNom"+opcionPreguntaId).html("<input class='form-control' id='inputOpcNom'>");
		$("#inputOpcNom").val(nombre);
	}
}
function nuevoOpcion(){
	$("#btnAgregarOpcion").hide();
	$("#btnCancelarOpcion").show();
	$("#btnEliminarOpcion").hide();
	$("#btnGuardarOpcion").show();
	banderaEdicionOpc=true;
	
	opcionPreguntaId = 0;
	$("#tblOpcPreg tbody")
			.append(
					"<tr id='tr0'>"
					+"<td><input class='form-control' id='inputOpcNom'></td>"
					+"<td>...</td>"
				+ "</tr>");
	formatoCeldaSombreableTabla(false,"tblOpcPreg");
}

function eliminarOpcion(){
	var OpcionObj={
			id:opcionPreguntaId
	}
	var r=confirm("¿Está seguro de eliminar este Opcion?");
	if(r){
		callAjaxPost(URL + '/capacitacion/curso/pregunta/opcion/delete', OpcionObj,
				function(data) {
					
					
			llamarOpcionesPregunta();
					
				});
	}
	
}

function guardarOpcion(){
	var nombre=$("#inputOpcNom").val();
	var campoVacio=false;
	if(nombre.length==0){
		campoVacio=true;
	}
	
	if(campoVacio){
		alert("Complete el campo")
	}else{
		var OpcionObj={
				id:opcionPreguntaId,
				nombre:nombre,
				isCorrecto:0,
				preguntaCursoId:PreguntaCursoId
		}
		
		callAjaxPost(URL + '/capacitacion/curso/pregunta/opcion/save', OpcionObj,
				function(data) {
					
					
			llamarOpcionesPregunta();
					
				});
	}	
	
}


function guardarOpcionCorrecta(pindex){
	var campoVacio=false;
	var nombre=listFullOpciones[pindex].nombre;
	opcionPreguntaId=listFullOpciones[pindex].id;
	
		var OpcionObj={
				id:opcionPreguntaId,
				nombre:nombre,
				isCorrecto:1,
				preguntaCursoId:PreguntaCursoId
		}
		
		callAjaxPost(URL + '/capacitacion/curso/pregunta/opcion/save', OpcionObj,
				function(data) {
					
				});
	
	
}

