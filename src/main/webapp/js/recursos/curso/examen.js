/**
 * 
 */
var cursoExamenActual;
var resolviendoExamen;
var contadorNoIniciado=true;
function verTodosCursosImplementar(){
	var heightAux= $(window).height();
	console.log(heightAux+" v 1.0");
	$("#cursosCompletados").hide();
	
	$("#cursosImplementar .linkVerTodos")
	.attr("onclick","ocultarTodosCursosImplementar()")
	.html("<i class='fa fa-angle-left' aria-hidden='true'></i>Volver");
	$("#cursosImplementar .divListCursos .classFormacionHide").show();
	 $( "#cursosImplementar .divListCursos" ).css({"overflow-y":"auto"}).animate({
		    height:heightAux-200+"px"
		  }, 1000 );
}

function ocultarTodosCursosImplementar(){
	$("#cursosImplementar .divListCursos .classFormacionHide").hide();
	
	$("#cursosImplementar .linkVerTodos")
	.attr("onclick","verTodosCursosImplementar() ")
	.html("Ver todos<i class='fa fa-angle-right' aria-hidden='true'></i>");
	$( "#cursosImplementar .divListCursos" ).css({"overflow-y":"auto"}).animate({
	    height:320+"px"
	  }, 1000 );
	$("#cursosCompletados").show();
}

function verTodosCursosCompletados(){
	var heightAux= $(window).height();
	
	$("#cursosImplementar").hide();
	
	$("#cursosCompletados .linkVerTodos")
	.attr("onclick","ocultarTodosCursosCompletados()")
	.html("<i class='fa fa-angle-left' aria-hidden='true'></i>Volver");
	$("#cursosCompletados .divListCursos .classFormacionHide").show();
	 $( "#cursosCompletados .divListCursos" ).css({"overflow-y":"auto"}).animate({
		    height:heightAux-200+"px"
		  }, 1000 );
}

function ocultarTodosCursosCompletados(){
	$("#cursosCompletados .divListCursos .classFormacionHide").hide();
	
	$("#cursosCompletados .linkVerTodos")
	.attr("onclick","verTodosCursosCompletados() ")
	.html("Ver todos<i class='fa fa-angle-right' aria-hidden='true'></i>");
	$( "#cursosCompletados .divListCursos" ).css({"overflow-y":"auto"}).animate({
	    height:320+"px"
	  }, 1000 );
	$("#cursosImplementar").show();
}

function verDetallesCurso(indCap,indCurso,isPasado){
	var heightAux= $(window).height();
	var capActual=listFullFormaciones[indCap].cursos
	cursoExamenActual=capActual[indCurso];
	var imagenCap="<img></img>";
	if(cursoExamenActual.evidencia!=null){
		imagenCap=	insertarImagenParaTablaMovil(cursoExamenActual.evidencia,"250px","100%");	
	}
	var dataParam={
			id:cursoExamenActual.id,
			trabajadorAsociadoId:(sessionStorage.getItem("trabajadorGosstId"))};
	var butonFinal="<button class='btn btn-success' onclick='empezarExamenCurso();'>" +
	"<i class='fa fa-align-left'> </i>" +
	"&nbsp;Empezar Examen" +
	"&nbsp;<i class='fa fa-angle-right'> </i></button>";
	
	if(isPasado==1){
		butonFinal="<button class='btn btn-success' onclick='descargarCertificadoCurso("+cursoExamenActual.id+");' style='font-size: 20px;'>" +
		"<i class='fa fa-certificate'> </i>" +
		"&nbsp;Descargar Certificado" +
		"</button>";
	}
	if(isPasado==2){
		butonFinal="<button class='btn btn-info' onclick='alertarCertificado()' style='font-size: 20px;'>" +
		"<i class='fa fa-certificate'> </i>" +
		"&nbsp;Descargar Certificado" +
		"</button>";
	}
	$("#cursoEspecificoGosst").show();
	$("#cursosTrabajadorGosst").hide();
	$("#cursoExamenGosst").hide();
	$("#cursoResultadosGosst").hide();
	$(".divDetalleCurso").css({"height":heightAux-200+"px"})
	.html("")
	.append("<div class='imgDetalleCurso'></div>" +
			"<div class='textDetalleCurso'></div>" +
			"<div class='objDetalleCurso'></div>" +
			"<div class='archDetalleCurso'></div>" +
			"<div class='enlDetalleCurso'></div>" +
			butonFinal);
	$(".divDetalleCurso button").hide();
	$(".imgDetalleCurso").html(imagenCap);
	$(".textDetalleCurso").html(cursoExamenActual.descripcion);
$("#cursoEspecificoGosst .titleDivCursos .col-md-10").html("Curso Online '"+cursoExamenActual.nombre+"'");
	
callAjaxPostNoLoad(URL + '/capacitacion/curso/evaluacion', dataParam, function(data) {
	var eval=data.list;
	console.log(eval);
	$(".divDetalleCurso button").show();
	if(eval.length==0){
		eval={
				id:0,
				numIntentos:0,
				cursoId:cursoExamenActual.id,
				trabajadorId:(sessionStorage.getItem("trabajadorGosstId"))
		}
		cursoExamenActual.evaluacion=eval;
	}else{
		cursoExamenActual.evaluacion=eval[0];
	}
	
	

});
	
	callAjaxPostNoLoad(URL + '/capacitacion/curso/objetivos', dataParam, function(data) {
		var objetivos=data.list;
		$(".objDetalleCurso").html("<table class='table tableCursoGosst table-bordered ' >" +
				"<thead>" +
				"<tr>" +
				"<td>Objetivos del curso</td>" +
				"</tr>" +
				"</thead>" +
				"<tbody></tbody>" +
				"</table>");
		objetivos.forEach(function(val,index){
			$(".objDetalleCurso table")
			.append("<tr>" +
					"<td>&#8226;" +val.nombre+"</td>"+
			"</tr>");
		})
	});
	callAjaxPostNoLoad(URL + '/capacitacion/curso/archivos', dataParam, function(data) {
		var archivos=data.list;
		$(".archDetalleCurso").html("<table class='table tableCursoGosst table-bordered ' >" +
				"<thead>" +
				"<tr>" +
				"<td style='width:100px'>...</td>" +
				"<td>Material del curso</td>" +
				"<td> Detalle del material</td>" +
				"</tr>" +
				"</thead>" +
				"<tbody></tbody>" +
				"</table>");
		archivos.forEach(function(val,index){
			var archivoCursoId=val.id;
			$(".archDetalleCurso table")
			.append("<tr>" +
					"<td style='text-align: center;'>" +"<a  target='_blank' href='"+URL
				+ "/capacitacion/curso/archivo/evidencia?archivoId="
				+ archivoCursoId+"''><i class='fa fa-2x fa-download'></i></a>"+"</td>"+
					"<td>&#8226;" +val.nombre+"</td>"+"<td>" +val.observacion+"</td>"+
			"</tr>");
		})
	});
	callAjaxPostNoLoad(URL + '/capacitacion/curso/enlaces', dataParam, function(data) {
		var enlaces=data.list;
		$(".enlDetalleCurso").html("<table class='table tableCursoGosst table-bordered ' >" +
				"<thead>" +
				"<tr>" +
				"<td style='width:100px'>...</td>" +
				"<td>Enlace de apoyo</td>" +
				"<td> Detalle de los enlaces</td>" +
				"</tr>" +
				"</thead>" +
				"<tbody></tbody>" +
				"</table>");
		enlaces.forEach(function(val,index){
			var enlaceId=val.id;
			$(".enlDetalleCurso table")
			.append("<tr>" +
					"<td style='text-align: center;'>" +"<a target='_blank' href='"+val.nombre+"'>" +
							"<i class='fa fa-2x fa-external-link'></i></a>"+"</td>"+
					"<td>&#8226;" +val.nombre+"</td>"+"<td>" +val.observacion+"</td>"+
			"</tr>");
		})
	});
	
	
	
	
	
}
function alertarCertificado(){
	alert('No fue aprobado');
}
function verTodosCursosTrabajador(){
	$("#cursoEspecificoGosst").hide();
	$("#cursosTrabajadorGosst").show();
}


function empezarExamenCurso(){
	$("#cursoEspecificoGosst").hide();
	$("#cursosTrabajadorGosst").hide();
	$("#cursoExamenGosst").show();
	resolviendoExamen=true;
	$("#cursoExamenGosst .titleDivCursos")
	.html("<div class='col-md-10'>" +
			"<i class='fa fa-file-text-o' aria-hidden='true'></i>"+
			"Examen del Curso "+cursoExamenActual.nombre+"<br>"+
			
			"<div id='timerActual'></div>"+
			"</div>" +
			"<div class='col-md-2'>"+
			"<i class='fa fa-check-circle-o' aria-hidden='true'></i>" +
			
			" Intentos: "+cursoExamenActual.evaluacion.numIntentos+
			"</div>"
						
			);
	var heightAux= $(window).height();
	
	$(".containerExamenGosst")
	.html("");
	var cursoAux={
			id: cursoExamenActual.id,
			verImagenPregunta: 1
	}
	callAjaxPostNoLoad(URL + '/capacitacion/curso/preguntas', cursoAux, function(data) {
		setTimeout(function(){
			$(".containerExamenGosst")
			.html("");
		var preguntasImagen=data.list;
		cursoExamenActual.preguntas=preguntasImagen;
		cursoExamenActual.preguntas.forEach(function(val,index){
			
			var opciones=val.opciones;
			var textOpc="";
			var textImagen="";
			var alternateFila=true;
			opciones.forEach(function(val1,index1){
				var colorFondo="white";
				if(alternateFila){
					colorFondo="#f9f9f9";
				}
				alternateFila=!alternateFila;
				textOpc+="<div class='opcionGosstCss' style='background-color:"+colorFondo+"'><input type='radio' name='optionFamily"+val.id+"' " +
						"id='optionId"+val1.id+"'><label for='optionId"+val1.id+"'>"+val1.nombre+"</label></div>";
			});
			
			if(val.imagen!=null){
				textImagen=insertarImagenParaTablaMovil(val.imagen,(heightAux-200-200)/2+"px","100%");
			}
			$(".containerExamenGosst")
			.append("<div class='divPreguntaCurso row' id='preguntaId"+val.id+"'>"
					+"<div class='divNombrePregunta'>"+(index+1)+".&nbsp;"+val.nombre+"</div>" +
					"<div class='divOpcionesGosst col-md-6'>"+textOpc+"</div>"+
					"<div class='divImagenPregunta col-md-6'>"+textImagen+"</div>"+
					
					
					"</div>");
			$(".containerExamenGosst").css({"height":heightAux-200+"px","overflow-y":"auto"})
		});
		$(".containerExamenGosst")
		.append("<button class='btn btn-success' onclick='completarExamenCurso()'>Completar Examen&nbsp;<i class='fa fa-angle-right'> </i></button>")
		contadorNoIniciado=true;
		countdown();
	},2000)},preCargaExamenImagen,{});
	
}


function completarExamenCurso(){
	$("#cursoResultadosGosst").show();
	$("#cursoExamenGosst").hide();
	resolviendoExamen=false;
	
	cursoExamenActual.preguntas.forEach(function(val,index){
		var opciones=val.opciones;
		opciones.forEach(function(val1,index1){
			val1.isCorrecto=($("#optionId"+val1.id).is(':checked')?1:0);
		});
	});
	cursoExamenActual.evidencia=null;
	cursoExamenActual.trabajadorAsociadoId=(sessionStorage.getItem("trabajadorGosstId"));
	
	callAjaxPostNoLoad(URL + '/capacitacion/curso/preguntas/evaluacion', cursoExamenActual, function(data) {
		setTimeout(function(){
		var cursoCorregido=data.list;
		var textResult=(cursoCorregido.evaluacion.isAprobado==1?"APROBADO":"DESAPROBADO");
		var textCerti=(cursoCorregido.evaluacion.isAprobado==1?"<button class='btn btn-success'>Descargar Certificado" +
				"<br>No es necesario enviar al área SASS</button>":"");
		var textPreg="";
		var numPreguntasMalas=0;
		$(".containerResultadosGosst")
		.html("<div class='tituloResultado'></div><br>" +
				"<div class='divCertificadoResultado'></div>" +
				"<div class='divPreguntasMalas'></div>");
		cursoCorregido.preguntas.forEach(function(val,index){
			if(val.evaluacionCorrecta==0){
				numPreguntasMalas++;
				var opciones=val.opciones;
				var textOpc="";
				opciones.forEach(function(val1,index1){
					if(val1.isCorrecto){
						textOpc+="<div class='opcionGosstCss'><input type='radio' name='optionFamily"+val.id+"' " +
						"id='optionId"+val1.id+"'><label for='optionId"+val1.id+"'>"+val1.nombre+"</label></div>";
			
					}
				});
				
				textPreg+="<div class='divPreguntaCurso row'>" 
				+"<div class='divNombrePregunta'>"+(index+1)+".&nbsp;"+val.nombre+"</div>" +"" 
				+"<br>" +textOpc+"" +
						"<div class='opcionGosstCss justificacionGosst'>" +
						"<strong>Justificación: </strong>"+val.justificacion+
						"</div>"+
						"</div>";
			}
			
		});
		$(".tituloResultado").html("RESULTADO DEL EXAMEN: " +textResult);
		$(".divCertificadoResultado").html(textCerti);
		$(".divCertificadoResultado button").on("click",function(){
			descargarCertificadoCurso(cursoExamenActual.id)
		});
		if(textPreg.length==0){
			$(".divPreguntasMalas").hide();
		}else{
			$(".divPreguntasMalas").append("PREGUNTAS MAL CONTESTADAS ("+numPreguntasMalas+" / "+cursoCorregido.preguntas.length+")"
					+textPreg);
		}
	
		
		
		console.log(cursoCorregido);
		},2000)
	}
		
	 
		
		,algoCHido,{});
}
function preCargaExamenImagen(){
	
	 $(".containerExamenGosst").html('<i class="fa fa-spinner fa-pulse fa-2x" style="color:#19958E; font-size: 20px;" aria-hidden="true"></i>'+
			 "<strong style='color:#19958E;font-size: 20px; '>Procesando</strong>");
	 
	 return true;

}
function algoCHido(){
	
		 $(".containerResultadosGosst").html('<i class="fa fa-spinner fa-pulse fa-2x" aria-hidden="true"></i>'+
				 "Procesando");
		 
		 return true;
	
}
function descargarCertificadoCurso(cursoId,trabajadorId){
	var trabId=sessionStorage.getItem("trabajadorGosstId");
	trabajadorId = typeof trabajadorId !== 'undefined' ? trabajadorId : trabId;
	window.open(URL
			+ "/capacitacion/curso/certificado?trabajadorId="+trabajadorId+"&cursoId="
			+ cursoId , '_blank');
	
	
}


function verCursosCapacitacion(){
	cargarPrimerEstado();
	$("#cursoResultadosGosst").hide();
	$("#cursosTrabajadorGosst").show();
	$("#containerResultadosGosst").html("");
}




function countdown(id){
    
    var hoy=new Date();
    var hoyAux=new Date();
   var fecha=cursoExamenActual.tiempoLimite;
    if(contadorNoIniciado){
    	fecha=new Date(hoyAux.setSeconds(cursoExamenActual.tiempoSolucion*60*60));
    	cursoExamenActual.tiempoLimite=fecha;
        contadorNoIniciado=false;
    }
    
    var dias=0
    var horas=0
    var minutos=0
    var segundos=0
    if ((fecha>=hoy) && resolviendoExamen){
        var diferencia=(fecha.getTime()-hoy.getTime())/1000;
        dias=Math.floor(diferencia/86400)
        diferencia=diferencia-(86400*dias)
        horas=Math.floor(diferencia/3600)
        diferencia=diferencia-(3600*horas)
        minutos=Math.floor(diferencia/60)
        diferencia=diferencia-(60*minutos)
        segundos=Math.floor(diferencia);
       
$("#cursoExamenGosst .titleDivCursos #timerActual").html(
		'<label>  Quedan ' + horas + ' Horas, ' + minutos + ' Minutos, ' + segundos + ' Segundos</label>')

        if ( (horas>0 || minutos>0 || segundos>0) ){
            setTimeout(countdown,1000) 
        }else{
        	completarExamenCurso();
        }
    }
    else{
}
}




