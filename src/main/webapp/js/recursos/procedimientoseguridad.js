var banderaEdicion;
var procedimientoSeguridadId;
var procedimientoSeguridadFecha;
var procedimientoSeguridadFechaProx;
var contador2 = 0;
var procEstado;
var procEstadoNombre;
var listFullProcedimiento;
var listTrabajadores;
var listanombretab;
$(document).ready(function() {

	insertMenu();
	cargarPrimerEstado();
	
});

function cargarPrimerEstado() {
	banderaEdicion = false;
	procedimientoSeguridadId = 0;
	procedimientoSeguridadFecha=null;

	removerBotones();
	crearBotones();
	deshabilitarBotonesEdicion();
	$("#fsBotones")
	.append(
			"<button id='btnClipTabla' type='button' class='btn btn-success' title='Tabla Clipboard' >"
					+ "<i class='fa fa-clipboard fa-2x'></i>" + "</button>");
	
	$("#fsBotones")
	.append(
			"<button id='btnProcSugerido' type='button' class='btn btn-success' title='Procedimientos Sugeridos' >"
					+ "<i class='fa fa-folder-open-o fa-2x'></i>" + "</button>");
	$("#btnUpload").attr("onclick", "javascript:importarDatos();");
	$("#btnNuevo").attr("onclick", "javascript:nuevoProcedimientoSeguridad();");
	$("#btnCancelar").attr("onclick", "javascript:cancelarProcedimientoSeguridad();");
	$("#btnGuardar").attr("onclick", "javascript:guardarProcedimientoSeguridad();");
	$("#btnEliminar").attr("onclick", "javascript:eliminarProcedimientoSeguridad();");
	$("#btnImportar").attr("onclick", "javascript:importarDatos();");
	$("#btnProcSugerido").attr("onclick", "javascript:verProcedimientoSugerido();");

	
	if(getSession("perfilGosstId")==1){
		$("#btnProcSugerido").show();
	}else{
		$("#btnProcSugerido").hide();
	}
	
	$("#btnClipTabla").on("click", function(){
		new Clipboard('#btnClipTabla', {
			text : function(trigger) {
				return obtenerDatosTablaEstandar("tblProSeg")
			}
		});
		
		alert("Se han guardado al clipboard la tabla de este módulo" );
	});
	
	var dataParam = {
			idCompany : sessionStorage.getItem("gestopcompanyid")
	};

	 callAjaxPost(URL + '/procedimientoseguridad', dataParam,
	 procesarDataDescargadaPrimerEstado);
	 callAjaxPost(URL + '/accidente/trab', dataParam,
				function (data){
			switch (data.CODE_RESPONSE) {
				
				case "05":
				
					listTrabajadores=data.listDniNombre;
					
					break;
				default:
					alert("Ocurrió un error al traer los trabajadores!");
				}

			listanombretab=listarStringsDesdeArray(listTrabajadores, "nombre");
			},function(){});
}

function procesarDataDescargadaPrimerEstado(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		var list = data.list;
		listFullProcedimiento=list;
		$("#h2Titulo")
				.html(
						"<a onclick='javascript:cargarPrimerEstado();' href='#'>Procedimientos de Seguridad</a>");
		$("#tblProSeg tbody tr").remove();

		for (var index = 0; index < list.length; index++) {
			var versionActiva={
					responsable:"",
					fechaActualizacionTexto:"",
					horaPlanificadaTexto:"",
					fechaPlanificadaTexto:"",
					evidenciaNombre:""
						
			};
			if(list[index].versiones[0]){
				versionActiva=list[index].versiones[0];
			}
			$("#tblProSeg tbody").append(

					"<tr id='tr" + list[index].procedimientoSeguridadId
							+ "' onclick='javascript:editarProcedimientoSeguridad("
						+index+")' >"
							+ "<td id='tdcod" + list[index].procedimientoSeguridadId
							+ "'>" + list[index].procedimientoSeguridadCodigo +"</td>"
							+ "<td id='tdnom" + list[index].procedimientoSeguridadId
							+ "'>" + list[index].procedimientoSeguridadNombre +"</td>"
							+ "<td id='tdres" + list[index].procedimientoSeguridadId
							+ "'>" + list[index].procedimientoSeguridadResumen +"</td>"
							+ "<td id='tdvers" + list[index].procedimientoSeguridadId
							+ "'>" + list[index].versiones.length +"</td>"
							
							+ "<td  id='tdrespon"
							+ list[index].procedimientoSeguridadId + "'>"
							+ versionActiva.responsable
							+ "</td>" 
							+ "<td  id='tdfap"
							+ list[index].procedimientoSeguridadId + "'>"
							+ versionActiva.fechaPlanificadaTexto
							+ "</td>" 
							+ "<td  id='tdhap"
							+ list[index].procedimientoSeguridadId + "'>"
							+ versionActiva.horaPlanificadaTexto
							+ "</td>" 
							+ "<td  id='tdfaar"
							+ list[index].procedimientoSeguridadId + "'>"
							+ versionActiva.fechaActualizacionTexto
							+ "</td>" 
							+ "<td  id='tdfar"
							+ list[index].procedimientoSeguridadId + "'>"
							+ list[index].fechaActualizacionTexto
							+ "</td>" 
							+ "<td  id='tdevipro"
							+ list[index].procedimientoSeguridadId + "'>"
							+ versionActiva.evidenciaNombre 

							+ "</td>" 
							
							+ "</tr>");
		}
		formatoCeldaSombreable(true);completarBarraCarga();
		if(getSession("linkCalendarioProcedimmientoId")!=null){
		
			listFullProcedimiento.every(function(val,index3){
				
				if(val.procedimientoSeguridadId==parseInt(getSession("linkCalendarioProcedimmientoId"))){
					editarProcedimientoSeguridad(index3);
					
					sessionStorage.removeItem("linkCalendarioProcedimmientoId");
					return false;
				}else{
					return true;
				}
			});
			
		}
		break;
	default:
		alert("Ocurrió un error al traer los procedimientos de Seguridad!");
	}
	if (contador2 == 0) {
		goheadfixed('table.fixed');
	}
	contador2 = contador2 + 1;
}

function editarProcedimientoSeguridad(pindex) {
	if (!banderaEdicion) {
		var versionActiva={
				responsable:"",
				fechaActualizacionTexto:"",
				horaPlanificadaTexto:"",
				fechaPlanificadaTexto:"",
				evidenciaNombre:""
					
		};
		if(listFullProcedimiento[pindex].versiones[0]){
			versionActiva=listFullProcedimiento[pindex].versiones[0];
		}
		procedimientoSeguridadId = listFullProcedimiento[pindex].procedimientoSeguridadId;
		procedimientoSeguridadFecha = listFullProcedimiento[pindex].procedimientoSeguridadFecha;
		procedimientoSeguridadFechaProx=listFullProcedimiento[pindex].fechaActualizacion;
		var horaPlanificada=listFullProcedimiento[pindex].horaPlanificada;
		formatoCeldaSombreable(false);$("#btnClipTabla").hide();
		var name = $("#tdnom" + procedimientoSeguridadId).text();
		$("#tdnom" + procedimientoSeguridadId)
				.html(
						"<input type='text' id='inputNom' class='form-control' placeholder='Procedimiento Seguridad' autofocus='true' ' value='"
								+ name + "'>");
		
		var codigo = $("#tdcod" + procedimientoSeguridadId).text();
		$("#tdcod" + procedimientoSeguridadId)
				.html(
						"<input type='text' id='inputCod' class='form-control' placeholder='Codigo' autofocus='true' ' value='"
								+ codigo + "'>");
		var name1 = $("#tdres" + procedimientoSeguridadId).text();
		$("#tdres" + procedimientoSeguridadId)
				.html(
						"<input type='text' id='inputRes' class='form-control' placeholder='Resumen' autofocus='true' ' value='"
								+ name1 + "'>");
			
	
		
		$("#tdfar" + procedimientoSeguridadId)
		.html(
				"<input type='date' id='inputFecProx' onchange='hallarEstadoImplementacion("+procedimientoSeguridadId+");'  class='form-control'>");

var today = convertirFechaInput(procedimientoSeguridadFechaProx);

$("#tdvers" + procedimientoSeguridadId)
.html("<a onclick='verVersionesProcedimiento()'>"+listFullProcedimiento[pindex].versiones.length+"</a>");
$("#inputFecProx").val(today);
var nombreEvidencia = $("#tdevi" + procedimientoSeguridadId).text();
		$("#tdevi" + procedimientoSeguridadId)
		.html(
				"<a href='"
						+ URL
						+ "/procedimientoseguridad/version/evidencia?versionId="
						+ procedimientoSeguridadId
						+ "' "
						+ "target='_blank'>"+nombreEvidencia+"</a>"
						+ "<br/><a href='#' onclick='javascript:mostrarCargarImagen("
						+ procedimientoSeguridadId + ")'  id='subirimagen"
						+ procedimientoSeguridadId + "'>Subir</a>");
		
		banderaEdicion = true;
		habilitarBotonesEdicion();
	}
}

function nuevoProcedimientoSeguridad() {
	if (!banderaEdicion) {

		$("#tblProSeg tbody:first")
				.append(
						"<tr id='0'>" 
						+ "<td><input type='text' id='inputCod' placeholder='Codigo' class='form-control' autofocus='true'></td>"		
						
						+ "<td><input type='text' id='inputNom' class='form-control' placeholder='procedimiento Seguridad' required='true' ></td>"
						+ "<td><input type='text' id='inputRes' placeholder='Resumen' class='form-control'></td>"
						+ "<td>...</td>" 
						+ "<td>...</td>" 
						+ "<td>...</td>" 
						+ "<td>...</td>" 
						+ "<td>...</td>" 
								+ "<td><input type='date' id='inputFecProx' onchange='hallarEstadoImplementacion(0);' class='form-control'></td>"
						
								+ "<td>...</td>" 
								
								+ "</tr>");
		llevarTablaFondo("wrapper");
		procedimientoSeguridadId = 0;
		habilitarBotonesNuevo();
		banderaEdicion = true;
	} else {
		alert("Guarde primero.");
	}
}

function cancelarProcedimientoSeguridad() {
	cargarPrimerEstado();
}

function eliminarProcedimientoSeguridad() {
	var r = confirm("¿Está seguro de eliminar la procedimientoSeguridad?");
	if (r == true) {
		var dataParam = {
				procedimientoSeguridadId : procedimientoSeguridadId,
		};

		callAjaxPost(URL + '/procedimientoseguridad/delete', dataParam,
				procesarResultadoEliminarprocedimientoSeguridad);
	}
}

function procesarResultadoEliminarprocedimientoSeguridad(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		cargarPrimerEstado();
		break;
	default:
		alert("Ocurrió un error al eliminar el procedimientoSeguridad!");
	}
}

function guardarProcedimientoSeguridad() {

	var campoVacio = true;
	var inputNom = $("#inputNom").val();
	var codigo=$("#inputCod").val();
	var resumen=$("#inputRes").val();
	var fecha2=$("#inputFecProx").val();
	var mes = $("#inputFecProx").val().substring(5, 7) - 1;
	var fechatempprox = new Date($("#inputFecProx").val().substring(0, 4), mes, $(
			"#inputFecProx").val().substring(8, 10));
	
	var inputFecProx = fechatempprox;
	
	
	if (!fecha2) {
		inputFecProx = null
	}

	if (resumen.length == 0 || inputNom.length == 0 || !fecha2) {
		campoVacio = false;
	}
	hallarEstadoImplementacion(procedimientoSeguridadId);
	
	if (campoVacio) {

		var dataParam = {
			procedimientoSeguridadId : procedimientoSeguridadId,
			procedimientoSeguridadNombre : inputNom,
			procedimientoSeguridadResumen:resumen,
			procedimientoSeguridadCodigo:codigo,
			fechaActualizacion: inputFecProx,
			idCompany : sessionStorage.getItem("gestopcompanyid")
		};

		 callAjaxPost(URL + '/procedimientoseguridad/save', dataParam,
		 procesarResultadoGuardarProSeg);
	} else {
		alert("Debe ingresar todos los campos.");
	}
}

function procesarResultadoGuardarProSeg(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		cargarPrimerEstado();
		break;
	default:
		alert("Ocurrió un error al guardar la procedimientoSeguridad!");
	}
}

function mostrarCargarImagen(procedimientoId) {
	$('#mdUpload').modal('show');
	$('#btnUploadArchivo').attr("onclick",
			"javascript:uploadArchivo(" + procedimientoId + ");");
}

function uploadArchivo(procedimientoId) {
	var inputFileImage = document.getElementById("fileEvi");
	var file = inputFileImage.files[0];
	var data = new FormData();

	if (file.size > bitsEvidenciaProcedimiento) {
		alert("Lo sentimos, solo se pueden subir " +
				"archivos menores a "+bitsEvidenciaProcedimiento/1000000+" MB");
	} else {
		data.append("fileEvi", file);
		data.append("procedimientoId", procedimientoId);
		
		var url = URL + '/procedimientoseguridad/evidencia/save';
		$.blockUI({
			message : 'cargando...'
		});
		$
				.ajax({
					url : url,
					xhrFields: {
			            withCredentials: true
			        },
					type : 'POST',
					contentType : false,
					data : data,
					processData : false,
					cache : false,
					success : function(data, textStatus, jqXHR) {
						$.unblockUI();
						switch (data.CODE_RESPONSE) {
						case "06":
							sessionStorage.clear();
							document.location.replace(data.PATH);
							break;
						default:
							console.log('Se subio el archivo correctamente.');
							$('#mdUpload').modal('hide');
						}

					},
					error : function(jqXHR, textStatus, errorThrown) {
						$.unblockUI();
						alert("Ups, tenemos problemas tecnicos. Intente más tarde. Error: "
								+ errorThrown);
						console.log('xhRequest: ' + jqXHR + "\n");
						console.log('ErrorText: ' + textStatus + "\n");
						console.log('thrownError: ' + errorThrown + "\n");
					}
				});
	}
}

function hallarEstadoImplementacion(procId){
	
	
}

function importarDatos() {
	$("#btnGuardarNuevos").attr("onclick",
			"javascript:guardarMasivoCopiaExcel();");

	$('#mdImpDatos').modal('show');
	
}
function guardarMasivoCopiaExcel() {
	var texto = $("#txtListado").val();
	var listExcel = texto.split('\n');
	var listRecurso = [];
	var listProcRepe = [];
	var listFullNombres = "";
	var validado = "";
	if (texto.length < 10000) {
		for (var int = 0; int < listExcel.length; int++) {

			var listCells = listExcel[int].split('\t');
			var filaTexto = listExcel[int];
			if (filaTexto.trim().length < 1) {
				validado = "Existen filas vacias.";
				break;
			}

			if (validado.length < 1 && listCells.length != 4) {
				validado = "No coincide el numero de celdas. ("
						+ listCells.length+"!=4 )";
				break;
			} else {
				var recurs = {};
				for (var j = 0; j < listCells.length; j++) {
					var element = listCells[j];
					if (j === 0) {
						
						recurs.procedimientoSeguridadCodigo = element.trim();
					
						}
					if (j === 1) {
						recurs.procedimientoSeguridadNombre = element.trim();
						if ($.inArray(recurs.procedimientoSeguridadNombre, listProcRepe) === -1) {
							listProcRepe.push(recurs.procedimientoSeguridadNombre);
						} else {
							listFullNombres = listFullNombres + recurs.procedimientoSeguridadNombre + ", ";
						}

					}
					
					if (j === 2) {
						
						recurs.procedimientoSeguridadResumen = element.trim();
					
				}
					
					
					if (j === 3) {
						recurs.version = element.trim();
						
					}
					var hoy=new Date();
					recurs.horaPlanificada="10:00:00";
					recurs.procedimientoSeguridadFecha=hoy;
					
					 var fFecha = Date.UTC(hoy.getUTCFullYear(), hoy.getUTCMonth(), hoy.getUTCDate())+(86400000*365); // 86400000 son los milisegundos que tiene un día

					recurs.procedimientoSeguridadFechaProx=fFecha;
					recurs.idCompany = sessionStorage.getItem("gestopcompanyid");
				
				}

				listRecurso.push(recurs);
				if (listProcRepe.length < listRecurso.length) {
					validado = "Existen procedimientos repetidos." + listFullNombres;
					break;
				}
			}
		}
	} else {
		validado = "Ha excedido los 10000 caracteres permitidos.";
	}

	if (validado.length < 1) {
		var r = confirm("¿Está seguro que desea subir estos "
				+ listExcel.length + " elemento(s)?");
		if (r == true) {
			var dataParam = {
				listRecurso : listRecurso
			};
			console.log(listRecurso);
			callAjaxPost(URL + '/procedimientoseguridad/masivo/save', dataParam,
					function (data) {
				switch (data.CODE_RESPONSE) {
				case "05":
					
						alert("Se guardaron exitosamente.");
						cargarPrimerEstado();
						$('#mdImpDatos').modal('hide');
					
					break;
				default:
					alert("Ocurri&oacute; un error al guardar los proce!");
				}
			});
		}
	} else {
		alert(validado);
	}
}

function verProcedimientoSugerido(){
	$("#modalSugerido").modal("show");
	
	$("#divProc1 table tbody tr").remove();
	$("#divProc2 table tbody tr").remove();
	$("#divProc3 table tbody tr").remove();

	var dataParam = {
		
			idCompany : sessionStorage.getItem("gestopcompanyid")
		};

		 callAjaxPost(URL + '/procedimientoseguridad/sugeridos', dataParam,
		 function(data){
			 if(data.CODE_RESPONSE=="05"){
				 var listSugerida=data.listSugerida;
				
				 listSugerida.forEach(function(val, index){
						$("#divProc"+val.tipoSugeridoId+" table tbody").append(
								"<tr>" +
								"<td>"+val.codigoSugerido+"</td>" +
								"<td>"+val.resumen+"</td>" +
								"<td>"+val.aplicacion+"</td>" +
								"<td>"+val.version+"</td>" +
								"<td>"+val.fechaActualizacion+"</td>" +
								"<td><a href='"
						+ URL
						+ "/procedimientoseguridad/sugerido/evidencia?sugeridoId="
						+ val.id
						+ "' target='_blank'><i class='fa fa-cloud-download fa-2x userLink' aria-hidden='true' " +
								"id='descSugg"+val.id+"' ></i>"+""+"</td>" +
								"</tr>");
					
					}); 
				 
			 }else{
				 console.log("No se pudieron traer los sugeridos")
			 }
			
		 });
	
	
}



function verVersionesProcedimiento(){
	$("#mdVersionProcedimiento").modal("show");
	llamarVersionesProcedimiento();
}
