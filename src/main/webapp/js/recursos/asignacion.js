/**
 * 
 */
var recursoEdicionObj = {};
var listFullPuestosRecurso = [];
function verPuestosRecurso(recursoId,recursoIndex){
	recursoId = parseInt(recursoId);
	var listModal = {id:"modalAsig",nombre:"Asignación",contenido:
		"<button class='btn btn-success' id='btnSaveAsigRecurso'><i class='fa fa-floppy-o'></i>Guardar</button>" +
		"" +
		"<table id='tblAsigRecuro' " +
		"class='table table-striped table-bordered table-hover table-fixed'>" +
		"<thead>" +
		"<tr>" +
		"<td class='tb-acc'>Puesto de trabajo</td>" +
		"<td class='tb-acc' style='width : 120px'>Asignación</td>" +
		(recursoId == 80?"<td class='tb-acc'># Trabajadores aprobados / # Trabajadores total</td>":"") +
		"</tr>" +
		"</thead>" +
		"" +
		"<tbody></tbody>" +
		"</table>"};
	agregarModalPrincipalUsuario(listModal);
	var functionText ="";
	var dataParam = {empresaId : getSession("gestopcompanyid")};
	
	$("#tblAsigRecuro tbody tr").remove();
	
	switch(recursoId){
	case 1:
		functionText = "guardarAsignacionFormacion()"
		recursoEdicionObj = listFullFormaciones[recursoIndex];
		dataParam.capacitacionId = recursoEdicionObj.capacitacionId;
		callAjaxPost(URL+"/capacitacion/asignacion/puestos",dataParam,procesarAsignacionPuestoRecursoSinTrabajadores);
		break;
	case 2:
		functionText = "guardarAsignacionExMedico()"
		recursoEdicionObj = listFullExamMedico[recursoIndex];
		dataParam.examenMedicoId = recursoEdicionObj.examenMedicoId;
		callAjaxPost(URL+"/examenmedico/asignacion/puestos",dataParam,procesarAsignacionPuestoRecursoSinTrabajadores);
		break;
	case 3:
		functionText = "guardarAsignacionEquipoSeguridad()"
			recursoEdicionObj = listFullEquipos[recursoIndex];
			dataParam.equipoSeguridadId = recursoEdicionObj.equipoSeguridadId;
			callAjaxPost(URL+"/equiposeguridad/asignacion/puestos",dataParam,procesarAsignacionPuestoRecursoSinTrabajadores);
			
		break;
	case 4:
		functionText = "guardarAsignacionProcedimiento()"
			recursoEdicionObj = listFullProcedimiento[recursoIndex];
			dataParam.procedimientoSeguridadId = recursoEdicionObj.procedimientoSeguridadId;
			callAjaxPost(URL+"/procedimientoseguridad/asignacion/puestos",dataParam,procesarAsignacionPuestoRecursoSinTrabajadores);
			
		break;
	case 5:
		break;
	}
	$("#btnSaveAsigRecurso").attr("onclick",functionText)
}
function procesarAsignacionPuestoRecurso(data){
	listFullPuestosRecurso = data.list;
	listFullPuestosRecurso.forEach(function(val){
		$("#tblAsigRecuro tbody").append("<tr>" +
				"<td>"+val.positionName+"</td>" +
				"<td><input type='checkbox' class='form-control' id='posAsig"+val.positionId+"' "+
					
					(val.isAsignado==1?"checked":"")+"></td>" +
				"<td><a class='efectoLink' onclick='verDetalleTrabajadoresAsigandosRecurso()'>"+
				val.numTrabajadoresAprobados+" / "+val.numTrabajadoresTotal+"</a></td>" +
				 
				"</tr>")
	});
}
function procesarAsignacionPuestoRecursoSinTrabajadores(data){
	listFullPuestosRecurso = data.list;
	listFullPuestosRecurso.forEach(function(val){
		$("#tblAsigRecuro tbody").append("<tr>" +
				"<td>"+val.positionName+"</td>" +
				"<td><input type='checkbox' class='form-control' id='posAsig"+val.positionId+"' "+
					
					(val.isAsignado==1?"checked":"")+"></td>" +
				 
				 
				"</tr>")
	});
}
function guardarAsignacionFormacion(){
	var listFormacion = [];
	listFullPuestosRecurso.forEach(function(val){
		listFormacion.push({capacitacionId : recursoEdicionObj.capacitacionId,
			positionId : val.positionId,
			isAsociado : ($("#posAsig"+val.positionId).prop("checked")?1:0) })
	});
	 
	callAjaxPost(URL+"/capacitacion/asignacion/puestos/save",listFormacion,function(data){
		$(".modal").modal("hide");
		alert("Asignaciones guardadas");
		cargarPrimerEstado();
	});
}
function guardarAsignacionExMedico(){
	var listExMedico = [];
	listFullPuestosRecurso.forEach(function(val){
		listExMedico.push({examenMedicoId : recursoEdicionObj.examenMedicoId,
			positionId : val.positionId,
			isAsociado : ($("#posAsig"+val.positionId).prop("checked")?1:0) })
	});
	 
	callAjaxPost(URL+"/examenmedico/asignacion/puestos/save",listExMedico,function(data){
		$(".modal").modal("hide");
		alert("Asignaciones guardadas");
		cargarPrimerEstado();
	});
}
function guardarAsignacionEquipoSeguridad(){
	var listEpp = [];
	listFullPuestosRecurso.forEach(function(val){
		listEpp.push({equipoSeguridadId : recursoEdicionObj.equipoSeguridadId,
			positionId : val.positionId,
			isAsociado : ($("#posAsig"+val.positionId).prop("checked")?1:0) })
	});
	 
	callAjaxPost(URL+"/equiposeguridad/asignacion/puestos/save",listEpp,function(data){
		$(".modal").modal("hide");
		alert("Asignaciones guardadas");
		cargarPrimerEstado();
	});
}
function guardarAsignacionProcedimiento(){
	var listProc = [];
	listFullPuestosRecurso.forEach(function(val){
		listProc.push({procedimientoSeguridadId : recursoEdicionObj.procedimientoSeguridadId,
			positionId : val.positionId,
			isAsociado : ($("#posAsig"+val.positionId).prop("checked")?1:0) })
	});
	 
	callAjaxPost(URL+"/procedimientoseguridad/asignacion/puestos/save",listProc,function(data){
		$(".modal").modal("hide");
		alert("Asignaciones guardadas");
		cargarPrimerEstado();
	});
}