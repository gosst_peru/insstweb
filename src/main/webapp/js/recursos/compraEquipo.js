var compraId;
var banderaEdicion12;
var listaFullCompraEpp;
var compraEstado;
var compraEstadoNombre;
var listEquiposTalla;
var listMotivosCompra;
var listMotivosRenovacion;
var listTiposUso;
var listEquiposTalla;
var tallasEquipoAux;
$(function(){
	$("#btnClipTablaCompra").on("click",function(){
		var listFullTablaProgramacion="";
		listFullTablaProgramacion="Motivo" +"\t"
		+"Fecha Compra Planificada" +"\t"
		+"Fecha Compra Real" +"\t"
		+"Inversión" +"\t"
		+"Evidencia" +"\t"
		+"Estado" +" \n";
		for (var index = 0; index < listaFullCompraEpp.length; index++){
			
		
			listFullTablaProgramacion=listFullTablaProgramacion
			+listaFullCompraEpp[index].motivoNombre+"\t"
			+listaFullCompraEpp[index].fechaPlanificadaTexto+"\t"
			+listaFullCompraEpp[index].fechaEntregaTexto+"\t"
			+listaFullCompraEpp[index].inversionEntregaEpp+"\t"
			+listaFullCompraEpp[index].numeroEquipos +"\t"
			+listaFullCompraEpp[index].numeroTrabajadores+"\t"
			+(listaFullCompraEpp[index].evidenciaNombre=="----"?"NO":"SI") +"\t"
			+listaFullCompraEpp[index].estadoImplementacionNombre
			+"\n";
		

		}
	 
		 
		copiarAlPortapapeles( obtenerDatosTablaEstandarNoFija("tblCompra"),"btnClipTablaCompra");
		
		alert("Se han guardado al clipboard la tabla de este módulo" );

	});
	

	
	
 	
})



function cambiarPaginaTablaCompra(){
	

	
	//Definiendo variables
	
	var list = listaFullCompraEpp;
	var totalSize=list.length;
	
	var partialSize=10;
	numPaginaTotal=Math.ceil(totalSize/partialSize);
	var inicioRegistro=(numPaginaActual*partialSize)-partialSize;
	var finRegistro=(numPaginaActual*partialSize)-1;
	
	//aplicando logica de compaginacion
	
	$('.izquierda_flecha').show();
	 $('.derecha_flecha').show();
	 if(numPaginaTotal==0){
			numPaginaTotal=1;
		}
		$("#labelFlechas").html(numPaginaActual+" de "+numPaginaTotal);
	 if(numPaginaActual==1){
			$('.izquierda_flecha').hide();
	 }
	
	 if(numPaginaActual==numPaginaTotal){
		 $('.derecha_flecha').hide();
	 }
	 
	if(finRegistro>totalSize){
		finRegistro=totalSize-1;
	}
	
$("#tblProg tbody tr").remove();
	for (var index = inicioRegistro; index <= finRegistro; index++) {

		 cantidadEPPP(list[index].compraId);
			$("#tblProg tbody").append(
					"<tr id='tr" + list[index].compraId
							+ "' onclick='javascript:editarEntregaEpp("
							+index+")' >"
							+ "<td id='tdmotivo"
							+ list[index].compraId + "'>"
							+ list[index].motivoNombre + "</td>"
						
							+ "<td id='tdfechaPlan"
							+ list[index].compraId + "'>"
							+ list[index].fechaPlanificadaTexto + "</td>"
							+ "<td id='tdhorapla"
							+ list[index].compraId + "'>"
							+ list[index].horaPlanificadaTexto + "</td>"
							+ "<td id='tdfechEntrega"
							+ list[index].compraId + "'>"
							+ list[index].fechaEntregaTexto + "</td>"
							+ "<td id='tdinversion"
							+ list[index].compraId + "'>S/  "
							+ list[index].inversionEntregaEpp + "</td>"
							+ "<td id='tdlistEpp"
							+ list[index].compraId + "'>"
							+ list[index].numeroEquipos+"</td>"
							+ "<td id='tdtrabEntrega"
							+ list[index].compraId + "'>"
							+ list[index].numeroTrabajadores+"</td>" 
							+ "<td id='tdevi" + list[index].compraId
							+ "'>"+list[index].evidenciaNombre+"</td>"
							+ "<td id='tdestadocompra" + list[index].compraId
							+ "'>"+list[index].estadoImplementacionNombre+"</td>"
							+"</tr>");
		
	}
}
function cargarComprasEpp() {
	
	var palabraClave=$("#buscadorTrabajadorInput").val();
	if(palabraClave.length==0){
		palabraClave=null;
	}else{
		palabraClave=("%"+palabraClave+"%").toUpperCase();
	}
	$("#btnAgregarCompra").attr("onclick", "javascript:nuevaCompraEPP();");
	$("#btnCancelarCompra").attr("onclick", "javascript:cancelarCompraEpp();");
	$("#btnGuardarCompra").attr("onclick", "javascript:guardarCompraEpp();");
	$("#btnEliminarCompra").attr("onclick", "javascript:eliminarCompraEpp();");
	
	$("#btnCancelarCompra").hide();
	$("#btnAgregarCompra").show();
	$("#btnEliminarCompra").hide();
	$("#btnGuardarCompra").hide();

	$("#modalCompraBody").show();
	$("#modalTallaCompra").hide();
	$("#btnVerCorreos").attr("onclick", "javascript:verCorreos();");
	$("#btnVerCorreos").hide();
	compraId = 0;
	listEquiposTalla=null;
	banderaEdicion12 = false;
	listaFullCompraEpp=[];
	listMotivosCompra=[];
	tallasEquipoAux=[];
	var dataParam = {
			eppId : equipoSeguridadId
	};

	callAjaxPost(URL + '/equiposeguridad/compras', dataParam, function(data) {
		switch (data.CODE_RESPONSE) {
		case "05":
			$("#tblCompra tbody tr").remove();
			var list = data.list;
			listaFullCompraEpp=data.list;
			listMotivosCompra=data.listMotivosCompra;
			listTiposUso=data.listTiposUso;
			listMotivosRenovacion=data.listMotivosRenovacion;
			
			listaFullCompraEpp.forEach(function(val,index){
				var totalTallas=0;
				val.tallasCompra.forEach(function(val1,index1){
					totalTallas+=val1.numTrabsTalla;
				});
				$("#tblCompra tbody").append(
				"<tr id='tr"+val.id+"' onclick='editarCompra("+index+")'>" +
				"<td id='tdmotcompra"+val.id+"'>"+val.motivoNombre+"</td>" +
				"<td id='tdmotadq"+val.id+"'>"+val.motivoRenovacionNombre+"</td>" +				
				"<td id='tddesccompra"+val.id+"'>"+val.descripcion+"</td>" +
				"<td id='tdfeccompra"+val.id+"'>"+val.fechaPlanificadaTexto+"</td>" +
				"<td id='tdhoracompra"+val.id+"'>"+val.horaPlanificadaTexto+"</td>" +
				"<td id='tdfecrealcompra"+val.id+"'>"+val.fechaRealTexto+"</td>" +
				"<td id='tdfecrealcompra"+val.id+"'>"+val.fechaProximaTexto+"</td>" +
				"<td id='tdcantidad"+val.id+"'>"+val.cantidad+"</td>" +
				"<td id='tdcantTalla"+val.id+"'> "	+totalTallas+" total</td>" +
				
				"<td id='tdinvcompra"+val.id+"'>"+val.inversion+"</td>" +
				"<td id='tdasocunidad"+val.id+"'>"+val.unidadNombre+"</td>" +
				"<td id='tdevicompra"+val.id+"'>"+val.evidenciaNombre+"</td>" +
				"<td id='tdusocompra"+val.id+"'>"+val.tipoUsoNombre+"</td>" +
				"<td id='tdestadocompra"+val.id+"'>"+val.estadoNombre+"</td>" +
				"</tr>"		
				);
			});
			
			$('#tblCompra td:nth-child(n)').show();
			switch(equipoSeguridadTipo){
			case 1:
				$('#tblCompra td:nth-child(1)').hide();
				$('#tblCompra td:nth-child(2)').hide();
				$('#tblCompra td:nth-child(3)').hide();
				$('#tblCompra td:nth-child(7)').hide();
				$('#tblCompra td:nth-child(8)').hide();
				$('#tblCompra td:nth-child(11)').hide();
				$('#tblCompra td:nth-child(13)').hide();
				break;
			case 2:
				$('#tblCompra td:nth-child(1)').hide();
				$('#tblCompra td:nth-child(8)').hide();
				$('#tblCompra td:nth-child(9)').hide();
				$('#tblCompra td:nth-child(13)').hide();
				break;
			case 3:
				$('#tblCompra td:nth-child(1)').hide();
				$('#tblCompra td:nth-child(2)').hide();
				$('#tblCompra td:nth-child(7)').hide();
				$('#tblCompra td:nth-child(9)').hide();
				break;
			case 4:
				
				$('#tblCompra td:nth-child(2)').hide();
				$('#tblCompra td:nth-child(3)').hide();
				$('#tblCompra td:nth-child(7)').hide();
				$('#tblCompra td:nth-child(9)').hide();
				break;
			}
			
			formatoCeldaSombreableTabla(true,"tblProg");
			if(getSession("linkCalendarioCompraId")!=null){
		 		
				listaFullCompraEpp.every(function(val,index3){
					if(val.id==parseInt(getSession("linkCalendarioCompraId"))){
						
						editarCompra(index3);
						sessionStorage.removeItem("linkCalendarioCompraId");
						return false;
					}else{
						return true;
					}
				});
				
			}
		
			break;
		default:
			alert("Ocurrió un error al traer las programaciones!");
		}
	});
}

function editarCompra(pindex) {

	if (!banderaEdicion12) {
		compraId = listaFullCompraEpp[pindex].id;
		var descripcion= listaFullCompraEpp[pindex].descripcion;
		var fechaReal= listaFullCompraEpp[pindex].fechaReal;
		listEquiposTalla=[];
		var cantidad=listaFullCompraEpp[pindex].cantidad;
		var estadoImpl=listaFullCompraEpp[pindex].estadoId;
		var fechaPlan=listaFullCompraEpp[pindex].fechaPlanificada;
		var inversion=listaFullCompraEpp[pindex].inversion;
		var motivoId=listaFullCompraEpp[pindex].motivoId;
		var motivoRenovacionId=listaFullCompraEpp[pindex].motivoRenovacionId;
		var tipoUsoId=listaFullCompraEpp[pindex].tipoUsoId;
		var unidadId=listaFullCompraEpp[pindex].unidadId;
		var horaPlanificada=listaFullCompraEpp[pindex].horaPlanificada;
		tallasEquipoAux=listaFullCompraEpp[pindex].tallasCompra;
		cargarCompraTallas();
		$("#modalTallaCompra").hide();
		$("#modalCompraBody").show();
$("#tddesccompra"+compraId).html(
		"<input type='text' id='inputDescCompra' class='form-control'>");
			nombrarInput("inputDescCompra",descripcion);
		
			var selMotivoCompra = crearSelectOneMenu("selMotivoCompra", "",
					listMotivosCompra, motivoId, "motivoId","motivoNombre");
		
		$("#tdmotcompra"+compraId).html(selMotivoCompra);
		var selMotivoRenovacion = crearSelectOneMenu("selMotivoRenovacion", "",
				listMotivosRenovacion, motivoRenovacionId, "motivoRenovacionId","motivoRenovacionNombre");
	
	$("#tdmotadq"+compraId).html(selMotivoRenovacion);
	var selTipoUso = crearSelectOneMenu("selTipoUso", "",
			listTiposUso, tipoUsoId, "tipoUsoId","tipoUsoNombre");

		$("#tdusocompra"+compraId).html(selTipoUso);
		var selUnidadCompra = crearSelectOneMenu("selUnidadCompra", "",
				unidadesBuscar, unidadId, "matrixId","matrixName");
		
		$("#tdasocunidad"+compraId).html(selUnidadCompra);
		
		formatoCeldaSombreableTabla(false,"tblProg");
		$("#tdcantidad"+compraId).html(
				"<input type='number' id='inputCantidadEpp'  " +
				"class='form-control' >"		
		);
		
		nombrarInput("inputCantidadEpp",cantidad);
		
		$("#tdinvcompra"+compraId).html(
				"<label for='inputPrecioCompra' style='float:left'>S/</label><input type='number' " 
				+"id='inputPrecioCompra' name='inputPrecioCompra' style='width:100px' " 
				+"class='form-control' value="+inversion+">");
	
		$("#tdcantTalla" + compraId)
		.html("<a id='linkTallaCompra' href='#' onclick='javascript:cargarCompraTallas();'>Link de Tallas</a>");

		$("#tdfecrealcompra" + compraId).html(
				"<input type='date' id='inputFecRealCompra' class='form-control' onchange='hallarEstadoImplementacionCompra("+compraId+")'>");
		$("#tdfeccompra" + compraId).html(
				"<input type='date' id='inputFecPlanCompra' class='form-control' onchange='hallarEstadoImplementacionCompra("+compraId+")'>");
		$("#tdhoracompra" + compraId).html(
				"<input type='time' id='inputHoraCompra' class='form-control'>");
		var fechaReal = convertirFechaInput(fechaReal);
		var fechaPlan = convertirFechaInput(fechaPlan);
		
	
		$("#inputHoraCompra").val(horaPlanificada);
		$("#inputFecRealCompra").val(fechaReal);
		$("#inputFecPlanCompra").val(fechaPlan);
		hallarEstadoImplementacionCompra(compraId);
		var nombreEvidencia=$("#tdevicompra" + compraId)
		.text();
		$("#tdevicompra" + compraId)
		.html(
				"<a href='"
						+ URL
						+ "/equiposeguridad/compra/evidencia?compraId="
						+ compraId
						+ "' "
						+ "target='_blank'>" +
						nombreEvidencia+	"</a>"
						+ "<br/><a href='#' onclick='javascript:mostrarCargarCompra("
						+ compraId + ")'  id='subirimagen"
						+ compraId + "'>Subir</a>");
		banderaEdicion12 = true;
		$("#btnCancelarCompra").show();
		$("#btnAgregarCompra").hide();
		$("#btnEliminarCompra").show();
		$("#btnGuardarCompra").show();
		
	}
}


function nuevaCompraEPP() {
	if (!banderaEdicion12) {

		var selMotivoCompra = crearSelectOneMenu("selMotivoCompra", "",
				listMotivosCompra, "-1", "motivoId",
				"motivoNombre");
		var selMotivoRenovacion = crearSelectOneMenu("selMotivoRenovacion", "",
				listMotivosRenovacion, "-1", "motivoRenovacionId","motivoRenovacionNombre");
	
	var selTipoUso = crearSelectOneMenu("selTipoUso", "",
			listTiposUso, "-1", "tipoUsoId","tipoUsoNombre");

		var selUnidadCompra = crearSelectOneMenu("selUnidadCompra", "",
				unidadesBuscar, "-1", "matrixId","matrixName");
		$("#tblCompra tbody")
				.append(
						"<tr id='tr0'>"
						+ "<td>"+selMotivoCompra+"</td>"
						+ "<td>"+selMotivoRenovacion+"</td>"
						+ "<td><input type='text' id='inputDescCompra' class='form-control'></td>"
						
						+ "<td><input type='date' id='inputFecPlanCompra' class='form-control' onchange='hallarEstadoImplementacionCompra(0)'></td>"
						+ "<td><input type='time' value='12:00:00' id='inputHoraCompra' class='form-control' ></td>"									
						+ "<td><input type='date' id='inputFecRealCompra' class='form-control' onchange='hallarEstadoImplementacionCompra(0)'></td>"
						+ "<td>...</td>"
						+ "<td><input type='number' id='inputCantidadEpp' class='form-control'></td>"
						+ "<td>...</td>"
						
						+ "<td>" +
						"<label for='inputPrecioCompra' style='float:left'>S/</label><input type='number' " 
						+"id='inputPrecioCompra' name='inputPrecioCompra' style='width:100px' " 
						+"class='form-control' value="+0+">"+
								"</td>"
						+ "<td>"+selUnidadCompra+"</td>"
						+ "<td >...</td>"
						
						+ "<td>"+selTipoUso+"</td>"
								+ "<td id='tdestadocompra0'>...</td>"
								+ "</tr>");
		compraId = 0;

		$('#tblCompra td:nth-child(n)').show();
		switch(equipoSeguridadTipo){
		case 1:
			$('#tblCompra td:nth-child(1)').hide();
			$('#tblCompra td:nth-child(2)').hide();
			$('#tblCompra td:nth-child(3)').hide();
			$('#tblCompra td:nth-child(7)').hide();
			$('#tblCompra td:nth-child(8)').hide();
			$('#tblCompra td:nth-child(11)').hide();
			$('#tblCompra td:nth-child(13)').hide();
			break;
		case 2:
			$('#tblCompra td:nth-child(1)').hide();
			$('#tblCompra td:nth-child(8)').hide();
			$('#tblCompra td:nth-child(9)').hide();
			$('#tblCompra td:nth-child(13)').hide();
			break;
		case 3:
			$('#tblCompra td:nth-child(1)').hide();
			$('#tblCompra td:nth-child(2)').hide();
			$('#tblCompra td:nth-child(7)').hide();
			$('#tblCompra td:nth-child(9)').hide();
			break;
		case 4:
			
			$('#tblCompra td:nth-child(2)').hide();
			$('#tblCompra td:nth-child(3)').hide();
			$('#tblCompra td:nth-child(7)').hide();
			$('#tblCompra td:nth-child(9)').hide();
			break;
		}
		$("#btnCancelarCompra").show();
		$("#btnAgregarCompra").hide();
		$("#btnEliminarCompra").hide();
		$("#btnGuardarCompra").show();
		
		
		banderaEdicion12 = true;
	} else {
		alert("Guarde primero.");
	}
}
function cargarCompraTallas(){
	$("#modalTallaCompra").show();
	$("#modalCompraBody").hide();
	$("#tblTallaCompra tbody tr").remove();
	tallasEquipoAux.forEach(function(val,index){
		$("#tblTallaCompra tbody").append(
				"<tr>" +
				"<td>" +val.nombre+"</td>"+
				"<td>" +"<input type='number' class='form-control' id='inputCantidadTalla"+val.id+"'>"+"</td>"+
				"</tr>");
		nombrarInput("inputCantidadTalla"+val.id+"",val.numTrabsTalla);
	});
	console.log(tallasEquipoAux);
}
function cancelarCargaTallasCompra(){
	$("#modalTallaCompra").hide();
	$("#modalCompraBody").show();
}
function cancelarCompraEpp() {
	cargarComprasEpp();
}

function eliminarCompraEpp() {
	var r = confirm("¿Está seguro de eliminar la adquisición?");
	if (r == true) {
		var dataParam = {
			id : compraId,
		};

		callAjaxPost(URL + '/equiposeguridad/compra/delete', dataParam,
				function(data) {
					switch (data.CODE_RESPONSE) {
					case "05":
						cargarComprasEpp();
						break;
					default:
						alert("Ocurrió un error al eliminar la compra!");
					}
				});
	}
}

function guardarCompraEpp() {

	var campoVacio = true;
var motivoCompraId=$("#selMotivoCompra").val();
	var inversion =$("#inputPrecioCompra").val();
	var horaPlanificada=$("#inputHoraCompra").val();
	var mes1 = $("#inputFecRealCompra").val().substring(5, 7) - 1;
	var fechatemp1 = new Date($("#inputFecRealCompra").val().substring(0, 4), mes1, $(
			"#inputFecRealCompra").val().substring(8, 10));

	var inputFecReal = fechatemp1;
	
	var mes2 = $("#inputFecPlanCompra").val().substring(5, 7) - 1;
	var fechatemp2 = new Date($("#inputFecPlanCompra").val().substring(0, 4), mes2, $(
			"#inputFecPlanCompra").val().substring(8, 10));
	
	var inputFecPla = fechatemp2;
	
if(!$("#inputFecRealCompra").val()){
		
	inputFecReal=null;
	}
if(!$("#inputFecPlanCompra").val()){
	
	inputFecPla=null;
}
tallasEquipoAux.forEach(function(val,index){
val.numTrabsTalla=$("#inputCantidadTalla"+val.id).val();
val.compraId=compraId
});
	if (campoVacio) {

		var dataParam = {
			tallasCompra:tallasEquipoAux,
			id : compraId,
			fechaPlanificada : inputFecPla,
			horaPlanificada:horaPlanificada,
			fechaReal:inputFecReal,
			eppId:equipoSeguridadId,
			inversion:inversion,
			descripcion:$("#inputDescCompra").val(),
			cantidad:$("#inputCantidadEpp").val(),
			estadoId:compraEstado,
			motivoId:motivoCompraId=="-1"?null:motivoCompraId,
			motivoRenovacionId:$("#selMotivoRenovacion").val()=="-1"?null:$("#selMotivoRenovacion").val(),
			tipoUsoId:$("#selTipoUso").val()=="-1"?null:$("#selTipoUso").val(),
			unidadId:$("#selUnidadCompra").val()=="-1"?null:$("#selUnidadCompra").val()
		};
		console.log(dataParam);
		callAjaxPost(URL + '/equiposeguridad/compra/save', dataParam,
				function(data) {
					switch (data.CODE_RESPONSE) {
					case "05":
						cargarComprasEpp();
						break;
					default:
						alert("Ocurrió un error al guardar la programacion!");
					}
				},loadingCelda,"tblCompra #tr"+compraId);
	} else {
		alert("Debe ingresar todos los campos.");
	}
}
function mostrarCargarCompra(compraId) {
	$('#mdUpload').modal('show');
	$('#btnUploadArchivo').attr("onclick",
			"javascript:uploadArchivoCompra(" + compraId + ");");
}

function uploadArchivoCompra(progEquipoId) {
	var inputFileImage = document.getElementById("fileEvi");
	var file = inputFileImage.files[0];
	var data = new FormData();

	if (file.size > bitsEvidenciaEPP) {
		alert("Lo sentimos, solo se pueden subir archivos menores a "+bitsEvidenciaEPP/1000000+" MB");
	} else {
		data.append("fileEvi", file);
		data.append("compraId", progEquipoId);
		
		var url = URL + '/equiposeguridad/compra/evidencia/save';
		$.blockUI({
			message : 'cargando...'
		});
		$
				.ajax({
					url : url,
					xhrFields: {
			            withCredentials: true
			        },
					type : 'POST',
					contentType : false,
					data : data,
					processData : false,
					cache : false,
					success : function(data, textStatus, jqXHR) {
						$.unblockUI();
						switch (data.CODE_RESPONSE) {
						case "06":
							sessionStorage.clear();
							document.location.replace(data.PATH);
							break;
						default:
							console.log('Se subio el archivo correctamente.');
							$('#mdUpload').modal('hide');
						}

					},
					error : function(jqXHR, textStatus, errorThrown) {
						$.unblockUI();
						alert("Ups, tenemos problemas tecnicos. Intente más tarde. Error: "
								+ errorThrown);
						console.log('xhRequest: ' + jqXHR + "\n");
						console.log('ErrorText: ' + textStatus + "\n");
						console.log('thrownError: ' + errorThrown + "\n");
					}
				});
	}
}

function hallarEstadoImplementacionCompra(progId){
	var fechaPlanificada=$("#inputFecPlanCompra").val();
	var fechaReal=$("#inputFecRealCompra").val();
	var fechaHoy=obtenerFechaActual();
	if(fechaReal!=''){
		compraEstado=2;
		compraEstadoNombre="Completado";
	}else{
		
		if(fechaPlanificada!=""){
			var dif=restaFechas(fechaHoy,fechaPlanificada)
			if(dif<0){
				compraEstado=3;
				compraEstadoNombre="Retrasado";
			}else{
				compraEstado=1;
				compraEstadoNombre="Por implementar";
			}
			
		}else{
			
			compraEstado=1;
			compraEstadoNombre="Por implementar";
			
		}
	}
	
	$("#tdestadocompra"+progId).html(compraEstadoNombre);
	
}

