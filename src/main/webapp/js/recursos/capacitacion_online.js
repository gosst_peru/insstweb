var banderaEdicion;
var capacitacionId;
var capacitacionNombre;
var capacitacionTipo;
var clasificacionId;
var listClasificacion;
var listCapacitacionTipo;
var listTiposVigencia;
var contador2 = 0;

var asigAux=[];
var vigAux=[];
var listFullFormaciones;
var listTrabajdores;
var listanombretab;
var capacitacionObj;
$(document).ready(function() {

	insertMenu();
	cargarPrimerEstado();
	resizeDivGosst("wrapper");
	resizeDivGosst("wrapperEval");
	$("#btnClipboardSmart").on("click",function(){
		var texto=obtenerDatosTablaEstandarNoFija("tblTrabFormacion");
	 copiarAlPortapapeles(texto,"btnClipboardSmart");
		alert("Se han guardado al clipboard la tabla " );

	
	});
	$(document).on("click","#btnClipTabla",function(){
		
				var fullDatosTabla="";
				 $(".fix-head #"+"tblCap"+" thead tr").each(function (index){   
			         $(this).children("th").each(function (index2){
			        	 if(index2<7){
			              fullDatosTabla =fullDatosTabla+ $(this).text()+"\t";
			        	 }
			         })
			         fullDatosTabla=fullDatosTabla+"\n"
			     });
				 
					for (index = 0; index < listFullFormaciones.length; index++) {
						fullDatosTabla=fullDatosTabla
						+listFullFormaciones[index].capacitacionNombre.replace("\t","") +"\t"
						+listFullFormaciones[index].clasificacionNombre.replace("\t","") +"\t"
						+listFullFormaciones[index].responsable.replace("\t","") +"\t"
						+listFullFormaciones[index].tema.replace("\t","") +"\t"
						+(listFullFormaciones[index].vigencia.id==null?
								"---":listFullFormaciones[index].vigencia.nombre)				
						+ "\n";			
				
					}
				

					 
					
					 copiarAlPortapapeles(fullDatosTabla); 
				
		
		
		alert("Se han guardado al clipboard la tabla de este módulo" );
		
	});
	$("#btnSmartProg").on("click",function(){
		listTrabajadoresPlanisSelecccionados=asigAux.filter(function(age){
			  return (vigAux.indexOf(age)==-1?true:false);
			});
	
	
		var r=true;
		r = confirm("Desea programar para mañana un evento de formación para "+
				listTrabajadoresPlanisSelecccionados.length+" trabajador(es)?");
		if (r) {
			var fechaManana=sumaFechaDias(1,obtenerFechaActual());
			var mes1 = fechaManana.substring(5, 7) - 1;
			var fechatemp1 = new Date(fechaManana.substring(0, 4), mes1, 
					fechaManana.substring(8, 10));

			var inputFecPla = fechatemp1;
			var dataParam = {
				programacionCapId : 0,
				fechaPlanificada : inputFecPla,
				fechaRealizada : null,
				inversionProgramacionCap:0,
				capacitacionId : capacitacionId,
				horaPlanificada:null,
				trabajadoresPlanificados: listTrabajadoresPlanisSelecccionados,
				progCapEstado:1,
				idCompany : sessionStorage.getItem("gestopcompanyid")
			};

			
			
			callAjaxPost(URL + '/capacitacion/programacion/save', dataParam,
					function(data) {
						switch (data.CODE_RESPONSE) {
						case "05":
							alert("Evento Creado!")
							$("#modalFormacionTrabajadores").modal("hide");
							break;
						default:
							console.log("Ocurrió un error al guardar la programacion!");
						}
					},function(){ 
						$("#mensajeGuardando").remove();
						$("#tblTrabFormacion").before("<p id='mensajeGuardando'>Guardando...</p>")},null);
			
		
		
		} 
	})
});

function cargarPrimerEstado() {
	banderaEdicion = false;
	capacitacionId = 0;
	capacitacionTipo = 0;
	clasificacionId = 0;
	
	removerBotones();
	crearBotones();
	
	$("#fsBotones")
			.append(
					"<button id='btnProgFecha' type='button' class='btn btn-success' title='Programar Fecha'>"
							+ "<i class='fa fa-calendar fa-2x'></i>"
							+ "</button>");
	$("#fsBotones")
	.append(
			"<button id='btnClipTabla' type='button' class='btn btn-success' title='Tabla Clipboard' >"
					+ "<i class='fa fa-clipboard fa-2x'></i>" + "</button>");
	
	$("#btnUpload").attr("onclick", "javascript:importarDatos();");
	
	deshabilitarBotonesEdicion();
	$("#btnProgFecha").hide();

	$("#btnNuevo").attr("onclick", "javascript:nuevoCapacitacion();");
	$("#btnCancelar").attr("onclick", "javascript:cancelarCapacitacion();");
	$("#btnGuardar").attr("onclick", "javascript:guardarCapacitacion();");
	$("#btnEliminar").attr("onclick", "javascript:eliminarCapacitacion();");
	$("#btnProgFecha").attr("onclick", "javascript:programarCurso();");
	$("#btnImportar").attr("onclick", "javascript:importarDatos();");
	var dataParam = {
		idCompany : sessionStorage.getItem("gestopcompanyid")
	};
	if(parseInt(sessionStorage.getItem("accesoUsuarios")) == 0){
		var capObj={
				trabajadorId:parseInt(sessionStorage.getItem("trabajadorGosstId"))
		}
		$("#containerFormacionGosst").remove();
		callAjaxPost(URL + '/capacitacion', capObj,
				procesarDataCapacitacionOnline);
	}else{
		 $("#containerFormacionTrabajador").remove();
		 
		 callAjaxPost(URL + '/capacitacion', dataParam,
					procesarDataDescargadaPrimerEstado);
	}
	
	
}
function procesarDataCapacitacionOnline(data){
	
	listFullFormaciones=data.list;
	$("#cursosImplementar .divListCursos").html("");
	$("#cursosCompletados .divListCursos").html("");
	var capImplCounter=0,capComplCounter=0;
	listFullFormaciones.forEach(function(val,index){
		
		var cursosCap=val.cursos;
		
		cursosCap.forEach(function(val1,index1){
			var imagenCap="<img></img>";
			
			if(val1.evidencia!=null){
				imagenCap=	insertarImagenParaTablaMovil(val1.evidencia,"300px","100%");	
			}
			var classFormacionHide="";
			var textoSuspensivo="";
			if(val1.nombre.length>90){
				textoSuspensivo=" ..."
			}
			var textoCap=""+val1.nombre.substring(0,90)+textoSuspensivo+"<br>";
			var textoCertificado="<i class='fa fa-certificate fa-2x cerrado-gosst' title='Curso cerrado' ></i>" +
					"<a class='btn btn-success' onclick='verDetallesCurso("+index+","+index1+",2)'>Ver Detalles</a>";
			var puntajeNoAprobado=true;
			if(val1.evaluacion==null){
				puntajeNoAprobado=true;
			}else{
			 
				if((val1.puntajeMinimo/100)>val1.evaluacion.puntajeAlcanzado){
					 
					puntajeNoAprobado=true;
					textoCertificado="<i class='fa fa-certificate fa-2x cerrado-gosst' title='Curso cerrado' ></i>" +
							"<a  class='btn btn-success' role='button'  onclick='verDetallesCurso("+index+","+index1+",2)'>Ver Detalles</a>"
				}else{
					puntajeNoAprobado=false;
					textoCertificado=
						"<i onclick='javascript:descargarCertificadoCurso("+val1.id+")' " +
						"class='fa fa-certificate fa-2x aprobado-gosst' " +
						"title='Descargar Certificado'></i>" +
						"<a  class='btn btn-success' role='button'  onclick='verDetallesCurso("+index+","+index1+",1)'>Ver Detalles</a>";
				}
			}
			 
			if(val1.estado.id==1 && puntajeNoAprobado){
				capImplCounter+=1;
				if(capImplCounter>4){
					classFormacionHide="classFormacionHide";
				}else{
					classFormacionHide="";
				}
				$("#cursosImplementar .divListCursos")
				.append("<div class='divFormacionOnline "+classFormacionHide+" col-sm-6 col-md-3 col-lg-2'>" +
						"<div class='imgFormacion thumbnail' > " +
						"<a>"+
						imagenCap+
						"</a>"+
						"<div class='divDescFormacion'>" +
						textoCap+"<br><a class='btn btn-success' role='button' onclick='verDetallesCurso("+index+","+index1+",0)'>Iniciar Curso</a>"+
						" </div>" +
						" </div>" +
						"</div>");
			}else{
				capComplCounter+=1;
				if(capComplCounter>4){
					classFormacionHide="classFormacionHide";
				}else{
					classFormacionHide="";
				}
				$("#cursosCompletados .divListCursos")
				.append("<div class='divFormacionOnline "+classFormacionHide+" col-sm-6 col-md-3 col-lg-2'>" +
						"<div class='imgFormacion thumbnail'> " +
						"<a>"+
						imagenCap+
						"</a>"+
						"<div class='divDescFormacion'>" +
						textoCap+"<br>"+
						textoCertificado+
						""+
						" </div>" +
						" </div>" +
						"</div>");
			}
			
		})
		
		
	});
	completarBarraCarga();
}
function nombreClasifCap(idTipo,nombreTipo){
	if(idTipo==4){
		return nombreTipo
	}else{
		return "Ninguna"
	}
}

function procesarDataDescargadaPrimerEstado(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		var list = data.list;
listFullFormaciones=list;
		listClasificacion = data.listCla;
		listCapacitacionTipo = data.listCapTipo;
		listTiposVigencia=data.listTiposVigencia;
		$("#h2Titulo")
				.html(
						"<a onclick='javascript:cargarPrimerEstado();' href='#'>Capacitaciones</a>");
		$("#tblCap tbody tr").remove(); 
		for (index = 0; index < list.length; index++) {
			if(list[index].capacitacionTipoId==5){
				
			
			var listAprobadosReal=dividirLista(list[index].listTrabVigentesAprobados);
			var lastListAprobadosReal=dividirLista(list[index].lastListTrabVigentesAprobados);
var capacitacionTipoId=list[index].capacitacionTipoId;
			$("#tblCap tbody").append(

					"<tr id='tr" + list[index].capacitacionId
							+ "' onclick='javascript:editarCapacitacion("
							+ index+")' >"

							+ "<td id='tdnom" + list[index].capacitacionId
							+ "'>" + list[index].capacitacionNombre + "</td>"
							
							+ "<td id='tdcla"
							+ list[index].capacitacionId + "'>"
							+ list[index].clasificacionNombre + "</td>"
							+ "<td id='tdres" + list[index].capacitacionId
							+ "'>" + list[index].responsable + "</td>"
							+ "<td id='tdtem" + list[index].capacitacionId
							+ "'>" + list[index].tema + "</td>"
							+ "<td id='tdvig" + list[index].capacitacionId
							+ "'>" + list[index].vigencia.nombre + "</td>"
							
							+ "<td id='tdTrabAsig" + list[index].capacitacionId
							+ "'>" +(capacitacionTipoId==5? lastListAprobadosReal.length:  listAprobadosReal.length ) + "/"+list[index].numTrabAsignados+"" +
									"</td>"
							+ "<td id='tdProg" + list[index].capacitacionId
							+ "'>" +(capacitacionTipoId==0?"---": list[index].numProgramacionesRealizadas + "/"+list[index].numProgramacionesPlaneadas )+ "</td>"
							 + "</tr>");
		}
		}
		formatoCeldaSombreableTabla(true,"tblCap");completarBarraCarga();
		
		if(getSession("linkCalendarioCapId")!=null){
			var indexIpercAux=0;
			listFullFormaciones.every(function(val,index3){
				
				if(val.capacitacionId==parseInt(getSession("linkCalendarioCapId"))){
					editarCapacitacion(index3);
					programarFecha();
					
					sessionStorage.removeItem("linkCalendarioCapId");
					return false;
				}else{
					return true;
				}
			});
			
		}
		break;
	default:
		alert("Ocurrió un error al traer las capacitaciones!");
	}
	
	
	if (contador2 == 0) {
		goheadfixed('table.fixed');
	}
	contador2 = contador2 + 1;
}
function verEstadoCapacitacionesTrabajadores(){
	var dataParam={capacitacionId:capacitacionId};
	callAjaxPost(
			URL + '/capacitacion/trabajador/formacion',
			dataParam,function(data){
				if(data.CODE_RESPONSE=="05"){
					var trabajadoresAsignados=data.trabajadoresAsignados;
					var trabajadoresVigentes=data.trabajadoresVigentes;
					var trabajadoresPlanificados=data.trabajadoresPlanificados;
					
					$("#modalFormacionTrabajadores").modal("show");
					$("#modalFormacionTrabajadores .modal-title").html("Resumen de capacitación: "+capacitacionObj.capacitacionNombre+" - "+capacitacionObj.tema);
					$("#modalFormacionTrabajadores .modal-body").show();
					$("#tblTrabFormacion tbody tr").remove();
					vigAux=[];
					asigAux=[]; 
					for(index=0;index<trabajadoresAsignados.length;index++){
						if(trabajadoresAsignados[index].estadoPlanificacion!=1){
							asigAux.push(trabajadoresAsignados[index].trabajadorId);	
						}
						
						$("#tblTrabFormacion tbody").append("" +
								
					"<tr id='ft"+trabajadoresAsignados[index].trabajadorId+"'>" +
					"<td>"+trabajadoresAsignados[index].unidadNombre+"</td>" +
					"<td>"+trabajadoresAsignados[index].divisionNombre+"</td>" +
					"<td>"+trabajadoresAsignados[index].areaNombre+"</td>" +
					"<td>"+trabajadoresAsignados[index].puestoNombre+"</td>" +
					"<td>"+trabajadoresAsignados[index].trabajadorNombre+"</td>" +
					"<td>"+"Sin asignar"+"</td>" +
					"<td>"+"---"+"</td>" +
					"<td>"+"---"+"</td>" +
					"<td>"+"---"+"</td>" +
					"<td>"+"---"+"</td>" +
					"</tr>")	;
						$("#ft"+trabajadoresAsignados[index].trabajadorId+" td").css({
							"background-color":"#C0504D"
						})
					}
					for(index=0;index<trabajadoresPlanificados.length;index++){
						if(trabajadoresPlanificados[index].fechaFutura.trim()!=""){
							vigAux.push(trabajadoresPlanificados[index].trabajadorId);
							$("#ft"+trabajadoresPlanificados[index].trabajadorId).remove();
							$("#tblTrabFormacion tbody").prepend("" +
									
									"<tr id='ft"+trabajadoresPlanificados[index].trabajadorId+"'>" +
									"<td>"+trabajadoresAsignados[index].unidadNombre+"</td>" +
									"<td>"+trabajadoresAsignados[index].divisionNombre+"</td>" +
									"<td>"+trabajadoresAsignados[index].areaNombre+"</td>" +
									"<td>"+trabajadoresAsignados[index].puestoNombre+"</td>" + 
									"<td>"+trabajadoresPlanificados[index].trabajadorNombre+"</td>" +
									
									"<td>"+"Planificado"+"</td>" +
									"<td>"+"---"+"</td>" +
									"<td>"+"---"+"</td>" +
									"<td>"+"---"+"</td>" +
									"<td>"+trabajadoresPlanificados[index].fechaFutura+"</td>" +
									"</tr>");
							
							
							
							$("#ft"+trabajadoresPlanificados[index].trabajadorId+" td").css({
								"background-color":"#F79646"
							})
						}
						
					}
					for(index=0;index<trabajadoresVigentes.length;index++){
						vigAux.push(trabajadoresVigentes[index].trabajadorId);
						$("#ft"+trabajadoresVigentes[index].trabajadorId).remove()	;
						$("#tblTrabFormacion tbody").prepend("" +
								
								"<tr id='ft"+trabajadoresVigentes[index].trabajadorId+"'>" +
								"<td>"+trabajadoresVigentes[index].unidadNombre+"</td>" +
								"<td>"+trabajadoresVigentes[index].divisionNombre+"</td>" +
								"<td>"+trabajadoresVigentes[index].areaNombre+"</td>" +
								"<td>"+trabajadoresVigentes[index].puestoNombre+"</td>" + 
								"<td>"+trabajadoresVigentes[index].trabajadorNombre+"</td>" +
								"<td>"+"Aprobado"+"</td>" +
								"<td>"+trabajadoresVigentes[index].notaExamen+"</td>" +
								"<td>"+trabajadoresVigentes[index].diasRestantes+"</td>" +
								"<td>"+trabajadoresVigentes[index].intentos+"</td>" +
								"<td>"+trabajadoresVigentes[index].fechaFutura+"</td>" +
								"</tr>");
						$("#ft"+trabajadoresVigentes[index].trabajadorId+" td").css({
							"background-color":"#9BBB59"
						})
					}
					
				}else{
					console.log("NOP")
				}
			})
}
function editarCapacitacion(pindex) {
	if (!banderaEdicion) {
		
		capacitacionObj=listFullFormaciones[pindex];
		capacitacionId = listFullFormaciones[pindex].capacitacionId ;
		capacitacionNombre= listFullFormaciones[pindex].capacitacionNombre ;
		capacitacionTipo = listFullFormaciones[pindex].capacitacionTipoId;
		clasificacionId = listFullFormaciones[pindex].clasificacionId;
		var vigenciaId=listFullFormaciones[pindex].vigencia.id;
		formatoCeldaSombreableTabla(false,"tblCap");
		var name = $("#tdnom" + capacitacionId).text();
		var iconoSmart=" <i class='fa fa-magic' aria-hidden='true'></i>";
		$("#tdTrabAsig"+capacitacionId).append(iconoSmart);
		$("#tdTrabAsig"+capacitacionId+" i").on("click",function(){
			verEstadoCapacitacionesTrabajadores()
		});
		$("#mdProgFechaLabel").html("Programaci&oacute;n de '"+name+"'");
		$("#tdnom" + capacitacionId)
				.html(
						"<input type='text' id='inputCap' class='form-control' placeholder='Capacitacion' autofocus='true' value='"
								+ name + "'>");
		var name1 = $("#tdres" + capacitacionId).text();
		$("#tdres" + capacitacionId)
				.html(
						"<input type='text' id='inputRes' class='form-control' placeholder='Responsable' value='"
								+ name1 + "'>");
		ponerListaSugerida("inputRes",listanombretab,true);
		var name2 = $("#tdtem" + capacitacionId).text();
		$("#tdtem" + capacitacionId)
				.html(
						"<input type='text' id='inputTem' class='form-control' placeholder='Tema' autofocus='true' value='"
								+ name2 + "'>");
		var name3 = $("#tdhor" + capacitacionId).text();
		$("#tdhor" + capacitacionId)
				.html(
						"<input type='number' id='inputHor' class='form-control' placeholder='Hora programada' autofocus='true' value='"
								+ name3 + "'>");
		var name4 = $("#tdvid" + capacitacionId).text();
		$("#tdvid" + capacitacionId)
				.html(
						"<input type='text' id='inputUrlVideo' class='form-control' placeholder='Url Video' autofocus='true' value='"
								+ name4 + "'>");
		/** ************************************************************************* */
		
		var selClaTipo = crearSelectOneMenu("selClaTipo", "",
				listClasificacion, clasificacionId, "clasificacionId", "nombre");
		$("#tdcla" + capacitacionId).html(selClaTipo);

		var selTipoVig = crearSelectOneMenu("selTipoVig", "",
				listTiposVigencia, vigenciaId, "id", "nombre");
		$("#tdvig" + capacitacionId).html(selTipoVig);
		banderaEdicion = true;
		habilitarBotonesEdicion();
		$("#btnProgFecha").show();
		
		
		
	}
}

function nuevoCapacitacion() {
	if (!banderaEdicion) {
		var selCapTipo = crearSelectOneMenu("selCapTipo", "",
				listCapacitacionTipo, "-1", "selCapId", "nombre");
		var selClaTipo = crearSelectOneMenu("selClaTipo", "",
				listClasificacion, "-1", "clasificacionId", "nombre");
		var selTipoVig = crearSelectOneMenu("selTipoVig", "",
				listTiposVigencia, "-1", "id", "nombre");
		$("#tblCap tbody:first")
				.append(
						"<tr id='tr0'>"
								+ "<td><input type='text' id='inputCap' class='form-control' placeholder='Formaci&oacute;n' required='true' autofocus='true'></td>"
								
								+ "<td>"
								+ selClaTipo
								+ "</td>"
								+ "<td><input type='text' id='inputRes' class='form-control' placeholder='Responsable' required='true' autofocus='true'></td>"
								+ "<td><input type='text' id='inputTem' class='form-control' placeholder='Tema' required='true' autofocus='true'></td>"
								+ "<td>"
								+ selTipoVig
								+ "</td>"
								+ "<td>...</td>"
								+ "<td>...</td>"
								+ "</tr>");
		$("#inputCap").focus();
		formatoCeldaSombreableTabla(false,"tblCap");
		ponerListaSugerida("inputRes",listanombretab,true);
		capacitacionId = 0;
		habilitarBotonesNuevo();
		banderaEdicion = true;
	} else {
		alert("Guarde primero.");
	}
}

function cancelarCapacitacion() {
	cargarPrimerEstado();
}

function eliminarCapacitacion() {
	var r = confirm("¿Está seguro de eliminar la capacitacion?");
	if (r == true) {
		var dataParam = {
			capacitacionId : capacitacionId,
		};

		callAjaxPost(URL + '/capacitacion/delete', dataParam,
				procesarResultadoEliminarCapacitacion);
	}
}

function procesarResultadoEliminarCapacitacion(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		cargarPrimerEstado();
		break;
	default:
		alert("Ocurrió un error al eliminar el Capacitacion!");
	}
}

function guardarCapacitacion() {

	var campoVacio = true;
	var capacitacionTipo = 5;
	var clasificacionTipo = $("#selClaTipo option:selected").val();
	var selTipoVig=$("#selTipoVig").val();
	var inputCap = $("#inputCap").val();
	var inputRes = $("#inputRes").val();
	var inputTem = $("#inputTem").val();  

	if (capacitacionTipo == '-1' || clasificacionTipo == '-1'
			|| inputCap.length == 0 || inputRes.length == 0
			|| inputTem.length == 0  ) {
		campoVacio = false;
	}
if(selTipoVig=="-1"){
	selTipoVig=null;
}
	if (campoVacio) {

		var dataParam = {
			capacitacionId : capacitacionId,
			capacitacionTipoId:5,
			clasificacionId : clasificacionTipo,
			capacitacionNombre : inputCap,
			responsable : inputRes,
			tema : inputTem, 
			capacitacionTipoId : capacitacionTipo, 
			vigencia:{id:selTipoVig},
			idCompany : sessionStorage.getItem("gestopcompanyid")
		};
var dataParamBefore="tr"+capacitacionId;
		callAjaxPost(URL + '/capacitacion/save', dataParam,
				procesarResultadoGuardarCap,loadingCelda,dataParamBefore);
	} else {
		alert("Debe ingresar todos los campos.");
	}
}

function procesarResultadoGuardarCap(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		cargarPrimerEstado();
		break;
	default:
		alert("Ocurrió un error al guardar la capacitacion!");
	}
}

function importarDatos() {
	$("#btnGuardarNuevos").attr("onclick",
			"javascript:guardarMasivoCopiaExcel();");

	$('#mdImpDatos').modal('show');
	
}
function guardarMasivoCopiaExcel() {
	var texto = $("#txtListado").val();
	var listExcel = texto.split('\n');
	var listRecurso = [];
	var listFormacionRepe = [];
	var listFullNombres = "";
	var validado = "";
	if (texto.length < 10000) {
		for (var int = 0; int < listExcel.length; int++) {

			var listCells = listExcel[int].split('\t');
			var filaTexto = listExcel[int];
			if (filaTexto.trim().length < 1) {
				validado = "Existen filas vacias.";
				break;
			}

			if (validado.length < 1 && listCells.length != 6) {
				validado = "No coincide el numero de celdas. ("
						+ listCells.length+"!=6 )";
				break;
			} else {
				var recurs = {};
				var tipo = {};
				var clas={};
				for (var j = 0; j < listCells.length; j++) {
					var element = listCells[j];
					if (j === 0) {
						recurs.capacitacionNombre = element.trim();
						if ($.inArray(recurs.capacitacionNombre, listFormacionRepe) === -1) {
							listFormacionRepe.push(recurs.capacitacionNombre);
						} else {
							listFullNombres = listFullNombres + recurs.capacitacionNombre + ", ";
						}

					}
					
					
					if (j === 1) {
						for (var k = 0; k < listCapacitacionTipo.length; k++) {
							if (listCapacitacionTipo[k].nombre == element.trim()) {
								recurs.capacitacionTipoId = listCapacitacionTipo[k].selCapId;
							}
						}

						if (!recurs.capacitacionTipoId) {
							validado = "Tipo de Formación no reconocido.";
							break;
						}
					}
					
					if (j === 2) {
						for (var k = 0; k < listClasificacion.length; k++) {
							if (listClasificacion[k].nombre == element.trim()) {
								recurs.clasificacionId = listClasificacion[k].clasificacionId;
							}
						}

						if (!recurs.clasificacionId) {
							validado = "Clasificación de Formación no reconocida.";
							break;
						}
					}
					if (j === 3) {
						recurs.responsable = element.trim();
					}
					if (j === 4) {
						recurs.tema = element.trim();
					}
					if (j === 5) {
						recurs.horasProgramadas = element.trim();
						if(isNaN(parseFloat(recurs.horasProgramadas ))){
							validado="Horas ingresadas no reconocidas"
						}
					}
					recurs.idCompany = sessionStorage.getItem("gestopcompanyid");
				
				}

				listRecurso.push(recurs);
				if (listFormacionRepe.length < listRecurso.length) {
					validado = "Existen fomaciones repetidos." + listFullNombres;
					break;
				}
			}
		}
	} else {
		validado = "Ha excedido los 10000 caracteres permitidos.";
	}

	if (validado.length < 1) {
		var r = confirm("¿Está seguro que desea subir estos "
				+ listExcel.length + " elemento(s)?");
		if (r == true) {
			var dataParam = {
				listRecurso : listRecurso
			};
			console.log(listRecurso);
			callAjaxPost(URL + '/capacitacion/masivo/save', dataParam,
					function (data) {
				switch (data.CODE_RESPONSE) {
				case "05":
					
						alert("Se guardaron exitosamente.");
						cargarPrimerEstado();
						$('#mdImpDatos').modal('hide');
					
					break;
				default:
					alert("Ocurri&oacute; un error al guardar el trabajador!");
				}
			});
		}
	} else {
		alert(validado);
	}
}
