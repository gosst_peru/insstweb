var entregaEppId;
var fechaEntrega;
var banderaEdicion2;
var listTrabajadores;
var listTrabajadoresSeleccionados;
var listEquipos;
var cantidadEPP;
var listaFullProgEpp;
var motivoId;
var progEppEstado;
var progEppEstadoNombre;
var numPaginaActual;
var numPaginaTotal;


$(function(){
	$("#btnClipTablaProgramacion").on("click",function(){
		var listFullTablaProgramacion="";
		listFullTablaProgramacion="Motivo" +"\t"
		+"Fecha Entrega Planificada" +"\t"
		+"Fecha Entrega Real" +"\t"
		+"# de EPP´s" +"\t"
		+"# de trabajadores" +"\t"
		+"Evidencia" +"\t"
		+"Estado" +" \n";
		for (var index = 0; index < listaFullProgEpp.length; index++){
			
		
			listFullTablaProgramacion=listFullTablaProgramacion
			+listaFullProgEpp[index].motivoNombre+"\t"
			+listaFullProgEpp[index].fechaPlanificadaTexto+"\t"
			+listaFullProgEpp[index].fechaEntregaTexto+"\t"
			+listaFullProgEpp[index].numeroEquipos +"\t"
			+listaFullProgEpp[index].numeroTrabajadores+"\t"
			+(listaFullProgEpp[index].evidenciaNombre=="----"?"NO":"SI") +"\t"
			+listaFullProgEpp[index].estadoImplementacionNombre
			+"\n";
		

		}
		
		copiarAlPortapapeles(listFullTablaProgramacion,"btnClipTablaProgramacion");
		
		 
		alert("Se han guardado al clipboard la tabla de este módulo" );

	});
	
	$("#cambiarBuscador").attr("onclick","toggleBuscadorJstree(2)");
	$("#cambiarBuscadorAsist").attr("onclick","toggleBuscadorAsistJstree(2)");
	
	$("#listTrabBuscar").hide();
	$("#listTrabAsistBuscar").hide();
	$('#modalRecomendacion').on('hidden.bs.modal', function(e) {
		$("#mdProgFecha").modal({
			  keyboard: true,
			  show:true,"backdrop":"static"
			});
	});
	$('#buscadorTrabajadorInput').keyup(function(e){
	    if(e.keyCode == 13)
	    {
	        $(this).trigger("enterKey");
	    }
	});
	$('#buscadorTrabajadorInput').bind("enterKey",function(e){
		cargarModalProgCap();
		});
	$("#buscadorTrabajadorInput").focus();
	
	$("#resetBuscador").attr("onclick","resetBuscador()")
	
	
	 $('.izquierda_flecha').on('click',function(){
		 numPaginaActual=numPaginaActual-1
    cambiarPaginaTabla();
    });
	 $('.derecha_flecha').on('click',function(){
		 numPaginaActual=numPaginaActual+1;
		 cambiarPaginaTabla();
    });
	 $('.izquierda_flecha').hover(function(){
         $(this).css('opacity','0.5');
     },function(){
         $(this).css('opacity','1');
     });

     $('.derecha_flecha').hover(function(){
         $(this).css('opacity','0.5');
     },function(){
         $(this).css('opacity','1');
     });
	
	
 	
})
function resetBuscador(){
	$("#buscadorTrabajadorInput").val("");
	cargarModalProgCap();
}
function programarFecha() {
	cargarModalProgCap();
	$('#mdProgFecha').modal({
		  keyboard: true,
		  show:true,"backdrop":"static"
		}); 

}





function cambiarPaginaTabla(){
	

	
	//Definiendo variables
	
	var list = listaFullProgEpp;
	var totalSize=list.length;
	
	var partialSize=10;
	numPaginaTotal=Math.ceil(totalSize/partialSize);
	var inicioRegistro=(numPaginaActual*partialSize)-partialSize;
	var finRegistro=(numPaginaActual*partialSize)-1;
	
	//aplicando logica de compaginacion
	
	$('.izquierda_flecha').show();
	 $('.derecha_flecha').show();
	 if(numPaginaTotal==0){
			numPaginaTotal=1;
		}
		$("#labelFlechas").html(numPaginaActual+" de "+numPaginaTotal);
	 if(numPaginaActual==1){
			$('.izquierda_flecha').hide();
	 }
	
	 if(numPaginaActual==numPaginaTotal){
		 $('.derecha_flecha').hide();
	 }
	 
	if(finRegistro>totalSize){
		finRegistro=totalSize-1;
	}
	
$("#tblProg tbody tr").remove();
	for (var index = inicioRegistro; index <= finRegistro; index++) {

		 cantidadEPPP(list[index].entregaEppId);
			$("#tblProg tbody").append(
					"<tr id='tr" + list[index].entregaEppId
							+ "' onclick='javascript:editarEntregaEpp("
							+index+")' >"
							+ "<td id='tdmotivo"
							+ list[index].entregaEppId + "'>"
							+ list[index].motivoNombre + "</td>"
						
							+ "<td id='tdfechaPlan"
							+ list[index].entregaEppId + "'>"
							+ list[index].fechaPlanificadaTexto + "</td>"
							+ "<td id='tdhorapla"
							+ list[index].entregaEppId + "'>"
							+ list[index].horaPlanificadaTexto + "</td>"
							+ "<td id='tdfechEntrega"
							+ list[index].entregaEppId + "'>"
							+ list[index].fechaEntregaTexto + "</td>"
							
							+ "<td id='tdlistEpp"
							+ list[index].entregaEppId + "'>"
							+ list[index].numeroEquipos+"</td>"
							+ "<td id='tdtrabEntrega"
							+ list[index].entregaEppId + "'>"
							+ list[index].numeroTrabajadores+"</td>" 
							+ "<td id='tdevi" + list[index].entregaEppId
							+ "'>"+list[index].evidenciaNombre+"</td>"
							+ "<td id='estadoEpp" + list[index].entregaEppId
							+ "'>"+list[index].estadoImplementacionNombre+"</td>"
							+"</tr>");
		
	}
}
function cargarModalProgCap() {
	
	$('#modalCorreos').on('hidden.bs.modal', function(e) {
		
		$("#mdProgFecha").modal({
			  keyboard: true,
			  show:true,"backdrop":"static"
			});
		
	});
	var palabraClave=$("#buscadorTrabajadorInput").val();
	if(palabraClave.length==0){
		palabraClave=null;
	}else{
		palabraClave=("%"+palabraClave+"%").toUpperCase();
	}
	$("#btnAgregarProg").attr("onclick", "javascript:nuevoProgCap();");
	$("#btnCancelarProg").attr("onclick", "javascript:cancelarProgCap();");
	$("#btnGuardarProg").attr("onclick", "javascript:guardarProgCap();");
	$("#btnEliminarProg").attr("onclick", "javascript:eliminarProgCap();");
	$("#btnGenReporte").attr("onclick", "javascript:generarReporteEntrega();");

	$("#btnRecomendacion").attr("onclick", "javascript:verRecomendacion();");

	$("#btnRecomendacion").hide();
	$("#btnCancelarProg").hide();
	$("#btnAgregarProg").show();
	$("#btnEliminarProg").hide();
	$("#btnGenReporte").hide();
	$("#btnGuardarProg").hide();

	$("#modalProg").show();
	$("#modalTree").hide();
	$("#modalListEpp").hide();
	$("#btnVerCorreos").attr("onclick", "javascript:verCorreos();");
	$("#btnVerCorreos").hide();
	entregaEppId = 0;
	numPaginaActual=1;
	listTrabajadoresSeleccionados = null;
	listEquipos=null;
	motivoId=null;
	fechaEntrega = null;
	banderaEdicion2 = false;
	listaFullProgEpp=[];
	var dataParam = {
			idCompany : sessionStorage.getItem("gestopcompanyid"),
			palabraClave:palabraClave
	};

	callAjaxPost(URL + '/equiposeguridad/entrega', dataParam, function(data) {
		switch (data.CODE_RESPONSE) {
		case "05":
			var list = data.list;
			listaFullProgEpp=data.list;
			
			formatoCeldaSombreableTabla(true,"tblProg");
			if(getSession("linkCalendarioEntregaEppId")!=null){
		 		
				listaFullProgEpp.every(function(val,index3){
					console.log(val.entregaEppId);
					console.log(parseInt(getSession("linkCalendarioEntregaEppId")))
					if(val.entregaEppId==parseInt(getSession("linkCalendarioEntregaEppId"))){
						
						numPaginaActual=Math.ceil((index3+1)/(10));
						cambiarPaginaTabla();
						editarEntregaEpp(index3);
						sessionStorage.removeItem("linkCalendarioEntregaEppId");
						return false;
					}else{
						return true;
					}
				});
				
			}else{
				cambiarPaginaTabla();
			}
		
			break;
		default:
			alert("Ocurrió un error al traer las programaciones!");
		}
	});
}

function editarEntregaEpp(pindex) {

	if (!banderaEdicion2) {
		listTrabajadores = [];
		listTrabajadoresSeleccionados = [];
		entregaEppId = listaFullProgEpp[pindex].entregaEppId;
		fechaEntrega = listaFullProgEpp[pindex].fechaEntrega;
		listEquipos=[];
		var correTrabajadores=listaFullProgEpp[pindex].correoTrabajadores;
		var numTrabajadores=listaFullProgEpp[pindex].numeroTrabajadores;
		
		var estadoImpl=listaFullProgEpp[pindex].estadoImplementacion;
		var fechaPlan=listaFullProgEpp[pindex].fechaPlanificada;
		var inversionEntregaEpp=listaFullProgEpp[pindex].inversionEntregaEpp;
		var motivoId=listaFullProgEpp[pindex].motivoId;
		var horaPlanificada=listaFullProgEpp[pindex].horaPlanificada;
		var dataParam = {
					entregaEppId : entregaEppId,
			};

			callAjaxPost(URL + '/equiposeguridad/recomendacion', dataParam,
					function(data) {
						switch (data.CODE_RESPONSE) {
						case "05":
						var list=data.list;
						var inversionRecomendada=list.inversionRecomendada;
					
						$("#tblRecomendacion tbody tr").remove();
						$("#tblRecomendacion tbody").append(
								"<tr>" 
								+"<td>"+inversionRecomendada+"</td>"
								+"<td>"+numTrabajadores+"</td>"
								+"<td>"+parseFloat(inversionRecomendada)*parseInt(numTrabajadores)+"</td>"
								+"</tr>"
								);
							break;
						default:
							alert("Ocurrió un error al eliminar la programacion!");
						}
					});
		
			var selMotivo = crearSelectOneMenu("selMotivo", "",
					listMotivos, motivoId, "motivoId",
					"motivoNombre");
		
		$("#tdmotivo"+entregaEppId).html(selMotivo);
			$("#btnRecomendacion").hide();
		$("#modalBodyCorreos").html("<p style='color:#008b8b'>Número de Trabajadores: </p>"+numTrabajadores+"<br>" +
				"<p style='color:#008b8b'>Correos: </p>"+correTrabajadores);
		
		$("#btnVerCorreos").show();
		formatoCeldaSombreableTabla(false,"tblProg");
		
		$("#tdinversion"+entregaEppId).html(
				"<input type='number' id='inputInversion' name='inputInversion'" +
		"class='form-control' value='"+inversionEntregaEpp+"'><label for='inputInversion'>S/</label>");
		$("#tdtrabEntrega" + entregaEppId)
				.html(
						"<a id='linkTrabEntr' href='#' onclick='javascript:cargarTrabajadores();'>Trabajadores a entregar</a>");
		
		
		$("#tdfechEntrega" + entregaEppId).html(
				"<input type='date' id='inputFecEntrega' class='form-control' onchange='hallarEstadoImplementacion("+entregaEppId+")'>");
		$("#tdfechaPlan" + entregaEppId).html(
				"<input type='date' id='inputFecPlan' class='form-control' onchange='hallarEstadoImplementacion("+entregaEppId+")'>");
		$("#tdhorapla" + entregaEppId).html(
				"<input type='time' id='inputHora' class='form-control'>");
		var today = convertirFechaInput(fechaEntrega);
		var today2 = convertirFechaInput(fechaPlan);
		
	
		$("#inputHora").val(horaPlanificada);
		$("#inputFecEntrega").val(today);
		$("#inputFecPlan").val(today2);
		hallarEstadoImplementacion(entregaEppId);
		var nombreEvidencia=$("#tdevi" + entregaEppId)
		.text();
		$("#tdevi" + entregaEppId)
		.html(
				"<a href='"
						+ URL
						+ "/equiposeguridad/evidencia?progEquipoId="
						+ entregaEppId
						+ "' "
						+ "target='_blank'>" +
						nombreEvidencia+	"</a>"
						+ "<br/><a href='#' onclick='javascript:mostrarCargarImagen("
						+ entregaEppId + ")'  id='subirimagen"
						+ entregaEppId + "'>Subir</a>");
		banderaEdicion2 = true;
		$("#btnCancelarProg").show();
		$("#btnAgregarProg").hide();
		$("#btnEliminarProg").show();
		$("#btnGenReporte").show();
		$("#btnGuardarProg").show();

		cargarTrabajadores();
		cargarEPPs();
		$("#modalProg").show();
		$("#modalTree").hide();
		$("#modalListEpp").hide();
	}
}

function cargarTrabajadores() {
	$("#modalProg").hide();
	$("#modalTree").show();
	$("#modalListEpp").hide();
	var buttonGuardar="<button id='btnGuardarShortcut' type='button' class='btn btn-success'"
		+"title='Guardar' style='margin-left:2px'>"
	+"	<i class='fa fa-floppy-o fa-2x'></i>"
	+"</button>";
	$("#modalTree #btnGuardarShortcut").remove();
	$("#modalTree #btnCancelarTrabs").after(buttonGuardar);
	$("#modalTree #btnGuardarShortcut").on("click",function(){
		cancelarSeleccionarTrabs();
		guardarProgCap();
	});
	if (listTrabajadoresSeleccionados.length === 0) {
		var fechaPlanAux=$("#inputFecPlan").val();
		var fechaEntregaAux=$("#inputFecEntrega").val();

		var dataParam = {
			entregaEppId : entregaEppId,
			fechaPlanificadaTexto:fechaPlanAux,
			fechaEntregaTexto:fechaEntregaAux,
			idCompany : sessionStorage.getItem("gestopcompanyid")
		};

		callAjaxPost(
				URL + '/equiposeguridad/entrega/trabajadores',
				dataParam,
				function(data) {
					switch (data.CODE_RESPONSE) {
					case "05":
						$('#jstree').jstree("destroy");

						listTrabajadores = data.list;

						$('#jstree').jstree({
							"plugins" : [ "checkbox", "json_data","search" ],
							'checkbox' : {
								"keep_selected_style" : false,
								"tie_selection" : false
							},
							'core' : {
								'data' : listTrabajadores
							}
						});
						//Inicio Para Buscador en jstree
						  var to = false;
						  $('#buscarTrabJstree').keyup(function () {
						    if(to) { clearTimeout(to); }
						    to = setTimeout(function () {
						      var v = $('#buscarTrabJstree').val();
						  $("#jstree").jstree(true).search(v);
						    
						    }, 250);
						  });
						  $.jstree.defaults.search.show_only_matches=true;
						 
						  $('#listTrabBuscar').keyup(function () {
							    if(to) { clearTimeout(to); }
							    to = setTimeout(function () {
							
							    	busquedaMasivaJstree(listTrabajadores,"jstree","resultSearchTrab","listTrabBuscar")
							    }, 250);
							  });
						//Fin Para Buscador en jstree

						for (index = 0; index < listTrabajadores.length; index++) {
							if (listTrabajadores[index].state.checked
									&& listTrabajadores[index].id.substr(
											0, 3) == "tra") {
								listTrabajadoresSeleccionados
										.push(listTrabajadores[index].id
												.substr(
														3,
														listTrabajadores[index].id.length));
							}
						}

						break;
					default:
						alert("Ocurrió un error al cargar el arbol de trabajadores!");
					}
				});
	}

}

function nuevoProgCap() {
	if (!banderaEdicion2) {
		var selMotivo = crearSelectOneMenu("selMotivo", "",
				listMotivos, "", "motivoId",
				"motivoNombre");
		$("#tblProg tbody")
				.append(
						"<tr id='tr0'>"
						+ "<td>"+selMotivo+"</td>"
						+ "<td><input type='date' id='inputFecPlan' class='form-control' onchange='hallarEstadoImplementacion(0)'></td>"
						
						+ "<td><input type='time' value='12:00:00' id='inputHora' class='form-control' ></td>"
						
						
						+ "<td><input type='date' id='inputFecEntrega' class='form-control' onchange='hallarEstadoImplementacion(0)'></td>"
						
								+ "<td>...</td>"
								+ "<td>...</td>" 
								+ "<td>...</td>"
								+ "<td id='estadoEpp0'>...</td>"
								+ "</tr>");
		entregaEppId = 0;
		$("#btnCancelarProg").show();
		$("#btnAgregarProg").hide();
		$("#btnEliminarProg").hide();
		$("#btnGuardarProg").show();
		$("#btnGenReporte").hide();
		
		
		banderaEdicion2 = true;
	} else {
		alert("Guarde primero.");
	}
}

function cancelarProgCap() {
	cargarModalProgCap();
}

function eliminarProgCap() {
	var r = confirm("¿Está seguro de eliminar la programacion?");
	if (r == true) {
		var dataParam = {
			entregaEppId : entregaEppId,
		};

		callAjaxPost(URL + '/equiposeguridad/entrega/delete', dataParam,
				function(data) {
					switch (data.CODE_RESPONSE) {
					case "05":
						cargarModalProgCap();
						break;
					default:
						alert("Ocurrió un error al eliminar la programacion!");
					}
				});
	}
}

function guardarProgCap() {
	$("#modalProg button:not(#btnClipTablaProgramacion)").hide();
	var campoVacio = true;
var motivoEntregaId=$("#selMotivo").val();
	var horaPlanificada=$("#inputHora").val();
	var mes1 = $("#inputFecEntrega").val().substring(5, 7) - 1;
	var fechatemp1 = new Date($("#inputFecEntrega").val().substring(0, 4), mes1, $(
			"#inputFecEntrega").val().substring(8, 10));

	var inputFecPla = fechatemp1;
	
	var mes2 = $("#inputFecPlan").val().substring(5, 7) - 1;
	var fechatemp2 = new Date($("#inputFecPlan").val().substring(0, 4), mes2, $(
			"#inputFecPlan").val().substring(8, 10));

	var inputFecPlanificada = fechatemp2;
if(!$("#inputFecPlan").val()){
		
	inputFecPlanificada=null;
	}
if(!$("#inputFecEntrega").val()){
	
	inputFecPla=null;
}
	var listEppSel=[];

	$("#list-epps .check").each(function() {
		if ($(this).is(':checked')) {
			var id = $(this).attr('id');
			listEppSel.push(id.substring(8, id.length));
		}
		console.log($(this).attr('id'));
	});

	if (campoVacio) {

		var dataParam = {
			fechaEntrega : inputFecPla,horaPlanificada:horaPlanificada,
			fechaPlanificada:inputFecPlanificada,
			entregaEppId : entregaEppId,
			trabajadores: listTrabajadoresSeleccionados,
			equipos: listEppSel,
			estadoImplementacion:progEppEstado,
			motivoId:motivoEntregaId,
			idCompany : sessionStorage.getItem("gestopcompanyid")
		};

		callAjaxPost(URL + '/equiposeguridad/entrega/save', dataParam,
				function(data) {
					switch (data.CODE_RESPONSE) {
					case "05":
						cargarModalProgCap();
						break;
					default:
						alert("Ocurrió un error al guardar la programacion!");
					}
				},loadingCelda,"tblProg #tr"+entregaEppId);
	} else {
		alert("Debe ingresar todos los campos.");
	}
}

function cantidadEPPP(pentregaEppId){
	
		var dataParam = {
			entregaEppId : pentregaEppId,
			idCompany : sessionStorage.getItem("gestopcompanyid")
		};
		
		callAjaxPost(
				URL + '/equiposeguridad/entrega/equiposeguridad',
				dataParam,
				function(data) {
					
					switch (data.CODE_RESPONSE) {
					case "05":
						listEquipos=data.list;
						cantidadEPP= listEquipos.length;
						for (index = 0; index < listEquipos.length; index++) {
							if (listEquipos[index].seleccionado === 1) {
								cantidadEPP= cantidadEPP-1;
														
							}else{
								cantidadEPP= cantidadEPP-1;	
							}
						}	
						break;
					default:
						alert("Ocurrió un error al cargar los epps!");
					}
				});
	
}

function cargarEPPs() {
	$("#modalProg").hide();
	$("#modalTree").hide();
	$("#modalListEpp").show();

	if (listEquipos.length === 0) {
		$("#list-epps").html("");
		var dataParam = {
			entregaEppId : entregaEppId,
			idCompany : sessionStorage.getItem("gestopcompanyid")
		};

		callAjaxPost(
				URL + '/equiposeguridad/entrega/equiposeguridad',
				dataParam,
				function(data) {
					switch (data.CODE_RESPONSE) {
					case "05":
						listEquipos=data.list;
						for (index = 0; index < listEquipos.length; index++) {
							var tallas=listEquipos[index].tallas;
							var textoTallas="";
							if (listEquipos[index].seleccionado === 1) {
								tallas.forEach(function(val1,index1){
									textoTallas+=val1.nombre +"-"+val1.numTrabsTalla+"<br>"
								});
								$("#list-epps").append(
										"<li class='list-group-item'>"
												+ "<input type='checkbox' class='check' "
												+ "id='list-epp"
												+ listEquipos[index].equipoSeguridadId + "'" 
												+ "checked>"
												+"<div style='width:50%;font-weight:bold'>"+ listEquipos[index].equipoSeguridadNombre + "</div>" +
												"<div style='width:50%'>"+textoTallas+"</div></li>");
								
							}else{
								$("#list-epps").append(
										"<li class='list-group-item'>"
												+ "<input type='checkbox' class='check' "
												+ "id='list-epp"
												+ listEquipos[index].equipoSeguridadId + "'>"
												+ listEquipos[index].equipoSeguridadNombre + "" +
														"</li>");
							}
						}	
						$("#tdlistEpp" + entregaEppId)
						.html("<a id='linkEpps' href='#' onclick='javascript:cargarEPPs();'>Lista de EPPs</a>");

						break;
					default:
						alert("Ocurrió un error al cargar los epps!");
					}
				});
	}

}

function cancelarListEpp(){
	$("#modalProg").show();
	$("#modalTree").hide();
	$("#modalListEpp").hide();	
}

function cancelarSeleccionarTrabs(){
	$("#modalProg").show();
	$("#modalTree").hide();
	$("#modalListEpp").hide();
	
	var seleccionados = $('#jstree').jstree(true).get_checked();
	listTrabajadoresSeleccionados = [];
	for (index = 0; index < seleccionados.length; index++) {
		var trab = seleccionados[index];
		if (seleccionados[index].substr(0, 3) == "tra") {
			listTrabajadoresSeleccionados.push(seleccionados[index]
					.substr(3, seleccionados[index].length));
		}
	}
}

function generarReporteEntrega(){
	if($("#inputFecEntrega").val()){
	window.open(URL
			+ "/equiposeguridad/informe?entregaEppId="
			+ entregaEppId , '_blank');	
	}else{
		alert("Asegúrese de haber ingresado una fecha de entrega, trabajadores y EPP´s ")
	}
}
function mostrarCargarImagen(progEquipoId) {
	$('#mdUpload').modal('show');
	$('#btnUploadArchivo').attr("onclick",
			"javascript:uploadArchivo(" + progEquipoId + ");");
}

function uploadArchivo(progEquipoId) {
	var inputFileImage = document.getElementById("fileEvi");
	var file = inputFileImage.files[0];
	var data = new FormData();

	if (file.size > bitsEvidenciaEPP) {
		alert("Lo sentimos, solo se pueden subir archivos menores a "+bitsEvidenciaEPP/1000000+" MB");
	} else {
		data.append("fileEvi", file);
		data.append("progEquipoId", progEquipoId);
		
		var url = URL + '/equiposeguridad/evidencia/save';
		$.blockUI({
			message : 'cargando...'
		});
		$
				.ajax({
					url : url,
					type : 'POST',
					contentType : false,
					data : data,
					xhrFields: {
			            withCredentials: true
			        },
					processData : false,
					cache : false,
					success : function(data, textStatus, jqXHR) {
						$.unblockUI();
						switch (data.CODE_RESPONSE) {
						case "06":
							sessionStorage.clear();
							document.location.replace(data.PATH);
							break;
						default:
							console.log('Se subio el archivo correctamente.');
							$('#mdUpload').modal('hide');
						}

					},
					error : function(jqXHR, textStatus, errorThrown) {
						$.unblockUI();
						alert("Ups, tenemos problemas tecnicos. Intente más tarde. Error: "
								+ errorThrown);
						console.log('xhRequest: ' + jqXHR + "\n");
						console.log('ErrorText: ' + textStatus + "\n");
						console.log('thrownError: ' + errorThrown + "\n");
					}
				});
	}
}

function hallarEstadoImplementacion(progId){
	var fechaPlanificada=$("#inputFecPlan").val();
	var fechaReal=$("#inputFecEntrega").val();
	var fechaHoy=obtenerFechaActual();
	if(fechaReal!=''){
		progEppEstado=2;
		progEppEstadoNombre="Completado";
	}else{
		
		if(fechaPlanificada!=""){
			var dif=restaFechas(fechaHoy,fechaPlanificada)
			if(dif<0){
				progEppEstado=3;
				progEppEstadoNombre="Retrasado";
			}else{
				progEppEstado=1;
				progEppEstadoNombre="Por implementar";
			}
			
		}else{
			
			progEppEstado=1;
			progEppEstadoNombre="Por implementar";
			
		}
	}
	
	$("#estadoEpp"+progId).html(progEppEstadoNombre);
	
}


function verCorreos(){
	
	$("#modalCorreos").modal("show");
	$("#mdProgFecha").modal("hide");
}
 
function verRecomendacion(){
	
	$("#modalRecomendacion").modal("show");
	$("#mdProgFecha").modal("hide");
}