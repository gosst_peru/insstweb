var banderaedicion15;



var eventoAccidId;

var eventoTipoId;
var eventoNombre;

var listeventos;

var isFallo;

var listFullEventosAccidente;

var listCausasRelacionadas=[];
function cargarEventosAcc(){
	//* Botones y logica de A/C inseguros del accidente*//
	
	$("#btnCancelarEvento").hide();
	$("#btnAgregarEvento").show();
	$("#btnEliminarEvento").hide();
	$("#btnGuardarEvento").hide();
	
	$("#btnAgregarEvento").attr("onclick", "javascript:nuevoEventoAcc();");
	$("#btnCancelarEvento").attr("onclick", "javascript:cancelarEventoAcc();");
	$("#btnGuardarEvento").attr("onclick", "javascript:guardarEventoAcc();");
	$("#btnEliminarEvento").attr("onclick", "javascript:eliminarEventoAcc();");
	
	//* Botones y logica de Causas básicas del accidente*//
	
	$("#btnCancelarCausaBasica").hide();
	$("#btnAgregarCausaBasica").show();
	$("#btnEliminarCausaBasica").hide();
	$("#btnGuardarCausaBasica").hide();
	
	$("#btnAgregarCausaBasica").attr("onclick", "javascript:nuevoCausaBasica();");
	$("#btnCancelarCausaBasica").attr("onclick", "javascript:cancelarEventoAcc();");
	$("#btnGuardarCausaBasica").attr("onclick", "javascript:guardarEventoAcc();");
	$("#btnEliminarCausaBasica").attr("onclick", "javascript:eliminarEventoAcc();");
	
	//* Botones y logica de Fallos en sistema*//
	isFallo=false;
	$("#btnCancelarFallo").hide();
	$("#btnAgregarFallo").show();
	$("#btnEliminarFallo").hide();
	$("#btnGuardarFallo").hide();
	
	$("#btnAgregarFallo").attr("onclick", "javascript:nuevoFallo();");
	$("#btnCancelarFallo").attr("onclick", "javascript:cancelarEventoAcc();");
	$("#btnGuardarFallo").attr("onclick", "javascript:guardarEventoAcc();");
	$("#btnEliminarFallo").attr("onclick", "javascript:eliminarEventoAcc();");
	
	
	banderaedicion15=false;

	 eventoAccidId=0;
	 eventoNombre="";
	 eventoTipoId=0;

	


	var dataParam = {
			accidenteId : accidenteId
	};
	 $("#tblEventAcc tbody tr").remove();
	 $("#tblFalloSST tbody tr").remove();
	 $("#tblCausaBasica tbody tr").remove();

	
	callAjaxPost(URL + '/accidente/eventacc', dataParam, function(data) {
		switch (data.CODE_RESPONSE) {
		case "05":
			var list = data.list;
			listFullEventosAccidente=list;
			
			listCausasRelacionadas=[];
			for (index = 0; index < list.length; index++) {
				var tipoEvento=list[index].eventoTipoNombre;
				var nombreEvento=list[index].eventoNombre;
				
					switch(list[index].eventoTipoId){
					case 1: 
						 $("#tblEventAcc tbody").append(
								 " <tr onclick='editarEventoAcc(" 
									+index
								 + " )'>" 
								 +	 "<th id='sel"+list[index].eventoInseguroAccidenteId+"'>"+ tipoEvento+"</th> " 
								 +" <th id='thnombre"+list[index].eventoInseguroAccidenteId+"'>"+ nombreEvento+"</th> </tr>  "
								 
						 );
						 listCausasRelacionadas.push({nombre:nombreEvento,id:0});
						break;
					case 2:
						 $("#tblEventAcc tbody").append(
								 " <tr onclick='editarEventoAcc(" 
									+index
									 + " )'>" 
									 +	 "<th id='sel"+list[index].eventoInseguroAccidenteId+"'>"+ tipoEvento+"</th> " 
									 +" <th id='thnombre"+list[index].eventoInseguroAccidenteId+"'>"+ nombreEvento+"</th> </tr>  "
								 
						 );
						 listCausasRelacionadas.push({nombre:nombreEvento,id:0});
						break;
					case 3: 
						 $("#tblCausaBasica tbody").append(
								 " <tr onclick='editarEventoAcc(" 
									+index
									 + ")'>" 
									 +	 "<th id='sel"+list[index].eventoInseguroAccidenteId+"'>"+ tipoEvento+"</th> " 
									 +	 "<th id='thsubtipo"+list[index].eventoInseguroAccidenteId+"'>"+ list[index].subTipoEventoNombre+"</th> " 
									 +" <th id='thnombre"+list[index].eventoInseguroAccidenteId+"'>"+ nombreEvento+"</th> </tr>  "
								 
						 );
						break;
					case 4: 
						 $("#tblCausaBasica tbody").append(
								 " <tr onclick='editarEventoAcc(" 
									+index
									 + ")'>" 
								 +	 "<th id='sel"+list[index].eventoInseguroAccidenteId+"'>"+ tipoEvento+"</th> " 
								 +	 "<th id='thsubtipo"+list[index].eventoInseguroAccidenteId+"'>"+ list[index].subTipoEventoNombre+"</th> " 
								 +" <th id='thnombre"+list[index].eventoInseguroAccidenteId+"'>"+ nombreEvento+"</th> </tr>  "
								 
						 );
						break;
					case 5:
						 $("#tblFalloSST tbody").append(
								 " <tr onclick='editarEventoAcc(" 
									+index
									 + ")'>" 
									 +" <th id='thnombre"+list[index].eventoInseguroAccidenteId+"'>"+ nombreEvento+"</th> </tr>  "
								 
						 );
						break;
					
					
					}
				
			}
			
			
			break;
		default:
			alert("Ocurrió un error al traer las programaciones!");
		}
	});
}


function editarEventoAcc(pindex) {
	
	if (!banderaedicion15) {
		 eventoAccidId=listFullEventosAccidente[pindex].eventoInseguroAccidenteId;
		 eventoNombre=listFullEventosAccidente[pindex].eventoNombre;
		 eventoTipoId=listFullEventosAccidente[pindex].eventoTipoId;
var subTipoEventoId=listFullEventosAccidente[pindex].subTipoEventoId;
		switch(eventoTipoId){
		case 1:
			
			$("#sel" + eventoAccidId)
			.html(
					"<select id='listTipoEvento' onchange='verCausasAcordeTipoEvento()' class='form-control'>"
					+"<option value='1'>Acto Subestándar</option>"
					+"<option value='2'>Condicion Subestándar</option>"
					+"</select>");
	
				$("#thnombre" + eventoAccidId)
				.html(
						"<input type='text'  id='invEventoInseguro' class='form-control'>");
				$('#listTipoEvento > option[value="'+eventoTipoId+'"]').attr('selected', 'selected');
				nombrarInput("invEventoInseguro",eventoNombre);
				verCausasAcordeTipoEvento();
				banderaedicion15 = true;
				$("#btnCancelarEvento").show();
				$("#btnAgregarEvento").hide();
				$("#btnEliminarEvento").show();
				
				$("#btnGuardarEvento").show();
			break;
		case 2:
			$("#sel" + eventoAccidId)
			.html(
					"<select id='listTipoEvento' class='form-control'>"
					+"<option value='1'>Acto Subestándar</option>"
					+"<option value='2'>Condicion Subestándar</option>"
					+"</select>");
	
				$("#thnombre" + eventoAccidId)
				.html(
						"<input type='text'  id='invEventoInseguro' class='form-control'>");
				$('#listTipoEvento > option[value="'+eventoTipoId+'"]').attr('selected', 'selected');
				nombrarInput("invEventoInseguro",eventoNombre);
				
			
				banderaedicion15 = true;
				$("#btnCancelarEvento").show();
				$("#btnAgregarEvento").hide();
				$("#btnEliminarEvento").show();
				
				$("#btnGuardarEvento").show();
			
			break;
		case 3:
			$("#sel" + eventoAccidId)
			.html(
					"<select id='listTipoEvento' onchange='verSubTipoAcordeTipo()' class='form-control'>"
					+"<option value='3'>Factores de trabajo</option>"
					+"<option value='4'>Factores personales</option>"
					+"</select>");
	
				$("#thnombre" + eventoAccidId)
				.html(
						"<input type='text'  id='invEventoInseguro' class='form-control'>");
				var selSubTipoEvento = crearSelectOneMenuY("selSubTipoEvento", "verSubSubTipoAcordeSubTipo()", subTiposEventos,
						subTipoEventoId,"subTipoEventoId","eventoAccidenteTipoId", "subTipoEventoNombre");
				$("#thsubtipo" + eventoAccidId)
				.html(
						selSubTipoEvento);
				
				
				$('#listTipoEvento > option[value="'+eventoTipoId+'"]').attr('selected', 'selected');
				nombrarInput("invEventoInseguro",eventoNombre);
				
				verSubTipoAcordeTipo();
				verSubSubTipoAcordeSubTipo(1);
				banderaedicion15 = true;
				$("#btnCancelarCausaBasica").show();
				$("#btnAgregarCausaBasica").hide();
				$("#btnEliminarCausaBasica").show();
				
				$("#btnGuardarCausaBasica").show();
			
			break;
		case 4:
			$("#sel" + eventoAccidId)
			.html(
					"<select id='listTipoEvento' onchange='verSubTipoAcordeTipo()' class='form-control'>"
					+"<option value='3'>Factores de trabajo</option>"
					+"<option value='4'>Factores personales</option>"
					+"</select>");
			var selSubTipoEvento = crearSelectOneMenuY("selSubTipoEvento", "verSubSubTipoAcordeSubTipo()", subTiposEventos,
					subTipoEventoId, "subTipoEventoId","eventoAccidenteTipoId", "subTipoEventoNombre");
			$("#thsubtipo" + eventoAccidId)
			.html(
					selSubTipoEvento);
				$("#thnombre" + eventoAccidId)
				.html(
						"<input type='text'  id='invEventoInseguro' class='form-control'>");
				
				$('#listTipoEvento > option[value="'+eventoTipoId+'"]').attr('selected', 'selected');
				nombrarInput("invEventoInseguro",eventoNombre);
				
				verSubTipoAcordeTipo();
				verSubSubTipoAcordeSubTipo(1);
				banderaedicion15 = true;
				$("#btnCancelarCausaBasica").show();
				$("#btnAgregarCausaBasica").hide();
				$("#btnEliminarCausaBasica").show();
				
				$("#btnGuardarCausaBasica").show();
			break;
		case 5:
		
			isFallo=true;
				$("#thnombre" + eventoAccidId)
				.html(
						"<input type='text' class='form-control' id='invEventoInseguro'>");
				
				nombrarInput("invEventoInseguro",eventoNombre);
				
				
			
				banderaedicion15 = true;
				$("#btnCancelarFallo").show();
				$("#btnAgregarFallo").hide();
				$("#btnEliminarFallo").show();
				
				$("#btnGuardarFallo").show();
			
			break;
		
		} 
		 
		 
		 
		
		
		
	}
}

function cancelarEventoAcc(){
	cargarEventosAcc();
}

function guardarEventoAcc() {

	var campoVacio = true;
	
	eventoNombre=$("#invEventoInseguro").val();
	if(isFallo){
		eventoTipoId=5;
		}else{
		eventoTipoId=$("#listTipoEvento").val();
	}
		
var subTipo=$("#selSubTipoEvento").val();
	obtenerIdEvento();
	if (campoVacio) {

		var dataParam = {
			accidenteId: accidenteId,
			eventoInseguroAccidenteId : eventoAccidId,
			eventoTipoId: eventoTipoId,
			eventoNombre: eventoNombre,
			subTipoEventoId:subTipo
			
			
		};

		callAjaxPost(URL + '/accidente/eventacc/save', dataParam,
				function(data) {
					switch (data.CODE_RESPONSE) {
					case "05":
						cargarEventosAcc();
						break;
					default:
						alert("Ocurrió un error al guardar el evento!");
					}
				});
	} else {
		alert("Debe ingresar todos los campos.");
	}
}

function eliminarEventoAcc(){
	var r = confirm("¿Está seguro de eliminar?");
	if (r == true) {
		var dataParam = {
				eventoInseguroAccidenteId : eventoAccidId
		};

		callAjaxPost(URL + '/accidente/eventacc/delete', dataParam,
				function(data) {
					switch (data.CODE_RESPONSE) {
					case "05":
						cargarEventosAcc();
						break;
					default:
						alert("Ocurrió un error al eliminar la programacion!");
					}
				});
	}
}

function verCausasAcordeTipoEvento(){
	var listaCausas=[];
	var subTipoAcc=$("#selAccSub").val().split('-');
	var tipoAux=$("#listTipoEvento").val();
var aux1=subTipoAcc[0];
	causasSugeridas.forEach(function(val,index){
		if(val.eventoTipoId==parseInt(tipoAux) && val.subTipoAccId==parseInt(aux1))
		listaCausas.push(val.casuaNombre);
		
	});
	ponerListaSugerida("invEventoInseguro",listaCausas,true);
}
function nuevoEventoAcc(){
	 banderaedicion15=false;
	if (!banderaedicion15) {
		$("#tblEventAcc tbody")
				.append(
						"<tr id='0'>"
						+"<th><select id='listTipoEvento' onchange='verCausasAcordeTipoEvento()' class='form-control'>"
						+"<option value='1'>Acto Subestándar</option>"
						+"<option value='2'>Condicion Subestándar</option>"
						+"</select></th>"
						+"<th><input type='text'  id='invEventoInseguro' class='form-control'></th>"
						+ "</tr>");
		eventoAccidId = 0;
		verCausasAcordeTipoEvento();
		$("#btnCancelarEvento").show();
		$("#btnAgregarEvento").hide();
		$("#btnEliminarEvento").hide();
		$("#btnGuardarEvento").show();
		
		banderaedicion15 = true;
	} else {
		alert("Guarde primero.");
	};

	
}
function verSubSubTipoAcordeSubTipo(idFinal){
	
	var listaSubTipos=[];
	var subTipoAcc=$("#selSubTipoEvento").val();
	subSubTiposSugeridos.forEach(function(val,index){
		if(val.subTipoEventoId==parseInt(subTipoAcc) )
			listaSubTipos.push(val.subSubTipoEventoNombre);
		
	});
	if(idFinal==null){
		$("#invEventoInseguro").val("");
	}
	
	ponerListaSugerida("invEventoInseguro",listaSubTipos,true);
}
function verSubTipoAcordeTipo(){
	var tipoEvento=$("#listTipoEvento").val();
	var subTipos=listarStringsDesdeArrayY(subTiposEventos,"subTipoEventoNombre","eventoAccidenteTipoId",parseInt(tipoEvento));
	var selectSubTipo="";
	listCausasRelacionadas.forEach(function(val,index){
		causasSugeridas.forEach(function(val1,index1){
			if(val.nombre==val1.casuaNombre){
				val.id=val1.causaId;
				return false;
			}
		})
	});
	var listSubTiposId=[];
	subTiposEventos.forEach(function(val,index){
		
		listCausasRelacionadas.forEach(function(val1,index1){
			if(val.eventoAccidenteTipoId==parseInt(tipoEvento) 
					&& val.causasRelacionadas.indexOf(val1.id)!=-1
					&& listSubTiposId.indexOf(val.subTipoEventoId)==-1){
				selectSubTipo=selectSubTipo+"<option value='"+val.subTipoEventoId+"'>"+val.subTipoEventoNombre+"</option>";
				listSubTiposId.push(val.subTipoEventoId);
			}
			
		});
	
	});
	$("#selSubTipoEvento").html(selectSubTipo);

}
function nuevoCausaBasica(){
	 banderaedicion15=false;
		var selSubTipoEvento = crearSelectOneMenuY("selSubTipoEvento", "verSubSubTipoAcordeSubTipo()", subTiposEventos,
				"-1", "subTipoEventoId","eventoAccidenteTipoId", "subTipoEventoNombre");
	if (!banderaedicion15) {
		$("#tblCausaBasica tbody")
				.append(
						"<tr id='0'>"
						+"<th><select id='listTipoEvento' onchange='verSubTipoAcordeTipo()' class='form-control'>"
						+"<option value='3'>Factores de trabajo</option>"
						+"<option value='4'>Factores personales</option>"
						+"</select></th>"
						+"<th>" +selSubTipoEvento+
								"</th>"
						+"<th><input type='text' class='form-control' id='invEventoInseguro'></th>"
						+ "</tr>");
		eventoAccidId = 0;
		$("#btnCancelarCausaBasica").show();
		$("#btnAgregarCausaBasica").hide();
		$("#btnEliminarCausaBasica").hide();
		$("#btnGuardarCausaBasica").show();
		verSubTipoAcordeTipo();
		verSubSubTipoAcordeSubTipo();
		banderaedicion15 = true;
	} else {
		alert("Guarde primero.");
	};

	
}


function nuevoFallo(){
	 banderaedicion15=false;
	 isFallo=true;
	if (!banderaedicion15) {
		$("#tblFalloSST tbody")
				.append(
						"<tr id='0'>"
						+"<th><input type='text' class='form-control' id='invEventoInseguro'></th>"
						+ "</tr>");
		eventoAccidId = 0;
		$("#btnCancelarFallo").show();
		$("#btnAgregarFallo").hide();
		$("#btnEliminarFallo").hide();
		$("#btnGuardarFallo").show();
		
		banderaedicion15 = true;
	} else {
		alert("Guarde primero.");
	};

	
}





function modificarAcordeTipoEvento(){
var listaevento=listarStringsDesdeArrayY(listeventos, "eventoNombre","eventoTipoId",$("#listTipoEvento").val());
	
$( "#invEventoInseguro" ).val("");
	
	
	 $( "#invEventoInseguro" ).autocomplete({
	        source: listaevento,
	        minLength : 0
	      }).focus(function() {
				$(this).autocomplete('search', '');
			});
}





function obtenerIdEvento(){
	
var nombreEvento=$("#invEventoInseguro").val();
	
	
		for (index = 0; index < listeventos.length; index++) {
			
			if(listeventos[index]["eventoNombre"]==nombreEvento){
			
				eventoId=listeventos[index]["eventoId"];
			break;
			}
		}
	
	
}
