var especificacionId;
var respuesta1;
var respuesta2;
var respuesta3;
var respuesta4;
var respuesta5;
var respuesta6;
var respuesta7;
var respuesta8;



function verCategorias() {
	if($("#selTipoAcc").val()=='3'){
	$("#listaCategoria").hide();
	
	$("#mdCategorias").modal('show');
	}else{
	$("#listaCategoria").show();
	
	$("#mdCategorias").modal('show');
	}
	}

$('#mdCategorias').on('show.bs.modal', function(e) {
	
	cargarModalEsp();
});



$('#mdCategorias').on('hidden.bs.modal', function(e) {
	guardarEspi();
	
	verCategAcordeTipoAccidente();
	mostrarCat();
});

function cargarModalEsp(){

	 var dataParam = {
					accidenteId : accidenteId
			};
	 
	 callAjaxPost(URL + '/accidente/especificacion', dataParam, function(data) {
			switch (data.CODE_RESPONSE) {
			case "05":
				var list = data.list;
				
				
				
				
				
				for (index = 0; index < list.length; index++) {
					especificacionId=list[index].accidenteEspecificacionId;
				
					marcarChechBox(list[index].respuestaUno,"accRpta1");
					marcarChechBox(list[index].respuestaDos,"accRpta2");
					marcarChechBox(list[index].respuestaTres,"accRpta3");
					marcarChechBox(list[index].respuestaCuatro,"accRpta4");
					marcarChechBox(list[index].respuestaCinco,"accRpta5");
					marcarChechBox(list[index].respuestaSeis,"accRpta6");
					marcarChechBox(list[index].respuestaSiete,"accRpta7");
					marcarChechBox(list[index].respuestaOcho,"accRpta8");
					
				
				}
				verCategAcordeTipoAccidente();
				
				break;
			default:
				alert("Ocurrió un error al traer las programaciones!");
			}
		});
	 
	
}




function guardarEspi() {

	var campoVacio = true;
	var respuesta1= pasarBoolInt($("#accRpta1").prop("checked"));
	
	var respuesta2=  pasarBoolInt($("#accRpta2").prop("checked"));
	var respuesta3= pasarBoolInt($("#accRpta3").prop("checked"));
	var respuesta4= pasarBoolInt($("#accRpta4").prop("checked"));
	var respuesta5=  pasarBoolInt($("#accRpta5").prop("checked"));
	var respuesta6= pasarBoolInt($("#accRpta6").prop("checked"));
	var respuesta7= pasarBoolInt($("#accRpta7").prop("checked"));
	var respuesta8=  pasarBoolInt($("#accRpta8").prop("checked"));
	
	if(!especificacionId){
		especificacionId=0;
			
	}
	
	if (campoVacio) {

		var dataParam = {
			accidenteId : accidenteId,
			accidenteEspecificacionId : especificacionId,
			respuestaUno : respuesta1,
			respuestaDos : respuesta2,
			respuestaTres : respuesta3,
			respuestaCuatro : respuesta4,
			respuestaCinco : respuesta5,
			respuestaSeis : respuesta6,
			respuestaSiete : respuesta7,
			respuestaOcho : respuesta8
			};

		callAjaxPost(URL + '/accidente/especificacion/save', dataParam,
				function(data) {
					switch (data.CODE_RESPONSE) {
					case "05":
						
						break;
					default:
						alert("Ocurrió un error al guardar el involucrado!");
					}
				});
	} else {
		alert("Debe ingresar todos los campos.");
	}
}
