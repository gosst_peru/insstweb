var banderaEdicion;
//ORA ORA ORA ORA ORA ORA ORA ORA ORA ORA ORA ORA ORA ORA ORA 
var nombreEmpresa;
var rucEmpresa;

var accidenteId;
var accidenteTipoId;
var accidenteInformeId;
var contador2;

var listAccidenteTipo;
var listAccidenteSubTipo;
var listAccidenteSubSubTipo;
var listAccidenteClasificacion;
var listAccidenteSubClasificacion;
var listInforme;
var accidenteTipoNombre;
var listAgente;
var listClasifAgente;
var listPartesCuerpo;
var listTipoLesion;
var listDniNombre;
var listeventos;

var accSubTipoId;
var accSubSubTipoId
var accClasifId;
var accSubClasifId;

var descripcion;
var accDgReporta;
var accDgJefeReporta;
var accDgLugar;
var   accDgFecha;
var accDgHora;
var   epTrabOper;
var   epTrabScrt;
var   epTrabSeguro;
var   etNombre;
var   etRuc;
var    etDireccion;
var   etRubro;
var   etTrabOper;
var   etTrabScrt;
var   etTrabSeguro;
var   doHorasInterr;
var   doValorizacion;
var   doObserv;
var   doEvidencia;
var   invFechaInicio;
var   invFehcaFin;
var   invFallosSG;
var   invAnalisisRazones;
var  	invCausasLaborales;
var   invCausasPersonales;
var   ClasifAgente;
var   invAgente;
var esNuevo;
var listUnidad;
var mdfId;

//Listas TASC
var  causasSugeridas=[];
var subTiposEventos=[];
var subSubTiposSugeridos=[];


$(document).ready(function() {
	
	insertMenu();
	cargarPrimerEstado();
	$("#mdVerif").load("agregaraccionmejora.html");
	resizeDivGosst("wrapper");

});
window.onresize = function(event) {
	resizeDivGosst("wrapper");
}

 
function cargarPrimerEstado() {
	
	 
		
	$("input").val("");
	banderaEdicion = false;
	mdfId=0;
	accidenteId = 0;
	accidenteTipoId = 0;	
	accidenteTipoNombre="";
	descripcion= ' ';
  	accDgLugar= ' ';
    accDgFecha = 0;
    epTrabOper= 0;
    epTrabScrt= 0;
    epTrabSeguro= 0;
    etNombre= ' ';
    etRuc= 0;
    etDireccion= ' ';
    etRubro= ' ';
    etTrabOper= 0;
    etTrabScrt= 0;
    etTrabSeguro= 0;
    doHorasInterr= 0;
    doValorizacion= 0;
    doObserv= 0;
    doEvidencia= 0;
    invFechaInicio= 0;
    invFehcaFin= 0;
    invFallosSG= ' ';
    invAnalisisRazones= ' ';
   	invCausasLaborales= ' ';
    invCausasPersonales= ' ';
    invAgente= ' ';
	    contador2=0;
	    esNuevo=false;
	removerBotones();
	crearBotones();
	$("#fsBotones")
	.append(
			"<button id='btnGenReporte' type='button' class='btn btn-success' title='Generar Informe'>"
					+ "<i class='fa fa-file-word-o fa-2x'></i>"
					+ "</button>");

	deshabilitarBotonesEdicion();
	$("#btnGenReporte").hide();
	$("#btnGenReporte").attr("onclick","javascript:generarHojaRuta();");
	$("#btnNuevo").attr("onclick", "javascript:nuevoAccidente();");
	$("#btnCancelar").attr("onclick", "javascript:cancelarAccidente();");
	$("#btnGuardar").attr("onclick", "javascript:guardarAccidente();");
	$("#btnEliminar").attr("onclick", "javascript:eliminarAccidente();");
	

	$("#btnUpload").hide();
	llamarUnidades();
	
	var dataParam = {
		
			idCompany: sessionStorage.getItem("gestopcompanyid")
		};
		
		callAjaxPost(URL + '/accidente', dataParam,
				procesarDataDescargadaPrimerEstado,
				function(){$("#wrapper").block({message:'<img src="../imagenes/gif/ruedita.gif"></img>cargando...'});});


}
function llamarUnidades(){
	var dataParam = {
				
				companyId : sessionStorage.getItem("gestopcompanyid"),
				
				
				};

			callAjaxPost(URL + '/estadistica/parametro/unidad', dataParam,
					procesarLlamadaUnidades,function(){});
		
	}

function procesarLlamadaUnidades(data){
	
	switch (data.CODE_RESPONSE) {
	case "05":
		
		var listaUnidades=data.unidades;
		
	
		var selUnidad=crearSelectOneMenuY("selUnidad","",listaUnidades,mdfId
				,"mdfId","mdfTipoId","mdfNombre");
		
			$("#slcUnidadAcc").html(selUnidad);
			$("#labelUnidadAcc").html("Unidad Referida");
			break;
			
	default:
		alert("Ocurrió un error al traer indicadores!"+data.CODE_RESPONSE);

}
	
	
}

function cancelarModal(idModal){
	switch(idModal){
	case 1:
		$("#accDescr").val(descripcion);
		
		break;
case 2:

	$("#dgReporta").val(accDgReporta);
	$("#dgJefe").val(accDgJefeReporta);	
	$("#dgLugar").val(accDgLugar);
	$("#dgFecha").val(accDgFecha);		
	$("#dgHora").val(accDgHora);	
		break;
case 3:

	$("#epTrabSeguro").val(epTrabSeguro);
	$("#epTrabOper").val(epTrabOper);
	$("#epTrabScrt").val(epTrabScrt);

	$("#etNombre").val(etNombre);
	$("#etRuc").val(etRuc);
	$("#etDireccion").val(etDireccion);
	$("#etRubro").val(etRubro);
	$("#etTrabOper").val(etTrabOper);
	$("#etTrabScrt").val(etTrabScrt);
	$("#etTrabSeguro").val(etTrabSeguro);
	
	break;
case 4:


	$("#doHorasInterr").val(doHorasInterr);
	$("#doValorizacion").val(doValorizacion);
	$("#doObserv").val(doObserv);
	
	
	break;
case 5:
	
	$("#invFechaInicio").val(invFechaInicio);
	$("#invFehcaFin").val(invFehcaFin);
	$("#invAnalisisRazones").val(invAnalisisRazones);
	$("#invFallosSG").val(invFallosSG);
	$("#invCausasLaborales").val(invCausasLaborales);
	$("#invCausasPersonales").val(invCausasPersonales);
	$("#inputAgente").val(invAgente);
	break;
	
	}
	
	
	

	
}

function devolverTercero(tercero){
	if(tercero==1){
		return tercero +" involucrado";
	}
	if(tercero==0){
		return "No hay involucrados";
	}
	if(tercero>1){
		return tercero +" involucrados";
	}
}

function ponerDescripcion(tipoId,descripcion,subtipo){
	
	if(tipoId==3){
		return descripcion;
	}else{
		return subtipo
	}
}

function procesarDataDescargadaPrimerEstado(data) {
	
	
	switch (data.CODE_RESPONSE) {
	
	case "05":
		
		var list = data.list;

		
		$("#tblAcc tbody tr").remove();
		
	
	
		for (index = 0; index < list.length; index++) {
	
			
			var subTipoAcc=list[index].accidenteSubTipoId;
			var fechaAuxAcc=list[index].fecha.substring(0,10).split("-");
			$("#tblAcc tbody").append(

					"<tr id='tr" + list[index].accidenteId
							+ "' onclick='javascript:editarAccidente("
							+ list[index].accidenteId + ","
							+ list[index].accidenteInformeId +","
							+ list[index].accidenteTipoId 
						+ ")' >"
							+"<td>"+(index+1)+"</td>"						
							+ "<td id='tdtipInf" + list[index].accidenteId
							+ "'>" + list[index].accidenteInformeNombre + "</td>"

							+ "<td id='tdaccTipo" + list[index].accidenteId + "'>"
							+ list[index].accidenteTipoNombre +" "	+list[index].accidenteSubClasificacionNombre+"</td>"

							+ "<td id='tdEsp" + list[index].accidenteId
							+ "'>"
							
							
							+ponerDescripcion(list[index].accidenteTipoId
									,list[index].accidenteDescripcion
									,list[index].accidenteSubTipoNombre+" " )
							
						
							+"</td>"
							
							

							+ "<td id='tdDG" + list[index].accidenteId
							+ "'>"+fechaAuxAcc[2]+"-"+fechaAuxAcc[1]+"-"+fechaAuxAcc[0]+"</td>"
							+ "<td id='tdInc" + list[index].accidenteId
							+ "'>...</td>"
							+ "<td id='tdInv" + list[index].accidenteId
							+ "'>"+list[index].nombreAccidentadosInterno +
							(list[index].nombreAccidentadosExterno.length>0?
									", "+list[index].nombreAccidentadosExterno:"") +"</td>"
							
							+ "<td id='tdDM" + list[index].accidenteId
							+ "'>S/. "+ list[index].operacionValor +" </td>"
							
							+ "<td id='tdIA" + list[index].accidenteId
							+ "'>"+list[index].numImpac+" Impacto</td>"
							
							+ "<td id='tdInve" + list[index].accidenteId
							+ "'>"+ list[index].investigacionAnalisisRazones 
							+"</td>"
							+ "<td id='tdevi" + list[index].accidenteId
							+ "'>"+list[index].evidenciaNombre+"</td>"
							+ "<td id='tdAM" + list[index].accidenteId
							+ "'>"+list[index].iconoEstadoAccion+list[index].nombreAccionMejora+"</td>"
							+ "</tr>");
			
		}
		
		formatoCeldaSombreable(true);$("#wrapper").unblock();
		if(getSession("linkCalendarioAccidenteId")!=null){
		
			list.every(function(val,index3){
				
				if(val.accidenteId==parseInt(getSession("linkCalendarioAccidenteId"))){
					editarAccidente(val.accidenteId ,
							val.accidenteInformeId,val.accidenteTipoId );
					sessionStorage.removeItem("linkCalendarioAccidenteId");
					return false;
				}else{
					return true;
				}
			});
			
		}
		nombreEmpresa=sessionStorage.getItem("gestopcompanyname");
		rucEmpresa=sessionStorage.getItem("gestopcompanyruc");
		
		listAccidenteSubTipo = data.listAccSubTipo;
		
		listAccidenteSubSubTipo = data.listAccSubSubTipo;
		
		 listAgente=data.listagente;
		 listClasifAgente=data.listclasifage;
		 listPartesCuerpo=data.listparte;
		 listTipoLesion=data.listLesion;
		 listDniNombre=data.listDniNombre;
		 listeventos=data.listEventos;
		 
		 causasSugeridas=data.causasSugeridas;
		 subTiposEventos=data.subTiposEventos;
		 subSubTiposSugeridos=data.subSubTiposSugeridos;
		 completarBarraCarga();
		break;
	default:
		alert("Ocurrió un error al traer los Accidentes!");
	}
	if (contador2 == 0) {
		goheadfixed('table.fixed');
	}
	contador2 = contador2 + 1;
}


function editarAccidente(paccidenteId, paccidenteInformeId, paccidenteTipoId){
	
		if (!banderaEdicion) {
			
			formatoCeldaSombreable(false);
		accidenteId = paccidenteId;
		accidenteTipoId = paccidenteTipoId;
		accidenteInformeId=paccidenteInformeId;
		
		var dataParam = {
					accidenteId: accidenteId
			};
		
		callAjaxPost(URL + '/accidente/detalle', dataParam,
				function (data){
			switch (data.CODE_RESPONSE) {
				
				case "05":
					
					var list = data.list;
					nombreEmpresa=sessionStorage.getItem("gestopcompanyname");
					rucEmpresa=sessionStorage.getItem("gestopcompanyruc");
					
					 accSubSubTipoId=list.accidenteSubSubTipoId;
					 accClasifId=list.accidenteClasificacionId;
					 accSubClasifId=list.accidenteSubClasificacionId;
					 accSubTipoId=list.accidenteSubTipoId;
						
				  	descripcion= list.accidenteDescripcion;
				  	
				  	accDgReporta=list.encargadoReportar;
				  	 accDgJefeReporta=list.jefeEncargadoReportar;
				  	accDgLugar= list.lugar;
				    accDgFecha=list.fecha.substring(0,10);
				    accDgHora=list.fecha.substring(11,19);
				    epTrabOper= list.trabOperacion;
				    epTrabScrt= list.trabSCRT;
				    epTrabSeguro= list.trabAsegurados;
				    etNombre= list.terceroNombre;
				    etRuc= list.terceroRuc;
				    etDireccion= list.terceroDireccion;
				    etRubro= list.terceroRubro;
				    etTrabOper= list.tercerotrabOperacion;
				    etTrabScrt= list.tercerotrabSCRT;
				    etTrabSeguro= list.tercerotrabAsegurados;
				    doHorasInterr= list.operacionHorasInterrupcion;
				    doValorizacion= list.operacionValor;
				    doObserv= list.operacionObservacion;
				    doEvidencia= list.operacionEvidencia;
				    invFechaInicio= list.investigacionFechaInicio;
				    invFehcaFin= list.investigacionFechaFin;
				    invFallosSG= list.investigacionFallosSistema;
				    invAnalisisRazones= list.investigacionAnalisisRazones;
				   	invCausasLaborales= list.investigacionCausaLaboral;
				    invCausasPersonales= list.investigacionCausaPersonal;
				    invAgente= list.investigacionAgente;
				    ClasifAgente=list.investigacionClasifAgenteId;
				mdfId=list.unidadId;
				llamarUnidades();
					
				    verCategAcordeTipoAccidente();
				    
				    
				    
					$("#accDescr").val(descripcion);
					
					$("#dgReporta").val(accDgReporta);
					$("#dgJefe").val(accDgJefeReporta);	
					$("#dgLugar").val(accDgLugar);
					$("#dgFecha").val(accDgFecha);		
					$("#dgHora").val(accDgHora);	
					$("#epTrabSeguro").val(epTrabSeguro);
					$("#epTrabOper").val(epTrabOper);
					$("#epTrabScrt").val(epTrabScrt);
					
					
					$("#etNombre").val(etNombre);
					$("#etRuc").val(etRuc);
					$("#etDireccion").val(etDireccion);
					$("#etRubro").val(etRubro);
					$("#etTrabOper").val(etTrabOper);
					$("#etTrabScrt").val(etTrabScrt);
					$("#etTrabSeguro").val(etTrabSeguro);
					

					$("#doHorasInterr").val(doHorasInterr);
					$("#doValorizacion").val(doValorizacion);
					$("#doObserv").val(doObserv);
					

					$("#invFechaInicio").val(invFechaInicio);
					$("#invFehcaFin").val(invFehcaFin);
					$("#invAnalisisRazones").val(invAnalisisRazones);
					$("#invFallosSG").val(invFallosSG);
					$("#invCausasLaborales").val(invCausasLaborales);
					$("#invCausasPersonales").val(invCausasPersonales);
					$("#inputAgente").val(invAgente);
					
					
					$("#tdtipInf" + accidenteId)
							.html("<select id='selTipoInf' class='form-control' onclick='verCategAcordeInforme()'><option value='1'>Preliminar</option>" +
													"<option value='2'>Completo</option></select>"	);
					 $('#selTipoInf > option[value="'+accidenteInformeId+'"]').attr('selected', 'selected');
					
					
				
					var selAccSub1 = crearSelectOneMenuY("selAccSub", "verCategAcordeSubTipo()", listAccidenteSubTipo,
							accSubTipoId, "accidenteSubTipoId","accidenteTipoId", "accidenteSubTipoNombre");

						
					var selAccSubSub1 = crearSelectOneMenuY("selAccSubSub", "", listAccidenteSubSubTipo, accSubSubTipoId,
							"accidenteSubSubTipoId", "accidenteSubTipoId","accidenteSubSubTipoNombre");
					
					$("#slcTipo").html("<label id='labelTipo'>Tipo de Accidente</label>"+selAccSub1+"<br>");
					$("#slcSubtipo").html("<label>SubTipo de Accidente</label>"+selAccSubSub1+"<br>");
					
					$('#clasifAcc > option[value="'+accClasifId+'"]').attr('selected', 'selected');
					$('#subClasiffAcc > option[value="'+accSubClasifId+'"]').attr('selected', 'selected');
					
					$("#tdEsp"+accidenteId).html("<a onclick='verEspecificacion(0)' >Detallar</a>");
					$("#tdInc"+accidenteId).html("<a onclick='verCategorias(0)' >Detallar</a>");
					$("#tdDG"+accidenteId).html("<a onclick='verDatosGenerales(0)' id='op2'>Detallar</a>");
					$("#tdInv"+accidenteId).html("<a onclick='verInvolucrados(0)' id='op3'>Detallar</a>");
					$("#tdDM"+accidenteId).html("<a onclick='verDanioMaterial(0)' id='op4'>Detallar</a>");
					$("#tdIA"+accidenteId).html("<a onclick='verImpactoAmbiental(0)' id='op5'>Detallar</a>");
					$("#tdInve"+accidenteId).html("<a onclick='verInvestigacion(0)' id='op6'>Detallar</a>");
					$("#tdAM"+accidenteId).html("<a onclick='verAccionMejora(0)' id='op7'>Detallar</a>");
					
					var evidenciaNombre=$("#tdevi" + accidenteId).text();
					$("#tdevi" + accidenteId)
					.html(
							"<a href='"
									+ URL
									+ "/accidente/evidencia?accidenteId="
									+ accidenteId
									+ "' "
									+ "target='_blank'>"+evidenciaNombre+"</a>"
									+ "<br/><a href='#' onclick='javascript:mostrarCargarImagen("
									+ accidenteId + ")'  id='subirimagen"
									+ accidenteId + "'>Subir</a>");
					

					banderaEdicion = true;
					var restriccionAccionId=0;
					switch(list.accidenteTipoId){
					case 1:
						restriccionAccionId=6;
						break;
					case 2:
						restriccionAccionId=7;
						break;
					case 3:
						restriccionAccionId=8;
						break;
					}
					$("#tdAM" + accidenteId).html(
							"<a href='#' onclick='javascript:agregarAccionMejora(4, "
									+ accidenteId + ","+restriccionAccionId+");'>Seleccionar</a>");
					verCategAcordeInforme();
					habilitarBotonesEdicion();
					cargarModalEsp();
					mostrarCat();
					verCategAcordeTipoAccidente();
					$("#btnGenReporte").show();
				
				
					break;
				default:
					alert("Ocurrió un error al traer el detalle!");
				}
				
				
			});
		
		
	    /** 1**/
	   
	}
}

function nuevoAccidente() {
	
	
	if (!banderaEdicion) {
		$("input").val("");
		$("textarea").val("");
		esNuevo=true;
		formatoCeldaSombreable(false);
		var selAccSub = crearSelectOneMenuY("selAccSub", "verCategAcordeSubTipo()", listAccidenteSubTipo,
				"-1", "accidenteSubTipoId","accidenteTipoId", "accidenteSubTipoNombre");

		var selAccSubSub = crearSelectOneMenuY("selAccSubSub", "", listAccidenteSubSubTipo, "-1",
				"accidenteSubSubTipoId", "accidenteSubTipoId","accidenteSubSubTipoNombre");
			
		$("#slcTipo").html("<label>Tipo </label>"+selAccSub+"<br>");
		$("#slcSubtipo").html("<label>SubTipo de Accidente</label>"+selAccSubSub+"<br>");
		
		$("#tblAcc tbody")
				.append(
						"<tr id='0'>"
						+ "<td>...</td>"
								+ "<td>"
								
								+ "<select id='selTipoInf' class='form-control' onclick='verCategAcordeInforme()'><option value='1'>Preliminar</option>" +
										"<option value='2'>Completo</option></select>"
								+ "</td>"
								+ "<td><select id='selTipoAcc' onchange='verCategAcordeTipoAccidente()'><option value='1'>Accidente</option>" +
										"<option value='2'>Incidente</option>" +
										"<option value='3'>Enfermedad</option></select></td>"
								+ "<td><a onclick='verEspecificacion(0)' >Detallar</a></td>"
								+ "<td><a onclick='verDatosGenerales(0)' id='op2'>Detallar</a></td>"
								+ "<td>...</td>"
								+ "<td>..</td>"
								+ "<td>...</td>"
								+ "<td>...</td>"
								+ "<td>...</td>"
								+ "<td>...</td>"
								+ "<td>...</td>"
								+ "</tr>");
		llevarTablaFondo("wrapper");
		habilitarBotonesNuevo();
		verCategAcordeTipoAccidente();
		verCategAcordeInforme();
		banderaEdicion = true;
	} else {
		alert("Guarde primero.");
	}
}

function cancelarAccidente() {
	cargarPrimerEstado();
}

function eliminarAccidente() {
	var r = confirm("¿Está seguro de eliminar el accidente?");
	if (r == true) {
		var dataParam = {
			accidenteId : accidenteId
		};

		callAjaxPost(URL + '/accidente/delete', dataParam,
				procesarResultadoEliminarACC);
	}
}

function procesarResultadoEliminarACC(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		cargarPrimerEstado();
		break;
	default:
		alert("Ocurrió un error al eliminar el accidnte!");
	}
}

function guardarAccidente() {
	
	
	var campoVacio = true;
	var TipoInf=$("#selTipoInf").val();
	var TipoAcc;

	
	var subTipoAcc=$("#selAccSub").val().split('-');
	
var aux1=subTipoAcc[0];
	var subClasifAcc=$("#subClasiffAcc option:selected").val();
	var clasifAcc=$("#clasifAcc option:selected").val();
	if(esNuevo){
		 TipoAcc=$("#selTipoAcc").val();
		 subClasifAcc=null;
		 clasifAcc=null;
		
	}else{
		 TipoAcc=accidenteTipoId;
		 
			
		 
	}
	
	if(accidenteTipoId!=1){
		subClasifAcc=null;
		clasifAcc=null;
	}
	
	
	var subSubTipoAcc=$("#selAccSubSub").val().split('-');
	var aux2=subSubTipoAcc[0];
	
	/** 1**/
	var descripcion=$("#accDescr").val();
	
	var rpta1=$("#accRpta1").is(':checked');
	var rpta2=$("#accRpta2").is(':checked');
	var rpta3=$("#accRpta3").is(':checked');
	var rpta4=$("#accRpta4").is(':checked');
	var rpta5=$("#accRpta5").is(':checked');
	var rpta6=$("#accRpta6").is(':checked');
	var rpta7=$("#accRpta7").is(':checked');
	var rpta8=$("#accRpta8").is(':checked');
	
	var enfrpta1=$("#enfRpta1").is(':checked');
	var enfrpta2=$("#enfRpta2").is(':checked');
	
	/** 2**/
	var accDgReporta=$("#dgReporta").val();
	var accDgJefe=$("#dgJefe").val();
	var accDgLugar=$("#dgLugar").val();
	
	
	var accDgHora=$("#dgHora").val();;
	var accDgFecha= $("#dgFecha").val();

	if(accDgFecha.length == 0){
		var d = new Date();

		var month = d.getUTCMonth()+1;
		var day = d.getUTCDate();

		var fechaHoy = d.getUTCFullYear() + '-' +
		    (month<10 ? '0' : '') + month + '-' +
		    (day<10 ? '0' : '') + day;
		accDgFecha = fechaHoy;
	}
	
	if(accDgHora.length == 0){
		var tiempo = new Date();

		var hora = tiempo.getHours();
		var minuto = tiempo.getMinutes();
		var segundo = tiempo.getSeconds(); 
		
		
		accDgHora = hora + ":" +
		minuto + ":00"  ;
	}
	/** 3**/
	
	var epTrabSeguro=$("#epTrabSeguro").val();
	var epTrabOper=$("#epTrabOper").val();
	var epTrabScrt=$("#epTrabScrt").val();
	
	if(epTrabSeguro.length == 0){
		epTrabSeguro =0;
	}
	
	if(epTrabOper.length == 0){
		epTrabOper = 0;
	}
	
	if(epTrabScrt.length == 0){
		epTrabScrt = 0;
	}
	/** 4**/
	var etNombre=$("#etNombre").val();
	var etRuc=$("#etRuc").val();
	var etDireccion=$("#etDireccion").val();
	var etRubro=$("#etRubro").val();
	var etTrabOper=$("#etTrabOper").val();
	var etTrabScrt=$("#etTrabScrt").val();
	var etTrabSeguro=$("#etTrabSeguro").val();
	
	if(etRuc.length == 0){
		etRuc =0;
	}
	
	if(etTrabOper.length == 0){
		etTrabOper = 0;
	}
	
	if(etTrabScrt.length == 0){
		etTrabScrt = 0;
	}
	if(etTrabSeguro.length == 0){
		etTrabSeguro = 0;
	}
	/** 5**/
	
	var doHorasInterr=$("#doHorasInterr").val();
	var doValorizacion=$("#doValorizacion").val();
	var doObserv=$("#doObserv").val();
	var doEvidencia="falta definir";

	if(doValorizacion.length == 0){
		doValorizacion = 0;
	}
	if(doHorasInterr.length == 0){
		doHorasInterr = 0;
	}
	/** 6**/
	
	var invFechaInicio=$("#invFechaInicio").val();
	var invFehcaFin=$("#invFehcaFin").val();
	var invAnalisisRazones=$("#invAnalisisRazones").val();
	var invFallosSG=$("#invFallosSG").val();
	var invCausasLaborales=$("#invCausasLaborales").val();
	var invCausasPersonales=$("#invCausasPersonales").val();
	var invAgente=$("#inputAgente").val();
	var unidadId=$("#selUnidad").val().split("-");
	if(invFechaInicio.length == 0){
		invFechaInicio = null;
	}
	if(invFehcaFin.length == 0){
		invFehcaFin = null;
	}
	
	var clasifAgente= $("#selClasifAgente").val();
	
	if(clasifAgente<0){
		clasifAgente=1;
	}

	if (campoVacio) {
	
		var dataParam = {
				
			idCompany : sessionStorage.getItem("gestopcompanyid"),
			accidenteDescripcion: descripcion,
			lugar : accDgLugar,
			encargadoReportar:accDgReporta,
			jefeEncargadoReportar:accDgJefe,
			fecha : accDgFecha+" "+accDgHora,
			fechaN: accDgFecha,
			trabOperacion : epTrabOper,
			trabSCRT : epTrabScrt,
			unidadId: unidadId[0],
			trabAsegurados : epTrabSeguro,
			terceroNombre : etNombre,
			terceroRuc : etRuc,
			terceroDireccion :  etDireccion,
			terceroRubro : etRubro,
			tercerotrabOperacion : etTrabOper,
			tercerotrabSCRT : etTrabScrt,
			tercerotrabAsegurados : etTrabSeguro,
			operacionHorasInterrupcion : doHorasInterr,
			operacionValor : doValorizacion,
			operacionObservacion : doObserv,
			operacionEvidencia : doEvidencia,
			investigacionFechaInicio : invFechaInicio,
			investigacionFechaFin : invFehcaFin,
			investigacionFallosSistema : invFallosSG,
			investigacionAnalisisRazones : invAnalisisRazones,
			investigacionCausaLaboral :	invCausasLaborales,
			investigacionCausaPersonal : invCausasPersonales,
			investigacionAgente : invAgente,
			accidenteId : accidenteId,
			accidenteTipoId : TipoAcc,
			accidenteInformeId : TipoInf,
			accidenteSubTipoId : aux1,
			accidenteSubSubTipoId: aux2,
			
			accidenteClasificacionId:clasifAcc,
			accidenteSubClasificacionId :subClasifAcc,
			investigacionClasifAgenteId:clasifAgente,
			accidenteSubTipoNombre:$("#selAccSub option:selected").text()
			};

		callAjaxPost(URL + '/accidente/save', dataParam,
				procesarResultadoGuardarAccidente);
	} else {
		alert("Debe ingresar todos los campos.");
	}
}

function procesarResultadoGuardarAccidente(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		cargarPrimerEstado();
		break;
	default:
		alert("Ocurrió un error al guardar el accidente!"+data.CODE_RESPONSE);
	}
}




function mostrarCargarImagen(accidenteId) {
	$('#mdUpload').modal('show');
	$('#btnUploadEvidencia').attr("onclick",
			"javascript:uploadEvidencia(" + accidenteId + ");");
}

function uploadEvidencia(accidenteId) {
	var inputFileImage = document.getElementById("fileEvi");
	var file = inputFileImage.files[0];
	var data = new FormData();

	if (file.size > bitsEvidenciaAccidente) {
		alert("Lo sentimos, solo se pueden subir archivos menores a "+bitsEvidenciaAccidente+" MB");
	} else {
		data.append("fileEvi", file);
		data.append("accidenteId", accidenteId);
		
		var url = URL + '/accidente/evidencia/save';
		$.blockUI({message:'cargando...'});
		$
				.ajax({
					url : url,
					xhrFields: {
			            withCredentials: true
			        },
					type : 'POST',
					contentType : false,
					data : data,
					processData : false,
					cache : false,
					success : function(data, textStatus, jqXHR) {
						$.unblockUI();
						switch (data.CODE_RESPONSE) {
						case "06":
							sessionStorage.clear();
							document.location.replace(data.PATH);
							break;
						default:
							console.log('Se subio el archivo correctamente.');
							$('#mdUpload').modal('hide');
						}

					},
					error : function(jqXHR, textStatus, errorThrown) {
						$.unblockUI();
						alert("Ups, tenemos problemas tecnicos. Intente más tarde. Error: "
								+ errorThrown);
						console.log('xhRequest: ' + jqXHR + "\n");
						console.log('ErrorText: ' + textStatus + "\n");
						console.log('thrownError: ' + errorThrown + "\n");
					}
				});
	}
}


/** ********Lógica básica de botones************************** */





function verEspecificacion(){
	
	modificarAcordeClasificacion();
	
	 $("#mdEspecificacion").modal('show');

}
function verDatosGenerales(){
	
	$("#mdDatoGeneral").modal('show');

	}



function verCategAcordeTipoAccidente(){


	if(esNuevo){
		var subTipoAcc=$("#selTipoAcc").val();

	 accidenteTipoId=parseInt(subTipoAcc);
		
	}
switch(accidenteTipoId){
case 2:
	
 $("#labelTipo").html("Tipo de incidente");
  $("#slcTipo").show();

 $("#slcSubtipo").hide();
  $("#clasifAcc").hide();
   $("#op4").show();
  $("#op5").show();
  
  $("#selAccSub option").each(function()
			{	
	   			
			  
				  
				   $(this).show();
			   
			  
			   
			});
  $("#selAccSub option").each(function()
			{	
	   			
			   var arr1= $(this).val().split('-');
			   
			   if(arr1[1]!='2'){
				  
				   $(this).hide();
			   }
			  
			   
			});
  
$("#equipoInvestigacion").show();
$("#invTestigos").show();
$("#causasInmeditas").show();
$("#actoInseguro").show();
$("#agente").hide();

 break;
case 1:
	
 $("#labelTipo").html("Tipo de accidente");
  
 $("#labelSubtipo").html("Subtipo de accidente");
 $("#slcTipo").show();

 $("#clasifAcc").show();
 $("#selAccSub option").each(function()
			{	
	   			
			  
				  
				   $(this).show();
			   
			  
			   
			});
 $("#selAccSub option").each(function()
			{	
	   			
			   var arr1= $(this).val().split('-');
			   
			   if(arr1[1]!='1'){
				  
				   $(this).hide();
			   }
			  
			   
			});
 
 $("#op4").show();
  $("#op5").show();
$("#equipoInvestigacion").show();
$("#invTestigos").show();
$("#causasInmeditas").show();
$("#actoInseguro").show();

$("#agente").hide();
  break;
 
 case 3:
	 $("#labelTipo").html("Tipo de enfermedad");
	  $("#slcTipo").show();
	  $("#selAccSub option").each(function()
				{	
		   			
				  
					  
					   $(this).show();
				   
				  
				   
				});
	 $("#selAccSub option").each(function()
				{	
		   			
				   var arr1= $(this).val().split('-');
				   
				   if(arr1[1]!='3'){
					  
					   $(this).hide();
				   }
				  
				   
				});
 $("#slcSubtipo").hide();
  $("#clasifAcc").hide();
	$("#op4").hide();
  $("#op5").hide();

  $("#equipoInvestigacion").show();
$("#listaCategoria").hide();
$("#invTestigos").hide();
$("#causasInmeditas").hide();
$("#actoInseguro").hide();
$("#agente").show();
  break;
 }


}

function verCategAcordeInforme(){
switch($("#selTipoInf").val()){
case "1":

$("#empresaPrincipal").hide();
$("#detalleTercero").hide();
$('#tblIA td:nth-child(3),#tblIA th:nth-child(3)').hide();
$('#tblIA td:nth-child(4),#tblIA th:nth-child(4)').hide();
$('#tblIA td:nth-child(5),#tblIA th:nth-child(5)').hide();
$("#op6").hide();
$("#op7").hide();
$("#dañosOperativos").hide();

  break;
case "2":
	
$("#empresaPrincipal").show();
$("#detalleTercero").show();
$('#tblIA td:nth-child(3),#tblIA th:nth-child(3)').show();
$('#tblIA td:nth-child(4),#tblIA th:nth-child(4)').show();
$('#tblIA td:nth-child(5),#tblIA th:nth-child(5)').show();
$("#op6").show();
$("#op7").show();
$("#dañosOperativos").show();


  break;
 
 }
mostrarCat();

}

function verCategAcordeSubTipo(){
	$("#slcSubtipo").show();
	$("#slcTipo").show();
	$('#selAccSubSub > option[value="-1"]').attr('selected', 'selected');
	   $("#selAccSubSub option").each(function()
				{	
		   			
					   $(this).show();

						   
				  
				   
				});
	var contar=0;
	var prese= $("#selAccSub option:selected").val();
			   var arr= prese.split('-');
			   
			   $("#selAccSubSub option").each(function()
						{	
				   			
						   var arr1= $(this).val().split('-');
						   
						   if(arr[0]==arr1[1]){
							  
							   	contar=contar+1;
						   }
						  
						   
						});
			   
		
	if(contar==0){
		$("#slcSubtipo").hide();
		
	}else{
		$("#slcSubtipo").show();
		var presee= $("#selAccSub option:selected").val();
		   var arrr= presee.split('-');
		   
		   $("#selAccSubSub option").each(function()
					{	
			   			
					   var arr11= $(this).val().split('-');
					   
					   if(arrr[0]!=arr11[1]){
						   
						   $(this).hide();

							   }
					  
					   
					});
		
	}
}


function mostrarCat(){

if($("#accRpta1").prop("checked")){
	 $("#empresaTercera").show();
}else{
	 $("#empresaTercera").hide();
}

if($("#accRpta2").prop("checked")){
	 $("#empresaPrincipal").show();
}else{
	 $("#empresaPrincipal").hide();
}
	
if($("#accRpta3").prop("checked")){
	 $("#trabajadoresAfectados").show();
}else{
	 $("#trabajadoresAfectados").hide();
}
	



if(!$("#accRpta1").prop("checked") && !$("#accRpta2").prop("checked")){
	
	 $("#op4").show();
		 $("#accRpta3").prop("checked","");
	
}

if($("#accRpta5").prop("checked")){
	
	 $("#daniosVehiculares").show();
	 
}else{
	
	 $("#daniosVehiculares").hide();
}
	
if($("#accRpta6").prop("checked")){
	 $("#daniosOperativos").show();
	 $("#accRpta4").prop("checked","checked");
}else{
	 $("#daniosOperativos").hide();
}
	
if($("#accRpta7").prop("checked")){
	 $("#op5").show();
}else{
	 $("#op5").hide();
}
	
if($("#accRpta8").prop("checked")){
	 $("#invTestigos").show();
}else{
	 $("#invTestigos").hide();
}
	

if(!$("#accRpta5").prop("checked") && !$("#accRpta6").prop("checked")){
	
	 $("#op4").hide();
		
}




}
	
function modificarAcordeClasificacion(){
	
	switch($("#clasifAcc #clasifAcc").val()){
	case "1":
	
		$("#nivelIncapacitante").hide();
	


	  break;
	case "2":
	
		$("#nivelIncapacitante").show();

	  break;
	case "3":
	
		$("#nivelIncapacitante").hide();
	


		  break;
	 
	 }
	
}

function generarHojaRuta(){

	switch(parseInt(accidenteTipoId)){
	
	case 1:
		window.open(URL
				+ "/accidente/informe/accidente?accidenteId="
				+ accidenteId 
			);	
		break;
	case 2:
		window.open(URL
				+ "/accidente/informe/incidente?accidenteId="
				+ accidenteId 
			
				);	
		break;
	case 3:
		window.open(URL
				+ "/accidente/informe/enfermedad?accidenteId="
				+ accidenteId 
			
				);	
		break;
	default:
		break;
	
	}
	
}





