
var banderaedicion11;
var testigoId;
var nombre;
var testigoFecha;
var declaracion;
var lugar;
var horaInicio;
var horaFin;
var investigadorId;
var tipoTestigo;
 
var listInvestigadores;


function modificarAcordeTipoTesti(){
	var tipoSeleccionado= $("#testigoTipo").val();
	$("#testigoNombre").val("");
	var listanombretab=listarStringsDesdeArray(listDniNombre, "nombre");
	if(tipoSeleccionado==2){
		

		
		$("#testigoNombre").autocomplete({
			
			source : ""
		});
	
		
		
	}else{
		
		$("#testigoNombre").autocomplete({
			
			source : listanombretab,
			minLength : 0
		}).focus(function() {
			$(this).autocomplete('search', '');
		});
		
		$("#testigoNombre").keypress(function(e){
			llenarCamposAutoTrab();		});
		$("#testigoNombre").keyup(function(e){
			llenarCamposAutoTrab();		});
		$("#testigoNombre").keydown(function(e){
			llenarCamposAutoTrab();		});
		$(document).click( function(){
			llenarCamposAutoTrab();
			});
		
	

	}
	
	
}


function cargarModalTesti() {
	$("#btnAgregarTesti").attr("onclick", "javascript:nuevoTesti();");
	$("#btnCancelarTesti").attr("onclick", "javascript:cancelarTesti();");
	$("#btnGuardarTesti").attr("onclick", "javascript:guardarTesti();");
	$("#btnEliminarTesti").attr("onclick", "javascript:eliminarTesti();");
	$("#testigoTipo").attr("onchange", "javascript:modificarAcordeTipoTesti();");
	
	$("#btnCancelarTesti").hide();
	$("#btnAgregarTesti").show();
	$("#btnEliminarTesti").hide();
	$("#btnGuardarTesti").hide();

	$("#declaracionTesti").hide();
	
	 banderaedicion11=false;
	 testigoId=0;
	 nombre="";
	 declaracion="";
	 lugar="";
	 horaInicio="";
	 horaFin="";
	 investigadorId=0;
	 tipoTestigo=0;

	var dataParam = {
			accidenteId : accidenteId
	};

	callAjaxPost(URL + '/accidente/testigo', dataParam, function(data) {
		switch (data.CODE_RESPONSE) {
		case "05":
			var list = data.lista;
			listInvestigadores=data.listInvestigadores;
			$("#tblTesti tbody tr").remove();
			
			for (index = 0; index < list.length; index++) {
				
				$("#tblTesti tbody").append(
						"<tr id='tr" + list[index].testigoId
								+ "' onclick='javascript:editarTesti("
								+ list[index].testigoId + ",&quot;"
								+ list[index].nombre + "&quot;,&quot;"
								+ list[index].declaracion + "&quot;,&quot;"
								+ list[index].lugarDeclarado + "&quot;,&quot;"
								+ list[index].horaInicio + "&quot;,&quot;"
								+ list[index].horaFin + "&quot;,"
								+ list[index].investigadorId +  ","
								+ list[index].testigoTipoId +  ")' >"

								+ "<td id='tdtipo"
								+ list[index].testigoId + "'>"
								+ list[index].testigoTipoNombre + "</td>"
								+ "<td id='tdnombre"
								+ list[index].testigoId + "'>"
								+ list[index].nombre + "</td>"
								+ "<td id='tddecl" 
								+	list[index].testigoId +	"'>..</td>"
								+ "</tr>");
			}

			break;
		default:
			alert("Ocurrió un error al traer las programaciones!");
		}
	});
}

function cancelarTesti(){
	cargarModalTesti();
}
function nuevoTesti(){
	 banderaedicion11=false;

	if (!banderaedicion11) {
		
		$("#tblTesti tbody")
				.append(
						"<tr id='0'>"
						+"<th><select id='testigoTipo' onchange='modificarAcordeTipoTesti()'>"
						+"<option value='1'>Interno</option>"
						+"<option value='2'>Externo</option></select></th>"
						+"<th><input type='text' id='testigoNombre'></th>"
						+"<th><a onclick='verDeclaracion()'>Detalle</a></th>"
						+ "</tr>");
		testigoId = 0;
		modificarAcordeTipoTesti();
		$("#tblTesti tbody tr input").addClass("form-control");
		$("#tblTesti tbody tr select").addClass("form-control");
		$("#btnCancelarTesti").show();
		$("#btnAgregarTesti").hide();
		$("#btnEliminarTesti").hide();
		$("#btnGuardarTesti").show();
		$("declaracionTesti input").val("");	
		$("#tblTesti tbody tr input").addClass("form-control");
		banderaedicion11 = true;
	} else {
		alert("Guarde primero.");
	};

	var selInvestigador = crearSelectOneMenu("selInvestigador", "", listInvestigadores,
			"-1", "investigadorId", "nombre");

	
	$("#listInves").html(selInvestigador);
}

function eliminarTesti(){
	var r = confirm("¿Está seguro de eliminar el testigo?");
	if (r == true) {
		var dataParam = {
				testigoId : testigoId,
		};

		callAjaxPost(URL + '/accidente/testigo/delete', dataParam,
				function(data) {
					switch (data.CODE_RESPONSE) {
					case "05":
						cargarModalTesti();
						break;
					default:
						alert("Ocurrió un error al eliminar el testigo!");
					}
				});
	}
}


function editarTesti(ptestigoId, pnombre, pdeclaracion,plugar,phoraInicio,
		phoraFin,pinvestigadorId,ptipoTestigo) {

	if (!banderaedicion11) {
		

			
		 testigoId=ptestigoId;
		 nombre=pnombre;
		 declaracion=pdeclaracion;
		 lugar=plugar;
		 horaInicio=phoraInicio;
		 horaFin=phoraFin;
		 investigadorId=pinvestigadorId;
		 tipoTestigo=ptipoTestigo;
		 
		 var selInvestigador = crearSelectOneMenu("selInvestigador", "", listInvestigadores,
					investigadorId, "investigadorId", "nombre");

			
			$("#listInves").html(selInvestigador);
		
		$("#tdtipo" + testigoId)
				.html(
						"<select id='testigoTipo'  onchange='modificarAcordeTipoTesti()'>"
						+"<option value='1'>Interno</option>"
						+"<option value='2'>Externo</option></select>");
 $('#testigoTipo > option[value="'+tipoTestigo+'"]').attr('selected', 'selected');
		
		$("#tdnombre" + testigoId)
		.html(
				"<input type='text' id='testigoNombre'>");
		
		$("#tddecl" + testigoId)
		.html(
				"<a onclick='verDeclaracion()'>Detalle</a>");
		
		$("#testigoNombre").val(nombre);
		$("#testigoFecha").val();
		$("#testigoLugar").val(lugar);
		$("#testigoHoraInicio").val(horaInicio);
		$("#testigoHoraFin").val(horaFin);
		$("#testigoDeclara").val(declaracion);
		$("#testigoLugar").val(lugar);
		modificarAcordeTipoTesti();
		$("#tblTesti tbody tr input").addClass("form-control");
		$("#tblTesti tbody tr select").addClass("form-control");
		$("#declaracion table tbody tr th input").addClass("form-control");
		$("#declaracion table tbody tr textarea").addClass("form-control");
		banderaedicion11 = true;
		$("#btnCancelarTesti").show();
		$("#btnAgregarTesti").hide();
		$("#btnEliminarTesti").show();
		
		$("#btnGuardarTesti").show();
		
		
	}
}


function guardarTesti() {

	var campoVacio = true;

	/***Poner atributos a guardar*****/
	tipoTestigo=$('#testigoTipo').val();
	nombre=$("#testigoNombre").val();
	testigoFecha=$("#testigoFecha").val();
	if(!$("#testigoFecha").val()){
		testigoFecha=null;
	}
	declaracion=$("#testigoDeclara").val();
	nombre=$("#testigoNombre").val();
	lugar=$("#testigoLugar").val();
	horaInicio=$("#testigoHoraInicio").val();
	horaFin=$("#testigoHoraFin").val();
	investigadorId=$("#selInvestigador").val();
	if (campoVacio) {

		var dataParam = {
				accidenteId: accidenteId,
			testigoId:testigoId,
			 nombre:nombre,
			 testigoFecha:testigoFecha,
			 declaracion:declaracion,
			 lugarDeclarado:lugar,
			 horaInicio:horaInicio,
			 horaFin:horaFin,
			 investigadorId:investigadorId,
			 testigoTipoId:  tipoTestigo
			
		};

		callAjaxPost(URL + '/accidente/testigo/save', dataParam,
				function(data) {
					switch (data.CODE_RESPONSE) {
					case "05":
						cargarModalTesti();
						break;
					default:
						alert("Ocurrió un error al guardar el involucrado!");
					}
				});
	} else {
		alert("Debe ingresar todos los campos.");
	}
}





function verDeclaracion(){
	
	$("#declaracionTesti").toggle();
}

