
var banderaedicion9;
var investigadorId;
var nombre;
var cargo;
var proceso;


function verInvestigacion(){
	$("#mdInvestigacion").modal('show');
}

$('#mdInvestigacion').on('show.bs.modal', function(e) {
	restringirFechaInv();
	cargarModalInve();
	cargarEventosAcc();
	cargarModalTesti();
	
	$("#invFehcaFin").attr("onchange", "javascript:restringirFechaInv();");
	$("#invFechaInicio").attr("onchange", "javascript:restringirFechaInv();");
	
	
}); 




function restringirFechaInv(){
	
	return	restringirFecha("invFechaInicio","invFehcaFin","advFechaInv");
}

function modAcordeTipoAgente(){
	var listaagente=listarStringsDesdeArrayY(listAgente, "investigacionAgente","investigacionClasifAgenteId",$("#selClasifAgente").val());
	
	
	 $( "#inputAgente").autocomplete({
	        source: listaagente,
	        minLength : 0
	      }).focus(function() {
				$(this).autocomplete('search', '');
			});


}

function cargarModalInve() {
	 var selClasifAgente = crearSelectOneMenu("selClasifAgente", "modAcordeTipoAgente()", listClasifAgente,
				"-1", "investigacionClasifAgenteId", "investigacionClasifAgenteNombre");
	$("#listTipoAgente").html(selClasifAgente);
	$('#selClasifAgente > option[value="'+ClasifAgente+'"]').attr('selected', 'selected');
	


	
	$("#btnAgregarInve").attr("onclick", "javascript:nuevoInve();");
	$("#btnCancelarInve").attr("onclick", "javascript:cancelarInve();");
	$("#btnGuardarInve").attr("onclick", "javascript:guardarInve();");
	$("#btnEliminarInve").attr("onclick", "javascript:eliminarInve();");
	
	$("#btnCancelarInve").hide();
	$("#btnAgregarInve").show();
	$("#btnEliminarInve").hide();
	$("#btnGuardarInve").hide();


	 banderaedicion9=0;
	 investigadorId=0;
	 nombre="";
	 cargo="";
	 proceso="";
	

	var dataParam = {
		accidenteId : accidenteId
	};

	callAjaxPost(URL + '/accidente/investigador', dataParam, function(data) {
		switch (data.CODE_RESPONSE) {
		case "05":
			var list = data.list;

			$("#tblInve tbody tr").remove();

			for (index = 0; index < list.length; index++) {

				$("#tblInve tbody").append(
						"<tr id='tr" + list[index].id
								+ "' onclick='javascript:editarInve("
								+ list[index].investigadorId + ",&quot;"
								+ list[index].nombre + "&quot;,&quot;"
								+ list[index].cargo + "&quot;,&quot;"
								+ list[index].proceso + "&quot;)' >"

								+ "<td id='tdnombre"
								+ list[index].investigadorId + "'>"
								+ list[index].nombre + "</td>"
								+ "<td id='tdcargo"
								+ list[index].investigadorId + "'>"
								+ list[index].cargo + "</td>"
								+ "<td id='tdproceso"
								+ list[index].investigadorId + "'>"
								+ list[index].proceso + "</td>"
								+ "</tr>");
			}

			break;
		default:
			alert("Ocurrió un error al traer las investigadoes!");
		}
	});
	
	
	
	
}

function cancelarInve(){
	
	cargarModalInve();	
}

function nuevoInve(){
	
	if (!banderaedicion9) {

		$("#tblInve tbody")
				.append(
						"<tr id='0'>"
						+"<th><input type='text' id='inveNombre'></th>"
						+"<th><input type='text' id='inveCargo'></th>"
						+"<th><input type='text' id='inveProceso'></th>"
						+ "</tr>");
		investigadorId = 0;
		$("#btnCancelarInve").show();
		$("#btnAgregarInve").hide();
		$("#btnEliminarInve").hide();
		$("#btnGuardarInve").show();
		$("#tblInve tbody tr input").addClass("form-control");
		banderaedicion9 = true;
	} else {
		alert("Guarde primero.");
	};



}


function eliminarInve(){
	var r = confirm("¿Está seguro de eliminar el investigador?");
	if (r == true) {
		var dataParam = {
				investigadorId : investigadorId,
		};

		callAjaxPost(URL + '/accidente/investigador/delete', dataParam,
				function(data) {
					switch (data.CODE_RESPONSE) {
					case "05":
						cargarModalInve();
						break;
					default:
						alert("Ocurrió un error al eliminar la programacion!");
					}
				});
	}
}

function editarInve(pinvestigadorId, pnombre, pcargo,pproceso) {

	if (!banderaedicion9) {
		

			
		investigadorId=pinvestigadorId ;
		nombre=pnombre ;
		cargo=pcargo ;
		proceso=pproceso;
		

		$("#tdnombre" + investigadorId)
				.html(
						"<input type='text' id='inveNombre'>");

		$("#tdcargo" + investigadorId)
		.html(
				"<input type='text' id='inveCargo'>");
		$("#tdproceso" + investigadorId)
		.html(
				"<input type='text' id='inveProceso'>");
		
		
		$("#inveNombre").val(nombre);
		$("#inveCargo").val(cargo);
		$("#inveProceso").val(proceso);
		
		$("#tblInve tbody tr input").addClass("form-control");

		banderaedicion9 = true;
		$("#btnCancelarInve").show();
		$("#btnAgregarInve").hide();
		$("#btnEliminarInve").show();
		
		$("#btnGuardarInve").show();
		
		
	}
}


function guardarInve() {

	var campoVacio = true;

	var inveNombre=$("#inveNombre").val();
	var inveCargo=$("#inveCargo").val();
	var inveProceso=$("#inveProceso").val();
	
	
	if (campoVacio) {

		var dataParam = {
			accidenteId: accidenteId,
			investigadorId : investigadorId,
			nombre: inveNombre,
			cargo:inveCargo,
			proceso:inveProceso
			
		};

		callAjaxPost(URL + '/accidente/investigador/save', dataParam,
				function(data) {
					switch (data.CODE_RESPONSE) {
					case "05":
						cargarModalInve();
						cargarModalTesti()
						break;
					default:
						alert("Ocurrió un error al guardar la investigador!");
					}
				});
	} else {
		alert("Debe ingresar todos los campos.");
	}
}
