
var banderaedicion10;
var involucradoId;
var dni;
var nombre;
var sexo;
var edad;
var area;
var puesto;
var tiempoPuesto;
var parteCuerpoId;
var tipoLesionId;
var trabAccidId;
var trabajadorId;
var turno;
var horasTrabajadas;
var diasDescanso;
var fechaInicioDescanso;
var listadnitrab;
var listanombretab;
var listTrabAccFull;

function verInvolucrados(){
	
	$("#mdInvolucrados").modal('show');
	
}	


$('#mdInvolucrados').on('show.bs.modal', function(e) {
	
	cargarModalInvo();
	
	
	
});

function cambiarFechaDesc(){
	 var dias=restaFechas($("#fechaDescanso").val(),$("#fechaFinDescanso").val());
	 $("#diasDescanso").html( dias);
	 
	return	restringirFecha("fechaDescanso","fechaFinDescanso","advFechaDesca");

}

function llenarCamposAutoTrab(){
	var nombreTrab=$("#invNombre").val();
	
	if($("#listTipoTrab").val()==1){
		for (var index = 0; index < listDniNombre.length; index++) {
			
			if(listDniNombre[index]["nombre"]==nombreTrab){
			$("#thdni").html(listDniNombre[index]["dni"]);
			$("#thArea").html(listDniNombre[index]["areaNombre"]);
			$("#thPuesto").html(listDniNombre[index]["puestoNombre"]);
			$("#thSexo").html(listDniNombre[index]["sexoNombre"]);
			$("#thEdad").html(listDniNombre[index]["edad"]);
			var antiguedadPuesto= restaFechas(listDniNombre[index].fechaInicioPuesto, accDgFecha);
			 
			$("#thAntiguedadPuesto").html(antiguedadPuesto +" días");
			trabajadorId=listDniNombre[index]["trabajadoresId"];
			break;
			}else{
				$("#thdni").html("");
				$("#thArea").html("");
				$("#thPuesto").html("");
				$("#thSexo").html("");
				$("#thEdad").html("");
				$("#thAntiguedadPuesto").html("");
			
			}
		}
	}

	
	
	
	
}

function modificarAcordeTipoInvo(){
	var tipoSeleccionado= $("#listTipoTrab").val();
	$("#invNombre").val("");
	 listanombretab=listarStringsDesdeArray(listDniNombre, "nombre");
	if(tipoSeleccionado==2){
		
		$("#btnGuardarInvo").attr("onclick", "javascript:guardarInvo();");
		$("#btnEliminarInvo").attr("onclick", "javascript:eliminarInvo();");
		$("#datos2").html("no llenar");
		$("#datost").html("no llenar");
		$("#datosh").html("no llenar");

		$("#thdni").html("<input type='text'  style='width:60px' id='invDni'>");
		$("#invNombre").autocomplete({
			
			source : ""
		});
		$("#thArea").html('<input type="text" id="invArea">');
		$("#thPuesto").html('<input type="text" id="invPuesto">');
		
		$("#thSexo").html('<select id="slcSexo">'
		+'<option value="1">Masculino</option>'
		+'<option value="2">Femenino</option>'
		+'</select></th>');
		$("#thEdad").html('<input type="text" class="form-control"  style="width:100px" id="invEdad">');
		$("#thAntiguedadPuesto").html('<input type="text" class="form-control"  style="width:100px" id="invAntiguedad">');
		
		
		
	}else{
		$("#btnGuardarInvo").attr("onclick", "javascript:guardarTrabAcc()");
		$("#btnEliminarInvo").attr("onclick", "javascript:eliminarTrabAcc()");
		$("#datos2").html("<a onclick='verDescanso()'>Datos Laborales</a>");
		
		$("#datosh").html("<input type='text'  style='width:60px' id='invHoras'>");
		$("#thdni").html("");
		$("#invNombre").autocomplete({
			
			source : listanombretab,
			minLength : 0
		}).focus(function() {
			$(this).autocomplete('search', '');
		});
		
		$("#invNombre").keypress(function(e){
			llenarCamposAutoTrab();		});
		$("#invNombre").keyup(function(e){
			llenarCamposAutoTrab();		});
		$("#invNombre").keydown(function(e){
			llenarCamposAutoTrab();		});
		$(document).click( function(){
			llenarCamposAutoTrab();
			});
		$("#datost").html("<select id='selTurno'>" 
		+"<option value=1>Mañana</option>" 
		+"<option value=2>Tarde</option>"
		+"<option value=3>Noche</option>"
		+"<option value=4>Madrugada</option>"
				+"</select>");
	

	}
	
	$("#tblInvo tbody tr input").addClass("form-control");
}


function cargarModalInvo() {
	$("#datosLaborales").hide();
	$("#datosMedicos").hide();
	
	$("#btnAgregarInvo").attr("onclick", "javascript:nuevoInvo();");
	$("#btnCancelarInvo").attr("onclick", "javascript:cancelarInvo();");
	
	
	$("#btnCancelarInvo").hide();
	$("#btnAgregarInvo").show();
	$("#btnEliminarInvo").hide();
	$("#btnGuardarInvo").hide();

	$("#epNombre").html(sessionStorage.getItem("gestopcompanyname"));
	$("#epRuc").html(sessionStorage.getItem("gestopcompanyruc"));
	
	 banderaedicion10=false;
	 involucradoId=0;
	 nombre="";
	 sexo="";
	 edad=0;
	 area="";
	 puesto="";
	 tiempoPuesto=0;
	 parteCuerpoId=0

	

	var dataParam = {
			accidenteId : accidenteId
	};
	 $("#tblInvo tbody tr").remove();

	callAjaxPost(URL + '/accidente/involucrado', dataParam, function(data2) {
		switch (data2.CODE_RESPONSE) {
		case "05":
			var list = data2.list;

			for (index = 0; index < list.length; index++) {

				$("#tblInvo tbody").append(
						"<tr id='tr" + list[index].involucradoId
								+ "' onclick='javascript:editarInvo("
								+ list[index].involucradoId + ",&quot;"
								+ list[index].nombre + "&quot;,"
								+ list[index].dni + ",&quot;"
								+ list[index].sexo + "&quot;,"
								+ list[index].edad + ",&quot;"
								+ list[index].area + "&quot;,&quot;"
								+ list[index].puesto + "&quot;,&quot;"
								+ list[index].tiempoPuesto + "&quot;,"
								+ list[index].parteCuerpoId + ","
								+list[index].tipoLesion.id +")' >"
								+ "<td id='tdtipo'>"
								+ "Externo</td>"
								+ "<td id='tddni"
								+ list[index].involucradoId + "'>"
								+ list[index].dni + "</td>"
								+ "<td id='tdnombre"
								+ list[index].involucradoId + "'>"
								+ list[index].nombre + "</td>"
								+ "<td id='tdturno"
								+ list[index].involucradoId + "'>"
								+ "...</td>"
								+ "<td id='tdhoras"
								+ list[index].involucradoId + "'>"
								+ "...</td>"
								+ "<td id='tdlesion"
								+ list[index].involucradoId + "'>"
								+ list[index].tipoLesion.nombre+"</td>"
								+ "<td id='tdparte"
								+ list[index].involucradoId + "'>"
								+ list[index].parteCuerpoNombre + "</td>"
								+"<td id='datoLaboral"+list[index].involucradoId+"'>...</td>"
								+"<td id='descansoMedico"+list[index].involucradoId+"'>...</td>"
								+ "</tr>");
			
			}
			
		
			break;
		default:
			alert("Ocurrió un error al traer las programaciones!");
		}
	});
	
	callAjaxPost(URL + '/accidente/trabacc', dataParam, function(data) {
		switch (data.CODE_RESPONSE) {
		case "05":
			var list = data.list;
			listTrabAccFull=list;

			for (var index = 0; index < list.length; index++) {

				$("#tblInvo tbody").append(
						"<tr id='tr" + list[index].trabAccidId
								+ "' onclick='javascript:editarTrabAc("
								+ list[index].trabAccidId + ","
								+ list[index].trabajadoresId + ","
								+ list[index].parteCuerpoId + ",&quot;"
								+ list[index].parteCuerpoNombre + "&quot;,&quot;"
								+ list[index].turno + "&quot;,"
								+ list[index].horasTrabajadas + ","
								+ list[index].diasDescanso + ",&quot;"
								+ list[index].fechaInicioDescanso + "&quot;,"
								+ list[index].dni + ",&quot;" 
								+ list[index].nombre + "&quot;," 
								+ list[index].sexoId + ","
								+ list[index].edad + ",&quot;"
								+ list[index].areaNombre + "&quot;,&quot;"
								+ list[index].puestoNombre +"&quot;,"+index+")' >"
								+ "<td id='tdatipo'>"
								+ "Interno</td>"
								+ "<td id='tdadni"
								+ list[index].trabAccidId + "'>"
								+ list[index].dni + "</td>"
								+ "<td id='tdanombre"
								+ list[index].trabAccidId + "'>"
								+ list[index].nombre + "</td>"
								+ "<td id='tdaturno"
								+ list[index].trabAccidId + "'>"
								+ obtenerNombreTurno(parseInt(list[index].turno ))+ "</td>"
								+ "<td id='tdahoras"
								+ list[index].trabAccidId + "'>"
								+ list[index].horasTrabajadas +"</td>"
								+ "<td id='tdalesiona"
								+ list[index].trabAccidId + "'>"
								+ list[index].tipoLesion.nombre+"</td>"
								+ "<td id='tdaparte"
								+ list[index].trabAccidId + "'>"
								+ list[index].parteCuerpoNombre + "</td>"
								+"<td id='adatoLaboral"+list[index].trabAccidId+"'>...</td>"
								+"<td id='adescansoMedico"+list[index].trabAccidId+"'>...</td>"
								+ "</tr>");
			
			}
			
			 mostrarCat();
			break;
		default:
			alert("Ocurrió un error al traer las programaciones!");
		}
	});
}

function cancelarInvo(){
	cargarModalInvo();
}

function nuevoInvo(){
	var selParteCuerpo = crearSelectOneMenu("selParteCuerpo", "", listPartesCuerpo,
			"-1", "parteCuerpoId", "parteCuerpoNombre");
	 var selTipoLesion = crearSelectOneMenu("selTipoLesion", "", listTipoLesion,
			 "-1", "id", "nombre");
	if (!banderaedicion10) {
		var textExterno="",textInterno="";
		if($("#accRpta1").prop("checked")){
			 textExterno="<option value='2'>Externo</option>";
		}else{
			 textExterno=" ";
		}

		if($("#accRpta2").prop("checked")){
			 textInterno="<option value='1'>Interno</option>";
		}else{
			 textInterno="";
		}
		
		$("#tblInvo tbody")
				.append(
						"<tr id='0'>"
						+"<th><select id='listTipoTrab' onclick='modificarAcordeTipoInvo()'>"
						+textInterno
						+textExterno
						+"</select></th>"
						+"<th id='thdni'><input type='text' style='width:80px' id='invDni'></th>"
						+"<th><input type='text' style='width:100px' id='invNombre'></th>"
						+"<th  id='datost'>"+"<select id='selTurno'>" 
						+"<option value=1>Mañana</option>" 
						+"<option value=2>Tarde</option>"
						+"<option value=3>Noche</option>"
						+"<option value=4>Madrugada</option>"
								+"</select>"+"</th>"
						+"<th  id='datosh'><input type='text'  style='width:60px' id='invHoras'></th>"
						+"<th >"+selTipoLesion+"</th>"
						+"<th >"+selParteCuerpo+"</th>"
						+"<th><a onclick='verDatosLaborales()'>Datos Laborales</a></th>"
						+"<th id='datos2'><a onclick='verDescanso()'>Descanso</a></th>"
						+ "</tr>");
		
		involucradoId = 0;
		trabAccidId=0;
		modificarAcordeTipoInvo();
		$("#btnCancelarInvo").show();
		$("#btnAgregarInvo").hide();
		$("#btnEliminarInvo").hide();
		$("#btnGuardarInvo").show();
		
		banderaedicion10 = true;
	} else {
		alert("Guarde primero.");
	};



}

function eliminarInvo(){
	var r = confirm("¿Está seguro de eliminar el involucrado?");
	if (r == true) {
		var dataParam = {
				involucradoId : involucradoId,
		};

		callAjaxPost(URL + '/accidente/involucrado/delete', dataParam,
				function(data) {
					switch (data.CODE_RESPONSE) {
					case "05":
						cargarModalInvo();
						break;
					default:
						alert("Ocurrió un error al eliminar la programacion!");
					}
				});
	}
}



function editarInvo(pinvolucradoId, pnombre,pdni, psexo,pedad,
	    	parea,ppuesto,ptiempoPuesto,pparteCuerpoId,ptipolesion) {
	$("#btnGuardarInvo").attr("onclick", "javascript:guardarInvo()");
	$("#btnEliminarInvo").attr("onclick", "javascript:eliminarInvo()");
	
	
	if (!banderaedicion10) {
		
		$("#thArea").html('<input type="text" id="invArea">');
		$("#thPuesto").html('<input type="text" id="invPuesto">');
		
		$("#thSexo").html('<select id="slcSexo" class="form-control">'
		+'<option value="1">Masculino</option>'
		+'<option value="2">Femenino</option>'
		+'</select></th>');
		$("#thEdad").html('<input type="number" class="form-control"  style="width:50px" id="invEdad">');
		$("#thAntiguedadPuesto").html('<input type="text" class="form-control"  style="width:100px" id="invAntiguedad">');
		 involucradoId=pinvolucradoId;
		 dni=pdni;
		 nombre=pnombre;
		 sexo=psexo;
		 edad=pedad;
		 area=parea;
		 puesto=ppuesto;
		 tiempoPuesto=ptiempoPuesto;
		 parteCuerpoId=pparteCuerpoId;
		 tipoLesionId=ptipolesion;
		 var selParteCuerpo = crearSelectOneMenu("selParteCuerpo", "", listPartesCuerpo,
					parteCuerpoId, "parteCuerpoId", "parteCuerpoNombre");
		 var selTipoLesion = crearSelectOneMenu("selTipoLesion", "", listTipoLesion,
				 tipoLesionId, "id", "nombre");
$("#tdlesion"+involucradoId).html(selTipoLesion);
		$("#tddni" + involucradoId)
				.html(
						"<input type='text' class='form-control' id='invDni' style='width:70px'>");

		$("#tdnombre" + involucradoId)
		.html(
				"<input type='text' id='invNombre' class='form-control' style='width:80px'>");
		
		$("#tdturno" + involucradoId)
		.html(
				"No es necesario llenar");
		$("#tdhoras" + involucradoId)
		.html(
				"No es necesario llenar");
		$("#tdparte" + involucradoId)
		.html(
				selParteCuerpo);
		
		$("#datoLaboral" + involucradoId)
		.html(
				"<a onclick='verDatosLaborales()'>Datos Laborales</a>");
		
		$("#descansoMedico" + involucradoId)
		.html(
				"No es necesario llenar");
		
		nombrarInput("invDni",dni);
		nombrarInput("invNombre",nombre);
		nombrarInput("invArea",area);
		nombrarInput("invPuesto",puesto);
		nombrarInput("invEdad",edad);
		nombrarInput("invAntiguedad",tiempoPuesto);
		
		
		
		$('#slcSexo > option[value="'+sexo+'"]').attr('selected', 'selected');
	
		
		

		banderaedicion10 = true;
		$("#btnCancelarInvo").show();
		$("#btnAgregarInvo").hide();
		$("#btnEliminarInvo").show();
		
		$("#btnGuardarInvo").show();
		
		
	}
}


function guardarInvo() {

	var campoVacio = true;

	
	var invDni=parseInt($("#invDni").val());
	var invNombre=$("#invNombre").val();
	
	
	var invArea=$("#invArea").val();
	var invPuesto=$("#invPuesto").val();
	var invSexo=$("#slcSexo").val();
	var invEdad=parseInt($("#invEdad").val());
	var invAntiguedad=$("#invAntiguedad").val();
	var parte=$("#selParteCuerpo").val();
	if($("#selParteCuerpo").val()=='-1'){
		parte=41;
	}
	var tipoLesion=$("#selTipoLesion").val();
	if($("#selTipoLesion").val()=='-1'){
		tipoLesion=null;
	}
	
	if (campoVacio) {

		var dataParam = {
			accidenteId: accidenteId,
			involucradoId : involucradoId,	
			 dni:invDni,
			 nombre:invNombre,
			 edad:invEdad,
			 sexo:invSexo,
			 area:invArea,
			 puesto:invPuesto,
			 tiempoPuesto:invAntiguedad,
			 parteCuerpoId:parte,
			 tipoLesion:{id:tipoLesion}
			
		};

		callAjaxPost(URL + '/accidente/involucrado/save', dataParam,
				function(data) {
					switch (data.CODE_RESPONSE) {
					case "05":
						cargarModalInvo();
						break;
					default:
						alert("Ocurrió un error al guardar el involucrado!");
					}
				});
	} else {
		alert("Debe ingresar todos los campos.");
	}
}

function obtenerNombreTurno(turnoId){
	var nombreTurno="";
	switch(turnoId){
	case 1:
		nombreTurno="Mañana";
		break;
	case 2:
		nombreTurno="Tarde";
		break;
	case 3:
		nombreTurno="Noche";
		break;
	case 4:
		nombreTurno="Madrugada   ";
		break;
	}
	
	return nombreTurno;
}

function editarTrabAc(
		 ptrabAccidId  ,
		 ptrabajadoresId  ,
		 pparteCuerpoId  ,
		 pparteCuerpoNombre  ,
		 pturno  ,
		 phorasTrabajadas  ,
		 pdiasDescanso  ,
		 pfechaInicioDescanso,
		 pdni  , 
		 pnombre  , 
		 psexoId  ,
		 pedad  ,
		 pareaNombre  ,
		 ppuestoNombre ,pindex) {
	
	
	$("#btnGuardarInvo").attr("onclick", "javascript:guardarTrabAcc()");
	$("#btnEliminarInvo").attr("onclick", "javascript:eliminarTrabAcc()");
	if (!banderaedicion10) {
		trabAccidId= ptrabAccidId  ;
		trabajadorId=ptrabajadoresId  ;
		parteCuerpoId=pparteCuerpoId  ;
		 turno =pturno ;
		 horasTrabajadas=phorasTrabajadas  ;
		 diasDescanso=pdiasDescanso  ;
		 fechaInicioDescanso=pfechaInicioDescanso;
		 tipoLesionId=listTrabAccFull[pindex].tipoLesion.id;
		 var fechaInicioPuesto=listTrabAccFull[pindex].fechaInicioPuesto;
		 tiempoPuesto
		 var antiguedadPuesto= listTrabAccFull[pindex].tiempoPuesto;
		var selectTurno="<select id='selTurno' class='form-control'>" 
			+"<option value=1>Mañana</option>" 
			+"<option value=2>Tarde</option>"
			+"<option value=3>Noche</option>"
			+"<option value=4>Madrugada</option>"
					"</select>";
		 var selParteCuerpo = crearSelectOneMenu("selParteCuerpo", "", listPartesCuerpo,
				 parteCuerpoId, "parteCuerpoId", "parteCuerpoNombre");
		 var selTipoLesion = crearSelectOneMenu("selTipoLesion", "", listTipoLesion,
				 tipoLesionId, "id", "nombre");
$("#tdalesiona"+trabAccidId).html(selTipoLesion);
		$("#tdadni" + trabAccidId)
				.html(
						pdni);

		$("#tdanombre" + trabAccidId)
		.html(
				pnombre);
		
		$("#tdaturno" + trabAccidId)
		.html(
				selectTurno);
		$('#selTurno > option[value="'+turno+'"]').attr('selected', 'selected');
		
		$("#tdahoras" + trabAccidId)
		.html(
				"<input type='text' id='invHoras' class='form-control' style='width:60px'>");
		$("#tdaparte" + trabAccidId)
		.html(
				selParteCuerpo);
		
		$("#adatoLaboral" + trabAccidId)
		.html(
				"<a onclick='verDatosLaborales()'>Datos Laborales</a>");
		
		$("#adescansoMedico" + trabAccidId)
		.html(
				"<a onclick='verDescanso()'>Descanso</a>");
		
		
		$("#invTurno").val(turno);
		$("#invHoras").val(horasTrabajadas);
		
		$("#thArea").html(pareaNombre);
		$("#thPuesto").html(ppuestoNombre);
		var genero;
		if(psexoId==1){
			genero="Masculino"
		}else{
			genero="Femenino"
		}
		
		$("#thSexo").html(genero);
		$("#thEdad").html(pedad);
		$("#thAntiguedadPuesto").html(antiguedadPuesto );
		
		$("#diasDescanso").html(diasDescanso);
		$("#fechaDescanso").val(fechaInicioDescanso)
		
		
		 var fechaAux=fechaInicioDescanso.split('-');
		 
	    var fecha= new Date(fechaAux[0], fechaAux[1], fechaAux[2]);
	   
	 
	    //Obtenemos los milisegundos desde media noche del 1/1/1970
	    var tiempo=fecha.getTime();
	    //Calculamos los milisegundos sobre la fecha que hay que sumar o restar...
	   var  milisegundos=parseInt(diasDescanso*24*60*60*1000);
	    //Modificamos la fecha actual
	    var total=fecha.setTime(tiempo+milisegundos);
	    var day=fecha.getUTCDate();
	    var month=fecha.getUTCMonth();
	    
	    if(parseInt(month)<10){
	    	month="0"+month;
	    }
	   
	    var year=fecha.getUTCFullYear();
	    
	    if(parseInt(day)<10){
	    	day="0"+day;
	    }
	    var fechaModificada=year+"-"+month+"-"+day;

	
	    $("#fechaFinDescanso").val(fechaModificada);
		
		banderaedicion10 = true;
		$("#btnCancelarInvo").show();
		$("#btnAgregarInvo").hide();
		$("#btnEliminarInvo").show();
		
		$("#btnGuardarInvo").show();
		
		
	}
}


function guardarTrabAcc() {

	var campoVacio = true;

	
	
	 turno=$("#selTurno").val();;
	 horasTrabajadas=$("#invHoras").val();
	
	 fechaInicioDescanso=$("#fechaDescanso").val();
	 
	 diasDescanso=restaFechas(fechaInicioDescanso,$("#fechaFinDescanso").val());
	if(fechaInicioDescanso==""){
		fechaInicioDescanso=null;
	}
	
	var parteCuerpoId=$("#selParteCuerpo").val();
	 var tipoLesion=$("#selTipoLesion").val();
	if (campoVacio) {

		var dataParam = {
			accidenteId: accidenteId,
			trabAccidId: trabAccidId  ,
			trabajadoresId:trabajadorId  ,
			parteCuerpoId:parteCuerpoId  ,
			 turno :turno   ,
			 horasTrabajadas:horasTrabajadas  ,
			 diasDescanso:diasDescanso  ,
			 fechaInicioDescanso:fechaInicioDescanso,
			 tipoLesion:{id:tipoLesion}
			
		};

		callAjaxPost(URL + '/accidente/trabacc/save', dataParam,
				function(data) {
					switch (data.CODE_RESPONSE) {
					case "05":
						cargarModalInvo();
						break;
					default:
						alert("Ocurrió un error al guardar el involucrado!");
					}
				});
	} else {
		alert("Debe ingresar todos los campos.");
	}
}

function eliminarTrabAcc(){
	var r = confirm("¿Está seguro de eliminar el involucrado?");
	if (r == true) {
		var dataParam = {
			trabAccidId : trabAccidId,
		};

		callAjaxPost(URL + '/accidente/trabacc/delete', dataParam,
				function(data) {
					switch (data.CODE_RESPONSE) {
					case "05":
						cargarModalInvo();
						break;
					default:
						alert("Ocurrió un error al eliminar la programacion!");
					}
				});
	}
}



function verDatosLaborales(){
	
	$("#datosLaborales").toggle();
}

function verDescanso(){
	
	$("#datosMedicos").toggle();
	
}


