var accionMejoraId;
var banderaEdicionCorr;
var listanombretab;
var listDniNombre;
var accionEstado;
var accionEstadoNombre;var listFullAcciones;
function cargarModalAccionMejora() {
	banderaEdicionCorr = false;
	$("#btnCancelarCorr").hide();
	$("#btnNuevaCorr").show();
	$("#btnGuardarCorr").hide();
	$("#btnEliminarCorr").hide();

	$("#btnNuevaCorr").attr("onclick", "javascript:nuevaAccionMejora();");
	$("#btnCancelarCorr").attr("onclick", "javascript:cancelarAccionMejora();");
	$("#btnGuardarCorr").attr("onclick", "javascript:guardarAccionMejora();");
	$("#btnEliminarCorr").attr("onclick", "javascript:eliminarAccionMejora();");

	var dataParam = {
		accionMejoraTipoId : accionMejoraTipoIdActual,
		gestionAccionMejoraId : gestionAccionMejoraId
	};

	callAjaxPost(URL + '/gestionaccionmejora/accionmejora', dataParam,
			procesarDataListadoAccionMejora);
}

function procesarDataListadoAccionMejora(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		var list = data.list;
listFullAcciones=list;
		$("#tblCorr tbody tr").remove();

		var nro = 0;
		for (var index = 0; index < list.length; index++) {
			nro++;
			
			$("#tblCorr tbody").append(
					"<tr id='tr" + list[index].accionMejoraId
							+ "' onclick='javascript:editarAccionMejora("
							+index+")' >"

						//	+ "<td id='tdtipoa" + list[index].accionMejoraId+ "'>" + list[index].tipoAccionNombre + "</td>"
							
							+ "<td id='tddesc" + list[index].accionMejoraId
							+ "'>" + list[index].descripcion + "</td>"

							+ "<td id='tdrespnom" + list[index].accionMejoraId
							+ "'>" + list[index].responsableNombre + "</td>"

							+ "<td id='tdrespmail" + list[index].accionMejoraId
							+ "'>" + list[index].responsableMail + "</td>"

							+ "<td id='tdfec" + list[index].accionMejoraId
							+ "'>" + list[index].fechaRevisionTexto + "</td>"
							+ "<td id='tdhorapla" + list[index].accionMejoraId
							+ "'>" + list[index].horaPlanificadaTexto + "</td>"
							
							+ "<td id='tdfecreal" + list[index].accionMejoraId
							+ "'>" + list[index].fechaRealTexto + "</td>"
							+ "<td id='tdinversion" + list[index].accionMejoraId
							+ "'>" + list[index].inversionAccion + "</td>"
							+ "<td id='tdpel" + list[index].accionMejoraId
							+ "'>" + list[index].peligrosAsociados + "</td>"
							+ "<td id='tdcontrol" + list[index].accionMejoraId
							+ "'>" + list[index].controlesRealizados+" / "+list[index].controlesPlanificados + "</td>"
							+ "<td id='estadoAccion" + list[index].accionMejoraId
							+ "'>" + list[index].estadoCumplimientoNombre
							+ "</td>"

							+ "<td id='tdevi" + list[index].accionMejoraId
							+ "'>"+ list[index].evidenciaNombre+"</td>"

							+ "</tr>");
			
			var estadoAux=parseInt( list[index].estadoId );
			
		}
	
		
		var dataParam2 = {
					idCompany: sessionStorage.getItem("gestopcompanyid")
			};
		callAjaxPost(URL + '/accidente/trab', dataParam2,
				function(data){
			switch (data.CODE_RESPONSE) {
			
			case "05":
			
				 listDniNombre=data.listDniNombre;
				 listanombretab=listarStringsDesdeArray(listDniNombre, "nombre");
					if(getSession("linkCalendarioSubAccMejoraId")!=null){
						
						listFullAcciones.every(function(val,index3){
							
							if(val.accionMejoraId==parseInt(getSession("linkCalendarioSubAccMejoraId"))){
								editarAccionMejora(index3);		
									verControlesAsociados();
									
								
								sessionStorage.removeItem("linkCalendarioSubAccMejoraId");
								return false;
							}else{
								return true;
							}
						});
						
					}
			
				
				break;
			default:
				alert("Ocurrió un error al traer los Accidentes!");
			}

		
		});
		break;
	default:
		alert("Ocurrió un error al traer las correcciones inmediatas!");
	}
}
var name2;
function editarAccionMejora( pindex) {

var 	pfechaReal=listFullAcciones[pindex].fechaReal;
	var 	pestadoCumpId=listFullAcciones[pindex].estadoId;
	var 	ptipoAccionId=listFullAcciones[pindex].tipoAccionId;
	var 	pinversion=listFullAcciones[pindex].inversionAccion;
	
	if (!banderaEdicionCorr) {
		accionMejoraId = listFullAcciones[pindex].accionMejoraId ;
		var inversionAcc=parseFloat($("#tdinversion"+accionMejoraId).text());
		var peligroRelacionado=$("#tdpel"+accionMejoraId).text();
		var controlRelacionado=$("#tdcontrol"+accionMejoraId).text();
		var horaPlanificada=listFullAcciones[pindex].horaPlanificada;
		$("#tdpel"+accionMejoraId).html(
				"<a onclick='verPeligrosAsoiados()'>"+peligroRelacionado+"</a>");
		$("#tdcontrol"+accionMejoraId).html(
				"<a onclick='verControlesAsociados()'>"+controlRelacionado+"</a>");
		$("#tdinversion"+accionMejoraId).html(
				"<label for='inputInversion'>S/</label><input type='number' id='inputInversion' name='inputInversion'" +
				"class='form-control' value='"+inversionAcc+"'>");
		var name = $("#tddesc" + accionMejoraId).text();
		$("#tddesc" + accionMejoraId)
				.html(
						"<textarea type='text' id='inputDesc' class='form-control' placeholder='Resumen' rows='2'>"
								+ name + "</textarea>");

		var name1 = $("#tdrespnom" + accionMejoraId).text();
		$("#tdrespnom" + accionMejoraId)
				.html(
						"<input type='text' id='inputRespNom' class='form-control' value='"
								+ name1 + "'>");
		$("#inputRespNom").on("change",function(){
			var nombreTrab=$("#inputRespNom").val();
			
			var correo;
			
				for (index = 0; index < listDniNombre.length; index++) {
					
					if(listDniNombre[index]["nombre"]==nombreTrab){
						correo=listDniNombre[index]["correo"]
					$("#inputRespMail").val(correo);
					
					break;
					}else{
						$("#inputRespMail").val("");
					
					
					}
				}
			
		})
		$("#tdhorapla" + accionMejoraId)
				.html(
						"<input type='time' id='inputHora' class='form-control' value='"
								+ horaPlanificada + "'>");
		
		 name2 = $("#tdrespmail" + accionMejoraId).text();
		$("#tdrespmail" + accionMejoraId)
				.html(
						"<input type='text' id='inputRespMail' class='form-control' value='"
								+ name2 + "'>");
 var nombreEvidencia=$("#tdevi" + accionMejoraId)
	.text();
		$("#tdevi" + accionMejoraId)
				.html(
						"<a href='"
									+ URL
									+ "/gestionaccionmejora/accionmejora/evidencia?accionMejoraId="
									+ accionMejoraId
					
									+ "' "
									+ "target='_blank' id='descargarimagen"
									+ accionMejoraId
									+ "'>" +
									nombreEvidencia+"</a>"
									+ "<br/><a href='#' onclick='javascript:cargarEvidencia();'>Subir</a>");
		/* ************************************************************************** */

		var selEstado = crearSelectOneMenu("selEstadoCorr", "", listEstado,
				pestadoCumpId, "estadoCumpId", "estadoCumplientoNombre");
		
		if(accionMejoraTipoIdActual != 1){
			var selTipoAcc = crearSelectOneMenu("selTipoAcc", "", listTipoAccionMejoraDepurado, ptipoAccionId,
					"tipoAccionId", "tipoAccionNombre");
			$("#tdtipoa" + accionMejoraId).html(selTipoAcc);			
		}
		
		$("#tdfec" + accionMejoraId).html(
				"<input type='date' id='inputFec' class='form-control' onchange='hallarEstadoImplementacion("+accionMejoraId+")'>");
		var myDate = new Date(listFullAcciones[pindex].fechaRevision );
		var day = ("0" + myDate.getUTCDate()).slice(-2);
		var month = ("0" + (myDate.getUTCMonth() + 1)).slice(-2);
		var today = myDate.getUTCFullYear() + "-" + (month) + "-" + (day);
		$("#inputFec").val(today);
		
		
		$("#tdfecreal" + accionMejoraId).html(
				"<input type='date' id='inputFecReal' class='form-control' onchange='hallarEstadoImplementacion("+accionMejoraId+")'>");
		
		if(pfechaReal!=null){
			var myDate = new Date(pfechaReal);
			var day = ("0" + myDate.getUTCDate()).slice(-2);
			var month = ("0" + (myDate.getUTCMonth() + 1)).slice(-2);
			var today = myDate.getUTCFullYear() + "-" + (month) + "-" + (day);
			$("#inputFecReal").val(today);
		}
		
		if(listFullAcciones[pindex].fechaRevision !=null){
			var myDate = new Date(listFullAcciones[pindex].fechaRevision );
			var day = ("0" + myDate.getUTCDate()).slice(-2);
			var month = ("0" + (myDate.getUTCMonth() + 1)).slice(-2);
			var today = myDate.getUTCFullYear() + "-" + (month) + "-" + (day);
			$("#inputFec").val(today);
		}
		
		 hallarEstadoImplementacion(accionMejoraId);
		banderaEdicionCorr = true;
		ponerListaSugerida("inputRespNom",listanombretab,true);
		$("#inputRespNom").keyup(function(e){
			var nombreTrab=$("#inputRespNom").val();
			
			var correo;
			
				for (index = 0; index < listDniNombre.length; index++) {
					
					if(listDniNombre[index]["nombre"]==nombreTrab){
						correo=listDniNombre[index]["correo"]
					$("#inputRespMail").val(correo);
					
					break;
					}else{
						
					
					
					}
				}});

		$(document).click( function(){
			var nombreTrab=$("#inputRespNom").val();
			
			var correo;
				for (index = 0; index < listDniNombre.length; index++) {
					
					if(listDniNombre[index]["nombre"]==nombreTrab){
						correo=listDniNombre[index]["correo"]
					$("#inputRespMail").val(correo);
					
					break;
					}else{
						
					
					
					}
				}
			});
		
		$("#btnCancelarCorr").show();
		$("#btnNuevaCorr").hide();
		$("#btnGuardarCorr").show();
		$("#btnEliminarCorr").show();
	}
}

function nuevaAccionMejora() {
	if (!banderaEdicionCorr) {
		var selEstado = crearSelectOneMenu("selEstadoCorr", "", listEstado,
				"-1", "estadoCumpId", "estadoCumplientoNombre");
		var selTipoAcc = "Inmediata";
		if(accionMejoraTipoIdActual != 1){
			selTipoAcc = crearSelectOneMenu("selTipoAcc", "", listTipoAccionMejoraDepurado, accionMejoraTipoIdActual,
					"tipoAccionId", "tipoAccionNombre");	
		}
		accionMejoraId = 0;
		$("#tblCorr tbody")
				.append(
						"<tr id='0'>"
							//	+ "<td>" + selTipoAcc + "</td>"
								+ "<td><textarea type='text' id='inputDesc' class='form-control' placeholder='Resumen' rows='2'/>"
								+ "</td>"
								+ "<td><input type='text' id='inputRespNom' class='form-control' placeholder='Nombre'></td>"
								+ "<td><input type='text' id='inputRespMail' class='form-control' placeholder='Correo'></td>"
								
								
								
								+ "<td><input type='date' id='inputFec' " +
										"class='form-control' " +
										"onchange='hallarEstadoImplementacion("+accionMejoraId+")'></td>"
										+ "<td><input type='time' value='12:00:00' id='inputHora' " +
										"class='form-control' " 
										+"></td>"
								+ "<td><input type='date' id='inputFecReal' " +
										"class='form-control' onchange='hallarEstadoImplementacion("+accionMejoraId+")'></td>"
								
										+"<td>"
										+"<label for='inputInversion'>S/</label><input class='form-control' type='number' id='inputInversion' name='inputInversion'" +
										+"class='form-control'>"
										+"</td>" +
												"<td>...</td><td>...</td>"
										+"<td id='estadoAccion0'></td><td>...</td>"
								+ "</tr>");
		
		$("#inputRespNom").keyup(function(e){
			var nombreTrab=$("#inputRespNom").val();
			
			var correo;
			
				for (index = 0; index < listDniNombre.length; index++) {
					
					if(listDniNombre[index]["nombre"]==nombreTrab){
						correo=listDniNombre[index]["correo"]
					$("#inputRespMail").val(correo);
					
					break;
					}else{
						
					
					
					}
				}});
		$("#btnCancelarCorr").show();
		$("#btnNuevaCorr").hide();
		$("#btnGuardarCorr").show();
		$("#btnEliminarCorr").hide();
		banderaEdicionCorr = true;
		ponerListaSugerida("inputRespNom",listanombretab,true);
	} else {
		alert("Guarde primero.");
	}
}

function cancelarAccionMejora() {
	cargarModalAccionMejora();
}

function eliminarAccionMejora() {
	var r = confirm("¿Está seguro de eliminar la Correccion inmediata?");
	if (r == true) {
		var dataParam = {
				accionMejoraId : accionMejoraId
		};

		callAjaxPost(URL + '/gestionaccionmejora/accionmejora/delete',
				dataParam, procesarResultadoEliminarAccionMejora);
	}
}

function procesarResultadoEliminarAccionMejora(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		cargarModalAccionMejora();
		break;
	default:
		alert("Ocurrió un error al eliminar la Correccion inmediata!");
	}
}

function guardarAccionMejora() {

	var campoVacio = true;
var inversionAcc=$("#inputInversion").val();
	var selTipoAcc = $("#selTipoAcc option:selected").val();
	var inputDesc = $("#inputDesc").val();
	var inputRespNom = $("#inputRespNom").val();
	var inputRespMail = $("#inputRespMail").val();
var horaPlanificada=$("#inputHora").val();
if(!$("#inputHora").val()){
	horaPlanificada=null;
}
	var mes2 = $("#inputFec").val().substring(5, 7) - 1;
	var fechatemp2 = new Date($("#inputFec").val().substring(0, 4), mes2, $(
			"#inputFec").val().substring(8, 10));

	var inputFecha = fechatemp2;
	
	
	
	var mes2 = $("#inputFecReal").val().substring(5, 7) - 1;
	var fechatemp3 = new Date($("#inputFecReal").val().substring(0, 4), mes2, $(
			"#inputFecReal").val().substring(8, 10));

	var inputFechaReal = fechatemp3;
	
	
	if(!$("#inputFecReal").val()){
		inputFechaReal=null;
	}
	if(!$("#inputFec").val()){
		inputFecha=null;
	}
	
	if(accionMejoraTipoIdActual == 1){
		selTipoAcc = accionMejoraTipoIdActual;
	}else{
		 
		switch(solicitudAccionId){
		case 1:
			accionMejoraTipoIdActual=2;
			break;
		case 2:
			accionMejoraTipoIdActual=3;
			break;
		case 3:
			accionMejoraTipoIdActual=4;
			break;
		}
	}

	if (   inputDesc.length == 0 || inputRespNom.length == 0
			|| inputRespMail == 0) {
		campoVacio = false;
	}
	hallarEstadoImplementacion(accionMejoraId);

	if (campoVacio) {

		var dataParam = {
				accionMejoraId : accionMejoraId,
			descripcion : inputDesc,
			responsableNombre : inputRespNom,
			responsableMail : inputRespMail,
			fechaRevision : inputFecha,horaPlanificada:horaPlanificada,
			fechaReal:inputFechaReal,
			accionMejoraTipoId : accionMejoraTipoIdActual,
			estadoId : accionEstado,
			inversionAccion:inversionAcc,
			companyId : sessionStorage.getItem("gestopcompanyid"),
			gestionAccionMejoraId : gestionAccionMejoraId
		};
		 
		callAjaxPost(URL + '/gestionaccionmejora/accionmejora/save', dataParam,
				procesarResultadoGuardarAccionMejora);
	} else {
		alert("Debe ingresar todos los campos.");
	}
}

function procesarResultadoGuardarAccionMejora(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		if(accionMejoraTipoIdActual!=1){
			accionMejoraTipoIdActual=-1;
		}
		
		cargarModalAccionMejora();
		break;
	default:
		alert("Ocurrió un error al guardar la accion Mejora!");
	}
}

function cargarEvidencia() {
	$("#modalAcc").hide();
	$("#modalAccUpload").show();
}

function cancelarAccUploadEvidencia() {
	$("#modalAcc").show();
	$("#modalAccUpload").hide();
}

function uploadAccEvidencia() {
	var inputFileImage = document.getElementById("fileAccEvi");
	var file = inputFileImage.files[0];
	var data = new FormData();

	if (file.size > bitsEvidenciaAccionMejora) {
		alert("Lo sentimos, solo se pueden subir archivos menores a "+bitsEvidenciaAccionMejora/1000000+" MB");
	} else {
		data.append("fileAccEvi", file);
		data.append("accionMejoraId", accionMejoraId);
	
		var url = URL + '/gestionaccionmejora/accionmejora/evidencia/save';
		$.blockUI({
			message : 'cargando...'
		});
		$
				.ajax({
					url : url,
					xhrFields: {
			            withCredentials: true
			        },
					type : 'POST',
					contentType : false,
					data : data,
					processData : false,
					cache : false,
					success : function(data, textStatus, jqXHR) {
						switch (data.CODE_RESPONSE) {
						case "06":
							sessionStorage.clear();
							document.location.replace(data.PATH);
							break;
						default:
							console.log('Se subio el archivo correctamente.');
							$("#modalAcc").show();
							$("#modalAccUpload").hide();
						}
						$.unblockUI();
					},
					error : function(jqXHR, textStatus, errorThrown) {
						alert("Ups, tenemos problemas tecnicos. Intente más tarde. Error: "
								+ errorThrown);
						console.log('xhRequest: ' + jqXHR + "\n");
						console.log('ErrorText: ' + textStatus + "\n");
						console.log('thrownError: ' + errorThrown + "\n");
						$.unblockUI();
					}
				});
	}

}

function llenarCorreoAutoTrab(){
	

	
	
	
}
function hallarEstadoImplementacion(accionId){
	var fechaPlanificada=$("#inputFec").val();
	var fechaReal=$("#inputFecReal").val();
	var fechaHoy=obtenerFechaActual();
	if(fechaReal!=''){
		accionEstado=2;
		accionEstadoNombre="COMPLETADO";
	}else{
		
		if(fechaPlanificada!=""){
			var dif=restaFechas(fechaHoy,fechaPlanificada)
			if(dif<0){
				accionEstado=3;
				accionEstadoNombre="RETRASADO";
			}else{
				accionEstado=1;
				accionEstadoNombre="EN PROCESO";
			}
			
		}else{
			
			accionEstado=1;
			accionEstadoNombre="EN PROCESO";
			
		}
	}
	
	$("#estadoAccion"+accionId).html(accionEstadoNombre);
	
}


function comprobarEstadoGestion(estadoAccion){
	


}
