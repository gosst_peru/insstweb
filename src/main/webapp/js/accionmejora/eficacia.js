var eficaciaId;
var banderaEdicionEfic;

function cargarModalEficacia() {
	banderaEdicionEfic = false;
	$("#btnCancelarEfic").hide();
	$("#btnNuevaEfic").show();
	$("#btnGuardarEfic").hide();
	$("#btnEliminarEfic").hide();

	$("#btnNuevaEfic").attr("onclick", "javascript:nuevaEficacia();");
	$("#btnCancelarEfic").attr("onclick", "javascript:cancelarEficacia();");
	$("#btnGuardarEfic").attr("onclick", "javascript:guardarEficacia();");
	$("#btnEliminarEfic").attr("onclick", "javascript:eliminarEficacia();");

	var dataParam = {
		gestionAccionMejoraId : gestionAccionMejoraId
	};

	callAjaxPost(URL + '/gestionaccionmejora/eficacia', dataParam,
			procesarDataListadoEficacia);
}

function procesarDataListadoEficacia(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		var list = data.list;

		$("#tblEfic tbody tr").remove();

		var nro = 0;
		for (index = 0; index < list.length; index++) {
			nro++;
			$("#tblEfic tbody").append(
					"<tr id='tr" + list[index].eficaciaId
							+ "' onclick='javascript:editarEficacia("
							+ list[index].eficaciaId + ")' >"

							+ "<td id='tdeficpor" + list[index].eficaciaId
							+ "'>" + list[index].porcentaje + "</td>"

							+ "<td id='tdeficmot" + list[index].eficaciaId
							+ "'>" + list[index].motivo + "</td>"

							+ "<td id='tdeficresnom" + list[index].eficaciaId
							+ "'>" + list[index].responsableNombre + "</td>"

							+ "<td id='tdeficresmail" + list[index].eficaciaId
							+ "'>" + list[index].responsableMail
							+ "</td>"
							
							+ "<td id='tdeficevi" + list[index].eficaciaId
							+ "'>...</td>"

							+ "</tr>");
		}

		break;
	default:
		alert("Ocurrió un error al traer las eficacias!");
	}
}

function editarEficacia(peficaciaId) {
	if (!banderaEdicionEfic) {
		eficaciaId = peficaciaId;

		var name = $("#tdeficpor" + eficaciaId).text();
		$("#tdeficpor" + eficaciaId)
		.html(
				"<input type='text' id='inputEficPorc' class='form-control' placeholder='0' value='"
						+ name + "'>");

		var name1 = $("#tdeficmot" + eficaciaId).text();
		$("#tdeficmot" + eficaciaId)
				.html(
						"<input type='text' id='inputEficMot' class='form-control' placeholder='Motivo' value='"
								+ name1 + "'>");

		var name2 = $("#tdcauresmail" + eficaciaId).text();
		$("#tdcauresmail" + eficaciaId)
				.html(
						"<input type='text' id='inputCausaResMail' class='form-control' placeholder='Correo' value='"
								+ name2 + "'>");

		$("#tdcauevi" + eficaciaId)
				.html(
						"<a href='#' onclick='javascript:cargarEvidencia();'>Agregar</a>");

		banderaEdicionEfic = true;
		$("#btnCancelarEfic").show();
		$("#btnNuevaEfic").hide();
		$("#btnGuardarEfic").show();
		$("#btnEliminarEfic").show();
	}
}

function nuevaeficacia() {
	if (!banderaEdicionEfic) {
		$("#tblEfic tbody")
				.append(
						"<tr id='0'>"
								+ "<td><input type='text' id='inputCausaDes' class='form-control' placeholder='Nombre'></td>"
								+ "<td>...</td>"
								+ "<td><input type='text' id='inputCausaResNom' class='form-control' placeholder='Nombre'></td>"
								+ "<td><input type='text' id='inputCausaResMail' class='form-control' placeholder='Correo'></td>"
								+ "</tr>");
		eficaciaId = 0;
		$("#btnCancelarEfic").show();
		$("#btnNuevaEfic").hide();
		$("#btnGuardarEfic").show();
		$("#btnEliminarEfic").hide();
		banderaEdicionEfic = true;
	} else {
		alert("Guarde primero.");
	}
}

function cancelareficacia() {
	cargarModaleficacia();
}

function eliminareficacia() {
	var r = confirm("¿Está seguro de eliminar el Analisis?");
	if (r == true) {
		var dataParam = {
			eficaciaId : eficaciaId
		};

		callAjaxPost(URL + '/gestionaccionmejora/eficacia/delete',
				dataParam, procesarResultadoEliminareficacia);
	}
}

function procesarResultadoEliminareficacia(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		cargarModaleficacia();
		break;
	default:
		alert("Ocurrió un error al eliminar el Analisis!");
	}
}

function guardareficacia() {

	var campoVacio = true;
	var inputDesc = $("#inputCausaDes").val();
	var inputRespNom = $("#inputCausaResNom").val();
	var inputRespMail = $("#inputCausaResMail").val();


	if (inputDesc.length == 0 || inputRespNom.length == 0
			|| inputRespMail == 0) {
		campoVacio = false;
	}

	if (campoVacio) {

		var dataParam = {
				eficaciaId : eficaciaId,
			descripcion : inputDesc,
			responsableNombre: inputRespNom,
			responsableMail : inputRespMail,
			gestionAccionMejoraId : gestionAccionMejoraId
		};

		callAjaxPost(URL + '/gestionaccionmejora/eficacia/save', dataParam,
				procesarResultadoGuardareficacia);
	} else {
		alert("Debe ingresar todos los campos.");
	}
}

function procesarResultadoGuardareficacia(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		cargarModaleficacia();
		break;
	default:
		alert("Ocurrió un error al guardar el analisis!");
	}
}