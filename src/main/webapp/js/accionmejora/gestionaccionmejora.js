var listCausa = [];
var listEstado = [];
var listSolicitud=[];
var listRiesgoPotencial = [];
var listTipoAccionMejora = [];
var listTipoAccionMejoraDepurado = [];
var gestionAccionMejoraId;
var causaId;
var solicitudAccionId;
var estadoCumpId;
var accionMejoraTipoIdActual;
var listTabla;
var listTablaAux;
var banderaEdicion;
var hayProceso;
var hayCompleto;
var hayRetrasado;
var huboCambiosAcciones;
var propiedad1;
var propiedad2;
var numPaginaTotal;
var numPaginaActual;
var listDniNombre;
var listanombretab;

var arrayFiltroCausa=[];
var arrayFiltroSolicitud=[];
var toggleFiltroCausa=false;
function filtrarListAccionesMejora(){ 
	 var listReturn=[];
	 listReturn=listTablaAux.filter(function(val,index){ 
			
			if( arrayFiltroCausa.indexOf(val.causaId)!=-1
					&& arrayFiltroSolicitud.indexOf(val.solicitud.id)!=-1){
				return true;
			}else{ 
				return false;
			}
		});
	return listReturn;
} 
$(document).ready(function() {
	
	 $('.izquierda_flecha').on('click',function(){
		 numPaginaActual=numPaginaActual-1
    cambiarPaginaTabla();
    });
	 $('.derecha_flecha').on('click',function(){
		 numPaginaActual=numPaginaActual+1;
		 cambiarPaginaTabla();
    });
	 $('.izquierda_flecha').hover(function(){
         $(this).css('opacity','0.5');
     },function(){
         $(this).css('opacity','1');
     });

     $('.derecha_flecha').hover(function(){
         $(this).css('opacity','0.5');
     },function(){
         $(this).css('opacity','1');
     });
     
     $('#buscadorAcciones').keyup(function(e) {
 		if (e.keyCode == 13) {
 			$(this).trigger("enterKey");
 		}
 	});
 	$('#buscadorAcciones').bind("enterKey", function(e) {
 		cargarPrimerEstado();
 	
 	});
 	$("#buscadorAcciones").focus();

 	$("#resetBuscador").on("click", function(){
 		  $('#buscadorAcciones').val("");
 		 cargarPrimerEstado();
 	});
     
     
     
	insertMenu();
	cargarPrimerEstado();
	resizeDivGosst("wrapper" ,1);
	
	
	$(document).on("click","#filtroCausaAccion",function(){
		 $(".filtroMenuGosst").hide();console.log("huhu");
		 if($("#opcionesFiltroCausa").length>0){
			 $("#opcionesFiltroCausa").show(); 
		 } else{
			 $("body").append("<div class='filtroMenuGosst' id='opcionesFiltroCausa'></div>");
				var topDiv=$(this).position().top;
				var leftDiv=$(this).position().left;
				var cssFiltro={"top":199,"left":leftDiv+30 };
				var opcionesDiv=$("#opcionesFiltroCausa");
			
				listCausa.forEach(function(val,index){ 
					console.log(val.causaId); 
					opcionesDiv.css(cssFiltro).append("<div class='opcionFiltro'>" +
							"<input type='checkbox' id='checkFiltro"+val.causaId+"' checked> " +
							val.causaNombre+"" +
							"</div>");
				}); 
				opcionesDiv.append("<br><button class='btn btn-danger' ><i class='fa fa-times' aria-hidden='true'> </i>Cancelar </button>");
				opcionesDiv.find(".btn-danger").on("click",function(){
					 $(".filtroMenuGosst").hide();
				});
				opcionesDiv.append(" <button class='btn btn-info'> <i class='fa  fa-pencil-square-o' aria-hidden='true'></i> </button>");
				opcionesDiv.find(".btn-info").on("click",function(){
					toggleFiltroCausa=!toggleFiltroCausa;
					 $("#opcionesFiltroCausa .opcionFiltro input").each(function(index,elem){
						 
						 if(toggleFiltroCausa){
							 $(elem).prop("checked",false);
						 }else{
							 $(elem).prop("checked",true);
						 }
					 });
				});
				opcionesDiv.append(" <button class='btn btn-success' ><i class='fa fa-check' aria-hidden='true'></i>Listo </button>");
				opcionesDiv.find(".btn-success").on("click",function(){
					 
					arrayFiltroCausa=[]; 
					$("#opcionesFiltroCausa .opcionFiltro input").each(function(index,elem){
						if($(elem).prop("checked")){
							var idAux=parseInt($(elem).prop("id").substr(11,4));
							arrayFiltroCausa.push(idAux);}
					 
					});  
					listTabla=filtrarListAccionesMejora();
					cambiarPaginaTabla();
					$(".filtroMenuGosst").hide();
					});
		 }
		
	});
	
	$(document).on("click","#filtroSolicitud",function(){
		 $(".filtroMenuGosst").hide();
		 if($("#opcionesFiltroSolicitud").length>0){
			 $("#opcionesFiltroSolicitud").show(); 
		 } else{
			 $("body").append("<div class='filtroMenuGosst' id='opcionesFiltroSolicitud'></div>");
				var topDiv=$(this).position().top;
				var leftDiv=$(this).position().left;
				var cssFiltro={"top":199,"left":leftDiv+30 };
				var opcionesDiv=$("#opcionesFiltroSolicitud");
				listSolicitud.forEach(function(val,index){ 
					opcionesDiv.css(cssFiltro).append("<div class='opcionFiltro'>" +
							"<input type='checkbox' id='checkFiltro"+val.id+"'  checked> " +
							val.nombre+"" +
							"</div>");
				}); 
				opcionesDiv.append("<br><button class='btn btn-danger' ><i class='fa fa-times' aria-hidden='true'> </i>Cancelar </button>");
				opcionesDiv.find(".btn-danger").on("click",function(){
					 $(".filtroMenuGosst").hide();
				});
				opcionesDiv.append(" <button class='btn btn-info'> <i class='fa  fa-pencil-square-o' aria-hidden='true'></i> </button>");
				opcionesDiv.find(".btn-info").on("click",function(){
					toggleFiltroCausa=!toggleFiltroCausa;
					 $("#opcionesFiltroSolicitud .opcionFiltro input").each(function(index,elem){
						 
						 if(toggleFiltroCausa){
							 $(elem).prop("checked",false);
						 }else{
							 $(elem).prop("checked",true);
						 }
					 });
				});
				opcionesDiv.append(" <button class='btn btn-success' ><i class='fa fa-check' aria-hidden='true'></i>Listo </button>");
				opcionesDiv.find(".btn-success").on("click",function(){
					 
					arrayFiltroSolicitud=[]; 
					$("#opcionesFiltroSolicitud .opcionFiltro input").each(function(index,elem){
						if($(elem).prop("checked")){
							var idAux=parseInt($(elem).prop("id").substr(11,4));
							arrayFiltroSolicitud.push(idAux);}
					 
					});  
					listTabla=filtrarListAccionesMejora();
					cambiarPaginaTabla();
					$(".filtroMenuGosst").hide();
					});
		 }
		
	});
	
});
function calcularFilasEntrantes(){
	var sizeArriba=275;
	var sizeAbajo=40;
	var alturaFila=57;
	var alturaWindow=$(window).height();
	
	return Math.floor((alturaWindow-sizeAbajo-sizeArriba)/alturaFila)
}
function cambiarPaginaTabla(){
	//Definiendo variables
	 
	var list = listTabla;
	
	var totalSize=list.length;
	var partialSize=calcularFilasEntrantes();
	numPaginaTotal=Math.ceil(totalSize/partialSize); 
	var inicioRegistro=(numPaginaActual*partialSize)-partialSize;
	var finRegistro=(numPaginaActual*partialSize)-1;
	
	//aplicando logica de compaginacion
	
	$('.izquierda_flecha').show();
	 $('.derecha_flecha').show();
	 if(numPaginaTotal==0){
			numPaginaTotal=1;
		}
		$("#labelFlechas").html(numPaginaActual+" de "+numPaginaTotal);
	 if(numPaginaActual==1){
			$('.izquierda_flecha').hide();
	 }
	
	 if(numPaginaActual==numPaginaTotal){
		 $('.derecha_flecha').hide();
	 }
	if(finRegistro>=totalSize){
		finRegistro=totalSize-1;
	}
	$("#tblAccion tbody tr").remove();
	var nro = (partialSize-1)*(numPaginaActual-1);
	for (var index =inicioRegistro; index <= finRegistro; index++) {
		nro++;
		var val=list[index];
		 propiedad1=estadoAccion(
				val.numCorInmediataRetrasado,
				val.numCorInmediataProceso,
				val.numCorInmediataCompleto
				);
		 propiedad2=estadoAccion(
				val.numAccionRetrasado,
				val.numAccionProceso,
				val.numAccionCompleto
				);
		$("#tblAccion tbody")
				.append(
						"<tr id='tr"
								+ val.gestionAccionMejoraId
								+ "' onclick='javascript:editarGestionAccionMejora("+index+ ")' >"
								
								+ "<td id='tdgelug"+ val.gestionAccionMejoraId + "'>"
								+ val.lugar + "</td>"
								+ "<td id='tdgeneg"+ val.gestionAccionMejoraId + "'>"
								+ val.negocio+ "</td>"
								+ "<td id='tdgenoc"+ val.gestionAccionMejoraId + "'>"
								+ val.nroNoConformidad + "</td>"
								
								+ "<td id='tdcausa"+ val.gestionAccionMejoraId + "'>"
								+ val.causaNombre + "</td>"

								+ "<td id='tdgehall"+ val.gestionAccionMejoraId + "'>"
								+ val.hallazgo+ "</td>"
								+ "<td id='tdgenor"+ val.gestionAccionMejoraId + "'>"
								+ val.norma+ "</td>"
								+ "<td id='tdgeorg"+ val.gestionAccionMejoraId + "'>"
								+ val.organizacion+ "</td>"
								
								
								+ "<td id='tdsoli"+ val.gestionAccionMejoraId + "'>"
								+ val.solicitud.nombre + "</td>"
								+ "<td id='tdres"+ val.gestionAccionMejoraId + "'>"
								+ val.resumen + "</td>"

								+ "<td id='tdgefeap"+ val.gestionAccionMejoraId + "'>"
								+ val.fechaAperturaTexto+ "</td>"
								+ "<td id='tdgefeob"+ val.gestionAccionMejoraId + "'>"
								+ val.fechaObjetivoTexto+ "</td>"
								
								
								+ "<td id='tdcorr"+ val.gestionAccionMejoraId
								+ "' style='color:"+ propiedad1.color+"'>"+devolverNumeroSustantivo(
										val.numCorInmediata,
										"correción",
										"correcciones")
										+ propiedad1.icono
										
										+"</td>"

								+ "<td id='tdanali"+ val.gestionAccionMejoraId
								+ "'>"+devolverNumeroSustantivo(
										val.numCausas,
										"causa",
										"causas")+"</td>"

								+ "<td id='tdacc"+ val.gestionAccionMejoraId
								+ "' style='color:"+ propiedad2.color+"'>"+devolverNumeroSustantivo(
										val.numAccionesMejora,
										"acción",
										"acciones")+	 propiedad2.icono+
										"</td>"

								+ "<td id='tdefic"+ val.gestionAccionMejoraId
								+ "'>"+pasarDecimalPorcentaje(val.eficacia/100,2)+"</td>"

								+ "<td id='tdgeest"+ val.gestionAccionMejoraId + "'>"
								+ val.estadoCumplimientoNombre+ "</td>"
								+ "<td id='tdgedias"+ val.gestionAccionMejoraId + "'>"
								+ val.diasRetraso+ "</td>"
								+ "</tr>");
		
	}
	goheadfixed("table.fixed");
	completarBarraCarga();
	formatoCeldaSombreableTabla(true,"tblAccion");
	
	
}
function cargarPrimerEstado() {
	numPaginaActual=1;
	banderaEdicion = false;
	gestionAccionMejoraId = 0;
	causaId = 0;
	estadoCumpId = 1;
	accionMejoraTipoIdActual = 0;
	hayProceso=false;
	 hayCompleto=false;
	 hayRetrasado=false;
	 huboCambiosAcciones=false;
	 var palabraClave = $("#buscadorAcciones").val();
		if (palabraClave.length == 0) {
			palabraClave = null;
		} else {
			palabraClave = ("%" + palabraClave + "%").toUpperCase();
		}
	removerBotones();
	crearBotones();

	deshabilitarBotonesEdicion();

	$("#btnNuevo").attr("onclick", "javascript:nuevoGestionAccionMejora();");
	$("#btnCancelar").attr("onclick",
			"javascript:cancelarGestionAccionMejora();");
	$("#btnGuardar")
			.attr("onclick", "javascript:guardarGestionAccionMejora();");
	$("#btnEliminar").attr("onclick",
			"javascript:eliminarGestionAccionMejora();");
	$("#fsBotones")
	.append(
			"<button id='btnGenReporte' type='button' class='btn btn-success btn-word' title='Generar Informe'>"
					+ "<i class='fa fa-file-word-o fa-2x'></i>"
					+ "</button>");
	
	$("#fsBotones")
	.append("<button id='btnClearFiltro' type='button' title='Quitar filtros'"
	  + " class='btn btn-success btn-refresh' >"
		+"<i class='fa fa-filter fa-2x'></i></button>");
	$("#btnClearFiltro").on("click",function(){
		arrayFiltroCausa=listarStringsDesdeArray(listCausa,"causaId");
		arrayFiltroSolicitud=listarStringsDesdeArray(listSolicitud,"id");
		$(".filtroMenuGosst .opcionFiltro input").each(function(index,elem){
			$(elem).prop("checked",true) 
		 
		});  
		listTabla=listTablaAux;
		numPaginaActual=1;
		cambiarPaginaTabla();
	});
	$("#btnGenReporte").attr("onclick", "javascript:generarReporteAccion();");
	$("#btnGenReporte").hide();
	
	var dataParam = {
			idCompany : sessionStorage.getItem("gestopcompanyid"),
			palabraClave:palabraClave
	};

	callAjaxPost(URL + '/gestionaccionmejora', dataParam,
			procesarDataDescargadaPrimerEstado);
	
	var dataParam2 = {
			idCompany: sessionStorage.getItem("gestopcompanyid")
		};
	callAjaxPost(URL + '/accidente/trab', dataParam2,
			function(data){
		switch (data.CODE_RESPONSE) {
		
		case "05":
		
			 listDniNombre=data.listDniNombre;
			
			
		
			
			break;
		default:
			alert("Ocurrió un error al traer los Accidentes!");
		}
		
	listanombretab=listarStringsDesdeArray(listDniNombre, "nombre");
	});
	$("#btnUpload").hide();
}

function procesarDataDescargadaPrimerEstado(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		var list = data.list;
		listTabla=list;
		listTablaAux=listTabla;
		listCausa = data.listCausa;
		
		
		listEstado = data.listEstado;
		listSolicitud=[{id:1,nombre:"Preventiva"},
		               {id:2,nombre:"Correctiva"},
		               {id:3,nombre:"De mejora"}];
		arrayFiltroCausa=listarStringsDesdeArray(listCausa,"causaId");
		arrayFiltroSolicitud=listarStringsDesdeArray(listSolicitud,"id");
		listTipoAccionMejora = data.listTipoAccionMejora;
		listRiesgoPotencial = data.listRiesgoPotencial;
		propiedad1=[];
		propiedad2=[];
		$("#h2Titulo")
				.html(
						"<a onclick='javascript:cargarPrimerEstado();' href='#'>Gestion Accion Mejora</a>");
		
		if(getSession("linkCalendarioAccMejoraId")!=null){
			var indexIpercAux=0;
			listTabla.every(function(val,index3){
				
				if(val.gestionAccionMejoraId==parseInt(getSession("linkCalendarioAccMejoraId"))){
					
					numPaginaActual=Math.ceil((index3+1)/(calcularFilasEntrantes()));
					cambiarPaginaTabla();
					editarGestionAccionMejora(index3);
					
					if(parseInt(getSession("linkCalendarioTipoAccMejoraId"))==1){
					
						showCorreccionInmediata();
					}else{
						
						showAccionMejora();
					}
					
					sessionStorage.removeItem("linkCalendarioAccMejoraId");
					sessionStorage.removeItem("linkCalendarioTipoAccMejoraId");

					return false;
				}else{
					return true;
				}
			});
			
		}else{
			cambiarPaginaTabla();
		}
		
		break;
	default:
		alert("Ocurrió un error al traer las gestiones acciones de mejoras!");
	}
}

function editarGestionAccionMejora( index) {
	if (!banderaEdicion) {
		formatoCeldaSombreableTabla(false,"tblAccion");
		var pcausaId=listTabla[index].causaId;
		solicitudAccionId=listTabla[index].solicitud.id;
		var	pestadoCumpId=listTabla[index].estadoId; 
		var priesgoPotId=listTabla[index].riesgoPotencialId; 
		var pdanio=listTabla[index].danio; 
		var priesgoConsecuencia=listTabla[index].riesgoConsecuencia; 
		var peficacia=listTabla[index].eficacia;
		
		var peficaciaMotivo=listTabla[index].eficaciaMotivo; 
		var peficaciaResponsableNombre=listTabla[index].eficaciaResponsableNombre; 
		var peficaciaResponsableMail=listTabla[index].eficaciaResponsableMail;
		var pDeclaracionCierre=listTabla[index].declaracionCierre;
		var pEficaciaFecha=listTabla[index].fechaCierre;
		var fechaInputCierre = convertirFechaInput(pEficaciaFecha);
		gestionAccionMejoraId = listTabla[index].gestionAccionMejoraId;
		estadoCumpId= listTabla[index].estadoId;
		causaId = pcausaId;
		estadoCumpId = pestadoCumpId;
		var gestionAccionObj=listTabla[index];
		//
		$("#tdgelug" + gestionAccionMejoraId)
		.html("<input id='inputLugarGeAcc' class='form-control'>");
		$("#inputLugarGeAcc").val(gestionAccionObj.lugar);
		//
		$("#tdgeneg" + gestionAccionMejoraId)
		.html("<input id='inputNegocioGeAcc' class='form-control'>");
		$("#inputNegocioGeAcc").val(gestionAccionObj.negocio);
		//
		$("#tdgenoc" + gestionAccionMejoraId)
		.html("<input id='inputNoConfGeAcc' class='form-control'>");
		$("#inputNoConfGeAcc").val(gestionAccionObj.nroNoConformidad);
		//
		$("#tdgehall" + gestionAccionMejoraId)
		.html("<input id='inputHallGeAcc' class='form-control'>");
		$("#inputHallGeAcc").val(gestionAccionObj.hallazgo);
		//
		$("#tdgenor" + gestionAccionMejoraId)
		.html("<input id='inputNormaGeAcc' class='form-control'>");
		$("#inputNormaGeAcc").val(gestionAccionObj.norma);
		//
		$("#tdgeorg" + gestionAccionMejoraId)
		.html("<input id='inputOrgGeAcc' class='form-control'>");
		$("#inputOrgGeAcc").val(gestionAccionObj.organizacion);
		
		//
		
		$("#tdgefeap" + gestionAccionMejoraId)
		.html("<input type='date' id='dateAperGeAcc' class='form-control'>");
		$("#dateAperGeAcc").val(convertirFechaInput(gestionAccionObj.fechaApertura));
		//
		$("#tdgefeob" + gestionAccionMejoraId)
		.html("<input type='date' id='dateObjetGeAcc' class='form-control'>");
		$("#dateObjetGeAcc").val(convertirFechaInput(gestionAccionObj.fechaObjetivo));
		//
		
		var name = $("#tdres" + gestionAccionMejoraId).text();
		$("#tdres" + gestionAccionMejoraId)
				.html(
						"<textarea type='text' id='inputRes' class='form-control' placeholder='Resumen' rows='2'>"
								+ name + "</textarea>");

		$("#tdasoc" + gestionAccionMejoraId)
				.html(
						"<a href='#' onclick='javascript:showItemAsoc();'> "+$("#tdasoc" + gestionAccionMejoraId).html()+"</a>");
		$("#tdcorr" + gestionAccionMejoraId)
				.html(
						"<a href='#' onclick='javascript:showCorreccionInmediata();'> "+$("#tdcorr" + gestionAccionMejoraId).html()+" </a>");
		if(pcausaId != 3){
			$("#tdanali" + gestionAccionMejoraId)
			.html(
					"<a href='#' onclick='javascript:showAnalisisCausa();'> "+$("#tdanali" + gestionAccionMejoraId).html()+" </a>");
			$("#tdacc" + gestionAccionMejoraId)
			.html(
					"<a href='#' onclick='javascript:showAccionMejora();'> "+$("#tdacc" + gestionAccionMejoraId).html()+"</a>");
			$("#tdefic" + gestionAccionMejoraId).html(
					"<a href='#' onclick='javascript:showEficacia();'> "+$("#tdefic" + gestionAccionMejoraId).html()+"</a>");
		} else {
			$("#tdanali" + gestionAccionMejoraId)
			.html(
					"Restringido");
			$("#tdacc" + gestionAccionMejoraId)
			.html(
					"Restringido");
			$("#tdefic" + gestionAccionMejoraId).html(
					"Restringido");	
		}
		
		/* ************************************************************************** */
		var selCausa = crearSelectOneMenu("selCausa", "", listCausa, causaId,
				"causaId", "causaNombre");
		var slcTipoSoli = crearSelectOneMenu("slcTipoSoli", "", listSolicitud, solicitudAccionId,
				"id", "nombre");
		$("#tdcausa" + gestionAccionMejoraId).html(selCausa);
		$("#tdsoli" + gestionAccionMejoraId).html(slcTipoSoli);
		
		$("#selCausa").attr("onChange", "javascript:onchangeCmbCausa();");

	

		/** *************RIESGO************** */
		var selRiesgoPot = crearSelectOneMenu("selRiesgoPot", "",
				listRiesgoPotencial, priesgoPotId, "riesgoPotencialId",
				"riesgoPotencialNombre");
		$("#tdRieSelPot").html(selRiesgoPot);

		$("#inputRieCon").val(priesgoConsecuencia);
		$("#inputRieDan").val(pdanio);

		/** *************EFICACIA************** */
		$("#inputEficPorc").val(peficacia);
		$("#inputEficMot").val(peficaciaMotivo);
		$("#inputEficResNom").val(peficaciaResponsableNombre);
		
		ponerListaSugerida("inputEficResNom",listanombretab,true);
		$("#inputEficResNom").on("autocompleteclose",function(){
			
			llenarCorreoAutoTrabAnalisi();
			
		});
		
		$("#inputEficResMail").val(peficaciaResponsableMail);
		$("#inputEficFecha").val(fechaInputCierre);
		$("#inputDeclaracionCierre").val(pDeclaracionCierre);
		$("#tdEfiEvi")
		.html(
				"<a href='"
							+ URL
							+ "/gestionaccionmejora/evidencia?gestionAccionMejoraId="
							+ gestionAccionMejoraId
							+ "' "
							+ "target='_blank' id='descargarimagen"
							+ gestionAccionMejoraId
							+ "'>Descargar</a>"
							+ "<br/><a href='#' onclick='javascript:cargarEfEvidencia();'>Agregar</a>");

		banderaEdicion = true;
		habilitarBotonesEdicion();
		$("#btnGenReporte").show();
	}
}

function nuevoGestionAccionMejora() {
	if (!banderaEdicion) {
		var selCausa = crearSelectOneMenu("selCausa", "", listCausa, "-1",
				"causaId", "causaNombre");
		var slcTipoSoli = crearSelectOneMenu("slcTipoSoli", "", listSolicitud, "-1",
				"id", "nombre");
		$("#tblAccion tbody")
				.prepend(
						"<tr id='0'>"
								+ "<td><input id='inputLugarGeAcc' class='form-control'></td>"
								+ "<td><input id='inputNegocioGeAcc' class='form-control'></td>"
								+ "<td><input id='inputNoConfGeAcc' class='form-control'></td>"
								+ "<td>"
								+ selCausa
								+ "</td>"
								+ "<td><input id='inputHallGeAcc' class='form-control'></td>"
								+ "<td><input id='inputNormaGeAcc' class='form-control'></td>"
								+ "<td><input id='inputOrgGeAcc' class='form-control'></td>"
								+ "<td>"
								+ slcTipoSoli
								+ "</td>"
								+ "<td><textarea type='text' id='inputRes' class='form-control' placeholder='Resumen' rows='2'/>"
								+ "<td><input type='date' id='dateAperGeAcc' class='form-control'></td>"
								+ "<td><input type='date' id='dateObjetGeAcc' class='form-control'></td>"
								
								+ "<td></td>" 
								+ "<td>...</td>" 
								+ "<td>...</td>"
								+ "<td>...</td>" 
								+ "<td>...</td>"
								+"<td>0</td>"
								+ "</tr>");
		gestionAccionMejoraId = 0;
		habilitarBotonesNuevo();
		banderaEdicion = true;
		formatoCeldaSombreableTabla(false,"tblAccion");
		
		estadoCumpId= 1;
			
	} else {
		alert("Guarde primero.");
	}
}

function cancelarGestionAccionMejora() {
	cargarPrimerEstado();
}

function eliminarGestionAccionMejora() {
	var r = confirm("¿Está seguro de eliminar la Gestion de Accion Mejora?");
	if (r == true) {
		var dataParam = {
			gestionAccionMejoraId : gestionAccionMejoraId
		};

		callAjaxPost(URL + '/gestionaccionmejora/delete', dataParam,
				procesarResultadoEliminarGestionAccionMejora);
	}
}

function procesarResultadoEliminarGestionAccionMejora(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		cargarPrimerEstado();
		break;
	default:
		alert("Ocurrió un error al eliminar la Gestion Accion Mejora!");
	}
}

function guardarGestionAccionMejora() {

	var campoVacio = true;
	var selCausa = $("#selCausa option:selected").val();
	
	var selRiePot = $("#selRiesgoPot option:selected").val();
	var inputRes = $("#inputRes").val();
	var inputRieCon = $("#inputRieCon").val();
	var inputRieDan = $("#inputRieDan").val();
	var inputEficPorc = $("#inputEficPorc").val();
	var inputEficMot = $("#inputEficMot").val();
	var inputEficResNom = $("#inputEficResNom").val();
	var inputEficResMail = $("#inputEficResMail").val();
	
	var inputDeclaracion = $("#inputDeclaracionCierre").val();
	
var tipoSolicitudId=$("#slcTipoSoli").val();

	var inputEficFecha = convertirFechaTexto($("#inputEficFecha").val());
	
	var lugar=$("#inputLugarGeAcc").val();
	var negocio=$("#inputNegocioGeAcc").val();
	var nroNoConformidad=$("#inputNoConfGeAcc").val();

	var hallazgo=$("#inputHallGeAcc").val();
	var norma=$("#inputNormaGeAcc").val();
	var organizacion=$("#inputOrgGeAcc").val();

	var fechaApertura=convertirFechaTexto($("#dateAperGeAcc").val());
	var fechaObjetivo=convertirFechaTexto($("#dateObjetGeAcc").val());

	if(huboCambiosAcciones){
		if(hayRetrasado){
			estadoCumpId= 3
		}else{
			if(hayProceso){
				estadoCumpId= 1
			}else{
				if(hayCompleto){
					estadoCumpId= 2
				}
			}
		}
	}

	if (selCausa == '-1' ||  inputRes.length == 0 ||tipoSolicitudId=="-1") {
		campoVacio = false;
	}

	if (campoVacio) {

		var dataParam = {
			gestionAccionMejoraId : gestionAccionMejoraId,
			lugar:lugar,
			negocio:negocio,
			nroNoConformidad:nroNoConformidad,
			hallazgo:hallazgo,
			norma:norma,
			organizacion:organizacion,
			fechaApertura:fechaApertura,
			fechaObjetivo:fechaObjetivo,
			
			resumen : inputRes,
			causaId : selCausa,
			solicitud:{id:tipoSolicitudId},
			riesgoConsecuencia : inputRieCon,
			danio : inputRieDan,
			riesgoPotencialId : selRiePot,
			eficacia : inputEficPorc,
			eficaciaMotivo : inputEficMot,
			eficaciaResponsableNombre : inputEficResNom,
			eficaciaResponsableMail : inputEficResMail,
			estadoId:estadoCumpId,
			fechaCierre:inputEficFecha,
		declaracionCierre:inputDeclaracion,
			idCompany : sessionStorage.getItem("gestopcompanyid")
		};

		callAjaxPost(URL + '/gestionaccionmejora/save', dataParam,
				procesarResultadoGuardarGestionAccionMejora);
	} else {
		alert("Debe ingresar todos los campos.");
	}
}

function procesarResultadoGuardarGestionAccionMejora(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		cargarPrimerEstado();
		break;
	default:
		alert("Ocurrió un error al guardar la Gestion accion Mejora!");
	}
}

function showItemAsoc() {
	$('#mdItemAsoc').modal('show');
}

$('#mdItemAsoc').on('show.bs.modal', function(e) {
	var dataParam = {
			gestionAccionMejoraId : gestionAccionMejoraId
		};

		callAjaxPost(URL + '/gestionaccionmejora/detalle', dataParam,
				procesarDataListadoDetalleAsociados);
});

function procesarDataListadoDetalleAsociados(data){
	switch (data.CODE_RESPONSE) {
	case "05":
		var list = data.list;

		$("#tblItemAsoc tbody tr").remove();

		var nro = 0;
		for (index = 0; index < list.length; index++) {
			nro++;
			$("#tblItemAsoc tbody")
					.append(
							"<tr id='tr"
									+ list[index].detalleGestAccionMejoraId
									+ "'>"

									+ "<td id='tdmod"
									+ list[index].detalleGestAccionMejoraId + "'>"
									+ list[index].moduloNombre + "</td>"

									+ "<td id='tdres"
									+ list[index].detalleGestAccionMejoraId + "'>"
									+ list[index].categoriaNombre + "</td>"

									+ "<td id='tddetalle"
									+ list[index].detalleGestAccionMejoraId
									+ "'>"+ list[index].detalleNombre +"</td>"
									+ "<td id='tdcorr"
									+ list[index].detalleGestAccionMejoraId
									+ "'>"+ list[index].observacion +"</td>"
									+ "<td id='tdest"
									+ list[index].detalleGestAccionMejoraId + "'>"
									+ list[index].fechaInicioTexto
									+ "</td>"

									+ "</tr>");	$("#tddetalle"+ list[index].detalleGestAccionMejoraId).css({"text-align":"left"});
		}
	
		
		break;
	default:
		alert("Ocurrió un error al traer los items asociados!");
	}	
}

function showCorreccionInmediata() {
	$("#mdCorrLabel").text("Correccion Inmediata");
	var band = $("#selCausa option:selected").val();
	if(band > -1){
		accionMejoraTipoIdActual = 1;
		$('#mdCorr').modal('show');	

		cargarModalAccionMejora();
	}
}

function showAccionMejora() {
	$("#mdCorrLabel").text("Accion de Mejora");
	var band = $("#selCausa option:selected").val();
	
	if(band > -1){
		accionMejoraTipoIdActual=-1;
		
		listTipoAccionMejoraDepurado = listTipoAccionMejora;
		if(band == 2){
			var index = listTipoAccionMejora.indexOf(3);
			listTipoAccionMejoraDepurado.splice(index-1, 1);
		}
		if(band == 4){
			var index = listTipoAccionMejora.indexOf(3);
			listTipoAccionMejoraDepurado = listTipoAccionMejoraDepurado.splice(index, 1);
		}
		
		$('#mdCorr').modal('show');	

		cargarModalAccionMejora();
	}

}

$('#mdCorr').on('show.bs.modal', function(e) {
	$("#modalAcc").show();
	$("#modalAccUpload").hide();
});

function showAnalisisCausa() {
	$('#mdAnalCau').modal('show');
}

$('#mdAnalCau').on('show.bs.modal', function(e) {
	$("#modalAn").show();
	$("#modalAnUpload").hide();
	cargarModalAnalisisCausa();
});

function showEficacia() {
	$("#modalEf").show();
	$("#modalEfUpload").hide();
	$('#mdEfic').modal('show');
}

function cargarEfEvidencia() {
	$("#modalEf").hide();
	$("#modalEfUpload").show();
}

function cancelarEfUploadEvidencia() {
	$("#modalEf").show();
	$("#modalEfUpload").hide();
}

function uploadEfEvidencia() {
	var inputFileImage = document.getElementById("fileEfEvi");
	var file = inputFileImage.files[0];
	var data = new FormData();

	if (file.size > 512000) {
		alert("Lo sentimos, solo se pueden subir archivos menores a 500Kb.");
	} else {
		data.append("fileEfEvi", file);
		data.append("gestionAccionMejoraId", gestionAccionMejoraId);
	
		var url = URL + '/gestionaccionmejora/evidencia/save';
		$.blockUI({
			message : 'cargando...'
		});
		$
				.ajax({
					url : url,
					xhrFields: {
			            withCredentials: true
			        },
					type : 'POST',
					contentType : false,
					data : data,
					processData : false,
					cache : false,
					success : function(data, textStatus, jqXHR) {
						switch (data.CODE_RESPONSE) {
						case "06":
							sessionStorage.clear();
							document.location.replace(data.PATH);
							break;
						default:
							console.log('Se subio el archivo correctamente.');
							$("#modalEf").show();
							$("#modalEfUpload").hide();
						}
						$.unblockUI();
					},
					error : function(jqXHR, textStatus, errorThrown) {
						alert("Ups, tenemos problemas tecnicos. Intente más tarde. Error: "
								+ errorThrown);
						console.log('xhRequest: ' + jqXHR + "\n");
						console.log('ErrorText: ' + textStatus + "\n");
						console.log('thrownError: ' + errorThrown + "\n");
						$.unblockUI();
					}
				});
	}

}

function onchangeCmbCausa(){
	var band = $("#selCausa option:selected").val();
	
	if(band != 3){
		$("#tdanali" + gestionAccionMejoraId)
		.html(
				"<a href='#' onclick='javascript:showAnalisisCausa();'>Agregar</a>");
		$("#tdacc" + gestionAccionMejoraId)
		.html(
				"<a href='#' onclick='javascript:showAccionMejora();'>Agregar</a>");
		$("#tdefic" + gestionAccionMejoraId).html(
				"<a href='#' onclick='javascript:showEficacia();'>Agregar</a>");
	} else {
		$("#tdanali" + gestionAccionMejoraId)
		.html(
				"Restringido");
		$("#tdacc" + gestionAccionMejoraId)
		.html(
				"Restringido");
		$("#tdefic" + gestionAccionMejoraId).html(
				"Restringido");	
	}
}


function devolverNumeroSustantivo(numero,singular,plural){
	if(numero==1){
		return numero +" "+singular;
	}
	if(numero==0){
		return "No hay "+plural;
	}
	if(numero>1){
		return numero +" "+plural;
	}
}

function estadoAccion(numRetrasados, numImplementar, numCompletados) {
	var color = "";
	var icono = "";
	var propiedades;
	var mensaje = "";
	var totalAcciones=numRetrasados+numImplementar+numCompletados;
	mensaje = "<br>";
	
	
	

	if (numImplementar > 0 && numRetrasados == 0) {
		mensaje = devolverNumeroSustantivo( numImplementar,"acci&oacuten","acciones") ;
		color = "#FF8000";

		icono = ' <i class="fa fa-eye"></i>';
	
		
	}

	if (numRetrasados > 0) {
		mensaje = devolverNumeroSustantivo( numRetrasados,"acci&oacuten","acciones") ;
		icono = ' <i class="fa fa-exclamation-circle"></i>';
		color = "red";
		

	}

	if (numRetrasados == 0 && numImplementar == 0 && numCompletados > 0) {
		color = "green";
		mensaje = devolverNumeroSustantivo( numCompletados,"acci&oacuten","acciones") ;
		icono = ' <i class="fa fa-check-square"></i>';
		
	}
	
	mensaje="<h5 style='color:"+color+";font-weight:bold'>"
	+devolverNumeroSustantivo( totalAcciones,"acci&oacuten","acciones")+" "+icono+"</h5>"
	
	propiedades = {
		color : color,
		icono : icono,
		mensaje : mensaje
		
	}
	
	return propiedades

}


function generarReporteAccion(){
	var tienePermisoReporte=true;
	if(tienePermisoReporte){
		window.open(URL
				+ "/gestionaccionmejora/informe?gestionAccionId="
				+ gestionAccionMejoraId 
				+ '');	
	}else{
		alert("Faltan cmapos de registro ")
		
	}

}
