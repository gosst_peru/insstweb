var asociadoTipo;
var asociadoId;
var gestionMejoraId;
var restriccionId;
$(document).ready(function(){
		 
})
function irAAccionMejora() {
	document.location.href = 'accionmejora.html';
}

function agregarGestionAccionMejora(asociadoTipo, asociadoId) {
	$("#divAsocGAM").show();
	$("#divOpcion").hide(); 
	var dataParam = {
		idCompany : sessionStorage.getItem("gestopcompanyid"),
		asociadoTipo : parseInt(asociadoTipo),
		asociadoId : asociadoId
	};
	 
	callAjaxPost(URL + '/gestionaccionmejora/items', dataParam,
			procesarDataDescargadaAgregarGAM);
}

function procesarDataDescargadaAgregarGAM(data) {
	switch (data.CODE_RESPONSE) {
	case "05":
		var list = data.list;
var listTipoAccionMejora=data.listTipoAccionMejora;
		$("#tblV tbody tr").remove(); 
		var selCausaAcc = crearSelectOneMenu("selCausaAcc", "", 
				listTipoAccionMejora, "-1", "causaId", "causaNombre","Seleccionar nueva acción de mejora");
	
		var fila = "<tr id='tr" + "0"
		+ "'>"

		+ "<td id='tdsel" +"0"
		+ "'><input type='radio' name='gestion' id='rad"
		+ "0"+ "'></td>"
		+ "<td id='tdcausa" + "0"
		+ "'>" + selCausaAcc + "</td>"

		+ "<td id='tdres" + "0"
		+ "'>" + "<input type='text' class='form-control' id='resumenAcc'>"+ "</td>"

		+ "<td id='tdest" + "0"
		+ "'>" + "Por implementar" + "</td>"

		+ "</tr>"
$("#tblV tbody").append(fila);
		
		$("#rad" + "0")
		.attr(
				"onclick",
				"javascript:seleccionarGAM("
						+ "0"+ ");");
		for (index = 0; index < list.length; index++) {
			var fila = "<tr id='tr" + list[index].gestionAccionMejoraId
					+ "'>"

					+ "<td id='tdsel" + list[index].gestionAccionMejoraId
					+ "'><input type='radio' name='gestion' id='rad"
					+ list[index].gestionAccionMejoraId + "'></td>"
					+ "<td id='tdcausa" + list[index].gestionAccionMejoraId
					+ "'>" + list[index].causaNombre + "</td>"

					+ "<td id='tdres" + list[index].gestionAccionMejoraId
					+ "'>" + list[index].resumen + "</td>"

					+ "<td id='tdest" + list[index].gestionAccionMejoraId
					+ "'>" + list[index].estadoCumplimientoNombre + "</td>"

					+ "</tr>"
			$("#tblV tbody").append(fila);

			$("#rad" + list[index].gestionAccionMejoraId)
					.attr(
							"onclick",
							"javascript:seleccionarGAM("
									+ list[index].gestionAccionMejoraId + ");");
			if (list[index].seleccionado > 0) {
				$("#rad" + list[index].gestionAccionMejoraId).prop('checked',
						true);
				gestionAccionMejoraId = list[index].gestionAccionMejoraId;
			}

		}
$("#selCausaAcc").prop('disabled', true);
		
		$("#resumenAcc").prop('disabled', true);
		
		
		switch(restriccionId){
		case 1:
			$("#selCausaAcc option").each(function(index,elem){
				if($(elem).prop("value")!=5){
					$(elem).remove();
				}
			});
			 
			break;
		case 2:
		case 3:
		case 4:
			$("#selCausaAcc option").each(function(index,elem){
				var listPermitido=[1,2,3,4]; 
				if(listPermitido.indexOf(parseInt($(elem).prop("value")))==-1){
					$(elem).remove();
				}
			});
			 
			break;
		
			 
		case 6:
			$("#selCausaAcc option").each(function(index,elem){
				if($(elem).prop("value")!=6){
					$(elem).remove();
				}
			});
			$("#tblV tbody tr").each(function(index,elem){
				if(index>0){
					var selected=$(elem).find("td input");
					if(selected.prop("checked")){
						
					}else{
						$(elem).remove();
					}
				}
			});
			break;
		case 7:
			$("#selCausaAcc option").each(function(index,elem){
				if($(elem).prop("value")!=7){
					$(elem).remove();
				}
			});
			$("#tblV tbody tr").each(function(index,elem){
				if(index>0){
					var selected=$(elem).find("td input");
					if(selected.prop("checked")){
						
					}else{
						$(elem).remove();
					}
				}
			});
			break;
		case 8:
			$("#selCausaAcc option").each(function(index,elem){
				if($(elem).prop("value")!=8){
					$(elem).remove();
				}
			});
			$("#tblV tbody tr").each(function(index,elem){
				if(index>0){
					var selected=$(elem).find("td input");
					if(selected.prop("checked")){
						
					}else{
						$(elem).remove();
					}
				}
				
			});
			break;
		case 9:
			$("#selCausaAcc option").each(function(index,elem){
				if($(elem).prop("value")!=9){
					$(elem).remove();
				}
			});
			$("#tblV tbody tr").each(function(index,elem){
				if(index>0){
					var selected=$(elem).find("td input");
					if(selected.prop("checked")){
						
					}else{
						$(elem).remove();
					}
				}
				
			});
			break;
		}
		
		
		break;
	default:
		alert("Ocurrió un error al traer las gestiones acciones de mejoras!");
	}
}
// onload del modal
function agregarAccionMejora(pasociadoTipo, pasociadoId,prestriccionId) {
	if(pasociadoId){
		asociadoTipo = pasociadoTipo;
		asociadoId = pasociadoId;
		restriccionId=prestriccionId;
		$("#mdradagregacc").attr(
				"onclick",
				"javascript:agregarGestionAccionMejora(" + asociadoTipo + ", "
						+ asociadoId + ");");
		$('#mdVerif').modal('show');
		$("#divAsocGAM").show();
		$("#divOpcion").hide();
		agregarGestionAccionMejora(asociadoTipo,asociadoId);
	} else {
		alert("Esta pregunta no esta respondida.");
	}

}



function seleccionarGAM(pgestionAccionMejoraId) {
	if(parseInt(pgestionAccionMejoraId)==0){
		$("#selCausaAcc").prop('disabled', false);
		
		$("#resumenAcc").prop('disabled', false);
	}else{
		$("#selCausaAcc").prop('disabled', true);
		
		$("#resumenAcc").prop('disabled', true);
	}
	gestionAccionMejoraId = pgestionAccionMejoraId;
}

function guardarSeleccion() {
	var dataParam = {
			idCompany : sessionStorage.getItem("gestopcompanyid"),
		asociadoTipo : asociadoTipo,
		asociadoId : asociadoId,
		causaId:$("#selCausaAcc").val(),
		resumen:$("#resumenAcc").val(),estadoId:1,
		gestionAccionMejoraId : gestionAccionMejoraId
	};
console.log(dataParam);
	callAjaxPost(URL + '/gestionaccionmejora/items/save', dataParam,
			procesarGuardarAsociacionGAM);
}

function procesarGuardarAsociacionGAM(data){
	switch (data.CODE_RESPONSE) {
	case "05":
		$('#mdVerif').modal('hide');
		break;
	default:
		alert("Ocurrió un error al guardar la Asociacion!");
	}
		
}