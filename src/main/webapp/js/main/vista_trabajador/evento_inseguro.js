/**
 * 
 */

function verImagenActoMovil(idActo){
	var dataParam2={
			acInseguroId:idActo 
	}
	$("#imgVer"+idActo).show();
	callAjaxPostNoLoad(URL + '/acinseguro/id', dataParam2,function(data){
		var acto=data.actos;
		var imagenActo=insertarImagenParaTablaMovil(acto.evidencia);
		$("#actoMovil"+idActo).find(".cropGosst").html(imagenActo);
		$("#descargaVistaPrevia"+idActo).remove()
		$("#actoMovil"+idActo)
		.append("<a class='efectoLink' target='_blank'" +
				"href='"+URL+"/acinseguro/evidencia?acInseguroId="+idActo+"' id='descargaVistaPrevia"+idActo+"'>" +
				"<i class='fa fa-download'></i> Descargar" +
				"</a>");
	},function(){
		$("#actoMovil"+idActo).find(".cropGosst").html("Cargando...")
	});
}

var indexFunction=0;
var indexFunctionAC=0;
function toggleMenuOpcionAC(obj,pindex,menu)
{
	indexFunction=pindex;
	indexFunctionAC=pindex;
	$(obj).parent("#opciones"+menu).find("ul").toggle();
	$(obj).parent("#opciones"+menu).siblings().find("ul").hide();
}
var funcionalidadesAC=[
	{menu:"1",opcion: 
		[{id:"opcAgregarAccion",nombre:"<i class='fa fa-plus'></i> Agregar acción de mejora",
			functionClick:function(data){agregarAccionMejoraEventoInseguro() }},
		{id:"opcAnularAc",nombre:"<i class='fa fa-trash'></i> Calificar reporte como no valido",
			functionClick:function(data){anularEventoInseguroColaborador() }}]
	},
	{menu:"2",opcion: 
		[{id:"opcDescargar",nombre:"<i class='fa fa-download'></i> Descargar",
				functionClick:function(data){window.open(URL+
					"/gestionaccionmejora/accionmejora/evidencia?accionMejoraId="+indexFunctionAC);
		}}]
	},
	{menu:"3",opcion: 
		[{id:"opcAgregarAccion",nombre:"<i class='fa fa-plus'></i> Agregar acción de mejora",
			functionClick:function(data){agregarAccionMejoraEventoInseguro() }}]
	},
	{menu:"AccionMejora",opcion:
		[{id:"opcEditar",nombre:"<i class='fa fa-pencil-square-o'></i> Editar",functionClick:
			function(data)
			{ 
			editarAccionMejoraEvento();
			}
		},
		{id:"opcEditar",nombre:"<i class='fa fa-trash-o'></i> Eliminar",functionClick:
			function(data)
			{ 
				eliminarAccionMejoraEvento();
			}
		}
	]},
	{menu:"5",opcion:
		[{id:"asd",nombre:"<i class='fa fa-download'></i> Descargar plantilla",functionClick:
			function(data)
			{ 
			verPlantillaImportActosColaborador();
			}
		},
		{id:"asd1",nombre:"<i class='fa fa-upload'></i> Subir plantilla",functionClick:
			function(data)
			{ 
			verImportActosColaborador();
			}
		}
	]}
];
function editarAccionMejoraEvento()
{ 
	var listItemsForm=[
			{sugerencia:"",label:"<i class='fa fa-info'></i> Clasificación: ",inputForm: "  ",divContainer:"divClasifAccColaborador"},
			{sugerencia:" ",label:"<i class='fa fa-fire'></i> Descripción: ",inputForm: "<input class='form-control' id='inputDescAccion'>",divContainer:""},
			{sugerencia:"Si ya ha realizado la acción, introduzca la evidencia",label:"<i class='fa fa-calendar'></i> Fecha Planificada: ",inputForm: "<input type='date' class='form-control' id='inputDateAccion'>",divContainer:""},
			{sugerencia:" ",label:"<i class='fa fa-upload'></i> Evidencia : ",inputForm: "<div id='inputEvidenciaAccEvento'></div>",divContainer:""},
			{sugerencia:" ",label:"",inputForm: "<button class='btn btn-success'  ><i class='fa fa-floppy-o'></i> Guardar</button>",divContainer:""}
		];
	var text="";
	listItemsForm.forEach(function(val,index)
	{
		text+=obtenerSubPanelModulo(val);
	});
	var contenido=
		"<form id='formAccEvento' class='eventoGeneral' >" +
			"<div id='tituloEvento'>" + 
				text+
			"</div>"+
			"</div>" +
		"</form>";  
	var listPaneles= 
		 {id:"divNuevoAccMejoraEvento",nombre:"",clase : "listaAuxiliar",contenido:contenido}
	                  ;

	accionMejoraEventoObj = listFullAccionesEvento[accionMejoraEventoColaboradorIndex];
	agregarModalPrincipalColaborador(listPaneles,function(){
		listClasificacionAccionEventoColaborador.forEach(function(val){
			val.selected = 0;
			if(val.id == accionMejoraEventoObj.clasificacion.id){val.selected = 1};
		});
		crearSelectOneMenuObligUnitarioCompleto
		 ("selClasificacionAcc", "",listClasificacionAccionEventoColaborador, "id","nombre",
				 "#divClasifAccColaborador","Todos");
	});
	$("#inputDescAccion").val(accionMejoraEventoObj.descripcion);
	$("#inputDateAccion").val(convertirFechaInput(accionMejoraEventoObj.fechaRevision));
	$("#formAccEvento").on("submit",function(e){
		e.preventDefault();
		guardarAccionMejoraEvento();
	});
	
	var options=
	{container:"#inputEvidenciaAccEvento",
			functionCall:function(){ },
			descargaUrl: "/gestionaccionmejora/accionmejora/evidencia?accionMejoraId="+accionMejoraEventoObj.accionMejoraId,
			esNuevo:false,
			idAux:"AccEvento",
			evidenciaNombre:""+accionMejoraEventoObj.evidenciaNombre};
	crearFormEvidenciaCompleta(options);
	
}
function eliminarAccionMejoraEvento(){
	var r = confirm("¿Está seguro de eliminar esta acción ?");
	if(r){
		accionMejoraEventoObj = listFullAccionesEvento[accionMejoraEventoColaboradorIndex];
		var dataParam=
		{accionMejoraId: accionMejoraEventoObj.accionMejoraId}
		callAjaxPost(URL + '/gestionaccionmejora/accionmejora/delete', dataParam,
				function(data)
				{
			cargarReportesEventoUsuario();
				})
	}
}
function anularEventoInseguroColaborador(){
	var r = confirm("¿Está seguro que este reporte no es válido?");
	if(r){
		$(".divListPrincipal > div").hide();
		$(".divListPrincipal > #subListNoValidoActo").show();
		eventoColaboradorObj = listFullEventosColaborador[indexEventoColaborador]; 
		var btnRegresar="<button class='btn btn-default' onclick='regresarEventosInseguros()' style='font-size:15px;float: left;color:white ;background-color:#008b8b'>" +
		"<i class='fa fa-arrow-left' aria-hidden='true'></i></button>"
		$("#subListNoValidoActo .tituloSubList").html(btnRegresar+" A/C Subestándar No Válido")
		$("#divDescripcionEventoNoValido").html(eventoColaboradorObj.acInseguroNombre);
		$("#inputNoValidoActo").val("")
	}
}
function regresarEventosInseguros()
{
	$(".divListPrincipal > div").show();
	$(".divListPrincipal > .listaAuxiliar").hide();
}
var listFullEventosColaborador = [];
var indexEventoColaborador = 0;
var eventoColaboradorObj = {};
var listCausaInmediataColaborador = [];
var listAreaActoColaborador = [];
var listDivisionActoColaborador = [];
var listDivisionTotalActoColaborador = [];
var listSubTipoContactoColaborador = [];
function marcarSubOpcionAC(pindex,pindex1)
{
	funcionalidadesAC[pindex].opcion[pindex1].functionClick();
	$("#opciones"+pindex+" .listaGestionGosst").hide();
}
var menuOpcionAc=[];
function listarOpcionesAc()
{ 
	menuOpcionAc=[];
	funcionalidadesAC.forEach(function(val1,index1)
		{var textLista="";
			var opciones=val1.opcion;
			textLista="<ul class='list-group listaGestionGosst' style='display:none;'>";
			opciones.forEach(function(val2,index2)
				{
				textLista+="<li class='list-group-item' onclick='marcarSubOpcionAC("+index1+","+index2+")'>"+val2.nombre+"</li>"
			});
			textLista+="</ul>";
			menuOpcionAc.push(textLista);
		});	
}

function habilitarReporteEventoUsuario(){
	$(".divListPrincipal > div").remove();
	var listPaneles=[
				{id:"subListNoValidoActo",nombre:"A/C Subestándar No Válido",contenido:"",clase : "listaAuxiliar"},
				 
				{id:"subImportActosColaborador",nombre:" ",contenido:""},
				{id:"subListNuevoActoCompleto",nombre:"Nuevo A/C Subestándar",contenido:""},
				 {id:"subFiltroActo",nombre:"<i class='fa fa-filter'></i> Filtros",contenido:""},
				  {id:"subListFullActosNoRevisados",nombre:"Por ",contenido:""},
				  {id:"subListFullActos",nombre:"Por ",contenido:""},
				  {id:"subListFullActosNoValidos",nombre:"Por ",contenido:""},
				  {id:"subConsolidadoActos",nombre:"Por ",contenido:""},
				  {id:"subIndicadoresActos",nombre:"Por ",contenido:""},
				  
				         ];
	
	if(numAreasResponsable == 0){
		listPaneles=[
						{id:"subListNoValidoActo",nombre:"A/C Subestándar No Válido",contenido:"",clase : "listaAuxiliar"},
						 {id:"subListNuevoActo",nombre:"Nuevo A/C Subestándar",contenido:""}, 
						  {id:"subListFullActosNoRevisados",nombre:"Por ",contenido:""},
						  {id:"subListFullActos",nombre:"Por ",contenido:""},
						  {id:"subListFullActosNoValidos",nombre:"Por ",contenido:""},
						         ];
	}
	
	if(getSession("gestopcompanyid") == 336 || getSession("gestopcompanyid") == 154){
		listPaneles=[
						{id:"subListNoValidoActo",nombre:"A/C Subestándar No Válido",contenido:"",clase : "listaAuxiliar"},
						 
						{id:"subImportActosColaborador",nombre:" ",contenido:""},
						{id:"subListNuevoActoCompletoPersonal",nombre:"Nuevo A/C Subestándar",contenido:""},
						 {id:"subFiltroActo",nombre:"<i class='fa fa-filter'></i> Filtros",contenido:""},
						  {id:"subListFullActosNoRevisados",nombre:"Por ",contenido:""},
						  {id:"subListFullActos",nombre:"Por ",contenido:""},
						  {id:"subListFullActosNoValidos",nombre:"Por ",contenido:""},
						  {id:"subConsolidadoActos",nombre:"Por ",contenido:""},
						  {id:"subIndicadoresActos",nombre:"Por ",contenido:""},
						  
						         ];
	}
	
				agregarPanelesDivPrincipal(listPaneles);
				$("#subFiltroActo").find(".contenidoSubList").html(
				"<form class='eventoGeneral' >"+
				"<div style='width : 50%;display: inline-block;'><input onchange='cargarReportesEventoUsuario()' type='radio' id='filtroEventoMovil0' name='filtroEventoMovil' value='0'> <label for = 'filtroEventoMovil0'>Ver todos los reportes</label></div>"+
				"<div style='width : 50%;display: inline-block;'><input onchange='cargarReportesEventoUsuario()' type='radio' id='filtroEventoMovil1' name='filtroEventoMovil' value='1' checked> <label for = 'filtroEventoMovil1'>Ver sólo mis reportes</label></div>"+
				 ""+
				"</form>"
				);
				cargarReportesEventoUsuario();
}
var listFullTipoContactoColaborador = [];
function verCategAcordeTipoActoInseguro(){
	var selActoTipo=parseInt($("#selActoTipo").val() );
	var listFinal = []; 
	listCausaInmediataColaborador.forEach(function(val){
		if(val.causaInmTipoEvent == selActoTipo){
			listFinal.push(val);
		}
	});
	crearSelectOneMenuNoObligUnitarioCompleto("selCausaInmediataColaborador", 
			"verAcordeCausaInmediata()", listFinal, "id","nombre",
			"#divCausaInmColaborador","Sin especificar");
	
	verAcordeCausaInmediata();
}
function verAcordeCausaInmediata(){
	var causaId = parseInt( $("#selCausaInmediataColaborador").val() );
	
	$("#inputEspCausaActo").parent().parent().hide();
	if(causaId == 17 || causaId == 34 || causaId == 46 || causaId == 56 || causaId == 71){
		$("#inputEspCausaActo").parent().parent().show();
	}
}
function cargarReportesEventoUsuario(){
	regresarEventosInseguros();
	var radioValue = $("input[name='filtroEventoMovil']:checked").val();
	
	var dataParam2={
			trabajadorId:getSession("trabajadorGosstId"),
			matrixId:getSession("unidadGosstId"),
			companyId:getSession("gestopcompanyid"),
			isTrabajador : (radioValue=="0"?null: 1)
	}
	callAjaxPost(URL + '/acinseguro/movil', dataParam2,procesarCargaReportesEventoUsuario);
}
var listActoTipo=[],listOrigenActo=[],lisTipoReporteLugar=[],listTrabajadoresEvento=[];
var listClasificacionAccionEventoColaborador = [];
function procesarCargaReportesEventoUsuario(data){
	regresarEventosInseguros();
	
	var radioValue = $("input[name='filtroEventoMovil']:checked").val();
	
	listarOpcionesAc();
	 
	 listActoTipo=data.listActInsegTipo;
	 listTrabajadoresEvento = data.trabajadores;
	 listClasificacionAccionEventoColaborador = data.clasificacionAccion;
	listOrigenActo=data.listOrigen;
	lisTipoReporteLugar = data.lisTipoReporteLugar;
	listCausaInmediataColaborador = data.listCausaInmediataSimple;
	listAreaActoColaborador = data.listArea;
	listDivisionActoColaborador = data.divisiones;
	listDivisionTotalActoColaborador = data.divisionesTotal;
	listSubTipoContactoColaborador = data.subTipoContacto;
	listFullEventosColaborador = data.actos;
	listFullTipoContactoColaborador = data.listFullTipoContacto;
	var selActoTipo=crearSelectOneMenuOblig("selActoTipo", "verCategAcordeTipoActoInseguro()", listActoTipo, "", "actTipoId",
			"actTipoName") ;
	var selOrigenActo=crearSelectOneMenuOblig("selOrigenActo", "", listOrigenActo, "", "id",
	"nombre");
	var selTipoReporteLugar=crearSelectOneMenuOblig("selTipoReporteLugar", "", 
			lisTipoReporteLugar, "", "id","nombre");
	var selAreaColaborador=crearSelectOneMenuOblig("selAreaColaborador", "", 
			listAreaActoColaborador, "", "areaId","areaName");
	
	
	
	 //
	
	var textoFormNoVlaido="";
	var listItemsForm=[
	{sugerencia:"",label:"Descripción",inputForm:"",divContainer : "divDescripcionEventoNoValido"},
	{sugerencia:"",label:"Comentario",inputForm:"<input type='text' class='form-control' id='inputNoValidoActo' placeholder=' ' autofocus>"},
	{sugerencia:"",label:"",inputForm:"<button type='submit' class='btn btn-success' ><i aria-hidden='true' class='fa fa-floppy'></i>Enviar</button>"},
	  ];
	listItemsForm.forEach(function(val,index){
		textoFormNoVlaido+=obtenerSubPanelModulo(val);
	});
	
	$("#subListNoValidoActo").find(".contenidoSubList").html(
			"<form class='eventoGeneral' id='noValidoActoTrabajador'>"+
			textoFormNoVlaido+
			 ""+
			"</form>"
	);
	$('#noValidoActoTrabajador').on('submit', function(e) { 
        e.preventDefault(); 
        var motivoValido=$("#inputNoValidoActo").val(); 
        
        var dataParam = {
				acInseguroId : eventoColaboradorObj.acInseguroId,
				motivoValido : motivoValido
			};
        
		callAjaxPost(URL + '/acinseguro/valido/save', dataParam,
					function(data){
			cargarReportesEventoUsuario();
		} );
    });
	//
	var textoForm="";
	var listItemsForm=[
	{sugerencia:"e.g. Personal sin EPP, falta señalización",label:"Descripción",inputForm:"<input type='text' class='form-control' id='inputDescActo' placeholder=' '>"},
	{sugerencia:"",label:"Área de trabajo",inputForm:selTipoReporteLugar},
	{sugerencia:"e.g. Almacen N°5 en las escaleras",label:"Lugar",inputForm:"<input type='text' class='form-control' id='inputDescLugar' placeholder=' '>"},
	{sugerencia:"",label:"Tipo de reporte",inputForm:selActoTipo},
	{sugerencia:"e.g. Señalización temporal, se notificó al supervisor ",label:"Acción de mejora realizada / propuesta por el trabajador",inputForm:"<input type='text' class='form-control' id='inputDescCorrInm' placeholder=' '>"},
	//{sugerencia:"",label:"Observación",inputForm:selOrigenActo},
	{sugerencia:"",label:"Evidencia *",inputForm:" ",divContainer:"eviActo"},
	{sugerencia:"",label:"",inputForm:"<button type='submit' class='btn btn-success' ><i aria-hidden='true' class='fa fa-floppy'></i>Guardar</button>"}
	];
	listItemsForm.forEach(function(val,index){
		textoForm+=obtenerSubPanelModulo(val);
	});
	
	$("#subListNuevoActo").find(".contenidoSubList").html(
			"<form class='eventoGeneral' id='nuevoActoTrabajador'>"+
			textoForm+
			 ""+
			"</form>"
	);
	//
	var textoFormCompleto="";
	var listItemsFormCompleto=[
	{sugerencia:"",label:"Fecha de evento*",inputForm:"<input type='date' class='form-control' id='dateEventoColaborador'>"},
	{sugerencia:"",label:"Área ¿Dónde?*",inputForm:"",divContainer : "divSelAreaActo"},
	{sugerencia:"e.g. Personal sin EPP, falta señalización, ronda",label:"¿Qué operación se realizaba?",inputForm:"<input type='text' class='form-control' id='inputDescActo' placeholder=' ' >"},
	{sugerencia:"",label:"Código de riesgo*",inputForm:"",divContainer: "divSelSubTipoContactoActo"},
	
	{sugerencia:"",label:"Acto/Condición (para marcar)*",inputForm:"",divContainer : "divSelTipoActoColaborador"},
	{sugerencia:"",label:"Causa Raíz (¿Por qué pasó?)*",inputForm:"",divContainer : "divCausaInmColaborador"},
	//{sugerencia:"e.g. Almacen N°5 en las escaleras",label:"Lugar",inputForm:"<input type='text' class='form-control' id='inputDescLugar' placeholder=' '>"},
	{sugerencia:"",label:"Especificar Causa Raíz",inputForm:"<input type='text' class='form-control' id='inputEspCausaActo' placeholder=' ' >"},
	{sugerencia:"e.g. Señalización temporal, se notificó al supervisor ",label:"Acción Correctiva (Propuesta del trabajador)",inputForm:"<input type='text' class='form-control' id='inputDescCorrInm' placeholder=' '>"},
	
	//{sugerencia:"",label:"Observación",inputForm:selOrigenActo},
	{sugerencia:"",label:"Evidencia",inputForm:" ",divContainer:"eviActo"},
	{sugerencia:"",label:"",inputForm:"<button type='submit' class='btn btn-success' ><i aria-hidden='true' class='fa fa-floppy'></i>Guardar</button>"}
	];
	listItemsFormCompleto.forEach(function(val,index){
		textoFormCompleto+=obtenerSubPanelModulo(val);
	});
	$("#subListNuevoActoCompleto").find(".contenidoSubList").html(
			"<form class='eventoGeneral' id='nuevoActoCompletoTrabajador'>"+
			textoFormCompleto+
			 ""+
			"</form>"
	);
	//
	var textoFormCompletoPersonal="";
	var listItemsFormCompletoPersonal=[
	{sugerencia:"",label:"Fecha de evento*",inputForm:"<input type='date' class='form-control' id='dateEventoColaborador'>"},
	{sugerencia:"",label:"Hora de evento*",inputForm:"<input type='time' class='form-control' id='timeEventoColaborador'>"},
	{sugerencia:"",label:"<i class='fa fa-search'></i>Buscar trabajador",inputForm:"<input placeholder='' class='form-control' id ='inputAutoTrabajador'>",divContainer:""},
	{sugerencia:"",label:"Trabajador reporta*",inputForm:"",divContainer:"divAutoCompletarTrab"},
	{sugerencia:"",label:"Área ¿Dónde Ocurrió?*",inputForm:"",divContainer : "divSelAreaActo"},
	{sugerencia:"e.g. Almacen N°5 en las escaleras",label:"Lugar",inputForm:"<input type='text' class='form-control' id='inputDescLugar' placeholder=' '>"},
	{sugerencia:"",label:"Acto/Condición (para marcar)*",inputForm:"",divContainer : "divSelTipoActoColaborador"},
	{sugerencia:"e.g. Personal sin EPP, falta señalización, ronda",label:"¿Cuál es?",inputForm:"<input type='text' class='form-control' id='inputDescActo' placeholder=' ' >"},
	{sugerencia:"",label:"Código de riesgo*",inputForm:"",divContainer: "divSelSubTipoContactoActo"},
	
	{sugerencia:"",label:"Causa Raíz (¿Por qué pasó?)*",inputForm:"",divContainer : "divCausaInmColaborador"},
	{sugerencia:"",label:"Especificar Causa Raíz",inputForm:"<input type='text' class='form-control' id='inputEspCausaActo' placeholder=' ' >"},
	{sugerencia:"e.g. Señalización temporal, se notificó al supervisor ",label:"Acción inmediata (Qué hiciste)",inputForm:"<input type='text' class='form-control' id='inputDescCorrInm' placeholder=' '>"},
	
	//{sugerencia:"",label:"Observación",inputForm:selOrigenActo},
	{sugerencia:"",label:"Evidencia",inputForm:" ",divContainer:"eviActo"},
	{sugerencia:"",label:"",inputForm:"<button type='submit' class='btn btn-success' ><i aria-hidden='true' class='fa fa-floppy'></i>Guardar</button>"}
	];
	listItemsFormCompletoPersonal.forEach(function(val,index){
		textoFormCompletoPersonal+=obtenerSubPanelModulo(val);
	});
	$("#subListNuevoActoCompletoPersonal").find(".contenidoSubList").html(
			"<form class='eventoGeneral' id='nuevoActoCompletoPersonalTrabajador'>"+
			textoFormCompletoPersonal+
			 ""+
			"</form>"
	);
	
	
	listAreaActoColaborador.forEach(function(val){
		val.selected = 0;
		if(val.areaId == parseInt(getSession("areaGosstId") )){
			//val.selected = 1;
		}
	});
	$("#subListNuevoActoCompletoPersonal .contenidoSubList").show();
	$("#subListNuevoActoCompleto .contenidoSubList").show();
	crearSelectOneMenuNoObligUnitarioCompleto("selAutoTrabajador", 
			getSession("trabajadorGosstId"), listTrabajadoresEvento, "trabajadorId","nombre",
			"#divAutoCompletarTrab","");
	$("#inputAutoTrabajador")
	.attr("onkeyup","verAutoCompletarTrabAcInseguro()");
	crearSelectOneMenuObligUnitarioCompleto("selActoTipo", 
			"verCategAcordeTipoActoInseguro()", listActoTipo, "actTipoId","actTipoName",
			"#divSelTipoActoColaborador","Sin especificar");
	crearSelectOneMenuNoObligUnitarioCompleto("selAreaColaborador", "", listAreaActoColaborador, "areaId","areaName",
			"#divSelAreaActo","Sin especificar");
	crearSelectOneMenuNoObligUnitarioCompleto("selSubTipoContactoColaborador", "", listSubTipoContactoColaborador, "id","nombreAux",
			"#divSelSubTipoContactoActo","Sin especificar");
	  verCategAcordeTipoActoInseguro();
	$("#subListNuevoActoCompletoPersonal .contenidoSubList").hide();
	$("#subListNuevoActoCompleto .contenidoSubList").hide();
	//
	  $('#nuevoActoTrabajador').on('submit', function(e) { //use on if jQuery 1.7+
	        e.preventDefault();  //prevent form from submitting
	       guardarActoTrabajador();
	    });
	  $('#nuevoActoCompletoTrabajador').on('submit', function(e) { //use on if jQuery 1.7+
	        e.preventDefault();  //prevent form from submitting
	       guardarActoCompletoTrabajador();
	    });

	  $('#nuevoActoCompletoPersonalTrabajador').on('submit', function(e) { //use on if jQuery 1.7+
	        e.preventDefault();  //prevent form from submitting
	       guardarActoCompletoTrabajador();
	    });
	  
	  verAcordeCausaInmediata();
	  
	var options=
	{container:"#eviActo",
			functionCall:function(){ },
			descargaUrl: "",
			esNuevo:true,
			idAux:"EventoSub",
			evidenciaNombre:""};
	crearFormEvidenciaCompleta(options);
	var textActosRevisados="",textActosNoRevisados="",textActosNoValidos="";
	var numActRevisados=0,numActNoRevisados=0,numActNoValidos=0;
	listFullEventosColaborador.forEach(function(val,index){
		var accion="";
		var noticia="<div class='cropGosst' id='imgVer"+val.acInseguroId+"' style='display:none;''><img src='../imagenes/preview.png'></div>";
		
		var hasAcciones=false;
		if(val.solicitudAccion!=null){
			if(val.solicitudAccion.acciones.length>0){
				hasAcciones=true;
			}
		}
		if(val.numAccionesColaboradorTotal > 0){ 
			hasAcciones=true;
			val.solicitudAccion = {acciones : []};
		}
		var textoAcciones = "";
		if(hasAcciones && val.tipoValido.id != 2){
			var tablaTotalAcciones="Sin acciones asociadas";
			if(val.solicitudAccion.acciones.length>0){
				tablaTotalAcciones="";
			}
			val.solicitudAccion.acciones.forEach(function(val1,index1){
				tablaTotalAcciones+=
					"" +
					"<div class='detalleAccion'> " +
					"<section    id='opciones1'  >" +
					"<a class='efectoLink btn-gestion' onclick=' toggleMenuOpcionAC(this,"+val1.accionMejoraId+",1)'>" +
						"Ver Opciones<i class='fa fa-angle-double-down' aria-hidden='true'></i>" +
					"</a>"+menuOpcionAc[1]+
					"</section>"+
				
					"<i class='fa fa-calendar'></i>"+val1.fechaRevisionTexto+"<br>"+
					"<i class='fa fa-minus-square'></i>"+val1.descripcion+"" +
					"</div>";
			});
			accion="" +
			""+"<i aria-hidden='true' class='fa fa-star'></i> "+"</td>" + 
			"<a class='efectoLink' onclick='verDetalleAccionActo(this)'>  "+
			val.nombreAccionMejora+" ("+val.solicitudAccion.acciones.length+")<i class='fa fa-angle-double-down'></i>"+"</a>" +
					
				"<div class='divAccionesSubMovil'> "+tablaTotalAcciones+" </div>" +
				"" ;
			//
			 textoAcciones = "<br><a class='efectoLink' " +
					"onclick='verAccionesMejoraEventoInseguro("+index+")' >" +
							"Ver Acciones de Mejora ("+val.numAccionesColaboradorCompletadas+" / "+val.numAccionesColaboradorTotal+") " +
									"<i class='fa fa-angle-double-down'></i></a>" +
									"<div class='divMovilHistorialTrabajador' id='detalleAccionEvento"+val.acInseguroId+"' ></div>"
			//
		}
		var textRiesgo =  "<i aria-hidden='true' class='fa fa-warning'></i>" +"" +
 		"<strong >Riesgo asociado :  </strong>" +  val.subTipoContacto.nombre  +"<br>";
		var textLugar ="";
		textLugar = "<i class='fa fa-info'></i>" +
		"<strong >" + "Lugar :</strong>  "+val.reporteLugar+"<br>";
		if(numAreasResponsable == 0){
			textRiesgo= "";
			
			textoAcciones = "";
		}
		var textMotivo = "";
		var textVerOpciones = "<section style='text-align: left;' id='opciones0'>"+
		
			"<a class='efectoLink btn-gestion' onclick='indexEventoColaborador = "+index+";toggleMenuOpcionAC(this,"+val.acInseguroId+",0)'>" +
			"Ver Opciones<i class='fa fa-angle-double-down' aria-hidden='true'></i>" +
			"</a>"+menuOpcionAc[(hasAcciones?2:0)]+
		"</section>" ;
		if(val.tipoValido.id == 2){
			textMotivo = "<i class='fa fa-info'></i><strong ><label style='color : red' >Motivo: "+ val.motivoValido+" </label>";
			textVerOpciones = "";
		} 
		var textActos="<div class='eventoGeneral' id='actoMovil"+val.acInseguroId+"' style='border-left:8px solid "+val.nivelColor+";   '>" +
		"<div id='tituloEvento'>" +
		
		textVerOpciones+
		 " " +"" +
	 		"<strong ># CÓDIGO :  </strong>" + val.codigo +"<br>"+
		 "<i aria-hidden='true' class='fa fa-calendar'></i>" +"" +
	 		"<strong >Fecha:  </strong>" + val.reporteFechaTexto +"<br>"+
	 		"<i aria-hidden='true' class='fa fa-clock-o'></i><strong >Hora:  </strong>" + val.horaTexto +"<br>"+
	 		"<i class='fa fa-info'></i>" +
			"<strong >" + "Área dónde ocurrió:</strong> "+val.areaNombre +" <br>"+
			
		"<i aria-hidden='true' class='fa fa-info'></i>" +
		"<strong>Descripción / Operación realizada:  </strong>   ("+val.nivelRiesgoNombre+") "+val.acInseguroNombre+"<br>"+
			
		textRiesgo+
	 		
		"<i aria-hidden='true' class='fa fa-info'></i>" +"" +
 		"<strong >Tipo :  </strong>" +  val.acInseguroTipoNombre  +"<br>"+
 			
			 
 		textLugar+
		
		
		"<i class='fa fa-picture-o'></i>" +
		"<strong>Evidencia:  </strong>  <a onclick='verImagenActoMovil("+val.acInseguroId+");' class='efectoLink'>"+val.evidenciaNombre+" </a><br>"+
		"<i class='fa fa-user'></i>" +
		"<strong>Reportado por:  </strong>    "+val.reporteNombre+"<br>"+
		 
		textoAcciones+
		textMotivo+
		
		"<tr>" +
		"<td colspan=3>"+noticia+"</td>" +
		"</tr>"+
		"<tr>" +
		"<td style='font-size: 24px'> "+"  "+"</td>" +  
		"</tr>" +
		"</tbody>" +
		"</table>"+ 
		"</div>"+
		"</div>";
		if(val.tipoValido.id == 2){
			numActNoValidos++;
			textActosNoValidos+=textActos;
		}else if(hasAcciones){
			numActRevisados++;
			textActosRevisados+=textActos;
		}else{
			numActNoRevisados++;
			textActosNoRevisados+=textActos;
		}
	})
var textVerOpciones = "<section style='text-align: left;' id='opciones4'>"+
		
			"<a class='efectoLink btn-gestion' onclick='toggleMenuOpcionAC(this,0,4)'>" +
			"Ver Opciones<i class='fa fa-angle-double-down' aria-hidden='true'></i>" +
			"</a>"+menuOpcionAc[4]+
		"</section>" ;
 
	
	$("#subImportActosColaborador .tituloSubList").html(textVerOpciones+"<i aria-hidden='true' class='fa fa-upload'></i> Importar A/C subestándar")
	$("#subListFullActos").find(".tituloSubList").html("<i aria-hidden='true' class='fa fa-warning'></i>Reportes revisados ("+numActRevisados+") "+textoBotonToggleContenidoColaborador);
	$("#subListFullActos").find(".contenidoSubList").html(
			textActosRevisados
	);
	$("#subListFullActosNoRevisados").find(".tituloSubList").html("<i aria-hidden='true' class='fa fa-warning'></i>Reportes sin revisar ("+numActNoRevisados+")"+textoBotonToggleContenidoColaborador);
	$("#subListFullActosNoRevisados").find(".contenidoSubList").html(
			textActosNoRevisados
	);
	$("#subListFullActosNoValidos").find(".tituloSubList")
	.html("<i aria-hidden='true' class='fa fa-warning'></i>Reportes revisados clasificados como no Válidos ("+numActNoValidos+")"+textoBotonToggleContenidoColaborador);
	$("#subListFullActosNoValidos").find(".contenidoSubList").html(
			textActosNoValidos
	);
	$("#subListNuevoActo").find(".contenidoSubList").hide();
	$("#subListNoValidoActo").hide();
	var btnConsolidado = "<button onclick='verTablaConsolidadoActos()' class='btn btn-success'>" +
			"<i class='fa fa-2x fa-arrow-right'></i></button>";
	$("#subConsolidadoActos .tituloSubList")
	.html("<i aria-hidden='true' class='fa fa-table'></i> Ver Consolidado"+btnConsolidado);
	
	var btnIndicador = "<button onclick='verIndicadoresConsolidadoActos()' class='btn btn-success'>" +
	"<i class='fa fa-2x fa-arrow-right'></i></button>";
$("#subIndicadoresActos .tituloSubList")
.html("<i aria-hidden='true' class='fa fa-pie-chart'></i> Ver Indicadores"+btnIndicador);
	if(radioValue == "1"){
		
	}else{
		$("#liMenuTrabajador10 .alertaMenu").html(numActNoRevisados);
	}
	
	
	$("#subListFullActosNoValidos .tituloSubList").css({"color":colorPuntajeGosst(0.5)+" !important"});
	$("#subListFullActos .tituloSubList").css({"color":colorPuntajeGosst(1.0)+" !important"});
	$("#subListFullActosNoRevisados .tituloSubList").css({"color":colorPuntajeGosst(0.0)+" !important"});
	
	//$("#dateEventoColaborador").val(obtenerFechaActual());
	//$("#timeEventoColaborador").val(obtenerHoraActual(1));
	//.focus()
	
} 
function verAutoCompletarTrabAcInseguro(){
	var nombre = $("#inputAutoTrabajador").val().trim();
	if(nombre == ""){nombre = null};
	var dataParam = {
			trabajadorNombre : "%"+nombre+"%",
			mdfId:getSession("unidadGosstId"),
			empresaId : getSession("gestopcompanyid")}
	callAjaxPostNoLoad(URL+"/trabajador/buscar",dataParam,function(data){
		if(data.trabajadores.length == 0){
			data.trabajadores.push({
				trabajadorId : -1,nombre:"Sin asignar"
			})
		}
		crearSelectOneMenuObligUnitarioCompleto("selAutoTrabajador", 
				"", data.trabajadores, "trabajadorId","nombre",
				"#divAutoCompletarTrab","");

		$("#inputAutoTrabajador")
		 
		.attr("onkeyup","verAutoCompletarTrabAcInseguro()");
	});
}
function verPlantillaImportActosColaborador(){
	window.open(URL+"/acinseguro/plantilla/colaborador?empresaId=" +getSession("gestopcompanyid")+"&"+
			"unidadId="+getSession("unidadGosstId"),"_blank");
}
function verImportActosColaborador(){
	var listItemsForm=[
	       			{sugerencia:"",label:"<i class='fa fa-info'></i> Achivo Excel: ",inputForm: "",divContainer:"divExcelImport"},
	       			 {sugerencia:" ",label:"",inputForm: "<button class='btn btn-success'  ><i class='fa fa-floppy-o'></i> Guardar</button>",divContainer:""}
	       		];
	       	var text="";
	       	listItemsForm.forEach(function(val,index)
	       	{
	       		text+=obtenerSubPanelModulo(val);
	       	});
	       	var contenido=
	       		"<form id='formImportEvento' class='eventoGeneral' >" +
	       			"<div id='tituloEvento'>" + 
	       				text+
	       			"</div>"+
	       			"</div>" +
	       		"</form>";  
	       	var listPaneles= 
	       		 {id:"divImportEventoColaborador",nombre:"Importar A/C subestándar supervisor",clase : "listaAuxiliar",contenido:contenido}
	       	                  ;
	agregarModalPrincipalColaborador(listPaneles,function(){
		var options=
		{container:"#divExcelImport",
				functionCall:function(){enviarExcelImportActos() },
				descargaUrl: "",
				esNuevo:true,
				idAux:"ImportExcel",
				evidenciaNombre:""};
		crearFormEvidenciaCompleta(options);
	});
	$("#formImportEvento").on("submit",function(e){
		e.preventDefault();
		enviarExcelImportActos();
	});
}
function enviarExcelImportActos(){
	guardarEvidenciaAuto(0,"fileEviImportExcel",
			bitsEvidenciaActoInseguro,
			"/acinseguro/excel/masivo/save",function(){
		alert("Import completado!");
		$(".modal").modal("hide");
		cargarReportesEventoUsuario();
	},"id",[{id:"trabajadorId",value:getSession("trabajadorGosstId")},
	        {id:"unidadId",value:getSession("unidadGosstId")},
	        {id : "empresaId",value:getSession("gestopcompanyid")}]);
}
function guardarImportEventoColaborador(){
	var numColumnas = 12;
	
	var listExcel = obtenerListaImportGosst("#textImportActo",numColumnas);
	var validado = "";
	 
	var listActosImport = [];
	listExcel.forEach(function(val,index){
		var listCells=val.split("\t");
		if(listCells.length!=numColumnas)
		{
			validado="No coincide el numero de las celdas ->> " +
					"("+listCells.length+"!="+numColumnas+")";		
			 
		}
		else
		{
			var acto = {};
			listCells.forEach(function(val1,index1){
				val1 = val1.trim();
				switch(index1){
				case 0:
					break;
				case 1:
					break;
				case 2:
					try {
					var fechaRegistro = new Date("20"+val1,listCells[1]-1,listCells[0]);
					acto.reporteFecha = fechaRegistro;
					if(fechaRegistro == null){
						validado = "Error en el formato de fecha de registro.";
					}
					break;
					} catch (err) {
						validado = "Error en el formato de fecha de registro.";
						break;
					}
				case 3:
					acto.areaId = 0;
					listAreaActoColaborador.forEach(function(val2){
						if(val2.areaNameShort.toUpperCase() == val1.toUpperCase()){
							acto.areaId = val2.areaId;
						}
					});
					if(acto.areaId == 0){
						validado = "Area no reconocida: "+val1;
					}
					break;
				case 4:
					acto.acInseguroNombre = val1;
					break;
				case 5:
					break;
				case 6: 
					var riesgoId = 0;
					listFullTipoContactoColaborador.forEach(function(val2){
						if(val2.nombre.toUpperCase().trim() == val1.toUpperCase().trim()){
							riesgoId = val2.id;
						}
					});
					if(riesgoId == 0){
						validado = "Riesgo no reconocido: "+val1;
					}
					acto.tiposContacto = [{id : riesgoId}]
					break;
				case 7: 
					break;
				case 8:
					 acto.acInseguroTipoId = 1;
					if(val1.toUpperCase() == "X"){
						acto.acInseguroTipoId = 1;
					}
					break;
				case 9:
					if(val1.toUpperCase() == "X"){
						acto.acInseguroTipoId = 2;
					}
					break;
				case 10:
					if(val1.length > 0){
						acto.acInseguroNombre += " / Causa: "+val1;
					}
					break;
				case 11:
					acto.reporteAccion = val1;
					break;
					
				}
			});
		}
		acto.acInseguroCausaId = 17;
		if(acto.acInseguroTipoId == 2){
			acto.acInseguroCausaId = 34;
		}
		acto.nivelRiesgoId = 2;
		acto.reporteNombre = getSession("gestopusername");
		acto.trabajadorId = getSession("trabajadorGosstId");
		acto.matrixId = getSession("unidadGosstId");
		listActosImport.push(acto);
	});
	if (validado.length < 1) {
		//return;
		var r = confirm("¿Está seguro que desea subir estos "
				+ listExcel.length + " elemento(s)?");
		if (r == true) {
			 
		 callAjaxPost(URL + '/acinseguro/colaborador/masivo/save', listActosImport,
		 		function(data){
			 $(".modal").modal("hide");
			 alert("Importación guardada");
			 cargarReportesEventoUsuario();
		 });
		}
	} else {
		alert(validado);
	} 
}
function guardarActoTrabajador(){
	 var descripcion=$("#inputDescActo").val();
	 var detalleCausa = $("#inputEspCausaActo").val();
     var corrInmediata=$("#inputDescCorrInm").val();
     var lugar=$("#inputDescLugar").val();
     var selActTipo=$("#selActoTipo").val();
     var origenActo=$("#selOrigenActo").val();
     var tipoReporteLugar=parseInt( $("#selTipoReporteLugar").val() );
     var trabajadorReporta = parseInt($("#selAutoTrabajador").val());
     if( trabajadorReporta == 0){ trabajadorReporta =null};
     var dataParam = {
				acInseguroId : 0,
				origen:{id:origenActo},
				detalleCausa : detalleCausa,
				tipoLugar : {id : tipoReporteLugar},
				areaId : (tipoReporteLugar ==2?null:getSession("areaGosstId") ),
				nivelRiesgoId:2,
				acInseguroTipoId : selActTipo,
				acInseguroCausaId: 17,
				acInseguroNombre : descripcion,
				reporteAccion:corrInmediata,
				reporteLugar : lugar,
				reporteTipo : 1,
				trabajadorId:getSession("trabajadorGosstId"),
				reporteNombre : getSession("gestopusername"),
				reporteFecha : new Date(),
				 
				trabajadores : [],
				matrixId : getSession("unidadGosstId")
			};
     var existe=comprobarFieInputExiste("fileEviEventoSub");
     if(!existe.isValido){
     	alert(existe.mensaje);
     	return;
     }
		callAjaxPost(URL + '/acinseguro/save', dataParam,
					function(data){
			guardarEvidenciaAuto(data.idNuevo,"fileEviEventoSub",
					bitsEvidenciaActoInseguro,
					"/acinseguro/evidencia/save",function(){
				$.blockUI({
					message : '<i class="fa fa-check-square" style="color:green" aria-hidden="true"></i> Guardado '
				});
				$(".divListPrincipal").html(""); 
				verAlertaSistemaGosst(4,"El nuevo registro fue enviado correctamente."); 
				setTimeout(function(){habilitarReporteEventoUsuario();},500);
			},"acInseguroId");
		},function(){
				$.blockUI({
					message:'<img src="../imagenes/gif/ruedita.gif"></img>Guardando Información'});
			});
}
function guardarActoCompletoTrabajador(){
	 var fecha = convertirFechaTexto($("#dateEventoColaborador").val());
	 var hora = $("#timeEventoColaborador").val();

    if(fecha == null){
    	alert("Fecha obligatoria"); return ;
    }
    if(hora == ""){
    	alert("Hora obligatoria"); return ;
    }
    var trabajadorReporta = parseInt($("#selAutoTrabajador").val());
    if( trabajadorReporta == -1){alert("Trabajador reporta Obligatorio");return; trabajadorReporta =getSession("trabajadorGosstId")};
	 
	var areaId=parseInt( $("#selAreaColaborador").val() );
	if(areaId == -1){alert("Área obligatoria"); return;};
	
  	var descripcion=$("#inputDescActo").val().trim();
  	if(descripcion.length == 0){
  		alert("Descripción obligatoria");return;
  	}
  	
  	var detalleCausa = $("#inputEspCausaActo").val();
  	
  	var subTipoContactoId = $("#selSubTipoContactoColaborador").val();
  	if(subTipoContactoId == -1){alert("Código riesgo obligatorio"); return;};
  	var selActTipo=$("#selActoTipo").val();
  	var causaId = $("#selCausaInmediataColaborador").val();
  	if(causaId == -1){alert("Causa raíz obligatoria"); return;};
     // var lugar=$("#inputDescLugar").val();
   // var origenActo=$("#selOrigenActo").val();
    var corrInmediata=$("#inputDescCorrInm").val();

    var lugar=$("#inputDescLugar").val();
     var dataParam = {
				acInseguroId : 0, hora:hora,reporteLugar:lugar,
				//subTipoContacto : {id : subTipoContactoId},
				tiposContacto : [{id : subTipoContactoId}],
				origen:{id:3},
				detalleCausa : detalleCausa,
				tipoLugar : {id : (parseInt(getSession("areaGosstId")) == areaId?1:2)},
				areaId : areaId,
				nivelRiesgoId:2,
				acInseguroTipoId : selActTipo,
				acInseguroCausaId: causaId,
				acInseguroNombre : descripcion,
				reporteAccion:corrInmediata,
				//reporteLugar : lugar,
				reporteTipo : 1,
				reporteNombre : getSession("gestopusername"),
				reporteFecha : fecha,
				trabajadorId: trabajadorReporta,
				trabajadorRegistraId : getSession("trabajadorGosstId"),
				trabajadores : [],
				matrixId : getSession("unidadGosstId")
			};
    var existe=comprobarFieInputExiste("fileEviEventoSub");
    if(!existe.isValido){
    	//alert(existe.mensaje);
    	//return;
    }
		callAjaxPost(URL + '/acinseguro/save', dataParam,
					function(data){
			guardarEvidenciaAuto(data.idNuevo,"fileEviEventoSub",
					bitsEvidenciaActoInseguro,
					"/acinseguro/evidencia/save",function(){
				$.blockUI({
					message : '<i class="fa fa-check-square" style="color:green" aria-hidden="true"></i> Guardado '
				})
				verAlertaSistemaGosst(4,"El nuevo registro fue enviado correctamente."); 
				setTimeout(function(){
					var dataAux = {};
					dataAux.divisiones = listDivisionActoColaborador;
					dataAux.divisionesTotal = listDivisionTotalActoColaborador;
					dataAux.listActInsegTipo= listActoTipo;
					dataAux.trabajadores = listTrabajadoresEvento;
					dataAux.listOrigen =listOrigenActo;
					dataAux.lisTipoReporteLugar = lisTipoReporteLugar ;
					dataAux.clasificacionAccion =listClasificacionAccionEventoColaborador;
					dataAux.listCausaInmediataSimple = listCausaInmediataColaborador;
					dataAux.listArea =listAreaActoColaborador  ;
					dataAux.subTipoContacto = listSubTipoContactoColaborador ;
					var listFinal = [];var isFound = false;
					listFullEventosColaborador.forEach(function(val){
						if(val.acInseguroId == data.nuevoActo.acInseguroId){
							listFinal.push(data.nuevoActo);
							isFound = true;
						}else{
							listFinal.push(val);
						}
					});
					if(!isFound){
						listFinal = [data.nuevoActo].concat(listFinal);
					}
					dataAux.actos = listFinal ;
					dataAux.listFullTipoContacto = listFullTipoContactoColaborador ;
					
					procesarCargaReportesEventoUsuario(dataAux);
					//habilitarReporteEventoUsuario();
					$.unblockUI();
				},300);
			},"acInseguroId");
		},function(){
				$.blockUI({
					message:'<img src="../imagenes/gif/ruedita.gif"></img>Guardando Información'});
			});
}
function volverVistaActos(){
	$("#divActoResponsableTabla").remove();
	
	$(".divContainerGeneral").show(); 
	$(".divListPrincipal > div").show();
	
	$("#subListNoValidoActo").hide();
	$(".divTituloGeneral h4").html(tituloAnteriorColaborador)
}
function iniciarFiltroActosResponsable(){
	$(".camposFiltroTabla").show();
	
	$("#labelIndicadorActosResponsable").hide();
	$("#selTipoIndicadorActos").hide();
	$("#wrapperConsolidadoActosResponsable").hide();
	$("#wrapperGraficoActosResponsable").hide();
	
	$("#btnIniciarFiltro").hide();
	$("#btnAplicarFiltro").show();
	$("#buttonVolverFiltro").show();
	$("#btnVolverDetalle").hide();
}
function reporteFiltroActosResponsable(){
	var listaActosId="";
	var numInvalidos=0; 
	listFullActosConsolidado.forEach(function(val,index){
		 
		listaActosId=listaActosId+val.acInseguroId+","
	});
		
	window.open(URL
			+ "/acinseguro/reporte/excel?listActos="
			+ listaActosId + '&conEvidencia=0');	
	
	
	 
}
var listFullActosConsolidado = [];
function verTablaConsolidadoActos(){
	$("#wrapperConsolidadoActosResponsable").remove();
	$(".divContainerGeneral").hide();  
	var buttonVolver="<button type='button' id='btnVolverDetalle' class='btn btn-success ' style='margin-right:10px'" +
			" onclick='volverVistaActos()'>" +
						"<i class='fa fa-sign-out fa-rotate-180'></i>Volver</button>";
	var buttonVolverFiltro="<button type='button' id='buttonVolverFiltro' class='btn btn-success ' style='margin-right:10px'" +
	" onclick='volverVistaIndicadorActos()'>" +
				"<i class='fa fa-sign-out fa-rotate-180'></i>Volver</button>";
	var buttonFiltro="<button type='button' class='btn btn-info ' id='btnIniciarFiltro' style='margin-right:10px'" +
	" onclick='iniciarFiltroActosResponsable()'>" +
				"<i class='fa fa-filter'></i>Ver filtros</button>";
	var buttonExcel="<button type='button' class='btn btn-excel' id='btnReporteFiltro' style='color:white ; margin-right:10px'" +
	" onclick='reporteFiltroActosResponsable()'>" +
				"<i class='fa fa-file-excel-o'></i>Generar reporte</button>";
	var buttonFiltroAplicar="<button type='button' class='btn btn-info ' id='btnAplicarFiltro' style='margin-right:10px'" +
	" onclick='aplicarFiltroActoResponsable()'>" +
				"<i class='fa fa-filter'></i>Aplicar filtros</button>";
	var buttonFiltroRefresh="<button type='button' class='btn btn-success ' id='btnReiniciarFiltro' style='margin-right:10px'" +
	" onclick='reiniciarFiltroActoResponsable()'>" + 
				"<i class='fa fa-refresh'></i>Refresh</button>";
	var textoDivision = "",textoArea="";
	if(numAreasSupervisor > 0){
		textoDivision = "Solo puede ver "+getSession("gestopdivision")
		+" de "+getSession("gestoparea")+" supervisadas";
		textoArea = "Solo puede ver "+getSession("gestoparea")+" supervisadas";
	}
	if(numAreasJefe > 0 || numAreasGerente > 0){
		textoDivision="Puede ver todo "+getSession("gestopdivision")+" de su "+getSession("gestopunidad");
		textoArea="";
	}
	var listIsTrabajador = [{id:0,nombre:"Reportes por áreas asignadas"},{id:1,nombre:"Reportes que generé"}]
	var selIsTrabajadorConsolidado = crearSelectOneMenuOblig("selIsTrabajadorConsolidado", "", listIsTrabajador, "0", "id",
			"nombre");
	$("body")
	.append(
			"<div id='divActoResponsableTabla' style='padding-left: 40px;'>" +
			"<form class='form-inline' id='formBtnGeneral'>"+
			buttonVolver+buttonVolverFiltro+
			buttonFiltroRefresh+buttonFiltroAplicar+buttonFiltro+buttonExcel+"</form>"+
			
			"<div class='camposFiltroTabla' style='width : 500px ; max-width: 100%'>"+
			 
					"<div class='contain-filtro' id=''>" +
					
					"<label for=''  >Fecha Inicio "+
					"</label>" +
					"<div id=''> <input type='date' class='form-control' id='fechaInicioFiltroActo'>" +
					"</div>" + 
				"</div>"+
				"<div class='contain-filtro' id=''>" +
				
				"<label for=''  >Fecha Fin "+
				"</label>" +
				"<div id=''> <input type='date' class='form-control' id='fechaFinFiltroActo'>" +
				"</div>" + 
			"</div>"+
			
"<div class='contain-filtro' id='filtroTipoReporteActos'>" +
			
			"<label for='filtroTipoReporteActos'  > "+"Tipo de consolidado:"+
			"</label>" +
			selIsTrabajadorConsolidado+
			"<small>"+""+"</small>"+
		"</div>"+
			"<div class='contain-filtro' id='filtroDivisionActosResponsable'>" +
			
			"<label for='filtroDivisionActosResponsable'  > "+getSession("gestopdivision")+
			"</label>" +
			"<div id='divSelectDivisionFiltroActos'> " +
			"</div>" +
			"<small>"+textoDivision+"</small>"+
		"</div>"+
		
					"<div class='contain-filtro' id='filtroAreaActosResponsable'>" +
						
						"<label for='filtroAreaActosResponsable'  > "+getSession("gestoparea")+
						"</label>" +
						"<div id='divSelectAreaFiltroActosResponsable'> " +
						"</div>" + 
						"<small>"+textoArea+"</small>"+
					"</div>"+
					
				
					"<div class='contain-filtro' id='filtroSubTipoContResponsable'>" +
					
					"<label for='filtroSubTipoContResponsable'  >Riesgos Asociados"+
					"</label>" +
					"<div id='divSelectSubTipoContactoFiltro'> " +
					"</div>" + 
				"</div>"+ 
			
					"<div class='contain-filtro' id='filtroCausaActosResponsable'>" +
				
						"<label for='filtroCausaActosResponsable'  >Causas Inmediatas"+
						"</label>" +
						"<div id='divSelectCausaFiltroActosResponsable'> " +
						"</div>" + 
					"</div>"+
					"<div class='contain-filtro' id='filtroTipoActosResponsable'>" +
					
					"<label for='filtroTipoActosResponsable'  >Tipo"+
					"</label>" +
					"<div id='divSelectTipoFiltroActosResponsable'> " +
					"</div>" + 
				"</div>"+
					
				" </div>" +
				"<small >Para visualizar actos de otras áreas, modificar el filtro</small>"+
				
				 
				 
				"<div class='wrapper' id='wrapperConsolidadoActosResponsable'>" +
			"<table  style='min-width: 100% ' " +
			"id='tblConsolidadoActo' class='table table-striped table-bordered table-hover table-fixed'>" +
			"<thead>" +
				 "<tr>" +
				 "<td class='tb-acc' style='width : 180px' >Fecha reporte</td>" +
				 "<td class='tb-acc' style='width : 180px' >Hora reporte</td>" +
				 "<td class='tb-acc' style='width : 180px' >"+getSession("gestopdivision")+"</td>" + 
				 "<td class='tb-acc' style='width : 180px' >"+getSession("gestoparea")+"</td>" + 
				 "<td class='tb-acc' style='width : 180px' >Lugar</td>" +
				 "<td class='tb-acc' style='width : 180px' >Trabajador reporta</td>" +
				 "<td class='tb-acc' style='width : 180px' >¿Qué Operación se realizaba?</td>" +
				 "<td class='tb-acc' style='width : 180px' >¿Qué Acción inmediata se hizo?</td>" +
				 "<td class='tb-acc' style='width : 180px' >Riesgos Asociados</td>" + 
				 "<td class='tb-acc' style='width : 130px' >Tipo</td>" +
				 "<td class='tb-acc' style='width : 180px' >Causa Inmediata</td>" +
				 
				"<td class='tb-acc' style='width : 190px' >Evidencia</td>" +
				"<td class='tb-acc' style='width : 150px' >Acciones de mejora</td>" +
				
						
				 "</tr>"+
			"</thead>" +
			"<tbody></tbody>" +
			"</table>" +
			"</div>" +
			" </div>");
	
	reiniciarFiltroActoResponsable();
	
	  $("#divTrabActosResponsable form").on("submit",function(e){
			e.preventDefault(); 
		}); 
	tituloAnteriorColaborador=$(".divTituloGeneral h4").text();
	$(".divTituloGeneral h4").html("Consolidado de actos");
	resizeDivWrapper("wrapperConsolidadoActosResponsable",300);
	
}
function verAcordeDivisionFiltroActo(){
	var listDivisiones = $("#selDivisionActoFiltro").val();
	if(listDivisiones== null && numAreasJefe == 0 && numAreasGerente == 0 && false){
		listDivisionActoColaborador.forEach(function(val,index){
			if(index == 0){val.selected = 1};
		})
		crearSelectOneMenuObligMultipleCompleto
		("selDivisionActoFiltro", "verAcordeDivisionFiltroActo()",listDivisionActoColaborador, "divisionId","divisionName",
				 "#divSelectDivisionFiltroActos","Todos");
	
		alert(getSession("gestopdivision")+" Obligatoria");
		return;
	}else{
		
	}
	var unFiltro = true;console.log(listAreaActoColaborador);
	var listAreasFiltro = listAreaActoColaborador.filter(function(val){
		var isReturn = false;
		val.selected = 0;
		if(listDivisiones == null){
			return true;
		}
		listDivisiones.forEach(function(val1){
			
			 if(val.divisionId == parseInt(val1) ){
				 isReturn =true
			 };
			 if(isReturn && unFiltro){
				 val.selected = 1;
				// unFiltro = false;
			 }
			 if(isReturn){
				 return isReturn;
			 }
		});
		return isReturn;
	});
	 
	crearSelectOneMenuObligMultipleCompleto
	("selAreaActoFiltro", "",listAreasFiltro, "areaId","areaName",
			 "#divSelectAreaFiltroActosResponsable","Todos");
}
function reiniciarFiltroActoResponsable(){
	var hoy= obtenerFechaActual();
	$("#fechaInicioFiltroActo").val(sumaFechaDias(-30,hoy));
	$("#fechaFinFiltroActo").val(hoy);
	$(".camposFiltroTabla").show();
	crearSelectOneMenuObligMultipleCompleto
	 ("selSubTipoContactoActoFiltro", "",listSubTipoContactoColaborador, "id","nombre",
			 "#divSelectSubTipoContactoFiltro","Todos");
	crearSelectOneMenuObligMultipleCompleto
	 ("selCausaActoFiltro", "",listCausaInmediataColaborador, "id","nombre",
			 "#divSelectCausaFiltroActosResponsable","Todos");
	listDivisionActoColaborador.forEach(function(val,index){
		if(index == 0){val.selected = 1};
	})
	crearSelectOneMenuObligMultipleCompleto
	("selDivisionActoFiltro", "verAcordeDivisionFiltroActo()",listDivisionActoColaborador, "divisionId","divisionName",
			 "#divSelectDivisionFiltroActos","Todos");
	if(numAreasJefe > 0 || numAreasGerente > 0 || true){
		crearSelectOneMenuObligMultipleCompleto
		("selDivisionActoFiltro", "verAcordeDivisionFiltroActo()",listDivisionTotalActoColaborador, "divisionId","divisionName",
				 "#divSelectDivisionFiltroActos","Todas");
		
	}
	verAcordeDivisionFiltroActo();

	crearSelectOneMenuObligMultipleCompleto
	 ("selTipoActoFiltro", "",listActoTipo, "id","nombre",
			 "#divSelectTipoFiltroActosResponsable","Todos");
	aplicarFiltroActoResponsable();
}
function aplicarFiltroActoResponsable(){
	$(".camposFiltroTabla").hide();
	$("#wrapperConsolidadoActosResponsable").show();
	
	$("#btnIniciarFiltro").show();
	$("#btnAplicarFiltro").hide();
	$("#btnVolverDetalle").show();
	$("#buttonVolverFiltro").hide();
	cargarConsolidadoActosColaborador();
}
function cargarConsolidadoActosColaborador(){
	var listDivisiones = $("#selDivisionActoFiltro").val();
	var listArea = $("#selAreaActoFiltro").val();
	var listCausasInmediatas = $("#selCausaActoFiltro").val();
	var listSubTiposContacto = $("#selSubTipoContactoActoFiltro").val();
	var listTipoActo = $("#selTipoActoFiltro").val();
	
	var listCausaId = [];
	var listDivisionId = [];
	var listAreaId = [];
	var listSubTipoContactoId = [];
	var listTipoActoId = [];
	if(listCausasInmediatas != null){
		listCausasInmediatas.forEach(function(val){
			listCausaId.push(parseInt(val));
		});
	}
	if(listDivisiones != null){
		listDivisiones.forEach(function(val){
			listDivisionId.push(parseInt(val));
		});
	}
	if(listArea != null){
		listArea.forEach(function(val){
			listAreaId.push(parseInt(val));
		});
	}
	if(listSubTiposContacto != null){
		listSubTiposContacto.forEach(function(val){
			listSubTipoContactoId.push(parseInt(val));
		});
	}
	if(listTipoActo != null){
		listTipoActo.forEach(function(val){
			listTipoActoId.push(parseInt(val));
		});
	}
	var fechaInicioFiltro = convertirFechaTexto($("#fechaInicioFiltroActo").val() );
	var fechaFinFiltro = convertirFechaTexto($("#fechaFinFiltroActo").val() );
	 
	if(listCausaId.length == 0?listCausaId = null:"");
	if(listAreaId.length == 0?listAreaId = null:"");
	if(listDivisionId.length == 0?listDivisionId = null:"");
	
	
	if(listSubTipoContactoId.length == 0?listSubTipoContactoId = null:"");
	if(listTipoActoId.length == 0?listTipoActoId = null:"");
	var isTrabajadorConsolidado = parseInt($("#selIsTrabajadorConsolidado").val())
	var dataParam2={
			matrixId:getSession("unidadGosstId"),
			companyId:getSession("gestopcompanyid"),
			listCausaId : listCausaId,
			listAreaId : listAreaId,listDivisionId : listDivisionId,
			listTipoContactoId : listSubTipoContactoId,
			listTipoActoId : listTipoActoId,
			fechaInicioFiltro : fechaInicioFiltro,
			fechaFinFiltro : fechaFinFiltro,
			isTrabajador : (isTrabajadorConsolidado==0?null: 1),
			trabajadorId:getSession("trabajadorGosstId"),
	}
	callAjaxPost(URL + '/acinseguro/movil', dataParam2,function(data){
		listFullActosConsolidado = data.actos;
		$("#tblConsolidadoActo tbody tr").remove();
		listFullActosConsolidado.forEach(function(val){
			$("#tblConsolidadoActo tbody")
			.append("<tr>" +
					"<td>"+val.reporteFechaTexto+"</td>" +
					"<td>"+val.horaTexto+"</td>" +
					"<td>"+val.divisionNombre+"</td>" + 
					"<td>"+val.areaNombre+"</td>" + 
					"<td>"+val.reporteLugar+"</td>" +
					"<td>"+val.reporteNombre+"</td>" +
					"<td>"+val.acInseguroNombre+"</td>" +
					"<td>"+val.reporteAccion+"</td>" +
					"<td>"+val.subTipoContacto.nombre+"</td>" + 
					
					"<td>"+val.acInseguroTipoNombre+"</td>" +
					"<td>"+val.acInseguroCausaNombre+"</td>" +
					"<td>"+val.evidenciaNombre+"</td>" +
					"<td><a class='efectoLink' " +
					"onclick='verAccionesConsolidadoActo("+val.acInseguroId+")'><i class='fa fa-list'></i>"+
					val.numAccionesColaboradorCompletadas+" / "+val.numAccionesColaboradorTotal+"</td>" +
					"" +
					"</tr>")
		});
		
	});
}
function verAccionesConsolidadoActo(actoId){
	var contenido ="<div class='wrapper' id='wrapperConsolidadoAccionesResponsable'>" +
	"<table  style='min-width: 100% ' " +
	"id='tblConsolidadoAcciones' class='table table-striped table-bordered table-hover table-fixed'>" +
	"<thead>" +
		 "<tr>" +
		 "<td class='tb-acc' style='width : 180px' >Clasificación</td>" +
		 "<td class='tb-acc' style='width : 180px' >Descripción</td>" +
		 "<td class='tb-acc' style='width : 180px' >Fecha de registro</td>" +
		 "<td class='tb-acc' style='width : 180px' >Responsable</td>" +
		 "<td class='tb-acc' style='width : 180px' >Evidencia</td>" +
		 
		 "</tr>"+
	"</thead>" +
	"<tbody></tbody>" +
	"</table>" +
	"</div>" ;
	var listPaneles=
	 {id:"asd",nombre:"Acciones de mejora del acto/condición",clase:"",contenido:contenido}
                ;
	agregarModalPrincipalColaborador(listPaneles,function(){
		 
	});
	$("#asd .modal-dialog").css({"width":"90%","max-width":"100%"})
	callAjaxPost(URL+"/gestionaccionmejora/accionmejora",
			{accionMejoraTipoId : 2,
		eventoInseguroId : actoId},function(data){
		 
		var asd = data.list;
		asd.forEach(function(val){
			$("#tblConsolidadoAcciones tbody").append("<tr>" +
					"<td>"+val.clasificacion.nombre+"</td>"+
					"<td>"+val.descripcion+"</td>"+
					"<td>"+(val.fechaRevision ==null?val.fechaRealTexto:val.fechaRevisionTexto)+"</td>"+
					"<td>"+val.responsableNombre+"</td>"+
					"<td>"+
					(val.estadoId != 2 ?iconoGosst.desaprobado+"":iconoGosst.aprobado+"")+
					"<a target='_blank' class='efectoLink' " +
							"href='"+URL+"/gestionaccionmejora/accionmejora/evidencia?accionMejoraId="+
							val.accionMejoraId+"'>"+val.evidenciaNombre+"</a>"+"</td>"
					)
		})
	});
}
//Ver Estadísticas
function verIndicadoresConsolidadoActos(){
	$("#wrapperConsolidadoActosResponsable").remove();
	$(".divContainerGeneral").hide();  
	var buttonVolver="<button type='button' id='btnVolverDetalle' class='btn btn-success ' style='margin-right:10px'" +
	" onclick='volverVistaActos()'>" +
				"<i class='fa fa-sign-out fa-rotate-180'></i>Volver</button>";
	var buttonVolverFiltro="<button type='button' id='buttonVolverFiltro' class='btn btn-success ' style='margin-right:10px'" +
	" onclick='volverVistaIndicadorActos()'>" +
				"<i class='fa fa-sign-out fa-rotate-180'></i>Volver</button>";
	var buttonFiltro="<button type='button' class='btn btn-info ' id='btnIniciarFiltro' style='margin-right:10px'" +
	" onclick='iniciarFiltroActosResponsable()'>" +
				"<i class='fa fa-filter'></i>Ver filtros</button>";
	var buttonFiltroAplicar="<button type='button' class='btn btn-info ' id='btnAplicarFiltro' style='margin-right:10px'" +
	" onclick='aplicarFiltroActoIndicadorResponsable()'>" +
				"<i class='fa fa-filter'></i>Aplicar filtros</button>";
	 
	var textoDivision = "",textoArea="";
	if(numAreasSupervisor > 0){
		textoDivision = "Solo puede ver "+getSession("gestopdivision")
		+" de "+getSession("gestoparea")+" supervisadas";
		textoArea = "Solo puede ver "+getSession("gestoparea")+" supervisadas";
	}
	if(numAreasJefe > 0 || numAreasGerente > 0){
		textoDivision="Puede ver todo "+getSession("gestopdivision")+" de su "+getSession("gestopunidad");
		textoArea="";
	}
	var listOption = [
	                  {id : 4,nombre :"Área vs meses"},
	                {id : 1,nombre :"Área vs Causa Inmediata"},
	                  {id : 2,nombre :"Área vs Riesgo asociado",selected :1},
	                  
	                  {id : 5,nombre : "Reportes emitidos por cada área "},
	                  {id : 6,nombre :"Colaboradores que emiten reportes"},
	                  {id : 7,nombre : "Reportes por áreas en donde se registró el evento"}
	                  //{id : 3,nombre :"Área vs Tipo"},
	                  ]
	var selTipoIndicadorActos = crearSelectOneMenuOblig("selTipoIndicadorActos", "verAcordeTipoIndicadorActos();", listOption,"6", "id",
			"nombre","Seleccionar")
	$("body")
	.append(
			"<div id='divActoResponsableTabla' style='padding-left: 40px;'>" +
			"<form class='form-inline' id='formBtnGeneral'>"+
			buttonVolver+
			buttonVolverFiltro+
			"<label id='labelIndicadorActosResponsable'  >Tipo de indicador"+
			"</label>" +
			selTipoIndicadorActos + 
			
			buttonFiltroAplicar+buttonFiltro+"</form>"+
			
			"<div class='camposFiltroTabla' style='width : 500px ; max-width: 100%'>"+
			 
					"<div class='contain-filtro' id=''>" +
					
					"<label for=''  >Fecha Inicio "+
					"</label>" +
					"<div id=''> <input type='date' class='form-control' id='fechaInicioFiltroActo'>" +
					"</div>" + 
				"</div>"+
				"<div class='contain-filtro' id=''>" +
				
				"<label for=''  >Fecha Fin "+
				"</label>" +
				"<div id=''> <input type='date' class='form-control' id='fechaFinFiltroActo'>" +
				"</div>" + 
			"</div>"+
			
			

"<div class='contain-filtro' id='filtroDivisionActosResponsable'>" +
			
			"<label for='filtroDivisionActosResponsable'  > "+getSession("gestopdivision")+
			"</label>" +
			"<div id='divSelectDivisionFiltroActos'> " +
			"</div>" + 
			"<small>"+textoDivision+"</small>"+
		"</div>"+
					"<div class='contain-filtro' id='filtroAreaActosResponsable'>" +
						
						"<label for='filtroAreaActosResponsable'  > "+getSession("gestoparea")+
						"</label>" +
						"<div id='divSelectAreaFiltroActosResponsable'> " +
						"</div>" + 
						"<small>"+textoArea+"</small>"+
					"</div>"+	
					
					
"<div class='contain-filtro'  >" +
					
					"<label   >Tipo de reporte"+
					"</label>" +
					"<div id='divSelectTipoActoFiltro'> " +
					"</div>" + 
				"</div>"+
					"<div class='contain-filtro' id='filtroDetalleIndicadorActosResponsable'>" +
					"<label for='filtroDetalleIndicadorActosResponsable'  >test"+
					"</label>" +
					"<div id='divSelectDetalleFiltroActosResponsable'> " +
					"</div>" + 
				"</div>"+
				" </div>" +
				"<small >Para visualizar diferentes indicadores, modificar el filtro</small>"+
				
				 
				 "<div class='wrapper' id='wrapperGraficoActosResponsable'></div>"+
				"<div class='wrapper' id='wrapperConsolidadoActosResponsable'>" +
			"<table  style='min-width: 100% ' " +
			"id='tblConsolidadoActo' class='table table-striped table-bordered table-hover table-fixed'>" +
			"<thead>" +
				 "<tr>" +
						
				 "</tr>"+
			"</thead>" +
			"<tbody></tbody>" +
			"</table>" +
			"</div>" +
			" </div>");
	
	$(".camposFiltroTabla").show();
	var hoy= obtenerFechaActual();
	$("#fechaInicioFiltroActo").val(sumaFechaDias(-30,hoy));
	$("#fechaFinFiltroActo").val(hoy);
	//
	listDivisionActoColaborador.forEach(function(val,index){
		if(index == 0){val.selected = 1};
	})
	crearSelectOneMenuObligMultipleCompleto
	("selDivisionActoFiltro", "verAcordeDivisionFiltroActo()",listDivisionActoColaborador, "divisionId","divisionName",
			 "#divSelectDivisionFiltroActos","Todos");

	 
	crearSelectOneMenuObligMultipleCompleto
	("selTipoActoFiltro", "",listActoTipo, "id","nombre",
		 "#divSelectTipoActoFiltro","Todos");
	
	if(numAreasJefe > 0 || numAreasGerente > 0){
		crearSelectOneMenuObligMultipleCompleto
		("selDivisionActoFiltro", "verAcordeDivisionFiltroActo()",listDivisionTotalActoColaborador, "divisionId","divisionName",
				 "#divSelectDivisionFiltroActos","Todas");
		
	}
	verAcordeDivisionFiltroActo();
	//

	
	$(".camposFiltroTabla").hide();
	$("#buttonVolverFiltro").hide();
	$("#btnAplicarFiltro").hide(); 
	  $("#divTrabActosResponsable form").on("submit",function(e){
			e.preventDefault(); 
		}); 
	tituloAnteriorColaborador=$(".divTituloGeneral h4").text();
	$(".divTituloGeneral h4").html("Indicadores de Actos/Condiciones");
	resizeDivWrapper("wrapperGraficoActosResponsable",300);
	resizeDivWrapper("wrapperConsolidadoActosResponsable",300);
	verAcordeTipoIndicadorActos();
}
var noDefinidoFiltro = [{id:0,nombreAux:"No definido"}];
function verAcordeTipoIndicadorActos(){
	var tipoIndicador =parseInt($("#selTipoIndicadorActos").val() );
	$("#filtroDetalleIndicadorActosResponsable").show();
	$(".camposFiltroTabla").show();
	switch(tipoIndicador){
	case 1:
		$("#filtroDetalleIndicadorActosResponsable label").html("Causas Inmediatas")
		crearSelectOneMenuObligMultipleCompleto
		 ("selCausaActoFiltro", "",listCausaInmediataColaborador, "id","nombre",
				 "#divSelectDetalleFiltroActosResponsable","Todos");
		break;
	case 2:
		$("#filtroDetalleIndicadorActosResponsable label").html("Riesgos Asociados");
		
		crearSelectOneMenuObligMultipleCompleto
		("selSubTipoContactoActoFiltro", "",noDefinidoFiltro.concat(listSubTipoContactoColaborador), "id","nombreAux",
			 "#divSelectDetalleFiltroActosResponsable","Todos");
		break;
	case 3:
		$("#filtroDetalleIndicadorActosResponsable label").html("Tipo")
		crearSelectOneMenuObligMultipleCompleto
		("selTipoActoFiltro", "",listActoTipo, "id","nombre",
			 "#divSelectDetalleFiltroActosResponsable","Todos");
		break;
	case 4:
	case 5:
	case 6:
	case 7:
		$("#filtroDetalleIndicadorActosResponsable").hide();
		break;
	}
	$(".camposFiltroTabla").hide();
	cargarIndicadoresActosColaborador();
}
function volverVistaIndicadorActos (){
	$(".camposFiltroTabla").hide();
	$("#wrapperConsolidadoActosResponsable").show();
	$("#wrapperGraficoActosResponsable").show();

	$("#labelIndicadorActosResponsable").show();
	$("#selTipoIndicadorActos").show();
	
	$("#btnIniciarFiltro").show();
	$("#btnAplicarFiltro").hide();
	$("#buttonVolverFiltro").hide();
	$("#btnVolverDetalle").show();
}
function aplicarFiltroActoIndicadorResponsable(){
	volverVistaIndicadorActos();
	cargarIndicadoresActosColaborador();
}
var listCausaIdGlobal = [];
var listAreaIdGlobal = [];
var listSubTiposContactoIdGlobal = [];
var listTipoActoIdGlobal = [];
var listDivisionIdGlobal = [];
var listResponsablesActoColaborador = [];
function hallarFechasPeriodoActo(){
var fechaInicio=$("#fechaInicioFiltroActo").val();
var fechaFin=$("#fechaFinFiltroActo").val();
	var fecha1=fechaInicio.split('-');
	var fecha2=fechaFin.split('-');
	var difanio= fecha2[0]-fecha1[0]+1;
	var difDias=restaFechas(fechaInicio,fechaFin);
	var listfecha=[];
	 
		for (index =0; index <difanio; index++) {
			var anioaux=parseInt(index)+parseInt(fecha1[0]);
			
			if(difanio>1){
				if(index==0){
					for(index1 =0; index1 < 12-fecha1[1]+1; index1++) {
						
						var mesaux=parseInt(index1)+parseInt(fecha1[1]);
						listfecha.push({mes:mesaux,anio:anioaux,nombre:anioaux+"-"+((''+mesaux).length<2 ? '0' : '') + mesaux });
						}
				}
				if(index==difanio-1){
					for(index4=0;index4<parseInt(fecha2[1]);index4++){
						var mesaux3=parseInt(index4)+parseInt("1");
						listfecha.push({mes:mesaux3,anio:anioaux,nombre:anioaux+"-"+((''+mesaux3).length<2 ? '0' : '') + mesaux3 });
					}
							}	
				
				if(index>0 && index <difanio-1 ){
						
						for(index3 =0;index3 <12;index3++){
							var mesaux2=parseInt(index3)+1;	
							listfecha.push({mes:mesaux2,anio:anioaux,nombre:anioaux+"-"+((''+mesaux2).length<2 ? '0' : '') + mesaux2 });
						}
						
						
					}
					
			}else{
				for(index5 =0; index5 < fecha2[1]-fecha1[1]+1; index5++) {
					
					var mesaux4=parseInt(index5)+parseInt(fecha1[1]);
					listfecha.push({mes:mesaux4,anio:anioaux,nombre:anioaux+"-"+((''+mesaux4).length<2 ? '0' : '') + mesaux4 });
					}
				
			}
			
			

			
		}; 
	
	
	return listfecha;
	
}
function cargarIndicadoresActosColaborador(){
	var listDivision = $("#selDivisionActoFiltro").val();
	var listArea = $("#selAreaActoFiltro").val();
	var listCausasInmediatas = $("#selCausaActoFiltro").val();
	var listSubTiposContacto = $("#selSubTipoContactoActoFiltro").val();
	var listTipoActo = $("#selTipoActoFiltro").val();
	
	listCausaIdGlobal = [];listAreaIdGlobal = [];
	listSubTiposContactoIdGlobal = []; listTipoActoIdGlobal = [];
	listDivisionIdGlobal = [];
	if(listCausasInmediatas != null){
		listCausasInmediatas.forEach(function(val){
			listCausaIdGlobal.push(parseInt(val));
		});
	}
	if(listArea != null){
		listArea.forEach(function(val){
			listAreaIdGlobal.push(parseInt(val));
		});
	}
	if(listDivision != null){
		listDivision.forEach(function(val){
			listDivisionIdGlobal.push(parseInt(val));
		});
	}
	
	if(listSubTiposContacto != null){
		listSubTiposContacto.forEach(function(val){
			listSubTiposContactoIdGlobal.push(parseInt(val));
		});
	}
	if(listTipoActo != null){
		listTipoActo.forEach(function(val){
			listTipoActoIdGlobal.push(parseInt(val));
		});
	}
	var fechaInicioFiltro = convertirFechaTexto($("#fechaInicioFiltroActo").val() );
	var fechaFinFiltro = convertirFechaTexto($("#fechaFinFiltroActo").val() );
	 
	(listCausaIdGlobal.length == 0?listCausaIdGlobal = null:"");
	(listAreaIdGlobal.length == 0?listAreaIdGlobal = null:"");
	(listDivisionIdGlobal.length == 0?listDivisionIdGlobal = null:"");
	(listSubTiposContactoIdGlobal.length == 0?listSubTiposContactoIdGlobal = null:"");
	(listTipoActoIdGlobal.length == 0?listTipoActoIdGlobal = null:"");
	var dataParam2={
			matrixId:getSession("unidadGosstId"),
			companyId:getSession("gestopcompanyid"),
			listCausaId : listCausaIdGlobal,
			listAreaId : listAreaIdGlobal,
			listDivisionId : listDivisionIdGlobal,
			listTipoContactoId : listSubTiposContactoIdGlobal,
			listTipoActoId : listTipoActoIdGlobal,
			
			fechaInicioFiltro : fechaInicioFiltro,
			fechaFinFiltro : fechaFinFiltro,
			trabajadorId:getSession("trabajadorGosstId"),
	}
	callAjaxPost(URL + '/acinseguro/movil', dataParam2,function(data){
		var tipoIndicador =parseInt($("#selTipoIndicadorActos").val() );
		listFullActosConsolidado = data.actos;
		listResponsablesActoColaborador = data.trabajadoresAreas;
		$("#tblConsolidadoActo tbody tr").remove();
		$("#tblConsolidadoActo thead tr td").remove();
		var listTipoActo = listActoTipo.filter(function(val){
			if(listTipoActoIdGlobal == null){
				return true;
			}
			 return listTipoActoIdGlobal.indexOf(val.id) > -1 ;
		});
		var listSubTiposContacto = noDefinidoFiltro.concat(listSubTipoContactoColaborador).filter(function(val){
			if(listSubTiposContactoIdGlobal == null){
				return true;
			}
			 return listSubTiposContactoIdGlobal.indexOf(val.id) > -1 ;
		});
		var listCausaIndicador = listCausaInmediataColaborador.filter(function(val){
			if(listCausaIdGlobal == null){
				return true;
			}
			 return listCausaIdGlobal.indexOf(val.id) > -1 ;
		});
		var listAreaIndicador = listAreaActoColaborador.filter(function(val){
			if(listAreaIdGlobal == null){
				if(listDivisionIdGlobal == null){
					return true;
				}
				return listDivisionIdGlobal.indexOf(val.divisionId) > -1
				
			}
			 return listAreaIdGlobal.indexOf(val.areaId) > -1 ;
		});
		var listFechas= hallarFechasPeriodoActo();
		switch(tipoIndicador){
		case 5:
			 
			listAreaIndicador.forEach(function(val){
				val.numActos = 0;
				listFullActosConsolidado.forEach(function(val2){
					if(val2.trabajadorAreaId == val.areaId){
						val.numActos++;
					}
				});
			});
			listAreaIndicador.sort(function(a, b) {
				  return -a.numActos + b.numActos;
			});
			$("#tblConsolidadoActo tbody").html("<tr><td><strong>Total</strong></td></tr>");
			$("#tblConsolidadoActo thead tr").append("<td class='tb-acc' style='width : 160px'><strong>Área</strong></td>");
			listAreaIndicador.forEach(function(val){
				$("#tblConsolidadoActo thead tr").append("<td class='tb-acc' style='width : 180px'>"+val.areaName+"</td>");
				$("#tblConsolidadoActo tbody tr").append("<td>"+val.numActos+"</td>");
				
			});
			break;
		case 6:
			listResponsablesActoColaborador.forEach(function(val){
				val.numActos = 0;
				listFullActosConsolidado.forEach(function(val2){
					if(val2.trabajadorId == val.trabajadorId){
						val.numActos++;
					}
				});
			});
			listResponsablesActoColaborador.sort(function(a, b) {
				  return -a.numActos + b.numActos;
			});
			$("#tblConsolidadoActo tbody").html("<tr><td><strong>Total</strong></td></tr>");
			$("#tblConsolidadoActo thead tr").append("<td class='tb-acc' style='width : 160px'><strong>Trabajador reporta</strong></td>");
			listResponsablesActoColaborador.forEach(function(val){
				$("#tblConsolidadoActo thead tr").append("<td class='tb-acc' style='width : 180px'>"+val.nombre+"</td>");
				$("#tblConsolidadoActo tbody tr").append("<td>"+val.numActos+"</td>");
				
			});
			break;
		case 7:
			 
			listAreaIndicador.forEach(function(val){
				val.numActos = 0;
				listFullActosConsolidado.forEach(function(val2){
					if(val2.areaId == val.areaId){
						val.numActos++;
					}
				});
			});
			listAreaIndicador.sort(function(a, b) {
				  return -a.numActos + b.numActos;
			});
			$("#tblConsolidadoActo tbody").html("<tr><td><strong>Total</strong></td></tr>");
			$("#tblConsolidadoActo thead tr").append("<td class='tb-acc' style='width : 160px'><strong>Área</strong></td>");
			listAreaIndicador.forEach(function(val){
				$("#tblConsolidadoActo thead tr").append("<td class='tb-acc' style='width : 180px'>"+val.areaName+"</td>");
				$("#tblConsolidadoActo tbody tr").append("<td>"+val.numActos+"</td>");
				
			});
			break;
			
		case 4:
			break;
		case 1:
			$("#tblConsolidadoActo thead tr").append("<td class='tb-acc' style='width : 160px'>"+"Área / Causa"+"</td>");
			listCausaIndicador.forEach(function(val){
				$("#tblConsolidadoActo thead tr").append("<td class='tb-acc' style='width : 160px'>"+val.nombre+"</td>");
			});
			break;
		case 2:
			
			break;
		case 3:
			$("#tblConsolidadoActo thead tr").append("<td class='tb-acc' style='width : 160px'>"+"Área / Tipo"+"</td>");
			listTipoActo.forEach(function(val){
				$("#tblConsolidadoActo thead tr").append("<td class='tb-acc' style='width : 160px'>"+val.nombre+"</td>");
			});
			break;
		}

		if(tipoIndicador == 4){
			 $("#tblConsolidadoActo tbody")
			 .append("<tr id='trtotal'><td>Total</td></tr>" +
			 		"<tr id='trindicador1'><td>Porcentaje</td></tr>"+
					 "<tr id='trindicadorAcum' style='    border-bottom: 2px solid black;'><td>Porcentaje acumulado</td></tr>"+
					 "");
			 	var indicadorAcum = 0; 
				listFechas.forEach(function(val){
					val.numActosOrden =0;
					
					listFullActosConsolidado.forEach(function(val2){
						if(val2.fechaAnio == val.anio && val2.fechaMes == val.mes){
							val.numActosOrden++;
						}
						
					});
				});
				listFechas.forEach(function(val){
					val.indicadorTest = val.numActosOrden +"/"+ listFullActosConsolidado.length;
					val.indicador = val.numActosOrden / listFullActosConsolidado.length;
					indicadorAcum += val.indicador;
					val.indicadorAcumulado = indicadorAcum;
					$("#trindicadorAcum").append("<td>"+pasarDecimalPorcentaje(val.indicadorAcumulado,2)+"</td>")
					$("#trindicador1").append("<td>"+pasarDecimalPorcentaje(val.indicador,2)+"</td>")
					$("#trtotal").append("<td>"+val.numActosOrden+"</td>")
				});

				$("#tblConsolidadoActo thead tr").append("<td class='tb-acc' style='width : 160px'>"+"Área / Meses"+"</td>");
				listFechas.forEach(function(val){
					$("#tblConsolidadoActo thead tr").append("<td class='tb-acc' style='width : 160px'>"+val.nombre+"</td>");
				});
				 
			}
		if(tipoIndicador == 2){
			 $("#tblConsolidadoActo tbody")
			 .append("<tr id='trtotal' style='border-top: 2px solid black;'><td>Total</td></tr>" +
			 		"<tr id='trindicador1'><td>Porcentaje</td></tr>"+
					 "<tr id='trindicadorAcum' ><td>Porcentaje acumulado</td></tr>"+
					 "");
			 	var indicadorAcum = 0; 
				listSubTiposContacto.forEach(function(val){
					val.numActosOrden =0;
					
					listFullActosConsolidado.forEach(function(val2){
						var pertenece = false;
						var lista1=val2.idTiposContacto; 
						var listaTipoContacto=lista1.split(",");
						
						listaTipoContacto.forEach(function(val3){
							if(val.id == val3){
								pertenece= true;
							}
						});
						if(pertenece){
							val.numActosOrden++;
						}
						
					});
				});
				listSubTiposContacto.sort(function(a, b) {
					  return -a.numActosOrden + b.numActosOrden;
				});
				$("#tblConsolidadoActo thead tr").append("<td class='tb-acc' style='width : 160px'>"+"Área / Riesgo Asociado"+"</td>");
				listSubTiposContacto.forEach(function(val){
					$("#tblConsolidadoActo thead tr").append("<td class='tb-acc' style='width : 160px'>"+val.nombreAux+"</td>");
				});
				$("#tblConsolidadoActo thead tr").append("<td class='tb-acc' style='width : 160px'>"+"Total"+"</td>");
				var numActosOrdenTotal = 0;
				listSubTiposContacto.forEach(function(val){
					val.indicadorTest = val.numActosOrden +"/"+ listFullActosConsolidado.length;
					val.indicador = val.numActosOrden / listFullActosConsolidado.length;
					indicadorAcum += val.indicador;
					val.indicadorAcumulado = indicadorAcum;
					
					numActosOrdenTotal +=val.numActosOrden;
					$("#trindicadorAcum").append("<td>"+pasarDecimalPorcentaje(val.indicadorAcumulado,2)+"</td>")
					$("#trindicador1").append("<td>"+pasarDecimalPorcentaje(val.indicador,2)+"</td>")
					$("#trtotal").append("<td>"+val.numActosOrden+"</td>");
				});
				$("#trindicadorAcum").append("<td>"+"100%"+"</td>")
				$("#trindicador1").append("<td>"+"100%"+"</td>")
				$("#trtotal").append("<td>"+numActosOrdenTotal+"</td>");
			}
		listAreaIndicador.forEach(function(val){
			 val.causas = [];
			 listCausaIndicador.forEach(function(val1){
				 val.causas.push({id: val1.id,nombre : val1.nombre})
			 });
			 
			 val.subtiposcontacto = [];
			 listSubTiposContacto.forEach(function(val1){
				 val.subtiposcontacto.push({id: val1.id,nombre : val1.nombre})
			 });
			 val.fechas = [];
			 listFechas.forEach(function(val1){
				 val.fechas.push({anio: val1.anio,mes: val1.mes,nombre : val1.nombre})
			 })
			 val.tipos = listTipoActo;
			 switch(tipoIndicador){
			 case 1:
			 case 2:
			 case 3:
			 case 4:
				 $("#tblConsolidadoActo tbody").prepend("<tr id='trfilar"+val.areaId+"'><td>"+val.areaName+"</td></tr>");
				 break;
			 }
			
			 
				switch(tipoIndicador){
				case 1:
				 val.causas.forEach(function(val1){
					val1.numActos = 0;
					listFullActosConsolidado.forEach(function(val2){
						if(val2.areaId == val.areaId && val2.acInseguroCausaId == val1.id){
							val1.numActos++;
						}
					});
					$("#trfilar"+val.areaId).append("<td>" +
							//"<a onclick='verDetalleActoAreaCausa("+val.areaId+","+val1.id+")' class='efectoLink'>"+
							val1.numActos
							//+"</a>" 
							+"</td>")
				 });
				 break;
				case 2:
					var numActosTotal =0;
					 val.subtiposcontacto.forEach(function(val1){
						val1.numActos = 0;
						listFullActosConsolidado.forEach(function(val2){
							var pertenece = false;
							var lista1=val2.idTiposContacto; 
							var listaTipoContacto=lista1.split(",");
							
							listaTipoContacto.forEach(function(val3){
								if(val1.id == val3){
									pertenece= true;
								}
							});
							if(pertenece && val2.areaId == val.areaId){
								val1.numActos++;
							}
							
						});
						numActosTotal += val1.numActos;
						$("#trfilar"+val.areaId).append("<td>" +
								//"<a onclick='verDetalleActoSubTipoContactoArea("+val.id+","+val1.id+")' class='efectoLink'>"+
								val1.numActos
								//+"</a>" 
								+"</td>")
					 });
					 $("#trfilar"+val.areaId).append("<td>" +
						numActosTotal
						+"</td>")
					 break; 
				case 4:
					val.fechas.forEach(function(val1){
						val1.numActos = 0;
						listFullActosConsolidado.forEach(function(val2){
							if(val2.areaId == val.areaId && val2.fechaAnio == val1.anio && val2.fechaMes == val1.mes){
								val1.numActos++;
							}
						});
						$("#trfilar"+val.areaId).append("<td>" +
								//"<a onclick='verDetalleActoTipoArea("+val.id+","+val1.id+")' class='efectoLink'>"+
								val1.numActos
								//+"</a>" 
								+"</td>")
					 });
					 break;
					 
				}
				
			 
		});
		
		
		 
		switch(tipoIndicador){
		case 5:
			var dataPrincipal = [];
			var listCategories = [];
			var listCantidad =  [];
			listAreaIndicador.forEach(function(val){
				listCategories.push(val.areaName);
				listCantidad.push(val.numActos);
				dataPrincipal.push(
						{"name": val.nombre,
		                    "y": val.numActos}
						);
			});
			dataPrincipal.sort(function(a, b) {
				  return -a.y + b.y;
				});
			// Create the chart
			Highcharts.chart('wrapperGraficoActosResponsable', {
			    chart: {
			        type: 'column'
			    },
			    title: {
			        text: 'RANKING DE ÁREA QUE MÁS REPORTA'
			    },
			    xAxis: {
			        categories: listCategories,
			        crosshair: true
			    },
			    yAxis: {
			        min: 0,
			        title: {
			            text: 'Reportes'
			        }
			    },
			    tooltip: {
			        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
			            '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
			        footerFormat: '</table>',
			        shared: true,
			        useHTML: true
			    },
			    plotOptions: {
			        column: {
			            pointPadding: 0.2,
			            borderWidth: 0
			        }
			    },
			    series: [{
			        name: 'Total',
			        data: listCantidad

			    }]
			});
			break;
		case 6:
			var listCategories = [];
			var listCantidad =  [];
			listResponsablesActoColaborador.forEach(function(val){
				listCategories.push(val.nombre);
				listCantidad.push(val.numActos); 
			});
			// Create the chart
			Highcharts.chart('wrapperGraficoActosResponsable', {
			    chart: {
			        type: 'column'
			    },
			    title: {
			        text: 'RANKING PERSONA QUE MÁS REPORTA'
			    },
			    xAxis: {
			        categories: listCategories,
			        crosshair: true
			    },
			    yAxis: {
			        min: 0,
			        title: {
			            text: 'Reportes'
			        }
			    },
			    tooltip: {
			        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
			            '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
			        footerFormat: '</table>',
			        shared: true,
			        useHTML: true
			    },
			    plotOptions: {
			        column: {
			            pointPadding: 0.2,
			            borderWidth: 0
			        }
			    },
			    series: [{
			        name: 'Total',
			        data: listCantidad

			    }]
			});
			
			break;
		case 7:
			var dataPrincipal = [];
			var listCategories = [];
			var listCantidad =  [];
			listAreaIndicador.forEach(function(val){
				listCategories.push(val.areaName);
				listCantidad.push(val.numActos);
				dataPrincipal.push(
						{"name": val.nombre,
		                    "y": val.numActos}
						);
			});
			dataPrincipal.sort(function(a, b) {
				  return -a.y + b.y;
				});
			// Create the chart
			Highcharts.chart('wrapperGraficoActosResponsable', {
			    chart: {
			        type: 'column'
			    },
			    title: {
			        text: 'REPORTES POR ÁREA DÓNDE SE REGISTRÓ EL EVENTO'
			    },
			    xAxis: {
			        categories: listCategories,
			        crosshair: true
			    },
			    yAxis: {
			        min: 0,
			        title: {
			            text: 'Reportes'
			        }
			    },
			    tooltip: {
			        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
			            '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
			        footerFormat: '</table>',
			        shared: true,
			        useHTML: true
			    },
			    plotOptions: {
			        column: {
			            pointPadding: 0.2,
			            borderWidth: 0
			        }
			    },
			    series: [{
			        name: 'Total',
			        data: listCantidad

			    }]
			});
		break;
		
		case 1:
			var dataPrincipal = [];
			var dataSeries = [];
			listCausaIndicador.forEach(function(val){
				val.numActosTotal = 0;
				val.areas = [];
				listAreaIndicador.forEach(function(val1){
					val.areas.push({areaId : val1.areaId, areaName : val1.areaName})
				});
				 
				var dataArea = [];
				val.areas.forEach(function(val1){
					val1.numActos = 0;
					listFullActosConsolidado.forEach(function(val2){
						if(val2.areaId == val1.areaId && val2.acInseguroCausaId == val.id){
							val1.numActos++;
						}
						
					}); 
					dataArea.push([val1.areaName,val1.numActos])
				});
				dataSeries.push(
						{"name": val.nombreAux,
			                "id": val.id+"",
			                "data":   dataArea  }		
						);
			});
			listAreaIndicador.forEach(function(val){
				val.causas.forEach(function(val1){
					
					listCausaIndicador.forEach(function(val2){
						if(val2.id == val1.id){
							val2.numActosTotal += val1.numActos;
						}
					});
				});
			});
			
			listCausaIndicador.forEach(function(val){
				dataPrincipal.push(
						{"name": val.nombre,
		                    "y": val.numActosTotal,
		                    "drilldown": val.id+""}
						);
			});
			dataPrincipal.sort(function(a, b) {
				  return -a.y + b.y;
				})
			Highcharts.setOptions({
			    lang: {
			        drillUpText: '<< Volver'
			    }
			});
			// Create the chart
			Highcharts.chart('wrapperGraficoActosResponsable', {
			    chart: {
			        type: 'column'
			    },
			    title: {
			        text: ' Causas Inmediatas Total'
			    },
			    xAxis: {
			        type: 'category',
			        labels: {
						style: {
			                width: '400px',
			                whiteSpace: 'nowrap'
						},
						//rotation : 90
						//"align": "left",
						//"y": -40,
						//"x": 4
					},
			    },
			    yAxis: {
			        title: {
			            text: 'Cantidad'
			        }

			    },
			    legend: {
			        enabled: false
			    },
			    plotOptions: {
			        series: {
			            borderWidth: 0,
			            dataLabels: {
			                enabled: true,
			                format: '{point.y}'
			            }
			        }
			    },

			    tooltip: {
			        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
			        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y }</b> <br/>'
			    },

			    "series": [
			        {
			            "name": "Causas Inmediatas",
			            "colorByPoint": true,
			            "data": dataPrincipal
			        }
			    ],
			    "drilldown": {
			    	 drillUpButton: {
			             relativeTo: 'spacingBox',
			             position: {
			             align: "left",
			                 y: 0,
			                 x: 0
			             }
			    	 },
			        "series": dataSeries
			    }
			}); 
			break;
		case 2:
			var dataPrincipal = [];
			var dataSeries = [];
			listSubTiposContacto.forEach(function(val){
				val.numActosTotal = 0;
				val.areas = [];
				listAreaIndicador.forEach(function(val1){
					val.areas.push({areaId : val1.areaId, areaName : val1.areaName})
				});
				 
				var dataArea = [];
				val.areas.forEach(function(val1){
					val1.numActos = 0;
					listFullActosConsolidado.forEach(function(val2){
						var pertenece = false;
						var lista1=val2.idTiposContacto; 
						var listaTipoContacto=lista1.split(",");
						
						listaTipoContacto.forEach(function(val3){
							if(val.id == val3){
								pertenece= true;
							}
						});
						if(pertenece && val2.areaId == val1.areaId){
							val1.numActos++;
						}
						
					}); 
					dataArea.push([val1.areaName,val1.numActos])
				});
				dataSeries.push(
						{"name": val.nombreAux,
			                "id": val.id+"",
			                "data":   dataArea  }		
						);
			});
			listAreaIndicador.forEach(function(val){
				val.subtiposcontacto.forEach(function(val1){
					
					listSubTiposContacto.forEach(function(val2){
						if(val2.id == val1.id){
							val2.numActosTotal += val1.numActos;
						}
					});
				});
			});
			
			listSubTiposContacto.forEach(function(val){
				dataPrincipal.push(
						{"name": val.nombreAux,
		                    "y": val.numActosTotal,
		                    "drilldown": val.id+""}
						);
			});
			dataPrincipal.sort(function(a, b) {
				  return -a.y + b.y;
				})
			Highcharts.setOptions({
			    lang: {
			        drillUpText: '<< Volver'
			    }
			});
			// Create the chart
			Highcharts.chart('wrapperGraficoActosResponsable', {
			    chart: {
			        type: 'column'
			    },
			    title: {
			        text: '  Riesgos Asociados Total'
			    },
			    xAxis: {
			        type: 'category',
			        labels: {
						style: {
			                width: '400px',
			                whiteSpace: 'nowrap'
						},
						//rotation : 90
						//"align": "left",
						//"y": -40,
						//"x": 4
					},
			    },
			    yAxis: {
			        title: {
			            text: 'Cantidad'
			        }

			    },
			    legend: {
			        enabled: false
			    },
			    plotOptions: {
			        series: {
			            borderWidth: 0,
			            dataLabels: {
			                enabled: true,
			                format: '{point.y}'
			            }
			        }
			    },

			    tooltip: {
			        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
			        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y }</b> <br/>'
			    },

			    "series": [
			        {
			            "name": "Riesgos Asociados",
			            "colorByPoint": true,
			            "data": dataPrincipal
			        }
			    ],
			    "drilldown": {
			    	 drillUpButton: {
			             relativeTo: 'spacingBox',
			             position: {
			             align: "left",
			                 y: 0,
			                 x: 0
			             }
			    	 },
			        "series": dataSeries
			    }
			});
			$("#wrapperGraficoActosResponsable .highcharts-axis-labels > text > tspan").each(function(index,elem){
				 if($(elem).next("title").length > 0){
				//	$(elem).html($(elem).next("title").html());
				 }
			});
			break;
		case 4:
			var dataPrincipal = [];
			var dataSeries = [];
			listFechas.forEach(function(val){
				val.numActosTotal = 0;
				val.areas = [];
				listAreaIndicador.forEach(function(val1){
					val.areas.push({areaId : val1.areaId, areaName : val1.areaName})
				});
				 
				var dataArea = [];
				val.areas.forEach(function(val1){
					val1.numActos = 0;
					listFullActosConsolidado.forEach(function(val2){
						 
						if(val2.fechaAnio == val.anio && val2.fechaMes == val.mes 
								&& val2.areaId == val1.areaId){
							val1.numActos++;
						}
						
					}); 
					dataArea.push([val1.areaName,val1.numActos])
				});
				dataSeries.push(
						{"name": val.anio+""+val.mes,
			                "id": val.anio+""+val.mes,
			                "data":   dataArea  }		
						);
			});
			listAreaIndicador.forEach(function(val){
				val.fechas.forEach(function(val1){
					
					listFechas.forEach(function(val2){
						if(val2.anio == val1.anio && val2.mes == val1.mes){
							val2.numActosTotal += val1.numActos;
						}
					});
				});
			});
			
			listFechas.forEach(function(val){
				dataPrincipal.push(
						{"name": val.nombre,
		                    "y": val.numActosTotal,
		                    "drilldown": val.anio+""+val.mes+""}
						);
			});
			 console.log(dataSeries);
			 console.log(dataPrincipal);
			Highcharts.setOptions({
			    lang: {
			        drillUpText: '<< Volver'
			    }
			});
			// Create the chart
			Highcharts.chart('wrapperGraficoActosResponsable', {
			    chart: {
			        type: 'column'
			    },
			    title: {
			        text: '  Cantidad por Mes Total'
			    },
			    xAxis: {
			        type: 'category'
			    },
			    yAxis: {
			        title: {
			            text: 'Cantidad'
			        }

			    },
			    legend: {
			        enabled: false
			    },
			    plotOptions: {
			        series: {
			            borderWidth: 0,
			            dataLabels: {
			                enabled: true,
			                format: '{point.y}'
			            }
			        }
			    },

			    tooltip: {
			        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
			        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y }</b> <br/>'
			    },

			    "series": [
			        {
			            "name": "Meses",
			            "colorByPoint": true,
			            "data": dataPrincipal
			        }
			    ],
			    "drilldown": {
			    	 drillUpButton: {
			             relativeTo: 'spacingBox',
			             position: {
			             align: "left",
			                 y: 0,
			                 x: 0
			             }
			    	 },
			        "series": dataSeries
			    }
			});
			break;
		}
	});
}
function verDetalleActoAreaCausa(areaId,causaId){
	alert("test 1")
}
function verDetalleActoSubTipoContactoArea(areaId,subTipoContactoId){
	alert("test 2")
}
function verDetalleActoTipoArea(areaId,tipoId){
	alert("test 3")
}
//Inicio acciones evento
var accionMejoraEventoObj = {};
var listFullAccionesEvento = [];
var accionMejoraEventoColaboradorIndex = 0;
function verAccionesMejoraEventoInseguro(pindex){
	indexEventoColaborador = pindex;
	eventoColaboradorObj = listFullEventosColaborador[indexEventoColaborador]; 
	var dataParam={
			accionMejoraTipoId : 2,
			eventoInseguroId:eventoColaboradorObj.acInseguroId
			};
//  
	$("#detalleAccionEvento"+eventoColaboradorObj.acInseguroId).siblings(".divMovilHistorialTrabajador").hide();
	
	$("#detalleAccionEvento"+eventoColaboradorObj.acInseguroId).toggle() 
	//
	callAjaxPost(URL + '/gestionaccionmejora/accionmejora', dataParam, function(data) {
		switch (data.CODE_RESPONSE) {
		case "05": 
			var todo="";
			listFullAccionesEvento = data.list;
			listFullAccionesEvento.forEach(function(val,index){
				
			
			var listItemsForm=[
				{sugerencia:"",label:"<i class='fa fa-info'></i> Descripcion: ",inputForm:val.descripcion,divContainer:""},
				{sugerencia:"",label:"<i class='fa fa-info'></i> Clasificación: ",inputForm:val.clasificacion.nombre,divContainer:""},
				{sugerencia:" ",label:"<i class='fa fa-fire'></i> Fecha de Registro: ",
					inputForm:(val.fechaRevision ==null?val.fechaRealTexto:val.fechaRevisionTexto),divContainer:""},
					{sugerencia:" ",label:"<i class='fa fa-user'></i> Responsable: ",inputForm:val.responsableNombre,divContainer:""},
					{sugerencia:" ",label:"<i class='fa fa-upload'></i> Evidencia: ",inputForm: "<a target='_blank' class='efectoLink' href='"+URL+"/gestionaccionmejora/accionmejora/evidencia?accionMejoraId="+val.accionMejoraId+"'>"+val.evidenciaNombre+"</a>",divContainer:""}
			];
			var text="";
			listItemsForm.forEach(function(val1,index1)
			{
				text+=obtenerSubPanelModulo(val1);
			});
			todo+=
				"<div class='subSubDetalleAccion  gosst-neutral'>" +
					"<div class='' style=''>" +
						"<section class='containerEvalAccionMejora' id='opciones3'>" +
							"<a class='efectoLink btn-gestion' onclick='" +
							"accionMejoraEventoColaboradorIndex = "+index+";toggleMenuOpcionAC(this,"+val.accionMejoraId+",3)';'>" +
									"Ver Opciones <i class='fa fa-angle-double-down'></i>" +
							"</a>"+
							menuOpcionAc[3]+
						"</section>"+
					"</div>"+
					"<div class='row'>" +
						"<section class='col col-xs-9'>" +
							"<strong>"+
								"ACCIÓN DE MEJORA "+(index+1)+
							"</strong>"+
						"</section>"+
					"</div>"+
					text+
				"</div>";
			});
			$("#detalleAccionEvento"+eventoColaboradorObj.acInseguroId).html(todo);
			
			break;
			default:
				alert("nop");
				break;
		}
	})
}
function agregarAccionMejoraEventoInseguro()
{
	//$(".divListPrincipal > div").hide();
	accionMejoraEventoObj = {accionMejoraId : 0};
	var listItemsForm=[
			{sugerencia:" ",label:"<i class='fa fa-info'></i> Clasificación*",inputForm: "",divContainer:"divClasifAccColaborador"},
			{sugerencia:" ",label:"<i class='fa fa-fire'></i> Descripción* ",inputForm: "<input class='form-control' id='inputDescAccion'>",divContainer:""},
			{sugerencia:"Si ya ha realizado la acción, introduzca la evidencia",label:"<i class='fa fa-calendar'></i> Fecha Planificada: ",inputForm: "<input type='date' class='form-control' id='inputDateAccion'>",divContainer:""},
			{sugerencia:" ",label:"<i class='fa fa-upload'></i> Evidencia : ",inputForm: "<div id='inputEvidenciaAccEvento'></div>",divContainer:""},
			{sugerencia:" ",label:"",inputForm: "<button class='btn btn-success'  ><i class='fa fa-floppy-o'></i> Guardar</button>",divContainer:""},
			//{sugerencia:" ",label:"",inputForm: "<button class='btn btn-success' onclick='guardarAccionMejoraEvento(1)'><i class='fa fa-floppy-o'></i> Guardar y agregar otra acción</button>",divContainer:""}
		];
	var text="";
	listItemsForm.forEach(function(val,index)
	{
		text+=obtenerSubPanelModulo(val);
	});
	var contenido=
		"<form class='eventoGeneral' id='formAccEvento'>" +
			"<div id='tituloEvento'>" + 
				text+
			"</div>"+
			"</div>" +
		"</form>";
	var btnRegresar="<button class='btn btn-default' onclick='regresarEventosInseguros()' style='font-size:15px;float: left;color:white ;background-color:#008b8b'>" +
			"<i class='fa fa-arrow-left' aria-hidden='true'></i></button>"
	var listPaneles= {id:"divNuevoAccMejoraEvento",nombre:"Agregar acción de mejora",clase : "listaAuxiliar",contenido:contenido};
	                
	agregarModalPrincipalColaborador(listPaneles,function(){
		crearSelectOneMenuObligUnitarioCompleto
		 ("selClasificacionAcc", "",listClasificacionAccionEventoColaborador, "id","nombre",
				 "#divClasifAccColaborador","Todos");
	});
	
	$("#formAccEvento").on("submit",function(e){
		e.preventDefault();
		guardarAccionMejoraEvento();
	});
	
	var options=
	{container:"#inputEvidenciaAccEvento",
			functionCall:function(){ },
			descargaUrl: "",
			esNuevo:true,
			idAux:"AccEvento",
			evidenciaNombre:""};
	crearFormEvidenciaCompleta(options);
	
}
function guardarAccionMejoraEvento(hasOtroNuevo)
{
	var descripcion=$("#inputDescAccion").val();
	var fechaRevision=convertirFechaTexto($("#inputDateAccion").val());
	var clasificacionId = $("#selClasificacionAcc").val();
	eventoColaboradorObj = listFullEventosColaborador[indexEventoColaborador];
	 var existe=comprobarFieInputExiste("fileEviAccEvento");
     
	if(fechaRevision == null && !existe.isValido){alert("Introducir fecha planificada o evidencia"); return;}
	var dataParam=
	{
			accionMejoraId: accionMejoraEventoObj.accionMejoraId,
			eventoInseguroId : eventoColaboradorObj.acInseguroId,
			descripcion:descripcion,
			fechaRevision:fechaRevision,
			clasificacion : {id: clasificacionId},
			responsableNombre : "",
			responsableMail : "",
			estadoId : 1,
			inversionAccion : 0,
			accionMejoraTipoId : 2,
			horaPlanificada : "13:00:00",
			trabajadorResponsableId : getSession("trabajadorGosstId") 
	}
	callAjaxPost(URL + '/gestionaccionmejora/accionmejora/save', dataParam,
			function(data)
			{
				if(data.CODE_RESPONSE=="05")
				{
					$(".modal").modal("hide");
					guardarEvidenciaAuto(data.nuevoId,"fileEviAccEvento",
							bitsEvidenciaEscenarioAccidente,"/gestionaccionmejora/accionmejora/evidencia/save",
							function(){
						alert("El nuevo registro fue guardado correctamente.")
						 cargarReportesEventoUsuario();
						 
						
					}
					,"accionMejoraId");
					
				}
			});
}
//FIn acciones evento