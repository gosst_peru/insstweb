/**
 * 
 */
var listFullEvaluacionesInspeccion=[];
var evaluacionInspeccionObj={id:0};

var inspeccionEvento;
var gestionAccionAsociadaId=0;
var listAreasHallazgo=[];
var listNivelRiesgoHallazgo=[],listTipoReporteHallazgo=[],listFactorHallazgo=[];
var listAreasInteresadasHallazgo=[],listSeccionesInteresadasHallazgo=[];
var lastHeightInspeccion=0;
function realizarInspeccionMovil(indexEvento){
	evaluacionInspeccionObj={id:0};
	 
	if(indexEvento!=null){
		if(indexEvento >= 0){
			inspeccionEvento=listFullEventos[indexEvento];
		}
	
	}
	if(inspeccionEvento.linkAuxiliarId2==100){
		alert("Por ahora, no disponible para Monitoreo de Agentes");return;
	}
	if(inspeccionEvento.linkAuxiliarId1==2){
		
	}
	$(".divListPrincipal > div").hide();
	$("#subRealizarInspecciones").show();
	
	$("#subRealizarInspecciones").find(".tituloSubList").html(
			textoBotonVolverContenidoColaborador+""+inspeccionEvento.text);
	var objInspeccion={auditoriaId:inspeccionEvento.idAux,
						auditoriaTipo:inspeccionEvento.linkAuxiliarId2}
	if(inspeccionEvento.linkAuxiliarId2==94 || inspeccionEvento.linkAuxiliarId2==194){
		callAjaxPostNoUnlock(URL + '/auditoria/hallazgos', objInspeccion, function(data) {
			if(data.CODE_RESPONSE=="05"){
				$.unblockUI()
		$("#subRealizarInspecciones").find(".contenidoSubList").html("");
			listFullEvaluacionesInspeccion=data.list;
			listAreasHallazgo=data.areas;
			listNivelRiesgoHallazgo=data.niveles;
			listTipoReporteHallazgo=data.tiposReporte;
			listFactorHallazgo=data.factor;
			
			listSeccionesInteresadasHallazgo=data.secciones;
			listAreasInteresadasHallazgo=data.areasInteresadas;
			
			var selAreaHallazgo= crearSelectOneMenuOblig("selAreaHallazgo", "", listAreasHallazgo, "", 
					"areaId","areaName"); 
			
			var selTipoReporteHallazgo= crearSelectOneMenuOblig("selTipoReporteHallazgo", "", listTipoReporteHallazgo, "", 
					"id","nombre");
			//
			var selNivelHallazgo= crearSelectOneMenuOblig("selNivelHallazgo", "", listNivelRiesgoHallazgo, "", 
					"id","nombre");
			var selFactorHallazgo= crearSelectOneMenuOblig("selFactorHallazgo", "", listFactorHallazgo, "", 
					"id","nombre");
			var listItemsHallInsp=[
{sugerencia:"",label:"Descripción",inputForm:"<input class='form-control' id='inputDescHall'> ",divContainer:""},
//{sugerencia:"",label:"Área",inputForm:selAreaHallazgo,divContainer:""},
{sugerencia:"",label:"Tipo de reporte",inputForm:selTipoReporteHallazgo,divContainer:""},
{sugerencia:"",label:"Nivel de Riesgo",inputForm:selNivelHallazgo,divContainer:""},
{sugerencia:"",label:"Factor de seguridad",inputForm:selFactorHallazgo,divContainer:""},
{sugerencia:"",label:"Partes Interesadas (áreas)",divContainer:"tdhaarea",inputForm:""},
{sugerencia:"",label:"Partes Interesadas (empresa)",divContainer:"tdhasecc",inputForm:""},
{sugerencia:"",label:"Causa que origina el hallazgo",inputForm:"<input class='form-control' id='inputCausaHall'> ",divContainer:""},
{sugerencia:"",label:"Lugar específico",inputForm:"<input class='form-control' id='inputLugarHall'> ",divContainer:""},
{sugerencia:"",label:"Evidencia",inputForm:" ",divContainer:"eviHallazgoMovil"}
			                       ];
			$("#subRealizarInspecciones").find(".contenidoSubList")
			.append("<div class='divInformacionCurso' id='divFormHallazgo'>" +
					"<div class='divNombreInformacion' >" +textoBotonToggleInformacion+
					"Nuevo Hallazgo" +
					"</div>" +
					"<div class='divEvalHallazgoInspeccion'>" +
					obtenerSubFormInspeccion(listItemsHallInsp,"guardarHallazgoInspeccion(0)","guardarHallazgoInspeccion(1)")+
					"</div>" +
					"</div>");
			listSeccionesInteresadasHallazgo.forEach(function(val){
				val.selected=0;
			});	
			crearSelectOneMenuObligMultipleCompleto("slcSeccionesPermitidaHallazgo", "",
					listSeccionesInteresadasHallazgo,  "id", "nombre","#tdhasecc","Secciones interesadas ...");
			
			listAreasInteresadasHallazgo.forEach(function(val){
				val.selected=0;
			});
			crearSelectOneMenuObligMultipleCompleto("slcAreasPermitidaHallazgo", "",
					listAreasInteresadasHallazgo,  "areaId", "areaName","#tdhaarea","Áreas interesadas ...");
									
				
			var options=
			{container:"#eviHallazgoMovil" ,
					functionCall:function(){ },
					descargaUrl: "",
					esNuevo:true,
					idAux:"Hallazgo",
					evidenciaNombre:""};
			crearFormEvidenciaCompleta(options);
			$(".divEvalHallazgoInspeccion .btn-danger").hide();
			listFullEvaluacionesInspeccion.forEach(function(val,index){
				$("#subRealizarInspecciones").find(".contenidoSubList")
				.append(crearDivEvaluacionHallazgos(val,index));
				$("#hallInsMovil"+index+">div").hide();
				$("#hallInsMovil"+index+" .divNombreInformacion").show();
				var mostrarOpciones=true;
				
				 
				
				if(mostrarOpciones){
					$("#hallInsMovil"+index+" .divOpcionesGosst").show();
				}else{
					$("#hallInsMovil"+index+" .divOpcionesGosst").hide();
				}
				
				});
			if(lastHeightInspeccion!=0){
				$(window).scrollTop(lastHeightInspeccion-$("#barraMenu").height()-60);
				lastHeightInspeccion=0;
			}
			}else{ 
				
			}
		},null,null, verSeccionRestringida );
		
		 
	}
	
	if(inspeccionEvento.linkAuxiliarId2!=94 && inspeccionEvento.linkAuxiliarId2!=100 && inspeccionEvento.linkAuxiliarId2!=194){
	callAjaxPostNoUnlock(URL + '/auditoria/verificaciones', objInspeccion, procesarListaInspeccionMovil,null,null, verSeccionRestringida );
	}
}
function verTodoPreguntaInspeccion(){
	listFullEvaluacionesInspeccion.forEach(function(val,index){
		$("#pregInsMovil"+index).show()
	});
}
function verSoloAplicaPreguntaInspeccion(){
	listFullEvaluacionesInspeccion.forEach(function(val,index){
		$("#pregInsMovil"+index).show();
		if(val.bitAplica==1){
			 
		}else{
			$("#pregInsMovil"+index).hide()
		}
	});
}
function verSoloIncompletaPreguntaInspeccion(){
	listFullEvaluacionesInspeccion.forEach(function(val,index){
		$("#pregInsMovil"+index).show();
		if(val.respuesta==null ){
			if(val.bitAplica==0){
				$("#pregInsMovil"+index).hide() 
			}
		}else{
			$("#pregInsMovil"+index).hide() 
		}
	});
}
function cancelarObservacionAuditoriaMovil(){
	$("#btnCambObsAudi").show();
	$("#btnCancelbObsAudi").hide();
	$("#btnGuardarObsAudi").hide();
	$("#divObsAudiMovil").html(auditoriaObjMovil.observacion);
	
}
function guardarObservacionAuditoriaMovil(){
	$("#btnCambObsAudi").show();
	$("#btnCancelbObsAudi").hide();
	$("#btnGuardarObsAudi").hide();
	auditoriaObjMovil.observacion=$("#textObsAudiMovil").val();
	callAjaxPost(URL+"/auditoria/editar/basico",
			{auditoriaId:auditoriaObjMovil.auditoriaId,observacion:auditoriaObjMovil.observacion},
			function(data){
		
		$("#divObsAudiMovil").html(auditoriaObjMovil.observacion);
	});
}
function cambiarObservacionAuditoriaMovil(){
	$("#btnCambObsAudi").hide();
	$("#btnCancelbObsAudi").show();
	$("#btnGuardarObsAudi").show();
	
	$("#divObsAudiMovil").html("<textarea rows='4' class='form-control' id='textObsAudiMovil'></textarea>");
	$("#textObsAudiMovil").val(auditoriaObjMovil.observacion);
	
}
function ocultarAplicaAuditoria(){
	$("#modalAplicaAudi").modal("hide");
}
function verAplicaPreguntaInspeccion(){
	var dataParam = {
			auditoriaId:inspeccionEvento.idAux,
			auditoriaTipo:inspeccionEvento.linkAuxiliarId2
		};

		callAjaxPost(URL + '/auditoria/verificaciones', dataParam,
				function(data){
			var contenido ="<table id='tblAplicaAudi' class='table table-striped table-bordered table-hover'>" +
                 		"<thead><tr><td class='tb-acc'>...</td><td class='tb-acc'>Sección / Pregunta</td></tr></thead> " +
                 		"<tbody> </tbody></table>";
			var contenidoFinal="";
			var listContenido = [
                  
                 {sugerencia:"",label:"<button class='btn btn-danger' type='button' onclick='ocultarAplicaAuditoria()'><i class='fa fa-times'></i>Cancelar</button>",
                	 inputForm:"<button class='btn btn-success' type='button' onclick='guardarAplicaAuditoria()'><i class='fa fa-floppy-o'></i>Guardar</button>"},
			                     ];
			listContenido.forEach(function(val){
				contenidoFinal +=obtenerSubPanelModuloGeneral(val);
			});
			var modalOptions= {id:"modalAplicaAudi",nombre:"<i class='fa fa-check-square-o'></i>Preguntas aplica",
					contenido: contenido};
			crearModalPrincipalUsuario(modalOptions,function(){
				var tipoVerificacion = "";
				data.list.forEach(function(val){
					if (tipoVerificacion != val.tipoVerificacion) {
						tipoVerificacion = val.tipoVerificacion;
						$("#tblAplicaAudi tbody").append(
								"<tr style='border-bottom: 3px solid black;' class='info' id='filaCategoria"+val.tipoVerificacionId+"'>" +
								"<td >" +"<input checked type='checkbox' onclick='marcarTodoCategoriaInspeccionColaborador("+val.tipoVerificacionId+",0)'>" +
											"</td>"+
								"<td class='info'  onclick='verListaVerifColaborador("+val.tipoVerificacionId+")'>"
										+"SECCIÓN : "+ val.tipoVerificacion + "</td>"
										+ "</tr>");
					}
					$("#tblAplicaAudi tbody")
					.append("<tr id='traplic"+val.verificacionId+"'>" +
							"<td><input "+ val.chckAplica+" class='form-control' type='checkbox'></td> " +
							"<td>"+val.verificacion+"</td></tr>")
				});
				$("#tblAplicaAudi").after(contenidoFinal);
			},false,true);
		});
}
function verListaVerifColaborador(tipoId){
	listFullEvaluacionesInspeccion.forEach(function(val){
		if(val.tipoVerificacionId == parseInt(tipoId)){
		 $("#traplic"+val.verificacionId).toggle();
		}
	})
}
function guardarAplicaAuditoria(){
	var listAplica = [];
	listFullEvaluacionesInspeccion.forEach(function(val){
		val.bitAplica = ($("#traplic"+val.verificacionId).find("input").prop("checked")?1:0)
		 val.auditoriaId = inspeccionEvento.idAux
	});
	
	callAjaxPost(URL + '/auditoria/verificaciones/respuesta/aplica/save', listFullEvaluacionesInspeccion,
			function(data){
		switch (data.CODE_RESPONSE) {
		case "05":
			ocultarAplicaAuditoria();
			realizarInspeccionMovil();
			break;
		default:
			alert("Ocurrió un error al guardar la auditoria!");
		}
	});
}
function marcarTodoCategoriaInspeccionColaborador(catId,tipo){
	//$.blockUI({message:'<img src="../imagenes/gif/ruedita.gif"></img>cargando...'});
	if(parseInt(tipo)==0){
		//var r=confirm("¿Está seguro de desaplicar esta categoría?")
		//if(!r){$("#filaCategoria"+catId).find("input").prop("checked",parseInt(tipo)==0);$.unblockUI();return;}
		$("#filaCategoria"+catId).find("input").attr("onclick","marcarTodoCategoriaInspeccionColaborador("+catId+",1)")
			
	}else{
		//var r=confirm("¿Está seguro de aplicar esta categoría?")
		//if(!r){$("#filaCategoria"+catId).find("input").prop("checked",parseInt(tipo)==0);$.unblockUI();return;}
		$("#filaCategoria"+catId).find("input").attr("onclick","marcarTodoCategoriaInspeccionColaborador("+catId+",0)")
		
	}
	// setTimeout(function(){
		 listFullEvaluacionesInspeccion.forEach(function(val){
				if(val.tipoVerificacionId==parseInt(catId)){
					$("#traplic"+val.verificacionId).find("input").prop("checked",(parseInt(tipo)==1));
					//guardarCeldaDeshabilitarPreguntaCategoria(val.verificacionId,catId)
				}
			});
	//},1000);
}
var auditoriaObjMovil={};var listAreasInteresadasDetalleAuditoriaMovil=[],listSeccionesInteresadasDetalleAuditoriaMovil=[];
function procesarListaInspeccionMovil(data) {
	if(data.CODE_RESPONSE=="05"){
		$.unblockUI()
$("#subRealizarInspecciones").find(".contenidoSubList").html("");
	listFullEvaluacionesInspeccion=data.list;

	listAreasInteresadasDetalleAuditoriaMovil=data.areas;
	listSeccionesInteresadasDetalleAuditoriaMovil = data.secciones;
	auditoriaObjMovil=data.auditoriaAux[0];
	var completas=auditoriaObjMovil.numSi;
	$("#subRealizarInspecciones").find(".tituloSubList").html(
			textoBotonVolverContenidoColaborador+""+
			auditoriaObjMovil.auditoriaTipoNombre+"<br> Avance: "+
			auditoriaObjMovil.rptaRespondida+" / "+auditoriaObjMovil.numPreguntas);
	
	$("#subRealizarInspecciones").find(".contenidoSubList")
	.append("<div class='divInformacionCurso' style='    padding: 20px;'>" +
			"<strong>Detalle del Evento:</strong> <div id='divObsAudiMovil' style='white-space: pre'>"+auditoriaObjMovil.observacion+"</div><br>"+
			"<button id='btnCambObsAudi' class='btn btn-success' style='margin-bottom:0px' onclick='cambiarObservacionAuditoriaMovil()'><i class='fa fa-cog '></i> Modificar detalle del evento </button> <br>" +
			"<button id='btnCancelbObsAudi' class='btn btn-danger' style='display:none;margin-bottom:20px' onclick='cancelarObservacionAuditoriaMovil()'><i class='fa fa-times '></i> Cancelar </button> " +
			"<button id='btnGuardarObsAudi' class='btn btn-success' style='display:none;margin-bottom:20px'  onclick='guardarObservacionAuditoriaMovil()'><i class='fa fa-floppy-o '></i> Guardar</button> <br>" +
			
			"<button class='btn btn-success' onclick='verAplicaPreguntaInspeccion()'><i class='fa fa-check-square-o '></i> Elegir preguntas que aplican </button> 					  <br><br>" +
			"<button class='btn btn-success' onclick='verTodoPreguntaInspeccion()'><i class='fa fa-list '></i> Ver Todo </button> 					  <br><br>" +
			"<button class='btn btn-success' onclick='verSoloAplicaPreguntaInspeccion()'><i class='fa fa-indent '></i>  Ver Solo Aplica </button> <br><br>" +

			"<button class='btn btn-success' onclick='verSoloIncompletaPreguntaInspeccion()'><i class='fa fa-indent '></i>  Ver Solo Sin Responder </button>" +
			"</div>")
	listFullEvaluacionesInspeccion.forEach(function(val,index){ 
		$("#subRealizarInspecciones").find(".contenidoSubList")
		.append(crearDivEvaluacionInspeccion(val,index));
		
		
		$("#pregInsMovil"+index+">div").hide();
		$("#pregInsMovil"+index+" .divNombrePregunta").show();
		var mostrarOpciones=true;
		
		if(val.bitAplica==0){
			mostrarOpciones=false;
		}
		mostrarOpciones=false;
		if(mostrarOpciones){
			$("#pregInsMovil"+index+" .divOpcionesGosst").show();
		}else{
			$("#pregInsMovil"+index+" .divOpcionesGosst").hide();
		}
		
		});
	if(lastHeightInspeccion!=0){
		$(window).scrollTop(lastHeightInspeccion-$("#barraMenu").height()-60);
		lastHeightInspeccion=0;
	}
	}else{ 
		
	}
}
function guardarHallazgoInspeccion(){
	var inputDesc = $("#inputDescHall").val();
	var inputCausa = $("#inputCausaHall").val();
	var inputLugar = $("#inputLugarHall").val();
	var area = $("#selAreaHallazgo").val();
	var nivel= $("#selNivelHallazgo").val(); 
	var tipoReporte = $("#selTipoReporteHallazgo").val();
	
	var factor= $("#selFactorHallazgo").val();
	var secciones=$("#slcSeccionesPermitidaHallazgo").val();
	var listSecciones=[];
	if(secciones!=null){
		if($("#slcSeccionesPermitidaHallazgo").length>0){
			secciones.forEach(function(val,index){
				listSecciones.push({id:val});
			});
		}
	}
	
	var areas=$("#slcAreasPermitidaHallazgo").val();
	var listAreas=[];
	if(areas != null){
		if($("#slcAreasPermitidaHallazgo").length>0){
			areas.forEach(function(val,index){
				listAreas.push({areaId:val});
			});
		}
	}
	
	if(inputDesc.length == 0){
		alert("Descripción es obligatorio"); return;
	}
		var dataParam = {
			id : evaluacionInspeccionObj.id,
			factor:{id:factor},secciones:listSecciones,areas:listAreas,
			descripcion : inputDesc, tipoReporte: {id:tipoReporte},
			area:{areaId:area},
			nivel:{id:nivel},
			causa: inputCausa,
			lugar:inputLugar,
			auditoriaId: inspeccionEvento.idAux
		};

		callAjaxPost(URL + '/auditoria/hallazgo/save', dataParam,
				function(data){
			guardarEvidenciaAuto(data.nuevoId,"fileEviHallazgo",bitsEvidenciaHallazgo,
					"/auditoria/hallazgo/evidencia/save",realizarInspeccionMovil ,"hallazgoId");
		});
}
var textoBotonTogglePregunta="<button class='btn btn-success'  onclick='cambiarDivSubPregunta(this)'>" +
"<i class='fa fa-arrows-v fa-2x' aria-hidden='true'></i></button>";
var textoBotonToggleInformacion="<button class='btn btn-success'  onclick='cambiarDivSubInformacion(this)'>" +
"<i class='fa fa-arrows-v fa-2x' aria-hidden='true'></i></button>";
function cambiarDivSubPregunta(obj){
	$(obj).parent(".divNombrePregunta").siblings( ".divOpcionesGosst" ).toggle();
}
function cambiarDivSubInformacion(obj){
	$(obj).parent(".divNombreInformacion").siblings( "div" ).toggle();
}
function agregarSolicitudAccionMejora(indexEval,tipoId){
	evaluacionInspeccionObj=listFullEvaluacionesInspeccion[indexEval];
	evaluacionInspeccionObj.index = indexEval;
	gestionAccionAsociadaId=0;
	var asociadoAuxId=0;
	if(parseInt(tipoId)==2){
		asociadoAuxId=evaluacionInspeccionObj.detalleAuditoriaId
	}
	if(parseInt(tipoId)==6){
		asociadoAuxId=evaluacionInspeccionObj.id
	}
	var dataParam = {
			idCompany : getSession("gestopcompanyid"),
			asociadoTipo : parseInt(tipoId),
			asociadoId : asociadoAuxId
		};
	callAjaxPostNoUnlock(URL + '/gestionaccionmejora/items', dataParam,
				function(data){
		if(data.CODE_RESPONSE=="05"){
			$.unblockUI();
			var listTipoAccionMejora=data.listTipoAccionMejora; 
			var selCausaAcc = crearSelectOneMenuOblig("selCausaAcc", "", 
					listTipoAccionMejora.filter(function(val,index){
						var listPermitido=[1,2,3,4];
						return listPermitido.indexOf(parseInt(val.causaId))!=-1
					}), "-1", "causaId", "causaNombre","Seleccionar nueva acción de mejora");
			 
			 var listItemsEvalInsp=[ {sugerencia:"",label:"Nueva Solicitud Acción",inputForm:""},
			                         {sugerencia:"",label:"Origen de la solicitud"
			                   			 ,inputForm:selCausaAcc}, 
			                   		 {sugerencia:"Dentro de la solicitud se asignará  el plan de trabajo asociado",label:"Título de la solicitud"
			                   			 ,inputForm:"<input  id='inputNombreGestion' class='form-control' >"}
			                   		];
			 var textFormAcc=obtenerSubFormInspeccion(listItemsEvalInsp,"guardarGestionAsociada("+tipoId+","+asociadoAuxId+",1)",null,"Guardar solicitud y agregar plan de trabajo");
			 	$(".divInformacionCurso").hide();
				$("#pregInsMovil"+indexEval ).show();
				$("#pregInsMovil"+indexEval+" > div").hide();
				$("#pregInsMovil"+indexEval+" > .divNombrePregunta").show();
				$("#pregInsMovil"+indexEval+" > .divAsignarGestionAccion").show();
				$("#pregInsMovil"+indexEval+" > .divAsignarGestionAccion").html(textFormAcc);
 
				$("#hallInsMovil"+indexEval).show();
				$("#hallInsMovil"+indexEval+" > div").hide();
				$("#hallInsMovil"+indexEval+" > .divNombreInformacion").show();
				$("#hallInsMovil"+indexEval+" > .divAsignarGestionAccion").show();
				$("#hallInsMovil"+indexEval+" > .divAsignarGestionAccion").html(textFormAcc);
				
		}
		},null,null, verSeccionRestringida );
}
function guardarGestionAsociada(tipoId,asociadoAuxId,isNuevo) { 
	var dataParam = {
			idCompany : getSession("gestopcompanyid"),
		asociadoTipo : tipoId,
		asociadoId : asociadoAuxId,
		causaId:$("#selCausaAcc").val(),
		resumen:$("#inputNombreGestion").val(),
		estadoId:1,
		gestionAccionMejoraId : gestionAccionAsociadaId
	}; 
callAjaxPostNoUnlock(URL + '/gestionaccionmejora/items/save', dataParam,
			function(data){
		if(data.CODE_RESPONSE=="05"){
			$.unblockUI();
			if(parseInt(isNuevo) == 1){
				verAccionesMejoraInspeccion(evaluacionInspeccionObj.index,data.nuevoId)
				
			}else{
				realizarInspeccionMovil();
			}
			
		}
	},null,null, verSeccionRestringida);
}
function verAccionesMejoraInspeccion(indexEval,solicitudId,wantsOtro){
	var asociadoAuxId=0;
	evaluacionInspeccionObj=listFullEvaluacionesInspeccion[indexEval];
	evaluacionInspeccionObj.index =indexEval;
	var dataParam = {
			accionMejoraTipoId : 1,
			gestionAccionMejoraId : solicitudId
		};
		 
		callAjaxPost(URL + '/gestionaccionmejora/accionmejora', dataParam,
				function(data){
			var btnVolver="<button type='button' class='btn btn-danger' onclick='volverAccionesInpseccion()'><i class='fa fa-backward'></i>Volver</button>";
			var btnGuardarVolver="<button type='button' class='btn btn-success' onclick='guardarAccionInsp(0)'><i class='fa fa-floppy-o'></i>Guardar y volver</button>";
			var btnGuardarOtro="<button type='button' class='btn btn-success' onclick='guardarAccionInsp(1)'><i class='fa fa-floppy-o'></i>Guardar y agregar otra corrección</button>"
			var listItemsEvalInsp=[ {sugerencia:"",label:"Solicitud Acción  ",inputForm:"Acciones de mejora ("+data.list.length+")"},
			                        {clase:"classNuevo",sugerencia:"",label:"Descripción de la corrección",inputForm:"<input class='form-control' id='inputDescAccInsp'>"},
			                        {clase:"classNuevo",sugerencia:"",label:"Fecha planificada",inputForm:"<input class='form-control' id='datePlanifAccInsp' type='date'>"},
			                        {clase:"classNuevo",sugerencia:"",label:"Trabajador Responsable",inputForm:"",divContainer:"divTrabAccIns"},
			                        {clase:"classNuevo",sugerencia:"",label:"Evidencia",inputForm:"",divContainer:"divEviAccIns"},
			                        {clase:"classNuevo",sugerencia:"",label:""+btnVolver,inputForm:""+btnGuardarVolver+"<br><br>"+btnGuardarOtro,divContainer:""}];
			data.list.forEach(function(val,index){
				var btnBorrar="<button type='button' class='btn btn-danger' onclick='borrarAccionesInpseccion("+val.accionMejoraId+")'><i class='fa fa-trash'></i></button>";
				var btnDescarga ="<a target='_blank' href='"+URL+"/gestionaccionmejora/accionmejora/evidencia?accionMejoraId="+val.accionMejoraId+"' class='efectoLink'><i class='fa fa-download'></i>Descargar</a>"
				if(val.evidenciaNombre=="" || val.evidenciaNombre == "----"){
					btnDescarga="<strong style='color : red'>Sin evidencia</strong>";
				}
				listItemsEvalInsp.push({sugerencia:
					(val.fechaRevisionTexto==null?val.fechaRealTexto:val.fechaRevisionTexto)+" //  "+val.responsableNombre,
							label:""+btnBorrar+" #"+(index+1),inputForm:val.descripcion+"<br>"+btnDescarga+"<br>"} 
							 );
			});
			var textFormAcc=obtenerSubFormInspeccion(listItemsEvalInsp,"nuevaAccionMejoraInspeccion()",null,"Nuevo");
			 $(".divInformacionCurso").hide();
				$("#pregInsMovil"+indexEval ).show();
				$("#pregInsMovil"+indexEval+" > div").hide();
				$("#pregInsMovil"+indexEval+" > .divNombrePregunta").show();
				$("#pregInsMovil"+indexEval+" > .divAsignarGestionAccion").show();
				$("#pregInsMovil"+indexEval+" > .divAsignarGestionAccion").html(textFormAcc);
				 
				$("#hallInsMovil"+indexEval).show();
				$("#hallInsMovil"+indexEval+" > div").hide();
				$("#hallInsMovil"+indexEval+" > .divNombreInformacion").show();
				$("#hallInsMovil"+indexEval+" > .divAsignarGestionAccion").show();
				$("#hallInsMovil"+indexEval+" > .divAsignarGestionAccion").html(textFormAcc);

				crearSelectOneMenuObligUnitarioCompleto
				("selTrabAccionInsp", "", data.trabajadores, 
						 "trabajadorId","nombre","#divTrabAccIns","Trabajador");
				var options=
				{container:"#divEviAccIns",
						functionCall:function(){ },
						descargaUrl: "",
						esNuevo:true,
						idAux:"AccionInsp",
						evidenciaNombre:""};
				crearFormEvidenciaCompleta(options);
				volverAccionesInpseccion();
				if(wantsOtro == 1){
					nuevaAccionMejoraInspeccion();
				}
		});
}
function volverAccionesInpseccion(){
	$(".divAsignarGestionAccion .form-group").show();
	 $(".classNuevo").hide();
}
function nuevaAccionMejoraInspeccion(){
	$(".divAsignarGestionAccion .form-group").hide();
	 
	$(".classNuevo").show();
}
function borrarAccionesInpseccion(accionId){
	var r = confirm("¿Está seguro de eliminar la acción?");
	if (r == true) {
		var dataParam = {
				accionMejoraId : accionId
		};

		callAjaxPost(URL + '/gestionaccionmejora/accionmejora/delete',
				dataParam, function(data){
			verAccionesMejoraInspeccion(evaluacionInspeccionObj.index,evaluacionInspeccionObj.gestionAccionMejoraId);
		});
	}
}
function guardarAccionInsp(wantsOtro){
	wantsOtro = parseInt(wantsOtro);
	var descripcion = $("#inputDescAccInsp").val();
	var trabajadorResponsableId=$("#selTrabAccionInsp").val();
	var fechaPlanificada=$("#datePlanifAccInsp").val(); 
	
	if(descripcion.length == 0){
		alert("Descripción obligatoria");return
	}
	
	var dataParam = {
			accionMejoraId : 0,
			clasificacion : {id : 1},
			trabajadorResponsableId:trabajadorResponsableId,
		descripcion : descripcion,
		fechaRevision : fechaPlanificada,
		accionMejoraTipoId : 1,
		estadoId : 1,
		inversionAccion:0,
		companyId : getSession("gestopcompanyid"),
		gestionAccionMejoraId : evaluacionInspeccionObj.gestionAccionMejoraId	
		};
	callAjaxPost(URL + '/gestionaccionmejora/accionmejora/save', dataParam,
			function(data) {
				switch (data.CODE_RESPONSE) {
				case "05": 
					guardarEvidenciaAuto(data.nuevoId,"fileEviAccionInsp",
							bitsEvidenciaAccidente,
							'/gestionaccionmejora/accionmejora/evidencia/save',
							function(){
						console.log(evaluacionInspeccionObj);
						verAccionesMejoraInspeccion(evaluacionInspeccionObj.index,evaluacionInspeccionObj.gestionAccionMejoraId,wantsOtro);
						
					},"accionMejoraId");
				
					break;
				default:
					alert("Ocurrió un error al guardar la acción");
				}
			});
}
function seleccionarGAM(gestionID){
	gestionAccionAsociadaId=gestionID;
}
function asignarSolicitudAccionMejora(indexEval,tipoId){
	evaluacionInspeccionObj=listFullEvaluacionesInspeccion[indexEval];
	evaluacionInspeccionObj.index = indexEval
	var asociadoAuxId=0;
	if(parseInt(tipoId)==2){
		asociadoAuxId=evaluacionInspeccionObj.detalleAuditoriaId
	}
	if(parseInt(tipoId)==6){
		asociadoAuxId=evaluacionInspeccionObj.id
	}
	var dataParam = {
			idCompany : getSession("gestopcompanyid"),
			asociadoTipo : tipoId,
			asociadoId : asociadoAuxId
		};
		 
		callAjaxPost(URL + '/gestionaccionmejora/items', dataParam,
				function(data){
			var listItemsEvalInsp=[ {sugerencia:"",label:"Asignar Solicitud Acción",inputForm:""}]
			data.list.forEach(function(val,index){
				listItemsEvalInsp.push({sugerencia:"Debido a: "+val.causaNombre,
							label:"<input onclick='seleccionarGAM("+val.gestionAccionMejoraId+") ' class='form-control' type='radio' " +
	                   			 				"name='gestion' " +
	                   			 				"id='boxGestion"+val.gestionAccionMejoraId+"'  >"
			                   			 ,inputForm:"<label style='color:black'>"+val.resumen+"</label><br>"});
			});
			var textFormAcc=obtenerSubFormInspeccion(listItemsEvalInsp,"guardarGestionAsociada("+tipoId+","+asociadoAuxId+",0)");
			 $(".divInformacionCurso").hide();
				$("#pregInsMovil"+indexEval ).show();
				$("#pregInsMovil"+indexEval+" > div").hide();
				$("#pregInsMovil"+indexEval+" > .divNombrePregunta").show();
				$("#pregInsMovil"+indexEval+" > .divAsignarGestionAccion").show();
				$("#pregInsMovil"+indexEval+" > .divAsignarGestionAccion").html(textFormAcc);
				 
				$("#hallInsMovil"+indexEval).show();
				$("#hallInsMovil"+indexEval+" > div").hide();
				$("#hallInsMovil"+indexEval+" > .divNombreInformacion").show();
				$("#hallInsMovil"+indexEval+" > .divAsignarGestionAccion").show();
				$("#hallInsMovil"+indexEval+" > .divAsignarGestionAccion").html(textFormAcc);
		});
}

function crearDivEvaluacionHallazgos(contenidoVal,contenidoIndex){
	var listItemsEvalInsp=[
               {sugerencia:"",label:"Descripción",inputForm:"<input class='form-control' id='inputDescHall'> ",divContainer:""},
           		{sugerencia:"" ,inputForm:" ",label:"Evidencia" ,divContainer:"divEvidenciaEvalIns"},
	                   		 ];
	var btnEvidencia="<a target='_blank' class='btn btn-success' " +
	"href='"+URL+"/auditoria/hallazgo/evidencia?hallazgoId="+contenidoVal.id+"'>" +
	"<i class='fa fa-download'></i>Descargar Evidencia</a>";
	var btnEditar="<button class='btn btn-success'  onclick='editarHallazgoInspeccionMovil("+contenidoIndex+")'>" +
	"<i class='fa fa-pencil' aria-hidden='true'></i>Editar</button>";
	var btnEliminar="<button class='btn btn-danger'  onclick='eliminarHallazgoInspeccionMovil("+contenidoIndex+")'>" +
	"<i class='fa fa-times' aria-hidden='true'></i>Eliminar</button>";
	var btnAccion="<button class='btn btn-success'  onclick='agregarSolicitudAccionMejora("+contenidoIndex+",6)'>" +
	"<i class='fa fa-file' aria-hidden='true'></i>Crear Solicitud  Acción Mejora</button>";
	var btnAsignarAccion="<br><br><button class='btn btn-success'  onclick='asignarSolicitudAccionMejora("+contenidoIndex+",6)'>" +
	"<i class='fa fa-star' aria-hidden='true'></i>  Asignar Solicitud Existente</button>";
	contenidoVal.gestionAccionMejoraId =  contenidoVal.accion.gestionAccionMejoraId;
	var btnAccionesAgregar ="<button class='btn btn-success'  onclick='verAccionesMejoraInspeccion("+contenidoIndex+","+contenidoVal.accion.gestionAccionMejoraId+")'>" +
	"<i class='fa fa-plus' aria-hidden='true'></i>  Ver acciones de solicitud</button>";
	return 		"<div class='divInformacionCurso' id='hallInsMovil"+contenidoIndex+"' >" +
	"<div class='divNombreInformacion'>"+textoBotonToggleInformacion+
	(contenidoIndex+1)+". "+contenidoVal.descripcion+" / " +contenidoVal.tipoReporte.nombre+
			"<br> Área: "+contenidoVal.area.areaName +
			" / Nivel: "+contenidoVal.nivel.nombre+" / Solicitud Acción: "+contenidoVal.accion.resumen+
			"<br><label class='respuestaPregunta'>"+" Causa: "+contenidoVal.causa+" / Lugar específico: "+contenidoVal.lugar+"</label></div>" +
	    "<div class='divOpcionesGosst'  >" +
		    "<div class='opcionGosstCss'>"+
		    btnEvidencia+
		    "</div>" +
		    "<div class='opcionGosstCss'>"+
		    btnEditar+
		    "</div>" +
		    "<div class='opcionGosstCss'>"+
		    btnEliminar+
		    "</div>" +
		    "<div class='opcionGosstCss'>"+
		    btnAccion+" "+btnAsignarAccion+
		    "</div>" +
		    (contenidoVal.accion.gestionAccionMejoraId == null? "":
		    "<div class='opcionGosstCss'>"+
		    btnAccionesAgregar+
		    "</div>" )+
	    "</div>" +
		"<div class='divEvalHallazgoInspeccion'>" +
		//obtenerSubFormInspeccion(listItemsEvalInsp,contenidoIndex,"guardarEvaluacionInspeccion()")+
		"</div>" +
		"<div class='divAsignarGestionAccion'>" +
		"</div>" +
	"</div>" ;
}
function editarHallazgoInspeccionMovil(pindex){
	evaluacionInspeccionObj=listFullEvaluacionesInspeccion[pindex];
	$(".divInformacionCurso").hide();
	$("#divFormHallazgo").show();
	$("#divFormHallazgo").find(".divNombreInformacion").html("Editar Hallazgo "+evaluacionInspeccionObj.descripcion+textoBotonToggleInformacion);

	$("#inputDescHall").val(evaluacionInspeccionObj.descripcion);
	$("#inputCausaHall").val(evaluacionInspeccionObj.causa);
    $("#inputLugarHall").val(evaluacionInspeccionObj.lugar);
     $("#selAreaHallazgo").val(evaluacionInspeccionObj.area.areaId);
	 $("#selNivelHallazgo").val(evaluacionInspeccionObj.nivel.id);
	 $("#selFactorHallazgo").val(evaluacionInspeccionObj.factor.id);
	 
	 //
		var seccionesId=evaluacionInspeccionObj.seccionesId.split(",")
		listSeccionesInteresadasHallazgo.forEach(function(val){
			val.selected=0;
			seccionesId.forEach(function(val1){
				if(parseInt(val1)==val.id){
					val.selected=1;
				}
			})
		});	
		crearSelectOneMenuObligMultipleCompleto("slcSeccionesPermitidaHallazgo", "",
				listSeccionesInteresadasHallazgo,  "id", "nombre","#tdhasecc","Secciones interesadas ...");
		//
		var areasId=evaluacionInspeccionObj.areasId.split(",")
		listAreasInteresadasHallazgo.forEach(function(val){
			val.selected=0;
			areasId.forEach(function(val1){
				if(parseInt(val1)==val.areaId){
					val.selected=1;
				}
			})
		});	
		crearSelectOneMenuObligMultipleCompleto("slcAreasPermitidaHallazgo", "",
				listAreasInteresadasHallazgo,  "areaId", "areaName","#tdhaarea","Áreas interesadas ...");
		//
	 var options=
		{container:"#eviHallazgoMovil" ,
				functionCall:function(){ },
				descargaUrl: "",
				esNuevo:true,
				idAux:"Hallazgo",
				evidenciaNombre:evaluacionInspeccionObj.evidenciaNombre};
		crearFormEvidenciaCompleta(options);
		$(".divEvalHallazgoInspeccion .btn-danger").show();
}
function eliminarHallazgoInspeccionMovil(pindex){
	var r = confirm("¿Está seguro de eliminar el hallazgo?");
	if (r == true) {
		var dataParam = {
				id : listFullEvaluacionesInspeccion[pindex].id,
		};

		callAjaxPost(URL + '/auditoria/hallazgo/delete', dataParam,
				function(data){
			realizarInspeccionMovil();
				});
	}
}
function crearDivEvaluacionInspeccion(contenidoVal,contenidoIndex){
	var textoIn="";
	   
	var textoRespuesta="Sin evaluar",textoPotencial="",textoAplica="No Aplica";
	var textComentario="Comentario: "+contenidoVal.comentario;
	var textAccionMejora="Solicitud Acción Mejora: "+contenidoVal.nombreAccionMejora;
	switch(contenidoVal.respuesta){
	case 1:
		textoRespuesta=iconoGosst.aprobado+" Cumple"
		break;
	case 0:
		textoRespuesta=iconoGosst.desaprobado+" No Cumple"
		break;
	case 0.5:
		textoRespuesta=iconoGosst.advertir+"Cumple Parcialmente"
		
		break;
	}
	if(contenidoVal.bitRiesgo==1){
		textoPotencial="(Con riesgo potencial)"
	}
	if(contenidoVal.bitAplica==1){
		textoAplica="Aplica"
	}else{
		textoRespuesta="";
		textComentario="";textAccionMejora="";
	}
	var sinEvaluar=false;
	if(contenidoVal.respuesta==null){
		sinEvaluar=true;
		textoRespuesta="";
		textComentario="";textAccionMejora="";
	}
	var btnEvidencia="<a target='_blank' class='btn btn-success' " +
			"href='"+URL+"/auditoria/verificaciones/evidencia?" +
					"auditoriaId="+inspeccionEvento.idAux+"&verificacionId="+contenidoVal.verificacionId+"'>" +
			"<i class='fa fa-download'></i>Descargar Evidencia</a>";
	var btnAccion="<button class='btn btn-success'  onclick='agregarSolicitudAccionMejora("+contenidoIndex+",2)'>" +
	"<i class='fa fa-file' aria-hidden='true'></i>Crear Solicitud  Acción Mejora</button>";
	var btnAsignarAccion="<br><br><button class='btn btn-success'  onclick='asignarSolicitudAccionMejora("+contenidoIndex+",2)'>" +
	"<i class='fa fa-star' aria-hidden='true'></i>  Asignar Solicitud Existente</button>";
	var btnAccionesAgregar ="<button class='btn btn-success'  onclick='verAccionesMejoraInspeccion("+contenidoIndex+","+contenidoVal.gestionAccionMejoraId+")'>" +
	"<i class='fa fa-plus' aria-hidden='true'></i>  Ver acciones de solicitud</button>";
	textoIn="<div class='opcionGosstCss'  >"+
	"<button class='btn btn-success'  onclick='iniciarEvaluacionInspeccion("+contenidoIndex+")'>" +
	"<i class='fa fa-pencil' aria-hidden='true'></i>Editar Evaluación</button>"+
	"<br>"+"<br>" +
	"<button class='btn btn-danger'  onclick='cambiarAplicaEmpresaEvaluacionInspeccion("+contenidoIndex+",0)'>" +
	"<i class='fa fa-times' aria-hidden='true'></i>No Aplica a empresa</button>"+
	"</div>";
	textoIn+=  
			"<div class='opcionGosstCss'  >"+
			" "+btnEvidencia+
			"</div>"+
			"<div class='opcionGosstCss'  >"+
			btnAccion+btnAsignarAccion+
			"</div>";
	if(contenidoVal.gestionAccionMejoraId != null){
		textoIn+=  
			"<div class='opcionGosstCss'  >"+
			btnAccionesAgregar+
			"</div>"
	}
	if(contenidoVal.detalleAuditoriaId==null || sinEvaluar){
		textoIn="<div class='opcionGosstCss'  >"+
		"<button class='btn btn-success'  onclick='iniciarEvaluacionInspeccion("+contenidoIndex+")'>" +
		"<i class='fa fa-pencil' aria-hidden='true'></i>Evaluar item</button>"+
		"<br>"+"<br>" +
		"<button class='btn btn-danger'  onclick='cambiarAplicaEmpresaEvaluacionInspeccion("+contenidoIndex+",0)'>" +
		"<i class='fa fa-times' aria-hidden='true'></i>No Aplica a empresa</button>"+
		"</div>"
		 
	}
	if(contenidoVal.bitAplica==0){
		textoIn= "<div class='opcionGosstCss'  >"+
		"<button class='btn btn-success'   onclick='cambiarAplicaEmpresaEvaluacionInspeccion("+contenidoIndex+",1)'>" +
		"<i class='fa fa-check' aria-hidden='true'></i> Aplica a empresa</button>"+
		"</div>"
		 
	}
	var listItemsEvalInsp=[
	                   		 {sugerencia:"",label:"Si"
	                   			 ,inputForm:"<input onclick='verAcordeRespuesta()' class='form-control' type='radio' " +
	                   			 		"name='rpta"+contenidoVal.verificacionId+"' " +
	                   			 				"id='boxRptaSi"+contenidoVal.verificacionId+"' value='1'> ",divContainer:"divPregSi"}, 
	                   		{sugerencia:""
	                   			,inputForm:"<input onclick='verAcordeRespuesta()' class='form-control' type='radio' " +
	                   					"name='rpta"+contenidoVal.verificacionId+"' " +
	                   							"id='boxRptaParc"+contenidoVal.verificacionId+"' value='0.5'> ",label:"Parcial" ,divContainer:"divPregParcial"},
	                   		{sugerencia:"",
	                   				inputForm:"<input onclick='verAcordeRespuesta()' class='form-control' " +
	                   						"type='radio' name='rpta"+contenidoVal.verificacionId+"' " +
	                   								"id='boxRptaNo"+contenidoVal.verificacionId+"' value='0'> ",label:"No",divContainer:"divPregNo"},
	                   						
	                   		{sugerencia:"",inputForm:"<input class='form-control' type='checkbox' id='checkPotencial"+contenidoVal.verificacionId+"'  > ",label:"¿Riesgo Potencial?",divContainer:"divPregPotecial"},
	                   		{sugerencia:"" ,label:"Partes Interesadas área",inputForm: " ",divContainer:"divParteInteresada"+contenidoVal.verificacionId},
	                   		{sugerencia:"" ,label:"Partes Interesadas empresa",inputForm: " ",divContainer:"divParteInteresadaSeccion"+contenidoVal.verificacionId},
	                   		{sugerencia:"",label:"Comentario",inputForm:"<input class='form-control' id='inputComentarioInsp"+contenidoVal.verificacionId+"'> ",divContainer:""},
	                   		{sugerencia:""
	                   			,inputForm:" ",label:"Evidencia" ,divContainer:"divEvidenciaEvalIns"+contenidoVal.verificacionId},
	                   		 ];
	
	return 		"<div class='divInformacionCurso' id='pregInsMovil"+contenidoIndex+"' >" +
				"<div class='divNombrePregunta'>"+textoBotonTogglePregunta+
				(contenidoIndex+1)+".&nbsp;"+contenidoVal.verificacion+"" +
						"<br><label class='respuestaPregunta'>"+textoAplica+" / "+textoRespuesta+" "+textoPotencial+" / " +
								textComentario+" / "+textAccionMejora+"</label></div>" +
				    "<div class='divOpcionesGosst'  >"+
				   	textoIn+
				    "</div>" +
					"<div class='divEvalPreguntaInspeccion'>" +
					obtenerSubFormInspeccionNoAplica(listItemsEvalInsp,"guardarEvaluacionInspeccion()","guardarNoAplicaEvaluacion()")+
					"</div>" +
					"<div class='divAsignarGestionAccion'>" +
					"</div>" +
				"</div>" ;
}
function iniciarEvaluacionInspeccion(indexPreg){
	evaluacionInspeccionObj=listFullEvaluacionesInspeccion[indexPreg];
	lastHeightInspeccion=$("#pregInsMovil"+indexPreg).offset().top;
	$(".divInformacionCurso").hide();
	$("#pregInsMovil"+indexPreg ).show();
	$("#pregInsMovil"+indexPreg+" > div").hide();
	$("#pregInsMovil"+indexPreg+" > .divNombrePregunta").show();
	$("#pregInsMovil"+indexPreg+" > .divEvalPreguntaInspeccion").show();
	
	$("#boxRptaSi"+evaluacionInspeccionObj.verificacionId).prop("checked",(evaluacionInspeccionObj.bitSi==1))
	$("#boxRptaParc"+evaluacionInspeccionObj.verificacionId).prop("checked",(evaluacionInspeccionObj.bitParc==1))
	$("#boxRptaNo"+evaluacionInspeccionObj.verificacionId).prop("checked",(evaluacionInspeccionObj.bitNo==1))
	$("#checkPotencial"+evaluacionInspeccionObj.verificacionId).prop("checked",(evaluacionInspeccionObj.bitRiesgo==1))
	$("#inputParteInteresada"+evaluacionInspeccionObj.verificacionId).val(evaluacionInspeccionObj.personaInteresada);
	$("#inputComentarioInsp"+evaluacionInspeccionObj.verificacionId).val(evaluacionInspeccionObj.comentario);
	verAcordeRespuesta();
	var eviNombre=evaluacionInspeccionObj.evidenciaNombre;
	var options=
	{container:"#divEvidenciaEvalIns"+evaluacionInspeccionObj.verificacionId,
			functionCall:function(){ },
			descargaUrl: "/auditoria/verificaciones/evidencia?" +
					"auditoriaId="+inspeccionEvento.auditoriaId+"&verificacionId"+evaluacionInspeccionObj.verificacionId,
			esNuevo:true,
			idAux:"EvalInspeccion"+evaluacionInspeccionObj.verificacionId,
			evidenciaNombre:eviNombre};
	crearFormEvidenciaCompleta(options);
	//
	var areasId=evaluacionInspeccionObj.areasId.split(",")
	listAreasInteresadasDetalleAuditoriaMovil.forEach(function(val){
		val.selected=0;
		areasId.forEach(function(val1){
			if(parseInt(val1)==val.areaId){
				val.selected=1;
			}
		})
	});	
	crearSelectOneMenuObligMultipleCompleto("slcAreaPermitidaDetalleAuditoriaMovil"+evaluacionInspeccionObj.verificacionId, "",
			listAreasInteresadasDetalleAuditoriaMovil,  "areaId", "areaName","#divParteInteresada"+evaluacionInspeccionObj.verificacionId,"Partes interesadas ...");
	//
	var seccionesId=evaluacionInspeccionObj.seccionesId.split(",")
	listSeccionesInteresadasDetalleAuditoriaMovil.forEach(function(val){
		val.selected=0;
		seccionesId.forEach(function(val1){
			if(parseInt(val1)==val.id){
				val.selected=1;
			}
		})
	});	
	crearSelectOneMenuObligMultipleCompleto("slcSeccionPermitidaDetalleAuditoriaMovil"+evaluacionInspeccionObj.verificacionId, "",
			listSeccionesInteresadasDetalleAuditoriaMovil,  "id", "nombre","#divParteInteresadaSeccion"+evaluacionInspeccionObj.verificacionId,"Partes interesadas empresa...");
			
	
	
	
}

function cambiarAplicaEmpresaEvaluacionInspeccion(indexEvalIns,isAplica){
	evaluacionInspeccionObj=listFullEvaluacionesInspeccion[indexEvalIns];
	var dataParam = {
			verificacionId : evaluacionInspeccionObj.verificacionId,
			bitAplica : isAplica,
			bitRiesgo : evaluacionInspeccionObj.bitRiesgo,
			bitRespuesta : evaluacionInspeccionObj.bitRespuesta,
			
			referenciaLegal :evaluacionInspeccionObj.referenciaLegal,
			comentario : evaluacionInspeccionObj.comentario,
			auditoriaId : inspeccionEvento.idAux
		}; 
		callAjaxPost(URL + '/auditoria/verificaciones/respuesta/save', dataParam,
				function(data){
			$(".divListPrincipal > div").hide();
			$("#subRealizarInspecciones").show();
			var dataAux={};
			var listFinal=[];
			listFullEvaluacionesInspeccion.forEach(function(val){
				if(val.verificacionId==evaluacionInspeccionObj.verificacionId){
					var dataParamAux = {
							verificacionId : evaluacionInspeccionObj.verificacionId,
							bitAplica : isAplica,
							bitRiesgo : evaluacionInspeccionObj.bitRiesgo,
							bitRespuesta : evaluacionInspeccionObj.bitRespuesta,
							
							referenciaLegal :evaluacionInspeccionObj.referenciaLegal,
							comentario : evaluacionInspeccionObj.comentario,
							auditoriaId : inspeccionEvento.idAux
						}; 
					//val.bitRiesgo=dataParamAux.bitRiesgo;
					//val.respuesta=dataParamAux.bitRespuesta;
					//val.bitRespuesta=dataParamAux.bitRespuesta;
					val.detalleAuditoriaId=dataParamAux.detalleAuditoriaId;
					//val.personaInteresada=dataParamAux.personaInteresada;
					//val.comentario=dataParamAux.comentario;
					val.auditoriaId=dataParamAux.auditoriaId;
					val.detalleAuditoriaId=data.nuevoId;
					val.bitAplica=isAplica;
					
					listFinal.push(val);
				}else{
					listFinal.push(val);
				}
			});
			dataAux.CODE_RESPONSE="05";
			dataAux.list=listFinal;

			dataAux.areas=listAreasInteresadasDetalleAuditoriaMovil;
			dataAux.secciones = listSeccionesInteresadasDetalleAuditoriaMovil;
			dataAux.auditoriaAux=data.auditoriaAux;
			procesarListaInspeccionMovil(dataAux)
			
		});
}

function obtenerSubFormInspeccion(listForm,functionSumit,functionSumitNext,textoGuardar){
	var textFormIn="";
	if(textoGuardar == null){
		textoGuardar = "Guardar";
	}
	listForm.forEach(function(val,index){
		textFormIn+="<div class='form-group row "+val.clase+"'>"+
		   " <label for='colFormLabel' class='col-sm-4 col-form-label'>"+val.label+"</label>"+
		    "<div class='col-sm-8' style='color : black' id='"+val.divContainer+"'>"+
		     " "+val.inputForm+
		     "<small>"+val.sugerencia+" </small>"+
		    "</div>"+
		  "</div>";
	});
	textFormIn+="<div class='form-group row'>"+
	   " <label class='col-sm-4 col-form-label'>" +
	   "<button class='btn btn-danger' type='button'   onclick='volverEvaluacionGeneralInspeccion()'>" +
		"<i class='fa fa-backward' aria-hidden='true'></i> Volver</button>"+
	   "</label>"+
	    "<div class='col-sm-8' >" +
	    "<button class='btn btn-success' type='button'  onclick='"+functionSumit+"'>" +
		"<i class='fa fa-pencil-square-o' aria-hidden='true'></i> "+textoGuardar+"</button>"+
	    "</div>"+
	    (functionSumitNext == null ?"":
    	"<label class='col-sm-4 col-form-label'>" +
 	   
 	   "</label>"+
	    "<div class='col-sm-8' style='margin-top : 15px' >" +
	    "<button class='btn btn-success' type='button'  onclick='"+functionSumitNext+"'>" +
		"<i class='fa fa-pencil-square-o' aria-hidden='true'></i> Guardar y registrar nuevo hallazgo</button>"+
	    "</div>")+
	  "</div>"
	return "<form id='form' class='eventoGeneral'>"
			+textFormIn
			+"</form>"
}
function obtenerSubFormInspeccionNoAplica(listForm,functionSumit,functionNoAplica){
	var textFormIn=""
	listForm.forEach(function(val,index){
		textFormIn+="<div class='form-group row'>"+
		   " <label for='colFormLabel' class='col-sm-4 col-form-label'>"+val.label+"</label>"+
		    "<div class='col-sm-8' id='"+val.divContainer+"'>"+
		     " "+val.inputForm+
		     "<small>"+val.sugerencia+" </small>"+
		    "</div>"+
		  "</div>";
	});
	textFormIn+="<div class='form-group row'>"+
	   " <label class='col-xs-12 col-form-label' style='margin-bottom: 20px;'>" +
	   "<button class='btn btn-danger' type='button'   onclick='volverEvaluacionGeneralInspeccion()'>" +
		"<i class='fa fa-backward' aria-hidden='true'></i> Volver</button>"+
	   "</label>"+
	   "<div class='col-xs-4' >" +
	    "<button class='btn btn-warning' type='button'  onclick='"+functionNoAplica+"'>" +
		"<i class='fa fa-pencil-square-o' aria-hidden='true'></i> No aplica</button>"+
	    "</div>"+
	    "<div class='col-xs-8' style='text-align: right;'>" +
	    "<button class='btn btn-success' type='button'  onclick='"+functionSumit+"'>" +
		"<i class='fa fa-pencil-square-o' aria-hidden='true'></i> Guardar</button>"+
	    "</div>"+
	  "</div>"
	return "<form id='form' class='eventoGeneral'>"
			+textFormIn
			+"</form>"
}

function volverEvaluacionGeneralInspeccion(){
	var r= confirm("No se guardará la evaluación de esta pregunta, ¿está seguro de volver?");
	if(r){
		$(".divInformacionCurso").show(); 
		$(".divInformacionCurso > div").hide();
		$(".divInformacionCurso .divNombrePregunta").show();

		$(".divAsignarGestionAccion").html("");
		//$(".divInformacionCurso .divOpcionesGosst").show();
		
		$(".divInformacionCurso .divNombreInformacion").show();
		$(".divInformacionCurso .divEvalHallazgoInspeccion").show();
		$("#divFormHallazgo .divNombreInformacion").html(
		textoBotonToggleInformacion+
		"Nuevo Hallazgo" );
		evaluacionInspeccionObj={id:0};
		$(".divEvalHallazgoInspeccion .btn-danger").hide();
	
		//$(".divInformacionCurso form select")[0].selectedIndex = 0; 
		//$(".divInformacionCurso form select")[1].selectedIndex = 0; 
		//$(".divInformacionCurso form input").val("");
		
		$(window).scrollTop(lastHeightInspeccion-$("#barraMenu").height()-60);
		lastHeightInspeccion=0;
	}
}
function guardarNoAplicaEvaluacion(){
	var dataParam = {
			verificacionId : evaluacionInspeccionObj.verificacionId,
			bitAplica : 0,
			auditoriaId : inspeccionEvento.idAux
		};
	callAjaxPost(URL + '/auditoria/verificaciones/respuesta/save', dataParam,
			function(data){
		$(".divListPrincipal > div").hide();
		$("#subRealizarInspecciones").show();
		var dataAux={};
		var listFinal=[];
		listFullEvaluacionesInspeccion.forEach(function(val){
			if(val.verificacionId==evaluacionInspeccionObj.verificacionId){
				 val.bitAplica=0;
				
				listFinal.push(val);
			}else{
				listFinal.push(val);
			}
		});
		dataAux.CODE_RESPONSE="05";
		dataAux.list=listFinal;
		dataAux.areas=listAreasInteresadasDetalleAuditoriaMovil;
		dataAux.secciones = listSeccionesInteresadasDetalleAuditoriaMovil;
		dataAux.auditoriaAux=data.auditoriaAux;
		var indexUsado=0;
		listFullEvaluacionesInspeccion.forEach(function(val,index){
			if(val.verificacionId==evaluacionInspeccionObj.verificacionId){
				indexUsado=index
			}
		});
		if(indexUsado+1==listFullEvaluacionesInspeccion.length){
			procesarListaInspeccionMovil(dataAux);
			alert("Evaluación completa !")
		}else{
			procesarListaInspeccionMovil(dataAux);
			iniciarEvaluacionInspeccion(indexUsado+1)
		}
	})
}
function guardarEvaluacionInspeccion(){
	var bitRespuesta= parseFloat($("input[name='rpta"+evaluacionInspeccionObj.verificacionId+"']:checked").val());
	var areas=$("#slcAreaPermitidaDetalleAuditoriaMovil"+evaluacionInspeccionObj.verificacionId).val();
	var secciones =$("#slcSeccionPermitidaDetalleAuditoriaMovil"+evaluacionInspeccionObj.verificacionId).val();
	var listArea=[];
	if(areas != null){
		areas.forEach(function(val,index){
			listArea.push({areaId:val});
		});	
	}
	
	var listSecciones=[];
	if(secciones != null){
		secciones.forEach(function(val,index){
			listSecciones.push({id:val});
		});
	}
	if(listArea.length == 0 ){ listArea = null};
	if(listSecciones.length == 0 ){ listSecciones = null};
	if(bitRespuesta == null){
		alert("Respuesta de evaluación obligatoria"); return;
	}
	var dataParam = {
			verificacionId : evaluacionInspeccionObj.verificacionId,areasDetalle:listArea,secciones: listSecciones,
			bitAplica : 1,
			bitRiesgo : ($("#checkPotencial"+evaluacionInspeccionObj.verificacionId).prop("checked")?1:0),
			bitRespuesta : bitRespuesta,
			detalleAuditoriaId:evaluacionInspeccionObj.detalleAuditoriaId,
			personaInteresada :$("#inputParteInteresada"+evaluacionInspeccionObj.verificacionId).val(),
			comentario :$("#inputComentarioInsp"+evaluacionInspeccionObj.verificacionId).val(),
			auditoriaId : inspeccionEvento.idAux
		}; 
		callAjaxPost(URL + '/auditoria/verificaciones/respuesta/save', dataParam,
				function(data){
			guardarEvidenciaAuto(data.nuevoId,"fileEviEvalInspeccion"+evaluacionInspeccionObj.verificacionId
					,bitsEvidenciaVerificacion,"/auditoria/verificaciones/evidencia/save",
					function(){
				$(".divListPrincipal > div").hide();
				$("#subRealizarInspecciones").show();
				var dataAux={};
				var listFinal=[];
				listFullEvaluacionesInspeccion.forEach(function(val){
					if(val.verificacionId==evaluacionInspeccionObj.verificacionId){
						var dataParamAux = {
								verificacionId : evaluacionInspeccionObj.verificacionId,
								bitAplica : 1,areasDetalle:listArea,secciones: listSecciones,
								bitRiesgo : ($("#checkPotencial"+evaluacionInspeccionObj.verificacionId).prop("checked")?1:0),
								bitRespuesta : bitRespuesta,
								detalleAuditoriaId:evaluacionInspeccionObj.detalleAuditoriaId,
								personaInteresada :$("#inputParteInteresada"+evaluacionInspeccionObj.verificacionId).val(),
								comentario :$("#inputComentarioInsp"+evaluacionInspeccionObj.verificacionId).val(),
								auditoriaId : inspeccionEvento.idAux
							}; 
						val.bitRiesgo=dataParamAux.bitRiesgo;
						val.areasId="";
						if(dataParamAux.areasDetalle != null){
							dataParamAux.areasDetalle.forEach(function(val1){
								val.areasId+=""+val1.areaId+",";
							})
						}
						if(dataParamAux.secciones != null){
							val.seccionesId="";
							dataParamAux.secciones.forEach(function(val1){
								val.seccionesId+=""+val1.id+",";
							})
						}
						 
						val.respuesta=dataParamAux.bitRespuesta;
						val.bitRespuesta=dataParamAux.bitRespuesta;
						val.detalleAuditoriaId=dataParamAux.detalleAuditoriaId;
						val.personaInteresada=dataParamAux.personaInteresada;
						val.comentario=dataParamAux.comentario;
						val.auditoriaId=dataParamAux.auditoriaId;
						val.detalleAuditoriaId=data.nuevoId
						
						listFinal.push(val);
					}else{
						listFinal.push(val);
					}
				});
				dataAux.CODE_RESPONSE="05";
				dataAux.list=listFinal;
				dataAux.areas=listAreasInteresadasDetalleAuditoriaMovil;
				dataAux.secciones = listSeccionesInteresadasDetalleAuditoriaMovil;
				dataAux.auditoriaAux=data.auditoriaAux;
				var indexUsado=0;
				listFullEvaluacionesInspeccion.forEach(function(val,index){
					if(val.verificacionId==evaluacionInspeccionObj.verificacionId){
						indexUsado=index
					}
				});
				var indexSiguiente=0;var stopContar = false;
				listFullEvaluacionesInspeccion.forEach(function(val,index){
					if(indexUsado < index && val.bitAplica == 1 && !stopContar){
						indexSiguiente=index;
						stopContar = true;
					}
					if(!stopContar){
						indexSiguiente++;	
					};
				});
				if(indexSiguiente==listFullEvaluacionesInspeccion.length){
					procesarListaInspeccionMovil(dataAux);
					alert("Evaluación completa !")
				}else{
					
					procesarListaInspeccionMovil(dataAux);
					
					iniciarEvaluacionInspeccion(indexSiguiente);
					alert("Pregunta evaluada, pasando a la siguiente !")
				}
				
			} ,"verificacionId");
			 
		});
}
function verAcordeRespuesta(){
	var bitRespuesta= parseFloat($("input[name='rpta"+evaluacionInspeccionObj.verificacionId+"']:checked").val());
	if(bitRespuesta==1){
		$("#checkPotencial"+evaluacionInspeccionObj.verificacionId).attr("disabled",false);
	}else{
		$("#checkPotencial"+evaluacionInspeccionObj.verificacionId).prop("checked",false);
		$("#checkPotencial"+evaluacionInspeccionObj.verificacionId).attr("disabled",true);
	}
}


