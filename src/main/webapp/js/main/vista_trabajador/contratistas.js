/**
 * 
 */
var listFullProyectosTrabajador=[];

var indexFunction=0;
function toggleMenuOpcionContratista(obj,pindex,menu)
{
	indexFunction=pindex;
	$(obj).parent("#opciones"+menu).find("ul").toggle();
	$(obj).parent("#opciones"+menu).siblings().find("ul").hide();
}
var funcionalidadesContratista=[
	//Status
	{menu:"status",opcion: 
		[{id:"opcCargar",nombre:"<i class='fa fa-refresh' ></i> Cargar Status Documentos",
			functionClick:function(data){cargarStatusDocumentosProyectoColaborador(indexFunction);}}]
	},
	//Trabajadores
	{menu:"trabajador",opcion: 
		[{id:"opcTrabajadorAsig",nombre:"<i class='fa fa-table' aria-hidden='true'></i> Ver resumen",
				functionClick:function(data){verTablaResumenTrabProyecto(indexFunction);}}]
	},
	//Observacion
	{menu:"obs",opcion: 
		[{id:"opcVer",nombre:"<i class='fa fa-eye' ></i> Ver Reporte",
			functionClick:function(data){verCompletarObsColaborador(indexFunction); $("#opcOcultarobs").show();$("#opcVerobs").hide();}},
		{id:"opcOcultar",nombre:"<i class='fa fa-eye-slash' ></i> Ocultar Reporte",
			functionClick:function(data){verCompletarObsColaborador(indexFunction); $("#opcVerobs").show();$("#opcOcultarobs").hide();}},
		{id:"opcAgregar",nombre:"<i class='fa fa fa-plus' ></i> Agregar Reporte",
				functionClick:function(data){nuevaObservacionProyectoColaborador(indexFunction);}}	
		]
	},
	//Incidentes
	{menu:"inci",opcion: 
		[{id:"opcVer",nombre:"<i class='fa fa-eye' ></i> Ver Reporte",
			functionClick:function(data){verCompletarIncidenteColaborador(indexFunction); $("#opcOcultarinci").show();$("#opcVerinci").hide();}},
		{id:"opcOcultar",nombre:"<i class='fa fa-eye-slash' ></i> Ocultar Reporte",
			functionClick:function(data){verCompletarIncidenteColaborador(indexFunction); $("#opcVerinci").show();$("#opcOcultarinci").hide();}},
		{id:"opcAgregar",nombre:"<i class='fa fa fa-plus' ></i> Agregar Reporte",
				functionClick:function(data){nuevoIncidenteProyectoColaborador(indexFunction);}}	
		]
	},
	//Inspecciones
	{menu:"insp",opcion: 
		[{id:"opcVer",nombre:"<i class='fa fa-eye' ></i> Ver Reporte",
			functionClick:function(data){verCompletarInspeccionColabGenral(indexFunction,data); $("#opcOcultarinsp").show();$("#opcVerinsp").hide();}},
		{id:"opcOcultar",nombre:"<i class='fa fa-eye-slash' ></i> Ocultar Reporte",
			functionClick:function(data){verCompletarInspeccionColabGenral(indexFunction,data); $("#opcVerinsp").show();$("#opcOcultarinsp").hide();}}	
		]
	},
	//Auditorias
	{menu:"audi",opcion: 
		[{id:"opcVer",nombre:"<i class='fa fa-eye' ></i> Ver Reporte",
			functionClick:function(data){verCompletarAuditoriaColabGenral(indexFunction,data); $("#opcOcultaraudi").show();$("#opcVeraudi").hide();}},
		{id:"opcOcultar",nombre:"<i class='fa fa-eye-slash' ></i> Ocultar Reporte",
			functionClick:function(data){verCompletarAuditoriaColabGenral(indexFunction,data); $("#opcVeraudi").show();$("#opcOcultaraudi").hide();}}	
		]
	}
];
function marcarSubOpcionContratista(pindex,pindex1,pidAux)
{
	$("#opcionesstatus .listaGestionGosst").hide();  
	$("#opcionestrabajador .listaGestionGosst").hide();
	$("#opcionesobs .listaGestionGosst").hide(); 
	$("#opcionesinci .listaGestionGosst").hide(); 
	$("#opcionesinsp .listaGestionGosst").hide(); 
	$("#opcionesaudi .listaGestionGosst").hide(); 
	funcionalidadesContratista[pindex].opcion[pindex1].functionClick(pidAux);
}
var menuOpcionContratista=[];
function ListarOpciones()
{ 
		funcionalidadesContratista.forEach(function(val1,index1)
		{
			var menu=val1.menu;
			var opciones=val1.opcion;
			menuOpcionContratista[index1]="<ul class='list-group listaGestionGosst' style='width: 155px;top: 25px; display:none;'>";
			opciones.forEach(function(val2,index2)
				{
					if(val2.id==("opcOcultar"))
					{
						menuOpcionContratista[index1]+="<li id='"+val2.id+menu+"'class='list-group-item' style='display:none;text-align:left;' onclick='marcarSubOpcionContratista("+index1+","+index2+")'>"+val2.nombre+"</li>"
					}
					else{
						menuOpcionContratista[index1]+="<li id='"+val2.id+menu+"'class='list-group-item' style='text-align:left;' onclick='marcarSubOpcionContratista("+index1+","+index2+")'>"+val2.nombre+"</li>"
					}
			});
			menuOpcionContratista[index1]+="</ul>";
		});	 
} 
function habilitarProyectosContratistaUsuario(pproyectoId,ptipoId,pidAux){
	ListarOpciones(); 
	var trabajadorId=getSession("trabajadorGosstId");
	var dataParam={
			empresaId:getSession("gestopcompanyid"),
			responsable:{trabajadorId:trabajadorId}
			};
	callAjaxPost(URL + '/contratista/proyectos', dataParam, function(data) {
		switch (data.CODE_RESPONSE) {
		case "05":
			var listPanelesPrincipal=[];
			listFullProyectosTrabajador=data.proyectos;
			listFullProyectoSeguridad=data.proyectos;
			listEvaluacionStatusTotal=data.tipo_evaluacion;
			listFullProyectosTrabajador.forEach(function(val,index){
				var tipoProyecto=0;
				var condicionAgregar=(tipoProyecto==4?
						val.postulante.asignacion!=1:
							(val.estado.id==tipoProyecto
									&& val.postulante.asignacion==1) );
				condicionAgregar=true;
				if(condicionAgregar ){
					var textoAsignacion=(val.postulante.asignacion==1?"(Asignado)  ":"(No asignado)  ");
					var textoBoton="",textoBotonAct1="",textoBotonAct2="",textoBotonAct3="",textoBotonAct4="",textoBotonAct5="",
					textoBotonAudi="",textoBotonInsp="",textoBotonTrab,
					textoBotonObs="",textoBotonObsContr="",textoBotonForm="",
					textoBotonInci="",textoBotonInduccion="",textoBotonIperc="",
					textoBotonStatus="";
					 
					if(val.postulante.estado.id==1){
						textoBoton="Desde: "+val.postulante.fechaPlanificadaTexto;
					}
					if(val.postulante.estado.id==2){
						textoBoton="Hasta: "+val.postulante.fechaRealTexto+"<br>"+
						"<button class='btn btn-success' style='padding:5px' onclick='verCompletarProyecto("+index+");alterarTextoBoton(this,1) '>" +
							"<i class='fa fa-arrow-down' aria-hidden='true'></i>Ver</button>";
					}
					if(val.postulante.estado.id==3){
						textoBoton="Hasta: "+val.postulante.fechaRealTexto+"<br>"+
						"<button class='btn btn-success' style='padding:5px' onclick='verSinEditarCompletarProyecto("+index+");alterarTextoBoton(this,1) '>" +
						"<i class='fa fa-arrow-down' aria-hidden='true'></i>Ver</button>" ;
					}
					alterarTextoBoton();
					if(true){
						textoBotonObs=	
						"<div classs='row'>" +
							"<section   id='opcionesobs' class='col col-xs-12'>" +
								"<a class='efectoLink btn-gestion' onclick='toggleMenuOpcionContratista(this,"+index+",\"obs\")'>" +
									"Ver Opciones<i class='fa fa-angle-double-down' aria-hidden='true'></i>" +
								"</a>"+menuOpcionContratista[2]+
							"</section>"+
						"</div>";
					 
						textoBotonStatus=	
						"<div classs='row'>" +
							"<section   id='opcionesstatus' class='col col-xs-12'>" +
								"<a class='efectoLink btn-gestion' onclick='toggleMenuOpcionContratista(this,"+index+",\"status\")'>" +
									"Ver Opciones<i class='fa fa-angle-double-down' aria-hidden='true'></i>" +
								"</a>"+menuOpcionContratista[0]+
							"</section>"+
						"</div>";
						textoBotonTrab=
							"<div classs='row'>" +
								"<section   id='opcionestrabajador' class='col col-xs-12'>" +
									"<a class='efectoLink btn-gestion' onclick='toggleMenuOpcionContratista(this,"+index+",\"trabajador\")'>" +
										"Ver Opciones<i class='fa fa-angle-double-down' aria-hidden='true'></i>" +
									"</a>"+menuOpcionContratista[1]+
								"</section>"
							"</div>";
						
						textoBotonAudi=
							"<div classs='row'>" +
								"<section   id='opcionesaudi' class='col col-xs-12'>" +
									"<a class='efectoLink btn-gestion' onclick='toggleMenuOpcionContratista(this,"+index+",\"audi\")'>" +
										"Ver Opciones<i class='fa fa-angle-double-down' aria-hidden='true'></i>" +
									"</a>"+menuOpcionContratista[5]+
								"</section>"
							"</div>";
						textoBotonInsp=
									"<div classs='row'>" +
										"<section   id='opcionesinsp' class='col col-xs-12'>" +
											"<a class='efectoLink btn-gestion' onclick='toggleMenuOpcionContratista(this,"+index+",\"insp\")'>" +
												"Ver Opciones<i class='fa fa-angle-double-down' aria-hidden='true'></i>" +
											"</a>"+menuOpcionContratista[4]+
										"</section>"
									"</div>";
						
						textoBotonInci=
								"<div classs='row'>" +
									"<section   id='opcionesinci' class='col col-xs-12'>" +
										"<a class='efectoLink btn-gestion' onclick='toggleMenuOpcionContratista(this,"+index+",\"inci\")'>" +
											"Ver Opciones<i class='fa fa-angle-double-down' aria-hidden='true'></i>" +
										"</a>"+menuOpcionContratista[3]+
									"</section>"
								"</div>"
						; 
						
					}
					if(val.estado.id==2){
						textoBoton="Hasta: "+val.postulante.fechaRealTexto+"<br>"+
						"<button class='btn btn-success' style='padding:5px' onclick='verSinEditarCompletarProyecto("+index+");alterarTextoBoton(this,1) '>" +
						"<i class='fa fa-arrow-down' aria-hidden='true'></i>Ver</button>"; 
					}
					if(val.estado.id==3){
						textoBoton="Hasta: "+val.postulante.fechaRealTexto+"<br>"+
						"<button class='btn btn-success' style='padding:5px' onclick='verSinEditarCompletarProyecto("+index+");alterarTextoBoton(this,1) '>" +
						"<i class='fa fa-arrow-down' aria-hidden='true'></i>Ver</button>";
						textoBotonTrab="";textoBotonAct1="",textoBotonAct2="",textoBotonAct3="",textoBotonAct4="";
						textoBotonAudi="";textoBotonInsp="";
						textoBotonObs="",textoBotonObsContr="";textoBotonForm="";
						textoBotonInci="";textoBotonInduccion="",textoBotonIperc="";textoBotonStatus="";
					} 
					var filasRelevantesProyecto="" +
					"<tr title='Status de documentos' id='trStatusColab"+val.id+"'>" +
					"<td > "+" <i aria-hidden='true' class='fa fa-eye'></i> Status de documentos"+"" +
							"<div class='barraIndicadorProyecto' > </div></td>" + 
					"<td class='celdaIndicadorProyecto col col-xs-5'></td>" +
					"<td class='celdaBotonAccion'>"+textoBotonStatus+"</td>"+
					"</tr>" +
					"<tr class='detalleStatusDocColaborador'></tr>"+
					"<tr title='Trabajadores Asignados' id='trTrabProyecto"+val.id+"'>" +
					"<td > "+" <i aria-hidden='true' class='fa fa-sort-amount-asc'></i> Trabajadores Asignados"+"" +
							"<div class='barraIndicadorProyecto' " +
								"style='width:"+pasarDecimalPorcentaje(val.postulante.indicadorTrabajadoresDecimal)+" ;" +
										"background-color:"+colorPuntajeGosst(val.postulante.indicadorTrabajadoresDecimal)+"'></div></td>" + 
					"<td class='celdaIndicadorProyecto'> "+val.postulante.indicadorTrabajadores+" </td>" +
					"<td>"+textoBotonTrab+"</td>"+
					"</tr>" +
					"<tr class='detalleTrabajadoresProyecto'></tr>"+
					 
					 
					"<tr title='Reportar Observación' id='trObsColab"+val.id+"'>" +
					"<td > "+" <i aria-hidden='true' class='fa fa-eye'></i> Reportar Observación"+"" +
							"<div class='barraIndicadorProyecto' " +
								"style='width:"+pasarDecimalPorcentaje(val.postulante.indicadorObservacionesDecimal)+" ;" +
										"background-color:"+colorPuntajeGosst(val.postulante.indicadorObservacionesDecimal)+"'></div></td>" + 
					"<td class='celdaIndicadorProyecto'> "+val.postulante.indicadorObservaciones+" </td>" +
					"<td>"+textoBotonObs+"</td>"+
					"</tr>" +
				 "<tr class='detalleObservacionesColaborador'></tr>"+
					"<tr title='Ver Incidentes'>" +
					"<td > "+" <i aria-hidden='true' class='fa fa-warning'></i> Ver Incidentes"+"" +
							"<div class='barraIndicadorProyecto' " +
								"style='width:"+pasarDecimalPorcentaje(val.postulante.indicadorIncidentesDecimal)+" ;" +
										"background-color:"+colorPuntajeGosst(val.postulante.indicadorIncidentesDecimal)+"'></div></td>" + 
					"<td > "+val.postulante.indicadorIncidentes+" </td>" +
					"<td>"+textoBotonInci+"</td>"+
					"</tr>" + 
					"<tr class='detalleIncidentesProyecto'></tr>"+
					
					
					"<tr title='Realizar Inspecciones  '>" +
					"<td > "+" <i aria-hidden='true' class='fa fa-check-square-o'></i> Realizar Inspecciones  "+"" +
							"<div class='barraIndicadorProyecto' " +
								"style='width:"+pasarDecimalPorcentaje(val.postulante.indicadorHallazgosDecimal)+" ;" +
										"background-color:"+colorPuntajeGosst(val.postulante.indicadorHallazgosDecimal)+"'></div></td>" + 
					"<td > "+
					val.postulante.indicadorHallazgos+
					" </td>" +
					"<td>"+textoBotonInsp+"</td>"+
					"</tr>" +
					"<tr class='detalleInspeccionProyecto'></tr>"+
					"<tr title='Realizar Auditorías  '>" +
					"<td > "+" <i aria-hidden='true' class='fa fa-check-square-o'></i> Realizar Auditorías  "+"" +
							"<div class='barraIndicadorProyecto' " +
								"style='width:"+pasarDecimalPorcentaje(val.postulante.indicadorAuditoriaGeneralDecimal)+" ;" +
										"background-color:"+colorPuntajeGosst(val.postulante.indicadorAuditoriaGeneralDecimal)+"'></div></td>" + 
					"<td > "+
					val.postulante.indicadorAuditoriaGeneral+
					" </td>" +
					"<td>"+textoBotonAudi+"</td>"+
					"</tr>" +
					"<tr class='detalleAuditoriasProyecto'></tr>"+
				 
					"";
					
					var puntajeProyecto=0.0;
					puntajeProyecto=val.postulante.indicadorDocumentosDecimal+
					val.postulante.indicadorFormacionesDecimal+
					
					val.postulante.indicadorActividadesDecimal1+
					val.postulante.indicadorActividadesDecimal2+
					val.postulante.indicadorActividadesDecimal3+
					val.postulante.indicadorActividadesDecimal4+
					
					val.postulante.indicadorObservacionesDecimal+
					val.postulante.indicadorAuditoriaGeneralDecimal+
					val.postulante.indicadorIncidentesDecimal+
					val.postulante.indicadorTrabajadoresDecimal +
					val.postulante.indicadorIpercDecimal 
					puntajeProyecto=puntajeProyecto/11;
					switch(tipoProyecto){
					case 1:
						
					break;
					case 4:
						puntajeProyecto=val.postulante.indicadorDocumentosDecimal;
						filasRelevantesProyecto="";
						break;
					}
					var descripcionProy="<div  class='eventoGeneral'>" +
					"<div id='tituloEvento'>" +
					"<table class='table table-movil' id='proyectoTable"+val.id+"'>" +
					"<tbody>" +
					"<tr>" +
					"<td > "+" <i aria-hidden='true' class='fa fa-user'></i> Contratista"+"</td>" + 
					"<td colspan='2'>  "+val.contratistaNombre+" </td>" + 
					"</tr>" +
					"<tr>" +
						"<td > "+"<i aria-hidden='true' class='fa fa-hourglass-start'></i>Fecha Inicio"+"</td>" + 
						"<td colspan='2'>  "+val.fechaInicioTexto+ "</td>" +
						 
					"</tr>" +
					"<tr>" +
						"<td > "+"<i aria-hidden='true' class='fa fa-hourglass-end'></i>Fecha Fin"+"</td>" + 
						"<td colspan='2'>   "+val.fechaFinTexto+"</td>" + 
					"</tr>" +
						"<tr>" +
						"<td > "+" <i aria-hidden='true' class='fa fa-info'></i> Descripción"+"</td>" + 
						"<td colspan='2'> "+val.descripcion+" </td>" + 
					"</tr>" +
					 
					filasRelevantesProyecto+
					"</tbody>" +
					"</table>"+ 
					"</div>"+
					"</div>";
					var iconoAlerta="";
				if(val.hasNotificacionPrograma==1 || 
						val.hasNotificacionPlan==1|| 
						val.hasNotificacionResiduo==1|| 
						val.hasNotificacionDocumento==1|| 
						val.hasNotificacionObservacionContratista==1|| 
						val.hasNotificacionIncidente==1|| 
						val.hasNotificacionAuditoria==1|| 
						val.hasNotificacionInspeccion==1|| 
						val.hasNotificacionIperc==1){
					iconoAlerta="<i style='color:red; font-size: 1.2em;' class='fa fa-exclamation-circle'></i>"
				}
						listPanelesPrincipal.push(
						{id:"pro"+val.id,clase:"divProyectoGeneral",proyectoId:val.id,
							nombre:iconoAlerta+""+val.titulo+" (Contratista: "+val.contratistaNombre+")"+" ("+pasarDecimalPorcentaje(val.notaFinal)+")",
							contenido:descripcionProy});
				}
			});
			$(".divTituloFull h4").append(" - "+listPanelesPrincipal.length+" proyecto(s) ")
			agregarPanelesDivPrincipalColaborador(listPanelesPrincipal);
			if(pproyectoId>0){
				pproyectoId=parseInt(pproyectoId);ptipoId=parseInt(ptipoId);
				$("#pro"+pproyectoId).find(".contenidoSubList").show();
				
				listFullProyectosTrabajador.forEach(function(val,index){
					if(val.estado.id == 2){
						if(val.id==pproyectoId){
							indexFunction=index;
							if(ptipoId==2){
								marcarSubOpcionContratista(4,0,pidAux)
							}
							if(ptipoId==3){
								marcarSubOpcionContratista(5,0,pidAux)
							}
						}
					}
				});
				var el=$("#pro"+pproyectoId).find(".detalleInspeccionProyecto");
				if(ptipoId==3){
					el=$("#pro"+pproyectoId).find(".detalleAuditoriasProyecto")
				}
				
				var bottom = el.position().top + el.outerHeight(true);
				
				$(window).scrollTop(bottom)
				
			}
			
			var textObservacion="";
			var listItemsFormObservacion=[
				{sugerencia:"",label:"Acción de mejora",inputForm:"",divContainer:"divDescAccColab"},
				{sugerencia:"",label:"Fecha planificada",inputForm:"",divContainer:"divFechaAccColab"},
				{sugerencia:"",label:"Observación del contratista",inputForm:"",divContainer:"divObsAccColab"},
				{sugerencia:"",label:"Evidencia",inputForm:" ",divContainer:"eviAccionMejora"}, 
				{sugerencia:"",label:"Fecha Realizada",inputForm:"",divContainer:"divFechaRealAccColab"},
				{sugerencia:"Si está todo conforme, dejar en blanco",label:"Respuesta de cliente",inputForm:"<input class='form-control' id='inpuRespAccColaborador'>",divContainer:"divFechaRealAccColab"},
				{sugerencia:"",label:" ",inputForm:"<button type='submit' class='btn btn-success' ><i aria-hidden='true' class='fa fa-paper-plane'></i>Enviar</button>"} 
  		 			];
			listItemsFormObservacion.forEach(function(val,index){
				textObservacion+=obtenerSubPanelModulo(val); 
			});
			
			var listEditarPrincipal=[ 
			{id:"editarMovilAccProyColab" ,clase:"contenidoFormVisible",
				nombre:""+"Editar Acción de mejora",
				contenido:"<form id='formEditarAccionColab' class='eventoGeneral'>"+textObservacion+"</form>"}
			];
			
			
			
			agregarPanelesDivPrincipalColaborador(listEditarPrincipal);
			$(".contenidoFormVisible").hide();
			 $('#formEditarAccionColab').on('submit', function(e) {  
			        e.preventDefault();   
			        guardarAccionProyectoColaborador();
			 }); 
			break;
		}
	});
	$("#opcAudiOcultar").hide();
}
 

function volverVistaMovil(){
	 contadorFijoY=0;
	 $(".divTituloGeneral h4").html(tituloAnteriorContratista);
	 $("#divIpercTabla").remove();
	 $("#divTrabProyTabla").remove();
	 $("#divObsProyTabla").remove();
	 $("#divStatDocTabla").remove();
	 $(".divTablaVisual").remove();
	$(".divContainerGeneral").show();
}