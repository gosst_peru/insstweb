var objObservacionTrabajador={};
var listFullObservacionTrabajador; 
var listTipoFactoresObsTrab;
var listTipoPerdidaObsTrab;
var listTipoReporteObsTrab;
var listTipoProyectoObsTrab;
function toggleMenuOpcionObsTrab(obj,pindex){
	objObservacionTrabajador=listFullObservacionTrabajador[pindex];
	objObservacionTrabajador.index=pindex;
	$(obj).parent(".eventoGeneral").find("ul").toggle();
	$(obj).parent(".eventoGeneral").siblings().find("ul").hide(); 	
}
var funcionalidadesObsTrab=[ 
{id:"opcDescarga",nombre:"Descargar",functionClick:function(data){
	window.open(URL+"/contratista/observacion/evidencia?observacionId="+objObservacionTrabajador.id,'_blank')
	}  },
	
    {id:"opcEditar",nombre:"Editar",functionClick:function(data){editarObservacionTrabajador()}  },
   {id:"opcElimnar",nombre:"Eliminar",functionClick:function(data){eliminarObservacionTrabajador()}  
   
    
    }
                           ]
function marcarSubOpcionObsTrab(pindex){
	funcionalidadesObsTrab[pindex].functionClick();
	$(".subDetalleAccion ul").hide();
	$(".listaGestionGosst").hide();
}
var escondioAvisoObs=true;
function eliminarAvisoObsGosst(){
	$(".gosst-aviso").remove();
	escondioAvisoObs=true;
}
function habilitarObservacionesColaborador(contratistaEdicionId,proyectoEdicionId){
	var textoConfidencial="Estimado colaborador: " 
		+"<br><br>En conformidad con la Ley N° 29783 “Ley de Seguridad y Salud en el trabajo”, el siguiente reporte es de carácter anónimo y los análisis serán confidenciales y evaluados de manera global. Para garantizar ello, el equipo GOSST ha tomado las medidas necesarias para proteger la información que ingresen los colaboradores, no pudiendo relacionarse un reporte particular con un colaborador determinado. Una vez guardada, no quedará un rastro electrónico que permita asociar a los colaboradores con sus reportes. Cualquier duda sobre este aspecto no dude en consultarnos al correo adrian.cox@aswan.pe" 
		+"<br><br>Muchas gracias por su participación. "
		+"<br><br>GOSST"
		+"<br><br>Tu Seguridad, Nuestra Cultura." +
		"<br><br><button class='btn btn-success' onclick='eliminarAvisoObsGosst()'>Esconder aviso</button>";
	$(".gosst-aviso").remove();
if(!escondioAvisoObs){
$(".divTituloFull").append("<div class='gosst-aviso'>"+textoConfidencial+"</div>"); 
 

}
	var dataParam={
			trabajadorId:getSession("trabajadorGosstId")
			};
	objObservacionTrabajador={id:0};
	var listPanelesPrincipal=[];
	$(".divListPrincipal").html("")
	callAjaxPost(URL + '/contratista/colaborador/observaciones', dataParam, function(data) {
		switch (data.CODE_RESPONSE) {
		case "05":
			 
			
			
			listTipoFactoresObsTrab=data.listFactores;
			listTipoPerdidaObsTrab=data.listPerdida;
			listTipoReporteObsTrab=data.listReporte;
			listTipoProyectoObsTrab=data.proyectos;
			var listContratistaProyectoObs=[];var listIdContratista=[];
			listTipoProyectoObsTrab.forEach(function(val){
				if(listIdContratista.indexOf(val.contratistaId)==-1){
					listIdContratista.push(val.contratistaId);
					listContratistaProyectoObs.push({id:val.contratistaId,nombre:val.contratistaNombre})
				}
			});
			var slcContratistaProyectoObs=crearSelectOneMenuOblig("slcContratistaProyectoObs", "verAcordeContratistaObsColab()",listContratistaProyectoObs, 
					contratistaEdicionId, "id","nombre");
			
			var slcTipoProyectoObs=crearSelectOneMenuOblig("slcTipoProyectoObs", "",listTipoProyectoObsTrab, 
					proyectoEdicionId, "id","titulo");
			var slcTipoReporteObs=crearSelectOneMenuOblig("slcTipoReporteObs", "verAcordeTipoReporteActo()",listTipoReporteObsTrab, 
					 "", "id","nombre");
			listTipoPerdidaObsTrab=listTipoPerdidaObsTrab.filter(function(val){
				if(val.id!=1){
					return true;
				}else{
					return false;
				}
			});
			var slcTipoPerdidaObs=crearSelectOneMenuOblig("slcTipoPerdidaObs", "",listTipoPerdidaObsTrab, 
					 "", "id","nombre");
			var listItemsExam=[  
			   				{sugerencia:"",label:"Contratista",inputForm:slcContratistaProyectoObs,divContainer:""},
							{sugerencia:"",label:"Proyecto",inputForm:slcTipoProyectoObs,divContainer:"divProyectoObsColab"},
				{sugerencia:"",label:"Tipo de reporte",inputForm:slcTipoReporteObs,divContainer:""},
				 {sugerencia:"",label:"Descripción",inputForm:"<input   class='form-control' id='inputDescObsTrab'>"},
				
				{sugerencia:"",label:"Nivel de riesgo",inputForm:slcTipoPerdidaObs,divContainer:"divNivelRiesgo"}, 
       			 {sugerencia:"",label:"Evidencia",inputForm:"",divContainer:"eviObservacionTrabajador"},
       			 {sugerencia:"",label:"",inputForm:"<button type='submit' class='btn btn-success' ><i aria-hidden='true' class='fa fa-paper-plane'></i>Enviar</button>"} 
             		];
			var textFormExamen="";
			listItemsExam.forEach(function(val,index){
				textFormExamen+=obtenerSubPanelModulo(val);
			});
			listPanelesPrincipal.push(
					{id:"nuevoMovilObsTrab" ,clase:"",
							nombre:""+"Nueva reporte de observación",
							contenido:"<form id='formNuevoObsTrab' class='eventoGeneral'>"+textFormExamen+"</form>"}
							//	{id:"editarMovilObsTrab" ,clase:"contenidoFormVisible",
								//		nombre:""+"Editar ObsTrab",
						//		contenido:"<form id='formEditarObsTrab' class='eventoGeneral'>"+textFormExamen+"</form>"},
					 
						
						//	{id:"ObservacionesSinAtender",clase:"divObservacionTrabajadorGeneral",
							//		nombre:"Reportes Sin atender"+" (" +numSin+")",
							//		contenido:textSinAtender},
							//{id:"ObservacionesAtenderSinRespuesta",clase:"divObservacionTrabajadorGeneral",
								//	nombre:"Reportes Atendidos Sin Implementar"+" (" +numConSinRespuesta+")",
						//	contenido:textAtenderSinRespuesta} ,
					//{id:"ObservacionesAtenderConRespuesta",clase:"divObservacionTrabajadorGeneral",
						//	nombre:"Reportes Atendidos e Implementados"+" (" +numConConRespuesta+")",
						//	contenido:textAtenderConRespuesta} 
						
			
			);
			
			agregarPanelesDivPrincipal(listPanelesPrincipal);verAcordeContratistaObsColab(proyectoEdicionId);
			verAcordeTipoReporteActo();
			var options=
			{container:"#eviObservacionTrabajador",
					functionCall:function(){ },
					descargaUrl: "",
					esNuevo:true,
					idAux:"ObsTrabajador"+objObservacionTrabajador.id,
					evidenciaNombre:""};
			crearFormEvidenciaCompleta(options);
			$(".listaGestionGosst").hide();
			$(".contenidoFormVisible").hide();
			 $('#formNuevoObsTrab').on('submit', function(e) {  
			        e.preventDefault();
			        objObservacionTrabajador={id:0}
			        guardarObservacionTrabajador();
			 });
			 $('#formEditarObsTrab').on('submit', function(e) {  
			        e.preventDefault(); 
			        guardarObservacionTrabajador();
			 });
			 if(listTipoProyectoObsTrab.length==0){
				 $("#formNuevoObsTrab").html("Sin proyectos pendientes")
			 }
			break;
			default:
				alert("nop");
				break;
		}
	})
}
function verAcordeContratistaObsColab(proyectoEdicionId){
	var contratistaId=$("#slcContratistaProyectoObs").val();
	var slcTipoProyectoObs=crearSelectOneMenuOblig("slcTipoProyectoObs", "",listTipoProyectoObsTrab.filter(function(val){return val.contratistaId==contratistaId}), 
			proyectoEdicionId, "id","titulo");
	$("#divProyectoObsColab").html(slcTipoProyectoObs)
	
}
function verAcordeTipoReporteActo(){
	var tipoId=parseInt($("#slcTipoReporteObs").val());
	$("#divNivelRiesgo").parent().show();
	if(tipoId==3){
		$("#divNivelRiesgo").parent().hide();
	}
} 
function guardarObservacionTrabajador(){
	var formDiv=$("#editarMovilObsTrab");
	if(objObservacionTrabajador.id==0){
		formDiv=$("#nuevoMovilObsTrab");
	}
	var campoVacio = true;
	var descripcion=formDiv.find("#inputDescObsTrab").val();  
	var fecha=new Date();
	var tipo=parseInt(formDiv.find("#slcTipoReporteObs").val());
	var factor=4;
	var perdida=formDiv.find("#slcTipoPerdidaObs").val();
	var proyectoId=formDiv.find("#slcTipoProyectoObs").val(); 
	var lugar=formDiv.find("#inputLugarObsTrab").val();
	var contratistaObj={id:getSession("contratistaGosstId")};
	var quienId=formDiv.find("#slcTipoDirigidoObs").val();
	if(parseInt(quienId)==0){
		contratistaObj={id:null}
	}
	var isactivo=comprobarFieInputExiste("fileEviObsTrabajador"+objObservacionTrabajador.id);
	if(isactivo.isValido){
		
	}else{
		alert(isactivo.mensaje)
		return;
	}
	var nivel=1;
	if(tipo==3){
		nivel=2;
	}
		if (campoVacio) {

			var dataParam = {
					id : objObservacionTrabajador.id, 
					proyectoId: proyectoId ,tipo:{id:1},
					descripcion:descripcion,
					nivel:nivel,
					fecha:fecha,hora:obtenerHoraActual(),
					perdida:{id:perdida},
					factor:{id:factor},
					reporte:{id:tipo}, 
					//trabajador:{id:getSession("trabajadorGosstId")},
					contratistaAsociado:{id:null}
			}

			callAjaxPost(URL + '/contratista/observacion/save', dataParam,
					function(data) {
						switch (data.CODE_RESPONSE) {
						case "05":  
							alert("Se guardó la observación y se notificó al contratista. " +
									"Revisar la sección de Proyectos para ver las acciones que tomó el contratista");
							guardarEvidenciaAuto(data.nuevoId,"fileEviObsTrabajador"+objObservacionTrabajador.id
									,bitsEvidenciaAccionMejora,"/contratista/observacion/evidencia/save",
									function(){
								habilitarObservacionesColaborador($("#slcContratistaProyectoObs").val(),proyectoId);
							},"observacionId"); 
							break;
						default:
							alert("Se guardó la observación, pero no se encontró el correo del responsable o hubo otro problema técnico")
							console.log("Ocurrió un error al guardar   !");
						}
					});
			 
		} else {
			alert("Debe ingresar todos los campos.");
		}
}
 