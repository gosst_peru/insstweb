/**
 * 
 */
var listFullStatusDocumentos=[];
var listFullStatusExamenes=[];
var listEvaluacionStatusTotal=[];

var indexFunctionStatus=0;
function toggleMenuOpcionStatus(obj,pindex,menu)
{
	indexFunctionStatus=pindex;
	$(obj).parent("#opciones"+menu).find("ul").toggle();
	$(obj).parent("#opciones"+menu).siblings().find("ul").hide();
}
var funcionalidadesStatus=[
	{menu:"0",opcion: 
		[{id:"opcShow",nombre:"<i class='fa fa fa-eye'></i> Ver",
			functionClick:function(data){showDivStatusDocumentos(indexFunctionStatus);	}}]
	},
	{menu:"1",opcion: 
		[
		{id:"opcHide",nombre:"<i class='fa fa-eye-slash' aria-hidden='true'></i> Ocultar",
				functionClick:function(data){hideDivStatusDocumentos(indexFunctionStatus);}},
		{id:"opcTabla",nombre:"<i class='fa fa-table' aria-hidden='true'></i> Ver Tabla",
				functionClick:function(data){tablaResumenStatusDocumentos(indexFunctionStatus); }}
		]
	} 
];
function marcarSubOpcionStatus(pindex,pindex1)
{
	$("#opcioneshide .listaGestionGosst").hide();
	funcionalidadesStatus[pindex].opcion[pindex1].functionClick(); 
}
var menuOpcionStatus=[];
function ListarOpcionesStatus()
{ 
		funcionalidadesStatus.forEach(function(val1,index1)
		{
			var opciones=val1.opcion;
			menuOpcionStatus[index1]="<ul class='list-group listaGestionGosst' style='width: 110px;top: 25px; display:none;'>";
			opciones.forEach(function(val2,index2)
				{
				menuOpcionStatus[index1]+="<li class='list-group-item' onclick='marcarSubOpcionStatus("+index1+","+index2+")'>"+val2.nombre+"</li>"
			});
			menuOpcionStatus[index1]+="</ul>";
		});	
}

function cargarStatusDocumentosProyectoColaborador(indexProy){
	ListarOpcionesStatus();
	showDivStatusDocumentos();
	$("#proyectoTable"+proyectoObj.id).find(".detalleStatusDocColaborador")
	.html("<td colspan='3'></td>");
	callAjaxPost(URL + '/contratista/proyecto/actividades', 
			{id:proyectoObj.id}, function(data){
				listFullStatusDocumentos=data.list;
				var numAprob=0,numTotal=0;
				listFullStatusDocumentos.forEach(function(val,index){
					if(val.evaluacion==null){
						val.evaluacion={fechaTexto:"",nota:{id:3,nombre:"Sin evaluar",icono:""},observacion:"" };
					}else{
						if(val.evaluacion.nota==null){
							val.evaluacion.nota={id:3,icono:"",nombre:""};
						}
					}
				})
				listFullStatusDocumentos.sort(function(a,b){
					return -a.id+b.id;
				}).sort(function(a,b){
					return -a.evaluacion.nota.id+b.evaluacion.nota.id;
				});
				listFullStatusDocumentos.forEach(function(val,index){
					var claseNotaDocumento="gosst-neutral";
					
					if(val.evaluacion.nota.id==1){
						numAprob++;
						claseNotaDocumento="gosst-aprobado"
					}
					numTotal++;
					
					var selEstadoObsTotal=crearSelectOneMenuOblig("selEvalTotalAct","verIconoAcuerdoEvalColaborador(this)",
							listEvaluacionStatusTotal,val.evaluacion.nota.id,"id","nombre");
					var detalle="";
					if(val.tipo.id==5){
						val.categoria.nombre ="";
					}else{
						val.observacion=" / "+val.observacion;
						val.categoria.nombre ="<strong>"+val.categoria.nombre+"</strong>";
					}
					if(val.tipo.id==1){
						detalle=val.estado.nombre;
						if(val.estado.id!=2){
							detalle+=" ("+val.fechaPlanificadaTexto+")";
						}
					}
					if(val.tipo.id==2 || val.tipo.id==4){
						if(val.evidenciaNombre==""){
							detalle="Pendiente de registrar";
						}else{
							detalle="Registrado"
						}
					}
					val.detalle=detalle;
					var textoBotonDescarga="<a href='"+URL+"/contratista/proyecto/actividad/evidencia?id="+val.id+
					"' target='_blank'>Descargar</a>",
					textoBotonAgregar="<a onclick='evaluarStatusDocumentoColaborador("+index+")'>Evaluar</a>",
					textoBotonConfirm="";
					if(val.observacion==""){
						
					}else{
						val.observacion=" / "+val.observacion;
					}
					if(val.evidenciaNombre==""){
						textoBotonDescarga="<strong style='color:red'>No hay evidencia</strong>"
					}else{
						textoBotonDescarga="<a class='efectoLink' href='"+URL+"/contratista/proyecto/actividad/evidencia?id="+val.id+
						"' target='_blank'>"+val.evidenciaNombre+"</a>"
					}
					$("#proyectoTable"+proyectoObj.id)
					.find(".detalleStatusDocColaborador td") 
					.append("<div id='divMovilActProy"+val.id+"'>" +
							"<div class='detalleAccion "+claseNotaDocumento+"'> " +
							"<i class='fa fa-info'></i> "+val.tipo.nombre+val.observacion+"  <br>" +
							"<i class='fa fa-calendar'></i> Se cargó el documento:  "+val.evidenciaFechaTexto+"<br>" +
							"<i class='fa fa-download'></i> "+textoBotonDescarga  + "<br>"+
									"Resultado de Evaluación: <br>" +
							""+val.evaluacion.nota.icono+""+val.evaluacion.nota.nombre+"<br>" +
							"<i class='fa fa-eye'></i> "+val.evaluacion.fechaTexto+" / " +val.evaluacion.observacion+""+
							
							"</div>" +
							"<div class='opcionesAccion'>"+ 
							textoBotonAgregar+"</div>" +
								"<div class='subOpcionAccion'></div>" +
								"</div>")
					  
				});
				//IPERC
				var claseNotaDocumento="gosst-neutral";
				numTotal++;
				if(proyectoObj.evaluacionIperc==null){
					proyectoObj.evaluacionIperc={fechaTexto:"",nota:{icono:"",id:3,nombre:"Sin Evaluar"},observacion:"" };
				}else if(proyectoObj.evaluacionIperc.nota.id==1){
					numAprob++;claseNotaDocumento="gosst-aprobado";
				}
				var textoBotonDescarga="",
				textoBotonAgregar="<a onclick='evaluarStatusIpercColaborador()'>Evaluar</a>",
				textoBotonConfirm="";
				$("#proyectoTable"+proyectoObj.id)
				.find(".detalleStatusDocColaborador td")
				.prepend("<div id='divMovilIpercEvalProy'>" +
						"<div class='detalleAccion "+claseNotaDocumento+"'> " +
						"<i class='fa fa-info'></i> "+"Matriz de riesgos"+"  <br>" +
						"<i class='fa fa-info'></i>  "+"Evaluaciones no significativas"+"<br>" +
						"<i class='fa fa-info'></i>  "+proyectoObj.postulante.indicadorIperc+"<br>" +
								"Evaluación: <br>" +
						""+proyectoObj.evaluacionIperc.nota.icono+""+proyectoObj.evaluacionIperc.nota.nombre+"<br>" +
						"<i class='fa fa-eye'></i> "+proyectoObj.evaluacionIperc.fechaTexto+" / " +proyectoObj.evaluacionIperc.observacion+""+
						
						"</div>" +
						"<div class='opcionesAccion'>"+textoBotonDescarga
						+textoBotonAgregar+textoBotonConfirm+"</div>" +
							"<div class='subOpcionAccion'></div>" +
							"</div>")
				
				$("#trStatusColab"+proyectoObj.id).find(".celdaIndicadorProyecto").html(numAprob+" / "+numTotal);
				$("#trStatusColab"+proyectoObj.id).find(".barraIndicadorProyecto")
				.css({"width":pasarDecimalPorcentaje(numAprob/numTotal),
					"background-color":colorPuntajeGosst(numAprob/numTotal)});
				
				callAjaxPost(URL + '/contratista/examenes', 
						{
					id:(proyectoObj.postulante.contratista.id),
					examen:{tipo:{id:null}}
					}, function(data){
						listFullStatusExamenes=data.examenes;
						listFullStatusExamenes.forEach(function(val,index){
							if(val.evaluacion==null){
								val.evaluacion={fechaTexto:"",nota:{id:3,nombre:"Sin evaluar",icono:""},observacion:"" };
							}else{
								if(val.evaluacion.nota==null){
									val.evaluacion.nota={id:3,icono:"",nombre:""};
								}
							}
						})
						listFullStatusExamenes.sort(function(a,b){
							return -a.id+b.id;
						}).sort(function(a,b){
							return -a.evaluacion.nota.id+b.evaluacion.nota.id;
						});
						listFullStatusExamenes.forEach(agregarFilaExamenesColaborador);
						 
				})
				
				
		})
	
	
}
function agregarFilaExamenesColaborador(val,index){
	var textoBotonDescarga="<a href='"+URL+"/contratista/examen/evidencia?id="+val.id+
	"' target='_blank'>Descargar</a>",
	textoBotonAgregar="<a onclick='evaluarStatusExamenColaborador("+index+")'>Evaluar</a>",
	textoBotonConfirm="";
	var claseNotaDocumento="gosst-neutral"
	if(val.evaluacion.nota.id==1){
		claseNotaDocumento="gosst-aprobado"
	}
	$("#proyectoTable"+proyectoObj.id)
	.find(".detalleStatusDocColaborador td") 
	.append("<div id='divMovilExamProy"+val.id+"'>" +
			"<div class='detalleAccion "+claseNotaDocumento+"'> " +
			"<i class='fa fa-info'></i> "+val.tipo.nombre+"  <br>" +
			"<i class='fa fa-download'></i> "+val.evidenciaNombre+"  <br>" +
			"<i class='fa fa-calendar'></i>  "+val.fechaRealTexto+"<br>" +
					"Evaluación: <br>" +
			""+val.evaluacion.nota.icono+""+val.evaluacion.nota.nombre+"<br>" +
			"<i class='fa fa-eye'></i> "+val.evaluacion.fechaTexto+" / " +val.evaluacion.observacion+""+
			
			"</div>" +
			"<div class='opcionesAccion'>"+textoBotonDescarga+"<i class='fa fa-minus'></i>"
			+textoBotonAgregar+"</div>" +
				"<div class='subOpcionAccion'></div>" +
				"</div>")
}
function verIconoAcuerdoEvalColaborador(obj){
	var notaId=parseInt($(obj).val());
	listEvaluacionStatusTotal.every(function(val){
		if(val.id==notaId){
			$(obj).parent().find("label").html(val.icono)
			return false;
		}
		return true;
	});
}
function guardarEvalStatusDocumentoColaborador(indexDoc){
	var objDoc=listFullStatusDocumentos[indexDoc];
	var evalId=$("#selEvalTotalAct").val();
	var obs=$("#inputObsEvalTotal").val();
	var objAct={id:objDoc.id,evaluacion:{nota:{id:evalId},observacion:obs}};
	
	callAjaxPost(URL + '/contratista/proyecto/evaluacion_doc/unitaria/save', 
			objAct
			, function(data){
				if(data.CODE_RESPONSE=="05"){
					cargarStatusDocumentosProyectoColaborador();
				}
			})
}
function guardarEvalStatusExamenColaborador(indexDoc){
	var objDoc=listFullStatusExamenes[indexDoc];
	var evalId=$("#selEvalTotalAct").val();
	var obs=$("#inputObsEvalTotal").val();
	var objAct={id:objDoc.id,evaluacion:{nota:{id:evalId},observacion:obs}};
	
	callAjaxPost(URL + '/contratista/proyecto/evaluacion_exam/unitaria/save', 
			objAct
			, function(data){
				if(data.CODE_RESPONSE=="05"){
					cargarStatusDocumentosProyectoColaborador();
				}
			})
}
function guardarEvalStatusIpercColaborador(){ 
	var evalId=$("#selEvalTotalAct").val(); 
	var obs=$("#inputObsEvalTotal").val();
	var objAct={id:proyectoObj.id,
			evaluacionIperc:{nota:{id:evalId},fechaTexto:obtenerFechaActualNormal(),observacion:obs,proyecto:{id:proyectoObj.id}}};
	
	callAjaxPost(URL + '/contratista/proyecto/evaluacion_iperc/save', 
			objAct
			, function(data){
				if(data.CODE_RESPONSE=="05"){
					proyectoObj.evaluacionIperc=objAct.evaluacionIperc;
					listEvaluacionStatusTotal.forEach(function(val){
						if(val.id==proyectoObj.evaluacionIperc.nota.id){
							proyectoObj.evaluacionIperc.nota=val
						}
					})
					cargarStatusDocumentosProyectoColaborador();
				}
			})
}
function cancelarEvaluacionStatusDocumentacion(indexDoc){
	var objDoc=listFullStatusDocumentos[indexDoc];
	$("#proyectoTable"+proyectoObj.id)
	.find(".detalleStatusDocColaborador td")
	.find(".opcionesAccion").show();
	var claseNotaDocumento="gosst-neutral";
	if(objDoc.evaluacion.nota.id==1){
		claseNotaDocumento="gosst-aprobado"
	}
	var textoBotonDescarga="<a href='"+URL+"/contratista/proyecto/actividad/evidencia?id="+objDoc.id+
	"' target='_blank'>Descargar</a>",
	textoBotonAgregar="<a onclick='evaluarStatusDocumentoColaborador("+indexDoc+")'>Evaluar</a>",
	textoBotonConfirm="";
	$("#proyectoTable"+proyectoObj.id)
	.find(".detalleStatusDocColaborador td") 
	.find("#divMovilActProy"+objDoc.id).html( 
			"<div class='detalleAccion "+claseNotaDocumento+"'> " +
			"<i class='fa fa-file'></i> "+objDoc.evidenciaNombre+"  <br>" +
			"<i class='fa fa-info'></i> "+objDoc.categoria.nombre+" "+objDoc.descripcion +" "+objDoc.observacion+"<br>"	+
			"<i class='fa fa-calendar'></i>  "+objDoc.fechaRealTexto+"<br>" +
			"<i class='fa fa-info'></i>  "+objDoc.detalle+"<br>" +
					"Evaluación: <br>" +
			""+objDoc.evaluacion.nota.icono+""+objDoc.evaluacion.nota.nombre+"<br>" +
			"<i class='fa fa-eye'></i> "+objDoc.evaluacion.fechaTexto+" / " +objDoc.evaluacion.observacion+""+
			
			"</div>" +
			"<div class='opcionesAccion'>"+textoBotonDescarga+"<i class='fa fa-minus'></i>"
			+textoBotonAgregar+textoBotonConfirm+"</div>" +
				"<div class='subOpcionAccion'></div>"  )
	
}
function evaluarStatusExamenColaborador(indexExam){
	$("#proyectoTable"+proyectoObj.id)
	.find(".detalleStatusDocColaborador td")
	.find(".opcionesAccion").hide();
	var objDoc=listFullStatusExamenes[indexExam];
	var selEstadoObsTotal=crearSelectOneMenuOblig("selEvalTotalAct","verIconoAcuerdoEvalColaborador(this)",
			listEvaluacionStatusTotal,objDoc.evaluacion.nota.id,"id","nombre");
	var textoBotonVolver="<a onclick='cancelarEvaluacionStatusExamenColaborador("+indexExam+")'>Cancelar</a>";
	var textoBotonConfirm="";
	var claseNotaDocumento="gosst-neutral";
	if(objDoc.evaluacion.nota.id==1){
		claseNotaDocumento="gosst-aprobado"
	}
	var btnGuardar="<button class='btn btn-success' onclick='guardarEvalStatusExamenColaborador("+indexExam+")'>" +
			"<i class='fa fa-save'></i>Guardar</button>";
	$("#proyectoTable"+proyectoObj.id)
	.find(".detalleStatusDocColaborador td") 
	.find("#divMovilExamProy"+objDoc.id).html( 
			"<div class='detalleAccion "+claseNotaDocumento+"'> " +
			"<i class='fa fa-info'></i> "+objDoc.tipo.nombre+"  <br>" +
			"<i class='fa fa-download'></i> "+objDoc.evidenciaNombre+"  <br>" +
			"<i class='fa fa-calendar'></i>  "+objDoc.fechaRealTexto+"<br>" +
					"Evaluación: <br>" +
					selEstadoObsTotal +"<label>"+objDoc.evaluacion.nota.icono+"</label><br>"+
			"<input class='form-control' id='inputObsEvalTotal' placeholder='Observación'><br>" +
			""+btnGuardar+
			
			"</div>" +
			"<div class='opcionesAccion'>"
			+textoBotonVolver+textoBotonConfirm+"</div>" +
				"<div class='subOpcionAccion'></div>"  );
	 $("#divMovilActProy"+objDoc.id).find("#inputObsEvalTotal").val(objDoc.evaluacion.observacion)
	
}
function cancelarEvaluacionStatusExamenColaborador(indexExam){
	var objDoc=listFullStatusDocumentos[indexExam];
	$("#proyectoTable"+proyectoObj.id)
	.find(".detalleStatusDocColaborador td")
	.find(".opcionesAccion").show();
	var claseNotaDocumento="gosst-neutral";
	if(objDoc.evaluacion.nota.id==1){
		claseNotaDocumento="gosst-aprobado"
	}
	var textoBotonDescarga="<a href='"+URL+"/contratista/examen/evidencia?id="+objDoc.id+
	"' target='_blank'>Descargar</a>",
	textoBotonAgregar="<a onclick='evaluarStatusDocumentoColaborador("+indexExam+")'>Evaluar</a>",
	textoBotonConfirm="";
	$("#proyectoTable"+proyectoObj.id)
	.find(".detalleStatusDocColaborador td") 
	.find("#divMovilExamProy"+objDoc.id).html(  
			"<div class='detalleAccion "+claseNotaDocumento+"'> " +
			"<i class='fa fa-info'></i> "+objDoc.tipo.nombre+"  <br>" +
			"<i class='fa fa-download'></i> "+objDoc.evidenciaNombre+"  <br>" +
			"<i class='fa fa-calendar'></i>  "+objDoc.fechaRealTexto+"<br>" +
					"Evaluación: <br>" +
			""+objDoc.evaluacion.nota.icono+""+objDoc.evaluacion.nota.nombre+"<br>" +
			"<i class='fa fa-eye'></i> "+objDoc.evaluacion.fechaTexto+" / " +objDoc.evaluacion.observacion+""+
			
			"</div>" +
			"<div class='opcionesAccion'>"+textoBotonDescarga+"<i class='fa fa-minus'></i>"
			+textoBotonAgregar+"</div>" +
				"<div class='subOpcionAccion'></div>"  )
}
function evaluarStatusDocumentoColaborador(indexDoc){
	$("#proyectoTable"+proyectoObj.id)
	.find(".detalleStatusDocColaborador td")
	.find(".opcionesAccion").hide();
	var objDoc=listFullStatusDocumentos[indexDoc];
	var selEstadoObsTotal=crearSelectOneMenuOblig("selEvalTotalAct","verIconoAcuerdoEvalColaborador(this)",
			listEvaluacionStatusTotal,objDoc.evaluacion.nota.id,"id","nombre");
	var textoBotonVolver="<a onclick='cancelarEvaluacionStatusDocumentacion("+indexDoc+")'>Cancelar</a>";
	var textoBotonConfirm="";
	var claseNotaDocumento="gosst-neutral";
	if(objDoc.evaluacion.nota.id==1){
		claseNotaDocumento="gosst-aprobado"
	}
	var detalle=objDoc.detalle;
	var btnGuardar="<button class='btn btn-success' onclick='guardarEvalStatusDocumentoColaborador("+indexDoc+")'>" +
			"<i class='fa fa-save'></i>Guardar</button>";
	$("#proyectoTable"+proyectoObj.id)
	.find(".detalleStatusDocColaborador td") 
	.find("#divMovilActProy"+objDoc.id).html( 
			"<div class='detalleAccion "+claseNotaDocumento+"'> " +
			"<i class='fa fa-info'></i> "+objDoc.tipo.nombre+"  <br>" +
			"<i class='fa fa-file'></i> "+objDoc.evidenciaNombre+"  <br>" +
			"<i class='fa fa-info'></i> "+objDoc.categoria.nombre+" "+objDoc.descripcion +" "+objDoc.observacion+"<br>"	+
			"<i class='fa fa-calendar'></i>  "+objDoc.fechaRealTexto+"<br>" +
			"<i class='fa fa-info'></i>  "+detalle+"<br>" +
					"Evaluación: <br>" +
					selEstadoObsTotal +"<label>"+objDoc.evaluacion.nota.icono+"</label><br>"+
			"<input class='form-control' id='inputObsEvalTotal' placeholder='Observación'><br>" +
			""+btnGuardar+
			
			"</div>" +
			"<div class='opcionesAccion'>"
			+textoBotonVolver+textoBotonConfirm+"</div>" +
				"<div class='subOpcionAccion'></div>"  );
	 $("#divMovilActProy"+objDoc.id).find("#inputObsEvalTotal").val(objDoc.evaluacion.observacion)
	
	
}

function evaluarStatusIpercColaborador(){
	$("#proyectoTable"+proyectoObj.id)
	.find(".detalleStatusDocColaborador td")
	.find(".opcionesAccion").hide();
	var objDoc=proyectoObj;
	var selEstadoObsTotal=crearSelectOneMenuOblig("selEvalTotalAct","verIconoAcuerdoEvalColaborador(this)",
			listEvaluacionStatusTotal,objDoc.evaluacionIperc.nota.id,"id","nombre");
	var textoBotonVolver="<a onclick='cancelarEvaluacionStatusDocumentacion()'>Cancelar</a>";
	var textoBotonConfirm="";
	var claseNotaDocumento="gosst-neutral";
	if(objDoc.evaluacionIperc.nota.id==1){
		claseNotaDocumento="gosst-aprobado"
	} 
	var btnGuardar="<button class='btn btn-success' onclick='guardarEvalStatusIpercColaborador()'>" +
			"<i class='fa fa-save'></i>Guardar</button>";
	$("#proyectoTable"+proyectoObj.id)
	.find(".detalleStatusDocColaborador td") 
	.find("#divMovilIpercEvalProy").html( 
			"<div class='detalleAccion "+claseNotaDocumento+"'> " +
			"<i class='fa fa-info'></i> "+"Matriz de riesgos"+"  <br>" +
			"<i class='fa fa-info'></i>  "+"Evaluaciones no significativas"+"<br>" +
			"<i class='fa fa-info'></i>  "+proyectoObj.postulante.indicadorIperc+"<br>" +
					"Evaluación: <br>" +
					selEstadoObsTotal +"<label>"+objDoc.evaluacionIperc.nota.icono+"</label><br>"+
			"<input class='form-control' id='inputObsEvalTotal' placeholder='Observación'><br>" +
			""+btnGuardar+
			
			"</div>" +
			"<div class='opcionesAccion'>"
			+textoBotonVolver+textoBotonConfirm+"</div>" +
				"<div class='subOpcionAccion'></div>"  );
	 $("#divMovilIpercEvalProy").find("#inputObsEvalTotal").val(objDoc.evaluacionIperc.observacion)
	
	
}

function showDivStatusDocumentos(){
	$("#proyectoTable"+proyectoObj.id).find(".detalleStatusDocColaborador")
	.show();
	$("#trStatusColab"+proyectoObj.id).find(".celdaBotonAccion")
	.html(
			/*"<button class='btn btn-success' onclick='hideDivStatusDocumentos("+proyectoObj.id+")'>" +
			"<i class='fa fa-eye'></i>Ocultar</button>" +
			"<br><br>" +
			"<button class='btn btn-success' onclick='tablaResumenStatusDocumentos("+proyectoObj.id+")'>" +
			"<i class='fa fa-table'></i>Ver Tabla</button>" + 
			*/
			"<div classs='row'>" +
				"<section   id='opcioneshide' class='col col-xs-12'>" +
					"<a class='efectoLink btn-gestion' onclick='toggleMenuOpcionStatus(this,"+proyectoObj.id+",\"hide\")'>" +
						"Ver Opciones<i class='fa fa-angle-double-down' aria-hidden='true'></i>" +
					"</a>"+menuOpcionStatus[1]+
				"</section>"+
			"</div>"
			);
}

function hideDivStatusDocumentos(){
	$("#proyectoTable"+proyectoObj.id).find(".detalleStatusDocColaborador")
	.hide();
	$("#trStatusColab"+proyectoObj.id).find(".celdaBotonAccion")
	.html(/*"<button class='btn btn-success' onclick='showDivStatusDocumentos("+proyectoObj.id+")'>" +
			"<i class='fa fa-eye'></i>Ver </button>"+
			*/
			"<div classs='row'>" +
				"<section   id='opcioneshide' class='col col-xs-12'>" +
					"<a class='efectoLink btn-gestion' onclick='toggleMenuOpcionStatus(this,"+proyectoObj.id+",\"hide\")'>" +
						"Ver Opciones<i class='fa fa-angle-double-down' aria-hidden='true'></i>" +
					"</a>"+menuOpcionStatus[0]+
				"</section>"+
		"</div>" 
		);
}

function tablaResumenStatusDocumentos(){
	$(".divContainerGeneral").hide();
	var buttonClip="<button type='button' id='btnClipboardTrabProy'   style='margin-right:10px'" +
			" class='btn btn-success clipGosst' onclick='obtenerTablaTrabajadoresProyecto()'>" +
					"<i class='fa fa-clipboard'></i>Clipboard</button>"
	var buttonVolver="<button type='button' class='btn btn-success ' style='margin-right:10px'" +
			" onclick='volverVistaMovil()'>" +
						"<i class='fa fa-sign-out fa-rotate-180'></i>Volver</button>";
	var listAct=[
	             {id:1,nombre:"Programa SASS",numAplica:0,numConEvidencia:0,numSinEvidencia:0,
	            	 numEvaluarAprobado:0,numEvaluarDesaprobado:0,numSinEvaluar:0},
	             {id:2,nombre:"Plan de Emergencia",numAplica:0,numConEvidencia:0,numSinEvidencia:0,
		            	 numEvaluarAprobado:0,numEvaluarDesaprobado:0,numSinEvaluar:0},
	             {id:4,nombre:"Plan de Manejo de Residuos",numAplica:0,numConEvidencia:0,numSinEvidencia:0,
			            	 numEvaluarAprobado:0,numEvaluarDesaprobado:0,numSinEvaluar:0},
	             {id:5,nombre:"Documentos Asociados",numAplica:0,numConEvidencia:0,numSinEvidencia:0,
				            	 numEvaluarAprobado:0,numEvaluarDesaprobado:0,numSinEvaluar:0}
	];
	buttonClip="";
	$("body")
	.append("<div id='divStatDocTabla' style='padding-left: 40px;'>" +
		//	"<form class='form-inline'>"+
			buttonVolver+buttonClip+
			//"</form>"+
			 
			"<div class='wrapper' id='wrapperTrabProy'>" +
			"<table  style=' ' " +
			"id='tblStatusDocs' class='table table-striped table-bordered table-hover  '>" +
			"<thead>" +
				"<tr>" + 
				"<td class='tb-acc'>Estado Documento / Tipo</td>" +
				"</tr>" +
			"</thead>" +
			"<tbody>" +
			"<tr id='trApl'>" +
			"<td>Aplicables</td>" +
			"</tr>" +
			"<tr id='trevic'>" +
			"<td>Evidenciados por el contratista</td>" +
			"</tr>" +
			"<tr id='trnoevi'>" +
			"<td>No evidenciados</td>" +
			"</tr>" +
			
			"<tr id='traprob'>" +
			"<td>Validados  </td>" +
			"</tr>" +
			"<tr id='trdesa'>" +
			"<td>Observados  </td>" +
			"</tr>" +
			"<tr id='trsineva'>" +
			"<td>Sin evaluar  </td>" +
			"</tr>" +
			"<tr id='trindica'>" +
			"<td>Indicador (Validados / Aplicables)  </td>" +
			"</tr>" +
			
			"</tbody>" +
			"</table>" +
			"</div>" +
			" </div>");
	
	listAct.forEach(function(val,index){
		$("#tblStatusDocs thead tr")
		.append("<td class='tb-acc' style='width:190px'>"+val.nombre+"</td>" );
		listFullStatusDocumentos.forEach(function(val1){
			if(val1.tipo.id==val.id){
				val.numAplica++;
				if(val1.evidenciaNombre==null || val1.evidenciaNombre==""){
					val.numSinEvidencia++;
				}else{
					val.numConEvidencia++;
				}
				if(val1.evaluacion.nota.id==0 || val1.evaluacion.nota.id==3){
					val.numSinEvaluar++;
				}
				if(val1.evaluacion.nota.id==1){
					val.numEvaluarAprobado++;
				}
				if(val1.evaluacion.nota.id==2){
					val.numEvaluarDesaprobado++;
				}
			}
		});
		$("#tblStatusDocs tbody #trApl").append("<td style='border-bottom:2px solid #abadaf'>"+
				val.numAplica+"</td>")
		$("#tblStatusDocs tbody #trevic").append("<td>"+val.numConEvidencia+"</td>")
		
		$("#tblStatusDocs tbody #trnoevi").append("<td  style='border-bottom:2px solid #abadaf'>"+
				val.numSinEvidencia+"</td>")
		$("#tblStatusDocs tbody #traprob").append("<td>"+val.numEvaluarAprobado+"</td>")
		$("#tblStatusDocs tbody #trdesa").append("<td>"+val.numEvaluarDesaprobado+"</td>")
		$("#tblStatusDocs tbody #trsineva").append("<td   style='border-bottom:2px solid #abadaf'>"
				+val.numSinEvaluar+"</td>")
		$("#tblStatusDocs tbody #trindica").append("<td   >"
				+pasarDecimalPorcentaje(val.numEvaluarAprobado/val.numAplica,2)+"</td>")
				
				
				
	});
	var evalIperc={nombre:"Matriz de riesgos",numAplica:1,numConEvidencia:0,numSinEvidencia:0,
			numEvaluarAprobado:0,numEvaluarDesaprobado:0,numSinEvaluar:0};
	if(parseInt(proyectoObj.indicadorIperc)>0){
		evalIperc.numConEvidencia=1;
	}else{
		evalIperc.numSinEvidencia=1;
	}
	if(proyectoObj.evaluacionIperc==null){
		evalIperc.numSinEvaluar=1;
	}else if(proyectoObj.evaluacionIperc.nota.id==1){
		evalIperc.numEvaluarAprobado=1;
	}else if(proyectoObj.evaluacionIperc.nota.id==2){
		evalIperc.numEvaluarDesaprobado=1;
	}else if(proyectoObj.evaluacionIperc.nota.id==3){
		evalIperc.numSinEvaluar=1;
	}
	
	
	$("#tblStatusDocs thead tr")
	.append("<td class='tb-acc' style='width:190px'>"+evalIperc.nombre+"</td>" );
	
	$("#tblStatusDocs tbody #trApl").append("<td style='border-bottom:2px solid #abadaf'>"+
			evalIperc.numAplica+"</td>")
	$("#tblStatusDocs tbody #trevic").append("<td>"+evalIperc.numConEvidencia+"</td>")
	$("#tblStatusDocs tbody #trnoevi").append("<td  style='border-bottom:2px solid #abadaf'>"+
			evalIperc.numSinEvidencia+"</td>");
	
	$("#tblStatusDocs tbody #traprob").append("<td>"+evalIperc.numEvaluarAprobado+"</td>")
	$("#tblStatusDocs tbody #trdesa").append("<td>"+evalIperc.numEvaluarDesaprobado+"</td>")
	$("#tblStatusDocs tbody #trsineva").append("<td   style='border-bottom:2px solid #abadaf'>"
			+evalIperc.numSinEvaluar+"</td>")
	$("#tblStatusDocs tbody #trindica").append("<td   >"
			+pasarDecimalPorcentaje(evalIperc.numEvaluarAprobado/evalIperc.numAplica,2)+"</td>")
	
	
	
	
}














