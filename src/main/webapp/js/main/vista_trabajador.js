/**
 * 
 */
var contadorRetrasadoAlertEventos=0;
var contadorRetrasadoAlertActividades=0;
var listFullTrabajadores;
var listFullEventos;
var textoBotonToggleContenidoColaborador="<button class='btn btn-success'  onclick='cambiarDivSubContenidoColaborador(this)'>" +
	"<i class='fa fa-caret-down fa-2x' aria-hidden='true'></i></button>";
var textoBotonTogglePregunta="<button class='btn btn-success'  onclick='cambiarDivSubPregunta(this)'>" +
"<i class='fa fa-caret-down fa-2x' aria-hidden='true'></i></button>";
var textoBotonVolverContenidoColaborador="<button class='btn btn-success' style='float:left'  onclick='volverDivSubContenidoColaborador(this)'>" +
"<i class='fa fa-arrow-left fa-2x' aria-hidden='true'></i></button>";
function volverDivSubContenidoColaborador(obj){
	$(".divListPrincipal > div").show();
	$("#subListPsicologiaExamen").hide();
	$(".divCursoExamenMovil").hide();
	$(".divSubEvento").hide();
	if(lastHeight>0){
		$(window).scrollTop(lastHeight-$("#barraMenu").height()-60);
		lastHeight=0;
	}
}
function cambiarDivSubPregunta(obj){
	$(obj).parent(".divNombrePregunta").siblings( ".divOpcionesGosst" ).toggle();
}
function cambiarDivSubContenidoColaborador(obj,proyectoId){
	if(proyectoId!=null){
		proyectoId=parseInt(proyectoId);
		listFullProyectoSeguridad.forEach(function(val,index){
			if(val.id==proyectoId){
				proyectoObj=val;
			}
		});
	}
	$(obj).parent(".tituloSubList").siblings( ".contenidoSubList" ).toggle();
}
function verDetalleAccionActo(obj){
	$(obj).next(".divAccionesSubMovil").toggle();
}
var menuDerechaVistaTrabajador=[
                 {id:1,icono:"fa-calendar",ident:"icoPendientes",onclick:"marcarIconoModuloSeleccionado(2)",titulo:"Actividades",adicional:""},
                 {id:1,icono:" fa-user-circle",ident:"icoPerfil",onclick:"marcarIconoModuloSeleccionado(1)",titulo:"Perfil",adicional:getSession("gestopusername").substr(0,12)+"."},
                 {id:1,icono:" fa-question-circle ",ident:"icoPreguntas",onclick:"marcarIconoModuloSeleccionado(1)",titulo:"Tutorial",adicional:""}];
var menuFuncionesIzquierdaTrabajador=[];
function verPerfilMenu(){
	
}
function verSeccionRestringida(){
	$(".divListPrincipal")
	.append(
			"<div id='subListCapacitacion'  >" +
			"<div class='tituloSubList' style='background:#e8e6e6'>Sin acceso a esta funcionalidad del módulo.<br> Más información consulte al administrador del sistema</div>" +
			"<div class='contenidoSubList'></div>" +
		"</div>" );
};
function marcarIconoModuloSeleccionado(menuId,proyectoId,tipoId,idAux){
 $("#menuExplicativo").hide();
 $(".divContainerGeneral").show();
	$(".divListModulos").find("li").removeClass("activeMenu");
	$(".divListPrincipal").html("");
	$(".gosst-aviso").remove();
	var menuObject=[];console.log(menuFuncionesIzquierdaTrabajador);
	menuFuncionesIzquierdaTrabajador.forEach(function(val){
		if(val.id==parseInt(menuId)){
			menuObject=val;
		}
	});
	$(".divListModulos #liMenuTrabajador"+menuObject.id).addClass("activeMenu");
	var isPermitido=false;
	listaModulosPermitidos.forEach(function(val,index){
		var moduloPermitido=parseInt(val.isPermitido);
		if(val.menuOpcionId==menuObject.moduloId && moduloPermitido==1){
			isPermitido=true;
		}
		var numerSubModulos=val.subMenus;
		numerSubModulos.forEach(function(val1,index1){
			var subModuloPermitido=val1.isPermitido;
			
			if(val1.menuOpcionId==menuObject.moduloId && subModuloPermitido==1){
				isPermitido=true;
			}
		});
	});
	if(isPermitido){
		switch(menuObject.id){
		case 1:
			habilitarPerfilUsuario();
			break;
		case 2:
			traerEventosTrabajador();
			break;
		case 3:
			habilitarEventosTrabajador();
			break;
		case 4:
			habilitarIpercUsuario();
			break;
		case 5:
			habilitarObjetivosUsuario();
			break;
		case 6:
			habilitarHistorialUsuario();
			break;
		case 7:
			habilitarDocumentosUsuario();
			break;
		case 8:
			habilitarCapacitacionOnlineUsuario();
			break;
		case 9:
			habilitarEvaluacionSicologicaUsuario();
			break;
		case 10:
			habilitarReporteEventoUsuario();
			break;
		case 11:
			habilitarProyectosContratistaUsuario(proyectoId,tipoId,idAux) ;
			break;
		case 12:
			habilitarComunicadoTrabajador();
			break;
		case 13:
			habilitarAccidentesPorEvaluar();
			break;
		case 14:
			habilitarVigilnaciaTrabajador();
			break;
		case 15:
			habilitarObservacionesColaborador();
			break;
		}
		completarBarraCarga();
	}else{
		verSeccionRestringida();completarBarraCarga();
		console.log(menuId);console.log(listaModulosPermitidos);
	}
	
	$(".divTituloFull").find("h4").html(menuObject.titulo)
	
}
var puestoTrabajoNombre="";
var areaNombreAux="";
var numComiteCsst = 0;
var numAreasResponsable = 0;
var numAreasSupervisor = 0;
var numAreasJefe = 0;
var numAreasGerente = 0;
function verLateralesFijos(){
	$("#menuPrincipal").html("" +
			"<input placeholder='Buscar ' type='text' class='buscadorGosst' style='width:280px'>" +
			"Tu Gestor Online de Seguridad y Salud en el Trabajo te da la Bienvenida")
	.css({color:"white"});
	$(".navbar").css({"min-height": "48px","margin-bottom": "0px","top":"0px"});
	var logo=$("#logoMenu");
	logo.css({"background-color":"#2e9e8f","margin-top":"-18px"})
	.attr("onclick","volverMenuInicio()");
	$("#barraMenu").css({"position": "fixed",
    "width": "100%"});
	$("#barraMenu").find(".navbar-right").html("<ul></ul> ");
	menuDerechaVistaTrabajador.forEach(function(val,index){
		var claseSub="class='iconoLink dropdown-toggle' data-toggle='dropdown'";
		if(val.ident!="icoPerfil"){
			claseSub="class='iconoLink'";
			
		}else{
			val.onclick="";
		}
		$("#barraMenu").find(".navbar-right").append("" +
				"<li id='"+val.ident+"' title='"+val.titulo+"'><a onclick='"+val.onclick+"'"+ 
				claseSub+"	>"+
				"<i class='fa "+val.icono+" fa-2x' aria-hidden='true'>"+
				"</i> "+val.adicional+"</a></li>"+
				"")
	});
	$("#icoPerfil").append(
			"<ul class='dropdown-menu' style='color :white'>" +
			"<li onclick='marcarIconoModuloSeleccionado(1)'> <a><i class='fa fa-user-o ' aria-hidden='true'></i>Perfil</a></li>" +
			"<li onclick='marcarIconoModuloSeleccionado(1)'> <a> <i class='fa fa-exchange ' aria-hidden='true'></i>Cambiar Contraseña</a></li>" +
			"<li class='dropdown-divider' role='presentation'></li>" +
			"<li onclick='cerrarSesion()'> <a> <i class='fa fa-power-off ' aria-hidden='true'></i>Salir</a></li>" +
			"" +
			"</ul>" +
			"" +
			"" +
			"</div>");
	$(".buscadorGosst").css({"margin-top": "8px",
    "border-radius": "21px","color":"black",
    "background-color": "#ffffff"});
	$("body").find("table").remove();
	$("body").find("#divCompletarActividad").remove();
	$("body").find("h4").remove();
	$("body").find(".divTituloFull").remove();
	$("body").find(".divContainerGeneral").remove();
	$("body").append("" +
			"<div class='divTituloFull'><div class='divTituloGeneral'><h4>Mis Actividades SST</h4></div></div>" +
			"<div class='divContainerGeneral'>" +
			"" +
			"<div class='divListModulos'><ul class='list-group'></ul></div>" +
			"<div class='divListPrincipal'></div>" +
			"<div class='divListSecundaria'>" +
				
				"<div id='divObjetivos' style='height:120px'>" +
				"<div class='subDivTitulo'>Objetivos </div><div class='subDivContenido'></div>" +
				"</div>" +
				"<div id='divNoticiaDiaria' style='height:250px'>" +
				"<div class='subDivTitulo'>Noticia del día </div><div class='subDivContenido'></div>" +
				"</div>" +
				"<div id='divPiramide' >" +
				"<div class='subDivTitulo'>Accidentabilidad </div><div class='subDivContenido'></div>" +
				"</div>" +
			"" +
			"</div>"+
			"</div>"  );
	//Noticia
	var noticia="<div class='cropGosst'><img src='../imagenes/logo-tecsup.png'></div>"
	var encabezado="Tecsup mejora la cultura de seguridad usando GOSST"
		var linkVerNoticia="<a> Ver Más</a>"
		$("#divNoticiaDiaria").find(".subDivContenido").html(noticia+encabezado+linkVerNoticia);
	$("#divNoticiaDiaria").hide();

	$("#divObjetivos").hide();
	$("#divPiramide").hide();
	//
	
	var dataParamAux = {
			mdfId:getSession("unidadGosstId"),
			empresaId:getSession("gestopcompanyid"),
			trabajadorId:parseInt(getSession("trabajadorGosstId"))
		};
	callAjaxPost(URL + '/trabajador', dataParamAux,function(data){
		var wantsObjetivos = data.list[0].wantsObjetivos;
		var wantsAccidentabilidad = data.list[0].wantsAccidentabilidad;
		numComiteCsst = data.list[0].numComiteCsst;
		puestoTrabajoNombre=data.list[0].puesto.positionName
		areaNombreAux=data.list[0].puesto.areaName;
		numAreasResponsable=data.list[0].numAreasResponsable;
		numAreasSupervisor=data.list[0].numAreasSupervisor;
		numAreasJefe=data.list[0].numAreasJefe;
		numAreasGerente=data.list[0].numAreasGerente;
		menuFuncionesIzquierdaTrabajador=data.secciones;
		$("#menuFigureti").html("<ul class='list-group'></ul>")
		menuFuncionesIzquierdaTrabajador.forEach(function(val){
				$(".divListModulos").find("ul").append("" +
						"<li class='list-group-item' id='liMenuTrabajador"+val.id+"' onclick='marcarIconoModuloSeleccionado("+val.id+")'>" +
						"<i class='fa "+val.icono+" ' aria-hidden='true'>"+
						"<span  ></span></i> "+val.nombre+
						(val.numAlerta >  0?"<div class='alertaMenu'>"+val.numAlerta+"</div>":"")+
						"</li>");
				$("#menuFigureti").find("ul").append("" +
						"<li class='list-group-item' style='color: #2e9e8f'  id='liMenuTrabajador"+val.id+"' onclick='marcarIconoModuloSeleccionado("+val.id+")'>" +
						"<i class='fa "+val.icono+" ' style='width:16px;' aria-hidden='true'>"+
						"<span  ></span></i> "+val.nombre+
						(val.numAlerta >  0?"<div class='alertaMenu'>"+val.numAlerta+"</div>":"")+
						"</li>");
			}); 
			$("#menuMovilGosst").on("click",function(){
				$(".divContainerGeneral").toggle();
			});
			marcarIconoModuloSeleccionado(data.list[0].seccionTrabajadorDefaultId)
		var trabajadorObj=data.list[0];
		if(trabajadorObj.imagen==null){
			
		}else{
		$("#icoPerfil > a").html(
			insertarImagenParaTablaMovil(trabajadorObj.imagen,"32px","32px")+
			getSession("gestopusername").substr(0,12)+".");
		}
		if(wantsAccidentabilidad == 1){
$("#divPiramide").show();
			var dataParam = {
					empresaId : sessionStorage.getItem("gestopcompanyid"),
					grupoId:1
				}; 
			
			callAjaxPost(URL + '/empresa/indicadores/basicos', dataParam, function(data){
				var horasHombre=data.indicadoresBasico.numTrabajadoresActivos*7.5*22*12;
				var indiceSeveridad=data.indicadoresBasico.numDiasDescanso*200000/horasHombre;
				var indicadorFrecuenciaTotal=(data.indicadoresBasico.numAccidentes*200000/horasHombre);
				var indiceAccidentabilidad=indiceSeveridad*indicadorFrecuenciaTotal/200;
				eventosInseguros=data.eventosInseguros[0].numCond+data.eventosInseguros[0].numActos;
				
				$("#divPiramide .subDivContenido").html(
			"# de Accidentes fatales :	<label>"+data.indicadoresBasico.numAccidentesFatales+"</label><br>"
						+"# de Accidentes incapacitantes :	<label>"+data.indicadoresBasico.numAccidentesInc+"</label><br>"
		+"#  enfermedades ocupacionales :	<label>"+data.indicadoresBasico.numEnfermedades2+"</label><br>"
		+"# incidentes :	<label>"+data.indicadoresBasico.numIncidentes+"</label><br>"
		+"#  días de descanso médico  :	<label>"+data.indicadoresBasico.numDiasDescanso+"</label><br>"
		+"#  A/C subestándar:	<label>"+eventosInseguros+"</label><br>"
		+"#  días sin accidentes :	<label>"+data.indicadoresBasico.numDiasSinAccidente+"</label><br>"
		+"Indice de Accidentabilidad :	<label>"+indiceAccidentabilidad.toFixed(2)+"</label><br>");
			});
		};
		
		if(wantsObjetivos == 1){
			$("#divObjetivos").show();
			var dataParam2 = {
					trabajadorId :getSession("trabajadorGosstId")
				};
			callAjaxPost(URL + '/trabajador/puntajes', dataParam2,
					function(data){
				var indicadoresSeguridad = data.indicadoresSeguridad;
				var indEvento = data.indEvento;
				var indAcc = data.indAcc;
				var indEpp = data.indEpp;
				var indExam = data.indExam;
				var indFormacion = data.indFormacion;
				
				var indicadorForTrab=0;
				var indicadorEntrega=0;
				var indicadorAcc=1;
				var sumaDiasDescanso=0;
				var diasTrabajados=0;
				var indicadorPart=0;
				var indicadorExamTrab=0;
				
				indEvento.forEach(function(val,index){ 
						indicadorPart = val.numEventosReportados;
				});
				
				indFormacion.forEach(function(val,index){
						var primerDia = new Date();
						primerDia=convertirFechaNormal2(primerDia);
						var capacitacionesTrab=val.caps;
						var indicadorRangoActual=1;
						for(var index22=0;index22<capacitacionesTrab.length;index22++){
							if(index22==0){
								indicadorRangoActual=0
							}
							var programs=capacitacionesTrab[index22].programsCap;
							
							for(var index31=0;index31<programs.length;index31++){
								if(programs[index31].fechaPlaTexto==null){
									programs[index31].fechaPlaTexto=primerDia;
								}
								if(programs[index31].fechaRealTexto==null){
									programs[index31].fechaRealTexto=programs[index31].fechaPlaTexto;
								}
								var enRangoInicial=fechaEnRango(primerDia,programs[index31].fechaPlaTexto,programs[index31].fechaRealTexto)
								var  boolEntreFechas=false;
								if(enRangoInicial ){
									 boolEntreFechas=true;
								}
								if(programs[index31].tipoNotaTrabId==1 &&boolEntreFechas ){
									indicadorRangoActual=
										indicadorRangoActual+(1/capacitacionesTrab.length);
									index31=programs.length;
								}
							}
						}
						indicadorForTrab=indicadorRangoActual;
				});
				indExam.forEach(function(val,index){ 
					var primerDia = new Date();
					
					primerDia=convertirFechaNormal2(primerDia);
					var examsTrab=val.exams;
					var indicadorRangoActual=0;
					for(var index22=0;index22<examsTrab.length;index22++){
						
						var programs=examsTrab[index22].programsExams;
						
						for(var index33=0;index33<programs.length;index33++){
							if(programs[index33].fechaInicialTexto==null){
								programs[index33].fechaInicialTexto=primerDia;
							}
							if(programs[index33].fechaFinTexto==null){
								programs[index33].fechaFinTexto=programs[index33].fechaInicialTexto;
							}
							var enRangoInicial=fechaEnRango(primerDia,programs[index33].fechaInicialTexto,programs[index33].fechaFinTexto)
							var  boolEntreFechas=false;
							if(enRangoInicial ){
								 boolEntreFechas=true;
							}
							if(programs[index33].tipoNotaTrabId==1 &&boolEntreFechas ){
							
								indicadorRangoActual=1
								index33=programs.length;

								index22=examsTrab.length;
							}
						}
					
						
					}
					
					indicadorExamTrab=indicadorRangoActual;
				});
				indEpp.forEach(function(val,index){ 
					var primerDia = new Date();
					
					primerDia=convertirFechaNormal2(primerDia);
					var eppsTrabajador=val.epps;
					var indicadorRangoActual=1;
					for(var index22=0;index22<eppsTrabajador.length;index22++){
						if(index22==0){
							indicadorRangoActual=0
						}
						var programs=eppsTrabajador[index22].programsEpp;
						
						for(var index31=0;index31<programs.length;index31++){
							if(programs[index31].fechaPlanificadaTexto==null){
								programs[index31].fechaPlanificadaTexto=primerDia;
							}
							if(programs[index31].fechaEntregaTexto==null){
								programs[index31].fechaEntregaTexto=programs[index31].fechaPlanificadaTexto;
							}
							var enRangoInicial=fechaEnRango(primerDia,programs[index31].fechaPlanificadaTexto,programs[index31].fechaEntregaTexto)
							var  boolEntreFechas=false;
							if(enRangoInicial ){
								 boolEntreFechas=true;
							}
							if(programs[index31].tipoNotaTrabId!=null &&boolEntreFechas ){
								indicadorRangoActual=
									indicadorRangoActual+(1/eppsTrabajador.length);
								index31=programs.length;
							}
						}
					
						
					}
					indicadorEntrega=indicadorRangoActual;
				});
				indAcc.forEach(function(val,index){
					indicadorAcc = val.indicadorDiasDescanso;
					sumaDiasDescanso=val.sumaDiasDescanso;
					diasTrabajados=  val.diasTrabajados
				});
				var promedio = (indicadorForTrab + indicadorExamTrab
						+ indicadorEntrega + indicadorAcc + 
						((indicadorPart / indicadoresSeguridad.indicadorMinimoParticipacion)>1?1:(indicadorPart / indicadoresSeguridad.indicadorMinimoParticipacion))

				) / 5;
				//Objetivos 
				var porcentajeCumplimiento=pasarDecimalPorcentaje(promedio); 
				var iconoEstrella="<span>★</span>";
				var iconoEstrellaVacia="<span>★</span>";
				var linkVerObjetivo="<a class='efectoLink' onclick='marcarIconoModuloSeleccionado(5)'> Ver Más</a>"
				var estrellas=	"<div class='star-ratings-css'>"
				  +"<div class='star-ratings-css-top' style='width: "+porcentajeCumplimiento+";color: " +
				  		(promedio<=0.25?"#FF3D3D;":
				  			(promedio>0.25&&promedio<0.8?"#FFCD00;":
				  				(promedio>=0.8?"#029F4C;":""))	
				  		
				  		)+"'>" +
				  iconoEstrella+iconoEstrella+iconoEstrella+iconoEstrella+iconoEstrella   +
				  		"</div>"
				 +" <div class='star-ratings-css-bottom'>" +
				 iconoEstrellaVacia+iconoEstrellaVacia+iconoEstrellaVacia+iconoEstrellaVacia+iconoEstrellaVacia+
				 		"</div>"
				+"</div>"
				$("#divObjetivos").find(".subDivContenido").html(estrellas+linkVerObjetivo);
				//
				
				
				 
				
			});
		}
		
	})
	
	
}
function habilitarNoticiasUsuario(){

	habilitarVistaUsuario();
}
var listFullPsicologia=[],cursoActual;
var listPreguntasEvaluacion=[];
function iniciarCursoPsicologia(indexPsico,indexCurso){
	$(".divListPrincipal>div").hide();
	$("#subListPsicologiaExamen").show();
	$("#subListPsicologiaExamen").find(".contenidoSubList").html("");
	var psico=listFullPsicologia[indexPsico];
	cursoActual=psico.cursos[indexCurso];
	var dataParam2={id:psico.id};
	
	 listPreguntasEvaluacion=[];
	 var listOpciones=[];
	callAjaxPost(URL + '/psicologia/valorizacion/preguntas', dataParam2,function(data){
		listPreguntasEvaluacion=data.list;
		listPreguntasEvaluacion.forEach(function(val,index){
			$("#subListPsicologiaExamen").find(".contenidoSubList")
			.append("<div class='eventoGeneral'><div class='divPreguntaCurso' id='pregCurso"+val.id+"'>" +
					"<div class='divNombrePregunta'>"+
					textoBotonTogglePregunta+
					(index+1)+". "+val.nombre+"" +
							
							"<br><label class='respuestaPregunta'></label>" +
							"</div>" +
					"<div class='divOpcionesGosst' style='width:100%'></div>" +
					"</div></div>");
			var listOpciones=val.opciones;
			$("#subListPsicologiaExamen").find(".tituloSubList").html(""+textoBotonVolverContenidoColaborador+""+psico.nombre+"")
			
			listOpciones.forEach(function(val1,index1){
				var textoObservacion="";
				if (val1.observacion.length==0){
					textoObservacion=""
				}else{
					textoObservacion=" ("+val1.observacion+")"
				}
				$("#subListPsicologiaExamen").find("#pregCurso"+val.id+" .divOpcionesGosst").append(
						"<div class='opcionGosstCss' onclick='llenarRespuestaPreguntaPsico(this)'>" +
						"<input  type='radio' name='opcionCurso"+val.id+"' id='opcionCurso"+val.id+"-"+val1.opcion.id+"'>" +
						"<label for='opcionCurso"+val.id+"-"+val1.opcion.id+"'>"+val1.opcion.nombre+textoObservacion+"</label>" +
						"</div>"		
						) ;
			});
		});
		$(".respuestaPregunta").hide();
		$("#subListPsicologiaExamen").find(".contenidoSubList")
		.append("<div class='eventoGeneral' style='height: 60px;'>" +
				"<button class='btn btn-success' style='float:right' " +
				"onclick='completarEvaluacionPsicologica()' ><i class='fa fa-check' > </i>Completar Evaluación </button>" +
				"</div>");
		
		 
	});
}
function llenarRespuestaPreguntaPsico(obj){
	
	$(obj).find("input").prop("checked",true);
	var opcionTexto=$(obj).find("label").text(); 
	$(obj).parent(".divOpcionesGosst").hide();
	$(obj).parent(".divOpcionesGosst").parent(".divPreguntaCurso").find(".respuestaPregunta").show().html(opcionTexto);
}
function completarEvaluacionPsicologica(){
	listPreguntasEvaluacion.forEach(function(val,index){
		val.opcionAsociada=null;
		val.opciones.forEach(function(val1,index1){
			var opcionCheck=$("#opcionCurso"+val.id+"-"+val1.opcion.id).prop("checked");
			if(opcionCheck){
				val.opcionAsociada={id:val1.opcion.id};
			}
		});
	});
	var examenCompletado=true;
	listPreguntasEvaluacion.forEach(function(val,index){
		 if(val.opcionAsociada==null){
			
			 examenCompletado=false;
		 }else{
			  
		 }
	});
	if(examenCompletado){
		var cursoObj={
				id:cursoActual.id,
				trabajadorId:getSession("trabajadorGosstId"),
				preguntas:listPreguntasEvaluacion
		}
		callAjaxPostNoLoad(URL + '/scheduler/trabajador/psicologia/evaluacion', cursoObj,function(data){
			switch(data.CODE_RESPONSE){
			case "05":
				
				alert("Resultados guardados!");
				marcarIconoModuloSeleccionado(9);
				break;
				default:
					console.log("nop")
					break;
			}
		},function(){
			$("#subListPsicologiaExamen").find(".tituloSubList").html("Evaluando resultados...")
			$("#subListPsicologiaExamen").find(".contenidoSubList").html("")
			
		})
	}else{
		 alert("Falta Completar algunas preguntas");
	}

}
var escondioAviso=false;
function eliminarAvisoGosst(){
	$(".gosst-aviso").remove();
	escondioAviso=true;
}
function habilitarEvaluacionSicologicaUsuario(){
	var dataParam2 = {
			trabajadorId :getSession("trabajadorGosstId") 
		};
	var numActuales=0;
	$(".divListPrincipal")
	.append(
			"<div id='subListPsicologiaPendiente'  >" + 
			"<div class='tituloSubList'></div>" +
			"<div class='contenidoSubList'></div>" +
			"</div>" +
			"<div id='subListPsicologiaCompletada'  >" +
			"<div class='tituloSubList'></div>" +
			"<div class='contenidoSubList'></div>" +
			"</div>"+
			"<div id='subListPsicologiaFutura'  >" +
			"<div class='tituloSubList'></div>" +
			"<div class='contenidoSubList'></div>" +
			"</div>"+ 
			"<div id='subListPsicologiaCerrada'  >" +
			"<div class='tituloSubList'></div>" +
			"<div class='contenidoSubList'></div>" +
			"</div>"+
			"<div id='subListPsicologiaExamen'  >" +
			"<div class='tituloSubList'>Cargando</div>" +
			"<div class='contenidoSubList'></div>" +
			"</div>" 
			);
	$("#subListPsicologiaPendiente .tituloSubList").css({"color":colorPuntajeGosst(0.5)+" !important"});
	$("#subListPsicologiaCompletada .tituloSubList").css({"color":colorPuntajeGosst(1.0)+" !important"});
	//$("#subListPsicologiaFutura .tituloSubList").css({"color":colorPuntajeGosst(0.5)+" !important"});
	$("#subListPsicologiaCerrada .tituloSubList").css({"color":colorPuntajeGosst(0.0)+" !important"});
	
	$("#subListPsicologiaExamen").hide();
	var textoConfidencial="Estimado colaborador: " 
		+"<br><br>En conformidad con la Ley N° 29783 “Ley de Seguridad y Salud en el trabajo”, la siguiente encuesta es de carácter anónima y los resultados serán confidenciales y evaluados de manera global. Para garantizar ello, el equipo GOSST ha tomado las medidas necesarias para proteger la información que ingresen los colaboradores, no pudiendo relacionarse una respuesta particular con un colaborador determinado. Una vez guardada la evaluación, no quedará un rastro electrónico que permita asociar a los colaboradores con sus respuestas. Cualquier duda sobre este aspecto no dude en consultarnos al correo adrian.cox@aswan.pe" 
		+"<br><br>Muchas gracias por su participación. "
		+"<br><br>GOSST"
		+"<br><br>Tu Seguridad, Nuestra Cultura." +
		"<br><br><button class='btn btn-success' onclick='eliminarAvisoGosst()'>Esconder aviso</button>";
	$(".gosst-aviso").remove();
	if(!escondioAviso){
		$(".divTituloFull").append("<div class='gosst-aviso'>"+textoConfidencial+"</div>"); 
	}


	callAjaxPost(URL + '/scheduler/trabajador/psicologia', dataParam2,function(data){
		 listFullPsicologia=data.list;
		var numCursoPendiente=0,numCursoCompleto=0,numCursoFuturo=0,numCursoCerrado=0;
		var textCursoPendiente="",textCursoCompleto="",textCursoFuturo="",textCursoCerrado="";
		  
		listFullPsicologia.forEach(function(val,index){
			var cursos=val.cursos;
			
			var noticia="<div class='cropGosst'><img src='../imagenes/psicologia.jpg'></div>";
			
			cursos.forEach(function(val1,index1){
				var puntajeNoAprobado=true;
				if(val1.numPreguntasContestadas>0){
					puntajeNoAprobado=false;
				}
				var textoAvance="("+val1.numPreguntas+" preguntas)";
				var textoAuxiliar="";
				var btnIniciarCurso="<button class='btn-success btn' onclick='iniciarCursoPsicologia("+index+","+index1+")'" +
				" ><i class='fa fa-image' aria-hidden='true'></i>Iniciar evaluación</button>";
				if(val1.estado.id==2 && puntajeNoAprobado){ 
					textoAuxiliar=btnIniciarCurso;
				}else{
					var divCursoAsoc;
					if(val1.estado.id==1){  
						textoAuxiliar="Disponible: "+convertirFechaNormal(val1.fechaInicio) 
						 
					}
					if(val1.estado.id==2){ 
						textoAuxiliar="<label>¡Felicidades! Completó la evaluación correctamente. <label>";
						 
					}
					
					if(val1.estado.id==3){
						textoAuxiliar= "<label>Cerrado<label>"; 
					}
				}
				
				var textCurso="<div class='eventoGeneral' id='cursoMovil"+val1.id+"'    >" +
				"<div id='tituloEvento'>" +
				"<table class='table'>" +
				"<tbody>" +
					"<tr>" +
						"<td style='font-size: 24px; width:30px'>"+"<i aria-hidden='true' class='fa fa-cogs'></i>"+"</td>" +
						"<td style='text-align: left;'>"+"<strong >" +  val.nombre +"  </strong> </td>" +
					"</tr>" +
					"<tr>" +
					"<td style='font-size: 24px; width:30px'>"+"<i aria-hidden='true' class='fa fa-info'></i>"+"</td>" +
					"<td style='text-align: left;'>"+"<strong >" +  val1.titulo+"   "+textoAvance+"</strong> </td>" +
				"</tr>" +
				"<tr>" +
				"<td colspan=3>"+noticia+"</td>" +
				"</tr>"+
				"<tr>" +
				"<td  >  </td>" +
				"<td>"+textoAuxiliar+"</td>" + 
				"</tr>" +
				"</tbody>" +
				"</table>"+ 
				"</div>"+
				"</div>";
				if(val1.estado.id==2 && puntajeNoAprobado){
					numCursoPendiente+=1;
					textCursoPendiente+=textCurso; 
				}else{
					var divCursoAsoc;
					if(val1.estado.id==1){ 
						numCursoFuturo+=1;
						textCursoFuturo+=textCurso; 
						 
					}
					if(val1.estado.id==2){
						numCursoCompleto+=1;
						textCursoCompleto+=textCurso; 
						 
					}
					
					if(val1.estado.id==3){ 
						numCursoCerrado+=1;
						textCursoCerrado+=textCurso;
					}
				}
			});
			
			
			
		});
		$("#subListPsicologiaPendiente").find(".tituloSubList")
		.html("<i aria-hidden='true' class='fa fa-book'></i>Pendientes ("+numCursoPendiente+")"+textoBotonToggleContenidoColaborador);
		$("#subListPsicologiaPendiente").find(".contenidoSubList")
		.html(textCursoPendiente);
		$("#subListPsicologiaCompletada").find(".tituloSubList")
		.html("<i aria-hidden='true' class='fa fa-book'></i>Completadas ("+numCursoCompleto+")"+textoBotonToggleContenidoColaborador);
		$("#subListPsicologiaCompletada").find(".contenidoSubList")
		.html(textCursoCompleto);
		$("#subListPsicologiaFutura").find(".tituloSubList")
		.html("<i aria-hidden='true' class='fa fa-book'></i>Futuras ("+numCursoFuturo+")"+textoBotonToggleContenidoColaborador);
		$("#subListPsicologiaFutura").find(".contenidoSubList")
		.html(textCursoFuturo);
		$("#subListPsicologiaCerrada").find(".tituloSubList")
		.html("<i aria-hidden='true' class='fa fa-book'></i>Cerradas ("+numCursoCerrado+")"+textoBotonToggleContenidoColaborador);
		$("#subListPsicologiaCerrada").find(".contenidoSubList")
		.html(textCursoCerrado);
	});
	 
}
 
function habilitarDocumentosUsuario(){
	var dataParam2 = {
			trabajadorId :getSession("trabajadorGosstId") 
		};
	callAjaxPost(URL + '/scheduler/trabajador/documentos', dataParam2,function(data){
		$(".divListPrincipal")
		.append(
				"<div id='subListDocumentos'  >" +
				"<div class='tituloSubList' style='color : "+colorPuntajeGosst(1)+"'></div>" +
				"<div class='contenidoSubList'></div>" +
				"</div>" +
				"<div id='subListDocumentosNoActuales'  >" +
				"<div class='tituloSubList' style='color : "+colorPuntajeGosst(0)+"'></div>" +
				"<div class='contenidoSubList'></div>" +
				"</div>" 
				);
		var documentos = data.listpeligros;
var docsTabla="",docsTablaNoactual="";
var numActuales=0,numNoActuales=0;
documentos.forEach(function(val,index){
	var colorFondo="gray";
	var iconoEvi="<a  id='evidenciaEvento"+val.id+"' class='efectoLink' target='_blank' href='"+URL 
				+ "/procedimientoseguridad/version/evidencia?versionId="+val.positionId+"' >" +
						""+val.evidenciaNombre+"</a>";
	if(val.evidenciaNombre=="----" || val.evidenciaNombre=="" || val.evidenciaNombre==null){
		iconoEvi="<strong style='color:red'>No hay evidencia</strong>"
	}
	var textoFechaVigencia="<i class='fa fa-calendar-check-o'></i>&nbspSe actualizó: "+convertirFechaNormal(val.fechaActualizacion);
	var difDias=restaFechas(convertirFechaInput(val.fechaActualizacion),obtenerFechaActual());
	if(difDias>0){
		numActuales++;
	}else{
		numNoActuales++;
		textoFechaVigencia="<i class='fa fa-calendar-plus-o'></i>&nbspSe actualizará: "+convertirFechaNormal(val.fechaActualizacion);
	}
	var textoDocs="";
	textoDocs="<div class='detalleFormacionTrabajador' style='border-left: 10px solid "+colorFondo+";   '>" +
	"<div style='font-weight:600;font-size:15px'>"+val.procedimientoSeguridadNombre+"</div>" +
	"<div >"+textoFechaVigencia+"   </div>" + 
	"<div >"+
	"<i class='fa fa-download' " +
						"  aria-hidden='true'></i>" +
	iconoEvi+
	
	""+
	"   </div>" + 
	"</div>";
	if(difDias>0){
		docsTabla+=textoDocs;
	}else{ 
		docsTablaNoactual+=textoDocs;
	}
	
});
		$("#subListDocumentos").find(".tituloSubList")
		.html("<i aria-hidden='true' class='fa fa-book'></i>Actualizados ("+numActuales+")"+textoBotonToggleContenidoColaborador);
		$("#subListDocumentos").find(".contenidoSubList")
		.html(
				docsTabla
		);
		$("#subListDocumentosNoActuales").find(".tituloSubList")
		.html("<i aria-hidden='true' class='fa fa-book'></i>Por Actualizar ("+numNoActuales+")"+textoBotonToggleContenidoColaborador);
		$("#subListDocumentosNoActuales").find(".contenidoSubList")
		.html(
				docsTablaNoactual
		);
	})
	
}

function habilitarEventosTrabajador(){
	
	var dataParam2 = {
			trabajadorId :getSession("trabajadorGosstId")
		};
	
	callAjaxPost(URL + '/trabajador/eventos', dataParam2,function(data){
		var list = data.list;
		var listNegativa = data.listEventosNegativos;
		var contenidoImplementar="", contenidoCompletado="", contenidoRetrasado="",contenidoEventInseguro="",contenidoAcc="";
		 
		var implementarNum=0,completarNum=0,retrasadoNum=0,eventoNum=0,accidenteNum=0;
		list.sort(function(a,b){
			return new Date(b.fecha) - new Date(a.fecha);
			});
		listNegativa.sort(function(a,b){
			return new Date(b.fecha) - new Date(a.fecha);
			});
		list.forEach(function(val,index){
			var colorFondo="gray";
			//var textEvi="<a class='btn btn-success' href='"+URL+val.urlEvidencia+"' target='_blank'  >" +
			//"<i class='fa fa-download'> </i>Descargar</a>";
			if(val.nombreEvidencia.length>0){
				val.nombreEvidencia="<a class='efectoLink' target='_blank' "
							 +"href='"+URL+val.urlEvidencia+"'>"+val.nombreEvidencia+"</a>  <br>";//+textEvi;
			}else{
				val.nombreEvidencia="<strong style='color:red'>No hay evidencia</strong>";
			}
			var textIn ="<div class='detalleFormacionTrabajador' style='border-left: 10px solid "+colorFondo+";   '>" +
			"<div style='font-weight:600;font-size:15px'>"+val.eventoNombre+"</div>" +
			"<div ><i class='fa fa-calendar'></i>   "+val.fechaTexto+ "</div>" + 
			"<div ><i class='fa fa-exclamation-circle'></i> "+val.estadoNombre+"    </div>" +  
			"<div ><i class='fa fa-download'></i> "+val.nombreEvidencia+"</div>" +  
			"</div>";
			if(val.estado){ 
				switch(val.estado.id){
				case 1:
					contenidoImplementar+=textIn;
					implementarNum++;
					break;
				case 2:
					contenidoCompletado+=textIn;
					completarNum++;
					break;
				case 3:
					contenidoRetrasado+=textIn;
					retrasadoNum++;
					break;
				default:
					contenidoImplementar+=textIn;
					implementarNum++;
					break;
				}
			}
		});
		listNegativa.forEach(function(val,index){
			var colorFondo="gray";
			var textEvi="<a class='btn btn-success' href='"+URL+val.urlEvidencia+"' target='_blank'  >" +
					"<i class='fa fa-download'> </i>Descargar</a>";
			if(val.nombreEvidencia.length>0){
				val.nombreEvidencia="<a class='efectoLink' target='_blank' "
							 +"href='"+URL+val.urlEvidencia+"'>"+val.nombreEvidencia+"</a>  <br>";//+textEvi;
			}else{
				val.nombreEvidencia="<strong style='color:red'>No hay evidencia</strong>";
			}
			var textIn ="<div class='detalleFormacionTrabajador' style='border-left: 10px solid "+colorFondo+";   '>" +
			"<div style='font-weight:600;font-size:15px'>"+val.tipoNombre+" - "+val.eventoNombre+"</div>" +
			"<div >  "+val.fechaTexto+ "</div>" +  
			"<div > <i class='fa fa-download'></i>"+val.nombreEvidencia+"    </div>" +  
			"</div>" + 
			"";   
			switch(val.eventoTipoId){
			case 4:
				contenidoEventInseguro+=textIn;
				eventoNum++;
				break;
			case 5:
				contenidoEventInseguro+=textIn;
				eventoNum++;
				break;
			case 6:
				contenidoAcc+=textIn;
				accidenteNum++;
				break; 
			}
			 
		});
		var listPaneles=[
		                 {id:"divImplementar",nombre:"Por asistir ("+implementarNum+")",contenido:contenidoImplementar},
		                 {id:"divCompletado",nombre:"Completados ("+completarNum+")",contenido:contenidoCompletado},
		                 {id:"divRetrasado",nombre:"Retrasados ("+retrasadoNum+")",contenido:contenidoRetrasado}
		               // , {id:"divActos",nombre:"Actos y condiciones subestándar reportados ("+eventoNum+")",contenido:contenidoEventInseguro},
		               //  {id:"divAccidntes",nombre:"Accidente ("+accidenteNum+")",contenido:contenidoAcc}
		                 ]
		agregarPanelesDivPrincipal(listPaneles);
		$("#divImplementar .tituloSubList").css({"color":colorPuntajeGosst(0.5)+" !important"});
		$("#divCompletado .tituloSubList").css({"color":colorPuntajeGosst(1.5)+" !important"});
		$("#divRetrasado .tituloSubList").css({"color":colorPuntajeGosst(0.0)+" !important"});
		contadorRetrasadoAlertEventos=retrasadoNum;//variable global almacenando la cantidad de Eventos retrasados. 
		if(contadorRetrasadoAlertEventos>0)
		{  
			$("#icoEventActividades label i").remove();
			$("#icoEventActividades label").append("<i style='color:red; font-size: 1.8em;'class='fa fa-exclamation-circle'></i>");
		}
		else
		{
			$("#icoEventActividades label i").remove();
			$("#icoEventActividades label").append("");
		} 
	});
}
function habilitarHistorialUsuario(){
	var dataParam2 = {
			trabajadorId :sessionStorage.getItem("trabajadorGosstId"),
			palabraEspacio:"<br>" 
		};
	
	callAjaxPostNoLoad(URL + '/trabajador/puntajes', dataParam2,function(data){
		$(".divListPrincipal")
		.append(
				"<div id='subHistorialCapacitacion'  >" +
				"<div class='tituloSubList'></div>" +
				"<div class='contenidoSubList'></div>" +
				"</div>"  +
				"<div id='subHistorialEpp'  >" +
				"<div class='tituloSubList'></div>" +
				"<div class='contenidoSubList'></div>" +
				"</div>"  +
				"<div id='subHistorialExam'  >" +
				"<div class='tituloSubList'></div>" +
				"<div class='contenidoSubList'></div>" +
				"</div>"  
		);
		
			var indicadoresSeguridad = data.indicadoresSeguridad;

		var indEvento = data.indEvento;
		var indAcc = data.indAcc;
		var  indEpp = data.indEpp;
		var indExam = data.indExam;
		var indFormacion = data.indFormacion;
		var listActosReportados=data.listActosReportados;
		var primerDia = new Date();
		primerDia=convertirFechaNormal2(primerDia);
		var capacitacionesTrab=[];
		if(indFormacion.length>0){
			capacitacionesTrab=indFormacion[0].caps;
		}
		
		var indicadorRangoActual=1;
		var capacitacionesTabla="",programsCapList=[],entregasEppList=[],examenesList=[];
		var participacionTabla="";
		var eppsTabla=" " ,examTabla="";
		
		
		for(var index22=0;index22<capacitacionesTrab.length;index22++){ 
			var programs=capacitacionesTrab[index22].programsCap;
			programs.forEach(function(val31,index31){
				var eventoIngresa=true;
				if(val31.fechaPlaTexto==null){
					eventoIngresa=false;
				}
				val31.capacitacionNombre=capacitacionesTrab[index22].capacitacionNombre;
				if(val31.fechaRealTexto==null){
					eventoIngresa=false;
					}
				if(eventoIngresa){
					programsCapList.push(val31);
				}
				
			}); 
				
		};
		var eppsTrabajador=[];
		if(indEpp.length>0){
			eppsTrabajador=indEpp[0].epps;
		}
		for(var index22=0;index22<eppsTrabajador.length;index22++){ 
			var programs=eppsTrabajador[index22].programsEpp;
			programs.forEach(function(val31,index31){
				var eventoIngresa=true;
				if(val31.fechaPlanificadaTexto==null){
					eventoIngresa=false;
				} 
				val31.equipoSeguridadNombre=eppsTrabajador[index22].equipoSeguridadNombre;
				if(val31.fechaEntregaTexto==null){
					eventoIngresa=false;
					}
				if(eventoIngresa){
					entregasEppList.push(val31);
				}
				
			}); 
				
		};
		var examsTrab=[];
		if(indExam.length>0){
			examsTrab=indExam[0].exams;
		}
	examsTrab.forEach(function(val,index){
		var programs=val.programsExams;
		programs.forEach(function(val31,index1){
			var eventoIngresa=true;
			if(val31.fechaInicialTexto==null){
				eventoIngresa=false;
			} 
			val31.examenMedicoNombre=val.examenMedicoNombre;
			if(val31.fechaFinTexto==null){
				eventoIngresa=false;
				}
			if(eventoIngresa){
				examenesList.push(val31);
			}
		});
		
	});
		programsCapList.sort(function(a,b){
			return new Date(a.fechaPlanificada) - new Date(b.fechaPlanificada);
			});
		entregasEppList.sort(function(a,b){
			return new Date(a.fechaPlanificada) - new Date(b.fechaPlanificada);
			});
		programsCapList.forEach(function(val31,index31){
				 var colorFondo="#bbb3b4";
					 var horaProgramada="";
					 var textoFechaPlaneada="";
					var textoFechaVigencia="----";
				var primerDia = new Date();
				primerDia=convertirFechaNormal2(primerDia); 
				var enRangoInicial=fechaEnRango(primerDia,val31.fechaPlaTexto,val31.fechaRealTexto)
				textoFechaPlaneada=""+convertirFechaNormal(val31.fechaPlaTexto);
				textoFechaVigencia=""+convertirFechaNormal(val31.fechaRealTexto);
					var textoAsistencia="Sin asistir",textoAprobacion="";
				if(val31.progCapTrabId>0){
					textoAsistencia="Asistió";
					
					if(val31.tipoNotaTrabId==1){
						textoAprobacion="<strong>Aprobado</strong>";
						colorFondo="#9BBB59"; 
					}else{
						textoAprobacion="<strong>No aprobado</strong>";
						colorFondo="#bb596b"; 
					}
				}
				if(restaFechas(val31.fechaPlaTexto,primerDia)<0  ){ 
					textoFechaVigencia="Próxima: "+convertirFechaNormal(val31.fechaRealTexto);
					horaProgramada=val31.horaPlanificada;
				}
				
				if(val31.tipoNotaTrabId==1 && enRangoInicial ){
					
					textoFechaVigencia="Vigente hasta: "+convertirFechaNormal(val31.fechaRealTexto);
					indicadorRangoActual=
						indicadorRangoActual+(1/capacitacionesTrab.length);
					 horaProgramada="";
				}
				
				capacitacionesTabla+="<div class='detalleFormacionTrabajador' style='border-left: 10px solid "+colorFondo+";   '>" +
				"<div style='font-weight:600;font-size:15px'>"+val31.capacitacionNombre+"</div>" +
				"<div >Realizado : "+textoFechaPlaneada+"    "+horaProgramada +"</div>" + 
				"<div >"+textoFechaVigencia+"    </div>" + 
				"<div >"+textoAsistencia+"     "+textoAprobacion +" </div>" + 
				"</div>";
			})
			
		var textoFechaVigenciaEpp="---",textoFechaEntregaEpp="";
		entregasEppList.forEach(function(val,index){
			var colorFondo="#bbb3b4";
			var primerDia = new Date();
			var horaProgramada="";
			primerDia=convertirFechaNormal2(primerDia); 
			var enRangoInicial=fechaEnRango(primerDia,val.fechaPlanificadaTexto,val.fechaEntregaTexto)
			textoFechaEntregaEpp="Última entrega: "+convertirFechaNormal(val.fechaPlanificada);
			var  boolEntreFechas=false;
			if(enRangoInicial ){
				 boolEntreFechas=true;
			}
			if(restaFechas(val.fechaPlanificadaTexto,primerDia)<0  ){
				textoFechaVigenciaEpp="Próxima Entrega: "+convertirFechaNormal(val.fechaEntregaTexto);
				horaProgramada=val.horaPlanificada;
			}
			if(val.tipoNotaTrabId!=null && boolEntreFechas ){
				colorFondo="#9BBB59"; 
				textoFechaVigenciaEpp="Próxima Entrega: "+convertirFechaNormal(val.fechaEntregaTexto);
			} 
		eppsTabla+="<div class='detalleEppsTrabajador' style='border-left: 10px solid "+colorFondo+"; '>" +
		"<div style='font-weight:600;font-size:15px'>"+val.equipoSeguridadNombre+"</div>" +
		"<div >"+textoFechaEntregaEpp+"    </div>" + 
		"<div >"+textoFechaVigenciaEpp+"    </div>" + 
		"</div>";
		})
		
		
		examenesList.forEach(function(val,index){
			var colorFondo="#bb596b";
			var fechaExamen="No Vigente";
				var primerDia = new Date();
				primerDia=convertirFechaNormal2(primerDia); 
				var enRangoInicial=fechaEnRango(primerDia,val.fechaInicialTexto,val.fechaFinTexto);
				var fechaLlevada="Realizada: "+convertirFechaNormal(val.fechaInicialTexto);
				var  boolEntreFechas=false;
				if(enRangoInicial ){
					 boolEntreFechas=true;
				}
				if(val.tipoNotaTrabId==1 &&boolEntreFechas ){
					fechaExamen="Vigente hasta: "+convertirFechaNormal(val.fechaFinTexto);
					 colorFondo="#9BBB59";
					index22=examsTrab.length;
				}
				examTabla+="<div class='detalleEppsTrabajador' style='border-left: 10px solid "+colorFondo+"; '>" +
				"<div style='font-weight:600;font-size:15px'>"+val.examenMedicoNombre+"</div>" +
				"<div >"+fechaLlevada+"    </div>" + 
				"<div >"+fechaExamen+"    </div>" + 
				"</div>";
			
		})
		$("#subHistorialCapacitacion").find(".tituloSubList").html("<i aria-hidden='true' class='fa fa-graduation-cap'></i>Formaciones "+textoBotonToggleContenidoColaborador);
		$("#subHistorialCapacitacion").find(".contenidoSubList").html(
				capacitacionesTabla
		);
		$("#subHistorialEpp").find(".tituloSubList").html("<i aria-hidden='true' class='fa fa-fire-extinguisher'></i>Equipos Seguridad "+textoBotonToggleContenidoColaborador);
		$("#subHistorialEpp").find(".contenidoSubList").html(
				eppsTabla
		);
		$("#subHistorialExam").find(".tituloSubList").html("<i aria-hidden='true' class='fa fa-medkit'></i>Exámenes Médicos "+textoBotonToggleContenidoColaborador);
		$("#subHistorialExam").find(".contenidoSubList").html(
				examTabla
		);
	})
}
function habilitarObjetivosUsuario(){ 
	var restriccionMedica="";
	
	
	var dataParam2 = {
			trabajadorId :sessionStorage.getItem("trabajadorGosstId"),
			palabraEspacio:"<br>" 
		};
	
	callAjaxPostNoLoad(URL + '/trabajador/puntajes', dataParam2,function(data){
		$(".divListPrincipal")
		.append(
				"<div id='subListCapacitacion'  >" +
				"<div class='tituloSubList'></div>" +
				"<div class='contenidoSubList'></div>" +
			"</div>" + 
				"<div id='subListEquipos'  >" +
					"<div class='tituloSubList'></div>" +
					"<div class='contenidoSubList'></div>" +
				"</div>" + 
				"<div id='subListMedico'  >" +
				"<div class='tituloSubList'></div>" +
				"<div class='contenidoSubList'></div>" +
			"</div>" + 
			"<div id='subListDisponible'  >" +
			"<div class='tituloSubList'></div>" +
			"<div class='contenidoSubList'></div>" +
		"</div>" + 
		"<div id='subListParticipacion'  >" +
		"<div class='tituloSubList'></div>" +
		"<div class='contenidoSubList'></div>" +
		"</div>"+
			"</div>" 
		);
		var indicadoresSeguridad = data.indicadoresSeguridad;

		var indEvento = data.indEvento;
		var indAcc = data.indAcc;
		var  indEpp = data.indEpp;
		var indExam = data.indExam;
		var indFormacion = data.indFormacion;
		var listActosReportados=data.listActosReportados;
		var primerDia = new Date();
		primerDia=convertirFechaNormal2(primerDia);
		var capacitacionesTrab=[];
		if(indFormacion.length>0){
			capacitacionesTrab=indFormacion[0].caps;
		}
		
		var indicadorRangoActual=1;
		var capacitacionesTabla="";
		var participacionTabla="";
		var eppsTabla=" " ;
		
		var colorFondo="#bbb3b4";
		var horaProgramada="";
		for(var index22=0;index22<capacitacionesTrab.length;index22++){
			if(index22==0){
				indicadorRangoActual=0
			}
			var programs=capacitacionesTrab[index22].programsCap;
			colorFondo="#bbb3b4"
			var textoFechaVigencia="----";
			 horaProgramada="";
			for(var index31=0;index31<programs.length;index31++){
				var primerDia = new Date();
				primerDia=convertirFechaNormal2(primerDia);
				if(programs[index31].fechaPlaTexto==null){
					programs[index31].fechaPlaTexto=primerDia;
				}
				
				if(programs[index31].fechaRealTexto==null){
					programs[index31].fechaRealTexto=programs[index31].fechaPlaTexto;
				}
				var enRangoInicial=fechaEnRango(primerDia,programs[index31].fechaPlaTexto,programs[index31].fechaRealTexto)
				if(!enRangoInicial && programs[index31].progCapEstado==2){
					textoFechaVigencia="Venció el: "+convertirFechaNormal(programs[index31].fechaRealTexto);
				}
				if(restaFechas(programs[index31].fechaPlaTexto,primerDia)<0  ){ 
					textoFechaVigencia="Próxima: "+convertirFechaNormal(programs[index31].fechaRealTexto);
					horaProgramada=programs[index31].horaPlanificada;
				}
				
				if(programs[index31].tipoNotaTrabId==1 && enRangoInicial ){
					colorFondo="#9BBB59"; 
					textoFechaVigencia="Vigente hasta: "+convertirFechaNormal(programs[index31].fechaRealTexto);
					indicadorRangoActual=
						indicadorRangoActual+(1/capacitacionesTrab.length);
					index31=programs.length;
					 horaProgramada="";
				}
				
				
			}
		
			capacitacionesTabla+="<div class='detalleFormacionTrabajador' style='border-left: 10px solid "+colorFondo+";   '>" +
					"<div style='font-weight:600;font-size:15px'>"+capacitacionesTrab[index22].capacitacionNombre+"</div>" +
					"<div >"+textoFechaVigencia+"    "+horaProgramada +"</div>" + 
					"</div>";
		}
		////
		
				var examsTrab=[];
				if(indExam.length>0){
					examsTrab=indExam[0].exams;
				}
				var fechaExamen="Sin exámenes";
				var colorFondoMedico=" "; 
				var restriccionPuntaje=0;
				colorFondoMedico="#bb596b"; 
				restriccionPuntaje=0;
				for(var index22=0;index22<examsTrab.length;index22++){
					
					var programs=examsTrab[index22].programsExams;
					
					for(var index33=0;index33<programs.length;index33++){
						var primerDia = new Date();
						primerDia=convertirFechaNormal2(primerDia);
						if(programs[index33].fechaInicialTexto==null){
							programs[index33].fechaInicialTexto=primerDia;
						}
						if(programs[index33].fechaFinTexto==null){
							programs[index33].fechaFinTexto=programs[index33].fechaInicialTexto;
						}
						var enRangoInicial=fechaEnRango(primerDia,programs[index33].fechaInicialTexto,programs[index33].fechaFinTexto)
						var  boolEntreFechas=false;
						if(enRangoInicial ){
							 boolEntreFechas=true;
						}
						if(programs[index33].tipoNotaTrabId==1 &&boolEntreFechas ){
							fechaExamen="Vigente hasta: "+convertirFechaNormal(programs[index33].fechaFinTexto);
							index33=programs.length;

							index22=examsTrab.length;
							restriccionPuntaje=1;
							colorFondoMedico="#9BBB59"; 
						}
					}
				
					
				}
				var dataParamAux = {
						mdfId:getSession("unidadGosstId"),
						empresaId:getSession("gestopcompanyid"),
						trabajadorId:parseInt(sessionStorage.getItem("trabajadorGosstId"))
					};
				callAjaxPostNoLoad(URL + '/trabajador', dataParamAux,function(data){
					restriccionMedica=data.list[0].restriccion;
					
					
					if(restriccionMedica.length==0){
						
						restriccionMedica="No tiene restricciones";
					}else{
						
						restriccionMedica="Restricción: "+restriccionMedica;
					}
					$("#subListMedico").find(".tituloSubList").html("<i aria-hidden='true' class='fa fa-medkit'></i>Examen Médico ("+pasarDecimalPorcentaje(restriccionPuntaje,2)+")");
					$("#subListMedico").find(".contenidoSubList").html(
							"<div class='detalleMedicoTrabajador' style='border-color:"+colorFondoMedico+"' >" +
							" "+fechaExamen+", <br>"+restriccionMedica+
							"</div>"
					);
				});
		///
		
				var eppsTrabajador=[];
				if(indEpp.length>0){
					eppsTrabajador=indEpp[0].epps;
				}
				var indicadorEpp=1;
				horaProgramada="";
				for(var index22=0;index22<eppsTrabajador.length;index22++){
					if(index22==0){
						indicadorEpp=0
					}
					var colorFondo="#bbb3b4";
					var textoFechaVigenciaEpp="---";
					var programs=eppsTrabajador[index22].programsEpp;
					
					for(var index31=0;index31<programs.length;index31++){
						var primerDia = new Date();
						primerDia=convertirFechaNormal2(primerDia);
						if(programs[index31].fechaPlanificadaTexto==null){
							programs[index31].fechaPlanificadaTexto=primerDia;
						}
						if(programs[index31].fechaEntregaTexto==null){
							programs[index31].fechaEntregaTexto=programs[index31].fechaPlanificadaTexto;
						}
						var enRangoInicial=fechaEnRango(primerDia,programs[index31].fechaPlanificadaTexto,programs[index31].fechaEntregaTexto)
						var  boolEntreFechas=false;
						if(enRangoInicial ){
							 boolEntreFechas=true;
						}
						if(restaFechas(programs[index31].fechaPlanificadaTexto,primerDia)<0  ){
							textoFechaVigenciaEpp="Próxima Entrega: "+convertirFechaNormal(programs[index31].fechaEntregaTexto);
							horaProgramada=programs[index31].horaPlanificada;
						}
						if(programs[index31].tipoNotaTrabId!=null && boolEntreFechas ){
							colorFondo="#9BBB59"; 
							textoFechaVigenciaEpp="Próxima Entrega: "+convertirFechaNormal(programs[index31].fechaEntregaTexto);
							indicadorEpp=
								indicadorEpp+(1/eppsTrabajador.length);
							index31=programs.length;
						}
					} 
					eppsTabla+="<div class='detalleEppsTrabajador' style='border-left: 10px solid "+colorFondo+"; '>" +
					"<div style='font-weight:600;font-size:15px'>"+eppsTrabajador[index22].equipoSeguridadNombre+"</div>" +
					"<div >"+textoFechaVigenciaEpp+"    "+horaProgramada +"</div>" + 
					"</div>";
					
				}
				
			//
				var indicadorPart=0;
				listActosReportados.forEach(function(val,index){
					participacionTabla+="<div class='detalleEppsTrabajador' style='border-color:"+val.color+"'>" +
					"<div  >"+val.acInseguroTipoNombre+"- "+val.acInseguroNombre+"</div>" + 
					"<div  ><strong>Mejora:</strong> "+val.iconoEstadoAccion+""+val.nombreAccionMejora+"</div>" +
					"</div>";
				});
				var dias=	indAcc[0].indicadorDiasDescanso*5*52	;
				var reportes = indEvento[0].numEventosReportados;

		 
		var divTrabajadores=$("#divTrabajadorGosst");
		$("#subListCapacitacion").find(".tituloSubList").html("<i aria-hidden='true' class='fa fa-graduation-cap'></i>Formaciones ("+pasarDecimalPorcentaje(indicadorRangoActual,2)+") "+textoBotonToggleContenidoColaborador);
		$("#subListCapacitacion").find(".contenidoSubList").html(
				capacitacionesTabla
		);
		$("#subListEquipos").find(".tituloSubList").html("<i aria-hidden='true' class='fa fa-fire-extinguisher'></i>EPP´s ("+pasarDecimalPorcentaje(indicadorEpp,2)+")" +textoBotonToggleContenidoColaborador);
		$("#subListEquipos").find(".contenidoSubList").html(
				eppsTabla
		);
		
		var indicadorAcc = indAcc[0].indicadorDiasDescanso;
		var sumaDiasDescanso=indAcc[0].sumaDiasDescanso;
		var diasTrabajados=   indAcc[0].diasTrabajados
		$("#subListDisponible").find(".tituloSubList").html("<i aria-hidden='true' class='fa fa-fire-extinguisher'></i>Disponibilidad ("+pasarDecimalPorcentaje(indicadorAcc)+")" +textoBotonToggleContenidoColaborador);
		$("#subListDisponible").find(".contenidoSubList").html(
				"<div class='detalleMedicoTrabajador' >" +
				(diasTrabajados-sumaDiasDescanso)+" /"+diasTrabajados+
				"</div>"
		);
		var asd=(reportes / indicadoresSeguridad.indicadorMinimoParticipacion)>1?1:(reportes / indicadoresSeguridad.indicadorMinimoParticipacion);
		$("#subListParticipacion").find(".tituloSubList").html("<i aria-hidden='true' class='fa fa-fire-extinguisher'></i>" +
				"Participación ("+pasarDecimalPorcentaje(asd)+")" +textoBotonToggleContenidoColaborador);
		$("#subListParticipacion").find(".contenidoSubList").html(
				participacionTabla
		);
		 
		
		
		$(".listOpcionFiltroGosst .opcionFiltroGosst").show()
		
	})
}
function habilitarAyudaUsuario(){
	habilitarVistaUsuario();
}
var listFullPeligros;
var listFullActividades;
var listActividadesMarcadas=[];



var listEstadosValidarColaborador = [];
var listActividadesResponsableColaborador = [];
var listPuestosResponsableColaborador = [];
var listAreasResponsableColaborador = [];
var listDivisionesResponsableColaborador = [];
var listTrabajadoresResponsableColaborador = [];
var listIpercColaborador = [];
function habilitarIpercUsuario(){
	var textoForm="";
	
	var listPaneles=[
	               
	                 {id:"subVerMatrizIperc",nombre:"",contenido:""},
	                 {id:"subConsultarMatrizIperc",nombre:"",contenido:""},
	                 
	                 
	                  {id:"subDescargarRecomendaciones",nombre:"",contenido:""},
	                 {id:"subListActividades",nombre:"",contenido:""},
	                 {id:"subListPeligros",nombre:"",contenido:""}
	                 ];
	
	agregarPanelesDivPrincipalColaborador(listPaneles);
	
	var buttonAtajo1 = "<button style='float : left' onclick='volverVistaIperc()' " +
	"class='btn btn-success'><i class='fa fa-2x fa-arrow-left'></i></button>";

	
	var buttonAtajo2 = "<button onclick='verTablaResumenIpercResponsable(1)' " +
		"class='btn btn-success'><i class='fa fa-2x fa-arrow-right'></i></button>";
	$(".divListPrincipal").find("#subVerMatrizIperc .tituloSubList")
	.html(buttonAtajo2+"<i class='fa fa-check-square-o'></i> Validar Matriz IPERC asignada al trabajador ");
	var buttonAtajo3 = "<button onclick='verTablaResumenIpercResponsable(0)' " +
	"class='btn btn-success'><i class='fa fa-2x fa-arrow-right'></i></button>";
$(".divListPrincipal").find("#subConsultarMatrizIperc .tituloSubList")
.html(buttonAtajo3+"<i class='fa fa-search'></i> Consultar matriz IPERC en la empresa ");
	
	var buttonDescargaRecomendacion="" +
			"<a style='float: right;font-size: 12px;' target='_blank' class='btn btn-success' " +
			"href='"+URL+"/puesto/detalle/hojaruta?puestoTrabajoId="+getSession("puestoGosstId")+"'>" +
					"<i class='fa fa-download' aria-hidden='true'></i></a>"
	$(".divListPrincipal").find("#subDescargarRecomendaciones .tituloSubList")
	.html(buttonDescargaRecomendacion+"<i class='fa fa-download'></i> Descargar recomendaciones (Actualizadas el: "+getSession("fechaActualizacionPuesto")+" )")
	
	var dataParam={
		trabajadorId:getSession("trabajadorGosstId"),
		empresaId : getSession("gestopcompanyid")
	}
	callAjaxPost(URL + '/scheduler/trabajador/perfil', dataParam, function(data){
		listFullPeligros=data.listpeligros;
		listIpercColaborador = data.listIperc;
		listFullActividades=[];
		listFullPeligros.forEach(function(val,index){
			var listFullActividadesAux=listarStringsDesdeArray(listFullActividades,"nombre");
			if(listFullActividadesAux.indexOf(val.actividad)=="-1"){
				listFullActividades.push({nombre:val.actividad});
			}
			 
		});
		$(".divListPrincipal").find("#subListActividades .contenidoSubList")
		.append("<div class='eventoGeneral'  >" +
				"<div id='tituloEvento'>" +
				"<table class='table'>" +
				"<tbody>" + 
					 
				"</tbody>" +
				"</table>"+ 
			 
				"</div>");
		$(".divListPrincipal").find("#subListActividades .contenidoSubList table tbody")
		.append("<tr style='background-color: #dadcdb'>" +
					"<td style='width: 40px'>"+"<input checked id='inputCheckActividadIperTotal' onchange='verPeligrosAcordeActividades(1)' type='checkbox' class='form-control'>"+"</td>" + 
					"<td style='text-align: left;vertical-align: middle;'>"+"TODOS"+"</td>" +
				"</tr>"  
				 );
		listFullActividades.forEach(function(val,index){
			var textChecked="checked"
			if(listActividadesMarcadas.length>0){
				textChecked=(listActividadesMarcadas[index]?"checked":"");
			}
			$(".divListPrincipal").find("#subListActividades .contenidoSubList table tbody")
			.append("<tr>" +
						"<td style='width: 40px'>"+"<input "+textChecked+" id='inputCheckActividadIper"+index+"' onchange='verPeligrosAcordeActividades(0)' type='checkbox' class='form-control'>"+"</td>" + 
						"<td style='text-align: left;vertical-align: middle;'>"+val.nombre+"</td>" +
					"</tr>"  
					 );
		});
		verPeligrosAcordeActividades();
		listEstadosValidarColaborador = data.estadosValidar;
		listAreasResponsableColaborador = data.areas;
		if(numAreasSupervisor > 0){
			listAreasResponsableColaborador = data.areasSupervisor;
		}
		if(numAreasJefe > 0){
			listAreasResponsableColaborador = data.areasJefe;
		}
		if(numAreasGerente > 0){
			listAreasResponsableColaborador = data.areasGerente;
		}
		listPuestosResponsableColaborador = data.puestos;
		listActividadesResponsableColaborador = data.actividades;
		listDivisionesResponsableColaborador = data.divisones;
		listTrabajadoresResponsableColaborador = data.trabajadores;
		if(listAreasResponsableColaborador.length == 0){
			$("#subVerMatrizIperc").remove();
			$("#subConsultarMatrizIperc").remove();
		}else{
			
			
		}
	});
}
var tituloAnteriorColaborador="";
function iniciarFiltroIpercResponsable(){
	$(".camposFiltroTabla").show();
	$("#wrapperIpercResponsable").hide();
	
	$("#btnIniciarFiltro").hide();
	$("#btnAplicarFiltro").show();
	
	$("#btnVolverFiltro").show();
	$("#btnVolverDetalle").hide();
	
	$("#btnValidarFiltro").hide();
	$("#btnReiniciarFiltro").hide();
	$("#btnAgregarActividad").hide();
	$("#btnReporteIperc").hide();
	$("#selFiltroIperc").hide();
	$("#labelFiltroIperc").hide();
	
	
	 
}
function volverVistaFiltro(){
	$(".camposFiltroTabla").hide();
	$("#wrapperIpercResponsable").show();
	
	$("#btnIniciarFiltro").show();
	$("#btnAplicarFiltro").hide();
	
	$("#btnVolverFiltro").hide();
	$("#btnVolverDetalle").show();
	
	$("#btnValidarFiltro").show();
	$("#btnReiniciarFiltro").show();
	$("#btnAgregarActividad").show();
	$("#btnReporteIperc").show();
	$("#selFiltroIperc").show();
	$("#labelFiltroIperc").show();
}
function reiniciarFiltroIpercResponsable(){
	$(".camposFiltroTabla").show();
	crearSelectOneMenuObligMultipleCompleto
	 ("selEstadoFiltro", "",listEstadosValidarColaborador, "id","nombreCompleto",
			 "#divSelectEstadoFiltroIpercResponsable","Todos");
	//crearSelectOneMenuObligMultipleCompleto
	// ("selAreaFiltro", "",listAreasResponsableColaborador, "areaId","areaName",
	//		 "#divSelectAreaFiltroIpercResponsable","Todas");
	crearSelectOneMenuObligMultipleCompleto
	 ("selPuestoFiltro", "",listPuestosResponsableColaborador, "positionId","positionName",
			 "#divSelectPuestoFiltroIpercResponsable","Todos");
	crearSelectOneMenuObligMultipleCompleto
	 ("selActividadFiltro", "",listActividadesResponsableColaborador, "activityId","activityName",
			 "#divSelectActividadFiltroIpercResponsable","Todas")
	aplicarFiltroIpercResponsable();
}
function aplicarFiltroIpercResponsable(){
	
	
	verDetalleIpercColaborador();
	volverVistaFiltro();
}
function validarFiltroIpercResponsable(){
	var listEnviar = [];
	var listEvaluado = [];
	detallesFullIpercColaborador.forEach(function(val){
		var puedeElaborar = false;
		var puedeRevisar = false;
		var puedeAprobar = false;
		var isEvaluado = false;
		if(numAreasSupervisor > 0){
			puedeElaborar = true;
			puedeRevisar = false;
			puedeAprobar = false;
			isEvaluado = (val.trabajadorElaborado.trabajadorId != null);
		}
		if(numAreasJefe > 0){
			puedeElaborar = false;
			puedeRevisar = true;
			puedeAprobar = false;
			isEvaluado = (val.trabajadorRevisado.trabajadorId != null);
		}
		if(numAreasGerente > 0){
			puedeElaborar = false;
			puedeRevisar = false;
			puedeAprobar = true;
			isEvaluado = (val.trabajadorAprobado.trabajadorId != null);
		}
		if(!isEvaluado){
			var trabajadorGeneral = {trabajadorId : getSession("trabajadorGosstId")};
			var trabajadorElaborado = null,
			trabajadorRevisado = null,
			trabajadorAprobado = null;
			if(puedeElaborar){ trabajadorElaborado = trabajadorGeneral};
			if(puedeRevisar){ trabajadorRevisado = trabajadorGeneral};
			if(puedeAprobar){ trabajadorAprobado = trabajadorGeneral};
			listEnviar.push(
					{ipercDetailEvalID : val.ipercDetailEvalID,
						trabajadorElaborado : trabajadorElaborado,
						trabajadorRevisado : trabajadorRevisado,
						trabajadorAprobado : trabajadorAprobado,}
					)
		}else{
			listEvaluado.push({ipercDetailEvalID : val.ipercDetailEvalID,})
		}
		
	});
	var textoEvaluado = ""+listEvaluado.length+" evaluacion(es) ya evaluadas."
	if(listEvaluado.length == 0){textoEvaluado = ""};
	var textoEnviar = "¿Está seguro de validar estas "+listEnviar.length+" evaluaciones? "+textoEvaluado;
	if(listEnviar.length == 0){textoEnviar = "No hay evaluaciones por evaluar. "+textoEvaluado;alert(textoEnviar);return;};
	
	var r = confirm(textoEnviar);
	if(r){
		callAjaxPost(URL + '/trabajador/iperc/validar', 
				listEnviar, function(data){
			alert("Datos guardados. "+data.mensaje);
			aplicarFiltroIpercResponsable();
		});
	}
	
}
function verAcordeAreaActividad(){
	var areaId = parseInt( $("#selAreaActividad").val());
	crearSelectOneMenuObligUnitarioCompleto
	 ("selPuestoActividad", "",listPuestosResponsableColaborador.filter(function(val){
		 return val.areaId == areaId;
	 }), "positionId","positionName",
			 "#divPuestoActividad","---");
}
function agregarActividadIpercColaborador(){
	$("#agregarActividadIperc").show();
	$("#wrapperIpercResponsable").hide();
	crearSelectOneMenuObligUnitarioCompleto("selAreaActividad", "verAcordeAreaActividad()", 
			listAreasResponsableColaborador, "areaId",
			"areaName","#divAreaActividad","---");
	verAcordeAreaActividad();
	
	$("#btnAgregarActividad").hide();
	$("#btnVolverDetalle").hide();
	$("#buttonVolverFiltro").hide();
	$("#btnValidarFiltro").hide();
	$("#btnReiniciarFiltro").hide();
	$("#btnIniciarFiltro").hide();
	$("#btnVerEvalValidar").hide();
	$("#btnReporteIperc").hide();
	
	$("#btnCancelarAgregarActiv").show();
	$("#btnGuardarActividad").show();
}
function volverDetalleIpercColaborador(){
	$("#agregarActividadIperc").hide();
	$("#wrapperIpercResponsable").show();
	
	$("#btnAgregarActividad").show();
	$("#btnVolverDetalle").show();$("#btnVolverFiltro").hide();
	$("#btnValidarFiltro").show();
	$("#btnReiniciarFiltro").show();
	$("#btnIniciarFiltro").show();
	$("#btnVerEvalValidar").show();
	$("#btnReporteIperc").show();
	
	$("#btnCancelarAgregarActiv").hide();
	$("#btnGuardarActividad").hide();
}
function guardarActividadIpercColaborador(){
	var areaId = parseInt( $("#selAreaActividad").val());
	var positionId = parseInt( $("#selPuestoActividad").val());
	var actividad = $("#inputActividadColaborador").val();
	
	var dataParam = {
			activityId:0,
			activityName : actividad.toUpperCase(),
			positionActivityId : 0,
			positionId : positionId,
			isDirectedToIperc:(parseInt(ipercEdicionObj.mdfTypeId)==1?1:0)
		};
 
		callAjaxPost(URL + '/actividad/save', dataParam,
				function(data){
			if(data.mensaje.length > 0){
				alert(data.mensaje);
			}else{
				alert("Actividad Guardada correctamente!");
				volverDetalleIpercColaborador();
				listAreasResponsableColaborador.forEach(function(val){
					if(val.areaId == areaId){
						val.selected = 1;
					}
				});
				filtrarMayorArea = true;
				
				verTablaResumenIpercResponsable(isEditarGeneral);
			}
			
			
		});
}
function verPorValidarIpercResponsable(){
	$(".camposFiltroTabla").show();
	listEstadosValidarColaborador.forEach(function(val){
		val.selected = 0;
		if(val.id == 1 || val.id == 2 || val.id == 3){
			val.selected = 1;
		}
	});
	crearSelectOneMenuObligMultipleCompleto
	 ("selEstadoFiltro", "",listEstadosValidarColaborador, "id","nombre",
			 "#divSelectEstadoFiltroIpercResponsable","Todos");
	//crearSelectOneMenuObligMultipleCompleto
	// ("selAreaFiltro", "",listAreasResponsableColaborador.forEach(function(val,index){val.selected = 0;if(index == 0){val.selected = 1;}  }), "areaId","areaName",
	 //		 "#divSelectAreaFiltroIpercResponsable","Todas");
	crearSelectOneMenuObligMultipleCompleto
	 ("selPuestoFiltro", "",listPuestosResponsableColaborador.forEach(function(val){val.selected = 0}), "positionId","positionName",
			 "#divSelectPuestoFiltroIpercResponsable","Todos");
	crearSelectOneMenuObligMultipleCompleto
	 ("selActividadFiltro", "",listActividadesResponsableColaborador.forEach(function(val){val.selected = 0}), "activityId","activityName",
			 "#divSelectActividadFiltroIpercResponsable","Todas")
	$(".camposFiltroTabla").hide();
	aplicarFiltroIpercResponsable();
}
var filtrarMayorArea = true;
var isEditarGeneral = 0;
function verTablaResumenIpercResponsable(isEditar){
	isEditarGeneral =isEditar;
	if(listIpercColaborador.length == 0){
		alert("No tiene IPERCs asignados/pendientes");
		return;
	}
	isEditar = (parseInt(isEditar)==1);
	$("#ctrlImplModal").remove();
	$("#divIpercResponsableTabla").remove();
	$("#wrapperIpercResponsable").remove();
	$(".divContainerGeneral").hide();  
	var buttonVolver="<button type='button' id='btnVolverDetalle' class='btn btn-success ' style='margin-right:10px'" +
			" onclick='volverVistaIperc()' title='Volver'>" +
						"<i class='fa fa-sign-out fa-rotate-180'></i></button>";
	var buttonVolverFiltro="<button style='display:none' type='button' id='btnVolverFiltro' class='btn btn-info ' style='margin-right:10px'" +
	" onclick='volverVistaFiltro()' title='Volver'>" +
				"<i class='fa fa-sign-out fa-rotate-180'></i></button>";
	
	var buttonFiltro="<button type='button' class='btn btn-info ' id='btnIniciarFiltro' style='margin-right:10px'" +
	" onclick='iniciarFiltroIpercResponsable()' title='Ver filtros'>" +
				"<i class='fa fa-filter'></i></button>";
	var buttonFiltroAplicar="<button type='button' class='btn btn-info gosst-aprobado-total' id='btnAplicarFiltro' style='margin-right:10px'" +
	" onclick='aplicarFiltroIpercResponsable()' title='Aplicar filtros'>" +
				"<i class='fa fa-filter'></i></button>";
	var buttonFiltroRefresh="<button type='button' class='btn btn-success ' id='btnReiniciarFiltro' style='margin-right:10px'" +
	" onclick='reiniciarFiltroIpercResponsable()' title='Refresh'>" + 
				"<i class='fa fa-refresh'></i></button>";
	var buttonReporteIperc="<button type='button' class='btn btn-success ' id='btnReporteIperc' style='margin-right:10px'" +
	" onclick='descargarMatrizColaborador()' title='Descargar reporte'> " +
				"<i class='fa fa-download'></i></button>";
	var buttonFiltroValidar="<button type='button' class='btn btn-success ' id='btnValidarFiltro' style='margin-right:10px'" +
	" onclick='validarFiltroIpercResponsable()' title='Validar Evaluaciones'>" +
				"<i class='fa fa-check'></i></button>";
	var buttonFiltroValidarEstado="<button type='button' class='btn btn-success ' id='btnVerEvalValidar' style='margin-right:10px'" +
	" onclick='verPorValidarIpercResponsable()' title='Ver Evaluaciones Por Validar'>" +
				"<i class='fa fa-eye'></i></button>";
	var buttonAgregarActividad="<button type='button' class='btn btn-success ' id='btnAgregarActividad' style='margin-right:10px'" +
	" onclick='agregarActividadIpercColaborador()' title='Agregar Actividad'>" +
				"<i class='fa fa-plus'></i></button>";
	var btnCancelarActividad ="<button class='btn btn-success' onclick='volverDetalleIpercColaborador()' 	" +
	"id='btnCancelarAgregarActiv' style='margin-right:10px; display:none' title='Cancelar'>" +
		"<i class='fa fa-sign-out fa-rotate-180'></i></button>";
	var btnGuardarActividad ="<button class='btn btn-success ' onclick='guardarActividadIpercColaborador()'" +
	"id='btnGuardarActividad' style='margin-right:10px; display:none' title='Guardar'>" +
		"<i class='fa fa-floppy-o'></i></button>";
	var btnImportarActividad ="<button class='btn btn-success '  " +
	"id='btnImportar' style='margin-right:10px; display:none' title='Importar'>" +
		"<i class='fa fa-upload'></i></button>";
	
	var listItemsForm=[
	       			{sugerencia:" ",label:"Área: ",inputForm: "",divContainer:"divAreaActividad"},
	       			{sugerencia:" ",label:"Puesto : ",inputForm: "",divContainer:"divPuestoActividad"},
	       			{sugerencia:" ",label:"Descripción Actividad : ",inputForm: "<input class='form-control' id='inputActividadColaborador'>",divContainer:""}
	       		];
	       	var text="";
	       	listItemsForm.forEach(function(val,index)
	       	{
	       		text+=obtenerSubPanelModulo(val);
	       	});
	       	var contenido=
	       		"<div class='eventoGeneral' >" +
	       			"<div id='tituloEvento'>" + 
	       				text+
	       			"</div>"+
	       			"</div>" +
	       		"</div>";
	var selFiltroIperc = crearSelectOneMenuOblig("selFiltroIperc","verAcordeIpercColaborador();",listIpercColaborador ,"", "ipercId",
			"ipercName")
	 var btnIperc ="<label id='labelFiltroIperc'>IPERC:</label> "+selFiltroIperc;
	var btnNuevo ="<button class='btn btn-success ' " +
			"id='btnNuevo' style='margin-right:10px' title='Nuevo'>" +
				"<i class='fa fa-plus'></i></button>";
	var btnCopiar ="<button onclick='duplicarIperc()' class='btn btn-success ' " +
	"id='btnCopiar' style='margin-right:10px' title='Duplicar'>" +
		"<i class='fa fa-plus'></i> </button>";
	var btnCancelar ="<button class='btn btn-success ' " +
	"id='btnCancelar' style='margin-right:10px' title='Cancelar'>" +
		"<i class='fa fa-sign-out fa-rotate-180'></i></button>";
	var btnGuardar ="<button class='btn btn-success ' " +
	"id='btnGuardar' style='margin-right:10px' title='Guardar'>" +
		"<i class='fa fa-floppy-o'></i></button>";
	var btnEliminar ="<button class='btn btn-danger ' " +
	"id='btnEliminar' style='margin-right:10px' title='Eliminar'>" +
		"<i class='fa fa-times'></i></button>";
	if(!isEditar){
		buttonFiltroValidarEstado ="";
		buttonFiltroValidar = "";
		buttonAgregarActividad = "";
		btnGuardarActividad = "";
		btnEliminar = "";
		btnGuardar = "";
		btnCancelar = "";
		btnCopiar = "";
		btnImportarActividad = "";
	}
	btnImportarActividad = "";
	buttonFiltroValidarEstado = "";
	var textModalControl ="<div class='modal fade bs-example-modal-lg' id='ctrlImplModal' tabindex='-1' role='dialog'"
		+"aria-labelledby='ctrlImplModalLabel' aria-hidden='true' >"
		+"<div class='modal-dialog modal-lg' style='width:80%'>"
		+"<div class='modal-content' >"
		+"			<div class='modal-header'>"
		+"<button type='button' class='close' data-dismiss='modal'"
		+"aria-label='Close'>"
		+"<span aria-hidden='true'>&times;</span>"
		+"</button>"
		+"<h4 class='modal-title' id='ctrlImplModalLabel'>Control</h4>"
		+"			</div>"
		+"			<div class='modal-body' id='modalControl'>"
		
		
		+"<div id='controlOpciones'>"
		+"			    	<button id='btnCancelarControl' type='button' class='btn btn-success'"
		+"					title='Cancelar'>"
		+"					<i class='fa fa-sign-out fa-rotate-180 fa-2x'></i>"
		+"					</button>"
		+"					<button id='btnAgregarControl' type='button' class='btn btn-success'"
		+"						title='Agregar Nueva'>"
		+"						<i class='fa fa-file-o fa-2x'></i>"
		+"					</button>"
		+"					<button id='btnGuardarControl' type='button' class='btn btn-success'"
		+"						title='Guardar'>"
		+"						<i class='fa fa-floppy-o fa-2x'></i>"
		+"					</button>"
		+"					<button id='btnEliminarControl' type='button' class='btn btn-danger'"
		+"						title='Eliminar Control'>"
		+"						<i class='fa fa-times fa-2x'></i>"
		+"					</button>"
			         
		+"		         </div>"
		+"	<div class='table-responsive'>"
		+"		<div class='table-responsive'>"

		+"			<table class='table table-striped table-bordered table-hover'" 
		+"			 id='tblControl' style='min-width:1900px' >"

		+"			<thead>"
		+"				<tr>"
		+"			<th class='tb-acc' rowspan='2' style='width:220px'>Descripción</th>"
		+"			<th class='tb-acc' rowspan='2' style='width:130px'># Asociados</th>"
					
		+"				<th class='tb-acc' colspan='3' style='min-width:500px'>Responsable de la ejecución del control</th>"
		+"			<th class='tb-acc' rowspan='2' style='width:180px'>Fecha Planificada</th>"
		+"		<th class='tb-acc' rowspan='2' style='width:180px'>Hora Planificada</th>"
		+"			<th class='tb-acc' rowspan='2' style='width:180px'>Fecha Realizada</th>"
		+"			<th class='tb-acc' rowspan='2' style='width:100px'>Costo (S/)</th>"
		+"					<th class='tb-acc' rowspan='2' style='width:180px'>Evidencia</th>"
		+"			<th class='tb-acc' rowspan='2' style='width:140px'>Estado de realizaci&oacute;n</th>"
		+"			</tr>"
					
		+"				<tr>"
						
		+"			<th class='tb-acc' style='width:130px'>Tipo</th>"
		+"			<th class='tb-acc' style='width:150px'>Nombre</th>"						
		+"<th class='tb-acc' >Correo</th>"
		+"			</tr>"
						
		+"			</thead>"
		+"			<tbody >"
					
		+"			</tbody>"
						
		+"			</table>"
					
		+"		</div>"
		+"	</div>"
		+"	</div>"
		+"</div>"
		+"</div>"
		+"</div>";
	$("body")
	.append(textModalControl+
			"<div id='divIpercResponsableTabla' style='padding-left: 40px;'>" +
			"<form class='form-inline' id='formBtnGeneral'>"+
			buttonVolver+buttonVolverFiltro+btnIperc+buttonFiltroValidarEstado+buttonFiltroValidar+
			buttonFiltroRefresh+buttonFiltroAplicar+buttonFiltro+
			buttonAgregarActividad+btnCancelarActividad+
			btnGuardarActividad+buttonReporteIperc+btnImportarActividad+"</form>"+
			
			"<div class='camposFiltroTabla'>"+
			
			"<div class='contain-filtro' id='filtroEstadoIpercResponsable'>" +
			
			"<label for='filtroEstadoIpercResponsable'  >Estados de los registros "+
			"</label>" +
			"<div id='divSelectEstadoFiltroIpercResponsable'> " +
			"</div>" + 
		"</div>"+
			
				"<div class='contain-filtro' id='filtroAreaIpercResponsable'>" +
					
					"<label for='filtroAreaIpercResponsable'  >Áreas "+
					"</label>" +
					"<div id='divSelectAreaFiltroIpercResponsable'> " +
					"</div>" + 
				"</div>"+	
				
				"<div class='contain-filtro' id='filtroPuestoIpercResponsable'>" +
				
				"<label for='filtroPuestoIpercResponsable'  >Puestos "+
				"</label>" +
				"<div id='divSelectPuestoFiltroIpercResponsable'> " +
				"</div>" + 
			"</div>"+	
			
			"<div class='contain-filtro' id='filtroActividadIpercResponsable'>" +
			
			"<label for='filtroActividadIpercResponsable'  >Actividades "+
			"</label>" +
			"<div id='divSelectActividadFiltroIpercResponsable'> " +
			"</div>" + 
		"</div>"+	
				" </div>" +
				"<small >Para visualizar evaluaciones de otras áreas, modificar el filtro</small>"+
				
				
				"<br>"+btnNuevo+btnCancelar+btnCopiar+btnGuardar+btnEliminar+
				
				"<div id='agregarActividadIperc' style='width : 600px; display : none'>" +
				contenido+"" +
				"</div>" +
				 
				"<div class='wrapper' id='wrapperIpercResponsable'>" +
			"<table  style='min-width: 100% ' " +
			"id='tblMatrices' class='table table-striped table-bordered table-hover table-fixed'>" +
			"<thead>" +
				 
			"</thead>" +
			"<tbody></tbody>" +
			"</table>" +
			"</div>" +
			" </div>");
	$("#formBtnGeneral").on("submit",function(e){
		e.preventDefault();
	});
	
	if(!isEditar){
		if(filtrarMayorArea){
			filtrarMayorArea = false;
			listAreasResponsableColaborador[0].selected = 1;
		}
		
		crearSelectOneMenuObligMultipleCompleto
		 ("selAreaFiltro", "verPuestosAcordeAreaFiltro()",listAreasResponsableColaborador, "areaId","areaName",
				 "#divSelectAreaFiltroIpercResponsable","Todas");
	}else{
		var listaAux = listAreasResponsableColaborador.filter(function(val,index){
			 var isIgual = false;
			  if(index == 0){val.selected = 1}else{val.selected = 0};
			  if(numAreasSupervisor > 0){
				  if(val.listTrabajadoresValidar == null){val.listTrabajadoresValidar = [] };
				  val.listTrabajadoresValidar.forEach(function(val1){
					  
						if(val1.trabajadorId == parseInt(getSession("trabajadorGosstId")) ){
							isIgual = true;
						}
					 });
				  return (true);
			  }
			  isIgual = true;
			 return isIgual;
		 });
		
		//listaAux = [{areaId : 0,areaName: "Ninguna asignada",selected : 1}];
		
		crearSelectOneMenuObligMultipleCompleto
		 ("selAreaFiltro", "comprobarNumeroArea();verPuestosAcordeAreaFiltro()",listaAux, "areaId","areaName",
				 "#divSelectAreaFiltroIpercResponsable","Todas");
	}
	verPuestosAcordeAreaFiltro();

	verAcordeIpercColaborador();
	
	
	 
	$(".camposFiltroTabla").hide();
	
	$("#btnAplicarFiltro").hide(); 
	  $("#divTrabIpercResponsable form").on("submit",function(e){
			e.preventDefault(); 
		}); 
	tituloAnteriorColaborador=$(".divTituloGeneral h4").text();
	$(".divTituloGeneral h4").html("Resumen de IPERC");
	resizeDivWrapper("wrapperIpercResponsable",200);
	verDetalleIpercColaborador(isEditar);
}
function verPuestosAcordeAreaFiltro(){
	
	crearSelectOneMenuObligMultipleCompleto
	 ("selPuestoFiltro", "verActividadesAcordePuestoFiltro()",listPuestosResponsableColaborador.filter(function(val){
		 var isArea = false;
		 val.selected = 0;
		 var areasId = $("#selAreaFiltro").val();
		 if(areasId !=null){
			 
		 areasId.forEach(function(val1){
			if(parseInt(val1) ==  val.areaId ){
				isArea = true;
			}
		 });
		 return isArea;
		 }else{
			 return true;
		 }
	 }), "positionId","positionName",
			 "#divSelectPuestoFiltroIpercResponsable","Todos");
	verActividadesAcordePuestoFiltro();
}
function verActividadesAcordePuestoFiltro(){
	crearSelectOneMenuObligMultipleCompleto
	 ("selActividadFiltro", "",listActividadesResponsableColaborador.filter(function(val){
		 var isArea = false;
		 var isPuesto = false;
		 
		 val.selected = 0;
		 var areasId = $("#selAreaFiltro").val();
		 var puestosId = $("#selPuestoFiltro").val();
		 if(puestosId !=null){
			 
			 puestosId.forEach(function(val1){
				if(parseInt(val1) ==  val.positionId ){
					isPuesto = true;
				}
			 });
			 return isPuesto;
		 }else if(areasId !=null){
			 
		 areasId.forEach(function(val1){
			if(parseInt(val1) ==  val.areaId ){
				isArea = true;
			}
		 });
		 return isArea;
		 }else{
			 return true;
		 }
		 
		 return isPuesto
		 
	 }), "activityId","activityName",
			 "#divSelectActividadFiltroIpercResponsable","Todas")
}
function comprobarNumeroArea(){
	var selAreaFiltro = $("#selAreaFiltro").val();
	if(selAreaFiltro == null){
		alert("Filtro de área obligatorio");
		crearSelectOneMenuObligMultipleCompleto
		 ("selAreaFiltro", "comprobarNumeroArea()",listAreasResponsableColaborador.filter(function(val,index){
			 val.selected = 0;
			 if(index == 0){
				 val.selected = 1;
			 }
			 var isIgual =true; 
			 return isIgual;
		 }), "areaId","areaName",
				 "#divSelectAreaFiltroIpercResponsable","Todas");
	}
}
var detallesFullIpercColaborador = [];
var ipercEdicionObj;
var listaPersonaExpuesta=[];
var listaIndCapa=[];
var listaIndRisk=[];
var listaIndProc=[];
function verAcordeIpercColaborador(){
	ipercEdicionObj = {};
	listIpercColaborador.forEach(function(val){
		if(val.ipercId == parseInt( $("#selFiltroIperc").val() ) ){
			ipercEdicionObj = val;
		}
	});
	listEstadosValidarColaborador.forEach(function(val){
		switch(val.id){
		case 1:
			val.nombreCompleto = val.nombre+" ("+ipercEdicionObj.numEvaluacionesPendientes+") "+ipercEdicionObj.nombreSupervisores;
			break;
		case 2:
			val.nombreCompleto = val.nombre+" ("+ipercEdicionObj.numEvaluacionesElaboradas+") "+ipercEdicionObj.nombreSupervisores;
			break;
		case 3:
			val.nombreCompleto = val.nombre+" ("+ipercEdicionObj.numEvaluacionesRevisadas+") "+ipercEdicionObj.nombreJefes;
			break;
		case 4:
			val.nombreCompleto = val.nombre+" ("+ipercEdicionObj.numEvaluacionesAprobadas+") "+ipercEdicionObj.nombreGerentes;
			break;
		case 5:
			val.nombreCompleto = val.nombre+" ("+ipercEdicionObj.numEvaluacionesVencidas+") "+ipercEdicionObj.nombreSupervisores;
			break;
		}
	})
	$(".camposFiltroTabla").show();
	crearSelectOneMenuObligMultipleCompleto
	 ("selEstadoFiltro", "",listEstadosValidarColaborador, "id","nombreCompleto",
			 "#divSelectEstadoFiltroIpercResponsable","Todos");

	$(".camposFiltroTabla").hide();
	verDetalleIpercColaborador();
}
function verDetalleIpercColaborador(){
	var isEditar = isEditarGeneral;
	
	var areasId = $("#selAreaFiltro").val();
	var puestosId = $("#selPuestoFiltro").val();
	var actividadesId = $("#selActividadFiltro").val();
	var estadosId = $("#selEstadoFiltro").val();
	var listAreasId =[], listPuestosId = [], 
	listActividadesId = [], listEstadosId = [];
	
	if(areasId  == null || areasId.length==0){
		listAreasId = null;
	}else{
		areasId.forEach(function(val){
			listAreasId.push(parseInt(val));
		})
	}
	if(puestosId  == null || puestosId.length==0){
		listPuestosId = null;
	}else{
		puestosId.forEach(function(val){
			listPuestosId.push(parseInt(val));
		})
	}
	if(actividadesId  == null || actividadesId.length==0){
		listActividadesId = null;
	}else{
		actividadesId.forEach(function(val){
			listActividadesId.push(parseInt(val));
		})
	}
	if(estadosId  == null || estadosId.length==0){
		listEstadosId = null;
	}else{
		estadosId.forEach(function(val){
			listEstadosId.push(parseInt(val));
		})
	}
	var dataParam = {
			ipercId : ipercEdicionObj.ipercId,
		matrizRiesgoId : ipercEdicionObj.matrizRiesgoId,
		listAreasId : listAreasId,
		listPuestosId : listPuestosId,
		listActividadesId : listActividadesId,
		listEstadosId : listEstadosId
		
		
	};
	callAjaxPost(URL + '/iperc/detalle', dataParam,
			function(data){
		detallesFullIpercColaborador=data.list;
		procesarListarTablaDetalleIperc(data);
		if(!isEditar){
			$("#tblMatrices tbody tr").attr("onclick","");
			formatoCeldaSombreableTabla(false,"tblMatrices");
			detallesFullIpercColaborador.forEach(function(val,index){
				var text1 = $("#ctrlImpl1"+val.ipercDetailEvalID).html();
				$("#ctrlImpl1"+val.ipercDetailEvalID)
				.html("<a onclick='asignarObjIndexDetalleIperc("+index+") ;" +
						"verControlSegunTipo(1)' class='efectoLink'>" +
						text1+"</a>");
				var text2 = $("#ctrlImpl2"+val.ipercDetailEvalID).html();
				$("#ctrlImpl2"+val.ipercDetailEvalID)
				.html("<a onclick='asignarObjIndexDetalleIperc("+index+") ;" +
						"verControlSegunTipo(2)' class='efectoLink'>" +
						text2+"</a>");
				var text3 = $("#ctrlImpl3"+val.ipercDetailEvalID).html();
				$("#ctrlImpl3"+val.ipercDetailEvalID)
				.html("<a onclick='asignarObjIndexDetalleIperc("+index+") ;" +
						"verControlSegunTipo(3)' class='efectoLink'>" +
						text3+"</a>");
				var text4 = $("#ctrlImpl4"+val.ipercDetailEvalID).html();
				$("#ctrlImpl4"+val.ipercDetailEvalID)
				.html("<a onclick='asignarObjIndexDetalleIperc("+index+") ;" +
						"verControlSegunTipo(4)' class='efectoLink'>" +
						text4+"</a>");
				var text5 = $("#ctrlImpl5"+val.ipercDetailEvalID).html();
				$("#ctrlImpl5"+val.ipercDetailEvalID)
				.html("<a onclick='asignarObjIndexDetalleIperc("+index+") ;" +
						"verControlSegunTipo(5)' class='efectoLink'>" +
						text5+"</a>");
			});
		}
		//goheadfixedY('#tblMatrices',"#wrapper");
	});
}
function asignarObjIndexDetalleIperc(pindex){
	detalleIpercDetailObj = detallesFullIpercColaborador[pindex];
}
function descargarMatrizColaborador(){
	window.open(URL
			+ "/iperc/detail/excel?ipercId="
			+ ipercEdicionObj.ipercId + "&ipercName="
			+ ipercEdicionObj.ipercName, '_blank');	
}
var listFullDetalleIpercColaborador = [];
function procesarResumenIpercColaborador(data){
	listFullDetalleIpercColaborador = data.list;
}

function verPeligrosAcordeActividades(selectAll){
	if(parseInt(selectAll)==1){
		var checkTotal=$(".divListPrincipal   #subListActividades  table #inputCheckActividadIperTotal").prop("checked");
		
		listFullActividades.forEach(function(val,index){
			var checkIper=$(".divListPrincipal   #subListActividades  table #inputCheckActividadIper"+index);
			 
				checkIper.prop("checked",checkTotal);
		 
		}); 
	}
	var actividadesSeleccionadas=[];
	listActividadesMarcadas=[];
	listFullActividades.forEach(function(val,index){
		var isCheck=$(".divListPrincipal   #subListActividades  table #inputCheckActividadIper"+index)
		.prop("checked");
		if(isCheck){
			actividadesSeleccionadas.push(val.nombre)
		}
		listActividadesMarcadas.push(isCheck)
		
	});
	$(".divListPrincipal").find("#subListPeligros .contenidoSubList").html("");
	var contadorPeligrosAsociados=0;
	listFullPeligros.forEach(function(val,index){
		if(actividadesSeleccionadas.indexOf(val.actividad)!=-1){
			contadorPeligrosAsociados++;
			var listIper=[
{sugerencia:"",label:"<i aria-hidden='true' class='fa fa-location-arrow'></i> Actividad / Lugar:",inputForm:val.actividad+"/"+val.lugar,divContainer:""},
{sugerencia:"",label:"<i aria-hidden='true' class='fa fa-exclamation-triangle'></i> Peligro / Riesgo:",inputForm:val.peligro+"/"+val.riesgo +"("+(val.nivel==null?"Sin evaluar":val.nivel)+")",divContainer:""},
{sugerencia:"",label:"<i aria-hidden='true' class='fa fa-info'></i> Control(es):",inputForm:val.controles,divContainer:""}
			              ];
			var textIperc="";
			listIper.forEach(function(val,index){
				textIperc+=obtenerSubPanelModulo(val)
			})
			$(".divListPrincipal").find("#subListPeligros .contenidoSubList")
			.append("<div class='eventoGeneral' style='border-left:8px solid "+val.nivelColor+";   '>" +
					textIperc+
				 
					"</div>");
		}
		
	});
	var textoBotonHide1="<button class='btn btn-success'  onclick='cambiarDivSubIperc("+1+")'>" +
	"<i class='fa fa-caret-down fa-2x' aria-hidden='true'></i></button>";
	var textoBotonHide2="<button class='btn btn-success'  onclick='cambiarDivSubIperc("+2+")'>" +
	"<i class='fa fa-caret-down fa-2x' aria-hidden='true'></i></button>";
	$(".divListPrincipal").find("#subListActividades .tituloSubList")
	.html(textoBotonHide1+"<i class='fa fa-street-view'></i> Actividades de puesto '"+puestoTrabajoNombre+"' ("+actividadesSeleccionadas.length+" - "+listFullActividades.length+")")
	$(".divListPrincipal").find("#subListPeligros .tituloSubList")
	.html(textoBotonHide2+"<i class='fa fa-exclamation-triangle'></i> Peligros Asociados ("+contadorPeligrosAsociados+" - "+listFullPeligros.length+")")
}

function volverVistaIperc(){
	$("#divIpercResponsableTabla").remove();
	$("#wrapperIpercResponsable").remove();
	$("#ctrlImplModal").remove();
	
	$(".divContainerGeneral").show(); 
	$(".divListPrincipal > div").show();
}
function habilitarPerfilUsuario(){
	$(".divListPrincipal")
	.html( 
			"<div id='subListPassword'  >" +
			"<div class='tituloSubList'></div>" +
			"<div class='contenidoSubList'></div>" +
			"</div>" +
			"<div id='subListInfoExtra'  >" +
			"<div class='tituloSubList'></div>" +
			"<div class='contenidoSubList'></div>" +
			"</div>"+
			"<div id='subListCerrarSession'  >" +
			"<div class='tituloSubList'></div>" +
			"<div class='contenidoSubList'></div>" +
			"</div>" 
	);
	
	var textoForm="";
	var listItemsForm=[
	{sugerencia:"",label:"Anterior Contraseña",inputForm:"<input type='password' class='form-control' id='inputOldPass' placeholder=' ' autofocus>"},
	{sugerencia:" ",label:"Nueva Contraseña",inputForm:"<input type='password' class='form-control' id='inpuNewPass' placeholder=' '>"}, 
	{sugerencia:" ",label:"Repetir Contraseña",inputForm:"<input type='password' class='form-control' id='inpuNewNewPass' placeholder=' '>"} 
	];
	listItemsForm.forEach(function(val,index){
		textoForm+="<div class='form-group row'>"+
		   " <label for='colFormLabel' class='col-sm-4 col-form-label'>"+val.label+"</label>"+
		    "<div class='col-sm-8' id='"+val.divContainer+"'>"+
		     " "+val.inputForm+
		     "<small>"+val.sugerencia+" </small>"+
		    "</div>"+
		  "</div>";
	});
	
	///
	callAjaxPost(URL + '/login/licencia', {},
			function(data){
		var licencia = data.licencia;
		var butonRecibir = "<button class='btn btn-success' onclick='actualizarWantsCorreo(1)'><i class='fa fa-envelope-o'></i>Recibir Notificaciones</button>;";
		var butonNoRecibir = "<button class='btn btn-success' onclick='actualizarWantsCorreo(0)'><i class='fa fa-envelope-o'></i>No Recibir Notificaciones</button>;";
		var textoRecibir = (licencia.wantsCorreo==1?iconoGosst.aprobado+"Activo":iconoGosst.desaprobado+"Inactivo")
		var textoBtnRecibir = (licencia.wantsCorreo==1?butonNoRecibir:butonRecibir);
		var infoArray=[
		               {campo:"Usuario",descripcion:getSession("gestopusername")},
		               {campo:"Correo de notificaciones<br>"+textoRecibir,
		            	   descripcion:""+licencia.correo+"<br>"+textoBtnRecibir},
		               {campo:"Puesto de Trabajo",descripcion:puestoTrabajoNombre},
		               {campo:"Área",descripcion:areaNombreAux},
		               {campo:"Fecha de vigencia",descripcion:licencia.fechaVencimiento},
		               {campo:"Empresa",descripcion:licencia.empresaNombre},
		               {campo:"Logo",descripcion:insertarImagenParaTablaMovil(licencia.logo,"","100%")},
		               {campo:"Imagen perfil",descripcion:
		            	   (licencia.imagenTrabajador==null?
		            			   "<i class='fa  fa-user-circle fa-4x' aria-hidden='true'></i>" +
		            			   "Nota: tamaño recomendado 150x150 ."
				            	   +"<div id='imagenTrabajador'></div>":
		            	   insertarImagenParaTablaMovil(licencia.imagenTrabajador,"","100%")
		            	   +"<div id='imagenTrabajador'></div>") }
		               
		               ];
		var informacionTexto="";
		infoArray.forEach(function(val,index){
			informacionTexto+="<tr>" +
			"<td>"+val.campo+"</td>" +
			"<td>"+val.descripcion+"</td>" +
			"</tr>" ;
		});
		$("#subListInfoExtra").find(".tituloSubList")
		.html("Información General"+textoBotonToggleContenidoColaborador);
		$("#subListInfoExtra").find(".contenidoSubList")
		.html("<div class='eventoGeneral'><table class='table table-user-information'>" +
				"<tbody>" +
				informacionTexto+
				"</tbody>" +
				"</table>" +
				"</div>");
		
		if(licencia.imagenTrabajador!=null){
			$("#icoPerfil > a").html(
					insertarImagenParaTablaMovil(licencia.imagenTrabajador,"32px","32px")+
					getSession("gestopusername").substr(0,12)+".");
		}
		
		var options=
		{container:"#imagenTrabajador",
				functionCall:function(){
					guardarEvidenciaAuto(getSession("trabajadorGosstId"),"fileEviTrabajador"
							,bitsLogoEmpresa,"/scheduler/trabajador/imagen/save",
							habilitarPerfilUsuario ) },
				descargaUrl:"/scheduler/trabajador/imagen?id="+getSession("trabajadorGosstId"),
				esNuevo:false,
				idAux:"Trabajador",
				evidenciaNombre:"Modificar Imagen"};
		crearFormEvidenciaCompleta(options);
	});
	
	
	
	$("#subListCerrarSession").find(".tituloSubList")
	.html("Cerrar Sessión"+textoBotonToggleContenidoColaborador);
	$("#subListCerrarSession").find("button")
	.html("<i class='fa fa-times fa-2x' aria-hidden='true'></i>")
	.on("click",function(){
		cerrarSesion();
	});
	
	///
		$(".divListPrincipal").find("#subListPassword .tituloSubList")
		.html("Cambiar Contraseña"+textoBotonToggleContenidoColaborador);
		$("#subListPassword").find(".contenidoSubList").html(
				"<form class='eventoGeneral' id='nuevoPassTrabajador'>"+
				textoForm+
				 "<button type='submit' class='btn btn-success' ><i aria-hidden='true' class='fa fa-paper-plane'></i>Cambiar</button>"+
				"</form>"
		);
		$('#nuevoPassTrabajador').on('submit', function(e) {
	        e.preventDefault();
	    	var psw1 = $("#inputOldPass").val();
	    	var psw2 = $("#inpuNewPass").val();
	    	var psw3 = $("#inpuNewNewPass").val();
	    	var band = true;
	    	
	    	if(psw3.length == 0 || psw2.length == 0 || psw1.length == 0){
	    		band = false;
	    	}
	    	
	    	if(band){
	    		var dataParam = {
	    				trabajadorId : getSession("trabajadorGosstId"),
	    				userName : sessionStorage.getItem("gestopusername"),
	    				sessionId : sessionStorage.getItem("gestopsessionId"),
	    				accesoUsuarios:getSession("accesoUsuarios"),
	    				psw1 : psw1.trim(),
	    				psw2 : psw2.trim(),
	    				psw3 : psw3.trim()
	    			};

	    			callAjaxPost(URL + '/login/password/save', dataParam,
	    					function(data){
	    				$("#inputOldPass").val("");
	    				$("#inpuNewPass").val("");
	    				$("#inpuNewNewPass").val("");
	    				$("#subListPassword").find(".contenidoSubList").hide();
	    				switch (data.CODE_RESPONSE) {
	    				case "02":
	    					alert("El password actual es incorrecto.!");
	    					break;
	    				case "03":
	    					alert("El password se ha bloqueado por demasiados intentos fallidos.!");
	    					break;
	    				case "05":
	    					alert("El password se guardo correctamente.!");
	    					break;
	    				case "07":
	    					alert("La confirmacion del nuevo password es incorrecta.!");
	    					break;
	    				case "11":
	    					alert("La contraseña no es correcta. Ingrese nuevamente");
	    					break;
	    					
	    				default:
	    					
	    					alert("La contraseña ya es usada por otro trabajador, por favor ingresa una diferente");
	    				}
	    			});		
	    	} else {
	    		alert("Debe ingresar todos los campos.");
	    	}
		});
	 
}
function actualizarWantsCorreo(wants){
	var dataParam = {
			trabajadorId : getSession("trabajadorGosstId"),
			wantsCorreo : wants
	}
	callAjaxPost(URL+"/trabajador/correo/save",dataParam,function(daat){
		habilitarPerfilUsuario();
	});
}
function cambiarDivSubIperc(estadoId){
	var divContainer="subListPeligros";
	switch(estadoId){
	case 1: 
		divContainer="subListActividades";
		break;
	case 2: 
		divContainer="subListPeligros";
		break;
	}
	$("#"+divContainer).find(".contenidoSubList").toggle();
}
function habilitarVistaUsuario(){
	$(".divListPrincipal")
	.append("Working on it !")
}
//-----------------------------------------
var lastHeight=0;
function toggleMenuOpcionActiv(obj,pindex){
	indexEventoOpt=pindex;
	lastHeight=$(obj).offset().top;
	$(obj).parent(".eventoGeneral").find("ul").toggle();
	$(obj).parent(".eventoGeneral").siblings().find("ul").hide(); 	
}
var funcionalidadesActiv=[ 
{id:"opcEditar",nombre:"<i class='fa fa-check-square'></i>&nbspRegistrar como terminado",functionClick:function(data){verCompletarActividadVistaColaborador(data)}}
 ];
var indexEventoOpt;
var funcionalidadesEventosOpt=[
	{id:"opcEval",nombre:"<i class='fa fa-check-square-o'></i>&nbspRegistrar evaluación",functionClick:	function(){realizarPreguntasAperturaOptMovil(indexEventoOpt)}},
	{id:"opcEditar",nombre:"<i class='fa fa-check-square'></i>&nbspRegistrar como terminado",
		functionClick:function(data){verCompletarActividadVistaColaborador(data)}}
	];
function marcarSubOpcionActiv(pindex,indexEvento){
	funcionalidadesActiv[pindex].functionClick(indexEvento); 
	//$(".listaGestionGosst").hide();  
}
function marcarSubOpcionEventoOpt(pindex,indexEvento){
	funcionalidadesEventosOpt[pindex].functionClick(indexEvento); 
	//$(".listaGestionGosst").hide();  
}
//---------------------------------------------------------
function habilitarVistaTrabajador(){
var contadorImplementar=0,
contadorCompletado=0,
contadorRetrasado=0;
$(".divListPrincipal").html("")
$(".divListPrincipal")
.append(
		"<div id='subListCompletado'  >" +
		"<div class='tituloSubList'></div>" +
		"<div class='contenidoSubList' style='display:none'></div>" +
		"</div>"+
		
		"<div id='subListImplementar' >" +
			"<div class='tituloSubList' stlye='color:red'></div>" +
			"<div class='contenidoSubList'></div>" +
		"</div>"+
		"<div id='subListRetrasado'  >" +
			"<div class='tituloSubList'></div>" +
			"<div class='contenidoSubList'></div>" +
		"</div>"+
		"<div id='subFormEvento' class='divSubEvento'>" +
		"</div>"+
		"<div id='subRealizarInspecciones' class='divSubEvento' style='display : none'>" +
		"<div class='tituloSubList'></div>" +
		"<div class='contenidoSubList'></div>" +
		"</div>"
);
	listFullEventos.forEach(function(val,index){
		var divContainer;
		switch(val.estadoEvento){
		case 1:
			contadorImplementar++;
			divContainer="subListImplementar";
			break;
		case 2:
			contadorCompletado++;
			divContainer="subListCompletado";
			break;
		case 3:
			contadorRetrasado++;
			divContainer="subListRetrasado";
			break;
		}
		agregarActividadColaborador(val,index);
		
	});
	$(".listaGestionGosst").hide();
	var textoBotonHide1="<button class='btn btn-success'  onclick='cambiarDivEstadoEvento("+1+")'>" +
	"<i class='fa fa-caret-down fa-2x' aria-hidden='true'></i></button>";
	var textoBotonHide2="<button class='btn btn-success'  onclick='cambiarDivEstadoEvento("+2+")'>" +
	"<i class='fa fa-caret-down fa-2x' aria-hidden='true'></i></button>";
	var textoBotonHide3="<button class='btn btn-success'  onclick='cambiarDivEstadoEvento("+3+")'>" +
	"<i class='fa fa-caret-down fa-2x' aria-hidden='true'></i></button>";
	$(".divListPrincipal").find("#subListImplementar .tituloSubList").css({"color":"8px solid #F79646"}).html("Por Implementar ("+contadorImplementar+")" +textoBotonHide1)
	$(".divListPrincipal").find("#subListCompletado .tituloSubList").css({"color":"8px solid #9BBB59"}).html("Completado ("+contadorCompletado+")"+textoBotonHide2 )
	$(".divListPrincipal").find("#subListRetrasado .tituloSubList").css({"color":"8px solid #C0504D"}).html("Retrasado ("+contadorRetrasado+")"+textoBotonHide3 )
	
	 $("#liMenuTrabajador"+2+" .alertaMenu").html(contadorImplementar+contadorRetrasado);
	completarBarraCarga();
}
function agregarActividadColaborador(val,index){
	var respAux=val.responsable; 
	var linkEvi="";
	if(val.tipoEvento==16)
	{
		linkEvi="href='"+URL+"/opt/eventos/evidencia?id="+val.idAux+"'";
	}
	var iconoEvi="<a  id='evidenciaEvento"+val.id+"' class='efectoLink' target='_blank' "+linkEvi+">"+val.evidenciaNombre+"</a>"; 
	if(val.evidenciaNombre=="----"){
		iconoEvi="<strong style='color:red'>No hay evidencia</strong>"
	}
	
var textoBoton="<button class='btn btn-success'  onclick='verCompletarActividadVistaColaborador("+index+")'>" +
	"<i class='fa fa-external-link-square' aria-hidden='true'></i>Editar</button>";
	
	var divContainer;
			switch(val.estadoEvento){
			case 1:
				divContainer="subListImplementar";
				break;
			case 2:
				divContainer="subListCompletado";
				textoBoton="";
				break;
			case 3:
				divContainer="subListRetrasado";
				break;
			}
	var textAccionDebido="",texRealizarInspeccion="";
	
	
	//-----------------------------------------------------------------------------------------------
	var menuOpcion="<ul class='list-group listaGestionGosst' >";
	funcionalidadesActiv.forEach(function(val1,index1){
		menuOpcion+="<li class='list-group-item' onclick='marcarSubOpcionActiv("+index1+","+index+")'>"+val1.nombre+" </li>";
		if(val.tipoEvento==10){
			if(val.linkAuxiliarId3==null){
			texRealizarInspeccion="<li class='list-group-item' onclick='realizarInspeccionMovil("+index+")'><i class='fa fa-check-square-o'  ></i>Realizar evaluación </li>";
			}else{
			texRealizarInspeccion="<li class='list-group-item' onclick='marcarIconoModuloSeleccionado("+11+","+val.linkAuxiliarId3+","+val.linkAuxiliarId1+","+val.idAux+")'><i class='fa fa-check-square-o'  ></i>Realizar evaluación Proyecto</li>";
			}
		}
		menuOpcion+=texRealizarInspeccion;
		if(val.tipoEvento==8){
			menuOpcion+="<li class='list-group-item' onclick='verOrigenAccionMejora("+index+")'  '>" +
				"<i class='fa fa-info-circle'  ></i>Ver origen de acción</li>  "
		}
		
	});
	menuOpcion+="</ul>";
	var menuOpcioOpt="<ul class='list-group listaGestionGosst' >";
	funcionalidadesEventosOpt.forEach(function(val1,index1){
		menuOpcioOpt+="<li class='list-group-item' onclick='marcarSubOpcionEventoOpt("+index1+","+index+")'>"+val1.nombre+" </li>";
	});
	menuOpcioOpt+="</ul>";
	//-----------------------------------------------------------------------------------------------
	textoBoton="";
	texRealizarInspeccion="";
	if($("#divActColaborador"+val.id).length>0){
		$("#divActColaborador"+val.id)
		.html("" +
				"<a class='efectoLink btn-gestion' style='margin-left: 130px;' onclick='toggleMenuOpcionActiv(this,"+index+")'>" 
				+"Ver Opciones <i class='fa fa-angle-double-down' aria-hidden='true'></i></a>"+
				(val.tipoEvento==16?menuOpcioOpt:menuOpcion)+
				"<div id='tituloEvento' >" +
				val.iconoNombre+
				val.text+
				"<br><i class='fa fa-calendar'></i>" +
				"<strong>(" +
				val.estadoEventoNombre+
				")  </strong>"+
				val.fechaTexto+
				"<br><i class='fa fa-download'></i>"+
					 
					iconoEvi+
					textAccionDebido+
				 
				
				"</div>").css({"border-color":""+val.color});
		
	}else{
		$(".divListPrincipal").find("#"+divContainer+" .contenidoSubList")
		.append("<div id='divActColaborador"+val.id+"' class='eventoGeneral' style='border-left:8px solid "+val.color+";   '>" +
				"<a class='efectoLink btn-gestion' style='margin-left: 130px;' onclick='toggleMenuOpcionActiv(this,"+index+")'>" 
				+"Ver Opciones <i class='fa fa-angle-double-down' aria-hidden='true'></i></a>"+
				(val.tipoEvento==16?menuOpcioOpt:menuOpcion)+
				"<div id='tituloEvento' >" +
				val.iconoNombre+
				val.text+
				"<br><i class='fa fa-calendar'></i>" +
				"<strong>(" +
				val.estadoEventoNombre+
				")  </strong>"+
				val.fechaTexto+
				"<br><i class='fa fa-download'></i>"+
					
					iconoEvi+
					textAccionDebido+ 
				"</div>" +
				"</div>");
	}
	
	asignarFuncionesEvento(val);
}

function volverMenuActividadesSST(){
	$(".divTituloFull").find("h4").html("Mis Actividades SST")
	$(".divListPrincipal #subListCompletado").show();
	$(".divListPrincipal #subListImplementar").show();
	$(".divListPrincipal #subListRetrasado").show();
	
	$("#subFormEvento").html("");
	$(window).scrollTop(lastHeight-$("#barraMenu").height()-60);
	
}

function cambiarDivEstadoEvento(estadoId){
	var divContainer;
	switch(estadoId){
	case 1: 
		divContainer="subListImplementar";
		break;
	case 2: 
		divContainer="subListCompletado";
		break;
	case 3: ;
		divContainer="subListRetrasado";
		break;
	}
	$("#"+divContainer).find(".contenidoSubList").toggle();
}
function verCompletarActividadVistaColaborador(pindex){
	var listCondicional=[
		{id:1,nombre:"Si"},
		{id:0,nombre:"No"}
		];
	$(".divListPrincipal > div").hide();
	$("#subFormEvento").show();
	var objActividad=listFullEventos[pindex];
	var textoBotonVolver="<button class='btn btn-success' onclick='volverMenuActividadesSST()' style='color:#008b8b ;background-color:white '>" +
	"<i class='fa fa-arrow-left' aria-hidden='true'></i></button>";
	var textoBotonGuardar="<button class='btn btn-success' onclick='guardarActividadTrabajador("+pindex+")'>" +
	"<i class='fa fa-save' aria-hidden='true'></i>Guardar</button>";
	var formEvi=crearFormEvidencia("Actividad",true);
	var selFelicitacion = crearSelectOneMenuOblig("selFelicitacionOpt","",listCondicional ,"", "id","nombre");
	var selSeguimiento = crearSelectOneMenuOblig("selSeguimientoOpt","",listCondicional ,"", "id","nombre");
	$("#subFormEvento").html(
			"<table  class='table table-striped table-bordered table-hover'>" +
				"<thead >" +
					"<tr>"+ 
						"<th class='tb-acc' style='border-color:#008b8b;'colspan='2'>"+
							"<div class='row'>" +
								"<section class='col col-xs-4'>" +
									textoBotonVolver+
								"</section>" +
								"<section class='col col-xs-8'>" +
									objActividad.text+
								"</section>" +								
							"</div>"+
						"</th>" +
						//"<th class='tb-acc' style='border-color:#008b8b;'>"++"</td>" +
					"</tr>" +
				"</thead>" +
				"<tbody>" +
						(objActividad.tipoEvento==16?
					"<tr>" +
						"<td>¿Ha felicitado adecuadamente al trabajador y/o ha vuelto a instruirlo en base a las observaciones?</td>"+
						"<td>"+selFelicitacion+"</td>"+
					"</tr>"+
					"<tr>" +
						"<td>Vigencia de la Opt</td>"+
						"<td>" +
							"<div class='row'>" +
							"<section class='col col-xs-7'>" +
								"<input type='number' class='form-control' id='inputVigenciaOpt' min='0' value='0'>" +
							"</section>"+
							"<section class='col col-xs-2'>" +
								"<label style='vertical-align: sub;'>Dias</label>" +
							"</section>"+
							"</div>"+
						"</td>"+
					"</tr>"+
					"<tr>" +
						"<td>¿Se debería realizar una observación de seguimiento a este trabajador en un futuro cercano?</td>"+
						"<td>"+selSeguimiento+"</td>"+
					"</tr>"
						:"")+
					"<tr>" +
						"<td>Fecha Realizada *</td>" +
						"<td><input type='date' class='form-control' id='dateActividad'></td>" +
					"</tr>" +
					
					((objActividad.tipoEvento==8 || objActividad.tipoEvento==11|| objActividad.tipoEvento==12)?
						"<tr>"+
						"<td>Observaciones del responsable de la actividad</td>" +
						"<td><input   class='form-control' id='observacionActividad'></td>" +
						"</tr>"
							:"")+
					
					(objActividad.tipoEvento==16?
						"<tr>"+
							"<td colspan='2'><a class='efectoLink' onclick='verSelecionarTrabajadorOpt("+pindex+")'>Enviar Informe OPT</a></td>" +
						"</tr>"
							:"")+
					"<tr>" +
						"<td>Evidencia</td>" +
						"<td>"+formEvi+"</td>" +
					"</tr>" +
					"<tr>" +
						"<td></td>" +
						"<td>"+textoBotonGuardar+"</td>" +
					"</tr>" +
				"</tbody>" +
			"</table>");
$("#dateActividad").val(convertirFechaInput(objActividad.start_date));
$("#observacionActividad").val(objActividad.observacion);
$("#inputEviActividad").val(objActividad.evidenciaNombre);

}
function verOrigenAccionMejora(pindex){
	$(".divListPrincipal > div").hide();
	$("#subFormEvento").show();
	var objActividad=listFullEventos[pindex];
	var textoBotonVolver="<button class='btn btn-success' onclick='volverMenuActividadesSST()'>" +
	"<i class='fa fa-angle-left' aria-hidden='true'></i>Volver</button>";
	var dataParam = {
			gestionAccionMejoraId : objActividad.linkAuxiliarId1,
			accionMejoraId : objActividad.idAux
		};

		callAjaxPost(URL + '/gestionaccionmejora/detalle', dataParam,
				function(data){
			switch (data.CODE_RESPONSE) {
			case "05":
				var list = data.list; 
				var textIn=""; 
				var nro = 0;

				nro++;
				
				list.forEach(function(val,index){
					var btnDescargaCausa="";
					if(val.evidenciaNombre!=null){
						if(val.detalleGestAccionMejoraId > 0){
							btnDescargaCausa="<a class='efectoLink' " +
									"href='"+URL+"/gestionaccionmejora/detalle/evidencia?id="+val.detalleGestAccionMejoraId+"' " +
											"target='_blank' ><i class='fa fa-download'></i>"+val.evidenciaNombre+"</a>";
						}else{
							btnDescargaCausa="<a class='efectoLink' " +
							"href='"+URL+""+val.link+"' " +
									"target='_blank' ><i class='fa fa-download'></i>"+val.evidenciaNombre+"</a>";
						}
					}else{
						btnDescargaCausa="Sin evidencia"
					}
					textIn+="<tr id='tr"
					+ val.detalleGestAccionMejoraId
					+ "'>"

					+ "<td id='tdmod"
					+ val.detalleGestAccionMejoraId + "'>"
					+ val.moduloNombre+" / "+val.categoriaNombre 
					+"<br>"+val.detalleNombre +" / "+val.observacion 
					+"<br><i aria-hidden='true' class='fa fa-calendar'></i>Fecha: "+val.fechaInicioTexto
					+ "</td>"
					+"<td>" +btnDescargaCausa
							"</td>"

					 
					 
					 
					+ "</tr>"
				});
				$("#subFormEvento").html(textoBotonVolver+"<br>" +
						"<table  class='table table-striped table-bordered table-hover'>" +
						"<thead>" +
						"<tr><td  colspan='2' class='tb-acc'>Origen: '"+objActividad.text+"'</td></tr>" +
						"</thead>" +
						"<tbody>" +
						 textIn+
						"</tbody>" +
						"</table>"); 
			
				
				break;
			default:
				alert("Ocurrió un error al traer los items asociados!");
			}	
		}); 
	
}
 
function traerEventosTrabajador(){
	var dataParam = {
			empresaId : parseInt(sessionStorage.getItem("gestopcompanyid")),
			responsableId:(parseInt(sessionStorage.getItem("trabajadorGosstId"))>0?parseInt(sessionStorage.getItem("trabajadorGosstId")):null)
		};
		callAjaxPost(URL + '/scheduler', dataParam, function(data){
			listFullEventos=data.list;
			listFullTrabajadores=data.trabajadores;
			habilitarVistaTrabajador(); 
		});
		
}
function guardarActividadTrabajador(pindex){
	var fieldEvidencia="eventoId";
	var url = '/scheduler/evidencia/save';
	$.blockUI({
		message : 'Cargando...'
	});
	var fecha=convertirFechaTexto($("#dateActividad").val());
	listFullEventos[pindex].start_date=fecha;
	var inputFileImage = document.getElementById("fileEviActividad");
	var file = inputFileImage.files[0];
var observacion = $("#observacionActividad").val();
	var objActividad=listFullEventos[pindex];
	var dataSend = {
		fecha : fecha,observacion : observacion ,
		tipoEvento : objActividad.tipoEvento,
		id : objActividad.idAux,
	};
	
	if(fecha != null){
		listFullEventos[pindex].estadoEventoNombre="Completado";
		listFullEventos[pindex].estadoEvento=2;
		if(file){
			listFullEventos[pindex].evidenciaNombre=file.name;	
		}

		listFullEventos[pindex].observacion=observacion;
		listFullEventos[pindex].estadoEvento=2;
		listFullEventos[pindex].color="#9bbb59";
	}else{
		listFullEventos[pindex].estadoEventoNombre="Por implementar";
		listFullEventos[pindex].estadoEvento=1;
		listFullEventos[pindex].color="#F79646";
		//alert("Fecha obligatoria");$.unblockUI(); return;
	}
	if(objActividad.tipoEvento==16)
	{
		var felici=$("#selFelicitacionOpt").val();
		var vig=$("#inputVigenciaOpt").val();
		var seg=$("#selSeguimientoOpt").val();
		var dataParam=
		{
				id:objActividad.idAux,
				felicitacion:felici,
				vigenciaOpt:vig,
				seguimientoOpt:seg,
				fechaReal:fecha
		}
		callAjaxPostNoUnlock(URL + '/opt/eventos/cierre', dataParam, 
				function() {console.log("se actualizo el evento Opt");});
	}else{
		callAjaxPost(URL + '/scheduler/evento_normal/save', dataSend, 
				function() {console.log("se actualizo el scheduler");});
	}
	guardarEvidenciaAutoCalendar(objActividad.idAux,
			"fileEviActividad",bitsEvidenciaEventoCalendario,url,function(){
		
		$.unblockUI();
		agregarActividadColaborador(objActividad,pindex);  
		verAlertaSistemaGosst(1,"Se guardaron los cambios correctamente."); 
		setTimeout(function(){volverDivSubContenidoColaborador()},300);
		//traerEventosTrabajador();
	},fieldEvidencia,objActividad.tipoEvento); 
}

function obtenerSubPanelModulo(val){
	var estiloDefault="";
	if(val.inputForm=="" &&  val.sugerencia=="" && val.label==""){
		estiloDefault="    border-top: 1px solid #a5aaae;"
	}
	return "<div class='form-group row "+val.clase+"' style='"+estiloDefault+"'>"+
	   " <label for='colFormLabel' class='col-sm-4 col-form-label'>"+val.label+"</label>"+
	    "<div class='col-sm-8' id='"+val.divContainer+"'>"+
	     " "+val.inputForm+
	     "<small>"+val.sugerencia+" </small>"+
	    "</div>"+
	  "</div>";
}
function agregarModalPrincipalColaborador(modalOptions,functionCallBack,isStatic){
	$(".modal-backdrop").remove();
	$("#"+modalOptions.id).remove();
	$("body").append("<div class='modal' id='"+modalOptions.id+"' role='dialog'>"+
    "<div class='modal-dialog modal-dialog-centered' style='width:100%;    max-width: 650px;'>"+
     
    "<div class='modal-content' >"+
     "<div class='modal-header'>"+
     "   <button type='button' class='close' data-dismiss='modal'>&times;</button>"+
      "  <h4 class='modal-title'>"+modalOptions.nombre+"</h4>"+
      "</div>"+
       
      "<div class='modal-body'>"+
      modalOptions.contenido+
      "</div>"+
    "</div>"+
    
  "</div>"+
  "</div>");
	$("#"+modalOptions.id).on('shown.bs.modal', functionCallBack);
	if(isStatic){
		$('#'+modalOptions.id).modal({
			  keyboard: true,
			  show : true,
			  backdrop : false
			})
	}else{
		$("#"+modalOptions.id).modal("show");
	}
	
	
	 
}
function agregarPanelesDivPrincipalColaborador(listPanelesPrincipal){
	if(listPanelesPrincipal.length==0){
		$(".divListPrincipal")
		.append(
				"<div id='divNoResultados'  >" +
				"<div class='tituloSubList'></div>" +
				"<div class='contenidoSubList'></div>" +
				"</div>" 
				);
		$("#divNoResultados" ).find(".tituloSubList").html( 
			//"<i aria-hidden='true' class='fa fa-list'></i>"+ 
			"Sin resultados"); 
	}
	listPanelesPrincipal.forEach(function(val,index){
		$(".divListPrincipal")
		.append(
				"<div id='"+val.id+"' class='"+val.clase+"' >" +
				"<div class='tituloSubList'></div>" +
				"<div class='contenidoSubList'></div>" +
				"</div>" 
				);
		var textoBtn="<button class='btn btn-success'  onclick='cambiarDivSubContenidoColaborador(this,"+val.proyectoId+")'>" +
		"<i class='fa fa-caret-down fa-2x' aria-hidden='true'></i></button>";
		$("#"+val.id).find(".tituloSubList").html(textoBtn
			//	+"<i aria-hidden='true' class='fa fa-list'></i>"
				+val.nombre); 
		$("#"+val.id).find(".contenidoSubList").html(
				val.contenido
		);
	})
}
function verSelecionarTrabajadorOpt(pindex)
{
	var felici=$("#selFelicitacionOpt").val();
	var vig=$("#inputVigenciaOpt").val();
	var seg=$("#selSeguimientoOpt").val();
	var fechaReal=convertirFechaTexto($("#dateActividad").val());
	var objActividad = listFullEventos[pindex];
	var dataParam=
	{
			id:objActividad.idAux,
			felicitacion:felici,
			vigenciaOpt:vig,
			seguimientoOpt:seg,
			fechaReal:fechaReal
	}
	callAjaxPostNoUnlock(URL + '/opt/eventos/cierre', dataParam, 
			function(data) {
		if(data.CODE_RESPONSE=="05")
		{
			console.log("se actualizo el evento Opt");
			mostrarTrabEnviarInforme(pindex);
		}
	});
	
}
function mostrarTrabEnviarInforme(pindex)
{
	$.unblockUI();
	$("#mdInformeOpt").modal("show");
	$("#btnEnviarInformeOpt").attr("onclick", "javascript: enviarInformeOpt("+pindex+");");
	var selTrabajadores=crearSelectOneMenuOblig("selTrabajadores","",listFullTrabajadores,"","trabajadorId","trabajadorNombre");
	$("#mdInformeOpt .modal-header h4").remove()
	$("#mdInformeOpt .modal-header").append("<h4 class='modal-title' id='mdImpDatosLabel'>Seleccione Trabajador</h4>")
	$("#mdInformeOpt .modal-body").html(selTrabajadores)
}
function enviarInformeOpt(pindex)
{
	var objAux = listFullEventos[pindex];
	var trabajador=$("#selTrabajadores").val();
	var dataParam=
	{
		linkAuxiliarId1:trabajador,
		linkAuxiliarId2:parseInt(getSession("trabajadorGosstId")),
		tipoEvento:objAux.tipoEvento,
		idAux:objAux.idAux
	}
	callAjaxPost(URL + '/scheduler/eventos/informe', dataParam,
			function(){alert("enviado");
			$("#mdInformeOpt").modal("hide");});
}



