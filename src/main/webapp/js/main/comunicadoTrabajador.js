var listFullComunicado; 
var listFullTipoComunicado,listFullEmpresaContratista;
var comunicadoId, comunicadoObj;
var banderaEdicion=false;
$(function(){
	//Era attr no atrr
	$("#btnNuevoComunicadoTrabajador").attr("onclick","javascript: nuevoComunicadoTrabajador();");
	$("#btnCancelarComunicadoTrabajador").attr("onclick","javascript: cancelarComunicadoTrabajador();");
	$("#btnGuardarComunicadoTrabajador").attr("onclick","javascript: guardarComunicadoTrabajador();");
	$("#btnEliminarComunicadoTrabajador").attr("onclick","javascript: eliminarComunicadoTrabajador();");
	//Cuando se utiliza $(function) quiere decir que se va a realizar lo que está dentro una vez carguen todos los 
	//recursos de la página, por tanto lo de cargar comunicados no debería ir
	//insertMenu(cargarComunicado);
});


function volverComunicadoTrabajador()
{ 
	$("#divComunicadosTrabajador").show(); 
}
function cargarComunicadoTrabajador()
{
	
	$("#btnCancelarComunicadoTrabajador").hide();
	$("#btnNuevoComunicadoTrabajador").show();
	$("#btnEliminarComunicadoTrabajador").hide();
	$("#btnGuardarComunicadoTrabajador").hide();
	volverComunicadoTrabajador();
	callAjaxPost(URL + '/scheduler/comunicado', {
		empresaId : getSession("gestopcompanyid")},
		function(data) {
			if (data.CODE_RESPONSE = "05") {
				banderaEdicion = false;
				listFullComunicado = data.list;
				listFullTipoComunicado=data.listTipos;  
				$("#tblComunicadosTrabajador tbody tr").remove(); 
				listFullComunicado.forEach(function(val, index) {
					if(val.categoria.id==2)
					{
						$("#tblComunicadosTrabajador tbody").append(
								"<tr id='trcomun"+val.id+"' onclick='editarComunicadoTrabajador("+index+")'>" +
									"<td id='comuntipo"+val.id+"'>"+val.tipo.tipoComunicadoTexto+"</td>" 
									+"<td id='comunfechamens"+val.id+"'>"+val.fechaMensajeTexto+"</td>"
									+"<td id='comunhora"+val.id+"'>"+val.horaTexto+"</td>"
									+"<td id='comuncomentario"+val.id+"'>"+val.comunicado +"</td>" 
									+"<td id='comunevi"+val.id+"'>"+"</td>"
								+"</tr>");
						if(val.tipo.idTipo==1)//Aplicativo
						{
							$("#comunevi"+val.id).append(val.numAprob+" / "+val.numTotal);
						}
						else 
						{
							$("#comunevi"+val.id).append(val.numVisto+" / "+val.numTotal);
						}						
					}
				});
			
				completarBarraCarga();
				formatoCeldaSombreableTabla(true, "tblComunicadosTrabajador");
			} else {
				console.log("ERROR, no entro al IF :''v");
			}
		});
}
function nuevoComunicadoTrabajador(){
	if(!banderaEdicion){
		comunicadoId=0;
		var selTipo=crearSelectOneMenuOblig("inputComunTipo","",listFullTipoComunicado,"2","idTipo","tipoComunicadoTexto")
		
		$("#tblComunicadosTrabajador tbody").prepend(
			"<tr>" 
				+"<td>" +selTipo+"</td>"
				+"<td>" +"<input type='date' id='inputComunFechaMens' class='form-control' disabled>"+"</td>"
				+"<td>" +"<input type='time' id='inputComunHora' value='0' class='form-control' disabled>"+"</td>"
				+"<td>" +"<textarea id='inputComunComent' class='form-control'></textarea>"	+"</td>" 
				+"<td id='inputComunEvidencia'>...</td>"    
			+"</tr>");
		var fechaMens=obtenerFechaActual();
		$("#inputComunFechaMens").val(fechaMens);
		var horaActual=obtenerHoraActual();
		$("#inputComunHora").val(horaActual); 
		$("#inputComunTipo").prop("disabled",true);
		 
		$("#btnCancelarComunicadoTrabajador").show();
		$("#btnNuevoComunicadoTrabajador").hide();
		$("#btnEliminarComunicadoTrabajador").hide();
		$("#btnGuardarComunicadoTrabajador").show(); 
		banderaEdicion=true;
		formatoCeldaSombreableTabla(false,"tblComunicadosTrabajador");
	}
	else{
		alert("Guarde Primero.");
	}

}
 
function editarComunicadoTrabajador(pindex){
	if(!banderaEdicion){
		formatoCeldaSombreableTabla(false,"tblComunicadosTrabajador");
		comunicadoId=listFullComunicado[pindex].id;
		comunicadoObj=listFullComunicado[pindex];
		var tipo=comunicadoObj.tipo.idTipo;
		var selTipo=crearSelectOneMenuOblig("inputComunTipo","tipoCamposComunicado()",listFullTipoComunicado,tipo,"idTipo","tipoComunicadoTexto");
		$("#comuntipo"+comunicadoId).html(selTipo);
		$("#inputComunTipo").val(tipo);   
		var fechaMens=convertirFechaInput(comunicadoObj.fechaMensaje); 
		$("#comunfechamens"+comunicadoId).html("<input type='date' id='inputComunFechaMens' class='form-control' disabled>");
		$("#inputComunFechaMens").val(fechaMens);
		var hora=comunicadoObj.hora;
		$("#comunhora"+comunicadoId).html("<input type='time' id='inputComunHora' class='form-control' disabled>");
		$("#inputComunHora").val(hora);
		var comentario=comunicadoObj.comunicado;
		$("#comuncomentario"+comunicadoId).html("<textarea id='inputComunComent' class='form-control'></textarea>");
		$("#inputComunComent").val(comentario);
		var fechaImpl=convertirFechaInput(comunicadoObj.fechaImplementacion); 
		$("#comunfechaImpl"+comunicadoId).html("<input type='date' id='inputComunFechaImpl' class='form-control'>");
		$("#inputComunFechaImpl").val(fechaImpl);
	 
		var evidencias=$("#comunevi"+comunicadoId).text(); 
		 
		$("#inputComunTipo").prop("disabled",true); 
		banderaEdicion=true;   
		
		
		$("#btnCancelarComunicadoTrabajador").show();
		$("#btnNuevoComunicadoTrabajador").hide();
		$("#btnEliminarComunicadoTrabajador").show();
		$("#btnGuardarComunicadoTrabajador").show(); 
		
	} 
}
function guardarComunicadoTrabajador(){
	var campoVacio=true;
	//var fechaImpl="";
	var tipo=$("#inputComunTipo").val();
	var fechaMens=convertirFechaTexto($("#inputComunFechaMens").val());
	var hora=$("#inputComunHora").val(); 
	var comentario=$("#inputComunComent").val(); 
	var categoriaId=2; 
	var empresaid= getSession("gestopcompanyid");
 
	if(campoVacio){
		var dataParam=
		{
			id:comunicadoId,
			fechaMensaje:fechaMens,
			hora:hora,
			tipo:{idTipo: tipo},
			comunicado:comentario,
			empresaId: empresaid,
			categoria:{id:categoriaId} 
		};
		callAjaxPost(URL+'/scheduler/comunicado/save',dataParam,
				cargarComunicadoTrabajador);
	}
	else{
		alert("Debe ingresar todos los campos.");
	}
}
function cancelarComunicadoTrabajador(){
	cargarComunicadoTrabajador();
}






