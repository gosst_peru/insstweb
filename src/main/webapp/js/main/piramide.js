var ctx;
$(function(){
	
	
	llamarPiramide();

})


function llamarPiramide(){

	
	
	var dataParam = {
			empresaId : sessionStorage.getItem("gestopcompanyid"),
			
			
			};

		callAjaxPost(URL + '/estadistica/piramide', dataParam,
				procesarLlamadaPiramide);
	
}

function pasarConsultaString(consultaId){
	switch(consultaId){
	case 1:
		return "Accidentes Fatales"
		break;
	case 2:
		return "Accidentes Incapacitantes"
		break;
	case 3:
		return "Accidentes Leves"
		break;
	case 4:
		return "Incidentes"
		break;
	case 5:
		return "Actos y condiciones inseguras"
		break;
	
	}
	
}

function procesarLlamadaPiramide(data1){
	
	
	switch (data1.CODE_RESPONSE) {
	case "05":
			var piramid = data1.list;
			var data;
			var labels=[];
			var data1=[];
			var options=[];
			 for(index=0;index<piramid.length;index++){
		    	 labels.push(pasarConsultaString(piramid[index].consultaId))
		    	 data1.push( piramid[index].indicador);
		    }
			 data={
					 labels:labels,
					 datasets:[
					           {
					        	   label: "My Second dataset",
					               fillColor: "rgba(151,187,205,0.2)",
					               strokeColor: "rgba(151,187,205,1)",
					               pointColor: "rgba(151,187,205,1)",
					               pointStrokeColor: "#fff",
					               pointHighlightFill: "#fff",
					               pointHighlightStroke: "rgba(151,187,205,1)",
					        	   data:data1
					           }
					           
					           ]
			 }
				ctx = $("#myChart").get(0).getContext("2d");
				Chart.defaults.global.responsive = true;
			var myBarChart = new Chart(ctx).Bar(data, options);
			
			
		var data = new google.visualization.DataTable();
	    data.addColumn('string', "Numero");
	    data.addColumn('number', "Tipos de accidente");
	    data.addColumn({type:'string', role:'annotation'});
		
	    for(index=0;index<piramid.length;index++){
	    	 data.addRow(
	    	                [pasarConsultaString(piramid[index].consultaId), 
	    	                 piramid[index].indicador,
	    	                 pasarConsultaString(piramid[index].consultaId)]
	    	             );
	    	
	    }
	    var alto=450;
	    var ancho=350;
		
	    var wrapper = new google.visualization.ChartWrapper({
	      chartType: 'BarChart',
	      dataTable: data,
	      options: {'title': "Piramide de Accidentabilidad",
	    	  width: ancho ,
	    	  height: alto,
	    	  legend: { position: "none" },
	    	
	    	
	    	vAxis:{ textPosition:"none"},

	    
	      },
	      containerId: 'piramideAccidentabilidad'
	      });
	    wrapper.draw();		

		
		
		break;
	default:
		alert("Ocurrió un error al traer indicadores!"+data1.CODE_RESPONSE);

}
	
	
	
}


function llamarRadar(){

	
	
	var dataParam = {
				empresaId : sessionStorage.getItem("gestopcompanyid"),
			
			
			};

		callAjaxPost(URL + '/estadistica/radar', dataParam,
				procesarLlamadaRadar);
	
}

function pasarConsultaRadarString(consultaId){
	switch(consultaId){
	case 1:
		return "Accidentes Fatales"
		break;
	case 2:
		return "Accidentes Graves"
		break;
	case 3:
		return "Accidentes Leves"
		break;
	case 4:
		return "Incidentes"
		break;
	case 5:
		return "Actos y condiciones inseguras"
		break;
	
	}
	
}

function procesarLlamadaRadar(data1){
	
	
	switch (data1.CODE_RESPONSE) {
	case "05":
			var radar = data1.list;
	
		

		
		
		break;
	default:
		alert("Ocurrió un error al traer radar!"+data1.CODE_RESPONSE);

}
	
	
	
}





