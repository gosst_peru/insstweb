var listAyuda;
var ayudaId;
var bitsMega=512000*2;
var listaModulosPermitidos=[];
var bitsLogoEmpresa=bitsMega*1.5;
var bitsEvidenciaCurso=bitsMega*5;
var bitsEvidenciaArchivoCurso=bitsMega*5;
var bitsEvidenciaImagenPregunta=bitsMega*5;
var bitsEvidenciaProcedimiento=bitsMega*5;
var bitsEvidenciaVerificacion=bitsMega*4;
var bitsEvidenciaAccidente=bitsMega*1.5;
var bitsEvidenciaExamen=bitsMega*1;
var bitsEvidenciaFormacion=bitsMega*1;
var bitsEvidenciaVersion=bitsMega*5;
var bitsEvidenciaEPP=bitsMega*1;
var bitsEvidenciaActoInseguro=bitsMega*15;
var bitsEvidenciaRespuestaSeguridad=bitsMega*1;
var bitsEvidenciaAccionMejora=bitsMega*1;
var bitsEvidenciaAnalisisCausa=bitsMega*1.5;
var bitsEvidenciaEventoCalendario=bitsMega*3;
var bitsEvidenciaAuditoria=bitsMega*3;
var bitsEvidenciaHallazgo=bitsMega*3;
var bitsEvidenciaControl=bitsMega*3;
var bitsEvidenciaGrupo=bitsMega*1;
var bitsEvidenciaReunion=bitsMega*1;
var bitsEvidenciaAcuerdo=bitsMega*1;
var bitsContrato=bitsMega*3;
var bitsSueldo=bitsMega*3;
var bitsHijo=bitsMega*3;
var bitsCese=bitsMega*3;
var bitsCharla =bitsMega*3;
var bitsVacacion=bitsMega*3;
var bitsMemo=bitsMega*3;
var bitsEstudio=bitsMega*3;
var moduloSeleccionadoMenu;
var indexUnidadSeleccionada;
var irDirectamenteModulo=true;
var factorMovil;
 
var isMobile = {
	    Android: function() {
	        return navigator.userAgent.match(/Android/i);
	    },
	    BlackBerry: function() {
	        return navigator.userAgent.match(/BlackBerry/i);
	    },
	    iOS: function() {
	        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
	    },
	    Opera: function() {
	        return navigator.userAgent.match(/Opera Mini/i);
	    },
	    Windows: function() {
	        return navigator.userAgent.match(/IEMobile/i);
	    },
	    Tablet:function(){
	    	return $(window).width()<768;
	    },
	    any: function() {
	        return ((isMobile.Android() || isMobile.BlackBerry() 
	        		|| isMobile.iOS() || isMobile.Opera() 
	        		|| isMobile.Windows() )&&( isMobile.Tablet() ));
	    }
	};
function completarBarraCarga(){
	 var progressbar = $( "#barraCarga" );
	 var val =  progressbar.progressbar( "value" ) || 0;
	progressbar.progressbar( "value", val+4 );
	if ( val < 99 ) {
        setTimeout( completarBarraCarga, 1);
        //era 80 :v
      }
	 
}
var tiempoPrueba=1000*60*27;
$(function(){
	console.log("HTTPS working")
	if($(window).width()<768){
		factorMovil=80
	}else{
		factorMovil=0
	}
	 
	$.blockUI({message:'<img src="../imagenes/gif/ruedita.gif"></img>cargando...'});
	  var progressbar = $( "#barraCarga" ),
	  progressLabel = $( ".progress-label" );
	  progressbar.progressbar({
	      value: 5,
	      change: function() {
	          progressLabel.text( progressbar.progressbar( "value" ) + "%" );
	        },
	        complete: function() {
	          progressLabel.text( "100%" );
	         $(".cargaPagina").remove();
	        }
	    });
	$("head").append(
	"<link href='../imagenes/logoIcono.ico' rel='shortcut icon' type='image/x-icon'> "		
	);
	
	 var val =  progressbar.progressbar( "value" ) || 0;
	 


	 $.unblockUI();
	
	 $(document).on("keydown",function(e){
			
		    if(e.keyCode == 37)
		    {
		        $(this).trigger("avanzarPresentacion");
		    	diapositivasCargadas-=1;
		    	cambiarDiapositiva(false);
		    }
		    if(e.keyCode == 39)
		    {
		    	diapositivasCargadas+=1;
		    	cambiarDiapositiva(true);
		    }
		});
	 
	
	   
});
function funcionTiempoPrueba(){
	
		 tiempoPrueba+=2*60*1000
		 callAjaxPost(URL + '/empresa/delete', {idCompany:0}, function(data){
			 console.log(new Date());
		 },function(){},{},function(){console.log("Log pls")});
		 setTimeout(funcionTiempoPrueba,tiempoPrueba) ;
}
function cambiarAcceso(){
	irDirectamenteModulo=!irDirectamenteModulo;
}
function cerrarSesion() {
	var dataParam = {
		userId : sessionStorage.getItem("gestopUi"),
		sessionId : sessionStorage.getItem("gestopsessionId")
	};
	callAjaxPost(URL + '/login/session/delete', dataParam, null);
	removeAllSession();
}

function irConfiguracion() {
	document.location.href = 'configuracion.html';
}

function irLicencia() {
	document.location.href = 'licencia.html';
}


function llamarModulosPermitidosUsuario(){
	
	callAjaxPost(URL + '/login/menu', { }, procesarLlamadaModulos);
	
}

function procesarLlamadaModulos(data){
	
	 listaModulosPermitidos= data.licencia;
		createSession("perfilGosstId", data.perfilId);
	var listaModulos="";
	$("#menuPrincipal li").remove();
	$("#menuPrincipal").append(
		    " " +
		       ""+ 
			" <li class='dropdown submenu'>"+
			" <a href='#' class='navbar-toggle collapse in' data-toggle='collapse'  id='moduloSelected'>Mdulos<span class='caret'></span></a>"+
			" <ul class='dropdown-menu submenu' role='menu' id='listaTotalModulos'>"	
	);
	$("#barraMenu").after(
			"<div id='wrapperList' class='toggled toggled-2'>"+
	        "<!-- Sidebar -->"+
	        "<div id='sidebar-wrapper'>"+
	         "   <ul class='sidebar-nav nav-pills nav-stacked' id='menu'>"+
	            "</ul>"+
	      "  </div> " +
	      "</div>" 
	   	
	);
	
	$("#barraMenu").after(
		"<div id='menuExplicativo' >"+
	       "<div id='menuFigureti'  ></div>"+
	       "<div id='graficoResumenModulo' class='graficoModulo'>" +
		      "<div id='subDivResumen1'></div>" +
		      "<div id='subDivResumen2'></div>" +
		      "</div>"	+
	      "</div>"		
	     
	);
 
	$("#subDivResumen1").css({"width":"100%"});
	
	
	$("#sidebar-wrapper").css("height",$(window).height()-$("nav").height()-2+"px");
	
	$(".sidebar-nav").css("width","290px");

    
	$("#moduloSelected").html(
			sessionStorage.getItem("modulo")
					+ " <span class='caret'></span>");
	$("#moduloSelected").on(function(e) {
        e.preventDefault();
       
        $('#menu ul').hide();
    });
	var opcionResponsive;
	if($(window).width()<768){
		opcionResponsive=0;
		$("#menuMovilGosst").click(function(e) {
	        e.preventDefault();
	        irDirectamenteModulo=!irDirectamenteModulo; 
	    	  $("#menuExplicativo").toggle().css({"padding-top":$("#barraMenu").css("height")});
	       $("#graficoResumenModulo").fadeToggle();
	       $("#subDivResumen2").css({"width":"100%","height":$(window).height()-$("nav").height()-34+"px","float":"left","overflow-y":"scroll"});
	   	
	        $('#menu ul').hide();
	    });
	}else{
		opcionResponsive=0;
	}
	
     $("#moduloSelected").click(function(e) {
        e.preventDefault();
        irDirectamenteModulo=!irDirectamenteModulo; 
    	  $("#menuExplicativo").toggle();
       $("#graficoResumenModulo").fadeToggle();
       $("#subDivResumen2").css({"width":"100%","height":$(window).height()-$("nav").height()-34+"px","float":"left","overflow-y":"scroll"});
   	
        $('#menu ul').hide();
    });
     $("#graficoResumenModulo").hide();
     $("#wrapperList #menu li").remove();
 	$("#menuFigureti div").remove();
 	console.log(listaModulosPermitidos);
	for(var i=0;i<listaModulosPermitidos.length;i++){
		var isMovil=(isMobile.any()?1:0);
		var menuMovil=listaModulosPermitidos[i].menuMovil;
		var menuMovilValido=(isMovil==0?true:(menuMovil==1));
		if(menuMovilValido){
			
		
		var idModulo=listaModulosPermitidos[i].menuOpcionId;
		var isPermitido=listaModulosPermitidos[i].isPermitido
		var nombreModulo=listaModulosPermitidos[i].menuOpcionNombre;
		var numerSubModulos=listaModulosPermitidos[i].subMenus;
		var icono=listaModulosPermitidos[i].icono;
		var imagenMovil=listaModulosPermitidos[i].imagenUrl;
		imagenMovil=(imagenMovil==null?'no_imagen.png':imagenMovil)
	var iconoSubFlecha="";
		switch(idModulo){
		case 4:
			iconoSubFlecha='<i class="fa fa-arrow-down" aria-hidden="true"></i>'
			break;
		
		case 9:
			iconoSubFlecha='<i class="fa fa-arrow-down" aria-hidden="true"></i>'
			break;
		case 11: 
			iconoSubFlecha='<i class="fa fa-arrow-down" aria-hidden="true"></i>'
			break;
		case 15:
			iconoSubFlecha='<i class="fa fa-arrow-down" aria-hidden="true"></i>'
			break ;
			default:
				iconoSubFlecha=''
				break;
		}
		var classPermitidoLeft=(isPermitido==1?"fastAccesible":"fastNoAccesible");
		var classPermitido=(isPermitido==1?"moduloAccesible":"moduloNoAccesible");
		var iconoFlecha="<span><i aria-hidden='true' class='fa fa-arrow-right '></i></span>";
			$("#wrapperList #menu").append(
					"<li id='listaModulo"+idModulo+"' class='"+classPermitidoLeft+" '>" +
							"<a href='#' " +
							">" +
									"<span class='fa-stack fa-lg pull-left '>" +
									"<i aria-hidden='true' class='"+icono+" fa-stack-1x'></i></span>"+nombreModulo+iconoSubFlecha+""+""+"</a>"+""+"</li>");
			
		var textoDescripcion="";
		
		 
			$("#menuFigureti").append("<div class='"+classPermitido+" containerFig col-xs-6 col-ss-3 col-sm-3 col-md-3 col-lg-3' id='itemId"+idModulo+"' title='"+nombreModulo+"'>" +
					"<div class='itemFig' >" +
					"<img src='../imagenes/movil/"+imagenMovil+"'>"+
					"<span class='fa-stack fa-lg' >"
					+""
					  +"  <i class='"+icono+" fa-3x'></i>"
					  +"</span>"+
					"</div>");
			/***Inicio de funcion de items****/
			$("#itemId"+idModulo).on("click",{id:idModulo,isPermitido:isPermitido},function(ev){
				var idAux=ev.data.id;
				var hasAccess=ev.data.isPermitido;
				if(hasAccess==0){
					$("#subDivResumen1").html("No tiene acceso a este módulo");
					console.log("No tiene acceso");
					$("#btnIrModulo").remove();
					return false;
				}
				moduloSeleccionadoMenu=idAux;
				$("#textoDescrip").remove();
				$("#btnVolverModulos").remove();
				$("#btnIrModulo").remove();
				$(".containerFig").children().removeClass("itemNoFig").addClass("itemFig");
				
				if($(".subItem"+idAux).length>0){
					$(".containerFig").hide();
					$(".tooltip").hide();
					if($(".subItem"+idAux).length==1 &&(idAux==9
							|| idAux==11
							|| idAux==15)){
						var idLinkAux;
						switch(idAux){
						case 9:
							idLinkAux=10;
							break;
						case 11:
							idLinkAux=30
						break;
						case 15:
							idLinkAux=16
							break;
							default:
								idLinkAux=moduloSeleccionadoMenu
								break;
						}
						irHacia(idLinkAux,sessionStorage.getItem("unidadGosstId"),sessionStorage.getItem("unidadGosstNombre"));
					}else{
						$(this).after("<div id='btnVolverModulos' title='Volver' class='subContainerFig col-xs-6 col-ss-3 col-sm-3 col-md-3 col-lg-3'>" +
								
								"<div class='subItemFig'>"
								+"  <i class='"+"fa fa-arrow-circle-left"+" fa-3x'></i>"
								+"</div>" +
								"</div>");
						$(".subItem"+idAux).show();
						$(".subContainerFig").children().removeClass("itemNoFig").addClass("subItemFig");
						
					}
					verGraficoResumen(moduloSeleccionadoMenu);
				}else{
					
					
					$(this).children().removeClass("itemFig").addClass("itemNoFig");
					verGraficoResumen(moduloSeleccionadoMenu);
					var btnIrModulo="<div id='btnIrModulo' class='containerFig col-xs-6 col-ss-3 col-sm-3 col-md-3 col-lg-3'>" +
					" <div class='itemFig'> "
					+"  <i class='"+"fa fa-arrow-circle-right"+" fa-3x'></i>"
					
					+" </div>" +
							 
					"</div>";
					$("#menuFigureti").append(btnIrModulo);
					$("#btnIrModulo").on("click",function(){
						 irHacia(parseInt(moduloSeleccionadoMenu),null);
					});
				}
				
				$("#subDivResumen1").append("<div id='textoDescrip'>"+
						textoDescripcion+"" +
								"" +
								"</div>" 
								);
				
				
			});
			$(document).on("click","#btnVolverModulos",function(){
				var idAux=$(this).prop("id").split("btnVolverModulos");
				$("#btnIrModulo").remove();
				$(".containerFig").show();
				$(".subContainerFig").hide();
				$(this).remove();
				$("#textoDescrip").remove();
				$("#btnVolverModulos").remove();
			});
			/****Fin de funcion item*****/
			if(numerSubModulos.length>0){
			$("#listaModulo"+idModulo).append(
					"<ul class='nav-pills nav-stacked' style='list-style-type:none;margin-left:20px'>");
		for(var i2=0;i2<numerSubModulos.length;i2++){
			var idSubModulo=numerSubModulos[i2].menuOpcionId;
			var nombreSubModulo=numerSubModulos[i2].menuOpcionNombre;
			var subIcono=numerSubModulos[i2].icono;
			var isPermitidoSub=numerSubModulos[i2].isPermitido;
			var classPermitidoLeft=(isPermitidoSub==1?"fastAccesible":"fastNoAccesible");
			var classPermitido=(isPermitidoSub==1?"moduloAccesible":"moduloNoAccesible");
			var imagenMovil2=numerSubModulos[i2].imagenUrl;
			imagenMovil2=(imagenMovil2==null?'no_imagen.png':imagenMovil2)
			$("#listaModulo"+idModulo+" ul").append(
					"<li id='listaModulo"+idSubModulo+"' class='"+classPermitidoLeft+"'>" +
							"<a href='#' " +
							"'>" +
									"<span class='fa-stack fa-lg pull-left'><i aria-hidden='true' class='"+subIcono+" fa-stack-1x '></i>" +
											"</span>"+nombreSubModulo+"</a></li>"	
			);
			$("#listaModulo"+idModulo).append("</ul>");
			$("#menuFigureti").append("<div class='"+classPermitido+" subContainerFig col-xs-6 col-ss-3 col-sm-3 col-md-3 col-lg-3 subItem"+idModulo+"' title='"+nombreSubModulo+"' id='itemId"+idSubModulo+"' style='display:none'>" +
					"<div class='subItemFig' >" +
					"<img src='../imagenes/movil/"+imagenMovil2+"'>"+
					"	<span class='fa-stack fa-lg'>"
						+"  <i class='"+subIcono+" fa-3x'></i>"
							+"</span><br> "  +
					"</div>");
			
			
			/***Inicio de funcion de items****/
			$("#itemId"+idSubModulo).on("click",{id:idSubModulo,isPermitido:isPermitidoSub},function(ev){
				var idAux=ev.data.id;
				var hasAccess=ev.data.isPermitido;
				if(hasAccess==0){
					$("#subDivResumen1").html("No tiene acceso a este módulo");
					console.log("No tiene acceso");
					$("#btnIrModulo").remove();
					return false;
				}
				moduloSeleccionadoMenu=idAux;
				$("#textoDescrip").remove();
				$("#btnIrModulo").remove();
				$(".subContainerFig").children().removeClass("itemNoFig").addClass("subItemFig");
				
				
					$(this).children().removeClass("subItemFig").addClass("itemNoFig");
					verGraficoResumen(moduloSeleccionadoMenu);
					var btnIrModulo="<div id='btnIrModulo' class='containerFig col-xs-6 col-ss-3 col-sm-3 col-md-3 col-lg-3'>" +
					"<div id='subItemFig'><span class='fa-stack fa-lg'  >" 
					+"  <i class='"+"fa fa-arrow-circle-right"+" fa-3x'></i>"
					
					+"</div>" +
					"</div>";
					$("#menuFigureti").append(btnIrModulo);
					$("#btnIrModulo").on("click",function(){
						 irHacia(parseInt(moduloSeleccionadoMenu),null);
					});
				
				
				$("#subDivResumen1").append("<div id='textoDescrip'>"+
						textoDescripcion+"" +
								"" +
								"</div>" 
								);
				
				
			});
		
			/****Fin de funcion item*****/ 
		};
	
			}
		}
	};

	crearSubMenu(9, 10, "Puesto de Trabajo");
	
	crearSubMenu(11, 30, "Trabajador");

	crearSubMenu(15, 16, "A/C Subest&aacute;ndar");
	
	if(sessionStorage.getItem("submodulo")){
		$("#moduloSelected span").remove();
		$("#moduloSelected").append(
				" -"+sessionStorage.getItem("submodulo")
						+ " <span class='caret'></span>");			
	}
	

	 initMenu();
	 $(".containerFig").tooltip({
			placement: "bottom"
		});
	 $(document).on({
		    mouseenter: function () {
		      $("#sidebar-wrapper").hide();
		    },
		    mouseleave: function () {
		    	 $("#sidebar-wrapper").show();
		    }
		}, "#btnNuevo");
	 
	 if(getSession("gestopInicio")==0){
			createSession("gestopInicio",1);
			$("#menuMovilGosst").click();	
		}
}

function initMenu() {
     $('#menu ul').hide(); 
     $('#menu ul').children('.current').parent().show();
     //$('#menu ul:first').show();
       
     $('#menu li a').on("click",
   	        function() {
   	          var checkElement = $(this).next();
   	          if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
   	        	  checkElement.slideUp('normal');
   	            return false;
   	            }
   	          if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
   	            $('#menu ul:visible').slideUp('normal');
   	            checkElement.slideDown('normal');
   	            return false;
   	            }
   	          }
   	        );
     $('#menu li a').on("click",function() {
    	 var aux=$(this).parent().prop("id").split("listaModulo")
    	 verGraficoResumen(aux[1]),null}
    	        );
     }
function insertMenuPrimeraVez(functionCallback) {
	$("#barraMenu").html(CABECERA);
	var dataParam = {
			
			
	};

	callAjaxPost(URL + '/empresa/usuario', dataParam, function(data){
		switch (data.CODE_RESPONSE) {
		case "05":
			
			$("#loginUser").html(sessionStorage.getItem("gestopusername") + "<span class='caret'></span>");
			if (sessionStorage.getItem("modulo") != null) {
				
				
			}
			


			var list = data.list;
			listAyuda=data.listAyuda;
			
			//
			for (index = 0; index < list.length; index++) {
				$("#menuEmpresa").append(
						"<li><a href='#' onclick='javascript:seleccionaEmpresa("
						+ list[index].idCompany + ", &#34;"		
						+ list[index].name + "&#34;, &#34;"
								+ list[index].ruc + "&#34;);'>" + list[index].name
								+ "</a></li>");
			}
			if (sessionStorage.getItem("gestopcompanyid") == null
					|| sessionStorage.getItem("gestopcompanyid") == -1) {
				if (list.length > 0) {
					createSession("gestopcompanyid", list[0].idCompany);
					createSession("gestopcompanyname", list[0].name);
					createSession("gestopcompanyruc", list[0].ruc);
					$("#companySelected").html("<strong>Empresa :</strong>  "+list[0].name);
				} else {
					createSession("gestopcompanyid", -1);
					$("#companySelected").html("¡No existen empresas!");
				}

			} else {
				$("#companySelected").html("<strong>Empresa :</strong>  "+
						sessionStorage.getItem("gestopcompanyname")
								+ " <span class='caret'></span>");
			}
			var subFunctionCall=null;
			//
			llamarModulosPermitidosUsuario();
			completarBarraCarga();
			break;
		default:
			alert("Ocurrió un error al traer las empresas!");
		break;
		}
	},null,{},null,function(){
		if(functionCallback!=null){
			functionCallback();
		}
		});
	
	resizeDivi();
	
}
function insertMenu(functionCallback) {
	$("#barraMenu").html(CABECERA);
	
	cargarEmpresasxUsuario(functionCallback);
	resizeDivi();
	
}


function cargarEmpresasxUsuario(functionCallback) {
	var dataParam = {
		
	};

	callAjaxPost(URL + '/empresa/usuario', dataParam, function(data){

		switch (data.CODE_RESPONSE) {
		case "05":
			
			$("#loginUser").html(sessionStorage.getItem("gestopusername") + "<span class='caret'></span>");
			if (sessionStorage.getItem("modulo") != null) {
				
				
			}

			var list = data.list; 
			
			
			for (index = 0; index < list.length; index++) {
				$("#menuEmpresa").append(
						"<li><a href='#' onclick='javascript:seleccionaEmpresa("
						+ list[index].idCompany + ", &#34;"		
						+ list[index].name + "&#34;, &#34;"
								+ list[index].ruc + "&#34;);'>" + list[index].name
								+ "</a></li>");
			}
			if (sessionStorage.getItem("gestopcompanyid") == null
					|| sessionStorage.getItem("gestopcompanyid") == -1) {
				if (list.length > 0) {
					createSession("gestopcompanyid", list[0].idCompany);
					createSession("gestopcompanyname", list[0].name);
					createSession("gestopcompanyruc", list[0].ruc);
					$("#companySelected").html("<strong>Empresa: </strong>  "+list[0].name);
				} else {
					createSession("gestopcompanyid", -1);
					$("#companySelected").html("¡No existen empresas!");
				}

			} else {
				$("#companySelected").html("<strong>Empresa :</strong>  "+
						sessionStorage.getItem("gestopcompanyname")
								+ " <span class='caret'></span>");
			}
			if(functionCallback!=null){
				functionCallback();
			}
			llamarModulosPermitidosUsuario();
			break;
		default:
			alert("Ocurrió un error al traer las empresas!");
		}
	});
}

function procesarMenu(data) {


}
function callAjaxPostNoUnlock(pUrl, dataParam, funcionRespuesta,funcionBefore,dataBefore,functionError) {
	
	$.ajax({
		url : pUrl,
		type : 'post',
		async : true,
//		crossDomain: true,
		xhrFields: {
            withCredentials: true
        },
		data : JSON.stringify(dataParam),
		contentType : "application/json",
		beforeSend:
function(){
			if(funcionBefore==null){
				$.blockUI({message:'<img src="../imagenes/gif/ruedita.gif"></img>Cargando...'});
					
			}else{
				funcionBefore(dataBefore);
			};
			
			
		},
		success : function(data, textStatus, jqXHR) {
			
		
			switch (data.CODE_RESPONSE) {
			case "06":
				sessionStorage.clear();
				document.location.replace(data.PATH);
				break;
			case "07":
				//sessionStorage.clear();
				alert("Sesion expirada");
				document.location.replace(data.PATH);
				break;
			default:
				funcionRespuesta(data); 
			}

		},
		error : function(jqXHR, textStatus, errorThrown) {
			$.unblockUI();
			
			console.log("Ruta de error:"+pUrl);
			if(functionError!=null){
				functionError();
			}
				var responseText = jQuery.parseJSON(jqXHR.responseText);
				if(responseText.CODE_RESPONSE == "07"){
					alert("Sesión Expiradaa");
					 document.location.href=(responseText.PATH);
				}
				if(responseText.CODE_RESPONSE == "09"){
					alert("No tiene acceso al módulo");
				}
			
			//document.location.replace(responseText.PATH);
		}
	});
	
}
var gosstError=false;
function callAjaxPost(pUrl, dataParam, funcionRespuesta,funcionBefore,dataBefore,functionError,functionComplete) {
	 var dataResponseServer=[];
	$.ajax({
		url : pUrl,
		type : 'post',
		async : true,
//		crossDomain: true,
		xhrFields: {
            withCredentials: true
        },
		data : JSON.stringify(dataParam),
		contentType : "application/json",
		beforeSend:
			function(){
			if(funcionBefore==null){
				$.blockUI({message:'<img src="../imagenes/gif/ruedita.gif"></img>cargando...'});
					
			}else{
				funcionBefore(dataBefore);
			};
			
			
		},
		success : function(data, textStatus, jqXHR) {
			
			switch (data.CODE_RESPONSE) {
			case "06":
				sessionStorage.clear(); 
				document.location.replace(data.PATH);
				break;
			case "07":
				//sessionStorage.clear();
				alert("Sesion expirada");
				document.location.replace(data.PATH);
				break;
			case "09":
				alert("Sin acceso al módulo")
				break;
			default:
				funcionRespuesta(data);
				$.unblockUI();
				break;
			}

		},
		complete:function(data){
			
			if(functionComplete==null){
				
			}else{
				functionComplete();
				
			}
		},
		
		error : function(jqXHR, textStatus, errorThrown) {
			$.unblockUI();
			
				console.log("Ruta de error:"+pUrl);
				if(!gosstError){
					gosstError=true;
					var responseText = jQuery.parseJSON(jqXHR.responseText);
					if(responseText.CODE_RESPONSE == "07"){
						alert("Sesión Expiradaa");
						 document.location.href=(responseText.PATH);
					}
					if(responseText.CODE_RESPONSE == "09"){
						alert("No tiene acceso al módulo");
					}
				}
				
					
				
			
			
		}
	});
	
}

function loadingCelda(idFila){
	 var celdaPadre=$("#"+idFila+":first");
var numCol=celdaPadre.children().length;
celdaPadre.html("<td style='background-color:#9BBB59' colspan='"+numCol+"'>Guardando</td>");
 
}
var enviando=false;
function callAjaxPostGraficoMenu(pUrl, dataParam, funcionRespuesta,isParalelo){
	
	$.ajax({
		url : pUrl,
		type : 'post',
		async : true,
		xhrFields: {
            withCredentials: true
        },
		data : JSON.stringify(dataParam),
		contentType : "application/json",
		beforeSend:function(){
			if(!enviando || isParalelo){
			$('#subDivResumen3').html('<i class="fa fa-refresh fa-spin fa-3x fa-fw"></i>'+
	 		'<span class="sr-only">Loading...</span>');
			enviando=true;
			}else{
				return  false;
			};
			
		},
		success : function(data, textStatus, jqXHR) {
			
		
			switch (data.CODE_RESPONSE) {
			case "06":
				sessionStorage.clear();
				document.location.replace(data.PATH);
				break;
			case "07":
				//sessionStorage.clear();
				alert("Sesion expirada");
				 document.location.replace(data.PATH); 
			case "09":
				alert("Sin acceso al módulo")
				break; 
			default:
				funcionRespuesta(data);enviando=false;
			$.unblockUI();
			}

		},
		error : function(jqXHR, textStatus, errorThrown) {
			
			console.log("Ups, tenemos problemas tecnicos. Intente más tarde. Error: "
					+ errorThrown);
			
		}
	});
}
function callAjaxPostNoLoad(pUrl, dataParam, funcionRespuesta,funcionBefore,dataBefore) {
	
	$.ajax({
		url : pUrl,
		type : 'post',
		async : true,
		xhrFields: {
            withCredentials: true
        },
    	beforeSend:
			function(){
			if(funcionBefore==null){
				//$.blockUI({message:'<img src="../imagenes/gif/ruedita.gif"></img>cargando...'});
					
			}else{
				funcionBefore(dataBefore);
			};
			
			
		},
		data : JSON.stringify(dataParam),
		contentType : "application/json",
		success : function(data, textStatus, jqXHR) {
			
		
			switch (data.CODE_RESPONSE) {
			case "06":
				sessionStorage.clear();
				document.location.replace(data.PATH);
				break;
			case "07":
				//sessionStorage.clear();
				alert("Sesion expirada");
				document.location.replace(data.PATH);
			case "09":
				alert("Sin acceso al módulo")
				break; 
			default:
				funcionRespuesta(data);
			$.unblockUI();
			}

		},
		error : function(jqXHR, textStatus, errorThrown) {
			
			
var responseText = (jqXHR);
console.log(responseText);
			if(responseText.CODE_RESPONSE == "07")
				alert("No tiene permiso!!!");
		}
	});
	
}
function callAjaxPostTypeSync(pUrl, dataParam, funcionRespuesta) {
	$.ajax({
		url : pUrl,
		type : 'post',
		async : false,
		data : JSON.stringify(dataParam),
		contentType : "application/json",
		success : function(data, textStatus, jqXHR) {
			switch (data.CODE_RESPONSE) {
			case "06":
				sessionStorage.clear();
				document.location.replace(data.PATH);
				break;
			default:
				funcionRespuesta(data);
			}

		},
		error : function(jqXHR, textStatus, errorThrown) {
			alert("Ups, tenemos problemas tecnicos. Intente más tarde. Error: "
					+ errorThrown);
			console.log('xhRequest: ' + jqXHR + "\n");
			console.log('ErrorText: ' + textStatus + "\n");
			console.log('thrownError: ' + errorThrown + "\n");
		}
	});
}

function habilitarBotonesEdicion() {
	$("#btnGuardar").show();
	$("#btnCancelar").show();
	$("#btnEliminar").show();
	$("#btnNuevo").hide();
}

function deshabilitarBotonesEdicion() {
	$("#btnGuardar").hide();
	$("#btnCancelar").hide();
	$("#btnEliminar").hide();
	$("#btnNuevo").show();
}

function habilitarBotonesNuevo() {
	$("#btnGuardar").show();
	$("#btnCancelar").show();
	$("#btnNuevo").hide();
}

function habilitarBotonesSubConsulta() {
	$("#btnGuardar").hide();
	$("#btnCancelar").hide();
	$("#btnEliminar").hide();
	$("#btnCancelar").show();
	$("#btnNuevo").show();
}

function IsNumeric(e) {
	var keyCode = e.which ? e.which : e.keyCode; 
	var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
	// document.getElementById("error").style.display = ret ? "none" : "inline";
	return ret;
}
function IsNumericRestriccion(e) {
	var keyCode = e.which ? e.which : e.keyCode; 
	if(e.srcElement.value.length>11){
		e.srcElement.value=e.srcElement.value.substr(0,11);
		alert("Ingresar RUC de 11 dígitos");
		return false;
	}
}
function seleccionaEmpresa(idCompany, companyName,companyRuc) {
	createSession("gestopcompanyid", idCompany);
	createSession("gestopcompanyname", companyName);
	createSession("gestopcompanyruc", companyRuc);
	if (sessionStorage.getItem("submodulo") == null) {
		location.reload();
	} else {
		document.location.replace("main.html");
		sessionStorage.removeItem("submodulo");
		sessionStorage.removeItem("modulo");
	}

}

function createSession(key, token) {
	sessionStorage.setItem(key, token);
}
function getSession(key){
	return sessionStorage.getItem(key);
	
}

function removeAllSession(){
	sessionStorage.clear();
}

function cargarDatosVarios() {
	var dataParam = {
		userId : sessionStorage.getItem("gestopUi"),
		sessionId : sessionStorage.getItem("gestopsessionId")
	};

	callAjaxPost(URL + '/iperc/listvarios', dataParam, procesarDatosVarios);
}

function procesarDatosVarios(data) {

	switch (data.CODE_RESPONSE) {
	case "05":
		var clasifs = data.listaClasificaciones;
		var pels = data.mapPeligros;
		var riesgos = data.mapRiesgos;

		createSession("clasifs", JSON.stringify(clasifs));
		createSession("peligros", JSON.stringify(pels));
		createSession("riesgos", JSON.stringify(riesgos));

		break;
	default:
		alert("Ocurrió un error al traer las empresas!");
	}

}

function crearSelectOneMenuImagen(idSel, methodJs, listOption, idSelected, value,
		nameValue,selectedValue, nameOption) {
	var nameDefault = nameOption == null ? "Escoja una opci&oacute;n" : nameOption;
	var selectOneBox = "<select id='" + idSel + "'  "
			+ "><option value='-1'>" + nameDefault
			+ "</option>";
	if (listOption) {
		for (index = 0; index < listOption.length; index++) {
			var dataclass="";
			if(listOption[index][selectedValue]>0 ){
				dataclass="data-class='marcaCheck'";
			}
			
			selectOneBox = selectOneBox + "<option  "+dataclass+" value='"
					+ listOption[index][value] + "' ";
			if (idSelected == listOption[index][value]) {
				selectOneBox = selectOneBox + "selected>"
						+ listOption[index][nameValue] + "</option>";
			} else {
				selectOneBox = selectOneBox + ">"
						+ listOption[index][nameValue] + "</option>";
			}
		}
	}

	selectOneBox = selectOneBox + "</select>";
	return selectOneBox;
}
function crearSelectOneMenuMovil(idSel, methodJs, listOption, idSelected, 
		 nameOption, value,nameValue,value2,opcionId){
	var nameDefault = nameOption == null ? "Escoja una opci&oacute;n" : nameOption;
	
	var divOneBox = "<div class='listOpcionFiltroGosst' id='" + idSel + "' onclick='"+methodJs+"'>" +  
"<div class='tituloFiltro'>"+nameDefault +" (Todos) </div>";
		if (listOption) {
			for (index = 0; index < listOption.length; index++) {
				var add="";
				if(value2){
					 add="-"+listOption[index][value2]
				}
				divOneBox = divOneBox + "<div class='opcionFiltroGosst' id='divOpcion"
						+ listOption[index][value] + ""+add+"' >" 
						+ "<div class='checkbox checkbox-filtro checkbox-inline'>"
                        +"<input id='"+opcionId
						+ listOption[index][value] + ""+add+"' type='checkbox' checked>"
                        +"<label for='"+opcionId
						+ listOption[index][value] + ""+add+"'> "
                         +  listOption[index][nameValue] 
                        +"   </label>"
                        +" </div>" 
						 + "</div>";
				 
			}
		} 
		divOneBox = divOneBox + "</div>" ;
return divOneBox;
}
function crearSelectOneMenu(idSel, methodJs, listOption, idSelected, value,
		nameValue, nameOption) {

	var nameDefault = nameOption == null ? "Escoja una opci&oacute;n" : nameOption;
	
		var selectOneBox = "<select id='" + idSel + "' class='form-control' "
		+ "onchange='" + methodJs + "'><option value='-1'>" + nameDefault
		+ "</option>";
			if (listOption) {
				for (index = 0; index < listOption.length; index++) {
					selectOneBox = selectOneBox + "<option value='"
							+ listOption[index][value] + "' ";
					if (idSelected == listOption[index][value]) {
						selectOneBox = selectOneBox + "selected>"
								+ listOption[index][nameValue] + "</option>";
					} else {
						selectOneBox = selectOneBox + ">"
								+ listOption[index][nameValue] + "</option>";
					}
				}
			} 
	selectOneBox = selectOneBox + "</select>" ;
	return selectOneBox;
}

function crearSelectOneMenuY(idSel, methodJs, listOption, idSelected, value,value2,
		nameValue, nameOption) {
	var nameDefault = nameOption == null ? "Escoja una opci&oacute;n" : nameOption;
	var selectOneBox = "<select id='" + idSel + "' class='form-control' "
			+ "onchange='" + methodJs + "'><option value='-1'>" + nameDefault
			+ "</option>";
	if (listOption) {
		for (index = 0; index < listOption.length; index++) {
			selectOneBox = selectOneBox + "<option value='"
					+ listOption[index][value] +"-"+listOption[index][value2] +"' ";
			var nuevo=idSelected; 
			if (nuevo == listOption[index][value]) {
				selectOneBox = selectOneBox + "selected>"
						+ listOption[index][nameValue] + "</option>";
			} else {
				selectOneBox = selectOneBox + ">"
						+ listOption[index][nameValue] + "</option>";
			}
		}
	}

	selectOneBox = selectOneBox + "</select>";
	return selectOneBox;
}
function crearSelectOneMenuOblig(idSel, methodJs, listOption, idSelected, value,
		nameValue) {

		var selectOneBox = "<select id='" + idSel + "' class='form-control' "
		+ "onchange='" + methodJs + "'>";
			if (listOption) {
				for (index = 0; index < listOption.length; index++) {
					selectOneBox = selectOneBox + "<option value='"
							+ listOption[index][value] + "' ";
					if (idSelected == listOption[index][value]) {
						selectOneBox = selectOneBox + "selected>"
								+ listOption[index][nameValue] + "</option>";
					} else {
						selectOneBox = selectOneBox + ">"
								+ listOption[index][nameValue] + "</option>";
					}
				}
			} 
	selectOneBox = selectOneBox + "</select>" ;
	return selectOneBox;
}
function asignarValorLista(listOption, idSelected, value, nameValue) {
	
	var optionSelected = "";
	if (listOption) {
		for (index = 0; index < listOption.length; index++) {
		
			if (idSelected == listOption[index][value]) {
				optionSelected=listOption[index][nameValue] ;
			} 
		}
	}

	
	return optionSelected;
}

function listarStringsDesdeArray(lista, atributo) {
	var arrayString = [];
	if (lista) {
		for (index = 0; index < lista.length; index++) {
			arrayString.push(lista[index][atributo]);
			lista[index].filtroActivado=true;
		}
	}
	return arrayString;
}

function listarStringsDesdeArrayY(lista, atributo, restriccion,restriccionId) {
	var arrayString = [];
	if (lista) {
		for (index = 0; index < lista.length; index++) {
			if(lista[index][restriccion]==restriccionId){
			arrayString.push(lista[index][atributo]);
			}
			}
			
	}
	return arrayString;
}

function irHacia(indicadorPagina, opcional1, opcional2) {
	switch (indicadorPagina) {
	case 1:
		createSession("ayudaId", 1);
		createSession("modulo", "Empresa");
		sessionStorage.removeItem("submodulo");
		document.location.href = 'empresa.html';
		
		break;
	case 2:
		createSession("ayudaId", 2);
		createSession("modulo", "Despliegue Organizacional");
		sessionStorage.removeItem("submodulo");
		document.location.href = 'mdf.html';
		
		break;
	case 3:
		createSession("ayudaId",3);
		createSession("modulo", "IPERC");
		sessionStorage.removeItem("submodulo");
		document.location.href = 'iperc.html';
		
		break;
	case 4:
	
		break;
	case 5:
		createSession("ayudaId",4);
		createSession("modulo", "Recursos");
		createSession("submodulo", "Formaciones");
		$("#moduloSelected").html("Recursos-"+getSession("submodulo"));
		document.location.href = 'capacitacion.html';
		break;
	case 6:
		createSession("modulo", "Recursos");
		createSession("submodulo", "Ex&aacute;menes M&eacute;dicos");
		document.location.href = 'examenmedico.html';
		$("#moduloSelected").html("Recursos-"+getSession("submodulo"));
		createSession("ayudaId",5);
		break;
	case 7:
		createSession("ayudaId",6);
		createSession("modulo", "Recursos");
		createSession("submodulo", "Equipos de Seguridad");
		$("#moduloSelected").html("Recursos-"+getSession("submodulo"));
		document.location.href = 'equiposeguridad.html';
		
		break;
	case 8:
		createSession("modulo", "Recursos");
		createSession("submodulo", "Procedimientos de Seguridad");
		$("#moduloSelected").html("Recursos-"+getSession("submodulo"));
		document.location.href = 'procedimientoseguridad.html';
		createSession("ayudaId",7);
		break;
	case 9:
		$("#subMenu").remove();
		sessionStorage.removeItem("submodulo");
		createSession("ayudaId",8);
		break;
	case 10:
		createSession("modulo", "Asignaci&oacute;n");
		createSession("submodulo", opcional2);
		document.location.href = 'puestotrabajo.html?mdfId=' + opcional1;
		
		createSession("ayudaId",8);
		break;
	case 11:
		$("#subMenu").remove();
		sessionStorage.removeItem("submodulo");
		
		createSession("ayudaId",9);
		break;
	case 12:
		createSession("modulo", "Calendario");
		sessionStorage.removeItem("submodulo");
		document.location.href = 'main.html';
		
		break;
	case 13:
		createSession("modulo", "Diagn&oacute;stico");
		sessionStorage.removeItem("submodulo");
		document.location.href = 'auditoria.html';
		createSession("ayudaId",10);
		break;
	case 14:
		createSession("modulo", "Inspecci&oacute;n");
		sessionStorage.removeItem("submodulo");
		document.location.href = 'inspeccion.html';
		createSession("ayudaId",10);
		break;
	case 15:
		$("#subMenu").remove();
		sessionStorage.removeItem("submodulo");
		createSession("ayudaId",11);
		break;
	case 16:
		createSession("modulo", "A/C Subest&aacute;ndar");
		createSession("submodulo", opcional2);
		document.location.href = 'acinseguros.html?mdfId='+ opcional1;
		createSession("ayudaId",11);
		break;
	case 17:
		createSession("modulo", "Accidentes");
		sessionStorage.removeItem("submodulo");
		document.location.href = 'accidentes.html';
		createSession("ayudaId",12);
		break;
	case 18:
		createSession("modulo", "Acci&oacute;n de mejora");
		sessionStorage.removeItem("submodulo");
		document.location.href = 'accionmejora.html';
		createSession("ayudaId",13);
		break;
	case 19:
		createSession("modulo", "Herramientas de Análisis");
		sessionStorage.removeItem("submodulo");
		document.location.href = 'estadistica.html';
		createSession("ayudaId",15);
		break;
	case 20:
		createSession("modulo", "Empresa");
		sessionStorage.removeItem("submodulo");
		document.location.href = 'perfiles.html';
	//	createSession("ayudaId",16);
		break;
	case 21:
		createSession("ayudaId",4);
		createSession("modulo", "Recursos");
		createSession("submodulo", "Capacitación Online");
		$("#moduloSelected").html("Recursos-"+getSession("submodulo"));
		document.location.href = 'capacitacion_online.html';
		break;
	case 30:
		createSession("modulo", "Trabajador");
		createSession("submodulo", opcional2);
		document.location.href = 'trabajador.html?mdfId='+ opcional1;
		
		break;
	default:
		break;
	}
	
}

/**
 * 
 */
var listMdf ;
 function crearSubMenu(idModulo, param1, param3) {
	var mdfId=null;
	if(sessionStorage.getItem("unidadGosstId")>0){
		mdfId=sessionStorage.getItem("unidadGosstId");
	}
	
	var dataParam = {
			userId : sessionStorage.getItem("gestopUi"),
			sessionId : sessionStorage.getItem("gestopsessionId"),
			companyId : sessionStorage.getItem("gestopcompanyid"),
			mdfId:mdfId
		}; 
		callAjaxPostNoLoad(
				URL + '/mdf/listbox',
				dataParam,
				function(data) {
				 listMdf = data.list; 
				 if(listMdf.length==1){
					 createSession("unidadGosstId",listMdf[0].matrixId);
					 createSession("unidadGosstNombre",listMdf[0].matrixNameSimple); 
				 }
					$("#listaModulo"+idModulo+" ul").remove();
					$("#menuFigureti .subItem"+idModulo).remove();
						$("#listaModulo"+idModulo).append(
								"<ul class='nav-pills nav-stacked' id='subUnidades" +idModulo+"'"+
								"style='list-style-type:none;margin-left:20px;display:none'>");
						for (index = 0; index < listMdf.length; index++) {
							$("#listaModulo"+idModulo+" ul").append(
									" <li><a href='#' onclick='javascript:irHacia( "+param1+","
											+ listMdf[index].matrixId + ", &quot;"
											+ listMdf[index].matrixNameSimple
											+ "&quot;)' ><span class='fa-stack fa-lg pull-left '><i  class='fa fa-industry fa-stack-1x' aria-hidden='true' ></i></span>"
											+ listMdf[index].matrixNameSimple
											+ "</a></li>");
							$("#menuFigureti").append("<div class='subContainerFig moduloAccesible  col-xs-6 col-ss-3 col-sm-3 col-md-3 col-lg-3 subItem"+idModulo+"' title='"+listMdf[index].matrixNameSimple+"' id='unidadId"+index+"' style='display:none'>" +
									"<div class='subItemFig' >" +
									"<img src='../imagenes/movil/unidad.png'>"+
									"	<span class='fa-stack fa-lg'>"+
									
										"  <i class='fa fa-industry "+" fa-2x'>" +
												"<br> "+"</i>"
											+"</span>"+
									"</div>");
							$(".subContainerFig").tooltip({
								placement:"bottom"
							})
							
						}
					$("#listaModulo"+idModulo).append("</ul>");
						
					/***Inicio de funcion de items****/
					$(".subContainerFig.moduloAccesible").on("click",function(ev){
										var idAux=$(this).prop("id").split("unidadId");
									 indexUnidadSeleccionada=parseInt(idAux[1]);
										$("#textoDescrip").remove();
										$("#btnIrModulo").remove();
										$(".subContainerFig").children().removeClass("itemNoFig").addClass("subItemFig");
										
										
											$(this).children().removeClass("subItemFig").addClass("itemNoFig");
											
											var btnIrModulo="<div id='btnIrModulo' class='containerFig col-xs-6 col-ss-3 col-sm-3 col-md-3 col-lg-3'>" +
											"<div class='subItemFig'> "
											
											+"<i class='"+"fa fa-arrow-circle-right"+" fa-3x'></i>"
											+"</div>" +
											"</div>";
											$("#menuFigureti").append(btnIrModulo);
					
											
											$("#btnIrModulo").on("click",function(){
												var idLinkAux;
												switch(moduloSeleccionadoMenu){
												case 9:
													idLinkAux=10;
													break;
												case 11:
													idLinkAux=30
												break;
												case 15:
													idLinkAux=16
													break;
													default:
														idLinkAux=moduloSeleccionadoMenu
														break;
												}
												
												
												if(typeof(listMdf[indexUnidadSeleccionada])== 'undefined'){
													irHacia(idLinkAux);
												}else{
													irHacia(idLinkAux,listMdf[indexUnidadSeleccionada].matrixId,listMdf[indexUnidadSeleccionada].matrixNameSimple);

												}
												
											});
									
										
										
									});
								
									/****Fin de funcion item*****/

				});
		
		
	
	
}
 function indicadorUnidades(data){
	 if(data.CODE_RESPONSE=="05"){
			var indicadorFormacion=	0;
			var indicadorAcumulado=0;
					var indicadorEntregaEpp=	0;
					var indicadorDisponibilidad=	0;
					var indicadorParticipacion=	0;
					var indicadorExamenMedico=	0;
			for(index=0;index<data.indFormacion.length;index++){
				indicadorAcumulado=data.indFormacion[index].cumplimientoFormacion+indicadorAcumulado;
				var size=data.indFormacion.length ;
				(index+1==size ? indicadorFormacion=indicadorAcumulado/size : 0)
			}
			indicadorAcumulado=1;
			for(index=0;index<data.indEpp.length;index++){
				var entregadosVigentes= data.indEpp[index].entregasLastYear;
				var asignados=data.indEpp[index].eppAsignados;
				if( asignados>0){
					indicadorAcumulado=(entregadosVigentes/asignados)+indicadorAcumulado;
				}else{
					indicadorAcumulado=1+indicadorAcumulado;
				}
				
				var size=data.indEpp.length ;
				(index+1==size ? indicadorEntrega=indicadorAcumulado/size : 0)
			}
			indicadorAcumulado=0;
			for(index=0;index<data.indAcc.length;index++){
				indicadorAcumulado=data.indAcc[index].indicadorDiasDescanso+indicadorAcumulado;
				var size=data.indAcc.length ;
				(index+1==size ? indicadorDisponibilidad=indicadorAcumulado/size : 0)
			}
			indicadorAcumulado=0;
			for(index=0;index<data.indEvento.length;index++){
				indicadorAcumulado=data.indEvento[index].numEventosReportados+indicadorAcumulado;
				var size=data.indEvento.length ;
				(index+1==size ? indicadorParticipacion=indicadorAcumulado/size : 0)
			}
			indicadorAcumulado=0;
			for(index=0;index<data.indExam.length;index++){
				indicadorAcumulado=data.indExam[index].vigenciaMedicaPuntaje+indicadorAcumulado;
				var size=data.indExam.length ;
				(index+1==size ? indicadorExamenMedico=indicadorAcumulado/size : 0)
			}

					 evalRealizadas=data.indIperc[0].evalRealizadas;
					 evalRiesgosas=data.indIperc[0].evalRiesgosas;
					 controlesPorImplementar=data.indIpercGeneral.controlesPorImplementar;
					 controlesCompletados=data.indIpercGeneral.controlesCompletados;
					 controlesRetrasados=data.indIpercGeneral.controlesRetrasados;
					//////
					 numAccidentes=data.indAccidenteGeneral.numAccidentes;
					  numIncidentes=data.indAccidenteGeneral.numIncidentes;
					  numEnfermedades=data.indAccidenteGeneral.numEnfermedades;
					  numAccidentesLeves=data.indAccidenteGeneral.numAccidentesLeves;
					  numAccidentesFatales=data.indAccidenteGeneral.numAccidentesFatales;
					  numAccidentesInc=data.indAccidenteGeneral.numAccidentesInc;
					  numAccidentesInc1=data.indAccidenteGeneral.numAccidentesInc1;
					  numAccidentesInc2=data.indAccidenteGeneral.numAccidentesInc2;
					  numAccidentesInc3=data.indAccidenteGeneral.numAccidentesInc3;
					  numAccidentesInc4=data.indAccidenteGeneral.numAccidentesInc4;
					  numEnfermedades1=data.indAccidenteGeneral.numEnfermedades1;
					  numEnfermedades2=data.indAccidenteGeneral.numEnfermedades2;
					  numDiasDescanso=data.indAccidenteGeneral.numDiasDescanso;
					  numDiasSinAccidente=data.indAccidenteGeneral.numDiasSinAccidente;
					  /////
					  numTrabajadoresActivos=data.indAptitudGeneral.numTrabajadoresActivos;
					  numTrabajadoresAptos=data.indAptitudGeneral.numTrabajadoresAptos;
					  numTrabajadoresRestricciones=data.indAptitudGeneral.numTrabajadoresRestricciones;
					  numTrabajadoresNoAptos=data.indAptitudGeneral.numTrabajadoresNoAptos;
					  //////
					  form4plus=data.indFormacionGeneral.form4plus;
					  form3=data.indFormacionGeneral.form3;
					  form2=data.indFormacionGeneral.form2;
					  form1=data.indFormacionGeneral.form1;
					  form0=data.indFormacionGeneral.form0;
					  horasForm4plus=data.indFormacionGeneral.horasForm4plus;
					  horasForm3=data.indFormacionGeneral.horasForm3;
					  horasForm2=data.indFormacionGeneral.horasForm2;
					  horasForm1=data.indFormacionGeneral.horasForm1;
					  //////
					var indicadorIperc=(evalRealizadas==0 ? 1:(evalRealizadas-evalRiesgosas)/evalRealizadas)	;
					
					var indicadorMinimoFormacion=	data.indicadoresSeguridad.indicadorMinimoFormacion;
					var indicadorMinimoEntregaEpp=	data.indicadoresSeguridad.indicadorMinimoEntregaEpp;
					var indicadorMinimoDisponibilidad=	data.indicadoresSeguridad.indicadorMinimoDisponibilidad;
					var indicadorMinimoParticipacion=	data.indicadoresSeguridad.indicadorMinimoParticipacion;
					var indicadorMinimoExamenMedico=	data.indicadoresSeguridad.indicadorMinimoExamenMedico;
					var indicadorMinimoIperc=	data.indicadoresSeguridad.indicadorMinimoIperc;
				
					$('#subDivResumen3').highcharts({

					    chart: {
					        polar: true,
					        type: 'line'
					    },

					    title: {
					        text: 'Indicadores propuestos de la empresa',
					        x: -80
					    },

					    pane: {
					        size: '80%'
					    },

					    xAxis: {
					        categories: ['Vigencia Promedio de Formacion', 
					                     'Vigencia Promedio de EPP', 
					                     'Disponibilidad Laboral', 
					                     'Participacion en el SGSST',
					                'Vigencia Examen Médico',
					                'Porcentaje de riesgo significativo'],
					        tickmarkPlacement: 'on',
					        lineWidth: 0
					    },

					    yAxis: {
					        gridLineInterpolation: 'polygon',
					        lineWidth: 0,
					        min: 0,tickInterval:0.25,
					        max:1
					    },

					    tooltip: {
					        shared: true,
					        pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y}</b><br/>',
					        valueDecimals: 2
					    },

					    legend: {
					        align: 'center',
					        verticalAlign: 'bottom',
					        y: 70,
					        layout: 'vertical'
					    },
					    colors:["#9BBB59","#4F81BD"],
					    series: [{
					        name: 'Promedio Actual',
					        data: [indicadorFormacion, 
					               indicadorEntrega, 
					               indicadorDisponibilidad, 
					               indicadorParticipacion/indicadorMinimoParticipacion, 
					               indicadorExamenMedico,
					               indicadorIperc],
					        pointPlacement: 'on'
					    }, {
					        name: 'Meta',
					        data: [indicadorMinimoFormacion, 
					               indicadorMinimoEntregaEpp, 
					               indicadorMinimoDisponibilidad, 
					               1, 
					               indicadorMinimoExamenMedico,
					               indicadorMinimoIperc],
					        pointPlacement: 'on'
					    }]


					});

					
				}else{
					alert("No se pudo traer los indicadores de la empresa")
				}
						

						
			
	 
 }
 var requisitoOculto=false;
 function verGraficoResumen(moduloId,unidadId){
	 if(irDirectamenteModulo){
		 irHacia(parseInt(moduloId),unidadId);
	 }
	 var buttonIrModulo="<a class='btn btn-info' id='accessModulo' style='float:right;margin-right:20px ' href='#'>"+
		"<i class='fa fa-paper-plane'></i> </a>";
	 buttonIrModulo="";
	 switch(parseInt(moduloId)){
		
	 case 20:
		 buttonIrModulo=""
		 break;
	 
	 case 4:
		 buttonIrModulo=""
		 
		 break;
	 case 9:
		 buttonIrModulo=""
		 break;
case 15:
	buttonIrModulo=""
		 
		 break;
	 case 11:
		 buttonIrModulo=""
		 break;
	 default:
		
		 break;
	 }
	 var URLGraficaMenu="";
	 var dataParam ={}; 
	 var dataParamAux ={};
	 var functionGrafico="";
	 
	
	 
	 $("#subDivResumen1")
		.html("<strong >Requisitos" +
				"<span style='float:left;margin-right:20px' id='requisitoToggle'>" +
					"<i class='fa fa-arrow-up' aria-hidden='true'></i></span></strong>" +
				"<span id='requisitoDiv'></span>"+buttonIrModulo);
	 $("#accessModulo").on("click",function(){
		 irHacia(parseInt(moduloId),unidadId);
	 });
		$("#requisitoToggle").on("click",function(){
			var estilo={};
			if(requisitoOculto){
				$(this).html("<i class='fa fa-arrow-up' aria-hidden='true'></i>");
				estilo={
					"height":"300px","z-index":"2000"
				}
			}else{
				estilo={
						"height":"30px",
							"z-index":"-1"
					}
				$(this).html("<i class='fa fa-arrow-down' aria-hidden='true'></i>");	
			}
			$("#subDivResumen1").css(estilo);
			
			requisitoOculto=!requisitoOculto;
		});
		$("#subDivResumen2").html("");
	 for(var index=0;index<listaModulosPermitidos.length;index++){
		 var subMenuAux=listaModulosPermitidos[index].subMenus;
			var countObligatorios=0,countObligatoriosCumplidos=0;
			for (var index2=0;index2<subMenuAux.length;index2++){
			
				if(subMenuAux[index2].menuOpcionId==moduloId){
					var subRequisitoAux=subMenuAux[index2].restricciones;
					for(index3=0;index3<subRequisitoAux.length;index3++){
						var iconoRequisito,iconoColor,cumpleRequisito;
						//realizar una funcion para verificar el requisito
						var requisitoObject={
								id:subRequisitoAux[index3].id,
								companyId:sessionStorage.getItem("gestopcompanyid"),
								isObligatorio:subRequisitoAux[index3].isObligatorio,
								descripcion:subRequisitoAux[index3].descripcion,
								actividadManualId:subRequisitoAux[index3].actividadManualId
						};
						 
						var llamandoServidor=false;
						callAjaxPost(URL+"/empresa/requisito/modulo", requisitoObject, function(data){	
							if(data.CODE_RESPONSE=="05"){
								llamandoServidor=true;
								cumpleRequisito=data.respuesta;
								if(data.isObligatorio==1 && cumpleRequisito !=0 && cumpleRequisito!=null){
									countObligatoriosCumplidos=countObligatoriosCumplidos+1;
								}
								if(cumpleRequisito){
								iconoColor="#9BBB59"
									iconoRequisito="check"
								}else{
									iconoColor="#C0504D"
									iconoRequisito="times"
								}
							var textoOpcion="<span style='color:white;font-weight:bold'>(opcional)</span>";
								if(data.isObligatorio==1 ){
									textoOpcion=" ";
									countObligatorios=countObligatorios+1;
								}
							 $("#subDivResumen1").append(
										"<span class='fa-stack fa-sm'>"
										+"<i class='fa fa-square-o fa-stack-2x' ></i>"
										  +"  <i class='fa fa-"+iconoRequisito+" fa-stack-1x fa-inverse' style='color:"+iconoColor+"'></i>"
										+"</span>"
										+data.descripcion+" "
										+textoOpcion+"<i class='fa fa-question fa-2x' onclick='irActividadManual("+data.actividadManualId+")'></i><br>");
							 $("#subDivResumen1 #requisitoDiv")
								.html("<strong>"+countObligatoriosCumplidos+"/"+countObligatorios+"</strong><br><br>");
							
							}else{
								console.log("nop")
							};
						},function(){
							 console.log("no es");
							if(llamandoServidor){
								return false;
							}
							if(isMobile.any()){
								console.log("no es necesario");
								return false;
							}
						})
						
					}
					
					
					index2=index2+50;
				}
			}
			if(listaModulosPermitidos[index].menuOpcionId==moduloId){
				var subRequisitoAux=listaModulosPermitidos[index].restricciones;
				for(var index3=0;index3<subRequisitoAux.length;index3++){
					var iconoRequisito,iconoColor,cumpleRequisito;
					var requisitoObject={
							id:subRequisitoAux[index3].id,
							companyId:sessionStorage.getItem("gestopcompanyid"),
							isObligatorio:subRequisitoAux[index3].isObligatorio,
							descripcion:subRequisitoAux[index3].descripcion,
							actividadManualId:subRequisitoAux[index3].actividadManualId
					}
					//realizar una funcion para verificar el requisito
					callAjaxPostNoLoad(	URL+"/empresa/requisito/modulo", requisitoObject, function(data){
						if(data.CODE_RESPONSE=="05"){
							cumpleRequisito=data.respuesta;
					if(data.isObligatorio==1 && cumpleRequisito && cumpleRequisito!=null ){
						countObligatoriosCumplidos=countObligatoriosCumplidos+1;
					}
					if(cumpleRequisito){
					iconoColor="#9BBB59"
						iconoRequisito="check"
					}else{
						iconoColor="#C0504D"
						iconoRequisito="times"
					}
				var textoOpcion="<span style='color:white;font-weight:bold'>(opcional)</span>";
					if(data.isObligatorio==1 ){
						textoOpcion=" ";
						countObligatorios=countObligatorios+1;
					}
				 $("#subDivResumen1").append(
							"<span class='fa-stack fa-sm'>"
							+"<i class='fa fa-square-o fa-stack-2x' ></i>"
							  +"  <i class='fa fa-"+iconoRequisito+" fa-stack-1x fa-inverse' style='color:"+iconoColor+"'></i>"
							+"</span>"
							+data.descripcion+" "
							+textoOpcion+"<i class='fa fa-question fa-2x' onclick='irActividadManual("+data.actividadManualId+")'></i><br>");
						}
				 $("#subDivResumen1 #requisitoDiv")
							.html("<strong>"+countObligatoriosCumplidos+"/"+countObligatorios+"</strong><br><br>");
							
					},function(){
						 
						if(llamandoServidor){
							return false;
						}
						if(isMobile.any()){
							 
							return false;
						}
					});
				}
				
				
				index=index+50;
			}
			
			
		}
	
	
	
	 
	 
		
		
 }
 function irActividadManual(actId){

	 var subUrl=(actId==null?"":"?actividadManualId="+actId);
	 var url=URL+"web/pages/manual/manual.html"+subUrl;
	    window.open(url, '_blank');
	      return false;
 }
 function procesarDescargables(data){
	
		
	 switch(data.idModulo){
		case 1:
			$("#subDivResumen2").append("<div id='indFormEmp' class='indicadorEmpresa'>" +
					"<div style='width:20%;float:left'></div>" +
					"<div class='barraIndicador'></div>" +
					"<div  class='mensajeIndicador'></div>" +
					"</div>");
			$("#indFormEmp div:eq(0)").append(
					"Extensión de formación"
					);
			$("#indFormEmp div:eq(1)").append(
					"<div class='progress-container progress-success-container'>"
					+"<div id='myBar' class='progress-realizado progress-success-realizado ' style='width:75%'>"
					  +"<div class='w3-center w3-text-white'>75%</div>"
					    +"	  </div>"
				+"	</div>		"
			);
			$("#indFormEmp .mensajeIndicador").append(
					"<div class='msg-null'>" +
					"No hay trabajadores registrados<br>" +
					"No hay unidades registradas</div>"
					);
			
			$("#subDivResumen2").append("<div id='indVigEpp' class='indicadorEmpresa'>" +
					"<div style='width:20%;float:left'></div>" +
					"<div class='barraIndicador'></div>" +
					"<div class='mensajeIndicador'></div>" +
					"</div>");
			$("#indVigEpp div:eq(0)").append(
					"Vigencia de EPP's"
					);
			$("#indVigEpp div:eq(1)").append(
					"<div class='progress-container progress-success-container'>"
					+"<div id='myBar' class='progress-realizado progress-success-realizado ' style='width:35%'>"
					  +"<div class='w3-center w3-text-white'>35%</div>"
					    +"	  </div>"
				+"	</div>		"
			);
			$("#indVigEpp .mensajeIndicador").append(
					"<div class='msg-warning'>Carefull CarefullCarefullCarefull  CarefullCarefull Carefull operation message. Carefull operation message. Carefull operation message</div>"
					);
			
			$("#subDivResumen2").append("<div id='indVigMedica' class='indicadorEmpresa'>" +
					"<div style='width:20%;float:left'></div>" +
					"<div class='barraIndicador'></div>" +
					"<div  class='mensajeIndicador'></div>" +
					"</div>");
			$("#indVigMedica div:eq(0)").append(
					"Vigencia Médica"
					);
			$("#indVigMedica div:eq(1)").append(
					"<div class='progress-container progress-success-container'>"
					+"<div id='myBar' class='progress-realizado progress-success-realizado ' style='width:95%'>"
					  +"<div class='w3-center w3-text-white'>95%</div>"
					    +"	  </div>"
				+"	</div>		"
			);
			$("#indVigMedica .mensajeIndicador").append(
					"<div class='msg-success'>SuccessfulSuccessfulSuccessfulSuccessful operation message</div>"
					);
			
			$("#subDivResumen2").append("<div id='indRiesgo' class='indicadorEmpresa'>" +
					"<div style='width:20%;float:left'></div>" +
					"<div class='barraIndicador'></div>" +
					"<div  class='mensajeIndicador'></div>" +
					"</div>");
			$("#indRiesgo div:eq(0)").append(
					"Riesgos Significativos Actuales"
					);
			$("#indRiesgo div:eq(1)").append(
					"<div class='progress-container progress-success-container'>"
					+"<div id='myBar' class='progress-realizado progress-success-realizado ' style='width:45%'>"
					  +"<div class='w3-center w3-text-white'>45%</div>"
					    +"	  </div>"
				+"	</div>		"
			);
			$("#indRiesgo .mensajeIndicador").append(
					"<div class='msg-warning'> CarefullCarefullCarefullCarefullCarefull operation message</div>"
					);
			
			$("#subDivResumen2").append("<div id='indDisponibilidad' class='indicadorEmpresa'>" +
					"<div style='width:20%;float:left'></div>" +
					"<div class='barraIndicador'></div>" +
					"<div  class='mensajeIndicador'></div>" +
					"</div>");
			$("#indDisponibilidad div:eq(0)").append(
					"Disponibilidad Laboral"
					);
			$("#indDisponibilidad div:eq(1)").append(
					"<div class='progress-container progress-success-container'>"
					+"<div id='myBar' class='progress-realizado progress-success-realizado ' style='width:85%'>"
					  +"<div class='w3-center w3-text-white'>85%</div>"
					    +"	  </div>"
				+"	</div>		"
			);
			$("#indDisponibilidad .mensajeIndicador").append(
					"<div class='msg-success'> Successful SuccessfulSuccessful Successful  Successful SuccessfulSuccessful " +
					"Successful Successful SuccessfulSuccessful operation message <i class='fa fa-question-circle' aria-hidden='true'></i></div>"
					);
			
			$("#subDivResumen2").append("<div id='indPartSST' class='indicadorEmpresa'>" +
					"<div style='width:20%;float:left'></div>" +
					"<div class='barraIndicador'></div>" +
					"<div class='mensajeIndicador'></div>" +
					"</div>");
			$("#indPartSST div:eq(0)").append(
					"Participación SGSST"
					);
			$("#indPartSST div:eq(1)").append(
					"<div class='progress-container progress-success-container'>"
					+"<div id='myBar' class='progress-realizado progress-success-realizado ' style='width:15%'>"
					  +"<div class='w3-center w3-text-white'>15%</div>"
					    +"	  </div>"
				+"	</div>		"
			);
			$("#indPartSST .mensajeIndicador").append(
					"<div class='msg-error'> R </div>"
					);
$(".mensajeIndicador div").append("<i class='fa fa-question-circle' aria-hidden='true'></i>")
			var estiloCentro={"margin-top":($("#indFormEmp").height()/2)-10+"px"};
			var estiloCentroMensaje={"margin-top":($("#indFormEmp").height()/2)-25+"px"};
			$("#indVigMedica div:eq(0)").css(estiloCentro);
			$("#indVigEpp div:eq(0)").css(estiloCentro);
			$("#indVigMedica div:eq(1)").css(estiloCentro);
			$("#indVigEpp div:eq(1)").css(estiloCentro);
			$("#indVigMedica .mensajeIndicador").css(estiloCentroMensaje);
			$("#indVigEpp .mensajeIndicador").css(estiloCentroMensaje);
			
			$("#indFormEmp div:eq(0)").css(estiloCentro);
			$("#indRiesgo div:eq(0)").css(estiloCentro);
			$("#indFormEmp div:eq(1)").css(estiloCentro);
			$("#indRiesgo div:eq(1)").css(estiloCentro);
			$("#indFormEmp .mensajeIndicador").css(estiloCentroMensaje);
			$("#indRiesgo .mensajeIndicador").css(estiloCentroMensaje);
			
			$("#indDisponibilidad div:eq(0)").css(estiloCentro);
			$("#indPartSST div:eq(0)").css(estiloCentro);
			$("#indDisponibilidad div:eq(1)").css(estiloCentro);
			$("#indPartSST div:eq(1)").css(estiloCentro);
			$("#indDisponibilidad .mensajeIndicador").css(estiloCentroMensaje);
			$("#indPartSST .mensajeIndicador").css(estiloCentroMensaje);
			
			break;
	 }
	 
	 if(data.CODE_RESPONSE=="05"){
		var descargables= data.descargables;
		 $("#subDivResumen1").show();
		  $("#subDivResumen2").show();
		  $("#subDivResumen3").show();
		
		switch(data.idModulo){
		case 2:
			$("#subDivResumen2").append("<table class='table table-striped table-bordered table-hover'>" +
					"<thead><tr ><td class='tb-acc'>Unidad</td><td class='tb-acc'>Descargable</td></tr></thead>" +
					"<tbody></tbody></table>");
			for(index=0;index<descargables.length;index++){
				var mdfId=descargables[index].id;
				$("#subDivResumen2 table tbody").append(
						"<tr><td >"+descargables[index].nombre +"</td>" +
						"<td ><a id='m"+mdfId+"'><i class='fa fa-file-word-o' " +
						"aria-hidden='true'></i>"+
								"</td></tr>" 
									);	
				$("#m"+mdfId).on("click",function(){
				var idAux= $(this).prop("id").split("m")
				window.open(URL
						+ "/mdf/reporteanual?mdfId="
						+idAux[1]
						+ "&companyId="
						+ sessionStorage.getItem("gestopcompanyid"), '_blank');	

			})
			}
			
			break;
		case 5:
			$("#subDivResumen2").append("<table class='table table-striped table-bordered table-hover'>" +
					"<thead><tr ><td class='tb-acc'>Programaci&oacute;n</td><td class='tb-acc'>Descargable</td></tr></thead>" +
					"<tbody></tbody></table>");
			for(index=0;index<descargables.length;index++){
				var formId=descargables[index].id;
				$("#subDivResumen2 table tbody").append(
						"<tr><td rowspan='2'>"+descargables[index].nombre +"</td>" +
						"<td><a id='f"+formId+"'><i class='fa fa-file-word-o' " +
						"aria-hidden='true'></i>"+
								"</td></tr>" +
								"<tr><td>" +
								"<a href='"+URL+descargables[index].rutaDescarga+"' target='_blank'>"+
								descargables[index].evidenciaNombre+"</a>"+
								"</td></tr>"	);	
			
				
				$("#f"+formId).on("click",function(){
				var idAux= $(this).prop("id").split("f");
				console.log();
				window.open(URL
						+ "/capacitacion/informe?progcapacitacionId="
						+idAux[1] , '_blank');	

			})
			}
			
			
			
		
			break;
		}
		
		
	 }else{alert("error")}
	 
 }
 
function removerBotones() {
	$("#fsBotones").html("");
}

function crearBotones() {
/*
 * $("#fsBotones").append('<button>'). attr("class", "btn btn-success").
 * attr("type", "button"). attr("id", "btnCancelar");
 */
   $("#fsBotones")
      .append("<button id='btnCancelar' type='button' class='btn btn-success'  title='Cancelar acciones realizadas - Deshace " +
      		          "todo lo hecho al añadir/editar los datos en la fila seleccionada de la tabla actual.'>"
	       + "<i class='fa fa-sign-out fa-rotate-180 fa-2x'></i>"
	    + "</button>"
            + "<button id='btnNuevo' type='button' class='btn btn-success' title='Nuevo registro - Añade una nueva fila para " +
	    		  "ingresar los datos del registros (algunos campos se completan automáticamente).'>"
	       + "<i class='fa fa-file-o fa-2x'></i>"
	    + "</button>"
	    + "<button id='btnGuardar' type='button' class='btn btn-success' title='Guardar registro - Grabar la información ingresada " +
	    		  "al añadir/editar los datos en la fila seleccionada de la tabla actual.'>"
	       + "<i class='fa fa-floppy-o fa-2x'></i>"
	    + "</button>"
	    + "<button id='btnEliminar' type='button' class='btn btn-danger' title='Eliminar registro seleccionado - Borra la información " +
	   		  "de la fila seleccionada en la tabla actual.'>"
	       + "<i class='fa fa-times fa-2x'></i>" 
	    + "</button>"
	    + "<button id='btnUpload' type='button' class='btn btn-success' title='Subir datos - permite subir los registros de un excel siguiendo" +
	    		   "el formato establecido en el área de texto donde va la información'>"
               + "<i class='fa fa-upload fa-2x'></i>" 
            + "</button>");
   
   $("#btnUpload").css("margin-left","-10");
}

function getUrlParameter(sParam) {
	var sPageURL = window.location.search.substring(1);
	var sURLVariables = sPageURL.split('&');
	for (var i = 0; i < sURLVariables.length; i++) {
		var sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == sParam) {
			return sParameterName[1];
		}
	}
}

function validarLista(listObject) {
	if (listObject.length > 100)
		return "Supera el maximo de 100 lineas permitidas.";

	var arrayUnicos = [];
	for (var int = 0; int < listObject.length; int++) {
		var varElement = listObject[int];
		if (varElement.trim().length < 1) {
			return "Existen filas vacias.";
		}
		if (varElement.trim().length > 200) {
			return "Existen filas que superan los 200 caracteres permitidos.";
		}
		if ($.inArray(varElement.trim(), arrayUnicos) === -1)
			arrayUnicos.push(varElement.trim);
	}
	if (arrayUnicos.length < listObject.legth)
		return "Existen filas repetidas";

	return "";
}

function formatearLista(listObject) {
	if (listObject.length > 100)
		return "Supera el maximo de 100 lineas permitidas.";

	var arrayUnicos = [];
	for (var int = 0; int < listObject.length; int++) {
		var varElement = listObject[int];
		arrayUnicos.push(varElement.trim());
	}
	return arrayUnicos;
}

function guardarMasivoPlano(tablaId, padreId, funcionResultado,isDirectedToIperc) {
	var texto = $("#txtListado").val().toUpperCase();
	
	var listObject = texto.split('\n');
	console.log(listObject);
	$.each(listObject, function(i, l) {
		console.log("Index #" + i + ": " + l.trim().length);
	});
	var validado = validarLista(listObject);
	if (validado.length < 1) {
		var r = confirm("¿Está seguro que desea subir estos "
				+ listObject.length + " elementos?");
		if (r == true) {
			var dataParam = {
				userId : sessionStorage.getItem("gestopUi"),
				sessionId : sessionStorage.getItem("gestopsessionId"),
				listObject : formatearLista(listObject),
				tablaId : tablaId,
				padreId : padreId,
				isDirectedToIperc:isDirectedToIperc
			};

			callAjaxPost(URL + '/tablagenerica/listado/save', dataParam,
					funcionResultado);
		}
	} else {
		alert(validado);
	}
}

function marcarChechBox(entero,checkboxId){
	
	if(entero==1){
		$("#"+checkboxId).prop("checked",true);
	}else{
		$("#"+checkboxId).prop("checked",false);
	}
	
	
	
}

function pasarBoolInt(bool){
	
	if(bool){
		return 1;
	}else{
		return 0;
	}
}

function pasarBoolPalabra(bool){
	
	if(bool){
		return "Si";
	}
	if(bool==0){
		return "No";
	}
	if(bool==null){
		return "Por Definir";
	}
}

function pasarBoolNumber(bool){
	if(bool){
		return 1;
	}else{
		return 0;
	}
}

function pasarNumberBool(number){
	var bool=false;
	if(number==0){
		bool=false;
		
	}else{
		
		bool=true;
	}
	return bool;
	
}

function restringirFecha(idFechaInicio,idFechaFin,idAdvertencia){
	
	
	var fechaInicio=$("#"+idFechaInicio).val();
	var fechaFin=$("#"+idFechaFin).val()
	var correcto;
	fechaInicio=fechaInicio.replace('-', '');
	fechaFin=fechaFin.replace('-', '');
	
	$("#"+idFechaInicio).css("border-color","black");

		if(fechaInicio<=fechaFin){
			if(fechaInicio ==""){
				
				$("#"+idFechaFin).css("border-color","black");
				$("#"+idAdvertencia).html("");
				$("#"+idAdvertencia).css("color","black")
				correcto=true;
				
				if(fechaFin ==""){
					$("#"+idFechaFin).css("border-color","black");
					$("#"+idAdvertencia).html("");
					$("#"+idAdvertencia).css("color","black")
					correcto=true;
					
					}
			}else{
			$("#"+idFechaFin).css("border-color","green");
			$("#"+idAdvertencia).html("Fecha Correcta");
			$("#"+idAdvertencia).css("color","green")
			correcto=true;
			}
		}else{
			$("#"+idFechaFin).css("border-color","red")
			$("#"+idAdvertencia).html("Ingrese una fecha válida");
			$("#"+idAdvertencia).css("color","red")
			correcto=false;
		}
		
		
	
	
	return correcto;
	
}

function restaFechas(f1,f2)
{
var aFecha1 = f1.split('-'); 
var aFecha2 = f2.split('-'); 
var fFecha1 = Date.UTC(aFecha1[0],aFecha1[1]-1,aFecha1[2]); 
var fFecha2 = Date.UTC(aFecha2[0],aFecha2[1]-1,aFecha2[2]); 
var dif = fFecha2 - fFecha1;
var dias = Math.floor(dif / (1000 * 60 * 60 * 24)); 
return dias;
}
function restaFechasMinutos(f1,f2)
{
var aFecha1 = f1.split('-'); 
var aFecha2 = f2.split('-'); 
var fFecha1 = Date.UTC(aFecha1[0],aFecha1[1]-1,aFecha1[2]); 
var fFecha2 = Date.UTC(aFecha2[0],aFecha2[1]-1,aFecha2[2]); 
var dif = fFecha2 - fFecha1;
var dias = Math.floor(dif / (1000 * 60 )); 
return dias;
}
function fechaIncluidaEnRango(fecha,rango1,rango2){
	var boolEntreFechas=false;
	var diferenciaInicio=restaFechas(fecha,rango1);
	var diferenciaFinal=restaFechas(fecha,rango2);
	if(diferenciaInicio<=0 && diferenciaFinal>=0){
		 boolEntreFechas=true;
	}
	 
	return boolEntreFechas;
}
function fechaEnRango(fecha,rango1,rango2){
	var boolEntreFechas=false;
	var diferenciaInicio=restaFechas(fecha,rango1);
	var diferenciaFinal=restaFechas(fecha,rango2);
	if(diferenciaInicio<=0 && diferenciaFinal>=0){
		 boolEntreFechas=true;
	}
	if(rango1==rango2){
		boolEntreFechas=false;
	}
	return boolEntreFechas;
}
 function sumaFechaDias(dias, fecha)
 {
	
	 var aFecha = fecha.split('-');

	 var fFecha = Date.UTC(aFecha[0],aFecha[1]-1,aFecha[2])+(86400000*dias); // 86400000 son los milisegundos que tiene un día

	 var d = new Date(fFecha);



		var month = d.getUTCMonth()+1;
		
		var day = d.getUTCDate();

		var anno= d.getUTCFullYear();
		if(parseInt(month)==0){
			month=12;
			anno=anno-1;
		}
		
		var output =anno + '-' +
		    ((''+month).length<2 ? '0' : '') + month + '-' +
		    ((''+day).length<2 ? '0' : '') + day;
		return output;

	 return (output);

 }

 function sumaFechaDias2(dias, fecha)
 {
	
	 var aFecha = fecha.split('-');

	 var fFecha = Date.UTC(aFecha[0],aFecha[1]-1,aFecha[2])+(86400000*dias); // 86400000 son los milisegundos que tiene un día

	 var d = new Date(fFecha);



		var month = d.getUTCMonth()+1;
		
		var day = d.getUTCDate();

		var anno= d.getUTCFullYear();
		if(parseInt(month)==0){
			month=12;
			anno=anno-1;
		}
		
		var output =((''+day).length<2 ? '0' : '') + day+'-'+
		    ((''+month).length<2 ? '0' : '') + month + '-' +
		    anno ;
		return output;

	 return (output);

 }
function obtenerFechaActual(){
	//Today
	var d = new Date();

	var month = d.getUTCMonth()+1;
	var day = d.getUTCDate();

	var output = d.getUTCFullYear() + '-' +
	    ((''+month).length<2 ? '0' : '') + month + '-' +
	    ((''+day).length<2 ? '0' : '') + day;
	return output;
	
}
function obtenerFechaActualNormal(){
	//Today
	var d = new Date();

	var month = d.getUTCMonth()+1;
	var day = d.getUTCDate();

	var output =   ((''+day).length<2 ? '0' : '') + day+ '-'+
	    ((''+month).length<2 ? '0' : '') + month + '-' +
	  d.getUTCFullYear()  ;
	return output;
	
}

function formatoCeldaSombreable(booleanPrendido){
	if(booleanPrendido){
		$("tbody tr" ).addClass( "info" );
	}else{
		$("tbody tr" ).removeClass( "info" );
	}
	
	
}
function formatoCeldaSombreableTabla(booleanPrendido,tableId){
	if(booleanPrendido){
		$("#"+tableId+" tbody tr" ).addClass( "info" );
	}else{
		$("#"+tableId+" tbody tr" ).removeClass( "info" );
	}
	
	
}
//Funcines para cabecera fija 
   
var contadorFijo=0;

	function goheadfixed(classtable,wrapper) {
	if(contadorFijo==0){
		contadorFijo=1;
		wrapper = typeof wrapper !== 'undefined' ? wrapper : '#wrapper'
		if($(classtable).length) {
			 
			$(classtable).wrap('<div class="fix-inner"></div>'); 
			$('.fix-inner').wrap('<div class="fix-outer" style="position:relative; margin:auto;"></div>');
			$('.fix-outer').append('<div class="fix-head"></div>');
			$('.fix-head').prepend($('.fix-inner').html());
			$('.fix-head table').find('caption').remove();
			$('.fix-head table').css('width','100%');
	
			$('.fix-outer').css('width', $('.fix-inner table').outerWidth(true)+'px');
			$('.fix-head').css('width', $('.fix-inner table').outerWidth(true)+'px');
			$('.fix-head').css('height', $('.fix-inner table thead').height()+'px');
	
			// If exists caption, calculte his height for then remove of total
			var hcaption = 0;
			if($('.fix-inner table caption').length != 0)
				hcaption = parseInt($('.fix-inner table').find('caption').height()+'px');
			
			// Table's Top
			var hinner = parseInt( $('.fix-inner').position().top );

			// Let's remember that <caption> is the beginning of a <table>, it mean that his top of the caption is the top of the table
			$('.fix-head').css({'position':'absolute', 'overflow':'hidden', 'top': hcaption+'px', 'left':0, 'z-index':100 });
			$('.fix-head').css('top','0px');
			$(wrapper).scroll(function () {
				
				var vscroll = $(this).scrollTop();
				var aux=0;
			
					$('.fix-head').css('top',(vscroll-hinner+aux)+'px');
				  
			});
	
			/*	If the windows resize	*/
			$(wrapper).resize(goresize);
			$("#wrapper2").scroll(function () {
				
				var vscroll = $(this).scrollTop();
				var aux=0;
		
					$('.fix-head').css('top',(vscroll-hinner+aux)+'px');
				
				
			});
	
			/*	If the windows resize	*/
			$("#wrapper2").resize(goresize);
		}
	}
	}

	function resizeDivWrapper(wrapperId,marginPx) {
		var	vpw = $(window).width();
			 var vph = $(window).height();
			vph = vph - marginPx;
			$("#"+wrapperId).css({
				"height" : vph + "px"
			});
		}
	function goheadfixedY(classtable,wrapper) {
		
		wrapper = typeof wrapper !== 'undefined' ? wrapper : 'document'
			
		if($(".fix-inner "+classtable).length==0) {
	 
			$(classtable).wrap('<div class="fix-inner"></div>'); 
			$(wrapper+' .fix-inner').wrap('<div class="fix-outer" style="position:relative; margin:auto;"></div>');
			$(wrapper+' .fix-outer').append('<div class="fix-head"></div>');
			$(wrapper+' .fix-head').prepend($(wrapper+' .fix-inner').html());
			$(wrapper+ ' .fix-head table').find('caption').remove();
			$(wrapper+' .fix-head table').css('width','100%');
	
			$(wrapper+' .fix-outer').css('width', $(wrapper+' .fix-inner table').outerWidth(true)+'px');
			$(wrapper+ ' .fix-head').css('width', $(wrapper+' .fix-inner table').outerWidth(true)+'px');
			$(wrapper+' .fix-head').css('height', $(wrapper+' .fix-inner table thead').height()+'px');
	
			// If exists caption, calculte his height for then remove of total
			var hcaption = 0;
			if($(wrapper+' .fix-inner table caption').length != 0)
				hcaption = parseInt($(wrapper+' .fix-inner table').find('caption').height()+'px'); 
			// Table's Top
			var hinner = parseInt( $(wrapper+' .fix-inner').offset().top );

			// Let's remember that <caption> is the beginning of a <table>, it mean that his top of the caption is the top of the table
			$(wrapper +' .fix-head').css({'position':'absolute', 'overflow':'hidden', 'top': hcaption+'px', 'left':0, 'z-index':800 });
			$(wrapper+ ' .fix-head').css('top','0px');
			$(wrapper).scroll(function () {
				
				var vscroll = $(this).scrollTop();
				var aux=0; 
					$(wrapper+ ' .fix-head').css('top',(vscroll+aux)+'px');
				
				
			});
	
			/*	If the windows resize	*/
			$(wrapper).resize(goresizeY(wrapper));
	
		}
	}
	var sizeScrollLeft=0;
	var sizeScrollTop=0;
	function goheadfixedZ(classtable,wrapper,widthSeparator) {
		wrapper = typeof wrapper !== 'undefined' ? wrapper : 'document'
		if($(classtable).length) { 
			$(classtable).wrap('<div class="fix-inner"></div>'); 
			$(wrapper+' .fix-inner').wrap('<div class="fix-outer" style="position:relative; margin:auto;"></div>');
			$(wrapper+' .fix-outer').append("<div class='fix-head'></div>" +
					"<div class='fix-column'></div>" +
					"<div class='fix-divider'></div>");
			$(wrapper+' .fix-head').prepend($(wrapper+' .fix-inner').html());
			$(wrapper+' .fix-column').prepend($(wrapper+' .fix-inner').html());
			$(wrapper+' .fix-divider').prepend($(wrapper+' .fix-inner').html());
			
			$(wrapper+ ' .fix-head table').find('caption').remove();
			
			$(wrapper+' .fix-head table').css('width','100%');
			$(wrapper+' .fix-column table').css('width',$(classtable).width()+"px");
			$(wrapper+' .fix-divider table').css('width',$(classtable).width()+"px");
			
			
			
			$(wrapper+' .fix-outer').css('width', $(wrapper+' .fix-inner table').outerWidth(true)+'px');
			$(wrapper+ ' .fix-head').css('width', $(wrapper+' .fix-inner table').outerWidth(true)+'px');
			$(wrapper+ ' .fix-column').css('width', $(wrapper+' .fix-inner table').outerWidth(true)+'px');
			$(wrapper+ ' .fix-divider').css('width', $(wrapper+' .fix-inner table').outerWidth(true)+'px');
			//Se le da la altura completa
			$(wrapper+' .fix-head').css('height', "100%");
			$(wrapper+' .fix-column').css('height', "100%");
			$(wrapper+' .fix-divider').css('height', "100%");
			
			// If exists caption, calculte his height for then remove of total
			var hcaption = 0;
			if($(wrapper+' .fix-inner table caption').length != 0)
				hcaption = parseInt($(wrapper+' .fix-inner table').find('caption').height()+'px'); 
			// Table's Top
			var hinner = parseInt( $(wrapper+' .fix-inner').offset().top );

			// Let's remember that <caption> is the beginning of a <table>, it mean that his top of the caption is the top of the table
			$(wrapper +' .fix-head').css({'position':'absolute', 'overflow':'hidden', 'top': hcaption+'px', 'left':0, 'z-index':802,"border-bottom":"2px solid black" });
			$(wrapper+ ' .fix-head').css('top','0px');
			$(wrapper +' .fix-column').css({'position':'absolute', 'overflow':'hidden', 'top': hcaption+'px', 'left':0, 'z-index':801 });
			$(wrapper+ ' .fix-column').css('top','0px');
			$(wrapper +' .fix-divider').css({'position':'absolute', 'overflow':'hidden', 'top': hcaption+'px', 'left':0, 'z-index':803 });
			$(wrapper+ ' .fix-divider').css('top','0px');
			
			//Poniendo bordes
			var casillaReferenciaWidth;
			$(wrapper+ ' .fix-column table tbody tr').each(function(index,elem){
				var primeraCasilla=$(elem).children()[0];
				casillaReferenciaWidth=$(elem).find(primeraCasilla);
				casillaReferenciaWidth.css({"border-right":"2px solid black"});
			})
			$(wrapper).scroll(function () {
				
				var vscroll = $(this).scrollTop();
				var hscroll = $(this).scrollLeft();
				
				
				var aux=0; 
				$(wrapper+ ' .fix-column').css('left',(hscroll+aux)+'px');
				$(wrapper+ ' .fix-head').css('top',(vscroll+aux)+'px');
				$(wrapper +' .fix-divider').css({'left':(hscroll+aux)+'px', 'top':(vscroll+aux)+'px'});
					
				
			});
			var casillaReferencia=$(".fix-head "+classtable+" thead");
			var casillaReferenciaSeparador=$(".fix-divider "+classtable+" thead th:nth-child(1)");
			console.log(casillaReferenciaSeparador);
			casillaReferenciaSeparador.removeClass("tb-acc");
			casillaReferenciaSeparador.css({"background-color":"#046666","text-align":"center","color":"white","vertical-align":"middle"});
			
	/*	If the windows resize	*/
			console.log(casillaReferenciaWidth.css("width")+" altura pls");
			var anchoRequerido=parseInt(casillaReferenciaWidth.css("width").replace("px",""));
			
			$(wrapper+' .fix-column').css('width', anchoRequerido+2+'px');
			$(wrapper+' .fix-head').css('height', casillaReferencia.height()+2+'px');
			$(wrapper+' .fix-divider').css({'height':casillaReferencia.height()+'px','width': anchoRequerido+2+'px'});
			
	}
	}
	function goresize() {
		$('.fix-head').css('width', $('.fix-inner table').outerWidth(true)+'px');
		$('.fix-head').css('height', $('.fix-inner table thead').outerHeight(true)+'px');
	}
    
	function goresizeY(wrapper) {
		 
		$(wrapper+' .fix-head').css('width', $(wrapper+' .fix-inner table').outerWidth(true)+'px');
		$(wrapper+' .fix-head').css('height', $(wrapper+' .fix-inner table thead').outerHeight(true)+'px');
	}
	function goresizeZ(wrapper) {
		 
		$(wrapper+' .fix-head').css('width', $(wrapper+' .fix-inner table').outerWidth(true)+'px');
		//$(wrapper+' .fix-head').css('height', $(wrapper+' .fix-inner table thead').outerHeight(true)+'px');

	}
	function convertirFechaNormal(reporteFecha){
		if (reporteFecha) {
			var myDate = new Date(reporteFecha);
			var day = ("0" + myDate.getUTCDate()).slice(-2);
			var month = ("0" + (myDate.getUTCMonth() + 1)).slice(-2);
			var today = (day)+ "-" + (month) + "-" +myDate.getUTCFullYear()  ;
			return today
		} else {
			return ""
		}
	}
	function convertirFechaNormal2(reporteFecha){
		if (reporteFecha) {
			var myDate = new Date(reporteFecha);
			var day = ("0" + myDate.getUTCDate()).slice(-2);
			var month = ("0" + (myDate.getUTCMonth() + 1)).slice(-2);
			var today =myDate.getUTCFullYear()  + "-" + (month) + "-" +(day) ;
			
			return today
		} else {
			return ""
		}
	}
	function convertirFechaNormal3(reporteFecha){
		if (reporteFecha) {
			var hoy=obtenerFechaActual();
			var myDate = new Date(reporteFecha);
			var day = ("0" + myDate.getUTCDate()).slice(-2);
			var month = ("0" + (myDate.getUTCMonth() + 1)).slice(-2);
			var fecha = (day)+ "-" + (month) + "-" +myDate.getUTCFullYear()  ;
			var fechaComparar =myDate.getUTCFullYear()  + "-" + (month) + "-" +(day) ;
			var diferencia=restaFechas(hoy,fechaComparar);
			if(diferencia==0){
				fecha="Hoy";
			}
			
			return fecha
		} else {
			return ""
		}
	}
	function convertirFechaInput(reporteFecha)
	{
		if (reporteFecha!=null) {
			var myDate = new Date(reporteFecha);
			var day = ("0" + myDate.getUTCDate()).slice(-2);
			var month = ("0" + (myDate.getUTCMonth() + 1)).slice(-2);
			var today = myDate.getUTCFullYear()+ "-" + (month) + "-" +(day)  ;
			return today
		} else {
			return ""
		}
	}
	var oculto=false;
	function toggleColumna(numColumna,idTabla){
		if(oculto){
			mostrarColumna(numColumna,idTabla)
		}else{
			ocultarColumna(numColumna,idTabla)
		}
	}

	function ocultarColumna(numColumna,idTabla){
		$('#'+idTabla+' td:nth-child('+numColumna+'),#'+idTabla+' th:nth-child('+numColumna+')').hide();
		oculto=true;
	}
	
	function mostrarColumna(numColumna,idTabla){
		$('#'+idTabla+' td:nth-child('+numColumna+'),#'+idTabla+' th:nth-child('+numColumna+')').show();
		oculto=false
	}
	


	function llamarListaAyuda(list,ayudaId){
		
				
		for(index=0;index<list.length;index++){
			
			if(list[index].ayudaId==ayudaId){
				$("#divAyuda").html(list[index].textoAyuda);
				
			}
		}
		
		
	}


	
	function llenarEspacioAyuda(){
		$("#modalAyuda").modal('show');
		ayudaId=sessionStorage.getItem("ayudaId");
		llamarListaAyuda(listAyuda,ayudaId)
		
	}
	
	
	function ponerListaSugerida(inputId,lista,esInterno){
		if(esInterno){
		$("#"+inputId).autocomplete({
			source : lista,
			minLength : 0
		}).focus(function() {
			$(this).autocomplete('search', '');
		});
		}
		if(!esInterno){
			$("#"+inputId).val("");
			$("#"+inputId).autocomplete({
				
				source :""
				
			})
		}
	}
	
	function pasarDecimalPorcentaje(decimal,numDecimales){
		var porc;
		var aux=decimal*100;
		
		if(aux>=0){
			porc=aux.toFixed(numDecimales);
			return porc+"%"
		}else{
			return 0+"%"
		}
		
		
	} 
	
	function setTextareaHeight(textareas) {
	    textareas.each(function () {
	        var textarea = $(this);
	        textarea.css({"resize":"none"});
	        if ( !textarea.hasClass('autoHeightDone') ) {
	            textarea.addClass('autoHeightDone');
	            textarea.css({"resize":"none"});
	            var extraHeight = parseInt(textarea.css('padding-top')) + parseInt(textarea.css('padding-bottom')), // to set total height - padding size
	                h = textarea[0].scrollHeight - extraHeight;
	 
	            // init height
	            textarea.height('auto').height(h);
	 
	            textarea.bind('keyup', function() {
	            	
	                textarea.removeAttr('style'); // no funciona el height auto
	                textarea.css({"resize":"none"});
	                h = textarea.get(0).scrollHeight - extraHeight;
	 
	                textarea.height(h+'px'); // set new height
	            });
	        }
	    })
	}
	

	
function insertarImagenParaTabla(codigoImagen,altura,ancho){
	var texto="<img src='data:image/png;base64,"+codigoImagen+"' " +
			"style='height:"+altura+"  ; width:"+ancho+"  '>";
	return texto;
	
	
}	
function insertarImagenParaTablaMovil(codigoImagen,altura,ancho){
	var texto="<img src='data:image/png;base64,"+codigoImagen+"' " +
			"style='max-height:"+altura+"  ; max-width:"+ancho+"  '>";
	return texto;
	
	
}	
function resizeDivi() {
var	vpw = $(window).width();
	 var vph = $(window).height();
	vph = vph - 240;
	$("#wrapper").css({
		"height" : vph + "px"
	});
}


//Funcion para evitar comillas
function nombrarInput(idInput,valor){
	$("#"+idInput).attr("value", valor);
	
}

function calcularEdadAuto(fecha){ 

   	//calculo la fecha de hoy 
   var	hoy=new Date() 
   	//alert(hoy) 

   	//calculo la fecha que recibo 
   	//La descompongo en un array 
   	var array_fecha = fecha.split("-") 
   	//si el array no tiene tres partes, la fecha es incorrecta 
   	if (array_fecha.length!=3) 
   	{	return false} 

   	//compruebo que los ano, mes, dia son correctos 
   	var ano ;
   	ano = parseInt(array_fecha[0]); 
   	

   	var mes ;
   	mes = parseInt(array_fecha[1]); 
  

   	var dia ;
   	dia = parseInt(array_fecha[2]);	
  


   	//resto los años de las dos fechas 
var   	edad=hoy.getUTCFullYear()- ano - 1; //-1 porque no se si ha cumplido años ya este año 

   	//si resto los meses y me da menor que 0 entonces no ha cumplido años. Si da mayor si ha cumplido 
   	if (hoy.getMonth() + 1 - mes < 0) //+ 1 porque los meses empiezan en 0 
      	{return edad} 
   	if (hoy.getMonth() + 1 - mes > 0) 
      	{return edad+1 }

   	//entonces es que eran iguales. miro los dias 
   	//si resto los dias y me da menor que 0 entonces no ha cumplido años. Si da mayor o igual si ha cumplido 
   	if (hoy.getUTCDate() - dia >= 0) 
   	{	return edad + 1} 

   	return edad 
}
function calcularEdadRelativa(fechaNacimiento,fechaRelativa){ 

   	//calculo la fecha de hoy 
   var	hoy=convertirFechaTexto(fechaRelativa);
   	//alert(hoy) 

   	//calculo la fecha que recibo 
   	//La descompongo en un array 
   	var array_fecha = fechaNacimiento.split("-") 
   	//si el array no tiene tres partes, la fecha es incorrecta 
   	if (array_fecha.length!=3) 
   	{	return false} 

   	//compruebo que los ano, mes, dia son correctos 
   	var ano ;
   	ano = parseInt(array_fecha[0]); 
   	

   	var mes ;
   	mes = parseInt(array_fecha[1]); 
  

   	var dia ;
   	dia = parseInt(array_fecha[2]);	
  


   	//resto los años de las dos fechas 
var   	edad=hoy.getUTCFullYear()- ano - 1; //-1 porque no se si ha cumplido años ya este año 

   	//si resto los meses y me da menor que 0 entonces no ha cumplido años. Si da mayor si ha cumplido 
   	if (hoy.getMonth() + 1 - mes < 0) //+ 1 porque los meses empiezan en 0 
      	{return edad} 
   	if (hoy.getMonth() + 1 - mes > 0) 
      	{return edad+1 }

   	//entonces es que eran iguales. miro los dias 
   	//si resto los dias y me da menor que 0 entonces no ha cumplido años. Si da mayor o igual si ha cumplido 
   	if (hoy.getUTCDate() - dia >= 0) 
   	{	return edad + 1} 

   	return edad 
}


function llevarTablaFondo(tablaId){
	
	$("#"+tablaId).animate(
			{ scrollTop: 
				$('#'+tablaId)[0].scrollHeight}, 1000);
	
}


function comprobarNumEmpresasUnidades(numEmpresasUnidadesPermitadas,numEmpresasUnidadesActuales){
	
	var acceso=true;
	
	if(parseInt(numEmpresasUnidadesPermitadas)>parseInt(numEmpresasUnidadesActuales)){
		acceso=true;
		
	}else{
		
		acceso=false;
	}
	return acceso
	
	
}

function comprobarLicencia(){

}

function are_cookies_enabled()
{
    var cookieEnabled = (navigator.cookieEnabled) ? true : false;
    
   if (typeof navigator.cookieEnabled == "undefined" && !cookieEnabled)
    { 
        document.cookie="testcookie";
        cookieEnabled = (document.cookie.indexOf("testcookie") != -1) ? true : false;
    }
    return (cookieEnabled);
}


function toggleBuscadorJstree(idToggle){
	$("#buscarTrabJstree").hide();
	
	if(idToggle==1){
	$("#buscarTrabJstree").show();
	$("#listTrabBuscar").hide();
	$("#cambiarBuscador").html(
		"	<i class='fa fa-arrow-circle-down ' ></i>	"
	);
	$("#cambiarBuscador").attr("onclick","toggleBuscadorJstree(2)");
	$("#resultSearchTrab").hide();
	}else{
		$("#buscarTrabJstree").hide();
		$("#listTrabBuscar").show();
		$("#cambiarBuscador").html(
			"	<i class='fa fa-arrow-circle-up ' ></i>	"
		);
		$("#cambiarBuscador").attr("onclick","toggleBuscadorJstree(1)");
		$("#resultSearchTrab").show();
		
	}
}
function toggleBuscadorAsistJstree(idToggle){
$("#buscarTrabAsisJstree").hide();
	
	if(idToggle==1){
	$("#buscarTrabAsisJstree").show();
	$("#listTrabAsistBuscar").hide();
	$("#cambiarBuscadorAsist").html(
		"	<i class='fa fa-arrow-circle-down ' ></i>	"
	);
	$("#cambiarBuscadorAsist").attr("onclick","toggleBuscadorAsistJstree(2)");
	$("#resultSearchTrabAsist").hide();
	}else{
		$("#buscarTrabAsisJstree").hide();
		$("#listTrabAsistBuscar").show();
		$("#cambiarBuscadorAsist").html(
			"	<i class='fa fa-arrow-circle-up ' ></i>	"
		);
		$("#cambiarBuscadorAsist").attr("onclick","toggleBuscadorAsistJstree(1)");
		$("#resultSearchTrabAsist").show();
		
	}
	
}
var trabajadoresEncontrados;
function busquedaMasivaJstree(listTrabsFull,jstreeId,resultId,textAreaId,trabNodo){
	$('#'+jstreeId).jstree("destroy");
	var texto = $("#"+textAreaId).val();
	var listExcel = texto.split('\n');
	var listTrabs = [];	
	var validado="";
	trabajadoresEncontrados=0;
	for (index = 0; index < listTrabsFull.length; index++) {

		listTrabsFull[index].state.checked=false;
		
	}
	var trab = {};
	for (var int = 0; int < listExcel.length; int++) {

	
		var filaTexto = listExcel[int];
		if (filaTexto.trim().length < 1) {
			validado = "Existen filas vacias.";
			break;
		}

			trab.nombre=filaTexto;
			filaTexto=filaTexto.toUpperCase();
			marcarJstree(listTrabsFull,filaTexto,trabNodo);
			listTrabs.push(trab);
			
		
	};
	$('#'+jstreeId).jstree({
		"plugins" : [ "checkbox", "json_data" , "search" ],
		'checkbox' : {
			"keep_selected_style" : false,
			"tie_selection" : false
		},
		'core' : {
			'data' : listTrabsFull
		}
	});
//	$('#'+jstreeId).jstree("open_all");
	
	$("#"+resultId).html(
			"Se seleccionaron "+trabajadoresEncontrados+" de "+listTrabs.length+" trabajador(es) ingresado(s)");
}

function marcarJstree(listTrabs,nombreMarcar,trabNodo){
if(trabNodo==null){
	trabNodo="tra"
		for (var index = 0; index < listTrabs.length; index++) {
			var texto=listTrabs[index].text;
			var nombreTrab=texto.toUpperCase();
			if (listTrabs[index].id.substr(
					0, 3) == trabNodo  && nombreTrab.indexOf(nombreMarcar) !=-1 ) {
				listTrabs[index].state.checked=true;
				trabajadoresEncontrados=trabajadoresEncontrados+1;
			}
		};
		
}
if(trabNodo==1){
	trabNodo="nodo";
		for (var index = 0; index < listTrabs.length; index++) {
			
			if (listTrabs[index].id.substr(
					0, 4) == trabNodo  &&listTrabs[index].text.indexOf(nombreMarcar) !=-1 ) {
				listTrabs[index].state.checked=true;
				
				trabajadoresEncontrados=trabajadoresEncontrados+1;
			}
		};
		
}

	
	


}

function obtenerDatoEstadistico(dato){
	if(dato==Infinity){
		return 0;
	}
	if(dato>0){
		return parseFloat(dato.toFixed(2));
	}else{
		return 0
	}
	
}

function escribirFraccion(divContenedor,inicio,numerador,denominador){
var htmlFraccion="<div class='eq-c'>"
+inicio+"= "
+"<div class='fraction'>"
+"<span class='fup'>"+numerador+"</span>"
+"<span class='bar'>/</span>"
+"<span class='fdn'>"+denominador+"</span>"
+"</div>"
+"</div>";

$("#"+divContenedor).html(htmlFraccion);
	
}
function escribirFraccionSola(divContenedor,numerador,denominador){
	var htmlFraccion="<div class='eq-c'>"
	+"<div class='fraction'>"
	+"<span class='fup'>"+numerador+"</span>"
	+"<span class='bar'>/</span>"
	+"<span class='fdn'>"+denominador+"</span>"
	+"</div>"
	+"</div>";

	$("#"+divContenedor).html(htmlFraccion);
		
	}
function obtenerUTCDate(dateY){
var dateUTC=	new Date(Date.UTC(
			dateY.getUTCFullYear(),
			dateY.getUTCMonth(),
			dateY.getUTCDate(),
			dateY.getUTCHours(),
			dateY.getUTCMinutes())-5*60*60*1000 )
	return dateUTC;
	
}
function obtenerUTCDateFromInput(inputDate){
	var dateY=inputDate.split("-");
	var dateUTC=	new Date(Date.UTC(
				dateY[0],
				dateY[1]-1,
				dateY[2]));
		return dateUTC;
		
	}
function irFondoDiv(wrapperId){
	
	$("#"+wrapperId).animate(
			{ scrollTop: 
				$('#'+wrapperId)[0].scrollHeight}, 1000);
	
	
}
function obtenerDatosBodyTablaEstandarNoFija(tablaId) {
	
	var fullDatosTabla="";
	 $("#"+tablaId+" thead tr").each(function (index){   
         $(this).children("th").each(function (index2,elem2){
        	 var numCol=parseInt($(elem2).prop("colSpan"));console.log(numCol)
        	 if(numCol==1){
        		 fullDatosTabla =fullDatosTabla+ $(this).text()+"\t"; 
        	 }
              

         })
          $(this).children("td").each(function (index2,elem2){
        	  var numCol=parseInt($(elem2).prop("colSpan"));console.log(numCol)
         	 if(numCol==1)
              fullDatosTabla =fullDatosTabla+ $(this).text()+"\t";
              //  console.log($(this).attr("colspan"))
         });
     });
	 fullDatosTabla=fullDatosTabla+"\n"
	 $("#"+tablaId+" tbody tr").each(function (index){   
		            $(this).children("td").each(function (index2){
		                var  texto=$(this).text().replace("\t","");
		                
		                texto=texto.replace("S/","");
		                texto=texto.replace("/","//");
		                if(index2==6 || index2==9){
		                	switch($(this).html()){
		                	case '<i style="color:green" class="fa fa-check-square"></i>':
		                		texto="Actualizado";
		                		break;
		                	case '<i style="color:red" class="fa fa-exclamation-circle"></i>':
		                		texto="Sin actualizar";
		                		break;	
		                		
		                		
		                	}
		                	 
		                	 
		                }
		                
		                fullDatosTabla =fullDatosTabla+ texto+"\t";
		   	            })
		            fullDatosTabla=fullDatosTabla+"\n"
		        });
	 return fullDatosTabla;
	
	 
   }
function obtenerDatosBodyTablaEstandar(tablaId) {
	
	var fullDatosTabla="";
	 
	 $(".fix-head #"+tablaId+" tbody tr").each(function (index){   
		            $(this).children("td").each(function (index2){
		                var  texto=$(this).text().replace("\t","");
		                
		                texto=texto.replace("S/","");
		                texto=texto.replace("/","//");
		                if(index2==6 || index2==9){
		                	switch($(this).html()){
		                	case '<i style="color:green" class="fa fa-check-square"></i>':
		                		texto="Actualizado";
		                		break;
		                	case '<i style="color:red" class="fa fa-exclamation-circle"></i>':
		                		texto="Sin actualizar";
		                		break;	
		                		
		                		
		                	}
		                	 
		                	 
		                }
		                
		                fullDatosTabla =fullDatosTabla+ texto+"\t";
		   	            })
		            fullDatosTabla=fullDatosTabla+"\n"
		        });
	 return fullDatosTabla;
	
	 
   }
function obtenerDatosTablaEstandar(tablaId) {
	
	var fullDatosTabla="";
	 $(".fix-head #"+tablaId+" thead tr").each(function (index){   
        $(this).children("th").each(function (index2,elem){
             
             if($(elem).is(":visible")){
           	  fullDatosTabla =fullDatosTabla+ $(elem).text().trim()+"\t";
           	  console.log($(elem).text().trim());
	                 }
        })
         $(this).children("td").each(function (index2,elem){
            
             if($(elem).is(":visible")){
           	  fullDatosTabla =fullDatosTabla+ $(elem).text().trim()+"\t";
           	  console.log($(elem).text().trim());
	                 }
             //  console.log($(this).attr("colspan"))
        });
        fullDatosTabla=fullDatosTabla+"\n"
    });
	 $(".fix-head #"+tablaId+" tbody tr").each(function (index){   
		            $(this).children("td").each(function (index2,elem){
		                var  texto=$(this).text().replace("\t","");
		                
		                texto=texto.replace("S/","");
		                texto=texto.replace("/","//");
		                if(index2==6 || index2==9){
		                	if($(this).html()=='<i style="color:green" class="fa fa-check-square"></i>'){
		                		texto="Actualizado"
		                	}
		                	if($(this).html()=='<i style="color:red" class="fa fa-exclamation-circle"></i>'){
		                		texto="Sin actualizar"
		                	}
		                }
		                if($(elem).is(":visible")){
			                fullDatosTabla =fullDatosTabla+ texto+"\t";
			                 }
		   	            })
		            fullDatosTabla=fullDatosTabla+"\n"
		        });
	 return fullDatosTabla;
	
	 
   }
function obtenerDatosTablaEstandarNoFija(tablaId) {
	
	var fullDatosTabla="";
	 $("#"+tablaId+" thead tr").each(function (index){   
         $(this).children("td").each(function (index2,elem){
        	 if($(elem).is(":visible")){
        		 console.log($(this).text());
        		 fullDatosTabla =fullDatosTabla+ $(this).text()+"\t";
        	 }
             

         });
         $(this).children("th").each(function (index2,elem){
        	 if($(elem).is(":visible")){
        		 fullDatosTabla =fullDatosTabla+ $(this).text()+"\t";
        	 }

        });
         fullDatosTabla=fullDatosTabla+"\n"
     });
	 $("#"+tablaId+" tbody tr").each(function (index){   
		            $(this).children("td").each(function (index2,elem){
		                var  texto=$(this).text().replace("\t","");
		                 texto=texto.replace("/","//");
		                 
		                 if($(elem).is(":visible")){
		                fullDatosTabla =fullDatosTabla+ texto+"\t";
		                 }
		   	            })
		            fullDatosTabla=fullDatosTabla+"\n"
		        });
	
	 return fullDatosTabla;
	
	
   }
function obtenerDatosTablaEstadística(tablaId){
	var fullDatosTabla="";
	 $("#"+tablaId+" thead tr").each(function (index){   
         $(this).children("td").each(function (index2,elem){
        	 if($(elem).is(":visible")){
        		 fullDatosTabla =fullDatosTabla+ $(this).text()+"\t";
        	 }
             

         });
         $(this).children("th").each(function (index2,elem){
        	 if($(elem).is(":visible")){
        		 fullDatosTabla =fullDatosTabla+ $(this).text()+"\t";
        	 }

        });
         fullDatosTabla=fullDatosTabla+"\n"
     });
	 $("#"+tablaId+" tbody tr").each(function (index){   
		            $(this).children("td").each(function (index2,elem){
		                var  texto=$(this).text().replace("\t","");
		                 texto=texto.replace("/","//");
		                 if($(elem).html()=='<i class="fa fa-check fa-2x" style="color:green"></i>'){
		                	 texto="1";
				        }
		                 if($(elem).html()=='<i class="fa fa-times fa-2x" style="color:red"></i>'){
		                	 texto="0";
				        }
		                 
		                 if($(elem).is(":visible")){
		                fullDatosTabla =fullDatosTabla+ texto+"\t";
		                 }
		   	            })
		            fullDatosTabla=fullDatosTabla+"\n"
		        });
	 return fullDatosTabla;
}
function traerTrabajdoresEmpresa(){

	
}
var alertasOpen=false;
function verAlertasGosst(){
	var dataParam = {
			idCompany : sessionStorage.getItem("gestopcompanyid")
		};
	alertasOpen=!alertasOpen;
	$("#indAlertaGosst").remove();
	$("#logoMenu").append("<div id='indAlertaGosst' class='numAlertas'>!</div>");
	
	
	$("#menuAlertasGosst").remove();
	
	if(alertasOpen){
		$("#logoMenu span img").attr("src","../imagenes/logoVertical.png");
		$("body").append("<div id='menuAlertasGosst'>" +
				"" +
				"" +
				"</div>");
		 $("#menuAlertasGosst").css({
		 		"height":$(window).height()-$("nav").height()-2+"px"});
			
	}else{
		
			$("#logoMenu span img").attr("src","../imagenes/logoVerticalSel.png");
	}
	 alertasFormacion(dataParam);
	 alertasEpp(dataParam);
	 alertasProcedimiento(dataParam);
}

//Logica  de alertas GOSST
function alertasProcedimiento(dataParam){

	callAjaxPost(URL + '/procedimientoseguridad', dataParam,
			function(data){
		var procedimientos=data.list;
		var procedimientosVigente=0;
		var procedimientosNoVigente=0;
		var procedimientosTotal=0;
		
			$("#alertProc").remove();
		procedimientos.forEach(function(val,index){
			
				if(val.estadoId==2){
					procedimientosVigente+=1;
				}
				if(val.estadoId==3){
					procedimientosNoVigente+=1;
				}
			
	
				});
		procedimientosTotal=procedimientosVigente+procedimientosNoVigente;
		var porc=procedimientosVigente/procedimientosTotal;
		var porcTexto=procedimientosVigente+"/"+procedimientosTotal;
		if(procedimientosTotal>0){
$("#menuAlertasGosst").append("<div id='alertProc' class='alertGeneral'>" +
		"<div class='tituloAlert'>Vigencia Procedimientos</div>" +
		"</div>");
$("#menuAlertasGosst #alertProc").append(
		"<div class='alertSub'>"+
			"<div class='nombreItem'>"+
			"Total"
			+"</div>" +
			"<div class='indicadorAlerta "+comprobarFondoAceptable(porc)+"' " +
			"style='width:"+($("#menuAlertasGosst").width()*porc).toFixed(2)+"px'>" +
			porcTexto+"</div>" +
			
		"</div>"		
		);
		}else{
			$("#menuAlertasGosst").append("<div id='alertProc' class='alertGeneral'>" +
					"<div class='tituloAlert'>Vigencia Procedimientos (Sin registrar)</div>" +
					"</div>");
		}
		 $("#menuAlertasGosst").unblock();
	},functionBloquearAlertas,{},functionErrorAlerta("Procedimiento","alertProc"));
}

function alertasEpp(dataParam){

	callAjaxPost(URL + '/equiposeguridad', dataParam,
			function(data){
		var epps=data.list;
		var equiposEmergenciaVigente=0;
		var equiposEmergenciaNoVigente=0;
		var equiposTotal=0;
		if(epps.length>0){
			$("#alertEpp").remove();
			$("#alertEquip").remove();
$("#menuAlertasGosst").append("<div id='alertEpp' class='alertGeneral'>" +
		"<div class='tituloAlert'>Vigencia EPP´s &nbsp;<label>A</label><label>B</label><label>C</label>   </div>" +
		"</div>");
		}
		epps.forEach(function(val,index){
			if(val.equipoSeguridadTipoId==1){
				
				var listAprobadosReal=dividirLista(val.listTrabVigentesAprobados);
				var listAprobadosRetraso=dividirLista(val.listTrabRetrasadosAprobados);
				listAprobadosRetraso=fusionNoRepeticion(listAprobadosReal,listAprobadosRetraso);
				var listAprobadosIdeal=dividirLista(val.listTrabIdealesAprobados);
				listAprobadosIdeal=fusionNoRepeticion(listAprobadosIdeal,listAprobadosRetraso);
			
		var porcentajeCumplimiento=	listAprobadosReal.length /val.numTrabAsignados;
		var porcentajeCumplimientoTexto=devolverTextoPorcentajeAlerta(listAprobadosReal.length,val.numTrabAsignados);
		var widthAlerta=($("#menuAlertasGosst").width()*porcentajeCumplimiento).toFixed(2);
		
		var porcentajeCumplimientoB=	listAprobadosIdeal.length /val.numTrabAsignados;
		var porcentajeCumplimientoBTexto=devolverTextoPorcentajeAlerta(listAprobadosIdeal.length,val.numTrabAsignados);
		var widthAlertaB=($("#menuAlertasGosst").width()*porcentajeCumplimientoB).toFixed(2);
		
		var porcentajeCumplimientoC=	listAprobadosRetraso.length /val.numTrabAsignados;
		var porcentajeCumplimientoCTexto=devolverTextoPorcentajeAlerta(listAprobadosRetraso.length,val.numTrabAsignados);
		var widthAlertaC=($("#menuAlertasGosst").width()*porcentajeCumplimientoC).toFixed(2);
						$("#menuAlertasGosst #alertEpp").append(
								"<div class='alertSub'>"+
									"<div class='nombreItem'>"+
									val.equipoSeguridadNombre
									+"</div>" +
									"<div class='indicadorAlerta escenarioA "+comprobarFondoAceptable(porcentajeCumplimiento)+"' " +
									"style='width:"+widthAlerta+"px'>A_" +
									porcentajeCumplimientoTexto+"</div>" +
									"<div class='indicadorAlerta  escenarioB "+comprobarFondoAceptable(porcentajeCumplimientoC)+"' " +
									"style='width:"+widthAlertaC+"px'>B_" +
									porcentajeCumplimientoCTexto+"</div>" +
									"<div class='indicadorAlerta escenarioC "+comprobarFondoAceptable(porcentajeCumplimientoB)+"' " +
									"style='width:"+widthAlertaB+"px'>C_" +
									porcentajeCumplimientoBTexto+"</div> " +
									
									
								"</div>"		
								);
			}else{
				if(val.estadoId==2){
					equiposEmergenciaVigente+=1;
				}
				if(val.estadoId==3){
					equiposEmergenciaNoVigente+=1;
				}
			}
	
				});
		equiposTotal=equiposEmergenciaVigente+equiposEmergenciaNoVigente;
		var porc=equiposEmergenciaVigente/equiposTotal;
		var porcTexto=equiposEmergenciaVigente+"/"+equiposTotal;
		if(equiposTotal>0){
$("#menuAlertasGosst").append("<div id='alertEquip' class='alertGeneral'>" +
		"<div class='tituloAlert'>Vigencia Equipos de Emergencia</div>" +
		"</div>");
$("#menuAlertasGosst #alertEquip").append(
		"<div class='alertSub'>"+
			"<div class='nombreItem'>"+
			"Total"
			+"</div>" +
			"<div class='indicadorAlerta "+comprobarFondoAceptable(porc)+"' " +
			"style='width:"+($("#menuAlertasGosst").width()*porc).toFixed(2)+"px'>" +
			porcTexto+"</div>" +
			
		"</div>"		
		);
		}else{
			$("#menuAlertasGosst").append("<div id='alertEquip' class='alertGeneral'>" +
					"<div class='tituloAlert'>Vigencia Equipos de Emergencia (Sin registrar)</div>" +
					"</div>");
		}
		 $("#menuAlertasGosst").unblock();
	},functionBloquearAlertas,{},functionErrorAlerta("EPP","alertEpp"));
}

function alertasFormacion(dataParam){
$(document).on("click",".tituloAlert label",function(){
	$(this).siblings()
	  .removeClass( "escenarioSelect");
	$(this).addClass( "escenarioSelect");
	var alertasGenerales=$(this).parents(".alertGeneral");
	var escenarioSelected=$(this).text();
	alertasGenerales.find(".alertSub .indicadorAlerta").hide();
	
	alertasGenerales.find(".alertSub .escenario"+escenarioSelected).show();
	
	
});
	callAjaxPost(URL + '/capacitacion', dataParam,
			function(data){
		var capacitaciones=data.list;
		if(capacitaciones.length>0){
			$("#alertCap").remove();
$("#menuAlertasGosst").append("<div id='alertCap' class='alertGeneral'>" +
		"<div class='tituloAlert'>Vigencia Formaciones&nbsp;<label>A</label><label>B</label><label>C</label>   </div>" +
		"</div>");
		}
		capacitaciones.forEach(function(val,index){
			var listAprobadosReal=dividirLista(val.listTrabVigentesAprobados);
			var listAprobadosRetraso=dividirLista(val.listTrabRetrasadosAprobados);
			listAprobadosRetraso=fusionNoRepeticion(listAprobadosReal,listAprobadosRetraso);
			var listAprobadosIdeal=dividirLista(val.listTrabIdealesAprobados);
			listAprobadosIdeal=fusionNoRepeticion(listAprobadosIdeal,listAprobadosRetraso);
		
	var porcentajeCumplimiento=	listAprobadosReal.length /val.numTrabAsignados;
	var porcentajeCumplimientoTexto=devolverTextoPorcentajeAlerta(listAprobadosReal.length,val.numTrabAsignados);
	var widthAlerta=($("#menuAlertasGosst").width()*porcentajeCumplimiento).toFixed(2);
	
	var porcentajeCumplimientoB=	listAprobadosIdeal.length /val.numTrabAsignados;
	var porcentajeCumplimientoBTexto=devolverTextoPorcentajeAlerta(listAprobadosIdeal.length,val.numTrabAsignados);
	var widthAlertaB=($("#menuAlertasGosst").width()*porcentajeCumplimientoB).toFixed(2);
	
	var porcentajeCumplimientoC=	listAprobadosRetraso.length /val.numTrabAsignados;
	var porcentajeCumplimientoCTexto=devolverTextoPorcentajeAlerta(listAprobadosRetraso.length,val.numTrabAsignados);
	var widthAlertaC=($("#menuAlertasGosst").width()*porcentajeCumplimientoC).toFixed(2);
			$("#menuAlertasGosst #alertCap").append(
					"<div class='alertSub'>"+
						"<div class='nombreItem'>"+
						val.capacitacionNombre
						+"</div>" +
						"<div class='indicadorAlerta escenarioA "+comprobarFondoAceptable(porcentajeCumplimiento)+"' " +
						"style='width:"+widthAlerta+"px'>A_" +
						porcentajeCumplimientoTexto+"</div>" +
					
						"<div class='indicadorAlerta  escenarioB "+comprobarFondoAceptable(porcentajeCumplimientoC)+"' " +
						"style='width:"+widthAlertaC+"px'>B_" +
						porcentajeCumplimientoCTexto+"</div>" +
						"<div class='indicadorAlerta escenarioC "+comprobarFondoAceptable(porcentajeCumplimientoB)+"' " +
						"style='width:"+widthAlertaB+"px'>C_" +
						porcentajeCumplimientoBTexto+"</div> " +
						
					"</div>"		
					);
				});
		 $("#menuAlertasGosst").unblock();
		
	},functionBloquearAlertas,{},functionErrorAlerta("Formacion","alertCap"));
}

function functionBloquearAlertas(){
	$("#menuAlertasGosst").block({message:'<img src="../imagenes/gif/ruedita.gif"></img>cargando...'});
	
}
function functionErrorAlerta(nombreModulo,alertDivId){

		$("#menuAlertasGosst").unblock();
		$("#menuAlertasGosst").append("<div id='"+alertDivId+"' class='alertGeneral'>" +
				"<div class='tituloAlert'>"+nombreModulo+" (Sin acceso al módulo)</div>" +
				"</div>");
		
}
//fIN Logica  de alertas GOSST

function comprobarFondoAceptable(porcentaje){
	var claseBarra="";
	if(porcentaje<=0.33){
		claseBarra="barraPeligrosa"
	}
	
	if(porcentaje>0.33 && porcentaje<=0.66){
		claseBarra="barraModerada"
	}
	if(porcentaje>0.66 ){
		claseBarra="barraAceptable"
	}
	return claseBarra;
	
}

function fusionNoRepeticion(array1,array2){
	var arrayFinal=[];
	array1.forEach(function(val,index){
		arrayFinal.push(val);
	});
	
	array2.forEach(function(val1,index1){
		if(arrayFinal.indexOf(val1)==-1){
			arrayFinal.push(val1);
		}
	});
	return arrayFinal;
}



function devolverTextoPorcentajeAlerta(numerador,denominador){
	return (denominador>0?numerador+"/"+denominador:"Sin trabajadores asignados")
}



function dividirLista(lista){
	return (lista!=""?
			lista.split(","):[]);
}
/*Logica de presentacion Gosst*/
var fechaInicialPresentacion,fechaFinalPresentacion;
var diapositivasCargadas=0;
var diapositivasTotal=18;
var listFullEventosPresentacion=[];
var listFullCapacitacionPresentacion=[],
listFullEntregaEppPresentacion=[],listFullExamenPresentacion=[];
var listFullAuditoriasPresentacion=[],numRptaAudi=[];
var listFullInspeccionesPresentacion=[],numRptaIns=[];
var listFullIndicadoresAcc=[];
var listFullAccionMejora=[];
var listFullAccidentesPresentacion=[],listFullACSubPresentacion=[];
function verPresentacionGosst(){
	$(".fondoPresentacionGosst").remove();
	$(".diapositivaGosst").remove();
	$(".cerrarPresentacion").remove();
	
	var cierrePrese="<div class='cerrarPresentacion'>X</div>";
	var diapositiva0="<div class='diapositivaGosst' id='diapositiva0'>" +"</div>";
	var rangoFechasHtml="<form class='form-inline form-presentacion'>" 
	+"<label>Desde:</label> <input type='date' "
			+"class='form-control'" 
			+"id='fechaInicioPresentacion'"
			+"		value='2016-01-01'>"
	+"<br><br><label>Hasta:</label> "
			+"<input type='date'"
			+"class='form-control' id='fechaFinPresentacion'"
			 +" value='2016-12-01'>"
		 +"</form>";
	$("body").prepend("<div class='fondoPresentacionGosst'>" +
			"</div>"+cierrePrese+diapositiva0);
	
	$(".cerrarPresentacion").on("click",function(){
		$(".fondoPresentacionGosst").remove();
		$(".diapositivaGosst").remove();
		$(this).remove();
	});
	
	$(".fondoPresentacionGosst").after(diapositiva0);
	 $( "#diapositiva0" ).fadeIn( 1000, function() {
		
		 $( this).html("<div class='tituloPresentacion'>Informe – Comité de SST</div>"+
				 rangoFechasHtml+
				 "<br><button onclick='generarPresentacionGosst()' class='btn btn-success btn-presentacion'>Generar Presentación</button>");
	    });
	

}
function generarPresentacionGosst(){

	fechaInicialPresentacion=$("#fechaInicioPresentacion").val();
	fechaFinalPresentacion=$("#fechaFinPresentacion").val();
	var flechaPresentacionDerecha="<div class='flechaPresentacionDerecha'></div>";
	var flechaPresentacionIzquierda="<div class='flechaPresentacionIzquierda'></div>";	
	var imgRuedita="<img src='../imagenes/gif/ruedita.gif'></img>";
var mensajeCarga='<div class="mensajePresentacion">Armando Presentacion GOSST <br>'+imgRuedita+'</div>';


	$("#diapositiva0").html(mensajeCarga);
	var mensajeTraerDiapositivas="<div class='mensajePresentacion'>" +
			"Preparando Diapositivas<br>" +
			"<div class='barraLoadingDiapositivas'>" +
				"<div class='barrarCompletoDiapositivas'></div>" +
			"</div>" +
			"<div id='numDiapositivasMensaje'>0 de 11 " +imgRuedita+"</div>"
			"</div>";
	setTimeout(function(){
		$("#diapositiva0").html(mensajeTraerDiapositivas);		
		llamarDatosNecesariosPresentacionGosst();
	},2000);

	
}
function iniciarPresentacionGosst(){
	if( $("#diapositiva1").length>0){
		 $( "#diapositiva0").hide();
			var rangoFechasHtml="<form class='form-inline form-presentacion'>" 
				+"<label>Desde:</label> <input type='date' "
						+"class='form-control'" 
						+"id='fechaInicioPresentacion'"
						+"		value='2016-01-01'>"
				+"<br><br><label>Hasta:</label> "
						+"<input type='date'"
						+"class='form-control' id='fechaFinPresentacion'"
						 +" value='2016-12-01'>"
					 +"</form>";
			 $( "#diapositiva0").html("<div class='tituloPresentacion'>Informe – Comité de SST</div>"+
					 rangoFechasHtml+
					 "<br><button onclick='generarPresentacionGosst()' " +
					 "class='btn btn-success btn-presentacion'>Generar Presentación</button>");
			
			 $("#diapositiva1").show();
			 diapositivasCargadas=1;
	}

	
	 
}

function cambiarDiapositiva(cambioDerecha){
	var diapoAnterior=(diapositivasCargadas-1);
	var diapoSiguiente=(diapositivasCargadas+1);
	
		if(cambioDerecha){
			if( $("#diapositiva"+diapositivasCargadas).length &&  
					$("#diapositiva"+diapoAnterior).length)
			$("#diapositiva"+diapoAnterior).hide( "slide", { direction: "left"  }, 500,function(){
				 $("#diapositiva"+diapositivasCargadas).show("slide", { direction: "right"  }, 500,function(){
					 crearGraficoDiapositiva(diapositivasCargadas); 
				 });
				
			 });
			
		}else{
			if( $("#diapositiva"+diapositivasCargadas).length &&  
					$("#diapositiva"+diapoSiguiente).length)
			$("#diapositiva"+diapoSiguiente).hide( "slide", { direction: "right"  }, 800,function(){
				 $("#diapositiva"+diapositivasCargadas).show("slide", { direction: "left"  }, 800,function(){
					 crearGraficoDiapositiva(diapositivasCargadas);
				 });
				 
			 });	
		}
}

function cambiarBarraCarga(numDiaposAgregar){
	var imgRuedita="<img src='../imagenes/gif/ruedita.gif'></img>";
	diapositivasCargadas+=numDiaposAgregar;
	$("#numDiapositivasMensaje").html(diapositivasCargadas+" de "+diapositivasTotal+" "+imgRuedita);
console.log(diapositivasCargadas);
$(".barrarCompletoDiapositivas").css({"width":100*(diapositivasCargadas/diapositivasTotal).toFixed(2)+"%"});
if(diapositivasCargadas==diapositivasTotal){
setTimeout(function(){
	diapositivasCargadas=0;
		iniciarPresentacionGosst();
		
	},1000);
}

}
function llamarDatosNecesariosPresentacionGosst(){
	diapositivasCargadas=0;
	cambiarBarraCarga(1);
	llamarDatosAcordeCodigo(1,function(){

		cambiarBarraCarga(1);
		llamarDatosAcordeCodigo(2,function(){
			cambiarBarraCarga(4);
			llamarDatosAcordeCodigo(3,function(){
				cambiarBarraCarga(2);
				llamarDatosAcordeCodigo(4,function(){
						cambiarBarraCarga(1);
						llamarDatosAcordeCodigo(5,function(){
							cambiarBarraCarga(1);
							llamarDatosAcordeCodigo(6,function(){
								cambiarBarraCarga(1);
								llamarDatosAcordeCodigo(7,function(){
									cambiarBarraCarga(2);
									llamarDatosAcordeCodigo(8,function(){
										cambiarBarraCarga(2);
										llamarDatosAcordeCodigo(9,function(){
											cambiarBarraCarga(3);
										});
									});
								});
							});
						});
				});
			});
		});
	})
}


function llamarDatosAcordeCodigo(idCodigo,functionSuccess){

	var generarDiapositivaGosst=function(idDiapo,idgrafInside){
		var diapo="<div class='diapositivaGosst' id='diapositiva"+idDiapo+"'>" +
		
		"<div id='"+idgrafInside+"'  class='containerGraficoPresent'></div></div>";
		return diapo;
	};
	switch(idCodigo){
	case 1:
		var dataParam = {
			empresaId : sessionStorage.getItem("gestopcompanyid"),
		};
		
		callAjaxPost(URL + '/scheduler', dataParam, function(data){
			switch (data.CODE_RESPONSE) {
			case "05":
				functionSuccess();
				listFullEventosPresentacion=data.list;
				//////
				var matrizContadoraEventos=[{
														nombre:"Induccion",
														numRetrasados:0,
														numImplementar:0,
														numCompletados:0,
														numAlertas:0
															},
											{nombre:"Formación",
												numRetrasados:0,
												numImplementar:0,
												numCompletados:0,
												numAlertas:0
													},
											{nombre:"Evaluación médica",
												numRetrasados:0,
												numImplementar:0,
												numCompletados:0,
												numAlertas:0
													},
											{nombre:"ActualizaciónProcedimientos",
												numRetrasados:0,
												numImplementar:0,
												numCompletados:0,
												numAlertas:0
													},
											{nombre:"Mantenimiento de equipos de emergencia",
												numRetrasados:0,
												numImplementar:0,
												numCompletados:0,
												numAlertas:0
													},
											{nombre:"Inspección",
												numRetrasados:0,
												numImplementar:0,
												numCompletados:0,
												numAlertas:0
													},
											{nombre:"Diagnóstico-Auditoría",
												numRetrasados:0,
												numImplementar:0,
												numCompletados:0,
												numAlertas:0
													},
											{nombre:"Acciones de mejora",
												numRetrasados:0,
												numImplementar:0,
												numCompletados:0,
												numAlertas:0
													},
											{nombre:"TOTAL",
												numRetrasados:0,
												numImplementar:0,
												numCompletados:0,
												numAlertas:0
													}];
				var asignarSegunEstado=function(objetoAsignar,objetoEvaluar){
					var fechaEvaluar=objetoEvaluar.start_date.substring(0, 10);
				
var estaEnRango=fechaEnRango(fechaEvaluar,fechaInicialPresentacion,fechaFinalPresentacion);
					if(estaEnRango){
						
						switch(objetoEvaluar.estadoEvento){
						case 1:
							objetoAsignar.numImplementar+=1;
							matrizContadoraEventos[8].numImplementar+=1;
							break;
						case 2:
							objetoAsignar.numCompletados+=1;
							matrizContadoraEventos[8].numCompletados+=1;
							break;
						case 3:
							objetoAsignar.numRetrasados+=1;
							matrizContadoraEventos[8].numRetrasados+=1;
							break;
						case 4:
							objetoAsignar.numAlertas+=1;
							matrizContadoraEventos[8].numAlertas+=1;
							break;
						}
					}
				
				}
				listFullEventosPresentacion.forEach(function(val,index){
					switch(val.tipoEvento){
					case 3:
						if(parseInt(val.linkAuxiliarId2)==1){
							asignarSegunEstado(matrizContadoraEventos[0],val);
						}else{
							asignarSegunEstado(matrizContadoraEventos[1],val);
						}
						break;
					case 7:		
							asignarSegunEstado(matrizContadoraEventos[2],val);	
						break;
					case 6:		
						asignarSegunEstado(matrizContadoraEventos[3],val);	
					break;
					case 4:
						
							asignarSegunEstado(matrizContadoraEventos[4],val);
						
						break;
					case 10:		
						if(parseInt(val.linkAuxiliarId1)==2){
							asignarSegunEstado(matrizContadoraEventos[5],val);
						}else{
							asignarSegunEstado(matrizContadoraEventos[6],val);
						}
						break;
					case 8:		
						asignarSegunEstado(matrizContadoraEventos[7],val);	
					break;
					break;
					
					}
				});
				var cuerpoDetalle="";
				matrizContadoraEventos.forEach(function(val,index){
					var total=val.numImplementar+val.numCompletados+val.numRetrasados;
					cuerpoDetalle=cuerpoDetalle+"<tr>" +
							"<td>"+val.nombre+"</td>" +
							"<td>"+val.numImplementar+"</td>" +
							"<td>"+val.numCompletados+"</td>" +
							"<td>"+val.numRetrasados+"</td>" +
							"<td>"+val.numAlertas+"</td>" +
							"<td>"+pasarDecimalPorcentaje(val.numCompletados/total)+"</td>" +
							"</tr>"
				});
				var tablaDetalles="<table class='table table-striped table-bordered table-hover' style='font-size:21px'>  " +
						"<thead>" +
						"<tr>" +
							"<td class='tb-acc'>Tipo de Evento</td>" +
							"<td class='tb-acc'>En fecha</td>" +
							"<td class='tb-acc'>Realizados</td>" +"<td class='tb-acc'>Retrasados</td>" +
							"<td class='tb-acc'>Alertas</td>"+
							"<td class='tb-acc'>% de Avance</td>"+
						"</tr>" +
						"</thead>" +
						"<tbody>" +
						cuerpoDetalle+
						"</tbody>" +
						"" +
						"</table>"
				////
				
				var diapositiva1="<div class='diapositivaGosst' id='diapositiva1'>" +
				"<div class='tituloDiapositiva'>"+
						"Resumen de eventos planificados en el periodo</div><br>"+
						tablaDetalles+"</div>";
				$("body").prepend(diapositiva1);
				break;
			default:
				console.log("Hubo un error al cargar calendario.");
			}	
		},function(){},null,function(){console.log("no tiene permiso de calendario")});

		break;
	case 2:

		var dataParam5 = {
				idCompany : sessionStorage.getItem("gestopcompanyid"),
				fechaInicioPresentacion : fechaInicialPresentacion,
				fechaFinPresentacion:fechaFinalPresentacion
			};
		
			callAjaxPost(URL + '/capacitacion', dataParam5,
					function(data){
				switch (data.CODE_RESPONSE) {
				case "05":
					functionSuccess();
					listFullCapacitacionPresentacion=data.list;
					var diapositiva2=generarDiapositivaGosst("2","grafInd1");
					var diapositiva3=generarDiapositivaGosst("3","grafInd2");
					var diapositiva4=generarDiapositivaGosst("4","grafForm1");
					var diapositiva5=generarDiapositivaGosst("5","grafForm2");
					
					
					$("body").prepend(diapositiva2)
					.prepend(diapositiva3)
					.prepend(diapositiva4)
					.prepend(diapositiva5)
				
					break;
				default:
					console.log("Hubo un error al cargar auditorias.");
				}	
			},function(){},null,
					function(){
				functionSuccess();
				console.log("no tiene permiso de indicadores");
			});
		break;
	case 3:

		var dataParam1 = {
			idCompany : sessionStorage.getItem("gestopcompanyid"),
			fechaInicioPresentacion: fechaInicialPresentacion,
			fechaFinPresentacion: fechaFinalPresentacion 
			};

		callAjaxPost(URL + '/examenmedico/programacion', dataParam1, function(data){
			switch (data.CODE_RESPONSE) {
			case "05":
				functionSuccess();
				listFullExamenPresentacion=data.list;
				var diapositiva6=generarDiapositivaGosst("6","grafExam1");
				var diapositiva7=generarDiapositivaGosst("7","grafExam2");
				$("body").prepend(diapositiva6)
				.prepend(diapositiva7);
				break;
			default:
				console.log("Hubo un error al cargar calendario.");
			}	
		},function(){},null,
				function(){
			console.log("no tiene permiso de indicadores");functionSuccess();
		});
		break;
	case 4:

		var dataParam1 = {
			idCompany : sessionStorage.getItem("gestopcompanyid"),
			fechaInicioPresentacion: fechaInicialPresentacion,
			fechaFinPresentacion: fechaFinalPresentacion 
			};

		callAjaxPost(URL + '/equiposeguridad/entrega', dataParam1, function(data){
			switch (data.CODE_RESPONSE) {
			case "05":
				functionSuccess();
				listFullEntregaEppPresentacion=data.list;
				
				var diapositiva8=generarDiapositivaGosst("8","grafEpp1");
				var diapositiva9=generarDiapositivaGosst("9","grafEpp2");
				$("body")
				.prepend(diapositiva8)
				.prepend(diapositiva9);
				break;
			default:
				console.log("Hubo un error al cargar calendario.");
			}	
		},function(){},null,
				function(){
			console.log("no tiene permiso de indicadores");functionSuccess();
		});
		break;
	case 5:

		var dataParam3 = {
				idCompany : sessionStorage.getItem("gestopcompanyid"),
				auditoriaClasifId : 1
			};
			
			callAjaxPost(URL + '/auditoria', dataParam3,
					function(data){
				switch (data.CODE_RESPONSE) {
				case "05":
					functionSuccess();
					listFullAuditoriasPresentacion=data.list;
					numRptaAudi=data.listRpta;
					var diapositiva10=generarDiapositivaGosst("10","grafAudi");
					$("body").prepend(diapositiva10)
					break;
				default:
					console.log("Hubo un error al cargar auditorias.");
				}	
			},function(){},null,
					function(){
				console.log("no tiene permiso de indicadores");functionSuccess();
			});
		break;
	case 6:
		var dataParam4 = {
			idCompany : sessionStorage.getItem("gestopcompanyid"),
			auditoriaClasifId : 2
		};
		
		callAjaxPost(URL + '/auditoria', dataParam4,
				function(data){
			switch (data.CODE_RESPONSE) {
			case "05":
				functionSuccess();
				listFullInspeccionesPresentacion=data.list;
				numRptaIns=data.listRpta;
				var diapositiva11=generarDiapositivaGosst("11","grafIns");
				$("body").prepend(diapositiva11)
				break;
			default:
				console.log("Hubo un error al cargar inspecciones.");
			}	
		},function(){},null,
				function(){
			console.log("no tiene permiso de indicadores");functionSuccess();
		});
		
		break;
	case 7:
		var dataParam41 = {
			empresaId: sessionStorage.getItem("gestopcompanyid"),
			mdfId : null,
			fechaInicio: fechaInicialPresentacion,
			fechaFin: fechaFinalPresentacion ,
			tipoBusqueda:"3"
			
		};
		callAjaxPost(URL + '/estadistica', dataParam41,
				function(data){
			switch (data.CODE_RESPONSE) {
			case "05":
				functionSuccess();
				listFullIndicadoresAcc=data.numacc;
				var diapositiva12=generarDiapositivaGosst("12","grafEst");
				$("body").prepend(diapositiva12)
				break;
			default:
				console.log("Hubo un error al cargar auditorias.");
			}	
		},function(){},null,
				function(){
			console.log("no tiene permiso de indicadores");functionSuccess();
		});
		
		break;
	case 8:
		var dataParam41 = {
			idCompany : sessionStorage.getItem("gestopcompanyid")
			
		};
		
		callAjaxPost(URL + '/gestionaccionmejora', dataParam41,
				function(data){
			switch (data.CODE_RESPONSE) {
			case "05":
				functionSuccess();
				listFullAccionMejora=data.list;
				var diapositiva15=generarDiapositivaGosst("15","grafAccMejora1");
				var diapositiva16=generarDiapositivaGosst("16","grafAccMejora2");
				var diapositiva17=generarDiapositivaGosst("17","grafAccMejora3");
				$("body").prepend(diapositiva15)
				$("body").prepend(diapositiva16)
				$("body").prepend(diapositiva17)
				break;
			default:
				console.log("Hubo un error al cargar auditorias.");
			}	
		},function(){},null,
				function(){
			console.log("no tiene permiso de indicadores");functionSuccess();
		});
		
		break;
	case 9:
		var dataParam4 = {
			empresaId : sessionStorage.getItem("gestopcompanyid"),
			fechaInicio: fechaInicialPresentacion,
			fechaFin: fechaFinalPresentacion ,
			mdfIdLista:[],
			mdfId:null,
			mdfTipoId:1
			
		};
		
		callAjaxPost(URL + '/estadistica/graficos/piramide', dataParam4,
				function(data){
			switch (data.CODE_RESPONSE) {
			case "05":
				functionSuccess();
				var diapositiva13=generarDiapositivaGosst("13","grafAcc1");
				var diapositiva14=generarDiapositivaGosst("14","grafAcc2");
				listFullAccidentesPresentacion=data.accTotal;
				listFullACSubPresentacion=data.eventosTotal;
				$("body").prepend(diapositiva13)
				$("body").prepend(diapositiva14)
			
				break;
			default:
				console.log("Hubo un error al cargar auditorias.");
			}	
		},function(){},null,
				function(){
			console.log("no tiene permiso de indicadores");functionSuccess();
		});
		
		
		break;
	
	}
	
		
}


function crearGraficoDiapositiva(idDiapositiva){
	switch(idDiapositiva){
	case 2:
		var listNombreInd=[],listNombreForm=[];
		var numTrabIndPlan=[],numTrabIndAsist=[],numTrabIndNoEval=[];
		var numTrabFormPlan=[],numTrabFormAsist=[],numTrabFormNoEval=[];
		var aprobadoInd=0,noAprobadoInd=0,noEvaluadoInd=0;
		var aprobadoForm=0,noAprobadoForm=0,noEvaluadoForm=0
		;console.log(listFullCapacitacionPresentacion);
		listFullCapacitacionPresentacion.forEach(function(val,index){
			if(val.capacitacionTipoId==2){
				listNombreInd.push(val.capacitacionNombre);
				numTrabIndPlan.push(dividirLista(val.listTrabPlaneadosRango).length);
				numTrabIndAsist.push(dividirLista(val.listTrabAsistrango).length);
				numTrabIndNoEval.push(dividirLista(val.listTrabNoEvalRango).length);
				aprobadoInd+=dividirLista(val.listTrabAprobRango).length
				noAprobadoInd+=dividirLista(val.listTrabNoAprobRango).length
				noEvaluadoInd+=dividirLista(val.listTrabNoEvalRango).length
			}else{
				listNombreForm.push(val.capacitacionNombre);
				numTrabFormPlan.push(dividirLista(val.listTrabPlaneadosRango).length);
				numTrabFormAsist.push(dividirLista(val.listTrabAsistrango).length);
				numTrabFormNoEval.push(dividirLista(val.listTrabNoEvalRango).length);
				aprobadoForm+=dividirLista(val.listTrabAprobRango).length
				noAprobadoForm+=dividirLista(val.listTrabNoAprobRango).length
				noEvaluadoForm+=dividirLista(val.listTrabNoEvalRango).length
			}
		});
		 $('#grafInd1').highcharts({
		        chart: {
		            type: 'column'
		        },
		        title: {
		            text: 'Indicadores de inducción de periodo',
		            style:{fontSize:"24px"}
		        },
		        xAxis: {
		            categories:listNombreInd,
		            labels:{
		                style:{fontSize:"18px"}
		            }
		        },
		        yAxis: [{
		            min: 0,
		            title: {
		                text: 'Trabajadores',
		                style:{fontSize:"22px"}
		            },
		            labels:{
		                style:{fontSize:"15px"}
		            }
		         
		        }],
		        legend: {
		            shadow: false,
		            itemStyle: {
		                color: '#000000',
		                fontSize: '20px'
		            }
		        },
		        tooltip: {
		            shared: true,
		            headerFormat:'<span style="font-size: 18px">{point.key}</span><br/>',
		            pointFormat:'<span style="color:{point.color}">\u25CF</span> {series.name}:  <b style="font-size:20px">{point.y}</b><br/>'
		        },
		        plotOptions: {
		            column: {
		                grouping: false,
		                shadow: false,
		                borderWidth: 0
		            }
		        },
		        series: [{
		            name: 'Trabajadores planificados',
		            color: 'rgba(165,170,217,1)',
		            data: numTrabIndPlan,
		            pointPadding: 0.2,
		            pointPlacement: 0
		        }, {
		            name: 'Trabajadores asistidos',
		            color: 'rgba(126,86,134,.9)',
		            data: numTrabIndAsist,
		            pointPadding: 0.3,
		            pointPlacement: 0
		        }
		        ]
		    });
		break;
	case 3:
		var listNombreInd=[],listNombreForm=[];
		var numTrabIndPlan=[],numTrabIndAsist=[];
		var numTrabFormPlan=[],numTrabFormAsist=[];
		var aprobadoInd=0,noAprobadoInd=0,noEvaluadoInd=0;
		var aprobadoForm=0,noAprobadoForm=0,noEvaluadoForm=0;
		listFullCapacitacionPresentacion.forEach(function(val,index){
			if(val.capacitacionTipoId==2){
				listNombreInd.push(val.capacitacionNombre);
				numTrabIndPlan.push(dividirLista(val.listTrabPlaneadosRango).length);
				numTrabIndAsist.push(dividirLista(val.listTrabAsistrango).length);
				aprobadoInd+=dividirLista(val.listTrabAprobRango).length
				noAprobadoInd+=dividirLista(val.listTrabNoAprobRango).length
				noEvaluadoInd+=dividirLista(val.listTrabNoEvalRango).length
			}else{
				listNombreForm.push(val.capacitacionNombre);
				numTrabFormPlan.push(dividirLista(val.listTrabPlaneadosRango).length);
				numTrabFormAsist.push(dividirLista(val.listTrabAsistrango).length);
				aprobadoForm+=dividirLista(val.listTrabAprobRango).length
				noAprobadoForm+=dividirLista(val.listTrabNoAprobRango).length
				noEvaluadoForm+=dividirLista(val.listTrabNoEvalRango).length
			}
		});
		 $('#grafInd2').highcharts({
		        chart: {
		            plotBackgroundColor: null,
		            plotBorderWidth: null,
		            plotShadow: false,
		            type: 'pie'
		        },
		        title: {
		            text: 'Evaluación de trabajadores (Solo inducciones)',
		            style:{fontSize:"24px"}
		        },
		        tooltip: {
		        	
		            headerFormat:'<span style="font-size: 18px">{point.key}</span><br/>',
		            pointFormat:'<span style="color:{point.color}">\u25CF</span> {series.name}:  <b style="font-size:20px">{point.percentage:.1f}%</b><br/>'
		       
		        },
		        plotOptions: {
		            pie: {
		                allowPointSelect: true,
		                cursor: 'pointer',
		                dataLabels: {
		                    enabled: true,
		                    format: ' {point.y}',
		                    style: {
		                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
		                        fontSize:"19px"
		                    }
		                }
		            }
		        },	
		        legend: {
		            itemStyle: {
		                color: '#000000',
		                fontSize: '20px'
		            }
		        },
		        series: [{
		            name: 'Trabajadores',
		            colorByPoint: true,
		            showInLegend: true,
		            data: [{
		                name: 'Aprobados',
		                y: aprobadoInd,color:"#088A4B",
		            }, {
		                name: 'Desaprobados',
		                y: noAprobadoInd,color:"#D44141",
		                sliced: true,
		                selected: true
		            }, {
		                name: 'No evaluados',color:"#848484",
		                y: noEvaluadoInd
		            }]
		        }]
		    });
		break;
	case 4:
		var listNombreInd=[],listNombreForm=[];
		var numTrabIndPlan=[],numTrabIndAsist=[];
		var numTrabFormPlan=[],numTrabFormAsist=[];
		var aprobadoInd=0,noAprobadoInd=0,noEvaluadoInd=0;
		var aprobadoForm=0,noAprobadoForm=0,noEvaluadoForm=0;
		listFullCapacitacionPresentacion.forEach(function(val,index){
			if(val.capacitacionTipoId==2){
				listNombreInd.push(val.capacitacionNombre);
				numTrabIndPlan.push(dividirLista(val.listTrabPlaneadosRango).length);
				numTrabIndAsist.push(dividirLista(val.listTrabAsistrango).length);
				aprobadoInd+=dividirLista(val.listTrabAprobRango).length
				noAprobadoInd+=dividirLista(val.listTrabNoAprobRango).length
				noEvaluadoInd+=dividirLista(val.listTrabNoEvalRango).length
			}else{
				listNombreForm.push(val.capacitacionNombre);
				numTrabFormPlan.push(dividirLista(val.listTrabPlaneadosRango).length);
				numTrabFormAsist.push(dividirLista(val.listTrabAsistrango).length);
				aprobadoForm+=dividirLista(val.listTrabAprobRango).length
				noAprobadoForm+=dividirLista(val.listTrabNoAprobRango).length
				noEvaluadoForm+=dividirLista(val.listTrabNoEvalRango).length
			}
		});
		 $('#grafForm1').highcharts({
		        chart: {
		            type: 'column'
		        },
		        title: {
		            text: 'Indicadores de Formación de periodo (no incluye inducciones)',
		            style:{fontSize:"24px"}
		        },
		        xAxis: {
		            categories:listNombreForm  ,
		            labels:{
		                style:{fontSize:"18px"}
		            }
		        },
		        yAxis: [{
		            min: 0,
		            title: {
		                text: 'Trabajadores',
		               
			                style:{fontSize:"22px"}
			            
		            },
		            labels:{
		                style:{fontSize:"15px"}
		            }
		        }],
		        legend: {
		            shadow: false,
		            itemStyle: {
		                color: '#000000',
		                fontSize: '20px'
		            }
		        },
		        tooltip: {
		            shared: true,
		            headerFormat:'<span style="font-size: 18px">{point.key}</span><br/>',
		            pointFormat:'<span style="color:{point.color}">\u25CF</span> {series.name}:  <b style="font-size:20px">{point.y}</b><br/>'
		      
		        },
		        plotOptions: {
		            column: {
		                grouping: false,
		                shadow: false,
		                borderWidth: 0
		            }
		        },
		        series: [{
		            name: 'Trabajadores planificados',
		            color: 'rgba(165,170,217,1)',
		            data: numTrabFormPlan,
		            pointPadding: 0.3,
		            pointPlacement: 0
		        }, {
		            name: 'Trabajadores asistidos',
		            color: 'rgba(126,86,134,.9)',
		            data: numTrabFormAsist,
		            pointPadding: 0.4,
		            pointPlacement: 0
		        }]
		    });
		break;
	case 5:
		var listNombreInd=[],listNombreForm=[];
		var numTrabIndPlan=[],numTrabIndAsist=[];
		var numTrabFormPlan=[],numTrabFormAsist=[];
		var aprobadoInd=0,noAprobadoInd=0,noEvaluadoInd=0;
		var aprobadoForm=0,noAprobadoForm=0,noEvaluadoForm=0;
		listFullCapacitacionPresentacion.forEach(function(val,index){
			if(val.capacitacionTipoId==2){
				listNombreInd.push(val.capacitacionNombre);
				numTrabIndPlan.push(dividirLista(val.listTrabPlaneadosRango).length);
				numTrabIndAsist.push(dividirLista(val.listTrabAsistrango).length);
				aprobadoInd+=dividirLista(val.listTrabAprobRango).length
				noAprobadoInd+=dividirLista(val.listTrabNoAprobRango).length
				noEvaluadoInd+=dividirLista(val.listTrabNoEvalRango).length
			}else{
				listNombreForm.push(val.capacitacionNombre);
				numTrabFormPlan.push(dividirLista(val.listTrabPlaneadosRango).length);
				numTrabFormAsist.push(dividirLista(val.listTrabAsistrango).length);
				aprobadoForm+=dividirLista(val.listTrabAprobRango).length
				noAprobadoForm+=dividirLista(val.listTrabNoAprobRango).length
				noEvaluadoForm+=dividirLista(val.listTrabNoEvalRango).length
			}
		});
		 $('#grafForm2').highcharts({
		        chart: {
		            plotBackgroundColor: null,
		            plotBorderWidth: null,
		            plotShadow: false,
		            type: 'pie'
		        },
		        title: {
		            text: 'Evaluación de trabajadores (Sin incluir inducciones)',
		            style:{fontSize:"24px"}
		        },
		        tooltip: {
		            headerFormat:'<span style="font-size: 18px">{point.key}</span><br/>',
		            pointFormat:'<span style="color:{point.color}">\u25CF</span> {series.name}:  <b style="font-size:20px">{point.percentage:.1f}%</b><br/>'
		       
		        },
		        plotOptions: {
		            pie: {
		                allowPointSelect: true,
		                cursor: 'pointer',
		                dataLabels: {
		                    enabled: true,
		                    format: ' {point.y}',
		                    style: {
		                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
		                        fontSize:"19px"
		                    }
		                }
		            }
		        },	
		        legend: {
		            itemStyle: {
		                color: '#000000',
		                fontSize: '20px'
		            }
		        },
		        series: [{
		            name: 'Trabajadores',
		            colorByPoint: true,
		            showInLegend: true,
		            data: [{
		                name: 'Aprobados',
		                y: aprobadoForm,color:"#088A4B"
		            }, {
		                name: 'Desaprobados',
		                y: noAprobadoForm,
		                sliced: true,
		                selected: true,color:"#D44141"
		            }, {
		                name: 'No evaluados',
		                y: noEvaluadoForm,color:"#848484"
		            }]
		        }]
		    });
		break;
	case 6:
		var aptos=[0,0,0],observados=[0,0,0],noAptos=[0,0,0],noEval=[0,0,0];
		listFullExamenPresentacion.forEach(function(val,index){
			var listTrab=dividirLista(val.listTrabPlanificado);
			var listEval=dividirLista(val.listTrabEval);
			
			listTrab=listTrab.filter(function(val1,index1){
				if(listEval.indexOf(val1)==-1){
					return val1;
				}
			});
			switch(val.tipoExamenMedicoId){
			case 1:
				aptos[0]+=val.numeroEvaluadoApto+val.numeroEvaluadoRestriccion;
				observados[0]+=val.numeroEvaluadoObservado;
				noAptos[0]+=val.numeroEvaluadoNoApto;
				noEval[0]+=(listTrab.length);
				break;
			case 2:
				aptos[1]+=val.numeroEvaluadoApto+val.numeroEvaluadoRestriccion;
				observados[1]+=val.numeroEvaluadoObservado;
				noAptos[1]+=val.numeroEvaluadoNoApto;
				noEval[1]+=(listTrab.length);
				break;
			case 3:
				aptos[2]+=val.numeroEvaluadoApto+val.numeroEvaluadoRestriccion;
				observados[2]+=val.numeroEvaluadoObservado;
				noAptos[2]+=val.numeroEvaluadoNoApto;
				
				noEval[2]+=(listTrab.length);
				break;
			}
			
		});
		 $('#grafExam1').highcharts({

		        chart: {
		            type: 'column'
		        },

		        title: {
		            text: 'Indicadores de evaluaciones médicas en el periodo',
		           
		                style:{fontSize:"24px"}
		            
		        },

		        xAxis: {
		            categories: ['# de T. Nuevos', '# de T. Programados', '# de Ex Trabajadores'],
		            labels:{
		                style:{fontSize:"18px"}
		            }
		       
		        },
		        legend: {
		            itemStyle: {
		                color: '#000000',
		                fontSize: '20px'
		            }
		        },
		        yAxis: {
		            allowDecimals: false,
		            min: 0,
		            title: {
		                text: '# de Trabajadores',
		                style:{fontSize:"22px"}
		            },
		            labels:{
		                style:{fontSize:"15px"}
		            }
		        },

		        tooltip: {
		        	
		            headerFormat:'<span style="font-size: 18px">{point.key}</span><br/>',
		            pointFormat:'<span style="color:{point.color}">\u25CF</span> {series.name}:  <b style="font-size:20px">{point.y}</b><br/>'
		      
		        },

		        plotOptions: {
		            column: {
		                stacking: 'normal'
		            }
		        },

		        series: [{
		            name: 'Aptos',
		            data: aptos,
		            stack: 'good'
		        }, {
		            name: 'Observados',
		            data: observados,
		            stack: 'good'
		        }, {
		            name: 'No aptos',
		            data: noAptos,
		            stack: 'bad'
		        }, {
		            name: 'No evaluados',
		            data: noEval,
		            stack: 'bad'
		        }]
		    });
		break;
	case 7:
		var aptos=[0,0,0],observados=[0,0,0],noAptos=[0,0,0],noEval=[0,0,0];
		listFullExamenPresentacion.forEach(function(val,index){
			switch(val.tipoExamenMedicoId){
			case 1:
				aptos[0]+=val.numeroEvaluadoApto+val.numeroEvaluadoRestriccion;
				observados[0]+=val.numeroEvaluadoObservado;
				noAptos[0]+=val.numeroEvaluadoNoApto;
				noEval[0]+=(val.numeroAsistentes-val.numeroEvaluado);
				break;
			case 2:
				aptos[1]+=val.numeroEvaluadoApto+val.numeroEvaluadoRestriccion;
				observados[1]+=val.numeroEvaluadoObservado;
				noAptos[1]+=val.numeroEvaluadoNoApto;
				noEval[1]+=(val.numeroAsistentes-val.numeroEvaluado);
				break;
			case 3:
				aptos[2]+=val.numeroEvaluadoApto+val.numeroEvaluadoRestriccion;
				observados[2]+=val.numeroEvaluadoObservado;
				noAptos[2]+=val.numeroEvaluadoNoApto;
				noEval[2]+=(val.numeroAsistentes-val.numeroEvaluado);
				break;
			}
			
		});
		
		var aptosTotal=aptos[0]+aptos[1]+aptos[2];
		var obsTotal=observados[0]+observados[1]+observados[2];
		var noAptosTotal=noAptos[0]+noAptos[1]+noAptos[2];
		var noEvalTotal= noEval[0]+noEval[1]+noEval[2];
		 $('#grafExam2').highcharts({
		        chart: {
		            plotBackgroundColor: null,
		            plotBorderWidth: null,
		            plotShadow: false,
		            type: 'pie'
		        },
		        title: {
		            text: 'Evaluación médica trabajadores',
		            style:{fontSize:"24px"}
		        },
		        tooltip: {
		        	
		            headerFormat:'<span style="font-size: 18px">{point.key}</span><br/>',
		            pointFormat:'<span style="color:{point.color}">\u25CF</span> {series.name}:  <b style="font-size:20px">{point.percentage:.1f}%</b><br/>'
		      
		        },
		        plotOptions: {
		            pie: {
		                allowPointSelect: true,
		                cursor: 'pointer',
		                dataLabels: {
		                    enabled: true,
		                    format: ' {point.y}',
		                    style: {
		                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
		                        fontSize:"19px"
		                    }
		                }
		            }
		        },	
		        legend: {
		            itemStyle: {
		                color: '#000000',
		                fontSize: '20px'
		            }
		        },
		        series: [{
		            name: 'Trabajadores',
		            colorByPoint: true,
		            showInLegend: true,
		            data: [{
		                name: 'Aptos / Aptos con restricciones',
		                y: aptosTotal,color:"#088A4B"
		            }, {
		                name: 'Observados',
		                y: obsTotal,color:"#AEB404"
		            }, {
		                name: 'No aptos',
		                y: noAptosTotal,color:"#D44141"
		            }, {
		                name: 'No evaluados',
		                y:noEvalTotal,color:"#410C0C"
		            }]
		        }]
		    });
		break;
	case 8:
		var listEppsGraf=[
		                  {name:"Por Entregar",data:[],color:"#AEB404"},
		                  {name:"Entregados",data:[],color:"#4B8A08"}];
		var eppsListAux=[];
		var eppListIdAux=[];
		listFullEntregaEppPresentacion.forEach(function(val,index){
			if(val.estadoImplementacion==1){
				if(fechaEnRango(convertirFechaNormal2(val.fechaPlanificada),
						fechaInicialPresentacion,
						fechaFinalPresentacion)){
					var listaEpps=dividirLista(val.listEppId);
					var listEppNombre=dividirLista(val.listEppNombre);
					
					listaEpps.forEach(function(val1,index1){
						var sinIncluirEpp=true;
						eppsListAux.forEach(function(val2,index2){
							if(val1==val2.eppId){
								sinIncluirEpp=false;
								val2.numPlan+=val.numeroTrabajadores
							}
						});	
						
						if(sinIncluirEpp){
							eppsListAux.push({
								eppId:val1,
								nombre:listEppNombre[index1],
								numPlan:val.numeroTrabajadores,
								numEntrega:0
							})
						}
						
						
						
					});
				}
			}
			if(val.estadoImplementacion==2){
				if(fechaEnRango(convertirFechaNormal2(val.fechaEntrega),
						fechaInicialPresentacion,
						fechaFinalPresentacion)){
					var listaEpps=dividirLista(val.listEppId);
					var listEppNombre=dividirLista(val.listEppNombre);
				
					listaEpps.forEach(function(val1,index1){
						var sinIncluirEpp=true;
						eppsListAux.forEach(function(val2,index2){
							if(val1==val2.eppId){
								sinIncluirEpp=false;
								val2.numEntrega+=val.numeroTrabajadores
							}
						});	
						
						if(sinIncluirEpp){
							eppsListAux.push({
								eppId:val1,
								nombre:listEppNombre[index1],
								numPlan:0,
								numEntrega:val.numeroTrabajadores
							})
						}
						
						
						
					});
					
				}
			}
		});
		var nombreEpps=[];
		eppsListAux.forEach(function(val,index){
			nombreEpps.push(val.nombre);
			listEppsGraf[0].data.push(val.numPlan);
			listEppsGraf[1].data.push(val.numEntrega);
		});
		
		 $('#grafEpp1').highcharts({
		        chart: {
		            type: 'column'
		        },
		        title: {
		            text: 'Indicadores de seguimiento de entrega de EPP´s',
		            style:{fontSize:"24px"}
		        },
		        xAxis: {
		            categories:nombreEpps,
		            crosshair: true,
		            labels:{
		            	 style:{fontSize:"15px"}
		            }
		        
		            	
		        },
		        yAxis: {
		            min: 0,
		            title: {
		                text: '# de EPP´s',
		                style:{fontSize:"22px"}
		            },
		            
		            labels:{
		            	 style:{fontSize:"18px"}
		            }
		        },
		        legend: {
		            shadow: false,
		            itemStyle: {
		                color: '#000000',
		                fontSize: '20px'
		            }
		        },
		        tooltip: {
		        	 shared: true,
			            headerFormat:'<span style="font-size: 18px">{point.key}</span><br/>',
			            pointFormat:'<span style="color:{point.color}">\u25CF</span> {series.name}:  <b style="font-size:20px">{point.y}</b><br/>'
			      
		        },
		        plotOptions: {
		            column: {
		                pointPadding: 0.2,
		                borderWidth: 0
		            }
		        },
		        series: listEppsGraf
		    });
		break;
	case 9:
		var nuevoTrab=0,renovacion=0,extravio=0,robo=0,otro=0;
		listFullEntregaEppPresentacion.forEach(function(val,index){
			if(val.estadoImplementacion==2 ){
				if(fechaEnRango(convertirFechaNormal2(val.fechaEntrega),
							fechaInicialPresentacion,
							fechaFinalPresentacion)){
					switch(val.motivoId){
					case 1:
						nuevoTrab+=val.numeroEquipos
						break;
					case 2:
						renovacion+=val.numeroEquipos
						break;
					case 3:
						extravio+=val.numeroEquipos
						break;
					case 4:
						robo+=val.numeroEquipos
						break;
					case 5:
						otro+=val.numeroEquipos
						break;
					}
				}
			}
			
		});
		 $('#grafEpp2').highcharts({
		        chart: {
		            plotBackgroundColor: null,
		            plotBorderWidth: null,
		            plotShadow: false,
		            type: 'pie'
		        },
		        title: {
		            text: 'Equipos entregados por motivo',
		            style:{fontSize:"24px"}
		        },
		        tooltip: {
			            headerFormat:'<span style="font-size: 18px">{point.key}</span><br/>',
			            pointFormat:'<span style="color:{point.color}">\u25CF</span> {series.name}:  <b style="font-size:20px">{point.percentage:.1f}%</b><br/>'
			      
		        },
		        plotOptions: {
		            pie: {
		                allowPointSelect: true,
		                cursor: 'pointer',
		                dataLabels: {
		                    enabled: true,
		                    format: '{point.y}',
		                    style: {
		                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
		                        fontSize:"19px"
		                    }
		                }
		            }
		        },	
		        legend: {
		            itemStyle: {
		                color: '#000000',
		                fontSize: '20px'
		            }
		        },
		        series: [{
		            name: 'EPP´s',
		            colorByPoint: true,
		            showInLegend: true,
		            data: [{
		                name: 'Nuevo Trabajador',
		                y: nuevoTrab,color:"#3D58A1"
		            }, {
		                name: 'Renovación',
		                y: renovacion,color:"#278A27"
		            }, {
		                name: 'Extravío',
		                y: extravio,color:"#5F247F"
		            }, {
		                name: 'Robos',
		                y:robo,color:"#7F3124"
		            },{
		            	name:"Otros",
		                y:otro,color:"#37211E"
		            }]
		        }]
		    });
		break;
	case 10:
		var auditoriasNombreEnRango=[],arrayCumple=[],arrayParcial=[],arrayNoCumple=[],arrayAcciones=[];
		var totalCumple=0,totalParcial=0,totalNoCumple=0;
		listFullAuditoriasPresentacion.forEach(function(val,index){
		if(val.estadoImplementacion==2){
			if(fechaEnRango(convertirFechaNormal2(val.auditoriaFecha),
					fechaInicialPresentacion,
					fechaFinalPresentacion)){
				auditoriasNombreEnRango.push(val.auditoriaNombre);
				totalCumple+=(numRptaAudi[index].rptaSi+numRptaAudi[index].rptaRP);
				arrayCumple.push(numRptaAudi[index].rptaSi+numRptaAudi[index].rptaRP);
				totalParcial+=(numRptaAudi[index].rptaParcial);
				arrayParcial.push(numRptaAudi[index].rptaParcial);
				totalNoCumple+=(numRptaAudi[index].rptaNo);
				arrayNoCumple.push(numRptaAudi[index].rptaNo);
				arrayAcciones.push(val.accionesTotales);
			}
		}
			
		});
		$("#grafAudi").highcharts({
			title: {
	            text: 'Indicadores de Diagnósticos realizados en el periodo',
	            style:{fontSize:"24px"}
	        },
	        xAxis: {
	            categories: auditoriasNombreEnRango,
	            labels:{
	            	style:{fontSize:"18px"}
	            		}
	            	
	            
	        },
	        yAxis: {
	            min: 0,
	            title: {
	                text: 'Requisitos',
	                style:{fontSize:"22px"}
	            },
	            
	            labels:{
	            	 style:{fontSize:"18px"}
	            }
	        },
	        legend: {
	            itemStyle: {
	                color: '#000000',
	                fontSize: '20px'
	            }
	        },
	        labels: {
	            items: [{
	                html: 'Resumen de indicadores de cumplimiento',
	                style: {
	                    left: '50px', fontSize: '20px',
	                    top: '18px',
	                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
	                }
	            }]
	        },
	        series: [{
	            type: 'column',
	            name: 'Cumple Requisito',
	            data: arrayCumple,
	            tooltip:{   
	            	headerFormat:'<span style="font-size: 18px">{point.key}</span><br/>',
	                pointFormat:'<span style="color:{point.color}">\u25CF</span> {series.name}:  <b style="font-size:20px">{point.y}</b><br/>'
	                    }
	        }, {
	            type: 'column',
	            name: 'Cumple Parcialmente',
	            data: arrayParcial,
	            tooltip:{   
	            	headerFormat:'<span style="font-size: 18px">{point.key}</span><br/>',
	                pointFormat:'<span style="color:{point.color}">\u25CF</span> {series.name}:  <b style="font-size:20px">{point.y}</b><br/>'
	                    }
	        }, {
	            type: 'column',
	            name: 'No cumple',
	            data: arrayNoCumple,
	            tooltip:{   
	            	headerFormat:'<span style="font-size: 18px">{point.key}</span><br/>',
	                pointFormat:'<span style="color:{point.color}">\u25CF</span> {series.name}:  <b style="font-size:20px">{point.y}</b><br/>'
	                    }
	        }, {
	            type: 'spline',
	            name: 'Acciones de mejora generada',
	            data: arrayAcciones,
	            marker: {
	                lineWidth: 2,
	                lineColor: Highcharts.getOptions().colors[3],
	                fillColor: 'white'
	            },
	            tooltip:{   
	            	headerFormat:'<span style="font-size: 18px">{point.key}</span><br/>',
	                pointFormat:'<span style="color:{point.color}">\u25CF</span> {series.name}:  <b style="font-size:20px">{point.y}</b><br/>'
	                    }
	        }, {
	            type: 'pie',
	            name: 'Resumen de indicadores de cumplimiento',
	            data: [{
	                name: 'Cumple Requisito',
	                y: totalCumple,
	                color: Highcharts.getOptions().colors[0] // Jane's color
	            }, {
	                name: 'Cumple Parcialmente',
	                y: totalParcial,
	                color: Highcharts.getOptions().colors[1] // John's color
	            }, {
	                name: 'No cumple',
	                y: totalNoCumple,
	                color: Highcharts.getOptions().colors[2] // Joe's color
	            }],
	            center: [100, 80],
	            size: 100,
	            showInLegend: false,
	            dataLabels: {
	                enabled: false
	            },
	            tooltip:{   
	            	headerFormat:'<span style="font-size: 18px">{point.key}</span><br/>',
	                pointFormat:'<span style="color:{point.color}">\u25CF</span> {series.name}:  <b style="font-size:20px">{point.y}</b><br/>'
	                    }
	        }]
		});
		break;
	case 11:
		var auditoriasNombreEnRango=[],arrayCumple=[],arrayParcial=[],arrayNoCumple=[],arrayAcciones=[];
		var totalCumple=0,totalParcial=0,totalNoCumple=0;
		listFullInspeccionesPresentacion.forEach(function(val,index){
		if(val.estadoImplementacion==2){
			if(fechaEnRango(convertirFechaNormal2(val.auditoriaFecha),
					fechaInicialPresentacion,
					fechaFinalPresentacion)){
				auditoriasNombreEnRango.push(val.auditoriaNombre);
				totalCumple+=(numRptaIns[index].rptaSi+numRptaIns[index].rptaRP);
				arrayCumple.push(numRptaIns[index].rptaSi+numRptaIns[index].rptaRP);
				
				totalParcial+=(numRptaIns[index].rptaParcial);
				arrayParcial.push(numRptaIns[index].rptaParcial);
				
				totalNoCumple+=(numRptaIns[index].rptaNo);
				arrayNoCumple.push(numRptaIns[index].rptaNo);
				
				arrayAcciones.push(val.accionesTotales);
			}
		}
			
		});
		$("#grafIns").highcharts({
			title: {
	            text: 'Indicadores de Inspecciones realizados en el periodo',
	            style:{fontSize:"24px"}
	        },
	        xAxis: {
	            categories: auditoriasNombreEnRango,
	            labels:{
	            	style:{fontSize:"18px"}
	            		}
	            	
	            
	        },
	        yAxis: {
	            min: 0,
	            title: {
	                text: 'Requisitos',
	                style:{fontSize:"22px"}
	            },
	            
	            labels:{
	            	 style:{fontSize:"18px"}
	            }
	        },
	        legend: {
	            itemStyle: {
	                color: '#000000',
	                fontSize: '20px'
	            }
	        },
	        labels: {
	            items: [{
	                html: 'Resumen de indicadores de cumplimiento',
	                style: {
	                    left: '50px', fontSize: '20px',
	                    top: '18px',
	                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
	                }
	            }]
	        },
	        series: [{
	            type: 'column',
	            name: 'Cumple Requisito',
	            data: arrayCumple,

	            tooltip:{   
	            	headerFormat:'<span style="font-size: 18px">{point.key}</span><br/>',
	                pointFormat:'<span style="color:{point.color}">\u25CF</span> {series.name}:  <b style="font-size:20px">{point.y}</b><br/>'
	                    }
	        }, {
	            type: 'column',
	            name: 'Cumple Parcialmente',
	            data: arrayParcial,
	            tooltip:{   
	            	headerFormat:'<span style="font-size: 18px">{point.key}</span><br/>',
	                pointFormat:'<span style="color:{point.color}">\u25CF</span> {series.name}:  <b style="font-size:20px">{point.y}</b><br/>'
	                    }
	        }, {
	            type: 'column',
	            name: 'No cumple',
	            data: arrayNoCumple,
	            tooltip:{   
	            	headerFormat:'<span style="font-size: 18px">{point.key}</span><br/>',
	                pointFormat:'<span style="color:{point.color}">\u25CF</span> {series.name}:  <b style="font-size:20px">{point.y}</b><br/>'
	                    }
	        }, {
	            type: 'spline',
	            name: 'Acciones de mejora generada',
	            data: arrayAcciones,
	            marker: {
	                lineWidth: 2,
	                lineColor: Highcharts.getOptions().colors[3],
	                fillColor: 'white'
	            },
	            tooltip:{   
	            	headerFormat:'<span style="font-size: 18px">{point.key}</span><br/>',
	                pointFormat:'<span style="color:{point.color}">\u25CF</span> {series.name}:  <b style="font-size:20px">{point.y}</b><br/>'
	                    }
	        }, {
	            type: 'pie',
	            name: 'Resumen de indicadores de cumplimiento',
	            data: [{
	                name: 'Cumple Requisito',
	                y: totalCumple,
	                color: Highcharts.getOptions().colors[0] // Jane's color
	            }, {
	                name: 'Cumple Parcialmente',
	                y: totalParcial,
	                color: Highcharts.getOptions().colors[1] // John's color
	            }, {
	                name: 'No cumple',
	                y: totalNoCumple,
	                color: Highcharts.getOptions().colors[2] // Joe's color
	            }],
	            center: [100, 80],
	            size: 100,
	            showInLegend: false,
	            dataLabels: {
	                enabled: false
	            },
	            tooltip:{   
	            	headerFormat:'<span style="font-size: 18px">{point.key}</span><br/>',
	                pointFormat:'<span style="color:{point.color}">\u25CF</span> {series.name}:  <b style="font-size:20px">{point.y}</b><br/>'
	                    }
	        }]
		});
		break;
	case 12:
		var numEnfermedades=0;
		var numAccidentes=0;var numDias=restaFechas(fechaInicialPresentacion,fechaFinalPresentacion);
		var numHH=0,numDiasDescanso=0,numDanioMaterial=0;
		listFullIndicadoresAcc.forEach(function(val,index){
			switch(val.consultaId){
			case 1:
				numAccidentes=val.indicador;
				break;
			case 2:
				numEnfermedades=val.indicador;
				break;
			case 6:
				numDiasDescanso=val.indicador;
				break;
			case 8:
				numDanioMaterial=val.indicador;
				break;
			case 9:
				numHH=val.indicador*7.5*numDias;
				break;
			}
			
		});
		var cuerpoDetalle=";";
		var indiceFrec=(numAccidentes*1000000/numHH).toFixed(2);
		var indiceSev=(numDiasDescanso*1000000/numHH).toFixed(2)
			var tablaDetalles="<table class='table table-striped table-bordered table-hover' style='font-size:21px'>  " +
						"<thead>" +
						"<tr>" +
							"<td class='tb-acc'>Descripción</td>" +
							"<td class='tb-acc'>Indicador</td>" +
							
						"</tr>" +
						"</thead>" +
						"<tbody>" +
						"<tr>" +
						"<td>HH Trabajadas en el periodo</td>" +
						"<td>"+numHH+"</td>" +
						"</tr>"+
						"<tr>" +
						"<td># de enfermedades ocupacionales</td>" +
						"<td>"+numEnfermedades+"</td>" +
						"</tr>"+
						"<tr>" +
						"<td>Índice de morbilidad (1’000,000 HH)</td>" +
						"<td>"+(numEnfermedades*1000000/numHH).toFixed(2)+"</td>" +
						"</tr>"+
						"<tr>" +
						"<td># de accidentes</td>" +
						"<td>"+numAccidentes+"</td>" +
						"</tr>"+
						"<tr>" +
						"<td>Días de descanso médico</td>" +
						"<td>"+numDiasDescanso+"</td>" +
						"</tr>"+
						"<tr>" +
						"<td>Daños Materiales (S/)</td>" +
						"<td>"+numDanioMaterial+"</td>" +
						"</tr>"+
						"<tr>" +
						"<td>Índice de frecuencia (1’000,000 HH)</td>" +
						"<td>"+indiceFrec+"</td>" +
						"</tr>"+
						"<tr>" +
						"<td>índice de severidad (1’000,000 HH)</td>" +
						"<td>"+indiceSev+"</td>" +
						"</tr>"+
						"<tr>" +
						"<td>Índice de Accidentabilidad (1’000,000 HH)</td>" +
						"<td>"+(indiceSev*indiceFrec/1000).toFixed(2)+"</td>" +
						"</tr>"+
						"</tbody>" +
						"" +
						"</table>"
				////
				
				var diapositiva12="<div class='tituloDiapositiva'>"+
						"Resumen de eventos planificados en el periodo</div><br>"+
						tablaDetalles+"";
		$("#diapositiva12").html(diapositiva12);
		
		
		break;
	case 13:
		
		var campoAcc=[];
		var campoAccInc1=[];
		var campoAccInc2=[];
		var campoAccInc3=[];
		var campoAccInc4=[];
		var campoAccLeve=[];
		var campoInc=[];
		var campoActo=[];
		var campoCondicion=[];
			
		
		
			campoAcc=(["Accidentes Fatales",listFullAccidentesPresentacion[0].numAccFatal]);
			campoAccInc1=(["Inc. Total Temporal",listFullAccidentesPresentacion[0].numAccIncap1]);
			campoAccInc2=(["Inc. Parcial Temporal",listFullAccidentesPresentacion[0].numAccIncap2]);
			campoAccInc3=(["Inc Parcial Permanente",listFullAccidentesPresentacion[0].numAccIncap3]);
			campoAccInc4=(["Inc. Total Permanente",listFullAccidentesPresentacion[0].numAccIncap4]);
			campoAccLeve=(["Accidentes Leves",listFullAccidentesPresentacion[0].numAccLeve]);
			campoInc=(["Incidentes",listFullAccidentesPresentacion[0].numIncidente]);
			campoActo=(["Actos y Condiciones",listFullACSubPresentacion[0].numActos+listFullACSubPresentacion[0].numCond]);
			
		
	var datosTotal=[campoActo,campoAccLeve,campoAccInc4,campoAccInc3,campoAccInc2,campoAccInc1,campoAcc];
			
		$("#grafAcc1").highcharts({
			  chart: {
		            type: 'pyramid',
		            marginRight: 100
		        },
		        title: {
		            text: 'Pirámide de Accidentabilidad en el periodo',
		            x: -50
		        },
		        plotOptions: {
		            series: {
		                dataLabels: {
		                    enabled: true,
		                    format: '<b style="font-size:20px">{point.name}</b><b style="font-size:18px"> ({point.y:,.0f})</b>',
		                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
		                    softConnector: true
		                }
		            }
		        },
		        legend: {
		            enabled: false
		        },
		        series: [{
		            name: '#',colors:["#E3650B","#CE6418",
		                              "#E34C0B","#9C3509","#C43737","#B02828",
		                              "#8C1919"],
		            data: datosTotal
		        }]
		   
		});
		
		break;
	case 14:

	var	campoAcc=listFullAccidentesPresentacion[0].numAccFatal;
	var	campoAccInc=listFullAccidentesPresentacion[0].numAccIncap1+
	listFullAccidentesPresentacion[0].numAccIncap2+
	listFullAccidentesPresentacion[0].numAccIncap3+
	listFullAccidentesPresentacion[0].numAccIncap4;
	var	campoAccLeve=listFullAccidentesPresentacion[0].numAccLeve;
	var	campoInc=listFullAccidentesPresentacion[0].numIncidente;
	var	campoActo=listFullACSubPresentacion[0].numActos;
	var campoCond=listFullACSubPresentacion[0].numCond;
		
	
		$("#grafAcc2").highcharts({
			chart: {
	            type: 'column'
	        },
	        title: {
	            text: 'Indicadores de accidentabilidad',
	            style:{fontSize:"22px"}
	        },
	        xAxis: {
	            type: 'category',
	            labels:{
	            	style:{fontSize:"18px"}
	            }
	        },
	        yAxis: {
	            title: {
	                text: 'Cantidad',
	                	style:{fontSize:"22px"}
	            },
	        labels:{
            	style:{fontSize:"18px"}
            }
	        },
	        legend: {
	            enabled: false,
	            itemStyle:{
	            	fontSize:"15px"
	            }
	        },
	        plotOptions: {
	            series: {
	                borderWidth: 0,
	                dataLabels: {
	                    enabled: true,
	                    format: '{point.percentage:.1f}%'
	                }
	            }
	        },

	        tooltip: {
	            headerFormat:'<span style="font-size: 18px">{point.key}</span><br/>',
	            pointFormat:'<span style="color:{point.color}">\u25CF</span> {series.name}:  <b style="font-size:20px">{point.y}</b><br/>'
	      },

	        series: [{
	            name: '------',
	            colorByPoint: true,
	            data: [{
	                name: 'Actos Subestándar',
	                y: campoActo,
	                drilldown: 'numAct'
	            }, {
	                name: 'Condiciones Subestándar',
	                y: campoCond,
	                drilldown: 'numCond'
	            }, {
	                name: 'Incidentes',
	                y: campoInc,
	                drilldown: 'numInc'
	            }, {
	                name: 'Accidentes Leves',
	                y: campoAccLeve,
	                drilldown: 'numLeve'
	            }, {
	                name: 'Accidentes Incapacitantes',
	                y: campoAccInc,
	                drilldown: 'numIncap'
	            }, {
	                name: 'Accidentes Fatales',
	                y: campoAcc,
	                drilldown: 'numFatal'
	            }]
	        }],
	        drilldown: {}
	  
		});
		break;
	case 15:
		var noConformidad=0,riesgo=0,oportunidad=0,observacion=0,comite=0;
		
		listFullAccionMejora.forEach(function(val,index){
			console.log(val)
			switch(val.causaId){
			case 1:
				noConformidad+=1;
				break;
			case 2:
				riesgo+=1;
				break;
			case 3:
				observacion+=1;
				
				break;
			case 4:
				oportunidad+=1;
				break;
			case 5:
				comite+=1;
				break;
			}
		})
	 $('#grafAccMejora1').highcharts({
		        chart: {
		            plotBackgroundColor: null,
		            plotBorderWidth: null,
		            plotShadow: false,
		            type: 'pie'
		        },
		        title: {
		            text: 'Motivo de origen de la solicitud de acción de mejora',
		            style:{fontSize:"24px"}
		        },
		        tooltip: {
		            headerFormat:'<span style="font-size: 18px">{point.key}</span><br/>',
		            pointFormat:'<span style="color:{point.color}">\u25CF</span> {series.name}:  <b style="font-size:20px">{point.percentage:.1f}%</b><br/>'
		      
		        },
		        plotOptions: {
		            pie: {
		                allowPointSelect: true,
		                cursor: 'pointer',
		                dataLabels: {
		                    enabled: true,
		                    format: ' {point.y}',
		                    style: {
		                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
		                        fontSize:"19px"
		                    }
		                }
		            }
		        },	
		        legend: {
		            itemStyle: {
		                color: '#000000',
		                fontSize: '20px'
		            }
		        },
		        
		        
		        series: [{
		            name: 'Motivos',
		            colorByPoint: true,
		            showInLegend: true,
		            data: [{
		                name: 'No conformidad',
		                y: noConformidad,color:"#3D58A1"
		            }, {
		                name: 'Riesgo potencial',
		                y: riesgo,color:"#278A27"
		            }, {
		                name: 'Observación',
		                y: observacion,color:"#5F247F"
		            }, {
		                name: 'Oportunidad de mejora',
		                y:oportunidad,color:"#7F3124"
		            },{
		            	name:"Decisión del Comité",
		                y:comite,color:"#37211E"
		            }]
		        }]
		    });

		break;
	case 16:
		var numCorr=0,numPrev=0;
		listFullAccionMejora.forEach(function(val,index){
			console.log(val);
			switch(val.solicitud.id){
			case 1:
				numPrev+=1;
				break;
			case 2:
				numCorr+=1;
				break;
			}
		})
$("#grafAccMejora2").highcharts({
	chart: {
        type: 'column'
    },
    title: {
        text: 'Indicadores de solicitud de acciones de mejora generadas en el periodo',
        style:{fontSize:"22px"}
    },
    xAxis: {
        type: 'category',
        labels:{
        	style:{fontSize:"18px"}
        }
    },
    yAxis: {
        title: {
            text: 'Cantidad',
            	style:{fontSize:"22px"}
        },
    labels:{
    	style:{fontSize:"18px"}
    }
    },
    legend: {
        enabled: false,
        itemStyle:{
        	fontSize:"15px"
        }
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y}'
            }
        }
    },

    tooltip: {
    	   headerFormat:'<span style="font-size: 18px">{point.key}</span><br/>',
           pointFormat:'<span style="color:{point.color}">\u25CF</span> {series.name}:  <b style="font-size:20px">{point.y}</b><br/>'
     },

    series: [{
        name: '------',
        colorByPoint: true,
        data: [{
            name: 'Preventivas',
            y: numPrev,
            drilldown: 'numPrev'
        }, {
            name: 'Correctivas',
            y: numCorr,
            drilldown: 'numCorr'
        }]
    }],
    drilldown: {}

		});
		break;
	case 17:
		var numCorr=0,numPrev=0;
		listFullAccionMejora.forEach(function(val,index){
			
			if(val.declaracionCierre.trim().length>0 || val.estadoId==2){
				switch(val.solicitud.id){
				case 1:
					numPrev+=1;
					break;
				case 2:
					numCorr+=1;
					break;
				}
			}
			
		})
$("#grafAccMejora3").highcharts({
	chart: {
        type: 'column'
    },
    title: {
        text: 'Indicadores de solicitud de acciones de mejora cerradas en el periodo',
        style:{fontSize:"22px"}
    },
    xAxis: {
        type: 'category',
        labels:{
        	style:{fontSize:"18px"}
        }
    },
    yAxis: {
        title: {
            text: 'Cantidad',
            	style:{fontSize:"22px"}
        },
    labels:{
    	style:{fontSize:"18px"}
    }
    },
    legend: {
        enabled: false,
        itemStyle:{
        	fontSize:"15px"
        }
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y}'
            }
        }
    },

    tooltip: {
    	   headerFormat:'<span style="font-size: 18px">{point.key}</span><br/>',
           pointFormat:'<span style="color:{point.color}">\u25CF</span> {series.name}:  <b style="font-size:20px">{point.y}</b><br/>'
      },

    series: [{
        name: '------',
        colorByPoint: true,
        data: [{
            name: 'Preventivas',
            y: numPrev,
            drilldown: 'numPrev'
        }, {
            name: 'Correctivas',
            y: numCorr,
            drilldown: 'numCorr'
        }]
    }],
    drilldown: {}

		});
		break;
	}
	
	 
}


function copiarAlPortapapeles(texto,divId) {
	  var aux = "<textarea id='auxClip'>"+texto+"</textarea>"
	  if(divId!=null){
		  $("#"+divId).after(aux);
	  }else{
		  $("body").after(aux);
	  }
	
	  $("#auxClip").select();
	  document.execCommand("copy");
	 
	  $("#auxClip").remove();
	}
function resizeDivGosst(divId,contieneBuscador) {
	var factorBuscador=0,factorEspecial=0;
	if(contieneBuscador==1){
		factorBuscador=40;
	}
 
	var vpw = $(window).width();
	var vph = $(window).height();
	vph = vph - 240+factorMovil-factorBuscador;
	$("#"+divId).css({
		"height" : vph + "px"
	});
}
function ifVerificarMovil(tipoMovil,param1,param2){
var widthAux=0;
	switch(tipoMovil){
	case 1:
		widthAux=786
		break;
	case 2:
		widthAux=1000
		break;
	}
	if($(window).width()<widthAux){
		return param1;
	}else{
		return param2;
	}
}


function defaultFor(arg, val) { 
	return typeof arg !== 'undefined' ? arg : val; 
	}

function findValueArray(array,arrayValueId,arrayValueName,valueId){
	var valueReturn="";
	array.every(function(val,index){
		if(val[arrayValueId]==valueId){
			valueReturn= val[arrayValueName];
			return true;
		}else{
			return true;
		}
	});
	
	return valueReturn;
}

function comprabarImagen(archivoNombre){
	var isImage=false;
    var ext=['gif','jpg','jpeg','png',""];
    var archivoDividido=archivoNombre.split('.');
    var extension="gif";
    if(archivoDividido.length>1){
    	  extension=archivoDividido.pop().toLowerCase()
    } 
    for(var i=0,n;n=ext[i];i++){
        if(n.toLowerCase()==extension){
        	isImage= true;
        }
         
    }
    return isImage
	
	
}

function convertirFechaTexto(inputFecha){
	var fechaDate;
	if(!inputFecha){
		fechaDate=null;
	}else{
		var mes1 =inputFecha.substring(5, 7) - 1;
		var fechatemp1 = new Date(inputFecha.substring(0, 4), mes1, inputFecha.substring(8, 10));

		 fechaDate = fechatemp1;
	}
	
	
	return fechaDate;
}

function comprobarPorcentaje(e){
	var keyCode = e.which ? e.which : e.keyCode; 
	var valuePorc=parseFloat(e.srcElement.value);
	if(valuePorc>100 || valuePorc<0){
		e.srcElement.value=0;
		alert("Ingresar porcentaje valido");
		return false;
	}
}

function guardarEvidenciaAuto(archivoId,inputFileId,bitsMax,urlEvidencia,functionCallback){
	var archivoIdAux=archivoId;
	var inputFileImage = document.getElementById(inputFileId);
	var file = inputFileImage.files[0];
	var data = new FormData();
	
	if(file){
		if (file.size > bitsMax) {
			alert("Lo sentimos, solo se pueden subir archivos menores a "+bitsMax/1000000+" MB");
			functionCallback();
		} else {
			data.append("fileEvi", file);
			data.append("id", archivoId);
			var url = URL + urlEvidencia;
			 
			$.ajax({
						url : url,
						xhrFields: {
				            withCredentials: true
				        },
						type : 'POST',
						contentType : false,
						data : data,
						processData : false,
						cache : false,
						success : function(data, textStatus, jqXHR) {
						 
							switch (data.CODE_RESPONSE) {
							case "06":
								sessionStorage.clear();
								document.location.replace(data.PATH);
								break;
							default:
							 
								functionCallback();
							}

						},
						error : function(jqXHR, textStatus, errorThrown) {
							$.unblockUI();
							alert("Ups, tenemos problemas tecnicos. Intente más tarde. Error: "
									+ errorThrown);
							console.log('xhRequest: ' + jqXHR + "\n");
							console.log('ErrorText: ' + textStatus + "\n");
							console.log('thrownError: ' + errorThrown + "\n");
						}
					});
		}
	
	}else{
		functionCallback();
	}
	
}

function guardarEvidenciaAutoImport(archivoId,inputFileId,bitsMax,urlEvidencia,
	functionCallback,fieldEvidencia,arrayDataAppend){
fieldEvidencia=defaultFor(fieldEvidencia,"id");
var archivoIdAux=archivoId; 
var inputFileImage = document.getElementById(inputFileId);

var file = inputFileImage.files[0];
var data = new FormData(); 
if(file){
	if(file.name.indexOf(",")!=-1){
		alert("Por favor,ingrese un archivo sin comas (,)");
		$.unblockUI();
		return;
	}
	if (file.size > bitsMax) {
		alert("Lo sentimos, solo se pueden subir archivos menores a "+bitsMax/1000000+" MB");
		functionCallback();
	} else {
		var realizarCompresion= true;
		var filterType = /^(?:image\/bmp|image\/cis\-cod|image\/gif|image\/ief|image\/jpeg|image\/jpeg|image\/jpeg|image\/pipeg|image\/png|image\/svg\+xml|image\/tiff|image\/x\-cmu\-raster|image\/x\-cmx|image\/x\-icon|image\/x\-portable\-anymap|image\/x\-portable\-bitmap|image\/x\-portable\-graymap|image\/x\-portable\-pixmap|image\/x\-rgb|image\/x\-xbitmap|image\/x\-xpixmap|image\/x\-xwindowdump)$/i;
		if (!filterType.test(file.type) || file.size < bitsMega*1.5) {
			realizarCompresion = false;
		}
		if(realizarCompresion){
			var fileReader = new FileReader();
			fileReader.readAsDataURL(file);
			
			fileReader.onload = function (event) {
			  var image = new Image();
			  image.onload=function(){
			     var canvas=document.createElement("canvas");
			      var context=canvas.getContext("2d");
			      var MAX_WIDTH = 800;
			      var MAX_HEIGHT = 600;
			      var width = image.width;
			      var height = image.height;
			       
			      if (width > height) {
			        if (width > MAX_WIDTH) {
			          height *= MAX_WIDTH / width;
			          width = MAX_WIDTH;
			        }
			      } else {
			        if (height > MAX_HEIGHT) {
			          width *= MAX_HEIGHT / height;
			          height = MAX_HEIGHT;
			        }
			      }
			      canvas.width = width;
			      canvas.height = height;
			      context.drawImage(image,
			          0,
			          0,
			          image.width,
			          image.height,
			          0,
			          0,
			          canvas.width,
			          canvas.height
			      );
			      canvas.toBlob(function(blob){
			    	  
			    	  var fileFinal = new File([blob],file.name);
			    	  data.append("fileEvi", fileFinal);
						data.append("fechaSubida", new Date());
						data.append("fecha", $("#dateActividad").val());
						if(arrayDataAppend){
							arrayDataAppend.forEach(function(val,index){
								data.append(val.id, val.value);
							})
						}
						data.append(fieldEvidencia, archivoId);
						var url = URL + urlEvidencia;
						 
						$.ajax({
									url : url,
									type : 'POST',
									beforeSend:function(){
										$.blockUI({message:'<img src="../imagenes/gif/ruedita.gif"></img>cargando...'}); 
									},
									xhrFields: {
							            withCredentials: true
							        },
									contentType : false,
									data : data,
									processData : false,
									cache : false,
									success : function(data, textStatus, jqXHR) {
										
										switch (data.CODE_RESPONSE) {
										case "06":
											sessionStorage.clear();
											document.location.replace(data.PATH);
											break;
										default:
										 console.log("Prueba 1.2");
										$("#"+inputFileId).parent("span").find("#labelProgress").remove();
											functionCallback(data);
										}

									},
									complete:function(){
										$.unblockUI();
									},
									error : function(jqXHR, textStatus, errorThrown) {
										$.unblockUI();
										alert("Estimado usuario, no excederse de 14mb o 30 caracteres del nombre del archivo");
										console.log('xhRequest: ' + jqXHR + "\n");
										console.log('ErrorText: ' + textStatus + "\n");
										console.log('thrownError: ' + errorThrown + "\n");
									},
									// Custom XMLHttpRequest
							        xhr: function() {
							            var myXhr = $.ajaxSettings.xhr();
							            if (myXhr.upload) {
							                // For handling the progress of the upload
							                myXhr.upload.addEventListener('progress', function(e) {
							                    if (e.lengthComputable) {
							                    	var progreso=e.loaded/e.total;
							                    	console.log(pasarDecimalPorcentaje(progreso));
							                    	$("#"+inputFileId).parent("span").find("#labelProgress").remove();
							                    	$("#"+inputFileId).parent("span").append(
							                    			"<label id='labelProgress'>"
							                    			+pasarDecimalPorcentaje(progreso)+"</label>");
							                    	
							                    }
							                } , false);
							            }
							            return myXhr;
							        }
									
								});}, 'image/jpeg', 1);
			  }
			  image.src=event.target.result;
			  
			};
		}else{
			data.append("fileEvi", file);
			data.append("fechaSubida", new Date());
			data.append("fecha", $("#dateActividad").val());
			if(arrayDataAppend){
				arrayDataAppend.forEach(function(val,index){
					data.append(val.id, val.value);
				})
			}
			data.append(fieldEvidencia, archivoId);
			var url = URL + urlEvidencia;
			 
			$.ajax({
						url : url,
						type : 'POST',
						beforeSend:function(){
							$.blockUI({message:'<img src="../imagenes/gif/ruedita.gif"></img>cargando...'}); 
						},
						xhrFields: {
				            withCredentials: true
				        },
						contentType : false,
						data : data,
						processData : false,
						cache : false,
						success : function(data, textStatus, jqXHR) {
							
							switch (data.CODE_RESPONSE) {
							case "06":
								sessionStorage.clear();
								document.location.replace(data.PATH);
								break;
							default:
							 console.log("Prueba 1.2");
							$("#"+inputFileId).parent("span").find("#labelProgress").remove();
								functionCallback(data);
							}

						},
						complete:function(){
							$.unblockUI();
						},
						error : function(jqXHR, textStatus, errorThrown) {
							$.unblockUI();
							alert("Estimado usuario, no excederse de 14mb o 30 caracteres del nombre del archivo");
							console.log('xhRequest: ' + jqXHR + "\n");
							console.log('ErrorText: ' + textStatus + "\n");
							console.log('thrownError: ' + errorThrown + "\n");
						},
						// Custom XMLHttpRequest
				        xhr: function() {
				            var myXhr = $.ajaxSettings.xhr();
				            if (myXhr.upload) {
				                // For handling the progress of the upload
				                myXhr.upload.addEventListener('progress', function(e) {
				                    if (e.lengthComputable) {
				                    	var progreso=e.loaded/e.total;
				                    	console.log(pasarDecimalPorcentaje(progreso));
				                    	$("#"+inputFileId).parent("span").find("#labelProgress").remove();
				                    	$("#"+inputFileId).parent("span").append(
				                    			"<label id='labelProgress'>"
				                    			+pasarDecimalPorcentaje(progreso)+"</label>");
				                    	
				                    }
				                } , false);
				            }
				            return myXhr;
				        }
						
					});
		}
		
	}

}else{
	console.log("No se encontro "+inputFileId)
	functionCallback();
}

}

function guardarEvidenciaMultipleAuto(archivoId,inputFileId,bitsMax,urlEvidencia,functionCallback,tipoId){
	var archivoIdAux=archivoId;
	var inputFileImage = document.getElementById(inputFileId);
	var file = inputFileImage.files[0];
	var data = new FormData();
	
	if(file){
		if (file.size > bitsMax) {
			alert("Lo sentimos, solo se pueden subir archivos menores a "+bitsMax/1000000+" MB");
			functionCallback();
		} else {
			data.append("fileEvi", file);
			data.append("id", archivoId);
			data.append("tipoId", parseInt(tipoId));
		console.log(data);
			var url = URL + urlEvidencia;
			 
			$.ajax({
						url : url,
						xhrFields: {
				            withCredentials: true
				        },
						type : 'POST',
						contentType : false,
						data : data,
						processData : false,
						cache : false,
						success : function(data, textStatus, jqXHR) {
						 
							switch (data.CODE_RESPONSE) {
							case "06":
								sessionStorage.clear();
								document.location.replace(data.PATH);
								break;
							default:
							 
								functionCallback();
							}

						},
						error : function(jqXHR, textStatus, errorThrown) {
							$.unblockUI();
							alert("Ups, tenemos problemas tecnicos. Intente más tarde. Error: "
									+ errorThrown);
							console.log('xhRequest: ' + jqXHR + "\n");
							console.log('ErrorText: ' + textStatus + "\n");
							console.log('thrownError: ' + errorThrown + "\n");
						}
					});
		}
	
	}else{
		functionCallback();
	}
	
}
function crearFormEvidencia(idAux,esNuevo,containerHtmlId){
	var buttonDescarga="<button class='btn btn-success btnDescargar' style='float:left' id='btnDescargarFile"+idAux+"'>" +
	"<i class='fa fa-download' aria-hidden='true'></i>Descargar</button>";
	if(esNuevo){
		buttonDescarga="";
	}
	var inputEviA=buttonDescarga+"<div class='input-group'>" +
		"<label class='input-group-btn'>"+
         "<span class='btn btn-primary'>"+
          "<i class='fa fa-upload fa-lg'></i>   Subir Nuevo&hellip; <input style='display: none;'"+ 
           "type='file' name='fileEviCese' id='fileEvi"+idAux+"' accept='image/jpg,image/png,image/jpeg,image/gif'>"+
            "</span>"+
            "</label>"+
            "<input type='text' id='inputEvi"+idAux+"' class='form-control' readonly>"+
   		 	"</div>";
	if(containerHtmlId){
		$(containerHtmlId).html(inputEviA);
		$("#fileEvi"+idAux).on("change",function(){
			$("#btnDescargarFile"+idAux).remove();
		});
	}
	
	
	
	return inputEviA;
	
	
}


function hallarPermanencia(dateValue){
	var fechaInicio=convertirFechaNormal2(dateValue);
	var hoyDía=obtenerFechaActual();
	var diasDiff=restaFechas(fechaInicio,hoyDía);
	var textYear="",textMonth="",textDay="";
	
	var numYear=Math.floor(diasDiff/365);
	var restoDias=diasDiff%365;
	if( numYear==0){
		textYear="";
	}else{
		textYear=numYear+(numYear>1?" años ":" año ");
	}
	
	var numMeses=Math.floor(restoDias/30);
	restoDias=restoDias%30;
	
	if( numMeses==0){
		textMonth="";
	}else{
		textMonth=numMeses+(numYear>1?" meses ":" mes ");
	}
	
	textDay=restoDias+(restoDias==1?" día":" días");
	
	return textYear+textMonth+textDay;
	
}
function hallarPermanenciaObject(dateValue){
	var fechaInicio=convertirFechaNormal2(dateValue);
	var hoyDía=obtenerFechaActual();
	var diasDiff=restaFechas(fechaInicio,hoyDía);
	var textYear="",textMonth="",textDay="";
	
	var numYear=Math.floor(diasDiff/365);
	var restoDias=diasDiff%365;
	if( numYear==0){
		textYear="";
	}else{
		textYear=numYear+(numYear>1?" años ":" año ");
	}
	
	var numMeses=Math.floor(restoDias/30);
	restoDias=restoDias%30;
	
	if( numMeses==0){
		textMonth="";
	}else{
		textMonth=numMeses+(numYear>1?" meses ":" mes ");
	}
	
	textDay=restoDias+(restoDias==1?" día":" días");
	
	return {years:parseInt(numYear),months:parseInt(numMeses),days:parseInt(restoDias)};
	
}

function agregarFiltroGosst(filtroIconoId,filtroMenuId,listFiltrar,idElem,nombreElem,
		idToggle,idArrayGuardar,functionCallbackFiltro,tablaFija){
	console.log("Prueba de filtro 1.1")
		
$(document).on("click","#"+filtroIconoId,function(){
		 $(".filtroMenuGosst").hide();
		 var opcionesDiv=$("#"+filtroMenuId);
		 opcionesDiv.remove();
		 if(true){
			
			 $("body").append("<div class='filtroMenuGosst' id='"+filtroMenuId+"'></div>");
			 opcionesDiv=$("#"+filtroMenuId);
				var topDiv=$(this).position().top;
				var leftDiv=$(this).position().left;
				var cssFiltro={"top":99,"left":"50%"};
				if(tablaFija){
					cssFiltro={"top":199,"left":leftDiv+30 ,"z-index": 1900};
				}
				listFiltrar.forEach(function(val,index){
					var textoChecked="checked";
					if(val.filtroActivado){
						textoChecked="checked";
					}else{ 
						textoChecked="";
					}
					opcionesDiv.css(cssFiltro).append("<div class='opcionFiltro'>" +
							"<input type='checkbox' id='checkFiltro"+val[idElem]+"' "+textoChecked+"> " +
							val[nombreElem]+"" +
							"</div>");
				}); 
				opcionesDiv.append("<br><button class='btn btn-danger' ><i class='fa fa-times' aria-hidden='true'> </i>Cancelar </button>");
				opcionesDiv.find(".btn-danger").on("click",function(){
					 $(".filtroMenuGosst").hide();
				});
				opcionesDiv.append(" <button class='btn btn-info'> <i class='fa  fa-pencil-square-o' aria-hidden='true'></i> </button>");
				opcionesDiv.find(".btn-info").on("click",function(){
					idToggle=!idToggle;
					opcionesDiv.find(".opcionFiltro input").each(function(index,elem){
						 
						 if(idToggle){
							 $(elem).prop("checked",false);
						 }else{
							 $(elem).prop("checked",true);
						 }
					 });
				});
				opcionesDiv.append(" <button class='btn btn-success' ><i class='fa fa-check' aria-hidden='true'></i>Listo </button>");
				opcionesDiv.find(".btn-success").on("click",function(){
					 
					var idArrayGuardarAux=[]; 
					opcionesDiv.find(".opcionFiltro input").each(function(index,elem){
						if($(elem).prop("checked")){
							var idAux=parseInt($(elem).prop("id").substr(11,4));
							idArrayGuardarAux.push(idAux);
							}
					}); 
					
					functionCallbackFiltro(idArrayGuardarAux);
					$(".filtroMenuGosst").hide();
					});
		 }
		
	});
}

function crearSelectOneMenuObligMultipleCompleto(idSel, methodJs, listOption, value,
		nameValue,htmlContain,holderText,maxSelects){
	var selectOneBox = "<select data-placeholder='"+holderText+"' multiple id='" + idSel + "' class='form-control' "
	+ "onchange='" + methodJs + "'>";
		if (listOption) {
			for (var index = 0; index < listOption.length; index++) {
				selectOneBox = selectOneBox + "<option value='"
						+ listOption[index][value] + "' ";
				
					if (listOption[index]["selected"] == 1) {
						selectOneBox = selectOneBox + "selected>"
								+ listOption[index][nameValue] + "</option>";
					} else {
						selectOneBox = selectOneBox + ">"
								+ listOption[index][nameValue] + "</option>";
					}
				
				
			}
		} 
selectOneBox = selectOneBox + "</select>" ;
$(""+htmlContain).html(""+selectOneBox);
maxSelects=defaultFor(maxSelects,100000);
$("#"+idSel).chosen({
	max_selected_options: maxSelects,
	no_results_text: "No  se encontró"});
$("#"+idSel).bind("chosen:maxselected", function () { alert("Límite alcanzado") }); 
 
}

function crearFormEvidenciaCompleta(options){
	var textImagen ="";
	if(options.isImagen){
		textImagen = "accept = 'image/*' ";
	}
	var idAux=options.idAux;
	var buttonDescarga="<label class='label-download input-group-btn ' id='btnDescargarFile"+idAux+"' >" +
        		"<span class='btn btn-primary'><i class='fa fa-download fa-lg' '></i></span>" +
        		"</label>";
	if(options.esNuevo){
		buttonDescarga="";
	} 
	var inputEviA= "<div class='input-group'>" +
		"<label class='label-upload  input-group-btn'>"+
     "<span class='btn btn-primary'>"+
      "<i class='fa fa-upload fa-lg'></i> Subir  <input "+textImagen+" style='display: none;'"+ 
       "type='file'   id='fileEvi"+idAux+"'  >"+
        "</span>"+
        "</label>"+
        "<input type='text' id='inputEvi"+idAux+"' class='form-control' readonly>" +
        		""+buttonDescarga+
		 	"</div>";
	
		$(options.container).html(inputEviA);
		 
		$("#btnDescargarFile"+idAux).attr("onclick",
				"window.open('"+ URL+options.descargaUrl+"','_blank')"
		);
		 $("#fileEvi"+idAux).on('fileselect' , function(event, numFiles, label) {
			
	          var input = $(this).parents('.input-group').find(':text'),
	              log = numFiles > 1 ? numFiles + ' files selected' : label;

	          if( input.length ) {
	              input.val(log);
	          } else {
	              if( log ) alert(log);
	          }

	      });
		$("#inputEvi"+idAux).val(options.evidenciaNombre);
		$("#fileEvi"+idAux).on("change",function(){
			 var input = $(this),
		        numFiles = input.get(0).files ? input.get(0).files.length : 1,
		        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		    
			 if(options.isImagen){
				 if(!comprabarImagen(label)){
					 alert("Sólo imágenes")
					 return false;
				 }
			 }
			 $("#btnDescargarFile"+idAux).remove();
			input.trigger('fileselect', [numFiles, label]);
			options.functionCall(idAux);
		});
	
	
	
	return inputEviA;
}

function obtenerSubPanelModuloGeneral(val){
	var estiloDefault="";
	if(val.inputForm=="" &&  val.sugerencia=="" && val.label==""){
		estiloDefault="    border-top: 1px solid #a5aaae;"
	}
	return "<div class='form-group row "+val.clase+"' style='"+estiloDefault+"'>"+
	   " <label for='colFormLabel' class='col-sm-4 col-form-label'>"+val.label+"</label>"+
	    "<div class='col-sm-8' id='"+val.divContainer+"'>"+
	     " "+val.inputForm+
	     "<small>"+val.sugerencia+" </small>"+
	    "</div>"+
	  "</div>";
}

function agregarModalPrincipalColaboradorGeneral(modalOptions,functionCallBack,isStatic){
	$(".modal-backdrop").remove();
	$("#"+modalOptions.id).remove();
	$("body").append("<div class='modal' id='"+modalOptions.id+"' role='dialog'>"+
"<div class='modal-dialog modal-dialog-centered' style='width:100%;    max-width: 650px;'>"+
 
"<div class='modal-content' >"+
 "<div class='modal-header'>"+
 "   <button type='button' class='close' data-dismiss='modal'>&times;</button>"+
  "  <h4 class='modal-title'>"+modalOptions.nombre+"</h4>"+
  "</div>"+
   
  "<div class='modal-body'>"+
  modalOptions.contenido+
  "</div>"+
"</div>"+

"</div>"+
"</div>");
	$("#"+modalOptions.id).on('shown.bs.modal', functionCallBack);
	if(isStatic){
		$('#'+modalOptions.id).modal({
			  keyboard: true,
			  show : true,
			  backdrop : false
			})
	}else{
		$("#"+modalOptions.id).modal("show");
	}
}

