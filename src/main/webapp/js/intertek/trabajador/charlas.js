/**
 * 
 */


var charlaId;
var banderaEdicionCharla;
var listFullCharlas;
var tipoCharla={};

function verCharlasTrabajador(tipoId){
	$("#modalContratos").modal("show");
	$("#modalContratos").find(".modal-body").hide();
	$("#modalContratos").find(".modal-title").html("Inducciones");
	$("#modalContratos").find("#modalBodyCharla").show();
	tipoCharla={id:tipoId};
	llamarCharlas();
	
}
function llamarCharlas(){
	charlaId=0;
	banderaEdicionCharla=false;
	listFullCharlas=[];
	
	$("#btnAgregarCharla").show().attr("onclick","nuevoCharla()");
	$("#btnCancelarCharla").hide().attr("onclick","llamarCharlas()");
	$("#btnEliminarCharla").hide().attr("onclick","eliminarCharla()");
	$("#btnGuardarCharla").hide().attr("onclick","guardarCharla()");
	var dataParam={trabajadorId:trabajadorId ,charla:{tipo:tipoCharla}}
	callAjaxPost(URL + '/trabajador/charlas', dataParam, function(data) {
		
		var list = data.list;
		listFullCharlas=data.list;
		var listMotivoCharla=data.listMotivoCharla;
		listMotivoCharla.forEach(function(val,index){
			if(val.id==tipoCharla.id){
				$("#modalContratos").find(".modal-title").html("Inducciones: " +val.nombre);
			}
		});
		$("#tblCharlas tbody").html("");
		listFullCharlas.forEach(function(val,index){
			$("#tblCharlas tbody").append("<tr onclick='javascript:editarCharla("+
					index+")' >"+
					"<td id='chafecha"+
					val.id + "'>"+
					val.fechaTexto+ "</td>" +
					"<td id='charesp"+
					val.id + "'>"+
					val.responsable + "</td>" +
					
					
					"<td id='chaEvi"+
					val.id + "'>"+
					(val.evidenciaNombre==null?"---":val.evidenciaNombre) + "</td>" +
					"</tr>")
		});
		
		if(parseInt(tipoCharla.id)==1){
			$('#tblCharlas td:nth-child(2),#tblCharlas th:nth-child(2)').hide();
		}else{
			$('#tblCharlas td:nth-child(2),#tblCharlas th:nth-child(2)').show();
		}
		formatoCeldaSombreableTabla(true,"tblCharlas");
	
});
	
}
function editarCharla(pindex){
	if(!banderaEdicionCharla){
		banderaEdicionCharla=true;
		$("#btnAgregarCharla").hide();
		$("#btnCancelarCharla").show();
		$("#btnEliminarCharla").show();
		$("#btnGuardarCharla").show();
		formatoCeldaSombreableTabla(false,"tblCharlas");
		charlaId=listFullCharlas[pindex].id;
		var fInicio=convertirFechaNormal2(listFullCharlas[pindex].fecha);
		var responsable=listFullCharlas[pindex].responsable;
		var inputEvi=crearFormEvidencia("Charla",false,"#chaEvi"+charlaId);
		var eviNombre=listFullCharlas[pindex].evidenciaNombre;
		
		$("#btnDescargarFileCharla").attr("onclick","location.href='"+ URL
				+ "/trabajador/charla/evidencia?charlaId="
				+ charlaId+"'");
	 
	$("#inputEviCharla").val(eviNombre);
	
	$("#charesp"+charlaId).html("<input type='text' class='form-control' id='textRespCharla'>");
	 $("#chafecha"+charlaId).html("<input type='date' class='form-control' id='dateCharla' >");
	
	 $("#dateCharla").val(fInicio);
	$("#textRespCharla").val(responsable);
	var listaTrabs=listarStringsDesdeArray(listFullTrabajadores,"trabajadorNombreCompleto");
	ponerListaSugerida("textRespCharla",listaTrabs,true);
	}
}
function nuevoCharla(){
	$("#btnAgregarCharla").hide();
	$("#btnCancelarCharla").show();
	$("#btnEliminarCharla").hide();
	$("#btnGuardarCharla").show();
	var inputEvi=crearFormEvidencia("Charla",true);
	banderaEdicionCharla=true;
	charlaId = 0;
	if(tipoCharla.id==1){
		$("#tblCharlas tbody")
		.append(
				"<tr id='tr0'>"
				+"<td>"+"<input type='date' class='form-control' id='dateCharla' >"+"</td>"
				
				+"<td>"+inputEvi+" </td>"
						+ "</tr>");
	}else{
		$("#tblCharlas tbody")
		.append(
				"<tr id='tr0'>"
				+"<td>"+"<input type='date' class='form-control' id='dateCharla' >"+"</td>"
				+"<td>"+"<input type='text' class='form-control' id='textRespCharla' >"+"</td>"
				
				
				+"<td>"+inputEvi+" </td>"
						+ "</tr>");
var listaTrabs=listarStringsDesdeArray(listFullTrabajadores,"trabajadorNombreCompleto");
ponerListaSugerida("textRespCharla",listaTrabs,true);
	}
	
	formatoCeldaSombreableTabla(false,"tblCharlas");
}

function eliminarCharla(){
	var objetivoObj={
			id:charlaId
	}
	var r=confirm("¿Está seguro de eliminar esta inducción?");
	if(r){
		callAjaxPost(URL + '/trabajador/charla/delete', objetivoObj,
				function(data) {
					
					
			llamarCharlas();
					
				});
	}
	
}

function guardarCharla(){
	var fechaInicio=$("#dateCharla").val();
	var responsable=$("#textRespCharla").val();
	var campoVacio=false;
	 
	
	if(campoVacio){
		alert("Complete el campo")
	}else{
		var objetivoObj={
				id:charlaId,
				fecha:convertirFechaTexto(fechaInicio),
				responsable:responsable,
				tipo:tipoCharla,
				trabajadorId:trabajadorId
		}
		console.log(objetivoObj);
		callAjaxPost(URL + '/trabajador/charla/save', objetivoObj,
				function(data) {
				var nuevoId=	data.nuevoId
			guardarEvidenciaAuto(nuevoId,"fileEviCharla"
					,bitsCharla,"/trabajador/charla/evidencia/save",llamarCharlas);
			
					
				});
	}
	
}


	

	
	
	
	

