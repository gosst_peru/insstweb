/**
 * 
 */


var contactoId;
var banderaEdicionContacto;
var listFullContactos;
var estadoContacto={};


function verInfoExtraTrabajador(){
	$("#modalContactos").modal("show");
	$("#modalContactos").find(".modal-body").hide();
	$("#modalContactos").find(".modal-title").html("Información de contacto");
	$("#modalContactos").find("#modalBodyInfo").show();
}

function verContactosTrabajador(){
	$("#modalContactos").find(".modal-body").hide();
	$("#modalContactos").find(".modal-title").html("Contactos de emergencia");
	$("#modalContactos").find("#modalBodyContacto").show();
	
	llamarContactos();
}
function llamarContactos(){
	contactoId=0;
	banderaEdicionContacto=false;
	listFullContactos=[];
	estadoContacto={};
	$("#btnAgregarContacto").show().attr("onclick","nuevoContacto()");
	$("#btnCancelarContacto").hide().attr("onclick","llamarContactos()");
	$("#btnEliminarContacto").hide().attr("onclick","eliminarContacto()");
	$("#btnGuardarContacto").hide().attr("onclick","guardarContacto()");
	var dataParam={trabajadorId:trabajadorId}
	callAjaxPost(URL + '/trabajador/contactos', dataParam, function(data) {
		
		var list = data.list;
		listFullContactos=data.list;
		$("#tblContactos tbody").html("");
		listFullContactos.forEach(function(val,index){
			$("#tblContactos tbody").append("<tr onclick='javascript:editarContacto("+
					index+")' >"+
					"<td id='contNombre"+
					val.id + "'>"+
					val.nombre + "</td>" +
					"<td id='contPar"+
					val.id + "'>"+
					val.parentesco + "</td>" +
					"<td id='contTelCasa"+
					val.id + "'>"+
					val.telefonoCasa+ "</td>" +
					"<td id='contTelTrab"+
					val.id + "'>"+
					val.telefonoTrabajo + "</td>" +
					"<td id='contTelCel"+
					val.id + "'>"+
					val.telefonoCelular + "</td>" +
					"</tr>")
		});
	
		formatoCeldaSombreableTabla(true,"tblContactos");
	
});
	
}
function editarContacto(pindex){
	if(!banderaEdicionContacto){
		banderaEdicionContacto=true;
		$("#btnAgregarContacto").hide();
		$("#btnCancelarContacto").show();
		$("#btnEliminarContacto").show();
		$("#btnGuardarContacto").show();
		formatoCeldaSombreableTabla(false,"tblContactos");
		contactoId=listFullContactos[pindex].id;
		var nombre=listFullContactos[pindex].nombre;
		var parentesco=listFullContactos[pindex].parentesco;
		var telCasa=listFullContactos[pindex].telefonoCasa;
		var telTrab=listFullContactos[pindex].telefonoTrabajo;
		var telCelular=listFullContactos[pindex].telefonoCelular;
		
		$("#contNombre"+contactoId).html("<input type='text' class='form-control' id='textNombreContact' >");
		$("#contTelCasa"+contactoId).html("<input type='text' class='form-control' id='textTelCasa' >");
		$("#contTelTrab"+contactoId).html("<input type='text' class='form-control' id='textTelTrab' >");
		$("#contTelCel"+contactoId).html("<input type='text' class='form-control' id='textTelCelular' >");
		$("#contPar"+contactoId).html("<input type='text' class='form-control' id='textParentesco' >");
		
	$("#textNombreContact").val(nombre);
	$("#textTelCasa").val(telCasa);
	$("#textTelTrab").val(telTrab);
	$("#textTelCelular").val(telCelular);
	$("#textParentesco").val(parentesco);
}
}
function nuevoContacto(){
	$("#btnAgregarContacto").hide();
	$("#btnCancelarContacto").show();
	$("#btnEliminarContacto").hide();
	$("#btnGuardarContacto").show();
	
	banderaEdicionContacto=true;
	contactoId = 0;
	$("#tblContactos tbody")
			.append(
					"<tr id='tr0'>"
					+"<td>"+"<input type='text' class='form-control' id='textNombreContact' >"+"</td>"
					+"<td>"+"<input type='text' class='form-control' id='textParentesco' >"+"</td>"
					+"<td>"+"<input type='text' class='form-control' id='textTelCasa' >"+"</td>"
					+"<td>"+"<input type='text' class='form-control' id='textTelTrab' >"+"</td>"
					+"<td>"+"<input type='text' class='form-control' id='textTelCelular' >"+"</td>"
					+ "</tr>");
	formatoCeldaSombreableTabla(false,"tblContactos");
}

function eliminarContacto(){
	var objetivoObj={
			id:contactoId
	}
	var r=confirm("¿Está seguro de eliminar este contacto?");
	if(r){
		callAjaxPost(URL + '/trabajador/contacto/delete', objetivoObj,
				function(data) {
					
					
			llamarContactos();
					
				});
	}
	
}

function guardarContacto(){
	var nombre=$("#textNombreContact").val();
	var telefeonoCasa=$("#textTelCasa").val();
	var telefonoTrabajo=$("#textTelTrab").val();
	var telefonoCelular=$("#textTelCelular").val();
var parentesco=$("#textParentesco").val();
	var campoVacio=false;
	 
	
	if(campoVacio){
		alert("Complete el campo")
	}else{
		var objetivoObj={
				id:contactoId,
				nombre:nombre,
				telefonoCasa:telefeonoCasa,
				telefonoTrabajo:telefonoTrabajo,
				telefonoCelular:telefonoCelular,
				parentesco:parentesco,
				trabajadorId:trabajadorId
		}
		console.log(objetivoObj);
		callAjaxPost(URL + '/trabajador/contacto/save', objetivoObj,
				function(data) {
			llamarContactos();
					
				});
	}
	
}


	

	
	
	
	

