/**
 * 
 */

var listTrabHijos;
var listTrabTiempo;
var listTrabVacaciones;
var listTrabajadoresInOut;
var arrayMeses=["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Setiembre","Octubre","Noviembre","Diciembre"];
var arrayYear=[];
$(function() {
    $("form").submit(function() { return false; });
   
});
var lisstaEstadisticas=[
                        {id:1,nombre:"Cuadro Contabilidad",tableId:"tblContabilidad",divBody:"bodyComtabilidad",functionHtml:function(){llamarDatosContabilidad()}},
                        {id:2,nombre:"Reporte de Hijos",tableId:"tblReportHijos",divBody:"bodyReportHijo",functionHtml:function(){llamarDatosReporteHijos()}},
                        {id:3,nombre:"Reporte de Tiempo Laboral",tableId:"tblTiempoTrab",divBody:"bodyTiempoLaboral",functionHtml:function(){llamarDatosReporteTiempoLaboral()}},
                        {id:4,nombre:"Reporte de Vacaciones",tableId:"tblVacTrab",divBody:"bodyReportVacaciones",functionHtml:function(){llamarDatosReporteVacaciones()}},
                        {id:5,nombre:"Ingresos y Ceses",tableId:"tblReportIngreso",divBody:"bodyInOut",functionHtml:function(){llamarDatosIngresosCeses()}}];
function iniciarEstadisticasTrabajador(){
	var hoyDia=new Date();
	 var yearActual=hoyDia.getUTCFullYear();
		for(var i=yearActual-6;i<yearActual+6;i++){
			arrayYear.push(i);
		}
		var objMeses=[],objYear=[];
		arrayMeses.forEach(function(val,index){
			objMeses.push({
				id:index+1,nombre:val
			});
		});
		arrayYear.forEach(function(val,index){
			objYear.push({
				id:val,nombre:val
			});
		});
		var slcMesesDesde= crearSelectOneMenuOblig("slcMesesDesde", "", objMeses, "1", "id","nombre");
		var slcYearDesde= crearSelectOneMenuOblig("slcYearDesde", "", objYear, yearActual, "id","nombre");
		var slcMesesHasta= crearSelectOneMenuOblig("slcMesesHasta", "", objMeses, "12", "id","nombre");
		var slcYearHasta= crearSelectOneMenuOblig("slcYearHasta", "", objYear, yearActual, "id","nombre");
	$("#bodyComtabilidad .form-inline").html("Desde :"+slcMesesDesde+slcYearDesde+
												"Hasta :"+slcMesesHasta+slcYearHasta);
	
	var selEstad= crearSelectOneMenuOblig("selEstad", "", lisstaEstadisticas, 
			"", "id","nombre");
	$("#modalEstadTrab").modal("show");
	$("#modalEstadTrab").find(".modal-body").hide();
	$("#modalEstadTrab").find("#bodyParametro").show();
	$("#modalEstadTrab").find("#bodyParametro").html(selEstad
			+"<br> <button class='btn btn-success' onclick='llamarEstadisticaTrabajador()'>Ver estadísticas</button>");
}

function guardarTablaEstad(vasss){

		var listFullTablaProgramacion="";
		listFullTablaProgramacion=obtenerDatosBodyTablaEstandarNoFija(vasss);
		 copiarAlPortapapeles(listFullTablaProgramacion,"btnClipTablaEstadTrab");
		
		alert("Se han guardado al clipboard la tabla de este módulo" );

	
}

function llamarEstadisticaTrabajador(){
	$("#modalEstadTrab").modal("show");
	$("#modalEstadTrab").find(".modal-body").hide();
	$("#modalEstadTrab").find("#bodyParametro").show();
	$("#btnClipTablaEstadTrab").remove();
	var estadId=$("#selEstad").val();
	lisstaEstadisticas.forEach(function(val,index){
		if(val.id==parseInt(estadId)){
			$("#modalEstadTrab").find(".modal-title").html(val.nombre);
			$("#modalEstadTrab").find("#"+val.divBody).show().prepend(
					"<button id='btnClipTablaEstadTrab' type='button' class='btn btn-success clipGosst' title='Tabla Clipboard' >"
					+"<i class='fa fa-clipboard fa-2x'></i></button>");
			$("#btnClipTablaEstadTrab").on("click",function(){ guardarTablaEstad(val.tableId); })
			val.functionHtml();
			
			
		}
	});	
}

function llamarDatosContabilidad(){
	
	var dataParam = {
			mdfId : getUrlParameter("mdfId")
		};

		callAjaxPost(URL + '/trabajador/estadistica/contabilidad', dataParam,
				function(data){
			
			var infoTrabajadores=data.list;
			var listTipoContrato=data.listTipoContrato;
			listTipoContrato.push({id:0,nombre:"Total"});
			var listMotivoCese=data.listMotivoCese;
			listMotivoCese.push({id:0,nombre:"Total"});
			var tblConta=$("#tblContabilidad");
			tblConta.find("#trSexo").attr("colspan",listGenero.length);
			tblConta.find("#trDuracion").attr("colspan",listDuracion.length);
			tblConta.find("#trContrato").attr("colspan",listTipoContrato.length-1);
			tblConta.find("#trCese").attr("colspan",listMotivoCese.length);
			$("#subTrContabilidad").html("");
			listDuracion.forEach(function(val,index){
				$("#subTrContabilidad").append("<th class='tb-acc'>"+val.nombre+" </th> ")
			});
			listGenero.forEach(function(val,index){
				$("#subTrContabilidad").append("<th class='tb-acc'>"+val.generoNombre+" </th> ")
			});
			listTipoContrato.forEach(function(val,index){
				var auxVal={};
				if(val.id==3){
					$("#trContratoSeparado").remove();
					$("#trContrato").after(
							"<th class='tb-acc' style='width:120px' rowspan='2' id='trContratoSeparado'>"+val.nombre+"</th>");
					
				}else{
					$("#subTrContabilidad").append("<th class='tb-acc'>"+val.nombre+" </th> ");
					
				}
				
			});
			var elem3=listTipoContrato[2];
			var elemTot=listTipoContrato[3];
			listTipoContrato[2]=elemTot;
			listTipoContrato[3]=elem3;
			listMotivoCese.forEach(function(val,index){
				$("#subTrContabilidad").append("<th class='tb-acc'>"+val.nombre+" </th> ")
			});
			
			var listaFecha=hallarFechasEstadisticaTrab();
			tblConta.find("tbody").html("");
			var listInput=listaFecha[0];
			var listObject=listaFecha[1];
			listObject.forEach(function(val,index){
				var fechaInput=listInput[index];
				
				
				var trabsValidos=[];
				var trabsContratados=[];
				var trabsCesados=[];
				infoTrabajadores.forEach(function(val1,index1){
					
					var ceses=val1.ceses;
					var contratos=val1.contratos;
					var contratoValido=false;
					var contratoEnRangoValido=false;
					var ceseEnRangoValido=false;
					contratos.forEach(function(val2,index2){
						
						var fechaInicio=convertirFechaInput(val2.fechaInicio);
						fechaInicio=fechaInicio.split("-");
						var fechaInputAux=fechaInput.split("-");
						if(fechaInicio[1]==fechaInputAux[1] && fechaInicio[0]==fechaInputAux[0]){
							contratoEnRangoValido=true;
							val1.contratoActivo={id:val2.tipo.id};
							val1.ceseActivo={id:null};
						}
						if(val2.tipo.id!=3){
						if(fechaInicio[0]<fechaInputAux[0] ){
							contratoValido=true;
							val1.contratoActivo={id:val2.tipo.id};
							val1.ceseActivo={id:null};
						}else if(fechaInicio[0]==fechaInputAux[0] && fechaInicio[1]<=fechaInputAux[1]){
							contratoValido=true;
							val1.contratoActivo={id:val2.tipo.id};
							val1.ceseActivo={id:null};
						}
						}
					});
					ceses.forEach(function(val2,index2){
						var fechaInicio=convertirFechaInput(val2.fecha);
						fechaInicio=fechaInicio.split("-");
						var fechaInputAux=fechaInput.split("-");
						if(fechaInicio[1]==fechaInputAux[1] && fechaInicio[0]==fechaInputAux[0]){
							ceseEnRangoValido=true;
							val1.ceseActivo={id:val2.motivo.id};
							val1.contratoActivo={id:null};
						}
						if(fechaInicio[0]<fechaInputAux[0]
						|| (fechaInicio[0]==fechaInputAux[0] && fechaInicio[1]<=fechaInputAux[1])){
							contratoValido=false;
						}
						
						
					});
					if(contratoValido){
						trabsValidos.push(val1);
					}
					
					if(contratos.length>0 && contratoEnRangoValido){
						trabsContratados.push(val1);
					}
					if(ceses.length>0 && ceseEnRangoValido){
						trabsCesados.push(val1);
					}
					
				});
				var textDuracion="";
				var textSexo="";
				var textContrato="";
				var textCese="";
				listDuracion.forEach(function(val3,index3){
					var numValidos=0;
					trabsValidos.forEach(function(val4,index4){
						console.log(val4);
						if(val3.id==val4.tipoDuracion.id){
							
							numValidos++;
						}
					});
					textDuracion+="<td>"+numValidos+"</td>"
				});
				listGenero.forEach(function(val3,index3){
					var numValidos=0;
					trabsValidos.forEach(function(val4,index4){
						if(val3.generoId==val4.genero.generoId){
							numValidos++;
						}
					});
					textSexo+="<td>"+numValidos+"</td>"
				});
				listTipoContrato.forEach(function(val3,index3){
					var numValidos=0;
					trabsContratados.forEach(function(val4,index4){
						if(val3.id==val4.contratoActivo.id || (val3.id==0 && val4.contratoActivo.id!=3)){
							numValidos++;
						}
					});
					textContrato+="<td>"+numValidos+"</td>"
				});
				listMotivoCese.forEach(function(val3,index3){
					var numValidos=0;
					trabsCesados.forEach(function(val4,index4){
						if(val3.id==val4.ceseActivo.id || val3.id==0){
							numValidos++;
						}
					});
					textCese+="<td>"+numValidos+"</td>"
				});
				tblConta.find("tbody").append(
						"<tr>"
						+"<td>"+val.year+"</td>"
						+"<td>"+val.month+"</td>"
						+"<td>"+trabsValidos.length+"</td>"
						+textDuracion
						+textSexo
						+textContrato
						+textCese
						+"</tr>");
				
			});
			
		});
}

function hallarFechasEstadisticaTrab(){
	var mesDesde=$("#slcMesesDesde").val();
	var mesHasta=$("#slcMesesHasta").val();
	var yearDesde=$("#slcYearDesde").val();
	var yearHasta=$("#slcYearHasta").val();
var fechaInicio=yearDesde+"-"+mesDesde+"-"+"01";
var fechaFin=yearHasta+"-"+mesHasta+"-"+"01";
	var fecha1=fechaInicio.split('-');
	var fecha2=fechaFin.split('-');
	var difanio= fecha2[0]-fecha1[0]+1;
	var difDias=restaFechas(fechaInicio,fechaFin);
	var listfecha=[];
	var listObjectFecha=[];
		for (index =0; index <difanio; index++) {
			var anioaux=parseInt(index)+parseInt(fecha1[0]);
			
			if(difanio>1){
				if(index==0){
					for(index1 =0; index1 < 12-fecha1[1]+1; index1++) {
						
						var mesaux=parseInt(index1)+parseInt(fecha1[1]);
						listfecha.push(anioaux+"-"+((''+mesaux).length<2 ? '0' : '') + mesaux );
						listObjectFecha.push({
							year:anioaux,month:arrayMeses[mesaux-1] 
						});
						}
				}
				if(index==difanio-1){
					for(index4=0;index4<parseInt(fecha2[1]);index4++){
						var mesaux3=parseInt(index4)+parseInt("1");
						listfecha.push(anioaux+"-"+((''+mesaux3).length<2 ? '0' : '') + mesaux3 );
						listObjectFecha.push({
							year:anioaux,month:arrayMeses[mesaux3-1]
						});
					}
							}	
				
				if(index>0 && index <difanio-1 ){
						
						for(index3 =0;index3 <12;index3++){
							var mesaux2=parseInt(index3)+1;	
							listfecha.push(anioaux+"-"+((''+mesaux2).length<2 ? '0' : '') + mesaux2 );
							listObjectFecha.push({
								year:anioaux,month:arrayMeses[mesaux2-1]
							});
						}
						
						
					}
					
			}else{
				for(index5 =0; index5 < fecha2[1]-fecha1[1]+1; index5++) {
					
					var mesaux4=parseInt(index5)+parseInt(fecha1[1]);
					listfecha.push(anioaux+"-"+((''+mesaux4).length<2 ? '0' : '') + mesaux4 );
					listObjectFecha.push({
						year:anioaux,month:arrayMeses[mesaux4-1]
					});
					}
				
			}
			
			

			
		};
		
	
	var listFullFecha=[listfecha,listObjectFecha];
	return listFullFecha;
	
}

var arrayFiltroLocacionHijo=[];
var arrayFiltroGeneroHijo=[];
function llamarDatosReporteHijos(){
	var dataParam = {
			mdfId : getUrlParameter("mdfId")
		};

		callAjaxPost(URL + '/trabajador/estadistica/hijos', dataParam,function(data){
			listTrabHijos=data.list;
			arrayFiltroLocacionHijo=listarStringsDesdeArray(listLocacion, "id");
			arrayFiltroGeneroHijo=listarStringsDesdeArray(listGenero, "generoId");
			agregarFiltroGosst("filtroLocalHijo","menuFiltroLocacionHijo",listLocacion,"id","nombre",
					toggleLocacion,arrayFiltroLocacionHijo,function(lista){
				arrayFiltroLocacionHijo=lista;
				listLocacion.forEach(function(val,index){
					var estaLista=arrayFiltroLocacionHijo.indexOf(parseInt(val.id))!=-1;
					val.filtroActivado=estaLista;
				});
				actualizarTablasHijos();
			},true);
			agregarFiltroGosst("filtroGeneroHijo","menuFiltroGeneroHijo",listGenero,"generoId","generoNombre",
					toggleLocacion,arrayFiltroGeneroHijo,function(lista){
				arrayFiltroGeneroHijo=lista;
				listGenero.forEach(function(val,index){
					var estaLista=arrayFiltroGeneroHijo.indexOf(parseInt(val.generoId))!=-1;
					val.filtroActivado=estaLista;
				});
				console.log(listGenero);
				actualizarTablasHijos();
			},true);
			actualizarTablasHijos();
			});
		
}
var listaRangoEdad=[
                    {inicio:0,fin:2},
                    {inicio:3,fin:5},
                    {inicio:6,fin:8},
                    {inicio:9,fin:10},
                    {inicio:11,fin:17},
                    {inicio:18,fin:90}];
function actualizarTablasHijos(){
	var hijosTbl=$("#tblReportHijos");
	hijosTbl.find("tbody").html("");
	var hijosEstad=[];
	var edadInicio=$("#edadInicioEstad").val();
	var edadFin=$("#edadFinEstad").val();
	listTrabHijos.forEach(function(val,index){
		var hijos=val.hijos;
		hijos.forEach(function(val1,index1){
			var hijoValido=comprobarEdadHijo(val1,edadInicio,edadFin);
			var hijoFiltroLocal=arrayFiltroLocacionHijo.indexOf(parseInt(val.locacion.id))!=-1;
			var hijoFiltroGenero=arrayFiltroGeneroHijo.indexOf(parseInt(val1.genero.generoId))!=-1;
			console.log(arrayFiltroGeneroHijo);
			if(hijoValido.result && hijoFiltroLocal && hijoFiltroGenero){
				val1.edad=hijoValido.edad;
				hijosEstad.push(val1);
				hijosTbl.find("tbody").append(
						"<tr id='trHijoEstad"+val1.id+"'> " +
						"<td>"+val1.nombre+"</td>" +
						"<td>"+val.trabajadorNombre+"</td>" +
						"<td>"+val.locacion.nombre+"</td>" +
						
						"<td>"+val1.genero.generoNombre+"</td>" +
						"<td>"+val1.edad+"</td>" +
					"</tr>");
			}
		});
	});
	var numBoys=0,numGirls=0;
	var tblResumen=$("#tblResumenHijos");
	tblResumen.find("thead").html("");
	tblResumen.find("tbody").html("<tr></tr>");
var edadFila="<tr>";
	var sexoFila="<tr>";
	listaRangoEdad.forEach(function(val,index){
		val.numBoys=0;
		val.numGirls=0;
		
		var textRango=val.inicio+" - "+val.fin;
		
		edadFila+="<td colspan='2' class='tb-acc'>"+textRango+"</td>";
		sexoFila+="<td class='tb-acc'>H</td><td class='tb-acc'>M</td>";
		hijosEstad.forEach(function(val1,index1){
			var hijoValido=comprobarEdadHijo(val1,val.inicio,val.fin);
			if(hijoValido.result){
				if(val1.genero.generoId==1){
					val.numBoys++;
				}else{
					val.numGirls++;
				}
			}
			
		});
	});
	edadFila+="</tr>";
	sexoFila+="</tr>";
	tblResumen.find("thead").append(edadFila+sexoFila);
	listaRangoEdad.forEach(function(val,index){
		tblResumen.find("tbody tr").append("<td>"+val.numBoys+"</td>" +
				"<td>"+val.numGirls+"</td>");
	});
	hijosEstad.forEach(function(val,index){
		if(val.genero.generoId==1){
			numBoys++;
		}else{
			numGirls++;
		}
	});
	$("#reporteTextHijo").html("Total Niños: "+ numBoys+", Total Niñas: "+numGirls);	
	
	
}
function comprobarEdadHijo(hijoObject,edadInicio,edadFin){
	var fechaEje=$("#fechaEdadEstad").val();
	
	
	var edadRelativa=calcularEdadRelativa(convertirFechaInput(hijoObject.fechaNacimiento),fechaEje);
	
	if(edadInicio<=edadRelativa  && edadFin>=edadRelativa){
		return {result:true,edad:edadRelativa};
	}else{
		return {result:false,edad:edadRelativa};
	}
	
	
	
	
}



function llamarDatosReporteTiempoLaboral(){
	var dataParam = {
			mdfId : getUrlParameter("mdfId")
		};

		callAjaxPost(URL + '/trabajador/estadistica/tiempo', dataParam,function(data){
			listTrabTiempo=data.list;
			actualizarTablasTiempo();
			
			
			
		});
		
}

function actualizarTablasTiempo(){
	var paramYears=$("#yearTiempoMaxEstad").val();
	var paramMonths=$("#monthTiempoMaxEstad").val();
	var paramDays=$("#dayTiempoMaxEstad").val();
	
	var paramYearsMin=$("#yearTiempoMinEstad").val();
	var paramMonthsMin=$("#monthTiempoMinEstad").val();
	var paramDaysMin=$("#dayTiempoMinEstad").val();
	var tablaEstad=$("#tblTiempoTrab");
	tablaEstad.find("tbody").html("");
	listTrabTiempo.forEach(function(val,index){
		var tiempoObjet=hallarPermanenciaObject(val.fechaIngreso);
		var trabValidoMin=true;
		var trabValidoMax=true;
		if(tiempoObjet.years<paramYears){
			trabValidoMax=true;
		
		}else if(tiempoObjet.years==paramYears){
			if(tiempoObjet.months<paramMonths){
				trabValidoMax=true;
			}else if(tiempoObjet.months==paramMonths){
				if(tiempoObjet.days<paramDays){
					trabValidoMax=true;
				}else{
					trabValidoMax=false;
				}
			}else{
				trabValidoMax=false;
			}
		}else{
			trabValidoMax=false;
		}
		if(tiempoObjet.years>paramYearsMin){
			trabValidoMin=true;
		
		}else if(tiempoObjet.years==paramYearsMin){
			if(tiempoObjet.months>paramMonthsMin){
				trabValidoMin=true;
			}else if(tiempoObjet.months==paramMonthsMin){
				if(tiempoObjet.days>paramDaysMin){
					trabValidoMin=true;
				}else{
					trabValidoMin=false;
				}
			}else{
				trabValidoMin=false;
			}
		}else{
			trabValidoMin=false;
		}
		if(trabValidoMin && trabValidoMax){
			
		tablaEstad.find("tbody").append(
				"<tr>" +
				"<td>"+val.puesto.areaName+"</td>" +				
				"<td>"+val.puesto.positionName+"</td>" +				
				"<td>"+val.locacion.nombre+"</td>" +
				"<td>"+val.trabajadorNombre+"</td>" +
				"<td>"+tiempoObjet.years+"</td>" +
				"<td>"+tiempoObjet.months+"</td>" +
				"<td>"+tiempoObjet.days+"</td>" +
			"</tr>");
		}
	});
	
	
}

function llamarDatosReporteVacaciones(){
	var dataParam = {
			mdfId : getUrlParameter("mdfId")
		};

		callAjaxPost(URL + '/trabajador/estadistica/vacaciones', dataParam,function(data){
			listTrabVacaciones=data.list;
			actualizarTablasVacaciones();
			
		});
}

function actualizarTablasVacaciones(){
	var tablaEstad=$("#tblVacTrab");
	var daysMin=$("#daysVacacionesMin").val();
	var tipoVacas=parseInt($("#slcEstadVacacion").val());
	$('#tblVacTrab td:nth-child(6),#tblVacTrab th:nth-child(6)').show();
	$('#tblVacTrab td:nth-child(7),#tblVacTrab th:nth-child(7)').show();
	$('#tblVacTrab td:nth-child(8),#tblVacTrab th:nth-child(8)').show();
	tablaEstad.find("tbody").html("");
	$("#thAsigVacacion").html("Vacaciones Asignadas");
	if(tipoVacas!=5){
		
		listTrabVacaciones.forEach(function(val,index){
			var trabValido=true;
			var vacaciones=val.vacaciones;
			var numDiasPendientes=0;
			var numDiasCompletados=0;
			vacaciones.forEach(function(val1,index1){
				if(tipoVacas==val1.estado.id || tipoVacas==4){
					numDiasPendientes=numDiasPendientes+val1.diasPendientes;
					numDiasCompletados=numDiasCompletados+val1.diasCompletados;
				}
				
			});
			var diasTotal=numDiasCompletados+numDiasPendientes;
			if(diasTotal>=daysMin){
				trabValido=true;
			}else{
				trabValido=false;	
			}
			if(trabValido){
				
				tablaEstad.find("tbody").append(
						"<tr>" +
						"<td>"+val.puesto.areaName+"</td>" +				
						"<td>"+val.puesto.positionName+"</td>" +				
						"<td>"+val.locacion.nombre+"</td>" +
						"<td>"+val.trabajadorNombre+"</td>" +
						"<td>"+val.vacaciones.length+"</td>" +
						"<td>"+numDiasPendientes+"</td>" +
						"<td>"+numDiasCompletados+"</td>" +
						"<td style='color:blue'>"+(diasTotal)+"</td>" +
					"</tr>");
				}
		});
	}else{
		$("#thAsigVacacion").html("Días a favor");
		$('#tblVacTrab td:nth-child(6),#tblVacTrab th:nth-child(6)').hide();
		$('#tblVacTrab td:nth-child(7),#tblVacTrab th:nth-child(7)').hide();
		$('#tblVacTrab td:nth-child(8),#tblVacTrab th:nth-child(8)').hide();
		listFullTrabajadores.forEach(function(val,index){
			var trabValido=true;
			var diasFavor=val.numVacaciones;
			if(diasFavor>=daysMin){
				trabValido=true;
			}else{
				trabValido=false;	
			}
			if(trabValido){
				
				tablaEstad.find("tbody").append(
						"<tr>" +
						"<td>"+val.puesto.areaName+"</td>" +				
						"<td>"+val.puesto.positionName+"</td>" +				
						"<td>"+val.locacion.nombre+"</td>" +
						"<td>"+val.trabajadorNombre+"</td>" +
						"<td style='color:blue'>"+diasFavor+"</td>" +
					"</tr>");
				}
		});
	}
	
	
}

function llamarDatosIngresosCeses(){
	var dataParam = {
			mdfId : getUrlParameter("mdfId")
		};

		callAjaxPost(URL + '/trabajador/estadistica/contabilidad', dataParam,
				function(data){
			listTrabajadoresInOut=data.list;
			actualizarIngesosCeses();
		});
		
		
}

function actualizarIngesosCeses(){
	
	var tablaIngreso=$("#tblReportIngreso");
	var tablaCeses=$("#tblReportCeso");
	var fechaInicio=$("#inputInicioInOut").val();
	var fechaFin=$("#inputFinInOut").val();
	
	tablaIngreso.find("tbody").html("");
	tablaCeses.find("tbody").html("");
	listTrabajadoresInOut.forEach(function(val,index){
		var contratos=val.contratos;
		var ceses=val.ceses;
		console.log(val)
		contratos.forEach(function(val1,index1){
			var valido=true;
			var fechaComparar=convertirFechaNormal2(val1.fechaInicio);
			
			valido=fechaIncluidaEnRango(fechaComparar,fechaInicio,fechaFin);
			if(valido){
				tablaIngreso.find("tbody").append(
						"<tr>" +
						"<td>"+val.puesto.areaName+"</td>" +				
						"<td>"+val.puesto.positionName+"</td>" +				
						"<td>"+val.locacion.nombre+"</td>" +
						"<td>"+val.trabajadorNombre+"</td>" +
						"<td>"+convertirFechaNormal(val1.fechaInicio)+"</td>" +
						"<td>"+val.numSueldo+"</td>" +
						"" +
						"</tr>");
			}
			
		});
		
		ceses.forEach(function(val1,index1){
			var valido=true;
			var fechaComparar=convertirFechaNormal2(val1.fecha);
			console.log(val1)
			valido=fechaIncluidaEnRango(fechaComparar,fechaInicio,fechaFin);
			if(valido){
				tablaCeses.find("tbody").append(
						"<tr>" +
						"<td>"+val.puesto.areaName+"</td>" +				
						"<td>"+val.puesto.positionName+"</td>" +				
						"<td>"+val.locacion.nombre+"</td>" +
						"<td>"+val.trabajadorNombre+"</td>" +
						"<td>"+convertirFechaNormal(val1.fecha)+"</td>" +
						"<td>"+val1.motivo.nombre+"</td>" +
						"<td>"+val1.motivoDescripcion+"</td>" +
	"" +
						"</tr>");
			}
			
		});
		
		
	});
}




