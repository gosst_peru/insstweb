/**
 * 
 */


var contratoId;
var banderaEdicionObj;
var listFullContratos;
var listVigencia;
var listTiposContrato;
var estadoContrato={};

$(document).on('change', ':file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
    $("#btnDescargarFileCurso").hide();
    $("#btnDescargarFileSueldo").hide();
    $("#btnDescargarFileHijo").hide();
  });
 $(document).on('fileselect',"input:file", function(event, numFiles, label) {
	 
	          var input = $(this).parents('.input-group').find(':text'),
	              log = numFiles > 1 ? numFiles + ' files selected' : label;

	          if( input.length ) {
	              input.val(log);
	          } else {
	              if( log ) console.log(log);
	          }

	      });


function verContratosTrabajador(){
	$("#modalContratos").modal("show");
	$("#modalContratos").find(".modal-body").hide();
	$("#modalContratos").find(".modal-title").html("Historial Contratos");
	$("#modalContratos").find("#modalBodyContrato").show();
	
	llamarContratos();
}
function llamarContratos(){
	contratoId=0;
	banderaEdicionObj=false;
	listFullContratos=[];
	estadoContrato={};
	$("#btnAgregarContrato").show().attr("onclick","nuevoContrato()");
	$("#btnCancelarContrato").hide().attr("onclick","llamarContratos()");
	$("#btnEliminarContrato").hide().attr("onclick","eliminarContrato()");
	$("#btnGuardarContrato").hide().attr("onclick","guardarContrato()");
	var dataParam={trabajadorId:trabajadorId}
	callAjaxPost(URL + '/trabajador/contratos', dataParam, function(data) {
		
		var list = data.list;
		listFullContratos=data.list;
		listVigencia=data.listVigencia;
		listTiposContrato=data.listTiposContrato;
		$("#tblContratos tbody").html("");
		listFullContratos.forEach(function(val,index){
			$("#tblContratos tbody").append("<tr onclick='javascript:editarContrato("+
					index+")' >"+
					"<td id='conTipo"+
					val.id + "'>"+
					val.tipo.nombre + "</td>" +
					
					"<td id='conPuesto"+
					val.id + "'>"+
					val.puesto.positionName+ "</td>" +
					"<td id='conArea"+
					val.id + "'>"+
					val.puesto.areaName+ "</td>" +
					"<td id='conFInicio"+
					val.id + "'>"+
					val.fechaInicioTexto + "</td>" +
					"<td id='conVig"+
					val.id + "'>"+
					val.vigencia.nombre + "</td>" +
					"<td id='conFFin"+
					val.id + "'>"+
					val.fechaFinTexto + "</td>" +
					"<td id='conEstado"+
					val.id + "'>"+
					val.estado.nombre + "</td>" +
					"<td id='conEvi"+
					val.id + "'>"+
					(val.evidenciaNombre==null?"---":val.evidenciaNombre) + "</td>" +
					"</tr>")
		});
	
		formatoCeldaSombreableTabla(true,"tblContratos");
	
});
	
}
function editarContrato(pindex){
	if(!banderaEdicionObj){
		banderaEdicionObj=true;
		$("#btnAgregarContrato").hide();
		$("#btnCancelarContrato").show();
		$("#btnEliminarContrato").show();
		$("#btnGuardarContrato").show();
		formatoCeldaSombreableTabla(false,"tblContratos");
		contratoId=listFullContratos[pindex].id;
		var tipo=listFullContratos[pindex].tipo.id;
		var puesto=listFullContratos[pindex].puesto.positionId;
		var fInicio=convertirFechaNormal2(listFullContratos[pindex].fechaInicio);
		var fFin=convertirFechaNormal2(listFullContratos[pindex].fechaFin);
		var vigencia=listFullContratos[pindex].vigencia.id;
		var inputEvi="<button class='btn btn-success btnDescargar' style='float:left' id='btnDescargarFileCurso'>" +
		"<i class='fa fa-download' aria-hidden='true'></i>Descargar</button><div class='input-group'>" +
			"<label class='input-group-btn'>"+
             "<span class='btn btn-primary'>"+
              "<i class='fa fa-upload fa-lg'></i>   Subir Nuevo&hellip; <input style='display: none;'"+ 
               "type='file' name='fileEviArchivo' id='fileEviArchivo' accept='image/jpg,image/png,image/jpeg,image/gif'>"+
                "</span>"+
                "</label>"+
                "<input type='text' id='inputEviText' class='form-control' readonly>"+
       		 	"</div>";
		var eviNombre=listFullContratos[pindex].evidenciaNombre;
		$("#conEvi"+contratoId).html(inputEvi);
		$("#btnDescargarFileCurso").attr("onclick","location.href='"+ URL
				+ "/trabajador/contrato/evidencia?contratoId="
				+ contratoId+"'");
	 
	$("#inputEviText").val(eviNombre);
	var selTipo=crearSelectOneMenuOblig("selTipoCont","",listTiposContrato,tipo,"id","nombre");
	 $("#conTipo"+contratoId).html(selTipo);
	 $("#conArea"+contratoId).html("---");
	 //
	 var listPuesto=listarStringsDesdeArray(listPosition, "positionName");
	  
	 $("#conPuesto"+contratoId).html("<input class='form-control' id='inputPuestoCont'>");
	 listPosition.forEach(function(val,index){
		 if(val.positionId== puesto){
			$("#inputPuestoCont").val(val.positionName);
		 }
	 })
	 ponerListaSugerida("inputPuestoCont",listPuesto,true);
	 $("#inputPuestoCont").on("autocompleteclose",function(){
		
	 });
	 var selVigencia=crearSelectOneMenuOblig("selVigenciaCont","estadoPeriodoContrato()",listVigencia,vigencia,"id","nombre");
	 $("#conVig"+contratoId).html(selVigencia);
	 $("#conFInicio"+contratoId).html("<input type='date' class='form-control' id='dateInicioCont' onchange='estadoPeriodoContrato()' >");
	 $("#conFFin"+contratoId).html("<input type='date' class='form-control' id='dateFinCont'  onchange='estadoPeriodoContrato()' >");
		$("#dateInicioCont").val(fInicio);
		$("#dateFinCont").val(fFin);
	}
}
function nuevoContrato(){
	$("#btnAgregarContrato").hide();
	$("#btnCancelarContrato").show();
	$("#btnEliminarContrato").hide();
	$("#btnGuardarContrato").show();
	var inputEvi="<div class='input-group'>" +
	"<label class='input-group-btn'>"+
     "<span class='btn btn-primary'>"+
      "<i class='fa fa-upload fa-lg'></i>   Subir Nuevo&hellip; <input style='display: none;'"+ 
       "type='file' name='fileEviArchivo' id='fileEviArchivo' accept='image/jpg,image/png,image/jpeg,image/gif'>"+
        "</span>"+
        "</label>"+
        "<input type='text' id='inputEviText' class='form-control' readonly>"+
		 	"</div>";
	banderaEdicionObj=true;
	var selTipo=crearSelectOneMenuOblig("selTipoCont","",listTiposContrato,"0","id","nombre");
	  
	 var selVigencia=crearSelectOneMenuOblig("selVigenciaCont","estadoPeriodoContrato()",listVigencia,"0","id","nombre");
	contratoId = 0;
	$("#tblContratos tbody")
			.prepend(
					"<tr id='tr0'>"
					+"<td>"+selTipo+"</td>"
					+"<td>"+"<input class='form-control' id='inputPuestoCont'>"+"</td>"
					+"<td>"+""+"</td>"
					+"<td>"
					+"<input type='date' class='form-control' id='dateInicioCont' onchange='estadoPeriodoContrato()'>"+"</td>"
					+"<td>"+selVigencia+"</td>"
					+"<td id='conFFin0'>"+"<input type='date' class='form-control' id='dateFinCont'  onchange='estadoPeriodoContrato()'>"+"</td>"
					+"<td id='conEstado0'>"+""+"</td>"
					+"<td>"+inputEvi+" </td>"
							+ "</tr>");
	 var listPuesto=listarStringsDesdeArray(listPosition, "positionName");
	ponerListaSugerida("inputPuestoCont",listPuesto,true);
	formatoCeldaSombreableTabla(false,"tblContratos");
}

function eliminarContrato(){
	var objetivoObj={
			id:contratoId
	}
	var r=confirm("¿Está seguro de eliminar este contrato?");
	if(r){
		callAjaxPost(URL + '/trabajador/contrato/delete', objetivoObj,
				function(data) {
					
					
			llamarContratos();
					
				});
	}
	
}

function guardarContrato(){
	var tipo=$("#selTipoCont").val();
	var puesto=null;
	 listPosition.forEach(function(val,index){
		 if(val.positionName== $("#inputPuestoCont").val()){
			 puesto=val.positionId;
		 }
	 })
	var fechaInicio=$("#dateInicioCont").val();
	 var fechaFin=$("#dateFinCont").val();
	var vigencia=$("#selVigenciaCont").val();
	var estado=estadoContrato;
	
	var campoVacio=false;
	var msjCompletar="Complete ";
	if(puesto==null){
		campoVacio=true;
		msjCompletar+="el puesto de trabajo";
		
	}
	if(fechaInicio.length==0){
		campoVacio=true;
		msjCompletar+=", fecha de inicio";
	}
	if(fechaFin.length==0){
		campoVacio=true;
		msjCompletar+=", fecha fin";
	}
	if(campoVacio){
		alert(msjCompletar)
	}else{
		var objetivoObj={
				id:contratoId,
				tipo:{id:tipo},
				puesto:{positionId:puesto},
				fechaInicio:convertirFechaTexto(fechaInicio),
				fechaFin:convertirFechaTexto(fechaFin),
vigencia:{id:vigencia},
				estado:estado,
				trabajadorId:trabajadorId
		}
		console.log(objetivoObj);
		callAjaxPost(URL + '/trabajador/contrato/save', objetivoObj,
				function(data) {
				var nuevoId=	data.nuevoId;
			guardarEvidenciaAuto(nuevoId,"fileEviArchivo"
					,bitsContrato,"/trabajador/contrato/evidencia/save",llamarContratos);
			
					
				});
	}
	
}



function estadoPeriodoContrato(){
	//Cargando
	var fechaInicio=$("#dateInicioCont").val();
	var vigencia=$("#selVigenciaCont").val();
	var hoyDia=obtenerFechaActual();
	var numMeses=0;
	listVigencia.forEach(function(val,index){
		if(val.id==vigencia){
			numMeses=val.numMeses;
		}
	});
	var fechaFin=sumaFechaDias(numMeses*30.5,fechaInicio);
	var fechaFinTexto=sumaFechaDias2(numMeses*30.5,fechaInicio);
	
	$("#dateFinCont").val(fechaFin);
	
	var difInicio= restaFechas(hoyDia,fechaInicio);
	var difFinal= restaFechas(hoyDia,fechaFin);
	if(difInicio>0){
		estadoContrato={id:"1",nombre:"Pendiente"};
	}else{
		if(difFinal>0){
			estadoContrato={id:"2",nombre:"Vigente"};
		}else{
			estadoContrato={id:"3",nombre:"Caducado"};
		}
	}
	
	$("#conEstado"+contratoId).html(estadoContrato.nombre+"");
	
	
}	

	
	
	
	

