/**
 * 
 */


var hijoId;
var banderaEdicionHijo;
var listFullHijos;
function verHijosTrabajador(){
	$("#modalContratos").modal("show");
	$("#modalContratos").find(".modal-body").hide();
	$("#modalContratos").find(".modal-title").html("Hijos del Trabajador");
	$("#modalContratos").find("#modalBodyHijo").show();
	
	llamarHijos();
}
function llamarHijos(){
	hijoId=0;
	banderaEdicionHijo=false;
	listFullHijos=[];
	$("#btnAgregarHijo").show().attr("onclick","nuevoHijo()");
	$("#btnCancelarHijo").hide().attr("onclick","llamarHijos()");
	$("#btnEliminarHijo").hide().attr("onclick","eliminarHijo()");
	$("#btnGuardarHijo").hide().attr("onclick","guardarHijo()");
	var dataParam={trabajadorId:trabajadorId}
	callAjaxPost(URL + '/trabajador/hijos', dataParam, function(data) {
		
		var list = data.list;console.log(listFullHijos);console.log(dataParam)
		listFullHijos=list;
		$("#tblHijos tbody").html("");
		listFullHijos.forEach(function(val,index){
			$("#tblHijos tbody").append("<tr onclick='javascript:editarHijo("+
					index+")' >"+
					"<td id='sonNombre"+
					val.id + "'>"+
					val.nombre + "</td>" +
					"<td id='sonApP"+
					val.id + "'>"+
					val.apellidoPaterno + "</td>" +
					"<td id='sonApM"+
					val.id + "'>"+
					val.apellidoMaterno + "</td>" +
					"<td id='sonGenero"+
					val.id + "'>"+
					val.genero.generoNombre + "</td>" +
					"<td id='sonFecha"+
					val.id + "'>"+
					val.fechaNacimientoTexto + "</td>" +
					"<td id='sonEdad"+
					val.id + "'>"+
					val.edad + "</td>" +
					"<td id='sonEvi"+
					val.id + "'>"+
					(val.evidenciaNombre==null?"---":val.evidenciaNombre) + "</td>" +
					"</tr>")
		});
	
		formatoCeldaSombreableTabla(true,"tblHijos");
	
});
	
}
function editarHijo(pindex){
	if(!banderaEdicionHijo){
		banderaEdicionHijo=true;
		$("#btnAgregarHijo").hide();
		$("#btnCancelarHijo").show();
		$("#btnEliminarHijo").show();
		$("#btnGuardarHijo").show();
		formatoCeldaSombreableTabla(false,"tblHijos");
		hijoId=listFullHijos[pindex].id;
		var nombre=listFullHijos[pindex].nombre;
		var apellidoPaterno=listFullHijos[pindex].apellidoPaterno;
		var apellidoMaterno=listFullHijos[pindex].apellidoMaterno;
		var genero=listFullHijos[pindex].genero.generoId;
		var fecha=convertirFechaNormal2(listFullHijos[pindex].fechaNacimiento);
		var inputEvi="<button class='btn btn-success btnDescargar' style='float:left' id='btnDescargarFileHijo'>" +
		"<i class='fa fa-download' aria-hidden='true'></i>Descargar</button><div class='input-group'>" +
			"<label class='input-group-btn'>"+
             "<span class='btn btn-primary'>"+
              "<i class='fa fa-upload fa-lg'></i>   Subir Nuevo&hellip; <input style='display: none;'"+ 
               "type='file' name='fileEviHijo' id='fileEviHijo' accept='image/jpg,image/png,image/jpeg,image/gif'>"+
                "</span>"+
                "</label>"+
                "<input type='text' id='inputEviText' class='form-control' readonly>"+
       		 	"</div>";
		var eviNombre=listFullHijos[pindex].evidenciaNombre;
		$("#sonEvi"+hijoId).html(inputEvi);
		$("#btnDescargarFileCurso").attr("onclick","location.href='"+ URL
				+ "/trabajador/hijo/evidencia?hijoId="
				+ hijoId+"'");
	 
		///
		$("#sonNombre"+hijoId).html("<input type='text' class='form-control' id='inputNombreHijo'>");
		$("#sonApP"+hijoId).html("<input type='text' class='form-control' id='inputApellidoPatHijo'>");
		$("#sonApM"+hijoId).html("<input type='text' class='form-control' id='inputApellidoMatHijo'>");
		var selGenero = crearSelectOneMenuOblig("selGeneroHijo", "", listGenero,
				genero, "generoId", "generoNombre");
		$("#sonGenero"+hijoId).html(selGenero);
		$("#sonFecha"+hijoId).html("<input type='date' class='form-control' id='dateNacimientoHijo'>");
		
		///
		$("#inputNombreHijo").val(nombre);
		$("#inputApellidoPatHijo").val(apellidoPaterno);
		$("#inputApellidoMatHijo").val(apellidoMaterno);
		$("#dateNacimientoHijo").val(fecha);
		
	$("#inputEviText").val(eviNombre);

	
	
	
	$("#dateNacimientoHijo").on("change",function(){
		var edadd=calcularEdadAuto($(this).val());
		$("#tedadHijo" + hijoId).html(edadd);
	});
	}
}
function nuevoHijo(){
	$("#btnAgregarHijo").hide();
	$("#btnCancelarHijo").show();
	$("#btnEliminarHijo").hide();
	$("#btnGuardarHijo").show();
	var inputEvi="<div class='input-group'>" +
	"<label class='input-group-btn'>"+
     "<span class='btn btn-primary'>"+
      "<i class='fa fa-upload fa-lg'></i>   Subir Nuevo&hellip; <input style='display: none;'"+ 
       "type='file' name='fileEviHijo' id='fileEviHijo' accept='image/jpg,image/png,image/jpeg,image/gif'>"+
        "</span>"+
        "</label>"+
        "<input type='text' id='inputEviText' class='form-control' readonly>"+
		 	"</div>";
	banderaEdicionHijo=true;
	var selGenero = crearSelectOneMenuOblig("selGeneroHijo", "", listGenero,
			"-1", "generoId", "generoNombre");
	hijoId = 0;
	$("#tblHijos tbody")
			.append(
					"<tr id='tr0'>"
					+"<td><input type='text' class='form-control' id='inputNombreHijo'  ></td>"
					+"<td><input type='text' class='form-control' id='inputApellidoPatHijo'  ></td>"
					+"<td><input type='text' class='form-control' id='inputApellidoMatHijo'  ></td>"
					+"<td>"+selGenero+"</td>"
					+"<td>"+"<input type='date' class='form-control' id='dateNacimientoHijo' >"+"</td>"
					+"<td id='sonEdad0'>"+""+"</td>"
					
					+"<td>"+inputEvi+"</td>"
							+ "</tr>");
	formatoCeldaSombreableTabla(false,"tblHijos");
	$("#dateNacimientoHijo").on("change",function(){
		var edadd=calcularEdadAuto($(this).val());
		$("#sonEdad" + hijoId).html(edadd);
	});
}

function eliminarHijo(){
	var objetivoObj={
			id:hijoId
	}
	var r=confirm("¿Está seguro de eliminar este hijo?");
	if(r){
		callAjaxPost(URL + '/trabajador/hijo/delete', objetivoObj,
				function(data) {
					
					
			llamarHijos();
					
				});
	}
	
}

function guardarHijo(){
	var apellidoPaterno=$("#inputApellidoPatHijo").val();
	var apellidoMaterno=$("#inputApellidoMatHijo").val();
	var nombre=$("#inputNombreHijo").val();
	var fechaNacimiento=$("#dateNacimientoHijo").val();
	var genero=$("#selGeneroHijo").val();
	var campoVacio=false;
	 
	
	if(campoVacio){
		alert("Complete el campo")
	}else{
		var objetivoObj={
				id:hijoId,
				nombre:nombre,
				apellidoPaterno:apellidoPaterno,
				apellidoMaterno:apellidoMaterno,
				genero:{generoId :genero},
				fechaNacimiento:convertirFechaTexto(fechaNacimiento),
				
				trabajadorId:trabajadorId
		}
		console.log(objetivoObj);
		callAjaxPost(URL + '/trabajador/hijo/save', objetivoObj,
				function(data) {
				var nuevoId=	data.nuevoId;
			guardarEvidenciaAuto(nuevoId,"fileEviHijo"
					,bitsHijo,"/trabajador/hijo/evidencia/save",llamarHijos);
			
					
				});
	}
	
}



	
	
	
	

