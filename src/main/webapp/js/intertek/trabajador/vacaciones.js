/**
 * 
 */


var vacacionId;
var banderaEdicionVacacion;
var listFullVacacions;

var listMotivoVacacion=[];
var estadoVacacion={};
function verVacacionesTrabajador(){
	$("#modalContratos").modal("show");
	$("#modalContratos").find(".modal-body").hide();
	$("#modalContratos").find(".modal-title").html("Vacaciones");
	$("#modalContratos").find("#modalBodyVacacion").show();
	
	llamarVacacions();
	
}
function llamarVacacions(){
	vacacionId=0;
	banderaEdicionVacacion=false;
	listFullVacacions=[];
	listMotivoVacacion=[];
	estadoVacacion={};
	$("#btnAgregarVacacion").show().attr("onclick","nuevoVacacion()");
	$("#btnCancelarVacacion").hide().attr("onclick","llamarVacacions()");
	$("#btnEliminarVacacion").hide().attr("onclick","eliminarVacacion()");
	$("#btnGuardarVacacion").hide().attr("onclick","guardarVacacion()");
	var dataParam={trabajadorId:trabajadorId }
	callAjaxPost(URL + '/trabajador/vacaciones', dataParam, function(data) {
		
		var list = data.list;
		listFullVacacions=data.list;
		listMotivoVacacion=data.listMotivoVacacion;
		
		$("#tblVacaciones tbody").html("");
		listFullVacacions.forEach(function(val,index){
			$("#tblVacaciones tbody").append("<tr onclick='javascript:editarVacacion("+
					index+")' >"+
					"<td id='motVac"+
					val.id + "'>"+
					val.motivo.nombre+ "</td>" +
					"<td id='fInicioVac"+
					val.id + "'>"+
					val.fechaInicioTexto + "</td>" +
					"<td id='fFinVac"+
					val.id + "'>"+
					val.fechaFinTexto + "</td>" +
					"<td id='vacEstado"+
					val.id + "'>"+
					val.estado.nombre + "</td>" +
					"<td id='tdDiaVac"+
					val.id + "'>"+
					(val.diasVacacion+1) + "</td>" +
					"<td id='reempVac"+
					val.id + "'>"+
					val.reemplazo.trabajadorNombre + "</td>" +
					
					"<td id='eviVac"+
					val.id + "'>"+
					(val.evidenciaNombre==null?"---":val.evidenciaNombre) + "</td>" +
					"</tr>")
		});
	
		formatoCeldaSombreableTabla(true,"tblVacaciones");
	
});
	
}
function editarVacacion(pindex){
	if(!banderaEdicionVacacion){
		banderaEdicionVacacion=true;
		$("#btnAgregarVacacion").hide();
		$("#btnCancelarVacacion").show();
		$("#btnEliminarVacacion").show();
		$("#btnGuardarVacacion").show();
		formatoCeldaSombreableTabla(false,"tblVacaciones");
		vacacionId=listFullVacacions[pindex].id;
		var motivo=listFullVacacions[pindex].motivo.id;
		var selMotivo = crearSelectOneMenuOblig("selMotivoVac", "", listMotivoVacacion,
				motivo, "id", "nombre");
		 $("#motVac"+vacacionId).html(selMotivo);
		//
		var fInicio=convertirFechaNormal2(listFullVacacions[pindex].fechaInicio);
		 $("#fInicioVac"+vacacionId).html("<input type='date' class='form-control' id='dateInicioVac' onchange='verDiasVacaciones()'>");
		 $("#dateInicioVac").val(fInicio);
		 //
		var fFin=convertirFechaNormal2(listFullVacacions[pindex].fechaFin);
		 $("#fFinVac"+vacacionId).html("<input type='date' class='form-control' id='dateFinVac' onchange='verDiasVacaciones()'>");
		 $("#dateFinVac").val(fFin);
		 //
		 
		 var reemplazo=listFullVacacions[pindex].reemplazo.trabajadorNombre;
		 $("#reempVac"+vacacionId).html("<input type='text' class='form-control' id='textReempVac'>");
			$("#textReempVac").val(reemplazo);
			var listaTrabs=listarStringsDesdeArray(listFullTrabajadores,"trabajadorNombreCompleto");
			ponerListaSugerida("textReempVac",listaTrabs,true);
			
			//
		var inputEvi=crearFormEvidencia("Vacacion",false,"#eviVac"+vacacionId);
		var eviNombre=listFullVacacions[pindex].evidenciaNombre;
		
		$("#btnDescargarFileVacacion").attr("onclick","location.href='"+ URL
				+ "/trabajador/vacacion/evidencia?vacacionId="
				+ vacacionId+"'");
	 
	$("#inputEviVacacion").val(eviNombre);
	verDiasVacaciones();
	
	
	

	
	}
}
function nuevoVacacion(){
	$("#btnAgregarVacacion").hide();
	$("#btnCancelarVacacion").show();
	$("#btnEliminarVacacion").hide();
	$("#btnGuardarVacacion").show();
	var inputEvi=crearFormEvidencia("Vacacion",true);
	var selMotivo = crearSelectOneMenuOblig("selMotivoVac", "", listMotivoVacacion,
			"-1", "id", "nombre");
	banderaEdicionVacacion=true;
	vacacionId = 0;
	$("#tblVacaciones tbody")
			.append(
					"<tr id='tr0'>"
					+"<td>"+selMotivo+"</td>"
					+"<td>"+"<input type='date' class='form-control' id='dateInicioVac' onchange='verDiasVacaciones()'>"+"</td>"
					+"<td>"+"<input type='date' class='form-control' id='dateFinVac' onchange='verDiasVacaciones()'>"+"</td>"
					+"<td id='vacEstado0'></td>"
					+"<td id='tdDiaVac0'>"+"..."+"</td>"
					+"<td>"+"<input type='text' class='form-control' id='textReempVac' >"+"</td>"
					
					
					+"<td>"+inputEvi+" </td>"
							+ "</tr>");
	var listaTrabs=listarStringsDesdeArray(listFullTrabajadores,"trabajadorNombreCompleto");
	ponerListaSugerida("textReempVac",listaTrabs,true);
	formatoCeldaSombreableTabla(false,"tblVacaciones");
}

function eliminarVacacion(){
	var objetivoObj={
			id:vacacionId
	}
	var r=confirm("¿Está seguro de eliminar esta vacación?");
	if(r){
		callAjaxPost(URL + '/trabajador/vacacion/delete', objetivoObj,
				function(data) {
					
					
			llamarVacacions();
					
				});
	}
	
}

function guardarVacacion(){
	var motivo=$("#selMotivoVac").val();
	var fechaInicio=$("#dateInicioVac").val();
	var fechaFin=$("#dateFinVac").val();
	var reemplazo=0;
	listFullTrabajadores.forEach(function(val,index){
		if(val.trabajadorNombreCompleto==$("#textReempVac").val()){
			reemplazo=val.trabajadorId;
		}
	});
	
	var campoVacio=false;
	if(reemplazo==0){
		alert("Reemplazo es obligatorio");
		campoVacio=true;
	}
	
	if(campoVacio){
		
	}else{
		var objetivoObj={
				id:vacacionId,
				fechaInicio:convertirFechaTexto(fechaInicio),
				fechaFin:convertirFechaTexto(fechaFin),
				reemplazo:{trabajadorId:reemplazo},
				motivo:{id:motivo},
				estado:estadoVacacion,
				trabajadorId:trabajadorId
		}
		console.log(objetivoObj);
		callAjaxPost(URL + '/trabajador/vacacion/save', objetivoObj,
				function(data) {
				var nuevoId=	data.nuevoId
			guardarEvidenciaAuto(nuevoId,"fileEviVacacion"
					,bitsVacacion,"/trabajador/vacacion/evidencia/save",llamarVacacions);
			
					
				});
	}
	
}

function verDiasVacaciones(){
	var fechaInicio=$("#dateInicioVac").val();
	var fechaFin=$("#dateFinVac").val();
	var difFecha=restaFechas(fechaInicio,fechaFin)+1;
	$("#tdDiaVac"+vacacionId).html(difFecha+"");
	estadoPeriodoVacacion();
}




function estadoPeriodoVacacion(){
	
	
	var fechaInicio=$("#dateInicioVac").val();
	var fechaFin=$("#dateFinVac").val();
var hoyDia=obtenerFechaActual();
	
	var difInicio= restaFechas(hoyDia,fechaInicio);
	var difFinal= restaFechas(hoyDia,fechaFin);
	if(difInicio>0){
		estadoVacacion={id:"1",nombre:"Pendiente"};
	}else{
		if(difFinal>0){
			estadoVacacion={id:"2",nombre:"En Curso"};
		}else{
			estadoVacacion={id:"3",nombre:"Completado"};
		}
	}
	
	$("#vacEstado"+vacacionId).html(estadoVacacion.nombre);
	
	
}	

	

