/**
 * 
 */


var memoId;
var banderaEdicionMemo;
var listFullMemos;
var listMotivoMemo;

function verMemosTrabajador(){
	$("#modalContratos").modal("show");
	$("#modalContratos").find(".modal-body").hide();
	$("#modalContratos").find(".modal-title").html("Historial Memorándums");
	$("#modalContratos").find("#modalBodyMemo").show();
	
	llamarMemos();
}
function llamarMemos(){
	memoId=0;
	banderaEdicionMemo=false;
	listFullMemos=[];
	$("#btnAgregarMemo").show().attr("onclick","nuevoMemo()");
	$("#btnCancelarMemo").hide().attr("onclick","llamarMemos()");
	$("#btnEliminarMemo").hide().attr("onclick","eliminarMemo()");
	$("#btnGuardarMemo").hide().attr("onclick","guardarMemo()");
	var dataParam={trabajadorId:trabajadorId}
	callAjaxPost(URL + '/trabajador/memos', dataParam, function(data) {
		
		var list = data.list;
		listFullMemos=data.list;
		listMotivoMemo=data.listMotivoMemo;
		$("#tblMemos tbody").html("");
		listFullMemos.forEach(function(val,index){
			$("#tblMemos tbody").append("<tr onclick='javascript:editarMemo("+
					index+")' >"+
					"<td id='memMotivo"+
					val.id + "'>"+
					val.motivo.nombre + "</td>" +
					
					"<td id='memoDesc"+
					val.id + "'>"+
					val.descripcion+ "</td>" +
					"<td id='memoIni"+
					val.id + "'>"+
					val.iniciador+ "</td>" +
					"<td id='memofec"+
					val.id + "'>"+
					val.fechaTexto+ "</td>" +
					"<td id='memoEvi"+
					val.id + "'>"+
					(val.evidenciaNombre==null?"---":val.evidenciaNombre) + "</td>" +
					"</tr>")
		});
	
		formatoCeldaSombreableTabla(true,"tblMemos");
	
});
	
}
function editarMemo(pindex){
	if(!banderaEdicionMemo){
		banderaEdicionMemo=true;
		$("#btnAgregarMemo").hide();
		$("#btnCancelarMemo").show();
		$("#btnEliminarMemo").show();
		$("#btnGuardarMemo").show();
		formatoCeldaSombreableTabla(false,"tblMemos");
		memoId=listFullMemos[pindex].id;
		
		//
		var motivo=listFullMemos[pindex].motivo.id;
		var selMotivo=crearSelectOneMenuOblig("selMotivoMemo","",listMotivoMemo,motivo,"id","nombre");
		$("#memMotivo"+memoId).html(selMotivo);
		//
		var fecha=convertirFechaNormal2(listFullMemos[pindex].fecha);
		$("#memofec"+memoId).html("<input type='date' class='form-control' id='dateFechaMemo' >");
		$("#dateFechaMemo").val(fecha);
		//
		var descripcion=listFullMemos[pindex].descripcion;
		$("#memoDesc"+memoId).html("<input  class='form-control' id='textDescMemo' >");
		$("#textDescMemo").val(descripcion);
		//
		var iniciador=listFullMemos[pindex].iniciador;
		$("#memoIni"+memoId).html("<input  class='form-control' id='textIniMemo' >");
		$("#textIniMemo").val(iniciador);
		//
		crearFormEvidencia("Memo",false,"#memoEvi"+memoId);
		var eviNombre=listFullMemos[pindex].evidenciaNombre;
		$("#btnDescargarFileMemo").attr("onclick","location.href='"+ URL
				+ "/trabajador/memo/evidencia?memoId="
				+ memoId+"'");	 
		$("#inputEviMemo").val(eviNombre);
	}
}
function nuevoMemo(){
	$("#btnAgregarMemo").hide();
	$("#btnCancelarMemo").show();
	$("#btnEliminarMemo").hide();
	$("#btnGuardarMemo").show();
	var selMotivo=crearSelectOneMenuOblig("selMotivoMemo","",listMotivoMemo,"","id","nombre");
	var inputEvi=crearFormEvidencia("Memo",true);
	banderaEdicionMemo=true;
	memoId = 0;
	$("#tblMemos tbody")
			.append(
					"<tr id='tr0'>"
					+"<td>"+selMotivo+"</td>"
					+"<td>"+"<input type='text' class='form-control' id='textDescMemo' >"+"</td>"
					+"<td>"+"<input type='text' class='form-control' id='textIniMemo' >"+"</td>"
					+"<td>"+"<input type='date' class='form-control' id='dateFechaMemo' >"+"</td>"
					
					+"<td>"+inputEvi+" </td>"
							+ "</tr>");
	formatoCeldaSombreableTabla(false,"tblMemos");
}

function eliminarMemo(){
	var objetivoObj={
			id:memoId
	}
	var r=confirm("¿Está seguro de eliminar este memo?");
	if(r){
		callAjaxPost(URL + '/trabajador/memo/delete', objetivoObj,
				function(data) {
					
					
			llamarMemos();
					
				});
	}
	
}

function guardarMemo(){
	var motivo=$("#selMotivoMemo").val();
	var descripcion=$("#textDescMemo").val();
	var iniciador=$("#textIniMemo").val();
	var fecha=$("#dateFechaMemo").val();
	var campoVacio=false;
	 
	
	if(campoVacio){
		alert("Complete el campo")
	}else{
		var objetivoObj={
				id:memoId,
				fecha:convertirFechaTexto(fecha),
				iniciador:iniciador,
				descripcion:descripcion,
				motivo:{id:motivo},
			trabajadorId:trabajadorId
		}
		console.log(objetivoObj);
		callAjaxPost(URL + '/trabajador/memo/save', objetivoObj,
				function(data) {
				var nuevoId=	data.nuevoId
			guardarEvidenciaAuto(nuevoId,"fileEviMemo"
					,bitsMemo,"/trabajador/memo/evidencia/save",llamarMemos);
			
					
				});
	}
	
}


	

	
	
	
	

