/**
 * 
 */


var estudioId;
var banderaEdicionEstudio;
var listFullEstudios;
var listVigencia;
var listTipoEstudio;
var estadoEstudio={};


function verEstudiosTrabajador(){
	$("#modalContratos").modal("show");
	$("#modalContratos").find(".modal-body").hide();
	$("#modalContratos").find(".modal-title").html("Estudios del Trabajador");
	$("#modalContratos").find("#modalBodyEstudio").show();
	
	llamarEstudios();
}
function llamarEstudios(){
	estudioId=0;
	banderaEdicionEstudio=false;
	listFullEstudios=[];
	estadoEstudio={};
	$("#btnAgregarEstudio").show().attr("onclick","nuevoEstudio()");
	$("#btnCancelarEstudio").hide().attr("onclick","llamarEstudios()");
	$("#btnEliminarEstudio").hide().attr("onclick","eliminarEstudio()");
	$("#btnGuardarEstudio").hide().attr("onclick","guardarEstudio()");
	var dataParam={trabajadorId:trabajadorId}
	callAjaxPost(URL + '/trabajador/estudios', dataParam, function(data) {
		
		var list = data.list;
		listFullEstudios=data.list;
		listVigencia=data.listVigencia;
		listTipoEstudio=data.listTipoEstudio;
		$("#tblEstudios tbody").html("");
		listFullEstudios.forEach(function(val,index){
			$("#tblEstudios tbody").append("<tr onclick='javascript:editarEstudio("+
					index+")' >"+
					"<td id='estuTip"+
					val.id + "'>"+
					val.tipo.nombre + "</td>" +
					"<td id='estuPago"+
					val.id + "'>"+
					(val.pagoPropio==0?"SI":"NO")+ "</td>" +
					"<td id='estuNombre"+
					val.id + "'>"+
					val.nombre+ "</td>" +
					"<td id='estuEsp"+
					val.id + "'>"+
					val.especialidad+ "</td>" +
					"<td id='estuFinicio"+
					val.id + "'>"+
					val.fechaInicioTexto + "</td>" +
					"<td id='estuFfin"+
					val.id + "'>"+
					val.fechaFinTexto + "</td>" +
					"<td id='estuEstado"+
					val.id + "'>"+
					val.estado.nombre + "</td>" +
					"<td id='estuEvi"+
					val.id + "'>"+
					(val.evidenciaNombre==null?"---":val.evidenciaNombre) + "</td>" +
					"</tr>")
		});
	
		formatoCeldaSombreableTabla(true,"tblEstudios");
	
});
	
}
function editarEstudio(pindex){
	if(!banderaEdicionEstudio){
		banderaEdicionEstudio=true;
		$("#btnAgregarEstudio").hide();
		$("#btnCancelarEstudio").show();
		$("#btnEliminarEstudio").show();
		$("#btnGuardarEstudio").show();
		formatoCeldaSombreableTabla(false,"tblEstudios");
		estudioId=listFullEstudios[pindex].id;
		//
		var tipo=listFullEstudios[pindex].tipo.id;
		var selTipoEstudio=crearSelectOneMenuOblig("selTipoEstudio","",listTipoEstudio,tipo,"id","nombre");
		$("#estuTip"+estudioId).html(selTipoEstudio);
		//
		var pago=listFullEstudios[pindex].pagoPropio;
			$("#estuPago"+estudioId).html("<input type='checkbox' id='estudioPagoCheck' class='form-control' "+
					 (pago==0?"checked":"" )+ ">");
		
		//
		var nombre=listFullEstudios[pindex].nombre;
		$("#estuNombre"+estudioId).html("<input class='form-control' id='textNombreEstudio'>");
		$("#textNombreEstudio").val(nombre);
		//
		var especialidad=listFullEstudios[pindex].especialidad;
		$("#estuEsp"+estudioId).html("<input class='form-control' id='textEspEstudio'>");
		$("#textEspEstudio").val(especialidad);
		//
		var fechaInicio=convertirFechaNormal2(listFullEstudios[pindex].fechaInicio);
		$("#estuFinicio"+estudioId).html("<input type='date' class='form-control' id='dateInicioEstudio' onchange='estadoPeriodoEstudio()'>");
		$("#dateInicioEstudio").val(fechaInicio);
		//
		var fechaFin=convertirFechaNormal2(listFullEstudios[pindex].fechaFin);
		$("#estuFfin"+estudioId).html("<input type='date' class='form-control' id='dateFinEstudio' onchange='estadoPeriodoEstudio()'>");
		$("#dateFinEstudio").val(fechaFin);
		//
		crearFormEvidencia("Estudio",false,"#estuEvi"+estudioId)
		var eviNombre=listFullEstudios[pindex].evidenciaNombre;
		$("#btnDescargarFileEstudio").attr("onclick","location.href='"+ URL
				+ "/trabajador/estudio/evidencia?estudioId="
				+ estudioId+"'");	 
		$("#inputEviEstudio").val(eviNombre);
	estadoPeriodoEstudio();
	}
}
function nuevoEstudio(){
	$("#btnAgregarEstudio").hide();
	$("#btnCancelarEstudio").show();
	$("#btnEliminarEstudio").hide();
	$("#btnGuardarEstudio").show();
	var selTipoEstudio=crearSelectOneMenuOblig("selTipoEstudio","",listTipoEstudio,"","id","nombre");
	var inputEvi=crearFormEvidencia("Estudio",true);
	banderaEdicionEstudio=true;

	estudioId = 0;
	$("#tblEstudios tbody")
			.append(
					"<tr id='tr0'>"
					+"<td>"+selTipoEstudio+"</td>"
					+"<td><input type='checkbox' id='estudioPagoCheck' class='form-control'></td>"
					+"<td>"+"<input class='form-control' id='textNombreEstudio'>"+"</td>"
					+"<td>"+"<input class='form-control' id='textEspEstudio'>"+"</td>"
					+"<td>"
					+"<input type='date' class='form-control' id='dateInicioEstudio' onchange='estadoPeriodoEstudio()'>"+"</td>"
					+"<td>"+"<input type='date' class='form-control' id='dateFinEstudio' onchange='estadoPeriodoEstudio()'>"+"</td>"
					+"<td id='estuEstado0'>"+""+"</td>"
					+"<td>"+inputEvi+" </td>"
							+ "</tr>");
	formatoCeldaSombreableTabla(false,"tblEstudios");
}

function eliminarEstudio(){
	var objetivoObj={
			id:estudioId
	}
	var r=confirm("¿Está seguro de eliminar este estudio?");
	if(r){
		callAjaxPost(URL + '/trabajador/estudio/delete', objetivoObj,
				function(data) {
					
					
			llamarEstudios();
					
				});
	}
	
}

function guardarEstudio(){
	var tipo=$("#selTipoEstudio").val();
	var nombre=$("#textNombreEstudio").val();
	var espec=$("#textEspEstudio").val();
	var fechaInicio=$("#dateInicioEstudio").val();
	var fechaFin=$("#dateFinEstudio").val();
	var pagoIntertek=$("#estudioPagoCheck").prop("checked");
	var estado=estadoEstudio;
	
	var campoVacio=false;
	 
	
	if(campoVacio){
		alert("Complete el campo")
	}else{
		var objetivoObj={
				id:estudioId,
				tipo:{id:tipo},
				nombre:nombre,
				especialidad:espec,
				fechaInicio:convertirFechaTexto(fechaInicio),
				fechaFin:convertirFechaTexto(fechaFin),
				estado:estado,
				pagoPropio:(pagoIntertek?0:1),
				trabajadorId:trabajadorId
		}
		console.log(objetivoObj);
		callAjaxPost(URL + '/trabajador/estudio/save', objetivoObj,
				function(data) {
				var nuevoId=	data.nuevoId;
			guardarEvidenciaAuto(nuevoId,"fileEviEstudio"
					,bitsEstudio,"/trabajador/estudio/evidencia/save",llamarEstudios);
			
					
				});
	}
	
}



function estadoPeriodoEstudio(){
	
	
	var fechaInicio=$("#dateInicioEstudio").val();
	var fechaFin=$("#dateFinEstudio").val();
var hoyDia=obtenerFechaActual();
	
	var difInicio= restaFechas(hoyDia,fechaInicio);
	var difFinal= restaFechas(hoyDia,fechaFin);
	if(difInicio>0){
		estadoEstudio={id:"1",nombre:"Pendiente"};
	}else{
		if(difFinal>0){
			estadoEstudio={id:"2",nombre:"En Curso"};
		}else{
			if(fechaFin==""){
				estadoEstudio={id:"2",nombre:"En Curso"};
			}else{
				estadoEstudio={id:"3",nombre:"Completado"};
			}
			
		}
	}
	
	$("#estuEstado"+estudioId).html(estadoEstudio.nombre);
	
	
}	

	
	
	
	

