/**
 * 
 */


var sueldoId;
var banderaEdicionSueldo;
var listFullSueldos;
var estadoSueldo={};

function verSueldosTrabajador(){
	$("#modalContratos").modal("show");
	$("#modalContratos").find(".modal-body").hide();
	$("#modalContratos").find(".modal-title").html("Historial Remuneraciones");
	$("#modalContratos").find("#modalBodySueldo").show();
	
	llamarSueldos();
}
function llamarSueldos(){
	sueldoId=0;
	banderaEdicionSueldo=false;
	listFullSueldos=[];
	estadoSueldo={};
	$("#btnAgregarSueldo").show().attr("onclick","nuevoSueldo()");
	$("#btnCancelarSueldo").hide().attr("onclick","llamarSueldos()");
	$("#btnEliminarSueldo").hide().attr("onclick","eliminarSueldo()");
	$("#btnGuardarSueldo").hide().attr("onclick","guardarSueldo()");
	var dataParam={trabajadorId:trabajadorId}
	callAjaxPost(URL + '/trabajador/sueldos', dataParam, function(data) {
		
		var list = data.list;
		listFullSueldos=data.list;
		$("#tblSueldos tbody").html("");
		listFullSueldos.forEach(function(val,index){
			$("#tblSueldos tbody").append("<tr onclick='javascript:editarSueldo("+
					index+")' >"+
					"<td id='sulMonto"+
					val.id + "'>"+
					val.monto + "</td>" +
					
					"<td id='sulFecha"+
					val.id + "'>"+
					val.fechaTexto+ "</td>" +
					"<td id='sulEvi"+
					val.id + "'>"+
					(val.evidenciaNombre==null?"---":val.evidenciaNombre) + "</td>" +
					"</tr>")
		});
	
		formatoCeldaSombreableTabla(true,"tblSueldos");
	
});
	
}
function editarSueldo(pindex){
	if(!banderaEdicionSueldo){
		banderaEdicionSueldo=true;
		$("#btnAgregarSueldo").hide();
		$("#btnCancelarSueldo").show();
		$("#btnEliminarSueldo").show();
		$("#btnGuardarSueldo").show();
		formatoCeldaSombreableTabla(false,"tblSueldos");
		sueldoId=listFullSueldos[pindex].id;
		var fInicio=convertirFechaNormal2(listFullSueldos[pindex].fecha);
		var monto=listFullSueldos[pindex].monto;
		var inputEvi="<button class='btn btn-success btnDescargar' style='float:left' id='btnDescargarFileSueldo'>" +
		"<i class='fa fa-download' aria-hidden='true'></i>Descargar</button><div class='input-group'>" +
			"<label class='input-group-btn'>"+
             "<span class='btn btn-primary'>"+
              "<i class='fa fa-upload fa-lg'></i>   Subir Nuevo&hellip; <input style='display: none;'"+ 
               "type='file' name='fileEviSueldo' id='fileEviSueldo' accept='image/jpg,image/png,image/jpeg,image/gif'>"+
                "</span>"+
                "</label>"+
                "<input type='text' id='inputEviText' class='form-control' readonly>"+
       		 	"</div>";
		var eviNombre=listFullSueldos[pindex].evidenciaNombre;
		$("#sulMonto"+sueldoId).html("<input type='number' class='form-control' id='numMontoSueldo'>");
		$("#sulEvi"+sueldoId).html(inputEvi);
		$("#btnDescargarFileSueldo").attr("onclick","location.href='"+ URL
				+ "/trabajador/sueldo/evidencia?sueldoId="
				+ sueldoId+"'");
	 
	$("#inputEviText").val(eviNombre);

	 $("#sulFecha"+sueldoId).html("<input type='date' class='form-control' id='dateInicioSueldo' onchange='estadoPeriodoSueldo()'>");
	$("#dateInicioSueldo").val(fInicio);
	$("#numMontoSueldo").val(monto);
	}
}
function nuevoSueldo(){
	$("#btnAgregarSueldo").hide();
	$("#btnCancelarSueldo").show();
	$("#btnEliminarSueldo").hide();
	$("#btnGuardarSueldo").show();
	var inputEvi="<div class='input-group'>" +
	"<label class='input-group-btn'>"+
     "<span class='btn btn-primary'>"+
      "<i class='fa fa-upload fa-lg'></i>   Subir Nuevo&hellip; <input style='display: none;'"+ 
       "type='file' name='fileEviSueldo' id='fileEviSueldo' accept='image/jpg,image/png,image/jpeg,image/gif'>"+
        "</span>"+
        "</label>"+
        "<input type='text' id='inputEviText' class='form-control' readonly>"+
		 	"</div>";
	banderaEdicionSueldo=true;
	sueldoId = 0;
	$("#tblSueldos tbody")
			.append(
					"<tr id='tr0'>"
					+"<td>"+"<input type='number' class='form-control' id='numMontoSueldo' >"+"</td>"
					+"<td>"+"<input type='date' class='form-control' id='dateInicioSueldo' >"+"</td>"
					
					+"<td>"+inputEvi+" </td>"
							+ "</tr>");
	formatoCeldaSombreableTabla(false,"tblSueldos");
}

function eliminarSueldo(){
	var objetivoObj={
			id:sueldoId
	}
	var r=confirm("¿Está seguro de eliminar este remuneración?");
	if(r){
		callAjaxPost(URL + '/trabajador/sueldo/delete', objetivoObj,
				function(data) {
					
					
			llamarSueldos();
					
				});
	}
	
}

function guardarSueldo(){
	var monto=$("#numMontoSueldo").val();
	var fechaInicio=$("#dateInicioSueldo").val();
	
	var campoVacio=false;
	 
	
	if(campoVacio){
		alert("Complete el campo")
	}else{
		var objetivoObj={
				id:sueldoId,
				fecha:convertirFechaTexto(fechaInicio),
				monto:monto,
				trabajadorId:trabajadorId
		}
		console.log(objetivoObj);
		callAjaxPost(URL + '/trabajador/sueldo/save', objetivoObj,
				function(data) {
				var nuevoId=	data.nuevoId
			guardarEvidenciaAuto(nuevoId,"fileEviSueldo"
					,bitsSueldo,"/trabajador/sueldo/evidencia/save",llamarSueldos);
			
					
				});
	}
	
}


	

	
	
	
	

