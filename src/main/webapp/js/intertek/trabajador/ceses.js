/**
 * 
 */


var ceseId;
var banderaEdicionCese;
var listFullCeses;
var listMotivoCese;

function verCesesTrabajador(){
	$("#modalContratos").modal("show");
	$("#modalContratos").find(".modal-body").hide();
	$("#modalContratos").find(".modal-title").html("Historial Cese");
	$("#modalContratos").find("#modalBodyCese").show();
	
	llamarCeses();
}
function llamarCeses(){
	ceseId=0;
	banderaEdicionCese=false;
	listFullCeses=[];
	listMotivoCese=[];
	$("#btnAgregarCese").show().attr("onclick","nuevoCese()");
	$("#btnCancelarCese").hide().attr("onclick","llamarCeses()");
	$("#btnEliminarCese").hide().attr("onclick","eliminarCese()");
	$("#btnGuardarCese").hide().attr("onclick","guardarCese()");
	var dataParam={trabajadorId:trabajadorId}
	callAjaxPost(URL + '/trabajador/ceses', dataParam, function(data) {
		
		var list = data.list;
		listFullCeses=data.list;
		listMotivoCese=data.listMotivoCese;
		$("#tblCeses tbody").html("");
		listFullCeses.forEach(function(val,index){
			$("#tblCeses tbody").append("<tr onclick='javascript:editarCese("+
					index+")' >"+
					"<td id='ceseMot"+
					val.id + "'>"+
					val.motivo.nombre + "</td>" +
					
					"<td id='ceseDesc"+
					val.id + "'>"+
					val.motivoDescripcion+ "</td>" +
					"<td id='ceseFecha"+
					val.id + "'>"+
					val.fechaTexto+ "</td>" +
					"<td id='ceseEviA"+
					val.id + "'>"+
					(val.evidenciaNombre==null?"---":val.evidenciaNombre) + "</td>" +
					"<td id='ceseEviB"+
					val.id + "'>"+
					(val.evidencia2Nombre==null?"---":val.evidencia2Nombre) + "</td>" +
					"<td id='ceseEviC"+
					val.id + "'>"+
					(val.evidencia3Nombre==null?"---":val.evidencia3Nombre) + "</td>" +
					"<td id='ceseEviD"+
					val.id + "'>"+
					(val.evidencia4Nombre==null?"---":val.evidencia4Nombre) + "</td>" +
					"<td id='ceseEviE"+
					val.id + "'>"+
					(val.evidencia5Nombre==null?"---":val.evidencia5Nombre) + "</td>" +
					"</tr>")
		});
	
		formatoCeldaSombreableTabla(true,"tblCeses");
	
});
	
}
function editarCese(pindex){
	if(!banderaEdicionCese){
		banderaEdicionCese=true;
		$("#btnAgregarCese").hide();
		$("#btnCancelarCese").show();
		$("#btnEliminarCese").show();
		$("#btnGuardarCese").show();
		formatoCeldaSombreableTabla(false,"tblCeses");
		ceseId=listFullCeses[pindex].id; 
		var motivo=listFullCeses[pindex].motivo.id;
		var selMotivo = crearSelectOneMenuOblig("selMotivoCese", "", listMotivoCese,
				motivo, "id", "nombre");
		$("#ceseMot"+ceseId).html(selMotivo);
		//
		var descripcion=listFullCeses[pindex].motivoDescripcion;
		$("#ceseDesc"+ceseId).html("<input class='form-control' id='textDescCese'>");
		$("#textDescCese").val(descripcion);
		//
		var fecha=convertirFechaNormal2(listFullCeses[pindex].fecha);
		$("#ceseFecha"+ceseId).html("<input type='date' class='form-control' id='dateCese'>");
		$("#dateCese").val(fecha);
		//
		crearFormEvidencia("CeseA",false,"#ceseEviA"+ceseId);
		crearFormEvidencia("CeseB",false,"#ceseEviB"+ceseId);
		crearFormEvidencia("CeseC",false,"#ceseEviC"+ceseId);
		crearFormEvidencia("CeseD",false,"#ceseEviD"+ceseId);
		crearFormEvidencia("CeseE",false,"#ceseEviE"+ceseId);
		var eviNombre=listFullCeses[pindex].evidenciaNombre;
		var evi2Nombre=listFullCeses[pindex].evidencia2Nombre;
		var evi3Nombre=listFullCeses[pindex].evidencia3Nombre;
		var evi4Nombre=listFullCeses[pindex].evidencia4Nombre;
		var evi5Nombre=listFullCeses[pindex].evidencia5Nombre;
	$("#btnDescargarFileCeseA").attr("onclick","location.href='"+ URL
				+ "/trabajador/cese/evidencia?ceseId="
				+ ceseId+"&tipoId=1'");
	$("#btnDescargarFileCeseB").attr("onclick","location.href='"+ URL
			+ "/trabajador/cese/evidencia?ceseId="
			+ ceseId+"&tipoId=2'");
	$("#btnDescargarFileCeseC").attr("onclick","location.href='"+ URL
			+ "/trabajador/cese/evidencia?ceseId="
			+ ceseId+"&tipoId=3'");
	$("#btnDescargarFileCeseD").attr("onclick","location.href='"+ URL
			+ "/trabajador/cese/evidencia?ceseId="
			+ ceseId+"&tipoId=4'");
	$("#btnDescargarFileCeseE").attr("onclick","location.href='"+ URL
			+ "/trabajador/cese/evidencia?ceseId="
			+ ceseId+"&tipoId=5'");
	 
	$("#inputEviCeseA").val(eviNombre);
	$("#inputEviCeseB").val(evi2Nombre);
	$("#inputEviCeseC").val(evi3Nombre);
	$("#inputEviCeseD").val(evi4Nombre);
	$("#inputEviCeseE").val(evi5Nombre);
	}
}
function nuevoCese(){
	$("#btnAgregarCese").hide();
	$("#btnCancelarCese").show();
	$("#btnEliminarCese").hide();
	$("#btnGuardarCese").show();
	var selMotivo = crearSelectOneMenuOblig("selMotivoCese", "", listMotivoCese,
			"", "id", "nombre");
	var formA=crearFormEvidencia("CeseA",true);
	var formB=crearFormEvidencia("CeseB",true);
	var formC=crearFormEvidencia("CeseC",true);
	var formD=crearFormEvidencia("CeseD",true);
	var formE=crearFormEvidencia("CeseE",true);
	banderaEdicionCese=true;
	ceseId = 0;
	$("#tblCeses tbody")
			.append(
					"<tr id='tr0'>"
					+"<td>"+selMotivo+"</td>"
					+"<td>"+"<input  class='form-control' id='textDescCese' >"+"</td>"
					+"<td>"+"<input type='date' class='form-control' id='dateCese'>"+"</td>"
					+"<td>"+formA+" </td>"
					+"<td>"+formB+" </td>"
					+"<td>"+formC+" </td>"
					+"<td>"+formD+" </td>"
					+"<td>"+formE+" </td>"
							+ "</tr>");
	formatoCeldaSombreableTabla(false,"tblCeses");
}

function eliminarCese(){
	var objetivoObj={
			id:ceseId
	}
	var r=confirm("¿Está seguro de eliminar este remuneración?");
	if(r){
		callAjaxPost(URL + '/trabajador/cese/delete', objetivoObj,
				function(data) {
					
					
			llamarCeses();
					
				});
	}
	
}

function guardarCese(){
	var motivo=$("#selMotivoCese").val();
	var descripcion=$("#textDescCese").val();
	var fecha=$("#dateCese").val();
	var campoVacio=false;
	 
	
	if(campoVacio){
		alert("Complete el campo")
	}else{
		var objetivoObj={
				id:ceseId,fecha:convertirFechaTexto(fecha),
				motivo:{id:motivo},
				motivoDescripcion:descripcion,
				trabajadorId:trabajadorId
		}
		callAjaxPost(URL + '/trabajador/cese/save', objetivoObj,
				function(data) {
				var nuevoId=	data.nuevoId
				guardarEvidenciaMultipleAuto(nuevoId,"fileEviCeseA"
					,bitsCese,"/trabajador/cese/evidencia/save",preLlamarCeses,1);
				guardarEvidenciaMultipleAuto(nuevoId,"fileEviCeseB"
						,bitsCese,"/trabajador/cese/evidencia/save",preLlamarCeses,2);
				guardarEvidenciaMultipleAuto(nuevoId,"fileEviCeseC"
						,bitsCese,"/trabajador/cese/evidencia/save",preLlamarCeses,3);
				guardarEvidenciaMultipleAuto(nuevoId,"fileEviCeseD"
						,bitsCese,"/trabajador/cese/evidencia/save",preLlamarCeses,4);
				guardarEvidenciaMultipleAuto(nuevoId,"fileEviCeseE"
						,bitsCese,"/trabajador/cese/evidencia/save",preLlamarCeses,5);
				
					
				});
	}
	
}
var numEvidenciasGuardadas=0;
var numEvidenciasTotal=5;
function preLlamarCeses(){
	numEvidenciasGuardadas++;
	console.log(numEvidenciasGuardadas);
	if(numEvidenciasGuardadas==numEvidenciasTotal){
		llamarCeses();
		numEvidenciasGuardadas=0;
	};
}

	

	
	
	
	

